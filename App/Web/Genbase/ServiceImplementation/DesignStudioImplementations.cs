﻿using Genbase.BLL;
using Genbase.classes;
using Genbase.Classes;
using Genbase.IdentityServer;
using Genbase.ServiceInterfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;
using VariableHandler = Genbase.BLL.VariableHandler;

namespace Genbase.ServiceImplementation
{
    public class DesignStudioImplementations: DesignStudioInterfaces
    {
        IdentityTokens identityTokens = new IdentityTokens();
        VariableClass cv = new VariableClass();
        Robot robot = new Robot();
        cSystems_Robots csystems_Robots = new cSystems_Robots();
        Entities inputString = new Entities();
        Project project = new Project();
        Workflow workflow = new Workflow();
        Classes.Action action = new Classes.Action();
        cElement celement = new cElement();
        Step step = new Step();
        AppExecution appExecution = new AppExecution();
        ResponseResult responseResult = new ResponseResult();
        List<ResponseResult> SaveWorkflowResponse = new List<ResponseResult>();

        public string GenerateJwtToken(string mail, string MobilePhone)
        {
            if (mail == null) return "Mail can't be Null";
            if (MobilePhone == null) return "Mobile Number can't be Null";
            try
            {
                return identityTokens.GenerateJwtToken(mail, MobilePhone);
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }

        }

        public string ValidateToken(string JWToken)
        {
            try
            {
                if (JWToken == null)
                {
                    return "Token Can't be null";
                }
                IdentityTokens identityTokens = new IdentityTokens();
                string value = identityTokens.ValidateJwtToken(JWToken);
                if (value.Contains("Token failed validation"))
                {
                    return "Token failed validation";
                }
                else if (value.Contains("Token was invalid"))
                {
                    return "Token was invalid";
                }
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string Create(string input)
        {

            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
               // Entities inputString = new Entities();
                inputString = EntityService(input);
                if (inputString.Type == "Robot")
                {
                    return robot.createRobot(inputString.RobotType, inputString.Name, inputString.Parent, inputString.Description, inputString.CreateBy, inputString.UpdateBy);
                }
                else if (inputString.Type == "Project")
                {
                    return project.createProjectWorkflow(inputString.Type, inputString.Name, inputString.Description, inputString.projectType, inputString.LOBId, inputString.username, inputString.WorkFlowProjectType, inputString.CreateBy);
                }
                else if (inputString.Type == "Workflow")
                {
                    return workflow.createWorkflow(inputString.Type, inputString.Name, inputString.Parent);
                }
                else if (inputString.Type == "ProcessManager")
                {
                    return robot.cProcessManager(inputString.username, inputString.Robot_ID, inputString.userid);
                }
                else
                {
                    return "wrong";
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public static Entities EntityService(string input)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Entities inputString;
                inputString = JsonConvert.DeserializeObject<Entities>(input);
                return inputString;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return null;
            }
        }

        public string Get(string input)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
               // Entities inputString = new Entities();
                inputString = EntityService(input);
                if (inputString.Type == StringHelper.AllActionsByType)
                {
                    return action.getActions(inputString.projectType);
                }
                if (inputString.Type == StringHelper.AllActions)
                {
                    return action.getActions();
                }
                else if (inputString.Type == StringHelper.AllElements)
                {
                    return celement.getElements();
                }
                else if (inputString.Type == StringHelper.AllSteps)
                {
                    return step.getSteps(inputString.Parent);
                }
                else if (inputString.Type == "AllWorkflows")
                {
                    return workflow.getWorkflows(inputString.Parent);
                }
                else if (inputString.Type == "AllRobots")
                {
                    return robot.getRobots(inputString.Parent);
                }
                else if (inputString.Type == "AllRobotsByPrjId")
                {
                    return robot.getRobotsByProjectId(inputString.Project_Id);
                }
                else if (inputString.Type == "AllProjects")
                {
                    return project.getProjects();
                }
                else if (inputString.Type == "AllActionProperties")
                {
                    return action.getProperties();
                }
                else if (inputString.Type == "Robot")
                {
                    
                    return robot.getRobot(inputString.Name);
                }
                else if (inputString.Type == "Project")
                {
                    return project.getProject(inputString.Name);
                }
                else if (inputString.Type == "Workflow")
                {
                   
                    return workflow.getWorkflow(inputString.Name);
                }
                else if (inputString.Type == "allWorkflows")
                {
                   
                    return workflow.getAllWorkflows();
                }
                else if (inputString.Type == "allRobots")
                {
                    
                    return robot.getallRobots();
                }
                else if (inputString.Type == "stepProperties")
                {
                    return step.getStepProperties(inputString.Step_Id);
                }
                else if (inputString.Type == "AllstepProperties")
                {
                    return step.getAllStepProperties();
                }
                else if (inputString.Type == "StepsLinks")
                {
                    string x = robot.getRobots(inputString.Project_Id);
                    return x;
                }

                else if (inputString.Type == "AllProjectsnRobots")
                {
                    
                    return project.getProjectsRobots();
                }
                else if (inputString.Type == "AllVersionSteps")
                {

                    
                    string x = robot.getVersionRobots(inputString.rid, inputString.Version_Id);
                    return x;
                }
                else if (inputString.Type == "PredefinedBots")
                {
                    return new Predefined().loadPredefinedBotDetails();
                }
                else
                {
                    return "wrong";
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }

        }

        public string Delete(string input)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                inputString = EntityService(input);
                if (inputString.Type.ToLower() == StringHelper.robot.ToLower())
                {
                    return robot.deleteRobot(inputString.Name, inputString.Parent);

                }
                else if (inputString.Type.ToLower() == StringHelper.project.ToLower())
                {
                    return project.deleteProject(inputString.Name);
                }
                else if (inputString.Type.ToLower() == StringHelper.workflow.ToLower())
                {
                    return workflow.deleteWorkflow(inputString.Name);
                }
                else
                {
                    return StringHelper.wrongg;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string SaveWorkflow(Entities inputString)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                //inputString = EntityService(input["input"]);
                string result = new Save().SaveRobot(inputString);
                string state = result.Replace(" ", string.Empty);
                var robot = inputString.robot;
                //if (inputString.robot.dynamicvariables != null && inputString.robot.dynamicvariables.Count > 0)
                //{
                //    foreach (DVariables variable in inputString.robot.dynamicvariables)
                //    {
                //        string success = cv.setVariables(variable, robot.Id);
                //        if (success.ToLower() == "success")
                //        {
                //            if (Variables.dynamicvariables != null && Variables.dynamicvariables.Count > 0)
                //            {
                //                Variables.dynamicvariables.Add(variable);
                //            }
                //            else
                //            {
                //                Variables.dynamicvariables = new List<DVariables>();
                //                Variables.dynamicvariables.Add(variable);
                //            }
                //        }
                //    }
                //}

                switch (state)
                {
                    case "stepssaved":
                        responseResult.StatusCode = 200;
                        break;
                    case "robotupdated":
                        responseResult.StatusCode = 200;
                        break;
                    case "robotsavedandversioncreated":
                        responseResult.StatusCode = 200;
                        break;
                    default:
                        responseResult.StatusCode = 201;
                        break;

                }
                responseResult.ResultString = result;
                responseResult.StatusMessage = result;
                return JsonConvert.SerializeObject(responseResult);
            }

            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                responseResult.ResultString = "Robot not saved.Please fill the properties";
                return JsonConvert.SerializeObject(responseResult);

            }
        }
        public Entities entityService(string input)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Entities inputString;
                inputString = JsonConvert.DeserializeObject<Entities>(input);
                return inputString;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return null;
            }
        }
        public string Update(string input)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
               // Entities inputString = new Entities();
                inputString = EntityService(input);
                if (inputString.Type == "Steps")
                {
                    return string.Empty;
                }
                else if (inputString.Type == "stepproperties")
                {
                    return step.updateStepProperties(inputString.Step_Id, inputString.ID, inputString.newstepvalue);
                }
                else if (inputString.Type == "workflow")
                {
                    return workflow.updateWorkflow(inputString.Name);
                }
                else if (inputString.Type == "SystemsRobots")
                {
                   
                    return csystems_Robots.updateSysRobots(inputString.SystemName, inputString.Robot_ID);
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }

        }

        public string ExecuteRobot(string input)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                inputString = EntityService(input);
                string result = step.getStepsStepProperties(inputString.rid);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }

        }

        public void GetAllNextStep(Steps step)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<Steps> steps = new List<Steps>();
                List<LinkNodes> lnks = new List<LinkNodes>();
                List<Steps> OrderedSteps = new List<Steps>();
                Steps CurrentStep = steps.Find(sp => sp.Id == step.Id);
                LinkNodes LinkCurrentStep = lnks.Find(link => link.StepId == step.Id);
                Steps NextStep = steps.Find(sp => sp.Id == LinkCurrentStep.ChildStepIds.Split('`')[0]);
                if (NextStep != null)
                {
                    OrderedSteps.Add(NextStep);
                    GetAllNextStep(NextStep);
                }
            }
            catch (Exception ex)

            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
            }
        }

        public string debug_Order(string input)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<Steps> steps = new List<Steps>();
                List<LinkNodes> lnks = new List<LinkNodes>();
                List<Steps> OrderedSteps = new List<Steps>();
                Entities inputString = JsonConvert.DeserializeObject<Entities>(input);
                string robotid = inputString.ID;
                string rbtDetails = new Robot().getRobotProperties(robotid, string.Empty);
                List<Robots> rbts = JsonConvert.DeserializeObject<List<Robots>>(rbtDetails);
                steps = rbts[0].Steps;
                lnks = rbts[0].LinkNodes;
                Steps StartStep = steps.Find(stp => stp.Action_Id == "1");
                OrderedSteps.Add(StartStep);
                GetAllNextStep(StartStep);
                return JsonConvert.SerializeObject(OrderedSteps);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string debug_Execution(string input)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            responseResult = new ResponseResult();
            try
            {
                Entities inputString = JsonConvert.DeserializeObject<Entities>(input);
                string stepid = string.Empty;
                Variables.vDebugger.Description = string.Empty;
                StepDebug dbg = new StepDebug();
                inputString = EntityService(input);

                string ExecutionID = new StringFunction().getRandomString("32");

                new VariableHandler().CreateVariable("ExecutedUser", inputString.Namer, string.Empty, true, "string", inputString.rid, ExecutionID);
                stepid = new BLL.Validator().DebugStep(int.Parse(inputString.rid), int.Parse(inputString.Step_Id), ExecutionID);

                dbg = Variables.vDebugger;
                dbg.getNextstepid = stepid;
                responseResult.ResultString = JsonConvert.SerializeObject(dbg);
                return JsonConvert.SerializeObject(responseResult);
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 201;
                responseResult.ExceptionStatus = true;
                responseResult.StatusMessage = ex.Message.ToString();
                responseResult.ResultString = string.Empty;
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return JsonConvert.SerializeObject(responseResult);
            }
        }

        public string Set(Entities inputString)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
               // Entities inputString = new Entities();
                cSQLScripts cSS = new cSQLScripts();
               // inputString = cSS.Base64Decode(inputString);
               // inputString = EntityService(input);
                Result rs = new Result();
                //if (inputString.dynamicvariables != null && inputString.dynamicvariables.Count != 0)
                //{
                //    Variables.dynamicvariables.Clear();
                //    Variables.dynamicvariables.AddRange(inputString.dynamicvariables.ConvertAll(x => new DVariables() { vlname = x.vlname, vlvalue = x.vlvalue, vlstatus = x.vlstatus }));

                //}
                if (inputString.uploadFiles != null && inputString.uploadFiles.Count > 0)
                {
                    Variables.uploads = inputString.uploadFiles;
                }
                if (inputString.Type == "Steps")
                {
                    Step cs = new Step();
                    return cs.setSteps(inputString.Name, inputString.rid, inputString.wid, inputString.eid, inputString.aid, inputString.Order, inputString.RuntimeUserInput, "1", inputString.Project_Id, inputString.CreateBy, inputString.UpdateBy);
                }
                else if (inputString.Type == "stepproperties")
                {
                    Step csp = new Step();
                    return csp.setStepProperties(inputString.stepProperty, inputString.stepvalue, inputString.Idm, inputString.Namer, inputString.oldstepvalue, inputString.newstepvalue, inputString.StepPropertyType, inputString.Order);
                }
                else if (inputString.Type == "Execution")
                {
                    string ExecutionID = new StringFunction().getRandomString("32");

                    if (inputString.dynamicvariables != null && inputString.dynamicvariables.Count != 0)
                    {
                        //Variables.dynamicvariables.Clear();
                        Variables.dynamicvariables.AddRange(inputString.dynamicvariables.ConvertAll(x => new DVariables() { vlname = x.vlname, vlvalue = x.vlvalue, vlstatus = x.vlstatus, ExecutionID = ExecutionID, RobotID = inputString.ID }));
                    }


                    robot.updateProcessManager(inputString.systemId_pm, "In Progress");
                    string projectid = inputString.Project_Id;
                    string robotid = inputString.ID;
                    string CreateBy = inputString.CreateBy;
                    string UpdateBy = inputString.UpdateBy;
                    new ExecutionLog().deleteFromLogs(robotid);

                    Variables.vDebugger.ID = inputString.Step_Id;
                    Variables.robotid = robotid;
                    Variables.jobid = inputString.systemId_pm;

                    new BLL.VariableHandler().CreateVariable("ExecutedUser", inputString.Namer, string.Empty, true, "string", robotid, ExecutionID);

                    int ExecResult = appExecution.startRobot(int.Parse(robotid), 0, ExecutionID);

                    string state = Variables.state.ToString();

                    rs.SuccessState = state;
                    robot.updateProcessManager(inputString.systemId_pm, rs.SuccessState);
                    if ((state.ToLower() != StringHelper.sucess) && (state.ToLower() != StringHelper.operationfailed) && (state.ToLower() != StringHelper.nosteps))
                    {
                        if (rs.SuccessState.ToLower().IndexOf("inprogress") > -1)
                        {
                            RuntimeUserInputRequest ruir = new RuntimeUserInputRequest();
                            ruir.dvariables = Variables.dynamicvariables.FindAll(v => v.ExecutionID == ExecutionID);
                            ruir.nextStepID = new AppExecution().getNextStep(int.Parse(robotid), int.Parse(Variables.thisStep.Id), string.Empty).ToString();
                            ruir.step = Variables.thisStep;
                            ruir.thisVariableName = Variables.thisVariable;

                            rs.runtimerequest = ruir;
                            rs.elog = new List<ELogs>();
                            if (projectid != null && robotid != null)
                            {
                                rs.Downloads = DownloadFiles(projectid, robotid);
                                robot.createNotification(inputString.Name, inputString.Namer, state);
                            }
                        }
                        else
                        {
                            rs.elog = JsonConvert.DeserializeObject<List<ELogs>>(new ExecutionLog().getFromLogs(robotid, true));
                            rs.Downloads = DownloadFiles(projectid, robotid);
                        }

                    }
                    else if (rs.SuccessState.ToLower().IndexOf("success") > -1)
                    {
                        rs.Downloads = DownloadFiles(projectid, robotid);
                        robot.createNotification(inputString.Name, inputString.Namer, state);
                    }
                    if (state.ToLower() != StringHelper.sucess && state.ToLower() != "inprogress")
                    {
                        robot.updateProcessManager(inputString.systemId_pm, StringHelper.operationfailed);
                        robot.createNotification(inputString.Name, inputString.Namer, state);
                    }
                    else if (state.ToLower() == "inprogress")
                        robot.updateProcessManager(inputString.systemId_pm, "In Progress");
                    else
                        robot.updateProcessManager(inputString.systemId_pm, state);
                    rs.RobotId = Convert.ToInt32(robotid);
                    List<ELogs> elogs = JsonConvert.DeserializeObject<List<ELogs>>(new ExecutionLog().getFromLogs(robotid, true));
                    elogs.RemoveAll(e => e.Type == 702);
                    //foreach (var l in elogs)
                    //{
                    //    if (l.Type == 702)
                    //    {
                    //        robot.updateProcessManager(inputString.systemId_pm, StringHelper.operationfailed);
                    //        robot.createNotification(inputString.Name, inputString.Namer, state);
                    //    }
                    //}

                    //elogs.RemoveAll(e => e.Type == 700);

                    rs.elog = elogs;
                    rs.vdebugger = Variables.vDebugger;
                    //rs.MethodReturns = Variables.MethodReturns;
                    string res = JsonConvert.SerializeObject(rs);

                    responseResult.ResultString = res;
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == "ExecutionInProgress")
                {
                    string projectid = inputString.Project_Id;
                    string robotid = inputString.ID;
                    string num = inputString.stepvalue;
                    if (inputString.dynamicvariables != null && inputString.dynamicvariables.Count != 0)
                    {
                        Variables.dynamicvariables.RemoveAll(dv => dv.RobotID == robotid && dv.ExecutionID == inputString.dynamicvariables.First().ExecutionID);
                        Variables.dynamicvariables.AddRange(inputString.dynamicvariables.ConvertAll(x => new DVariables() { vlname = x.vlname, vlvalue = x.vlvalue, vlstatus = x.vlstatus, vlscope = x.vlscope, vltype = x.vltype, ExecutionID = x.ExecutionID, RobotID = x.RobotID }));
                    }
                    DVariables ty = null; string exeid = string.Empty;

                    if (inputString.dynamicvariables.Count > 0)
                    {
                        ty = inputString.dynamicvariables.First();
                        exeid = ty.ExecutionID;
                    }
                    else
                    {
                        exeid = new StringFunction().getRandomString("32");
                    }

                    int ExecResult = appExecution.startRobot(int.Parse(robotid), int.Parse(num), exeid);
                    string state = Variables.state.ToString();
                    robot.updateProcessManager(inputString.systemId_pm, state);
                    if ((state.ToLower() != StringHelper.sucess) && (state.ToLower() != StringHelper.operationfailed))
                    {

                        rs.RobotId = Convert.ToInt32(robotid);
                        rs.SuccessState = state;
                        if (rs.SuccessState.ToLower().IndexOf("inprogress") > -1)
                        {
                            RuntimeUserInputRequest ruir = new RuntimeUserInputRequest();
                            ruir.dvariables = Variables.dynamicvariables.FindAll(v => v.ExecutionID == exeid);
                            ruir.nextStepID = new AppExecution().getNextStep(int.Parse(robotid), int.Parse(Variables.thisStep.Id), string.Empty).ToString();
                            ruir.step = Variables.thisStep;
                            ruir.thisVariableName = Variables.thisVariable;

                            rs.runtimerequest = ruir;
                            rs.elog = new List<ELogs>();
                        }
                        else
                        {
                            rs.elog = JsonConvert.DeserializeObject<List<ELogs>>(new ExecutionLog().getFromLogs(robotid, true));
                            rs.Downloads = DownloadFiles(projectid, robotid);
                        }
                    }

                    else if (state.ToLower() == StringHelper.sucess)
                    {
                        rs.RobotId = Convert.ToInt32(robotid);
                        robot.createNotification(inputString.Name, inputString.Namer, state);
                    }
                    else if (state.ToLower() == StringHelper.operationfailed)
                    {
                        rs.RobotId = Convert.ToInt32(robotid);
                        robot.createNotification(inputString.Name, inputString.Namer, state);
                    }
                    if (state.ToLower() != StringHelper.sucess && state.ToLower() != "inprogress")
                        robot.updateProcessManager(inputString.systemId_pm, StringHelper.operationfailed);
                    else if (state.ToLower() == "inprogress")
                        robot.updateProcessManager(inputString.systemId_pm, "In Progress");
                    else
                        robot.updateProcessManager(inputString.systemId_pm, state);
                    List<ELogs> elogs = JsonConvert.DeserializeObject<List<ELogs>>(new ExecutionLog().getFromLogs(robotid, true));
                    elogs.RemoveAll(e => e.Type == 702);

                    rs.elog = elogs;
                    rs.SuccessState = state;
                    //rs.MethodReturns = Variables.MethodReturns;
                    string res = JsonConvert.SerializeObject(rs);
                    responseResult.ResultString = res;
                    return JsonConvert.SerializeObject(responseResult);

                }
                else
                {
                    responseResult.ResultString = StringHelper.ffalse;
                    return JsonConvert.SerializeObject(responseResult);
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }


        }

        private List<DownloadFilesObject> DownloadFiles(string projectid, string robotid)
        {
            List<DownloadFilesObject> returner = new List<DownloadFilesObject>();

            string vs = appExecution.getVersion(robotid);
            List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(step.getSteps(robotid, vs));

            List<Steps> blackone = steps.FindAll(stp => stp.Action_Id == "215");
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (blackone.Count > 0)
                {
                    if (Variables.downloads.Count > 0)
                    {
                        foreach (var download in Variables.downloads)
                        {
                            DownloadFilesObject dfo = new DownloadFilesObject();
                            dfo.FileName = download;
                            try
                            {
                                byte[] ress = System.IO.File.ReadAllBytes(download);
                                dfo.FileData = ress;
                            }
                            catch (Exception ex)
                            {
                                string ec = ex.Message;
                            }
                            returner.Add(dfo);
                        }
                    }
                }
                else
                {

                }

                return returner;
            }
            catch (Exception ex)

            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return null;
            }
        }

        public string getStudio(string roleId, string userId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Connection c = new Connection();
                string result = string.Empty; string result1 = string.Empty;
                string resultant = string.Empty;


                if (Convert.ToInt64(roleId) != 1)
                {
                    int i = 0; string query = string.Empty; string query1 = string.Empty;

                    if (int.TryParse(userId, out i))
                    {
                        query1 += "select \"UserName\" From \"USER\" WHERE \"ID\"='" + userId + "'";

                        result1 = c.executeSQL(query1, "Select");

                        List<Entities> sr = JsonConvert.DeserializeObject<List<Entities>>(result1);

                        resultant = (sr[0]).username;
                    }
                    else
                    {
                        query1 += "select \"ID\" From \"USER\" WHERE \"UserName\"='" + userId + "'";

                        result1 = c.executeSQL(query1, "Select");

                        List<Entities> sr = JsonConvert.DeserializeObject<List<Entities>>(result1);

                        resultant = (sr[0]).ID;
                    }

                    if (resultant.Trim() != string.Empty)
                    {
                        query += "select * from \"PROJECT\" where \"CreateBy\"='" + userId + "' or \"CreateBy\"='" + resultant + "' and \"Status\"=true order by \"ID\";";
                    }
                    else
                    {
                        query += "select * from \"PROJECT\" where \"CreateBy\"='" + userId + "'and \"Status\"=true order by \"ID\";";
                    }
                    result = c.executeSQL(query, "Select");
                }
                else
                {
                    string query = "select * from public.\"PROJECT\" as p inner join public.\"LOB\" as lb on p.\"LOBId\" = lb.\"ID\" where p.\"Status\"=true and lb.\"Status\"=true" ;
                    result = c.executeSQL(query, "Select");
                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string StepExecution(string input)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            responseResult = new ResponseResult();
            try
            {
                cSQLScripts cSS = new cSQLScripts();
                string atobinput = cSS.Base64Decode(input);
                Entities inputString = JsonConvert.DeserializeObject<Entities>(atobinput);
                inputString = EntityService(atobinput);
                BLL.MethodResult objResult = new BLL.MethodResult();
                new ExecutionLog().deleteFromLogs(inputString.ID);
                if (inputString.dynamicvariables != null)
                {
                    List<DVariables> duplilist = new List<DVariables>();
                    List<DVariables> frontList = new List<DVariables>();

                    duplilist.AddRange(Variables.dynamicvariables);

                    Variables.dynamicvariables.Clear();

                    frontList.AddRange(inputString.dynamicvariables.ConvertAll(x => new DVariables() { vlname = x.vlname, vlvalue = x.vlvalue, vlstatus = x.vlstatus }));

                    Variables.dynamicvariables.AddRange(duplilist.Union(frontList));

                }

                string ExecutionID = string.Empty;
                if (inputString.ID1 == string.Empty)
                {
                    ExecutionID = new StringFunction().getRandomString("32");
                }
                else
                {
                    ExecutionID = inputString.ID1;
                }

                objResult = new Validator().StepExecution(int.Parse(inputString.ID), int.Parse(inputString.Step_Id), ExecutionID);
                objResult.elog = JsonConvert.DeserializeObject<List<ELogs>>(new ExecutionLog().getFromLogs(inputString.ID, true));
                responseResult.ResultString = JsonConvert.SerializeObject(objResult);
                return JsonConvert.SerializeObject(responseResult);

            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 201;
                responseResult.ExceptionStatus = true;
                responseResult.StatusMessage = ex.Message.ToString();
                responseResult.ResultString = string.Empty;
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return JsonConvert.SerializeObject(responseResult);
            }
        }

        public string Validate(string LocalStepPairs)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            responseResult = new ResponseResult();
            try
            {
                //Entities inputString = new Entities();
                //Result rs = new Result();
                Steps cStep = JsonConvert.DeserializeObject<Steps>(LocalStepPairs);
                //inputString = entityService(LocalStepPairs);

                responseResult.ResultString = new Genbase.BLL.Validator().ValidateStep(cStep, inputString.ID1);
                return JsonConvert.SerializeObject(responseResult);
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 201;
                responseResult.ExceptionStatus = true;
                responseResult.StatusMessage = ex.Message.ToString();
                responseResult.ResultString = string.Empty;
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return JsonConvert.SerializeObject(responseResult);
            }

        }

        public string Upload(string fPath, string data1, bool isFolder, string previousFolderPath)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ext = Path.GetExtension(fPath);
                string name = Path.GetFileNameWithoutExtension(fPath);
                string filename = String.Empty; string filePath = String.Empty; string directorypath = String.Empty;
                if (isFolder)
                {
                    string[] files = fPath.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    if (previousFolderPath != string.Empty)
                        directorypath = previousFolderPath;
                    else
                        directorypath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), "Uploads");
                    //  directorypath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Uploads", files[0].Replace(" ", string.Empty) + new BLL.EncoderDecoderBase64().Base64Encode(Variables.username + DateTime.Now.ToString()) + DateTime.Now.ToString("MMddyyyyhhmmss"));
                    if (!Directory.Exists(directorypath))
                        Directory.CreateDirectory(directorypath);
                    string tempPath = directorypath;
                    for (int f = 1; f < files.Length - 1; f++)
                    {
                        tempPath += "\\" + files[f].Replace(" ", string.Empty);
                        if (!Directory.Exists(tempPath))
                            Directory.CreateDirectory(tempPath);
                    }
                    filename = files[files.Length - 1].Split('.')[0] + new BLL.EncoderDecoderBase64().Base64Encode(Variables.username + DateTime.Now.ToString()) + DateTime.Now.ToString("MMddyyyyhhmmss") + "." + fPath.Split('.')[1];
                    filePath = tempPath + "\\" + filename;
                }
                else
                {
                    filename = fPath;
                    // filename = fPath.Split('.')[0].Replace(" ", string.Empty) + new BLL.EncoderDecoderBase64().Base64Encode(Variables.username + DateTime.Now.ToString()) + DateTime.Now.ToString("MMddyyyyhhmmss") + "." + fPath.Split('.')[1];
                    //  filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Uploads", filename);
                    string datetime = DateTime.UtcNow.ToString().Replace('/', '-').Replace(':', '-').Replace(' ', '-');
                    filePath = Path.Combine(System.IO.Directory.GetCurrentDirectory(), "Uploads", name + datetime + ext);
                    directorypath = filePath;
                }
                var fileType = data1.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)[1];
                var attachment = data1.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)[2];
                byte[] data = Convert.FromBase64String(attachment.Replace("\"", ""));

                FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write);
                fs.Write(data, 0, data.Length);
                fs.Close();
                return directorypath;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string sendMail(string input)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
               // Entities inputString = new Entities();
                inputString = EntityService(input);

                Mail cm = new Mail();
                string ret;
                if (string.IsNullOrEmpty(inputString.subject))
                {
                    ret = cm.GetAllMailItems();
                }
                else
                {
                    ret = cm.getMailItems(inputString.subject);
                }
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string NewSaveWorkflow(Dictionary<string, string> input)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<Dictionary<string, string>> inputrobot = new List<Dictionary<string, string>>();
                inputrobot.Add(input);
                VariableClass cv = new VariableClass();
                List<string> state = new List<string>();
                List<string> result = new List<string>();
                foreach (var item in inputrobot)
                {

                   // Entities inputString = new Entities();
                    inputString = EntityService(item["input"]);
                    result.Add(new Save().SaveRobot(inputString));

                    // state = result.Replace(" ", string.Empty);
                    //"robot updated";robot saved and version created
                    var robot = inputString.robot;
                    if (inputString.robot.dynamicvariables != null && inputString.robot.dynamicvariables.Count > 0)
                    {
                        foreach (DVariables variable in inputString.robot.dynamicvariables)
                        {
                            string success = cv.setVariables(variable, robot.Id);
                            if (success.ToLower() == "success")
                            {
                                if (Variables.dynamicvariables != null && Variables.dynamicvariables.Count > 0)
                                {
                                    Variables.dynamicvariables.Add(variable);
                                }
                                else
                                {
                                    Variables.dynamicvariables = new List<DVariables>();
                                    Variables.dynamicvariables.Add(variable);
                                }
                            }
                        }
                    }
                }
                foreach (var item in result)
                {
                    switch (item)
                    {
                        case "stepssaved":
                            responseResult.StatusCode = 200;
                            break;
                        case "robotupdated":
                            responseResult.StatusCode = 200;
                            break;
                        case "robotsavedandversioncreated":
                            responseResult.StatusCode = 200;
                            break;
                        default:
                            responseResult.StatusCode = 201;
                            break;

                    }
                    responseResult.ResultString = item;
                    responseResult.StatusMessage = item;
                    SaveWorkflowResponse.Add(responseResult);
                }

                //if (state.ToLower().IndexOf("stepssaved")>-1)
                //{
                //    responseResult.StatusCode = 200;
                //}
                //if (state.ToLower().IndexOf("robotupdated") > -1)
                //{
                //    responseResult.StatusCode = 200;
                //}
                //if (state.ToLower().IndexOf("robotsavedandversioncreated") > -1)
                //{
                //    responseResult.StatusCode = 200;
                //}
                //else
                //{
                //    responseResult.StatusCode = 201;
                //}

                return JsonConvert.SerializeObject(SaveWorkflowResponse);

            }

            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                responseResult.ResultString = "Robot not saved.Please fill the properties";
                SaveWorkflowResponse.Add(responseResult);
                return JsonConvert.SerializeObject(SaveWorkflowResponse);

            }
        }
    }
}
