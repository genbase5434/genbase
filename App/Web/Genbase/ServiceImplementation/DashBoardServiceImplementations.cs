﻿using Genbase.BLL;
using Genbase.classes;
using Genbase.Classes;
using Genbase.Interface_DashBoard;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.Implementation_Dashboard
{
    public class DashBoardServiceImplementations : DashBoardServiceInterfaces
    {
		public async Task<string> dashboardIcons(int roleid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Monitor m = new Monitor();
                string result = m.dashboard(roleid);
                return  result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public async Task<string> updateDashboard(string Type , string roleid,string AddnewdashboardId, string dashboardId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Monitor m = new Monitor();
                string result = m.dashboardUpdate(Type, roleid, AddnewdashboardId, dashboardId);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public async Task<string> getCharts(int roleid, string monitor, string filters, string KPIid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string filterchartType = "";
                Monitor m = new Monitor();
                string feedback = m.KPIScreen(roleid, monitor, "KPI", KPIid);
                List<KPI> KPIList = JsonConvert.DeserializeObject<List<KPI>>(feedback);
                string KPIdbConnections = m.KPIdbConnection(roleid, "KPI");
                string minCompare = "";
                string maxCompare = "";
                string compareCheck = "";
                List<KPIConnection> KPIdbConnectionList = JsonConvert.DeserializeObject<List<KPIConnection>>(KPIdbConnections);
                int i = 0;
                int flag = 0;
                List<KPI> result = new List<KPI>();
                string minDate = ""; string maxDate = "";
                string timeSeries_minDate = ""; string timeSeries_maxDate = "";
                if (filters != "" && filters != null)
                {
                    Dictionary<string, string> filter = JsonConvert.DeserializeObject<Dictionary<string, string>>(filters);
                    compareCheck = filter["compareCheck"];
                    if (filter["type"] == "Today")
                    {
                        minDate = DateTime.Now.ToString("yyyy-MM-dd");
                        maxDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                    }
                    else if (filter["type"] == "Last Hour")
                    {
                        minDate = DateTime.Now.AddHours(-1).ToString("yyyy-MM-dd HH:mm:ss");
                        maxDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    }
                    else if (filter["type"] == "Yesterday")
                    {
                        minDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                        maxDate = DateTime.Now.ToString("yyyy-MM-dd");
                    }
                    else if (filter["type"] == "This Week")
                    {
                        minDate = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                        //maxDate = DateTime.Now.ToString("yyyy-MM-dd");
                        maxDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                    }
                    else if (filter["type"] == "This Month")
                    {
                        minDate = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd");
                        //maxDate = DateTime.Now.ToString("yyyy-MM-dd");
                        maxDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                    }
                    else if (filter["type"] == "This Year")
                    {
                        minDate = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
                        //maxDate = DateTime.Now.ToString("yyyy-MM-dd");
                        maxDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                    }
                    else if (filter["type"] == "Custom")
                    {
                        minDate = filter["minDate"];
                        maxDate = filter["maxDate"];
                    }
                    else
                    {
                        minDate = DateTime.MinValue.ToString("yyyy-MM-dd");
                        //minDate = DateTime.Now.AddDays(-12).ToString("yyyy-MM-dd");
                        maxDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                    }

                    timeSeries_minDate = DateTime.Now.AddDays(-12).ToString("yyyy-MM-dd");
                    timeSeries_maxDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");

                    filterchartType = filter["timeSeriesType"];
                    if (filter["compareType"] != "" && filter["compareType"] != null)
                    {
                        if (filter["compareType"] == "Yesterday")
                        {
                            string md = DateTime.Now.ToString("yyyy-MM-dd");
                            string maxd = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                            DateTime parsedDate = DateTime.Parse(md);
                            DateTime parsedDate1 = DateTime.Parse(maxd);
                            minCompare = parsedDate.AddDays(1).ToString("yyyy-MM-dd");
                            maxCompare = parsedDate.AddDays(2).ToString("yyyy-MM-dd");
                        }
                        if (filter["compareType"] == "Last Week")
                        {
                            string md = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                            string maxd = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                            DateTime parsedDate = DateTime.Parse(md);
                            DateTime parsedDate1 = DateTime.Parse(maxd);
                            minCompare = parsedDate.AddDays(-7).ToString("yyyy-MM-dd");
                            maxCompare = parsedDate.AddDays(1).ToString("yyyy-MM-dd");
                        }
                        if (filter["compareType"] == "Last Month")
                        {
                            string md = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd");
                            string maxd = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                            DateTime parsedDate = DateTime.Parse(md);
                            DateTime parsedDate1 = DateTime.Parse(maxd);
                            minCompare = parsedDate.AddMonths(-1).ToString("yyyy-MM-dd");
                            maxCompare = parsedDate.AddDays(1).ToString("yyyy-MM-dd");
                        }
                        if (filter["compareType"] == "Last Year")
                        {
                            string md = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
                            string maxd = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                            DateTime parsedDate = DateTime.Parse(md);
                            DateTime parsedDate1 = DateTime.Parse(maxd);
                            minCompare = parsedDate.AddYears(-1).ToString("yyyy-MM-dd");
                            maxCompare = parsedDate.AddDays(1).ToString("yyyy-MM-dd");
                        }
                        if (filter["compareType"] == "Custom")
                        {
                            minCompare = filter["minCompare"];
                            maxCompare = filter["maxCompare"];
                        }
                    }
                }
                foreach (KPIConnection scd in KPIdbConnectionList)
                {
                    foreach (KPI kpi in KPIList)
                    {

                        KPI k = new KPI();
                        k.data1 = "";
                        if (scd.Name == "MySQL" && scd.ID == kpi.DbConnection_Id && scd.ID1 == kpi.ID)
                        {
                            MysqlConnect c = new MysqlConnect(scd.Server, scd.Port, scd.Database, scd.Username, scd.Password);
                            kpi.SQL1 = kpi.SQL;
                            kpi.SQL = kpi.SQL.Replace("#minDate#", "'" + minDate + "'");
                            kpi.SQL = kpi.SQL.Replace("#maxDate#", "'" + maxDate + "'");
                            kpi.SQL = kpi.SQL.Replace("#timeseriestype#", "'" + filterchartType + "'");
                            k.data = c.executeSQL(kpi.SQL);
                            flag = 1;
                        }
                        else if (scd.Name == "SQL Server" && scd.ID == kpi.DbConnection_Id && scd.ID1 == kpi.ID)
                        {
                            MysqlConnect c = new MysqlConnect(scd.Server, scd.Port, scd.Database, scd.Username, scd.Password);
                            kpi.SQL1 = kpi.SQL;
                            kpi.SQL = kpi.SQL.Replace("#minDate#", "'" + minDate + "'");
                            kpi.SQL = kpi.SQL.Replace("#maxDate#", "'" + maxDate + "'");
                            kpi.SQL = kpi.SQL.Replace("#timeseriestype#", "'" + filterchartType + "'");
                            k.data = c.executeSQL(kpi.SQL);
                            flag = 1;
                        }
                        else if (scd.Name == "PostgreSQL" && scd.ID == kpi.DbConnection_Id && scd.ID1 == kpi.ID)
                        {
                            Connection c = new Connection(scd.Server, scd.Port, scd.Database, scd.Username, scd.Password);
                            kpi.SQL1 = kpi.SQL;
                            if (kpi.Chart_Type.ToLower() == "timeseries")
                            {
                                kpi.SQL = kpi.SQL.Replace("#minDate#", "'" + timeSeries_minDate + "'");
                                kpi.SQL = kpi.SQL.Replace("#maxDate#", "'" + timeSeries_maxDate + "'");
                            }
                            else
                            {
                                kpi.SQL = kpi.SQL.Replace("#minDate#", "'" + minDate + "'");
                                kpi.SQL = kpi.SQL.Replace("#maxDate#", "'" + maxDate + "'");
                            }
                            kpi.SQL = kpi.SQL.Replace("#timeseriestype#", "'" + filterchartType + "'");

                            if (compareCheck == "true" && kpi.Chart_Type.ToLower() == "timeseries")
                            {
                                kpi.SQL1 = kpi.SQL1.Replace("#minDate#", "'" + minCompare + "'");
                                kpi.SQL1 = kpi.SQL1.Replace("#maxDate#", "'" + maxCompare + "'");
                                kpi.SQL1 = kpi.SQL1.Replace("#timeseriestype#", "'" + filterchartType + "'");
                                k.data1 = c.executeSQL(kpi.SQL1, "Select");
                            }
                            k.data = c.executeSQL(kpi.SQL, "Select");
                            flag = 1;
                        }
                        else flag = 0;

                        if (flag == 1)
                        {

                            k.ID = kpi.Chart_Type + "_" + kpi.ID;
                            k.KPI_ID = kpi.ID;
                            k.PortalName = kpi.PortalName;
                            k.Chart_Type = kpi.Chart_Type;
                            k.Height = kpi.Height;
                            k.Link = kpi.Link;
                            k.Title = kpi.Title;

                            k.Width = kpi.Width;
                            k.Row_Number = kpi.Row_Number;
                            k.Column_Number = kpi.Column_Number;
                            result.Add(k);
                            i++;
                        }
                    }
                }
                string resultdata = JsonConvert.SerializeObject(result);
                return resultdata;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public async Task<string> getKPI(int roleid, string monitor, string filters, string KPIid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Monitor m = new Monitor();
                string feedback = m.KPIScreen(roleid, monitor, "NKPI", KPIid);
                List<NKPI> KPIList = JsonConvert.DeserializeObject<List<NKPI>>(feedback);
                string KPIdbConnections = m.KPIdbConnection(roleid, "NKPI");
                List<KPIConnection> KPIdbConnectionList = JsonConvert.DeserializeObject<List<KPIConnection>>(KPIdbConnections);
                int i = 0;
                int flag = 0;
                List<NKPI> result = new List<NKPI>();
                string minDate = "";
                string maxDate = "";
                if (filters != null)
                {
                    Dictionary<string, string> filter = JsonConvert.DeserializeObject<Dictionary<string, string>>(filters);

                    if (filter["type"] == "Today")
                    {
                        minDate = DateTime.Now.ToString("yyyy-MM-dd");
                        maxDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                    }
                    else if (filter["type"] == "Last Hour")
                    {

                        minDate = DateTime.Now.AddHours(-1).ToString("yyyy-MM-dd HH:mm:ss");
                        maxDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    }
                    else if (filter["type"] == "Yesterday")
                    {
                        minDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                        maxDate = DateTime.Now.ToString("yyyy-MM-dd");
                    }
                    else if (filter["type"] == "This Week")
                    {
                        minDate = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                        maxDate = DateTime.Now.ToString("yyyy-MM-dd");
                    }
                    else if (filter["type"] == "This Month")
                    {
                        minDate = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd");
                        maxDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                    }
                    else if (filter["type"] == "This Year")
                    {
                        minDate = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
                        maxDate = DateTime.Now.ToString("yyyy-MM-dd");
                    }
                    else if (filter["type"] == "Custom")
                    {
                        minDate = filter["minDate"];
                        maxDate = filter["maxDate"];
                    }
                    else
                    {
                        minDate = DateTime.MinValue.ToString("yyyy-MM-dd");
                        maxDate = DateTime.Now.AddDays(+1).ToString("yyyy-MM-dd");
                    }
                }
                foreach (KPIConnection scd in KPIdbConnectionList)
                {
                    foreach (NKPI nkpi in KPIList)
                    {
                        NKPI k = new NKPI();

                        if (scd.Name == "MySQL" && scd.ID == nkpi.DbConnection_Id && scd.ID1 == nkpi.ID)
                        {
                            MysqlConnect c = new MysqlConnect(scd.Server, scd.Port, scd.Database, scd.Username, scd.Password);
                            nkpi.SQL = nkpi.SQL.Replace("#minDate#", "'" + minDate + "'");
                            nkpi.SQL = nkpi.SQL.Replace("#maxDate#", "'" + maxDate + "'");
                            k.data = c.executeSQL(nkpi.SQL);
                            flag = 1;
                        }
                        else if (scd.Name == "SQL Server" && scd.ID == nkpi.DbConnection_Id && scd.ID1 == nkpi.ID)
                        {
                            MysqlConnect c = new MysqlConnect(scd.Server, scd.Port, scd.Database, scd.Username, scd.Password);
                            nkpi.SQL = nkpi.SQL.Replace("#minDate#", "'" + minDate + "'");
                            nkpi.SQL = nkpi.SQL.Replace("#maxDate#", "'" + maxDate + "'");
                            k.data = c.executeSQL(nkpi.SQL);
                            flag = 1;
                        }
                        else if (scd.Name == "PostgreSQL" && scd.ID == nkpi.DbConnection_Id && scd.ID1 == nkpi.ID)
                        {
                            Connection c = new Connection(scd.Server, scd.Port, scd.Database, scd.Username, scd.Password);
                            nkpi.SQL = nkpi.SQL.Replace("#minDate#", "'" + minDate + "'");
                            nkpi.SQL = nkpi.SQL.Replace("#maxDate#", "'" + maxDate + "'");
                            k.data = c.executeSQL(nkpi.SQL, "Select");
                            flag = 1;
                        }
                        else flag = 0;

                        if (flag == 1)
                        {
                            k.ID = "NKPI_" + nkpi.ID;
                            k.Title = nkpi.Title;
                            k.Group = nkpi.Group;
                            result.Add(k);
                            i++;
                        }
                    }
                }
                string resultdata = JsonConvert.SerializeObject(result);
                return resultdata;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
            throw new NotImplementedException();
        }

        public async Task<string> getMenu(string tablename)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Menu c = new Menu();
                string result = c.getMenu(tablename);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public async Task<string> getTimeCharts(int roleid, string monitor, string filters, string chartId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string filterchartType = "";
                Monitor m = new Monitor();
                string feedback = m.KPIScreen1(roleid, monitor, "KPI", chartId);
                List<KPI> KPIList = JsonConvert.DeserializeObject<List<KPI>>(feedback);
                string KPIdbConnections = m.KPIdbConnection(roleid, "KPI");
                List<KPIConnection> KPIdbConnectionList = JsonConvert.DeserializeObject<List<KPIConnection>>(KPIdbConnections);
                int i = 0;
                int flag = 0;
                List<KPI> result = new List<KPI>();
                string minDate = "";
                string maxDate = "";
                string minCompare = "";
                string maxCompare = "";
                string compareCheck = "";
                if (filters != "" && filters != null)
                {
                    Dictionary<string, string> filter = JsonConvert.DeserializeObject<Dictionary<string, string>>(filters);
                    compareCheck = filter["compareCheck"];
                    //if (filter["type"] == "Today")
                    //{
                    //    minDate = DateTime.Now.ToString("yyyy-MM-dd");
                    //    maxDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                    //}
                    //else if (filter["type"] == "Last Hour")
                    //{
                    //    maxDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    //    minDate = DateTime.Now.AddHours(-1).ToString("yyyy-MM-dd HH:mm:ss");
                    //}
                    //else if (filter["type"] == "Yesterday")
                    //{
                    //    minDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                    //    maxDate = DateTime.Now.ToString("yyyy-MM-dd");
                    //}
                    //else if (filter["type"] == "This Week")
                    //{
                    //    minDate = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                    //    maxDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                    //}
                    //else if (filter["type"] == "This Month")
                    //{
                    //    minDate = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd");
                    //    maxDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                    //}
                    //else if (filter["type"] == "This Year")
                    //{
                    //    minDate = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
                    //    maxDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                    //}
                    //else if (filter["type"] == "Custom")
                    //{
                    //    minDate = filter["minDate"];
                    //    maxDate = filter["maxDate"];
                    //}
                    //else
                    //{
                      //  minDate = DateTime.MinValue.ToString("yyyy-MM-dd");
                        minDate = DateTime.Now.AddDays(-12).ToString("yyyy-MM-dd");
                        maxDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                    //}
                    filterchartType = filter["timeSeriesType"];
                    if (filter["compareType"] != "" && filter["compareType"] != null)
                    {
                        if (filter["compareType"] == "Yesterday")
                        {
                            string md = DateTime.Now.ToString("yyyy-MM-dd");
                            string maxd = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                            DateTime parsedDate = DateTime.Parse(md);
                            DateTime parsedDate1 = DateTime.Parse(maxd);
                            minCompare = parsedDate.AddDays(-1).ToString("yyyy-MM-dd");
                            maxCompare = parsedDate.AddDays(1).ToString("yyyy-MM-dd");
                        }
                        if (filter["compareType"] == "Last Week")
                        {
                            string md = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                            string maxd = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                            DateTime parsedDate = DateTime.Parse(md);
                            DateTime parsedDate1 = DateTime.Parse(maxd);
                            minCompare = parsedDate.AddDays(-7).ToString("yyyy-MM-dd");
                            maxCompare = parsedDate.AddDays(1).ToString("yyyy-MM-dd");
                        }
                        if (filter["compareType"] == "Last Month")
                        {
                            string md = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd");
                            string maxd = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                            DateTime parsedDate = DateTime.Parse(md);
                            DateTime parsedDate1 = DateTime.Parse(maxd);
                            minCompare = parsedDate.AddMonths(-1).ToString("yyyy-MM-dd");
                            maxCompare = parsedDate.AddDays(1).ToString("yyyy-MM-dd");
                        }
                        if (filter["compareType"] == "Last Year")
                        {
                            string md = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
                            string maxd = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                            DateTime parsedDate = DateTime.Parse(md);
                            DateTime parsedDate1 = DateTime.Parse(maxd);
                            minCompare = parsedDate.AddYears(-1).ToString("yyyy-MM-dd");
                            maxCompare = parsedDate.AddDays(1).ToString("yyyy-MM-dd");
                        }
                        if (filter["compareType"] == "Custom")
                        {
                            minCompare = filter["minCompare"];
                            maxCompare = filter["maxCompare"];

                        }
                    }
                }

                foreach (KPIConnection scd in KPIdbConnectionList)
                {
                    foreach (KPI kpi in KPIList)
                    {

                        KPI k = new KPI();

                        if (scd.Name == "MySQL" && scd.ID == kpi.DbConnection_Id && scd.ID1 == kpi.ID)
                        {

                            MysqlConnect c = new MysqlConnect(scd.Server, scd.Port, scd.Database, scd.Username, scd.Password);
                            kpi.SQL1 = kpi.SQL;

                            kpi.SQL = kpi.SQL.Replace("#minDate#", "'" + minDate + "'");
                            kpi.SQL = kpi.SQL.Replace("#maxDate#", "'" + maxDate + "'");
                            kpi.SQL = kpi.SQL.Replace("#timeseriestype#", "'" + filterchartType + "'");
                            k.data = c.executeSQL(kpi.SQL);
                            if (Convert.ToBoolean(compareCheck) == true)
                            {
                                kpi.SQL1 = kpi.SQL1.Replace("#minDate#", "'" + minCompare + "'");
                                kpi.SQL1 = kpi.SQL1.Replace("#maxDate#", "'" + maxCompare + "'");
                                k.data1 = c.executeSQL(kpi.SQL1);
                            }
                            flag = 1;
                        }
                        else if (scd.Name == "SQL Server" && scd.ID == kpi.DbConnection_Id && scd.ID1 == kpi.ID)
                        {
                            MysqlConnect c = new MysqlConnect(scd.Server, scd.Port, scd.Database, scd.Username, scd.Password);
                            kpi.SQL1 = kpi.SQL;

                            kpi.SQL = kpi.SQL.Replace("#minDate#", "'" + minDate + "'");
                            kpi.SQL = kpi.SQL.Replace("#maxDate#", "'" + maxDate + "'");
                            kpi.SQL = kpi.SQL.Replace("#timeseriestype#", "'" + filterchartType + "'");
                            k.data = c.executeSQL(kpi.SQL);
                            if (Convert.ToBoolean(compareCheck) == true)
                            {
                                kpi.SQL1 = kpi.SQL1.Replace("#minDate#", "'" + minCompare + "'");
                                kpi.SQL1 = kpi.SQL1.Replace("#maxDate#", "'" + maxCompare + "'");
                                k.data1 = c.executeSQL(kpi.SQL1);
                            }
                            flag = 1;

                        }
                        else if (scd.Name == "PostgreSQL" && scd.ID == kpi.DbConnection_Id && scd.ID1 == kpi.ID)
                        {
                            Connection c = new Connection(scd.Server, scd.Port, scd.Database, scd.Username, scd.Password);
                            kpi.SQL1 = kpi.SQL;


                            kpi.SQL = kpi.SQL.Replace("#minDate#", "'" + minDate + "'");
                            kpi.SQL = kpi.SQL.Replace("#maxDate#", "'" + maxDate + "'");
                            kpi.SQL = kpi.SQL.Replace("#timeseriestype#", "'" + filterchartType + "'");
                            k.data = c.executeSQL(kpi.SQL, "Select");

                            if (Convert.ToBoolean(compareCheck) == true)
                            {
                                kpi.SQL1 = kpi.SQL1.Replace("#minDate#", "'" + minCompare + "'");
                                kpi.SQL1 = kpi.SQL1.Replace("#maxDate#", "'" + maxCompare + "'");
                                kpi.SQL1 = kpi.SQL1.Replace("#timeseriestype#", "'" + filterchartType + "'");
                                k.data1 = c.executeSQL(kpi.SQL1, "Select");
                            }
                            flag = 1;
                        }
                        else flag = 0;

                        if (flag == 1)
                        {
                            k.ID = kpi.Chart_Type + "_" + kpi.ID;
                            k.KPI_ID = kpi.ID;
                            k.PortalName = kpi.PortalName;
                            k.Chart_Type = kpi.Chart_Type;
                            k.Height = kpi.Height;
                            k.Link = kpi.Link;
                            k.Title = kpi.Title;
                            k.Width = kpi.Width;
                            k.Row_Number = kpi.Row_Number;
                            k.Column_Number = kpi.Column_Number;
                            result.Add(k);
                            i++;
                        }
                    }
                }
                string resultdata = JsonConvert.SerializeObject(result);
                return resultdata;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public async Task<string> queryValidate(string DBInstance, string Query)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Monitor l = new Monitor();
                string result = l.QueryValidate(DBInstance, Query);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }

        }

        public async Task<string> filterChartsSeries()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Robot l = new Robot();
                string result = l.FilterChartsOfRobots();
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public async Task<string> filterCharts(string category)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Robot l = new Robot();
                string result = l.FilterChartsOfRobots(category);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }

        }
    }
}
