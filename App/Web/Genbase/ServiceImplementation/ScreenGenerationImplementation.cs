﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.IO;
using Genbase.BLL;
using Genbase.Classes;
using Genbase.BLL.ElementsClasses;
using Genbase.ServiceInterfaces;
using Genbase.DAL;
using Newtonsoft.Json;
using Genbase.classes;

namespace Genbase.ServiceImplementation
{
    public class ScreenGenerationImplementation : ScreenGenerationInterfaces
    {
        public string SetPage(object screenData)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(screenData);
                string json2 = JsonConvert.SerializeObject(screenData, Formatting.Indented);
                JObject objscreen = JObject.Parse(json2);
                string data1 = objscreen["data1"].ToString();
                string data2 = objscreen["data2"].ToString();
               // string data3 = objscreen["data3"].ToString();


                JObject Obj1 = JObject.Parse(data1);
                Crud c = new Crud();
                string SName = Obj1["SName"].ToString();
                string TName = Obj1["TName"].ToString();
                bool AInsert = Convert.ToBoolean(Obj1["AInsert"]);
                bool AEdit = Convert.ToBoolean(Obj1["AEdit"]);
                bool ADelete = Convert.ToBoolean(Obj1["ADelete"]);
                bool SChild = Convert.ToBoolean(Obj1["SChild"]);
                string DSort = Obj1["DSort"].ToString();
                string FilterClause = Obj1["FilterClause"].ToString();
                string Insights = Obj1["Insights"].ToString();
                string FormType = Obj1["formType"].ToString();
                int RCount;
                if (Obj1["RCount"].ToString() == "")
                    RCount = 10;
                else
                    RCount = Convert.ToInt32(Obj1["RCount"]);
                string Preferences = Obj1["Preferences"].ToString();
                string Fields = Obj1["Fields"].ToString();
                string r = Obj1["Roles"].ToString();
                string CreateBy = Obj1["CreateBy"].ToString();
                string UpdateBy = Obj1["UpdateBy"].ToString();

                r = r.Replace("[\r\n", "_");
                r = r.Replace(",\r\n", "_");
                r = r.Replace("\r\n]", "");
                r = r.Replace("[", "");
                r = r.Replace("]", "");
                r = r.Replace(" ", "");
                r = r.Replace("\"", "");
                string s = SName + r;
                s = s.Replace(" ", "_");
                int screenId = c.generateFiles(SName, TName, AInsert, AEdit, ADelete, SChild, DSort, FilterClause, RCount, Preferences, Fields, Insights, FormType);
                if (screenId != 0)
                {
                    cTable ct = new cTable();
                    string tresult = ct.insertElementTable(data2, screenId);
                    Menu cm = new Menu();
                    new Logger().LogException("ScreenGenerate 1: " + Obj1["SName"].ToString() + ",4,/Views/Layout.html?generatedHTML/" + s + ",far fa-life-ring" + "," + screenId + "," + CreateBy + "," + UpdateBy);
                    int m_id = cm.addMenu(Obj1["SName"].ToString(), 4, "/Views/Layout.html?generatedHTML/" + s, "far fa-life-ring", screenId, CreateBy, UpdateBy);
                    new Logger().LogException("ScreenGenerate 2: " + m_id.ToString());
                    Roles cr = new Roles();
                    string rresult = cr.addRoles(m_id, Obj1["Roles"].ToString(), CreateBy, UpdateBy);
                    new Logger().LogException("ScreenGenerate 3: " + rresult);
                    cHtml html = new cHtml();
                    //string htmlresult = html.generateHTML(s, data1, data2, data3);
                    string htmlresult = html.generateHTML(s, data1, data2);
                    new Logger().LogException("ScreenGenerate 4: " + htmlresult);
                    cScript cs = new cScript();
                    if (htmlresult == "true")
                    {
                        string jsresult = cs.generateJS(s, data1, data2);
                    }
                    return rresult;
                }
                else
                {
                    return StringHelper.ffalse;
                }
               
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string UpdatePage(object screenData)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(screenData);
                string json2 = JsonConvert.SerializeObject(screenData, Formatting.Indented);
                JObject objscreen = JObject.Parse(json2);
                string data1 = objscreen["data1"].ToString();
                string data2 = objscreen["data2"].ToString();
                //string data3 = objscreen["data3"].ToString();

                JObject Obj1 = JObject.Parse(data1);
                Crud c = new Crud();
                int SId = Convert.ToInt32(Obj1["SId"]);
                string SName = Obj1["SName"].ToString();
                string TName = Obj1["TName"].ToString();
                bool AInsert = Convert.ToBoolean(Obj1["AInsert"]);
                bool AEdit = Convert.ToBoolean(Obj1["AEdit"]);
                bool ADelete = Convert.ToBoolean(Obj1["ADelete"]);
                bool SChild = Convert.ToBoolean(Obj1["SChild"]);
                string DSort = Obj1["DSort"].ToString();
                string Insights = Obj1["Insights"].ToString();
                string formtype = Obj1["formType"].ToString();
                string FilterClause = Obj1["FilterClause"].ToString();
               

                int RCount;
                if (Obj1["RCount"].ToString() == "")
                    RCount = 10;
                else
                    RCount = Convert.ToInt32(Obj1["RCount"]);
                string Preferences = Obj1["Preferences"].ToString();
                string Fields = Obj1["Fields"].ToString();
                string r = Obj1["Roles"].ToString();
                r = r.Replace("[\r\n", "_");
                r = r.Replace(",\r\n", "_");
                r = r.Replace("\r\n]", "");
                r = r.Replace("[", "");
                r = r.Replace("]", "");
                r = r.Replace(" ", "");
                r = r.Replace("\"", "");
                string s = SName + r;
                s = s.Replace(" ", "_");
                string tempresult = c.updateFiles(SId, SName, TName, AInsert, AEdit, ADelete, SChild, DSort, FilterClause, RCount, Preferences, Fields, Insights, formtype);
                cTable ct = new cTable();
                string tresult = ct.updateElementTable(data2);
                cHtml html = new cHtml();
                string htmlresult = html.generateHTML(s, data1, data2);
                cScript cs = new cScript();
                if (htmlresult == "true")
                {
                    string jsresult = cs.generateJS(s, data1, data2);
                }
                Menu cm = new Menu();
                string menuResult = cm.updateMenu(Convert.ToInt32(Obj1["SId"]), Obj1["SName"].ToString(), "/Views/Layout.html?generatedHTML/" + s/*Obj1["SName"].ToString()*/, "far fa-life-ring");
                Roles cr = new Roles();
                string result = cr.updateRoles(Convert.ToInt32(menuResult), Obj1["Roles"].ToString());

                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string DropDownTables()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                cTable c = new cTable();
                string result = c.getTables();
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string EditView(string name)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                cTable c = new cTable();
                string result = c.getElementRows(name);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string getScreenDetails(int id)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Crud c = new Crud();
                var result = c.getScreen(id);
                result += "|";
                Roles cr = new Roles();
                result += cr.getRoles(id);
                result += "|";
                cTable ct = new cTable();
                result += ct.getScreenElements(id);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }

        }

        public string updateMenuDetails(string Source, int destID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                JObject source = JObject.Parse(Source);
                Menu m = new Menu();
                string result = m.updateMenuParentID(Convert.ToInt32(source["id"]), source["Name"].ToString(), destID, source["Path"].ToString(), source["Class"].ToString());
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string LoadRoles()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Connection c = new Connection();
                string result = new QueryUtil().getTableData("ROLE", "Status|Equals|true", "\"ID\",\"RoleName\"", 0, "public", "\"ID\"", "");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }

        }

        public string getMenu(string tablename)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Menu c = new Menu();
                string result = c.getMenu(tablename);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string deleteScreen(string tableName, string deleteId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Menu cM = new Menu();
                string result = cM.deleteMenuScreenId(tableName, deleteId);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string getWidgetRow(int elementId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                
                Widget w = new Widget();
                int temp = w.checkWidget(elementId);
                if (temp != 0)
                {
                    string result = w.getWidget(elementId);
                    return result;
                }
                else
                    return "Not Exist";
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string widget(string columnDetails)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                JObject editColumns = JObject.Parse(columnDetails);
                string result;
                Widget w = new Widget();
                int temp = w.checkWidget(Convert.ToInt32(editColumns["ElementId"]));
                if (Convert.ToBoolean(temp))
                {
                    result = w.updateWidget(columnDetails);
                }
                else
                {
                    result = w.addWidget(columnDetails);
                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
    }
}
