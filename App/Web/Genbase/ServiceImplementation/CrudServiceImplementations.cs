﻿using Genbase.BLL;
using Genbase.BLL.ElementsClasses;
using Genbase.classes;
using Genbase.Classes;
using Genbase.ServiceInterfaces;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExcelDataReader;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;
using System.IO;
using System.Data;
using System.Text;

namespace Genbase.ServiceImplementation
{
    public class CrudServiceImplementations : CrudServiceInterfaces
    {
        Login login = new  Login();
        Crud crud = new Crud();
        Robot robot = new Robot();
        Monitor monitor = new Monitor();
        VariableClass Gvar = new VariableClass();
        Excel Dtable = new Excel();
        public string Connective_info()
        {
            return crud.Connective_info();
        }
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CrudServiceImplementations(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public Renci.SshNet.ConnectionInfo CreateConnection(string ipname, int port, string username, string password)
        {
            Renci.SshNet.ConnectionInfo connInfo = new Renci.SshNet.ConnectionInfo(ipname, port, username, new AuthenticationMethod[]
             {  new PasswordAuthenticationMethod(username, password),
                new PrivateKeyAuthenticationMethod(username, new PrivateKeyFile[]
                {
                    //new PrivateKeyFile(PrivateKeyFilePath, "passphrase")
                }),
             });
            try
            {
                return connInfo;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return connInfo;
            }
        }

        public string createTableForLob(string tablename, string Values)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string feedback = crud.addTableDataForLob(tablename, Values);
                if (feedback == "true")
                {
                    string v = crud.addTableData(tablename, Values);
                    return v;
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string createTableRow(string tablename, string Values)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string feedback = crud.addTableData(tablename, Values);
                return feedback;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string createMenuTableRow(string tablename, string Values)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Crud c = new Crud();
                string feedback = c.addMenuTableData(tablename, Values);

                return feedback;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }

        public string deleteTableRow(string tablename, string ID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string feedback = crud.deleteTableData(tablename, ID);
                return feedback;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        //MenuPermissionsScreen
        public string hardDeleteTableRow(string tablename, string roleid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Crud c = new Crud();
                string feedback = c.hardDeleteTableData(tablename, roleid);

                return feedback;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }

        public string ForgetPassword(string email)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = login.forgetPassword(email);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string generateSQLScripts(string Robots, string url)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                cSQLScripts cSql = new cSQLScripts();
                string result = cSql.cRobotScripts(Robots, url);

                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string genericScreenDropDown(int elementId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string temp = getWidgetRow(elementId);
                JArray obj = JArray.Parse(temp);
                string query = obj[0]["query"].ToString();
                Connection con = new Connection();
                string result = con.executeSQL(query, "Select");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public DataTables getConfigurationGridData(string tablename, string type, PagingParams data)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Crud c = new Crud();
                DataTables result = new DataTables();
                try
                {
                    //string search = _httpContextAccessor.HttpContext.Request.Query["search[value]"];
                    //string draw = _httpContextAccessor.HttpContext.Request.Query["draw"];
                    //string order = _httpContextAccessor.HttpContext.Request.Query["order[0][column]"];
                    //string orderDir = _httpContextAccessor.HttpContext.Request.Query["order[0][dir]"];
                    //int PageNo = Convert.ToInt32(_httpContextAccessor.HttpContext.Request.Query["start"]);
                   // int pageSize = Convert.ToInt32(_httpContextAccessor.HttpContext.Request.Query["length"]);

                    string search = data.search.value;
                    int draw = data.draw;
                    int order = data.order[0].column;
                    //string orderDir = data.order.
                    int PageNo = data.start;
                    int pageSize = data.length;

                    string dt = string.Empty;
                    if (tablename == StringHelper.CommandCenterNKPI)
                    {
                        string clickedNKPI = type;
                        dt = c.CommandCenterNKPIclick(clickedNKPI);
                    }
                    else if (tablename == StringHelper.CommandCenterSystemNKPI)
                    {
                        string clickedNKPI = type;
                        dt = c.CommandCenterSystemNKPIclick(clickedNKPI);
                    }
                    else
                    {
                        dt = c.getGridData(tablename);
                    }


                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    rows = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(dt);

                    List<Dictionary<string, object>> dataa = new List<Dictionary<string, object>>();
                    dataa = rows;

                    if ((tablename != StringHelper.CommandCenterNKPI) && (tablename != StringHelper.CommandCenterSystemNKPI))
                    {

                        List<Dictionary<string, object>> type_data_list = new List<Dictionary<string, object>>();
                        foreach (Dictionary<string, object> ld in dataa)
                        {
                            foreach (var vd in ld)
                            {
                                if (vd.Value != null & type != null)
                                {
                                    if (vd.Key == StringHelper.Type && vd.Value.ToString().ToLower() == type.ToLower())
                                    {
                                        type_data_list.Add(ld);
                                        break;
                                    }
                                }
                            }
                        }

                        dataa = type_data_list;

                    }

                    // Total record count.    
                    int totalRecords = dataa.Count;
                    //Verification. 

                    if (!string.IsNullOrEmpty(search) && !string.IsNullOrWhiteSpace(search))
                    {
                        //Apply search  

                        List<Dictionary<string, object>> search_data_list = new List<Dictionary<string, object>>();
                        foreach (Dictionary<string, object> ld in dataa)
                        {
                            foreach (var vd in ld)
                            {
                                if (vd.Value != null)
                                {
                                    if (vd.Value.ToString().ToLower().Contains(search.ToLower()))
                                    {

                                        search_data_list.Add(ld);
                                        break;
                                    }
                                }
                            }
                        }

                        dataa = search_data_list;


                    }

                    int recFilter = dataa.Count;
                    // Applypagination.    
                    dataa = dataa.Skip(PageNo).Take(pageSize).ToList();
                    //Loading drop down lists.    
                    result.draw = Convert.ToInt32(draw);
                    //result.draw = draw;
                    result.recordsTotal = totalRecords;
                    result.recordsFiltered = recFilter;
                    result.data = JsonConvert.SerializeObject(dataa);
                }
                catch (Exception ex)
                {
                    string exception = ex.Message;
                }
                //Return info.    
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return null;
            }
        }

        public DataTables getConnectionGridData(string tablename, string type, PagingParams data)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Crud c = new Crud();
                DataTables result = new DataTables();
                try
                {
                    string search = data.search.value;
                    int draw = data.draw;
                    int order = data.order[0].column;
                    //string orderDir = data.order.
                    int PageNo = data.start;
                    int pageSize = data.length;

                    string dt = string.Empty;
                    if (tablename == "ConnectionConfig")
                    {
                        dt = c.ConnectionGridData(tablename, type, data);
                    }
                    else
                    {
                        dt = c.getGridData(tablename);
                    }


                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    rows = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(dt);

                    List<Dictionary<string, object>> dataa = new List<Dictionary<string, object>>();
                    dataa = rows;

                    if (tablename == "ConnectionConfig")
                    {

                        List<Dictionary<string, object>> type_data_list = new List<Dictionary<string, object>>();
                        foreach (Dictionary<string, object> ld in dataa)
                        {
                            foreach (var vd in ld)
                            {
                                if (vd.Value != null & type != null)
                                {
                                    //if (vd.Key == StringHelper.Type && vd.Value.ToString().ToLower() == type.ToLower())
                                    //{
                                        type_data_list.Add(ld);
                                        break;
                                    //}
                                }
                            }
                        }

                        dataa = type_data_list;

                    }

                    // Total record count.    
                    int totalRecords = dataa.Count;
                    //Verification. 

                    if (!string.IsNullOrEmpty(search) && !string.IsNullOrWhiteSpace(search))
                    {
                        //Apply search  

                        List<Dictionary<string, object>> search_data_list = new List<Dictionary<string, object>>();
                        foreach (Dictionary<string, object> ld in dataa)
                        {
                            foreach (var vd in ld)
                            {
                                if (vd.Value != null)
                                {
                                    if (vd.Value.ToString().ToLower().Contains(search.ToLower()))
                                    {

                                        search_data_list.Add(ld);
                                        break;
                                    }
                                }
                            }
                        }

                        dataa = search_data_list;


                    }

                    int recFilter = dataa.Count;
                    // Applypagination.    
                    dataa = dataa.Skip(PageNo).Take(pageSize).ToList();
                    //Loading drop down lists.    
                    result.draw = Convert.ToInt32(draw);
                    //result.draw = draw;
                    result.recordsTotal = totalRecords;
                    result.recordsFiltered = recFilter;
                    result.data = JsonConvert.SerializeObject(dataa);
                }
                catch (Exception ex)
                {
                    string exception = ex.Message;
                }
                //Return info.    
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return null;
            }
        }

        public DataTables getGridData(string tablename, PagingParams data)
        {
            try
            {
                Crud c = new Crud();

                //Initialization.    
                DataTables result = new DataTables();
                try
                {
                    //Initialization.    
                 //   string search = _httpContextAccessor.HttpContext.Request.Query["search[value]"];
                 //   string draw = _httpContextAccessor.HttpContext.Request.Query["draw"];
                 //   string order = _httpContextAccessor.HttpContext.Request.Query["order[0][column]"];
                 //   string orderDir = _httpContextAccessor.HttpContext.Request.Query["order[0][dir]"];
                 //   int PageNo = Convert.ToInt32(_httpContextAccessor.HttpContext.Request.Query["start"]);
                 //   int pageSize = Convert.ToInt32(_httpContextAccessor.HttpContext.Request.Query["length"]);

                    string search = data.search.value;
                    int draw = data.draw;
                    int order = data.order[0].column;
                    //string orderDir = data.order.
                    int PageNo = data.start;
                    int pageSize = data.length;

                    //Loading.
                    string dt = string.Empty;
                    dt = c.getTableData(tablename);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    rows = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(dt);

                    List<Dictionary<string, object>> dataa = new List<Dictionary<string, object>>();
                    dataa = rows;

                    // Total record count.    
                    int totalRecords = dataa.Count;

                    //Verification.
                    if (!string.IsNullOrEmpty(search) && !string.IsNullOrWhiteSpace(search))
                    {
                        //Apply search  

                        List<Dictionary<string, object>> search_data_list = new List<Dictionary<string, object>>();
                        foreach (Dictionary<string, object> ld in dataa)
                        {
                            foreach (var vd in ld)
                            {
                                if (vd.Value != null)
                                {
                                    if (vd.Value.ToString().ToLower().Contains(search.ToLower()))
                                    {

                                        search_data_list.Add(ld);
                                        break;
                                    }
                                }
                            }
                        }

                        dataa = search_data_list;


                    }

                    //Sorting.  

                    // data = BAL.SortByColumnWithOrder(order, orderDir, data);

                    //Filter record count.    
                    int recFilter = dataa.Count;
                    // Applypagination.    
                    dataa = dataa.Skip(PageNo).Take(pageSize).ToList();
                    //Loading drop down lists.    
                    //result.draw = Convert.ToInt32(draw);
                    result.draw = draw;
                    result.recordsTotal = totalRecords;
                    result.recordsFiltered = recFilter;
                    result.data = JsonConvert.SerializeObject(dataa);
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //result.data = serializer.Serialize(data);
                }
                catch (Exception ex)
                {
                    string exception = ex.Message;
                }
                //Return info.    
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return null;
            }
        }

        public DataTables getGridDataByUser(string tablename, string CreateBy, PagingParams data)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Crud c = new Crud();
                DataTables result = new DataTables();
                try
                {
                    //string search = "";
                    //string draw = "";
                    //string order = "";
                    //string orderDir = "";
                    //int PageNo = Convert.ToInt32(2);
                    //int pageSize = Convert.ToInt32(20);
                    //string search = _httpContextAccessor.HttpContext.Request.Query["search[value]"];
                    //string draw = _httpContextAccessor.HttpContext.Request.Query["draw"];
                    //string order = _httpContextAccessor.HttpContext.Request.Query["order[0][column]"];
                    //string orderDir = _httpContextAccessor.HttpContext.Request.Query["order[0][dir]"];
                    //int PageNo = Convert.ToInt32(_httpContextAccessor.HttpContext.Request.Query["start"]);
                    //int pageSize = Convert.ToInt32(_httpContextAccessor.HttpContext.Request.Query["Length"]);

                    string search = data.search.value;
                    int draw = data.draw;
                    int order = data.order[0].column;
                    //string orderDir = data.order.
                    int PageNo = data.start;
                    int pageSize = data.length;

                    //Loading.
                    string dt = string.Empty;
                    dt = c.getTableData(tablename, CreateBy);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    rows = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(dt);

                    List<Dictionary<string, object>> dataa = new List<Dictionary<string, object>>();
                    dataa = rows;

                    // Total record count.    
                    int totalRecords = dataa.Count;

                    //Verification.
                    if (!string.IsNullOrEmpty(search) && !string.IsNullOrWhiteSpace(search))
                    {
                        //Apply search  

                        List<Dictionary<string, object>> search_data_list = new List<Dictionary<string, object>>();
                        foreach (Dictionary<string, object> ld in dataa)
                        {
                            foreach (var vd in ld)
                            {
                                if (vd.Value != null)
                                {
                                    if (vd.Value.ToString().ToLower().Contains(search.ToLower()))
                                    {

                                        search_data_list.Add(ld);
                                        break;
                                    }
                                }
                            }
                        }

                        dataa = search_data_list;


                    }

                    //Sorting.  

                    // data = BAL.SortByColumnWithOrder(order, orderDir, data);

                    //Filter record count.    
                    int recFilter = dataa.Count;
                    // Applypagination.    
                    dataa = dataa.Skip(PageNo).Take(pageSize).ToList();
                    //Loading drop down lists.    
                    //result.draw = Convert.ToInt32(draw);
                    result.draw = draw;
                    result.recordsTotal = totalRecords;
                    result.recordsFiltered = recFilter;
                    result.data = JsonConvert.SerializeObject(dataa);
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //result.data = serializer.Serialize(data);
                }
                catch (Exception ex)
                {
                    string exception = ex.Message;
                }
                //Return info.    
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return null;
            }
        }

        public DataTables getGridDataByUserCategory(string tablename, string CreateBy, PagingParams data, string Category)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Crud c = new Crud();
                DataTables result = new DataTables();
                try
                {
                    //string search = "";
                    //string draw = "";
                    //string order = "";
                    //string orderDir = "";
                    //int PageNo = Convert.ToInt32(2);
                    //int pageSize = Convert.ToInt32(20);
                    //string search = _httpContextAccessor.HttpContext.Request.Query["search[value]"];
                    //string draw = _httpContextAccessor.HttpContext.Request.Query["draw"];
                    //string order = _httpContextAccessor.HttpContext.Request.Query["order[0][column]"];
                    //string orderDir = _httpContextAccessor.HttpContext.Request.Query["order[0][dir]"];
                    //int PageNo = Convert.ToInt32(_httpContextAccessor.HttpContext.Request.Query["start"]);
                    //int pageSize = Convert.ToInt32(_httpContextAccessor.HttpContext.Request.Query["Length"]);

                    string search = data.search.value;
                    int draw = data.draw;
                    int order = data.order[0].column;
                    //string orderDir = data.order.
                    int PageNo = data.start;
                    int pageSize = data.length;

                    //Loading.
                    string dt = string.Empty;
                    dt = c.getTableData(tablename, CreateBy, Category);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    rows = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(dt);

                    List<Dictionary<string, object>> dataa = new List<Dictionary<string, object>>();
                    dataa = rows;

                    // Total record count.    
                    int totalRecords = dataa.Count;

                    //Verification.
                    if (!string.IsNullOrEmpty(search) && !string.IsNullOrWhiteSpace(search))
                    {
                        //Apply search  

                        List<Dictionary<string, object>> search_data_list = new List<Dictionary<string, object>>();
                        foreach (Dictionary<string, object> ld in dataa)
                        {
                            foreach (var vd in ld)
                            {
                                if (vd.Value != null)
                                {
                                    if (vd.Value.ToString().ToLower().Contains(search.ToLower()))
                                    {

                                        search_data_list.Add(ld);
                                        break;
                                    }
                                }
                            }
                        }

                        dataa = search_data_list;


                    }

                    //Sorting.  

                    // data = BAL.SortByColumnWithOrder(order, orderDir, data);

                    //Filter record count.    
                    int recFilter = dataa.Count;
                    // Applypagination.    
                    dataa = dataa.Skip(PageNo).Take(pageSize).ToList();
                    //Loading drop down lists.    
                    //result.draw = Convert.ToInt32(draw);
                    result.draw = draw;
                    result.recordsTotal = totalRecords;
                    result.recordsFiltered = recFilter;
                    result.data = JsonConvert.SerializeObject(dataa);
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //result.data = serializer.Serialize(data);
                }
                catch (Exception ex)
                {
                    string exception = ex.Message;
                }
                //Return info.    
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return null;
            }
        }

        public string GetHistory(string robotid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                ExecutionLog s = new ExecutionLog();
                string result = s.getFromRobotsLog(robotid);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        //Role_Menu_Relation
        public string getRoleMenu(int roleID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Menu c = new Menu();
                string result = c.getMenuRoleId(roleID);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }

        public string getMenuRoleId(int RoleId, string LOB)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Menu c = new Menu();
                string result = c.getMenuRole(RoleId, LOB);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string GetRobotLog(string tablename, string roleid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = crud.GetRobotlog(tablename, roleid);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string GetRobotsByLOB()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = robot.FilterChartsOfRobots();
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }

        }

        public string getRobotsByLOB(string timestamp)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = "";
                string query;
                string mindate;
                string maxdate = "";
                if (timestamp == "today")
                {
                    mindate = DateTime.Today.Date.ToString();
                }
                else if (timestamp == "yesterday")
                {
                    mindate = DateTime.Now.AddDays(-1).Date.ToString();
                    maxdate = DateTime.Now.Date.ToString();
                }
                else if (timestamp == "currentweek")
                {
                    mindate = DateTime.Now.AddDays(-7).Date.ToString();
                }
                else if (timestamp == "currentmonth")
                {
                    mindate = DateTime.Now.AddMonths(-1).Date.ToString();
                }
                else if (timestamp == "currentyear")
                {
                    mindate = DateTime.Now.AddYears(-1).Date.ToString();
                }
                else
                {
                    mindate = new DateTime().Date.ToString();
                }
                Connection con = new Connection();
                if (maxdate == "")
                {
                    query = "select l.\"Name\" as category,count(*) as value from \"ROBOT\" r inner join \"PROJECT\" p on p.\"ID\" = r.\"Project_Id\" inner join \"LOB\" l on p.\"LOBId\" = l.\"ID\" and r.\"Status\"=true and p.\"Status\"=true and l.\"Status\"=true and \"CreateDatetime\">='" + mindate + "' group by l.\"Name\"";
                }
                else
                {
                    query = "select l.\"Name\" as category,count(*) as value from \"ROBOT\" r inner join \"PROJECT\" p on p.\"ID\" = r.\"Project_Id\" inner join \"LOB\" l on p.\"LOBId\" = l.\"ID\" and r.\"Status\"=true and p.\"Status\"=true and l.\"Status\"=true and \"CreateDatetime\">='" + mindate + "' and \"CreateDatetime\"<='" + maxdate + "' group by l.\"Name\"";
                }
                result = con.executeSQL(query, "Select");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string GetRobotTimelineLive(string lasttimestamp)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = new MyController().robotTimelineLive(lasttimestamp);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return null;
            }
        }

        public string GetScheduleDetails(string RobotId, string SystemId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = new MyController().scheduleDetails(RobotId, SystemId);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return null;
            }
        }

        public string getStepProperties(string stepId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = crud.getStepPropertiesDetails(stepId);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string GetSystemsByRobotsCount()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Connection c = new Connection();
                string query = "select count(sr.\"System_Id\") as \"RobotsAssigned\",s.* from \"Systems\" s left join \"SystemsRobots\" sr on s.\"ID\"=sr.\"System_Id\" and sr.\"Status\"=true where s.\"Status\"=true group by s.\"ID\"";
                string result = c.executeSQL(query, "Select");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string gettablebyId(string tablename, string id)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = crud.tableDataByVersionId(tablename, id);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string getTableData(string tablename)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string UserDetails = crud.getTableData(tablename);
                return UserDetails;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string getTableDataByUserID(string tablename, string CreateBy)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string UserDetails = crud.getTableDataByUserID(tablename, CreateBy);
                return UserDetails;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string GetVersionsofRobot(string id)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = crud.GetVersionsofRobot(id);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string getWidgetRow(int elementId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Widget w = new Widget();
                int temp = w.checkWidget(elementId);
                if (temp != 0)
                {
                    string result = w.getWidget(elementId);
                    return result;
                }
                else
                    return "Not Exist";
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string loadUsernames()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                User c = new User();
                string result = c.getUser();
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string  LoginAuthenticationEX(string user)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                // Login // Login = new // Login();
                string result =  login.Authentication(user);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string Monitor(string ScreenName, string Role, string CreateBy, string UpdateBy)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = monitor.cMonitor(ScreenName, Role, CreateBy, UpdateBy);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string PreDefinedRobot(string projectid, string robotname, string predefinedbotid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = robot.cPredefinedBot(projectid, robotname, predefinedbotid);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string robotSchedule(string ScheduleDetails)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                MyController cM = new MyController();
                string result = cM.Schedule(ScheduleDetails);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string stopRobotSchedule(string ScheduleDetails)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                MyController cM = new MyController();
                string result = cM.stopSchedule(ScheduleDetails);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string UpdateRobotStatus(string BotID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = crud.updateRobots(BotID);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string UpdateTableRow(string tablename, string Values, string ID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string feedback = crud.UpdateTableData(tablename, Values, ID);
                if (feedback == "true")
                {

                    string l = crud.UpdateTableData(tablename, Values, ID);
                    return l;
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string UpdateTableRowforLob(string tablename, string Values, string ID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string feedback = crud.TableDataupdateforLob(tablename, Values, ID);
                if (feedback == "true")
                {

                    string l = crud.UpdateTableData(tablename, Values, ID);
                    return l;
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string uploadSQLScripts(object importRobotData)
        {
            new Logger().LogException("uploadSQLScripts"); // alpha code testing
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                new Logger().LogException("i m in try uploadSQLScripts" + importRobotData);
                cSQLScripts cSql = new cSQLScripts();
                string json2 = JsonConvert.SerializeObject(importRobotData, Formatting.Indented);
                JObject objscreen = JObject.Parse(json2);
                string Robots = objscreen["Robots"].ToString();
                string Fileupload = objscreen["Fileupload"].ToString();
                new Logger().LogException("i m in try uploadSQLScripts" + Robots + " " + Fileupload);

                string result = cSql.UploadSQLScripts(Robots, Fileupload);

                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string UserRegistration(string UserDetails)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = login.userRegister(UserDetails);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string getDataByRTable(string tableDetails)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Crud c = new Crud();
                string result = c.getDataByRelationTable(tableDetails);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public DataTables GetDataByUser(string tablename, string CreateBy)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                DataTables result = new DataTables();
                try
                {
                    //string search = "";
                    //string draw = "";
                    //string order = "";
                    //string orderDir = "";
                    //int PageNo = Convert.ToInt32(2);
                    //int pageSize = Convert.ToInt32(20);
                    string search = _httpContextAccessor.HttpContext.Request.Query["search[value]"];
                    string draw = _httpContextAccessor.HttpContext.Request.Query["draw"];
                    string order = _httpContextAccessor.HttpContext.Request.Query["order[0][column]"];
                    string orderDir = _httpContextAccessor.HttpContext.Request.Query["order[0][dir]"];
                    int PageNo = Convert.ToInt32(_httpContextAccessor.HttpContext.Request.Query["start"]);
                    int pageSize = Convert.ToInt32(_httpContextAccessor.HttpContext.Request.Query["length"]);

                    string dt = string.Empty;
                    dt = crud.getTableData(tablename, CreateBy);

                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    rows = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(dt);

                    List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                    data = rows;


                    // Total record count.    
                    int totalRecords = data.Count;
                    //Verification.    

                    //Apply search 
                    if (!string.IsNullOrEmpty(search) && !string.IsNullOrWhiteSpace(search))
                    {

                        List<Dictionary<string, object>> search_data_list = new List<Dictionary<string, object>>();
                        foreach (Dictionary<string, object> ld in data)
                        {
                            foreach (var vd in ld)
                            {
                                if (vd.Value != null)
                                {
                                    if (vd.Value.ToString().ToLower().Contains(search.ToLower()))
                                    {

                                        search_data_list.Add(ld);
                                        break;
                                    }
                                }
                            }
                        }

                        data = search_data_list;
                    }

                    //Sorting.  

                    // data = BAL.SortByColumnWithOrder(order, orderDir, data);

                    //Filter record count.    
                    int recFilter = data.Count;
                    // Applypagination.    
                    //data = data.Skip(PageNo).Take(pageSize).ToList();
                    //Loading drop down lists.    
                    //result.draw = Convert.ToInt32(draw);
                    result.draw = 0;
                    result.recordsTotal = totalRecords;
                    result.recordsFiltered = recFilter;
                    result.data = JsonConvert.SerializeObject(data);
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //result.data = serializer.Serialize(data);
                }
                catch (Exception ex)
                {
                    string exception = ex.Message;
                }
                //Return info.    
                return result;

                // return c.getGridData(tablename);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return null;
            }
        }

        public string getapp_tablesData()
        {

            Crud c = new Crud();
            string result = c.getapp_tablesData();
            return result;
        }

        public string deleteTables(string table_id)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                TableManager objTM = new TableManager();
                return objTM.deleteTables(table_id);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string UpdateNKPIPath(string winlocation, string path, int id)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Crud c = new Crud();
                string doubleQuotedPath = string.Format(@"""{0}""", path);
                path = winlocation + "+" + doubleQuotedPath + ",0)"; //layoutRefresh(window.location.origin + "/Views/Layout.html?generatedHTML/Robot_1_2_3_4", 0)
                string result = c.UpdateNKPIPath(path, id);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string CreateVariables(string rid, string variable)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                JObject objscreen = JObject.Parse(variable);
                DVariables dv = new DVariables();
                dv.vlname = objscreen["vlname"].ToString();
                dv.vlvalue = objscreen["vlvalue"].ToString();
                dv.vltype = objscreen["vltype"].ToString();
                dv.vlstatus = objscreen["vlstatus"].ToString();
                dv.sheetname = objscreen["sheetname"].ToString();
                StringBuilder sb = new StringBuilder();
                if (!string.IsNullOrEmpty(dv.sheetname))
                {
                    DataTable dataTable = Dtable.ReadAsDataTable(objscreen["vlvalue"].ToString(),true, dv.sheetname);
                    List<DataRow> list = dataTable.AsEnumerable().ToList();
                    foreach(var mylist in list)
                    {
                        var x =mylist.ItemArray;
                        foreach (string item in x)
                        {
                            sb.Append(item.ToString());
                        }
                        sb.AppendLine();
                    }
                    dv.vlvalue = sb.ToString();
                    string feedback = Gvar.setVariables(dv, rid);
                    return feedback;
                }
                else
                {
                    string feedback = Gvar.setVariables(dv, rid);
                    return feedback;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string UpdateVariables(string rid, string variable)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                JObject objscreen = JObject.Parse(variable);
                //string data1 = objscreen["vlvalue"].ToString();
                DVariables dv = new DVariables();
                dv.vlname = objscreen["vlname"].ToString();
                dv.vlvalue = objscreen["vlvalue"].ToString();
                dv.vltype = objscreen["vltype"].ToString();
                dv.vlstatus = objscreen["vlstatus"].ToString();
                dv.sheetname = objscreen["sheetname"].ToString();
                string feedback = Gvar.updateVariables(dv,rid);
                return feedback;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string Deletevariables(string rid, string variable)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                JObject objscreen = JObject.Parse(variable);
                //string data1 = objscreen["vlvalue"].ToString();
                DVariables dv = new DVariables();
                dv.vlname = objscreen["vlname"].ToString();
                dv.vlvalue = objscreen["vlvalue"].ToString();
                dv.vltype = objscreen["vltype"].ToString();
                dv.vlstatus = objscreen["vlstatus"].ToString();
                dv.sheetname = objscreen["sheetname"].ToString();
                string feedback = Gvar.DeleteVariables(dv, rid);
                return feedback;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string getVariables(string rid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = Gvar.getVariables(rid);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string getFilterTableData(string tablename, string Category)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string UserDetails = crud.getFilterTableData(tablename, Category);
                return UserDetails;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string connectionHealthCheck(string tablename)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            try
            {
                string dt = string.Empty;
                string result_data = string.Empty;
                dt = crud.getTableData(tablename);

                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                rows = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(dt);

                List<Dictionary<string, object>> dataa = new List<Dictionary<string, object>>();
                dataa = rows;
                List<ConnectionStatus> list = new List<ConnectionStatus>();

                foreach (Dictionary<string, object> ld in dataa)
                {
                    string Server = string.Empty;
                    string Port = string.Empty;
                    string UserName = string.Empty;
                    string Password = string.Empty;
                    string Auth_type = string.Empty;
                    string conn_ID = string.Empty;
                    string conn_type = string.Empty;
                    string conn_string = string.Empty;
                    string conn_name = string.Empty;
                    conn_ID = ld["ID"].ToString();
                    Server = ld["hostname"].ToString();
                    Port = ld["port"].ToString();
                    UserName = ld["username"].ToString();
                    Password = ld["password"].ToString();
                    conn_type = ld["connectiontype"].ToString();
                    conn_name = ld["connectionname"].ToString();
                    if (conn_type != null && ld["authenticationtype"] != null) {
                        Auth_type = ld["authenticationtype"].ToString();
                    }

                    if (conn_type == "Database") {
                        if (Auth_type == "postgres")
                        {
                            conn_string = "Server=" + Server + ";Port=" + Port + ";User Id=" + UserName + ";Password=" + Password + ";Database=" + Auth_type + ";";
                        }
                        else if (Auth_type == "mysql")
                        {
                            conn_string = "SERVER=" + Server + ";DATABASE=" + Auth_type + ";" + "UID=" + UserName + ";" + "PASSWORD=" + Password + ";";
                        }
                        else
                        {
                            conn_string = Server;
                        }
                    }
                    else if (conn_type == "Server") {
                        conn_string = Server;
                    }
                    else if (conn_type == "API") {
                        if (Auth_type != null)
                        {
                            conn_string = "URL=" + Server + ";API_Type=" + Auth_type + ";";
                        }
                    }
                    else {
                        conn_string = Server;
                    }
                    string Conn_Status = string.Empty;

                    try
                    {
                        Conn_Status = crud.Connectivity_info(conn_string, conn_name, conn_type, conn_ID);
                    }
                    catch (Exception ex) {
                        Conn_Status = "Failure";
                    }
                    if (Conn_Status == "Success")
                    {                        
                        list.Add(new ConnectionStatus(conn_ID, "true"));
                    }
                    else {
                        list.Add(new ConnectionStatus(conn_ID, "false"));
                    }
                }
                result_data = JsonConvert.SerializeObject(list);
                return result_data;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string getConnectivity_Info(string tablename, string Name, string Server, int Conn_ID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string dt = string.Empty;
                dt = crud.getConnectivity_Info(tablename,Name,Server,Conn_ID);
                return dt;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }


    }
}
public class ConnectionStatus
{
    public string _conn_id { get; set; }
    public string _conn_status { get; set; }
    public ConnectionStatus(string a, string b)
    {
        _conn_id = a;
        _conn_status = b;
    }
}