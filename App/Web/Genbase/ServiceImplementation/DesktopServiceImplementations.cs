﻿using Genbase.BLL;
using Genbase.classes;
using Genbase.Classes;
using Genbase.ServiceInterfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static Genbase.classes.Save;
using static Genbase.Classes.Login;
using MethodResult = Genbase.BLL.MethodResult;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.ServiceImplementation
{
    public class DesktopServiceImplementations: DesktopServiceInterfaces
    {
        Roles roles = new Roles();
        Robot robot = new Robot();
        Project project = new Project();
        Workflow workflow = new Workflow();
        Step step = new Step();
        Classes.Action ca = new Classes.Action();
        cElement element = new cElement();
        Entities entities = new Entities();
        DAL.PostgresConnect dbd = new DAL.PostgresConnect();
        LinkNode linkNode = new LinkNode();
        cSystems_Robots systems_robots = new cSystems_Robots();
        Login login = new Login();
        ExecutionLog executionLog = new ExecutionLog();
        Predefined predefined = new Predefined();
        AppExecution appExecution = new AppExecution();
        SystemService systemService = new SystemService();
        ResponseResult responseResult = null;
        LOB lob = new LOB();
        VariableClass variableClass = new VariableClass();

        public string CreateProject(Entities inputString)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            responseResult = new ResponseResult();
            try
            {
               //Entities inputString = new Entities();
                DateTime? curTime = DateTime.Now;
                DateTime? prevTime = inputString.PreviousDateTime;
               // inputString = entityService(input);


                if (inputString.Type == StringHelper.createrobot)
                {
                    responseResult.ResultString = robot.createRobot(inputString.Type, inputString.Name, inputString.Parent, inputString.Description, inputString.CreateBy, inputString.UpdateBy);

                }
                else if (inputString.Type == StringHelper.createproject)
                {
                    responseResult.ResultString = project.createProject(inputString.Type, inputString.Name, inputString.projectType, inputString.LOBId, inputString.username, inputString.Description, inputString.CreateBy);

                }
                else if (inputString.Type == StringHelper.createworkflow)
                {
                    responseResult.ResultString = workflow.createWorkflow(inputString.Type, inputString.Name, inputString.Parent);

                }
                else if (inputString.Type == StringHelper.linknodes)
                {
                    responseResult.ResultString = linkNode.createNodes(inputString.Step_Id, inputString.Location);
                }
                else if (inputString.Type == StringHelper.createsystems)
                {
                    responseResult.ResultString = systemService.addSystem(inputString.MAC, inputString.IP, inputString.SystemName, inputString.Name, curTime, curTime, inputString.systemtype);
                }
                else if (inputString.Type == StringHelper.ProcessManager)
                {
                    responseResult.ResultString = new Robot().createProcessManager(inputString.SystemId, inputString.rid, inputString.SystemName, inputString.MAC);
                }
                else
                {
                    responseResult.ResultString = StringHelper.incorrect;
                }

                return JsonConvert.SerializeObject(responseResult);
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 201;
                responseResult.ExceptionStatus = true;
                responseResult.StatusMessage = ex.Message.ToString();
                responseResult.ResultString = string.Empty;
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return JsonConvert.SerializeObject(responseResult);
            }

        }

        public string Delete(Entities inputString)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            responseResult = new ResponseResult();
            try
            {
                //Entities inputString = new Entities();
               // inputString = entityService(input);

                if (inputString.Type == StringHelper.deleterobot)
                {
                    responseResult.ResultString = robot.deleteRobot(inputString.Name, inputString.Parent);
                }
                else if (inputString.Type == StringHelper.deleteproject)
                {
                    responseResult.ResultString = project.deleteProject(inputString.Name);
                }
                else if (inputString.Type == StringHelper.deleteworkflow)
                {
                    responseResult.ResultString = workflow.deleteWorkflow(inputString.Name);
                }
                else if (inputString.Type == StringHelper.deletelinknodes)
                {
                    responseResult.ResultString = linkNode.initNodes(inputString.Step_Id);
                }
                else if (inputString.Type == StringHelper.deletesteps)
                {
                    responseResult.ResultString = step.deleteSteps(inputString.rid);
                }
                else
                {
                    responseResult.ResultString = StringHelper.incorrect;
                }

                return JsonConvert.SerializeObject(responseResult);
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 201;
                responseResult.ExceptionStatus = true;
                responseResult.StatusMessage = ex.Message.ToString();
                responseResult.ResultString = string.Empty;
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return JsonConvert.SerializeObject(responseResult);
            }
        }

        public string Get(string input)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            responseResult = new ResponseResult();
            try
            {
                Entities inputString = new Entities();
                inputString = entityService(input);

                if (inputString.Type == StringHelper.getallactions)
                {
                    responseResult.ResultString = ca.getActions();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getaction)
                {
                    responseResult.ResultString = ca.getAction(inputString.ID);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getactionrow)
                {
                    responseResult.ResultString = ca.getActionRow(inputString.ID);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getcategoryrobotsteps)
                {
                    responseResult.ResultString = predefined.getCategoryRobotSteps(inputString.ID);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getallelements)
                {
                    responseResult.ResultString = element.getElements();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getallsteps)
                {
                    responseResult.ResultString = step.getSteps(inputString.Parent);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getallworkflows)
                {
                    responseResult.ResultString = workflow.getWorkflows(inputString.Parent);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getallrobots)
                {
                    responseResult.ResultString = robot.getRobots(inputString.Project_Id);
                   // if (responseResult.StatusCode == 200)
                     //   responseResult.ExceptionStatus = true;
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getrobotproperties)
                {
                    responseResult.ResultString = robot.getRobotProperties(inputString.Robot_ID, inputString.Version_Id);
                    return JsonConvert.SerializeObject(responseResult);
                }
                //getRobotProperties(string rbtid)
                else if (inputString.Type == StringHelper.getallprojects)
                {
                    //string QgetRole = "select \"RoleName\" from public.\"ROLE\" where \"ID\" in (select \"Role_ID\" from public.\"USER\" where \"UserName\"='" + inputString.username + "')";
                    //string getRole = dbd.DbExecuteQuery(QgetRole, "selectwithnodes");
                    string getRole = roles.getRole(inputString.username);
                    if (!(getRole.ToLower().IndexOf("admin") > -1))
                    {
                        responseResult.ResultString = project.getProjects(inputString.username);
                        return JsonConvert.SerializeObject(responseResult);
                    }
                    else
                    {
                        responseResult.ResultString = project.getProjects();
                        return JsonConvert.SerializeObject(responseResult);
                    }
                    //return cp.getProjects(inputString.username);
                }
                else if (inputString.Type == StringHelper.getallactionproperties)
                {
                    responseResult.ResultString = ca.getProperties();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getrobot)
                {
                    // cRobot cr = new cRobot();
                    responseResult.ResultString = robot.getRobot(inputString.Name);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getproject)
                {
                    //cProject cp = new cProject();
                    responseResult.ResultString = project.getProject(inputString.Name);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getworkflow)
                {
                    //workfloworkflow workflow = new workfloworkflow();
                    responseResult.ResultString = workflow.getWorkflow(inputString.Name);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getalltheworkflows)
                {
                    // workfloworkflow workflow = new workfloworkflow();
                    responseResult.ResultString = workflow.getAllWorkflows();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getalltherobots)
                {
                    // cRobot cr = new cRobot();
                    responseResult.ResultString = robot.getallRobots();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getstepproperties)
                {
                    // cStep sp = new cStep();
                    responseResult.ResultString = step.getStepProperties(inputString.Step_Id);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getallstepproperties)
                {
                    // cStep asp = new cStep();
                    responseResult.ResultString = step.getAllStepProperties();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.linknodes)
                {
                    responseResult.ResultString = linkNode.getNodes(inputString.Step_Id);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.linkallnodes)
                {
                    responseResult.ResultString = linkNode.getAllNodes(inputString.rid);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.alllobs)
                {
                    responseResult.ResultString = project.getAllLOBs();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.stepid)
                {
                    // cStep sp = new cStep();
                    responseResult.ResultString = step.getStepbyId(inputString.Step_Id);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.actionproperties)
                {
                    //cAction ca = new cAction();
                    responseResult.ResultString = ca.getActionProperties();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.allsystems)
                {
                    responseResult.ResultString = systemService.getAllSystemsRobotsList();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.allsysrobots)
                {
                    responseResult.ResultString = systems_robots.getAllSysRobots(inputString.ID);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.allrobsystems)
                {
                    responseResult.ResultString = systems_robots.getAllRobSystems(inputString.ID);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.lob)
                {
                    responseResult.ResultString = lob.getLOB();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.lobprojects)
                {
                    responseResult.ResultString = project.getLOBProjects(inputString.ID);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.categoryprojects)
                {
                    responseResult.ResultString = predefined.getCategoryProjects(inputString.ID);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.systemrobotscollection)
                {
                    responseResult.ResultString = systems_robots.getSystemRobotsCollection();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getrobotsstepsstepprops)
                {
                    responseResult.ResultString = robot.getRobotsStepsStepProps();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getprojectid)
                {
                    responseResult.ResultString = project.getProjectId(inputString.ID);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getlog)
                {
                    responseResult.ResultString = executionLog.getFromLogsbyTime(inputString.ID);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getrobotslog)
                {
                    responseResult.ResultString = executionLog.getFromRobotsLog(inputString.ID);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getsystemsrobotslist)
                {
                    responseResult.ResultString = systemService.getAllSystemsRobotsList();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.predefindedbotsbycategory)
                {
                    responseResult.ResultString = predefined.getPredefindedBotsByCategory();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.loginauthentication)
                {
                    try
                    {
                        UserResult logs = Newtonsoft.Json.JsonConvert.DeserializeObject<UserResult>(login.Authentication(inputString.user));

                        if (logs.User.Count > 0)
                        {
                            responseResult.ResultString = StringHelper.sucess;
                            responseResult.StatusMessage = JsonConvert.SerializeObject(logs);
                            return JsonConvert.SerializeObject(responseResult);
                        }
                        else
                        {
                            responseResult.ResultString = logs.Message;
                            return JsonConvert.SerializeObject(responseResult);
                        }
                    }
                    catch (Exception ex)
                    {
                        responseResult.StatusCode = 201;
                        responseResult.ExceptionStatus = true;
                        responseResult.StatusMessage = ex.Message.ToString();
                        responseResult.ResultString = StringHelper.operationfailed;
                        return JsonConvert.SerializeObject(responseResult);
                    }

                }
                else if (inputString.Type == StringHelper.getalllobs)
                {
                    responseResult.ResultString = project.getLOBProjectRobots();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getallrobotsystemlist)
                {
                    responseResult.ResultString = systemService.getAllRobotSystemList();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getallemailtemplates)
                {
                    responseResult.ResultString = new Mail().GetAllMailItems();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getallrobotversions)
                {
                    responseResult.ResultString = new Save().getVersion(inputString.ID);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getrobotsstepssteppropsversion)
                {
                    responseResult.ResultString = new Robot().getRobotsStepsStepProps(inputString.rid, inputString.wid);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getwarobotssteps)
                {
                    responseResult.ResultString = new DBWARobot().getWARobotI(inputString.rid);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getWAActionsActionProperties)
                {
                    responseResult.ResultString = new Classes.Action().getWAActionsActionProperties();
                    return JsonConvert.SerializeObject(responseResult);
                }
                //workflow lob,projects,robots
                else if (inputString.Type == StringHelper.getwflobprojectrobots)
                {
                    responseResult.ResultString = lob.getWFLOBProjectRobots();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.getwfconnection)
                {
                    responseResult.ResultString = lob.getWFConnection();
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.scheduler)
                {
                    responseResult.ResultString = systems_robots.getAllSchedulesforthisSystem(inputString.MAC);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else
                {
                    responseResult.ResultString = StringHelper.incorrect;
                    return JsonConvert.SerializeObject(responseResult);
                }

            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 201;
                responseResult.ExceptionStatus = true;
                responseResult.StatusMessage = ex.Message.ToString();
                responseResult.ResultString = string.Empty;
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return JsonConvert.SerializeObject(responseResult);
            }

        }

        public string Update(Entities inputString)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            responseResult = new ResponseResult();
            try
            {
                //Entities inputString = new Entities();
                //inputString = entityService(input);
                if (inputString.Type == StringHelper.updatesteps)
                {
                    // cStep cs = new cStep();
                    responseResult.ResultString = step.updateSteps(StringHelper.truee, inputString.ID, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.updatestepproperties)
                {
                    // cStep csp = new cStep();
                    responseResult.ResultString = step.updateStepProperties(inputString.Step_Id, inputString.ID, inputString.StepPropertyValue);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.updateworkflow)
                {
                    //workfloworkflow workflow = new workfloworkflow();
                    responseResult.ResultString = workflow.updateWorkflow(inputString.Name);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.ProcessManager)
                {
                    responseResult.ResultString = new Robot().updateProcessManager(inputString.systemId_pm, inputString.state);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.polling)
                {
                    string strConnString = Config.getconfig();
                    DateTime? curTime = DateTime.Now;
                    DateTime? prevTime = inputString.PreviousDateTime;
                    string returnValue = string.Empty; string diffTime = string.Empty;

                    string mac = inputString.MAC;
                    string ip = inputString.IP;
                    string systemName = inputString.SystemName;
                    string systemtype = inputString.systemtype;
                    // cSystem cs = new cSystem();
                    Polling polling = new Polling();
                    polling.Connection = strConnString;

                    polling.CurrentDateTime = DateTime.Now;
                    returnValue = JsonConvert.SerializeObject(polling);


                    if (prevTime.ToString() != string.Empty || prevTime != null)
                    {
                        systemService.updateSystemTime(mac, ip, systemName, prevTime, curTime);
                        //cs.addSystemStatus(mac, ip, systemName, prevTime, curTime);
                    }
                    else if (prevTime.ToString() == string.Empty || prevTime == null)
                    {
                        try
                        {
                            string resulkt = systemService.getSystemByMAC(mac, ip, systemName);
                            List<Polling> systems = JsonConvert.DeserializeObject<List<Polling>>(resulkt);
                            if (systems.Count > 0)
                            {
                                systemService.updateSystemTime(mac, ip, systemName, prevTime, curTime);
                            }
                            else
                            {
                                systemService.addSystem(mac, ip, systemName, polling.Name, prevTime, curTime, systemtype);
                            }
                        }
                        catch(Exception ex)
                        {
                            new Logger().LogException(ex);
                            systemService.addSystem(mac, ip, systemName, polling.Name, prevTime, curTime, systemtype);
                        }
                    }
                    else
                    {

                    }
                    responseResult.ResultString = returnValue;
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.updaterobot)
                {
                    responseResult.ResultString = robot.deleteRobot(inputString.ID, inputString.Parent);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else
                {
                    responseResult.ResultString = string.Empty;
                    return JsonConvert.SerializeObject(responseResult);
                }
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 201;
                responseResult.ExceptionStatus = true;
                responseResult.StatusMessage = ex.Message.ToString();
                responseResult.ResultString = string.Empty;
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return JsonConvert.SerializeObject(responseResult);
            }

        }

        public string Execute(Entities inputString)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            responseResult = new ResponseResult();
            try
            {
                Result rs = new Result();
                Variables.robotid = inputString.ID;
                if (inputString.dynamicvariables != null)
                {
                    Variables.dynamicvariables.Clear();
                    Variables.dynamicvariables.AddRange(inputString.dynamicvariables.ConvertAll(x => new DVariables() { vlname = x.vlname, vlvalue = x.vlvalue, vlstatus = x.vlstatus }));

                }
                if (inputString.uploadFiles != null && inputString.uploadFiles.Count > 0)
                {
                    Variables.uploads = inputString.uploadFiles;
                }

                if (inputString.Type == StringHelper.execution)
                {
                    string executionId = new StringFunction().getRandomString("32");

                    string projectid = inputString.Project_Id;
                    string robotid = inputString.ID;

                    new ExecutionLog().deleteFromLogs(robotid);

                    Variables.vDebugger.ID = inputString.Step_Id;

                    int ExecResult = appExecution.startRobot(int.Parse(robotid), 0, executionId);

                    string state = Variables.state.ToString();

                    rs.SuccessState = state;
                    if ((state.ToLower() != StringHelper.sucess) && (state.ToLower() != StringHelper.operationfailed) && (state.ToLower() != StringHelper.nosteps))
                    {
                        if (rs.SuccessState.ToLower().IndexOf("inprogress") > -1)
                        {
                            RuntimeUserInputRequest ruir = new RuntimeUserInputRequest();
                            ruir.dvariables = Variables.dynamicvariables;
                            ruir.nextStepID = new AppExecution().getNextStep(int.Parse(robotid), int.Parse(Variables.thisStep.Id), string.Empty).ToString();
                            ruir.step = Variables.thisStep;
                            ruir.thisVariableName = Variables.thisVariable;

                            rs.runtimerequest = ruir;
                            rs.elog = new List<ELogs>();
                        }
                        else
                        {
                            rs.elog = JsonConvert.DeserializeObject<List<ELogs>>(new ExecutionLog().getFromLogs(robotid, true));
                            rs.Downloads = DownloadFiles(projectid, robotid);
                        }
                    }
                    else
                    {
                        rs.elog = JsonConvert.DeserializeObject<List<ELogs>>(new ExecutionLog().getFromLogs(robotid, true));
                        rs.SuccessState = state;
                        rs.Downloads = DownloadFiles(projectid, robotid);
                    }

                    rs.RobotId = Convert.ToInt32(robotid);
                    rs.vdebugger = Variables.vDebugger;
                    string res = JsonConvert.SerializeObject(rs);

                    responseResult.ResultString = res;
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.executioninprogress)
                {
                    string projectid = inputString.Project_Id;
                    string robotid = inputString.ID;
                    string num = inputString.stepvalue;

                    DVariables ty = null; string exeid = string.Empty;
                    if (inputString.dynamicvariables.Count > 0)
                    {
                        ty = inputString.dynamicvariables.First();
                        exeid = ty.ExecutionID;
                    }
                    else
                    {
                        exeid = new StringFunction().getRandomString("32");
                    }

                    int ExecResult = appExecution.startRobot(int.Parse(robotid), int.Parse(num), exeid);
                    string state = Variables.state.ToString();

                    if ((state.ToLower() != StringHelper.sucess) && (state.ToLower() != StringHelper.operationfailed))
                    {
                        rs.RobotId = Convert.ToInt32(robotid);
                        rs.SuccessState = state;
                        if (rs.SuccessState.ToLower().IndexOf("inprogress") > -1)
                        {
                            RuntimeUserInputRequest ruir = new RuntimeUserInputRequest();
                            ruir.dvariables = Variables.dynamicvariables;
                            ruir.nextStepID = new AppExecution().getNextStep(int.Parse(robotid), int.Parse(Variables.thisStep.Id), string.Empty).ToString();
                            ruir.step = Variables.thisStep;
                            ruir.thisVariableName = Variables.thisVariable;

                            rs.runtimerequest = ruir;
                            rs.elog = new List<ELogs>();
                        }
                        else
                        {
                            rs.elog = JsonConvert.DeserializeObject<List<ELogs>>(new ExecutionLog().getFromLogs(robotid, true));
                            rs.Downloads = DownloadFiles(projectid, robotid);
                        }
                    }
                    else
                    {
                        rs.elog = JsonConvert.DeserializeObject<List<ELogs>>(new ExecutionLog().getFromLogs(robotid, true));
                        rs.SuccessState = state;
                        rs.Downloads = DownloadFiles(projectid, robotid);
                    }


                    string res = JsonConvert.SerializeObject(rs);
                    responseResult.ResultString = res;
                    return JsonConvert.SerializeObject(responseResult);
                }
                else
                {
                    responseResult.ResultString = StringHelper.ffalse;
                    return JsonConvert.SerializeObject(responseResult);
                }
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 201;
                responseResult.ExceptionStatus = true;
                responseResult.StatusMessage = ex.Message.ToString();
                responseResult.ResultString = string.Empty;
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return JsonConvert.SerializeObject(responseResult);
            }
        }

        public string Set(Entities inputString)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            responseResult = new ResponseResult();
            try
            {
                string time = DateTime.Now.ToShortTimeString();
                string date = DateTime.Now.ToShortDateString();
                if (inputString.Type == StringHelper.setsteps)
                {
                    Step cs = new Step();
                    responseResult.ResultString = cs.setSteps(inputString.Name, inputString.rid, inputString.wid, inputString.eid, inputString.aid, inputString.Order, inputString.RuntimeUserInput, "1", inputString.Project_Id, inputString.CreateBy, inputString.UpdateBy);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.setstepproperties)
                {
                    Step csp = new Step();
                    responseResult.ResultString = csp.setStepProperties(inputString.stepvalue, inputString.Idm, inputString.Namer, inputString.StepPropertym, inputString.StepPropertyValue, inputString.StepPropertyType, inputString.Order, "null", inputString.CreateBy, inputString.UpdateBy);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.linknodes)
                {
                    responseResult.ResultString = linkNode.updateNodes(inputString.Step_Id, inputString.ChildStepId, inputString.Location);
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.robotschedule)
                {

                    string ret = robot.getRobotSchedule(inputString.Robot_ID);
                    ret = robot.setInputInterval(inputString.Robot_ID, inputString.interval, ret);
                    responseResult.ResultString = ret;
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.saverobot)
                {


                    string result = new Save().SaveRobot(inputString);
                    string state = result.Replace(" ", string.Empty);
                    //"robot updated";robot saved and version created
                    var robot = inputString.robot;
                    if (inputString.robot.dynamicvariables != null && inputString.robot.dynamicvariables.Count > 0)
                    {
                        foreach (DVariables variable in inputString.robot.dynamicvariables)
                        {
                            string success = variableClass.setVariables(variable, robot.Id);
                            if (success.ToLower() == StringHelper.success.ToLower())
                            {
                                if (Variables.dynamicvariables != null && Variables.dynamicvariables.Count > 0)
                                {
                                    Variables.dynamicvariables.Add(variable);
                                }
                                else
                                {
                                    Variables.dynamicvariables = new List<DVariables>();
                                    Variables.dynamicvariables.Add(variable);
                                }
                            }
                        }
                    }

                    switch (state)
                    {
                        case StringHelper.stepssavedcase:
                            responseResult.StatusCode = 200;
                            break;
                        case StringHelper.robotupdatedcase:
                            responseResult.StatusCode = 200;
                            break;
                        case StringHelper.robotsavedandversioncreated:
                            responseResult.StatusCode = 200;
                            break;
                        default:
                            responseResult.StatusCode = 201;
                            break;

                    }

                    responseResult.ResultString = result;
                    responseResult.StatusMessage = result;
                    return JsonConvert.SerializeObject(responseResult);
                }
                else if (inputString.Type == StringHelper.savewarobot)
                {
                    return new DBWARobot().saveWARobot(inputString.rid, inputString.stepvalue);
                }

                //else if (inputString.Type == StringHelper.execution)
                //{
                //    //string projectid = inputString.Project_Id;
                //    //string robotid = inputString.ID;
                //    ////var dvariables = inputString.dynamicvariables;


                //    //APILayer apilayer = new APILayer();
                //    ////return apilayer.apiLogic(projectid, robotid, false, "0");

                //    //responseResult.ResultString = apilayer.apiLogic(projectid, robotid, false, "0") + "||" + Newtonsoft.Json.JsonConvert.SerializeObject(Variables.errorsList);
                //    //return JsonConvert.SerializeObject(responseResult);
                //}

                //else if (inputString.Type == StringHelper.executioninprogress)
                //{
                //    //string projectid = inputString.Project_Id;
                //    //string robotid = inputString.ID;
                //    //string num = inputString.stepvalue;

                //    ////APILayer apilayer = new APILayer(projectid, robotid, true);
                //    //APILayer apilayer = new APILayer();

                //    //responseResult.ResultString = apilayer.apiLogic(projectid, robotid, true, num);
                //    //return JsonConvert.SerializeObject(responseResult);
                //}

                else if (inputString.Type == StringHelper.robottosystem)
                {
                    string robotid = inputString.rid;
                    string systemid = inputString.ID;

                    // cSystems_Robots csr = new cSystems_Robots();
                    responseResult.ResultString = systems_robots.setRobotSystem(robotid, systemid);
                    return JsonConvert.SerializeObject(responseResult);
                }

                else if (inputString.Type == StringHelper.savepredefinedbot)
                {
                    responseResult.ResultString = predefined.setPredefindedBots(inputString.predefinedbot);
                    return JsonConvert.SerializeObject(responseResult);
                }

                else
                {
                    responseResult.ResultString = StringHelper.incorrect;
                    return JsonConvert.SerializeObject(responseResult);
                }
            }
            catch (Exception ex)
            {
                responseResult.StatusCode = 201;
                responseResult.ExceptionStatus = true;
                responseResult.StatusMessage = ex.Message.ToString();
                responseResult.ResultString = string.Empty;
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return JsonConvert.SerializeObject(responseResult);
            }

        }

        public string Rename(Entities inputString)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            ResponseResult objResponce = new ResponseResult();
            try
            {
                //Entities inputString = new Entities();
                //inputString = entityService(input);

                if (inputString.Type == StringHelper.renamerobot)
                {
                    // cRobot cr = new cRobot();
                    objResponce.ResultString = robot.RenameRobot(inputString.oldname, inputString.newname, inputString.ID);
                    return JsonConvert.SerializeObject(objResponce);
                }
                else if (inputString.Type == StringHelper.renameproject)
                {
                    // cProject cp = new cProject();
                    objResponce.ResultString = project.RenameProject(inputString.oldname, inputString.newname, inputString.ID);
                    return JsonConvert.SerializeObject(objResponce);
                }
                else if (inputString.Type == StringHelper.renameworkflow)
                {
                    // cWorkflow cw = new cWorkflow();
                    objResponce.ResultString = workflow.RenameWorkflow(inputString.oldname, inputString.newname);
                    return JsonConvert.SerializeObject(objResponce);
                }
                else if (inputString.Type == StringHelper.renamerobot)
                {
                    objResponce.ResultString = robot.RenameRobot(inputString.newname, inputString.oldname, inputString.ID);
                    return JsonConvert.SerializeObject(objResponce);
                }
                else
                {
                    objResponce.ResultString = StringHelper.incorrect;
                    return JsonConvert.SerializeObject(objResponce);
                }
            }
            catch (Exception ex)
            {
                objResponce.StatusCode = 201;
                objResponce.ExceptionStatus = true;
                objResponce.StatusMessage = ex.Message.ToString();
                objResponce.ResultString = string.Empty;
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return JsonConvert.SerializeObject(objResponce);
            }
        }
        public string Debug(Entities inputString)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            ResponseResult objResponce = new ResponseResult();
            try
            {
                string stepid = string.Empty;
                Variables.vDebugger.Description = string.Empty;
                StepDebug dbg = new StepDebug();

                DVariables ty = null; string exeid = string.Empty;
                if (inputString.dynamicvariables.Count > 0)
                {
                    ty = inputString.dynamicvariables.First();
                    exeid = ty.ExecutionID;
                }
                else
                {
                    exeid = new StringFunction().getRandomString("32");
                }

                stepid = new Validator().DebugStep(int.Parse(inputString.rid), int.Parse(inputString.Step_Id), exeid);
                dbg = Variables.vDebugger;
                dbg.getNextstepid = stepid;
                objResponce.ResultString = JsonConvert.SerializeObject(dbg);
                return JsonConvert.SerializeObject(objResponce);
            }
            catch (Exception ex)
            {
                objResponce.StatusCode = 201;
                objResponce.ExceptionStatus = true;
                objResponce.StatusMessage = ex.Message.ToString();
                objResponce.ResultString = string.Empty;
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return JsonConvert.SerializeObject(objResponce);
            }
        }
        public static Entities entityService(string input)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Entities inputString;
                inputString = JsonConvert.DeserializeObject<Entities>(input);
                //Variables.dynamicvariables = inputString.dynamicvariables;
                return inputString;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return null;
            }
        }

        public string Validate(Entities inputString)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            ResponseResult objResponce = new ResponseResult();
            try
            {
                string exeid = new StringFunction().getRandomString("32");
                objResponce.ResultString = new Validator().ValidateStep(inputString.cstep, exeid);
                return JsonConvert.SerializeObject(objResponce);
            }
            catch (Exception ex)
            {
                objResponce.StatusCode = 201;
                objResponce.ExceptionStatus = true;
                objResponce.StatusMessage = ex.Message.ToString();
                objResponce.ResultString = string.Empty;
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return JsonConvert.SerializeObject(objResponce);
            }
        }
        public string StepExecution(Entities inputString)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            ResponseResult objResponce = new ResponseResult();
            try
            {
                //Entities inputString = JsonConvert.DeserializeObject<Entities>(input);
                //inputString = entityService(input);
                MethodResult objResult = new MethodResult();
                if (inputString.dynamicvariables != null)
                {
                    List<DVariables> duplilist = new List<DVariables>();
                    List<DVariables> frontList = new List<DVariables>();

                    duplilist.AddRange(inputString.dynamicvariables);

                   // Variables.dynamicvariables.Clear();
                    try
                    {
                        //frontList.AddRange(inputString.dynamicvariables.ConvertAll(x => new DVariables() { vlname = x.vlname, vlvalue = x.vlvalue, vlstatus = x.vlstatus }));
                        Entities inputString1 = new Entities();
                        if (inputString.dynamicvariables != null)
                        {
                            if (inputString.dynamicvariables.Count > 0)
                            {
                                for (int i = 0; i < inputString.dynamicvariables.Count; i++)
                                {
                                    if (inputString.dynamicvariables[i] == null)
                                    {
                                        inputString.dynamicvariables.RemoveAt(i);
                                    }
                                }

                                frontList.AddRange(inputString.dynamicvariables.ConvertAll(x => new DVariables() { vlname = x.vlname, vlvalue = x.vlvalue, vlstatus = x.vlstatus }));

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                        return ex.Message;
                    }
                    Variables.dynamicvariables.AddRange(duplilist.Union(frontList));

                }

                string ExecutionID = string.Empty;
                if (inputString.ID1 == string.Empty)
                {
                    ExecutionID = new StringFunction().getRandomString("32");
                }
                else
                {
                    ExecutionID = inputString.ID1;
                }

                objResult = new Validator().StepExecution(int.Parse(inputString.rid), int.Parse(inputString.Step_Id), ExecutionID);

                objResponce.ResultString = JsonConvert.SerializeObject(objResult);
                return JsonConvert.SerializeObject(objResponce);

            }
            catch (Exception ex)
            {
                objResponce.StatusCode = 201;
                objResponce.ExceptionStatus = true;
                objResponce.StatusMessage = ex.Message.ToString();
                objResponce.ResultString = string.Empty;
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return JsonConvert.SerializeObject(objResponce);
            }
        }
        private List<DownloadFilesObject> DownloadFiles(string projectid, string robotid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<DownloadFilesObject> returner = new List<DownloadFilesObject>();
            try
            {
                string vs = appExecution.getVersion(robotid);
                List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(step.getSteps(robotid, vs));

                List<Steps> blackone = steps.FindAll(stp => stp.Action_Id == "215");

                if (blackone.Count > 0)
                {
                    if (Variables.downloads.Count > 0)
                    {
                        foreach (var download in Variables.downloads)
                        {
                            DownloadFilesObject dfo = new DownloadFilesObject();
                            dfo.FileName = download;
                            try
                            {
                                byte[] ress = System.IO.File.ReadAllBytes(download);
                                dfo.FileData = ress;
                            }
                            catch (Exception ex)
                            {
                                string ec = ex.Message;
                            }
                            returner.Add(dfo);
                        }
                    }
                }
                else
                {

                }

                return returner;
            }
            catch (Exception ex)

            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return null;
            }
        }

        public string UploadDocument(byte[] file, string file_name)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            DocumentModel _objDM = new DocumentModel();
            try
            {
                string Destination = Path.Combine(Directory.GetCurrentDirectory(), StringHelper.uploads);
                string filename = file_name;
                string DestZFileName = Path.Combine(Destination, filename);

                if (System.IO.File.Exists(DestZFileName))
                {
                    System.IO.File.Delete(DestZFileName);
                }

                using (FileStream objfile = System.IO.File.Create(DestZFileName))
                {

                }

                System.IO.File.WriteAllBytes(DestZFileName, file);
                _objDM.ResultString = DestZFileName;
                _objDM.StatusCode = 200;
                _objDM.StatusMessage = "Success";
                _objDM.ExceptionStatus = false;
                return JsonConvert.SerializeObject(_objDM);
            }
            catch (Exception ex)
            {
                _objDM.StatusCode = 0;
                _objDM.StatusMessage = "Failure";
                _objDM.ExceptionStatus = true;
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return JsonConvert.SerializeObject(_objDM);
                //return null;
            }
        }
        public string ClearRobotGlobalVariables(string Robot_Id)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string _status = "false";
            try
            {
                List<DVariables> _objlvariables = new List<DVariables>();
                if (Variables.dynamicvariables != null)
                {
                    if (Variables.dynamicvariables.Count > 0)
                    {
                        for (int i = 0; i < Variables.dynamicvariables.Count; i++)
                        {
                            if (Variables.dynamicvariables[i] != null)
                            {
                                //if (Variables.dynamicvariables[i].RobotID == Robot_Id)
                                //{
                                    //Variables.dynamicvariables.Remove(Variables.dynamicvariables[i]);
                                    DVariables _objV = new DVariables();
                                    _objV = Variables.dynamicvariables[i];
                                    _objlvariables.Add(_objV);



                                //}
                            }
                        }
                    }
                }



                if (_objlvariables.Count > 0)
                {
                    for (int i = 0; i < _objlvariables.Count; i++)
                    {
                        Variables.dynamicvariables.Remove(_objlvariables[i]);
                    }
                }
                _status = "true";
                return _status;
            }
            catch (Exception ex)
            {
                _status = "false";
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return _status;

            }
        }
        public bool TestConnection()
        {
            return true;
        }
    }
}
