﻿using Genbase.classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using static Genbase.BLL.StepPropertyHandler;
using Genbase.DAL;
using Genbase.Classes;

namespace Genbase.BLL
{
    // Change to Robot Class and add these methods
    public class AppExecution
    {
        DAL.PostgresConnect dbd = new DAL.PostgresConnect();
        BotExecution be = new BotExecution();

        public string Id { get; set; }
        public string Name { get; set; }
        public string Project_Id { get; set; }
        public string Description { get; set; }
        public string RobotType { get; set; }
        public string currentStepType { get; set; }
        public int currnetStep { get; set; }
        public List<Steps> Steps { get; set; }
        public List<LinkNodes> LinkNodes { get; set; }
        public List<DVariables> dynamicvariables { get; set; }

        LinkNode c = new LinkNode();
        Step cs = new Step();
        Variables variables = new Variables();
        Robots r = new Robots();
        public int startRobot(int robotId, int stepId, string execid)
        {
            try
            {
                executeRobot(robotId, stepId, execid);
                return -1;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return -1;
            }
        }

        public string getVersion(string robotid)
        {
            string vquery = "select max(\"versionid\") from \"Versions\" where \"rid\" =" + robotid + "";
            return dbd.DbExecuteQuery(vquery, "createproject");
        }
        public int executeRobot(int robotId, int stepId, string execid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                int status = -1;
                //pass the version change the method in get steps
                string vs = getVersion(robotId.ToString());
                List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(cs.getSteps(robotId.ToString(), vs));
                try
                {
                    foreach (var step in steps)
                    {
                        if (step.StepProperties != null)
                        {
                            List<StepProperties> props = step.StepProperties.FindAll(s => s.StepPropertyValue.ToLower().Trim() == string.Empty);
                            props.ForEach(d => new Logger().LogException(Messages.EmptyStepProperty.Replace("---", d.StepProperty).Replace(StringHelper.star, step.Name)));
                            props.ForEach(d => new ExecutionLog().add2Log(Messages.EmptyStepProperty.Replace("---", d.StepProperty).Replace(StringHelper.star, step.Name), step.Robot_Id, step.Name, StringHelper.errorcode, false));

                            //if (props.Count > 0)
                            //{
                            //    return -1;
                            //}
                            foreach (var i in props)
                            {
                                if ((i.CustomValidation == "1") && (i.StepPropertyValue == ""))
                                {
                                    Variables.state = StringHelper.failure;
                                    return -1;
                                }
                            }

                        }
                       

                    }

                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    return -1;
                }

                if (steps == null || steps.Count == 0)
                {
                    Variables.state = "no steps";
                }

                //steps.ForEach(prop => new StepPropertyValidationHandler().ValidateProperties(prop));
               




                if (stepId == 0)
                {
                    stepId = getNextStep(robotId, stepId, "");
                }
                Steps stepname = steps.FindAll(stp => stp.Id == stepId.ToString()).ToList()[0];
                string steptype = stepname.Type;
                if (steptype.ToLower().IndexOf("workflow") > -1)
                {
                    var xy = stepname.StepProperties.FindAll(stp => stp.StepProperty == "RobotName").ToList()[0];

                    be.startRobot(Convert.ToInt32(xy.StepPropertyValue), 0, execid);
                    executeRobot(Convert.ToInt32(xy.StepPropertyValue), 0, execid);
                }
                if (currentStepType == null && stepId > 0)
                {
                    Variables.thisStep = steps.FindAll(stp => stp.Id == stepId.ToString()).ToList()[0];
                    currentStepType = getCurrentStepType(robotId, stepId);
                }
                if (currentStepType == null)
                {
                    Variables.thisStep = steps.FindAll(stp => stp.Id == stepId.ToString()).ToList()[0];
                    return -1;
                }
                if (currentStepType.ToLower() == "2")
                {
                    Variables.thisStep = steps.FindAll(stp => stp.Id == stepId.ToString()).ToList()[0];
                    executeStep(robotId, stepId, steps, execid);
                    return -1;
                }
                else if (currentStepType == "166")
                {
                    Variables.thisStep = steps.FindAll(stp => stp.Id == stepId.ToString()).ToList()[0];
                    executeStep(robotId, stepId, steps, execid);
                    var props = checkCondition(stepId, execid);
                    currnetStep = getNextStep(robotId, stepId, props);
                    executeRobot(robotId, currnetStep, execid);
                }

                else if (currentStepType == "167")
                {
                    Variables.thisStep = steps.FindAll(stp => stp.Id == stepId.ToString()).ToList()[0];
                    executeStep(robotId, stepId, steps, execid);
                    var props = checkCondition(stepId, execid);
                    currnetStep = getNextStep(robotId, stepId, props);
                    executeRobot(robotId, currnetStep, execid);
                }
                else if (currentStepType == "162")
                {
                    Variables.thisStep = steps.FindAll(stp => stp.Id == stepId.ToString()).ToList()[0];
                    string acntype = currentStepType;
                    executeStep(robotId, stepId, steps, execid);
                    string nextStepID = checkCondition(steps, stepId.ToString(), acntype, execid);
                    executeRobot(robotId, int.Parse(nextStepID), execid);
                }
                else if (currentStepType == "163")
                {
                    Variables.thisStep = steps.FindAll(stp => stp.Id == stepId.ToString()).ToList()[0];
                    string acntype = currentStepType;
                    executeStep(robotId, stepId, steps, execid);
                    string nextStepID = checkCondition(steps, stepId.ToString(), acntype, execid);
                    executeRobot(robotId, int.Parse(nextStepID), execid);
                }
                else if (currentStepType == "81" || currentStepType == "227")
                {
                    Variables.thisStep = steps.FindAll(stp => stp.Id == stepId.ToString()).ToList()[0];
                    executeStep(robotId, stepId, steps, execid);
                }
                else if (currentStepType == "82")
                {
                    Variables.thisStep = steps.FindAll(stp => stp.Id == stepId.ToString()).ToList()[0];
                    executeStep(robotId, stepId, steps, execid);
                }
                else
                {
                    Variables.thisStep = steps.FindAll(stp => stp.Id == stepId.ToString()).ToList()[0];
                    executeStep(robotId, stepId, steps, execid);
                   // var props = checkCondition(stepId, execid);
                    currnetStep = getNextStep(robotId, stepId, "");
                    executeRobot(robotId, currnetStep, execid);
                }
                return status;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return -1;
            }
        }
        public string checkCondition(int stepId, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<StepProperties> stepproperties = JsonConvert.DeserializeObject<List<StepProperties>>(cs.getStepProperties(stepId.ToString()));
                StepProperties outt = stepproperties.Find(x => x.StepProperty.ToLower() == StringHelper.output);

                DVariables value = new DVariables(); string output = string.Empty;
                if (outt.StepPropertyValue.Trim() != string.Empty)
                {
                    value = new VariableHandler().getVariables(outt.StepPropertyValue, executionID);
                    if (value != null)
                    {
                        output = value.vlvalue;
                    }
                }
                return output;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }
        public string checkCondition(List<Steps> steps, string curStepID, string actionid, string executionID)
        {
            string returning = string.Empty; string loopstartorder = string.Empty; string loopstarter = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (actionid == "162")
                {
                    string successer = steps.Find(stp => stp.Id == curStepID).StepProperties.Find(prop => prop.StepProperty.ToLower() == "success").StepPropertyValue;
                    string failurer = steps.Find(stp => stp.Id == curStepID).StepProperties.Find(prop => prop.StepProperty.ToLower() == "failure").StepPropertyValue;

                    string successorder = successer.Split(',')[1];
                    string failorder = failurer.Split(',')[1];

                    string successid = steps.Find(stp => Math.Abs(Convert.ToInt64(stp.Order.Trim())) == Math.Abs(Convert.ToInt64(successorder.Trim())) || Math.Abs(Convert.ToInt64(stp.Id.Trim())) == Math.Abs(Convert.ToInt64(successorder.Trim()))).Id;
                    string failid = steps.Find(stp => Math.Abs(Convert.ToInt64(stp.Order.Trim())) == Math.Abs(Convert.ToInt64(failorder.Trim())) || Math.Abs(Convert.ToInt64(stp.Id.Trim())) == Math.Abs(Convert.ToInt64(failorder.Trim()))).Id;

                    VariableHandler vh = new VariableHandler();
                    if (vh.checkVariableExists("IloopStart:" + curStepID, executionID))
                    {
                        string caser = vh.getVariables("IloopStart:" + curStepID, executionID).vlvalue.ToString();
                        if (caser.Trim().ToLower() == "failure")
                        {
                            returning = failid;
                        }
                        else
                        {
                            returning = successid;
                        }
                    }
                    else
                    {
                        returning = successid;
                    }
                }
                else if (actionid == "163")
                {
                    loopstarter = steps.Find(stp => stp.Id == curStepID).StepProperties.Find(prop => prop.StepProperty.ToLower() == "loopstartid").StepPropertyValue;
                    loopstartorder = loopstarter.Split(',')[1];
                    string loopstartid = steps.Find(stp => Math.Abs(Convert.ToInt64(stp.Order.Trim())) == Math.Abs(Convert.ToInt64(loopstartorder.Trim())) || Math.Abs(Convert.ToInt64(stp.Id.Trim())) == Math.Abs(Convert.ToInt64(loopstartorder.Trim()))).Id;
                    returning = loopstartid;
                }
                currentStepType = steps.Find(stp => stp.Id == returning.Trim()).Action_Id;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                string ec = ex.Message;
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
            }
            return returning;
        }

        public int executeStep(int robotId, int stepId, List<Steps> steps, string execid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string js = string.Empty;
                string robotnum = robotId.ToString();
                List<LinkNodes> LinkNode = JsonConvert.DeserializeObject<List<LinkNodes>>(c.getStepsLinks(robotnum));
                bool checkStart = steps.Any(x => x.Action_Id == "1");
                bool checkStop = steps.Any(x => x.Action_Id == "2");

                if (checkStart == true && checkStop == true)
                {
                    if (LinkNode != null && LinkNode.Count > 0)
                    {

                        LinkNode.OrderBy(o => int.Parse(o.Id));
                        var firstElement = LinkNode.First();
                        var il = (from j in steps where j.Action_Id == "1" select j).ToList();
                        if (stepId != 0)
                        {
                            il = (from j in steps where j.Id == stepId.ToString() select j).ToList();
                        }

                        if (il != null && il.Count > 0)
                        {
                            var result = LinkNode.Find(item => item.StepId == il[0].Id);
                            string xchild = string.Empty;
                            if (result != null) xchild = result.ChildStepIds;

                            try
                            {
                                Element cde = new Element(); Classes.Action ca = new Classes.Action();
                                string elemName = cde.getElementById(il[0].Element_Id);
                                string actName = ca.getAction(il[0].Action_Id);

                                new Logger().LogException(elemName + "." + actName + " ~ " + il[0].Name);
                                new ExecutionLog().add2Log(elemName + "." + actName + " ~ " + il[0].Name, robotId.ToString(), StringHelper.server, StringHelper.exceptioncode);

                                il[0].StepProperties = JsonConvert.DeserializeObject<List<StepProperties>>(cs.getStepProperties(stepId.ToString()));

                                Type type = Type.GetType(StringHelper.ExecutionClassTypes + elemName);
                                object instance = Activator.CreateInstance(type);
                                MethodInfo method = type.GetMethod(actName);
                                if (il[0].RuntimeUserInput.ToLower() == StringHelper.truee) // && InputValue == string.Empty)
                                {
                                    object[] paramss = new object[2] { il[0], execid };
                                    MethodResult mr = new MethodResult();
                                    mr = (MethodResult)method.Invoke(instance, paramss);

                                    if (xchild.IndexOf('`') > -1)
                                    {
                                        xchild = xchild.Split('`')[0];
                                    }

                                    Variables.state = StringHelper.inprogress;
                                }
                                else if (Variables.exeStatus)
                                {
                                    object[] paramss = new object[2] { il[0], execid };
                                    MethodResult mr = new MethodResult();
                                    mr = (MethodResult)method.Invoke(instance, paramss);
                                    Variables.exeStatus = false;

                                    Inprogress progress = new Inprogress();
                                    progress.Status = StringHelper.inprogress;
                                    progress.step = il[0];

                                }
                                else
                                {
                                    object[] paramss = new object[2] { il[0], execid };
                                    MethodResult mr = new MethodResult();
                                    mr = (MethodResult)method.Invoke(instance, paramss);
                                    if (mr.result == false)
                                    {
                                        Variables.state = StringHelper.failure;

                                        string failMessage = new StepPropertyHandler().getExceptionMessage(il[0], string.Join("||", mr.message));

                                        return -1;
                                    }
                                    else
                                    {
                                        Variables.state = StringHelper.success;
                                        return -1;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            }
                        }
                    }
                }
                else
                {
                    new ExecutionLog().add2Log(StringHelper.nostartorstop, robotnum, StringHelper.zero, StringHelper.exceptioncode, false);
                }
                //here added to make it clear the previous details
                Variables.downloadattachments.Clear(); Variables.isSocial.Clear(); Variables.readOutputupdate.Clear(); Variables.classifiedData.Clear();
                Variables.fbOutput.Clear(); Variables.fbOutputUpdate.Clear();
                return -1;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return -1;
            }
        }
        public string getCurrentStepType(int robotId, int stepId)
        {
            string vs = getVersion(robotId.ToString());
            List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(cs.getSteps(robotId.ToString(), vs));
            var il = (from j in steps where j.Id == stepId.ToString() select j).ToList();
            return currentStepType = il[0].Action_Id;
        }
        public int getINextStep(int robotId, int stepId, string flag)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string vs = getVersion(robotId.ToString());
                List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(cs.getSteps(robotId.ToString(), vs));
                List<LinkNodes> LinkNode = JsonConvert.DeserializeObject<List<LinkNodes>>(c.getStepsLinks(robotId.ToString()));
                var il = (from j in steps where j.Id == stepId.ToString() select j).ToList();
                var result = LinkNode.Find(item => item.StepId == il[0].Id);

                foreach (Steps step in steps)
                {
                    if (step.Action_Id + " ^ " + step.Order == flag)
                    {
                        Id = step.Id;
                    }
                }

                return int.Parse(Id);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return -1;
            }
        }
        public int getNextStep(int robotId, int stepId, string flag)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                int count = 0;
                string vs = getVersion(robotId.ToString());
                List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(cs.getSteps(robotId.ToString(), vs));
                List<LinkNodes> LinkNode = JsonConvert.DeserializeObject<List<LinkNodes>>(c.getStepsLinks(robotId.ToString()));
                if (steps.Count > 0 && LinkNode.Count > 0)
                {
                    if (stepId == 0)
                    {
                        var il = (from j in steps where j.Action_Id == "1" select j).ToList();
                        var result = LinkNode.Find(item => item.StepId == il[0].Id);
                        currentStepType = il[0].Action_Id;
                        string xchild = string.Empty;
                        if (result != null) xchild = result.StepId;

                        if (xchild.IndexOf('`') > -1)
                        {
                            Id = xchild.Split('`')[0];
                        }
                        else
                        {
                            Id = xchild;
                        }
                    }
                    else
                    {

                        var il = (from j in steps where j.Id == stepId.ToString() select j).ToList();
                        var result = LinkNode.Find(item => item.StepId == il[0].Id);


                        string xchild = string.Empty;
                        if (result != null) xchild = result.ChildStepIds;

                        if (xchild.IndexOf('`') > -1)
                        {

                            if (flag != string.Empty && flag != "N")
                            {
                                string[] childs = xchild.Split('`');
                                foreach (string child in childs)
                                {
                                    //flag
                                    //child
                                    if (child != string.Empty)
                                    {
                                        var il2 = (from j in steps where j.Id == child select j).ToList();
                                        if (il2[0].Name + " ^ " + il2[0].Order == flag || il2[0].Name + " , " + il2[0].Order == flag)
                                        {
                                            Id = il2[0].Id;
                                            count++;
                                        }
                                        else if (il2[0].Name + " ^ " + il2[0].Id == flag || il2[0].Name + " , " + il2[0].Id == flag)
                                        {
                                            Id = il2[0].Id;
                                        }


                                        if (count == 0)
                                        {
                                            foreach (Steps step in steps)
                                            {
                                                Element cde = new Element(); Classes.Action ca = new Classes.Action();
                                                string elemName = cde.getElementById(step.Element_Id);
                                                string actName = ca.getAction(step.Action_Id);

                                                if (actName + " ^ " + step.Order == flag || actName + " , " + step.Order == flag)
                                                {
                                                    Id = step.Id;
                                                }
                                                else if (actName + " ^ " + step.Id == flag || actName + " , " + step.Id == flag)
                                                {
                                                    Id = step.Id;
                                                }
                                            }
                                        }

                                    }


                                }
                            }
                            else if (flag == "N")
                            {
                                Id = xchild.Split('`')[1];
                            }
                            else
                            {
                                Id = xchild.Split('`')[0];
                            }

                            var il1 = (from j in steps where j.Id == Id.ToString() select j).ToList();
                            var result1 = LinkNode.Find(item => item.StepId == il1[0].Id);
                            currentStepType = il1[0].Action_Id;
                        }
                    }
                    return int.Parse(Id);
                }
                return -1;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return -1;
            }
        }

        public List<Steps> getNexStep1(string RobotId, string ActionId, string cur_StepId)
        {
            return null;
        }
        public List<Steps> setStepsOrder(string RobotId, List<Steps> orig_Steps, bool isAsc)
        {
            return null;
        }
        public RuntimeUserInputRequest getRunTimeUserInputRequest(Steps step, string executionID)
        {
            RuntimeUserInputRequest ruir = new RuntimeUserInputRequest();
            try
            {
                ruir.dvariables = Variables.dynamicvariables.FindAll(dv => dv.ExecutionID == executionID);
                if (Variables.thisStep.Id != null)
                {
                    ruir.nextStepID = getNextStep(int.Parse(Variables.robotid), int.Parse(Variables.thisStep.Id), string.Empty).ToString();
                    ruir.step = Variables.thisStep;
                }
                else
                {
                    ruir.nextStepID = getNextStep(int.Parse(Variables.robotid), int.Parse(step.Id), string.Empty).ToString();
                    ruir.step = step;
                }

                ruir.thisVariableName = Variables.thisVariable;
                return ruir;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return ruir;
            }
        }

    }
    public class BotExecution
    {
        DAL.PostgresConnect dbd = new DAL.PostgresConnect();

        public string Id { get; set; }
        public string Name { get; set; }
        public string Project_Id { get; set; }
        public string Description { get; set; }
        public string RobotType { get; set; }
        public string currentStepType { get; set; }
        public int currnetStep { get; set; }
        public List<Steps> Steps { get; set; }
        public List<LinkNodes> LinkNodes { get; set; }
        public List<DVariables> dynamicvariables { get; set; }

        LinkNode c = new LinkNode();
        Step cs = new Step();
        Variables variables = new Variables();
        Robots r = new Robots();
        public int startRobot(int robotId, int stepId, string exeid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                executeRobot(robotId, stepId, exeid);
                return 1;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return -1;
            }
        }

        public string getVersion(string robotid)
        {
            string vquery = "select max(\"versionid\") from \"Versions\" where \"rid\" =" + robotid + "";
            return dbd.DbExecuteQuery(vquery, "createproject");
        }
        public int executeRobot(int robotId, int stepId, string exeid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                int status = -1;
                //pass the version change the method in get steps
                string vs = getVersion(robotId.ToString());
                List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(cs.getSteps(robotId.ToString(), vs));
                if (steps.Count == 0)
                {
                    Variables.state = "no steps";
                }
                steps.ForEach(prop => new StepPropertyValidationHandler().ValidateProperties(prop));
                if (stepId == 0)
                {
                    stepId = getNextStep(robotId, stepId, "");
                }
                string steptype = (from step in steps where step.Id == stepId.ToString() select step.Type).ToString();
                if (currentStepType == null && stepId > 0)
                {
                    currentStepType = getCurrentStepType(robotId, stepId);
                }
                if (currentStepType == null)
                    return -1;
                if (currentStepType.ToLower() == "2")
                    return -1;
                else if (currentStepType == "166")
                {
                    // If Else condition if
                    executeStep(robotId, stepId, steps, exeid);
                    var props = checkCondition(stepId, exeid);
                    currnetStep = getNextStep(robotId, stepId, props);
                    executeRobot(robotId, currnetStep, exeid);
                }
                else if (currentStepType == "162")
                {
                    string acntype = currentStepType;
                    executeStep(robotId, stepId, steps, exeid);
                    string nextStepID = checkCondition(steps, stepId.ToString(), acntype, exeid);
                    executeRobot(robotId, int.Parse(nextStepID), exeid);
                }
                else if (currentStepType == "163")
                {
                    string acntype = currentStepType;
                    executeStep(robotId, stepId, steps, exeid);
                    string nextStepID = checkCondition(steps, stepId.ToString(), acntype, exeid);
                    executeRobot(robotId, int.Parse(nextStepID), exeid);
                }
                else if (currentStepType == "81")
                {
                    executeStep(robotId, stepId, steps, exeid);
                }
                else if (currentStepType == "82")
                {
                    executeStep(robotId, stepId, steps, exeid);
                }
                else
                {
                    executeStep(robotId, stepId, steps, exeid);
                    currnetStep = getNextStep(robotId, stepId, "");
                    executeRobot(robotId, currnetStep, exeid);
                }
                return status;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return -1;
            }
        }
        public string checkCondition(int stepId, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<StepProperties> stepproperties = JsonConvert.DeserializeObject<List<StepProperties>>(cs.getStepProperties(stepId.ToString()));
                StepProperties outt = stepproperties.Find(x => x.StepProperty.ToLower() == StringHelper.output);

                DVariables value = new DVariables(); string output = string.Empty;
                if (outt.StepPropertyValue.Trim() != string.Empty)
                {
                    value = new VariableHandler().getVariables(outt.StepPropertyValue, executionID);
                    if (value != null)
                    {
                        output = value.vlvalue;
                    }
                }
                return output;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }
        public string checkCondition(List<Steps> steps, string curStepID, string actionid, string ExecutionID)
        {
            string returning = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (actionid == "162")
                {
                    string successer = steps.Find(stp => stp.Id == curStepID).StepProperties.Find(prop => prop.StepProperty.ToLower() == "success").StepPropertyValue;
                    string failurer = steps.Find(stp => stp.Id == curStepID).StepProperties.Find(prop => prop.StepProperty.ToLower() == "failure").StepPropertyValue;

                    string successorder = successer.Split(',')[1];
                    string failorder = failurer.Split(',')[1];

                    string successid = steps.Find(stp => stp.Order.Trim() == successorder.Trim()).Id;
                    string failid = steps.Find(stp => stp.Order.Trim() == failorder.Trim()).Id;

                    VariableHandler vh = new VariableHandler();
                    if (vh.checkVariableExists("loop" + curStepID, ExecutionID))
                    {
                        string caser = vh.getVariables("loop" + curStepID, ExecutionID).vlvalue.ToString();
                        if (caser.Trim().ToLower() == "failure")
                        {
                            returning = failid;
                        }
                        else
                        {
                            returning = successid;
                        }
                    }
                }
                else if (actionid == "163")
                {
                    string loopstarter = steps.Find(stp => stp.Id == curStepID).StepProperties.Find(prop => prop.StepProperty.ToLower() == "loopstartid").StepPropertyValue;
                    string loopstartorder = loopstarter.Split(',')[1];
                    string loopstartid = steps.Find(stp => stp.Order.Trim() == loopstartorder.Trim()).Id;
                    returning = loopstartid;
                }
                currentStepType = steps.Find(stp => stp.Id == returning.Trim()).Action_Id;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                string ec = ex.Message;
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
            }
            return returning;
        }

        public int executeStep(int robotId, int stepId, List<Steps> steps, string exeid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string js = string.Empty;
                string robotnum = robotId.ToString();
                List<LinkNodes> LinkNode = JsonConvert.DeserializeObject<List<LinkNodes>>(c.getStepsLinks(robotnum));
                bool checkStart = steps.Any(x => x.Action_Id == "1");
                bool checkStop = steps.Any(x => x.Action_Id == "2");

                if (checkStart == true && checkStop == true)
                {
                    if (LinkNode != null && LinkNode.Count > 0)
                    {

                        LinkNode.OrderBy(o => int.Parse(o.Id));
                        var firstElement = LinkNode.First();
                        var il = (from j in steps where j.Action_Id == "1" select j).ToList();
                        if (stepId != 0)
                        {
                            il = (from j in steps where j.Id == stepId.ToString() select j).ToList();
                        }

                        if (il != null && il.Count > 0)
                        {
                            var result = LinkNode.Find(item => item.StepId == il[0].Id);
                            string xchild = string.Empty;
                            if (result != null) xchild = result.ChildStepIds;

                            try
                            {
                                Element cde = new Element(); Classes.Action ca = new Classes.Action();
                                string elemName = cde.getElementById(il[0].Element_Id);
                                string actName = ca.getAction(il[0].Action_Id);
                                il[0].StepProperties = JsonConvert.DeserializeObject<List<StepProperties>>(cs.getStepProperties(stepId.ToString()));


                                Type type = Type.GetType(StringHelper.ExecutionClassTypes + elemName);
                                object instance = Activator.CreateInstance(type);
                                MethodInfo method = type.GetMethod(actName);
                                if (il[0].RuntimeUserInput.ToLower() == StringHelper.truee)
                                {
                                    object[] paramss = new object[2] { il[0], exeid };
                                    MethodResult mr = new MethodResult();
                                    mr = (MethodResult)method.Invoke(instance, paramss);

                                    if (xchild.IndexOf('`') > -1)
                                    {
                                        xchild = xchild.Split('`')[0];
                                    }
                                    Inprogress progress = new Inprogress();
                                    progress.inDex = xchild;
                                    progress.Status = StringHelper.inprogress;
                                    progress.step = il[0];
                                    Variables.state = JsonConvert.SerializeObject(progress);
                                }
                                else if (Variables.exeStatus)
                                {
                                    object[] paramss = new object[2] { il[0], exeid };
                                    MethodResult mr = new MethodResult();
                                    mr = (MethodResult)method.Invoke(instance, paramss);
                                    Variables.exeStatus = false;

                                    Inprogress progress = new Inprogress();
                                    progress.Status = StringHelper.inprogress;
                                    progress.step = il[0];

                                }
                                else
                                {
                                    object[] paramss = new object[2] { il[0], exeid };
                                    MethodResult mr = new MethodResult();
                                    mr = (MethodResult)method.Invoke(instance, paramss);
                                    if (mr.result == false)
                                    {
                                        Variables.state = StringHelper.failure;

                                        string failMessage = new StepPropertyHandler().getExceptionMessage(il[0], string.Join("||", mr.message));

                                        return -1;
                                    }
                                    else
                                    {
                                        Variables.state = StringHelper.success;
                                        return -1;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            }
                        }
                    }
                }
                else
                {
                    new ExecutionLog().add2Log(StringHelper.nostartorstop, robotnum, StringHelper.zero, StringHelper.exceptioncode, false);
                }
                //here added to make it clear the previous details
                Variables.downloadattachments.Clear(); Variables.isSocial.Clear(); Variables.readOutputupdate.Clear(); Variables.classifiedData.Clear();
                Variables.fbOutput.Clear(); Variables.fbOutputUpdate.Clear();
                return -1;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return -1;
            }
        }
        public string getCurrentStepType(int robotId, int stepId)
        {
            string vs = getVersion(robotId.ToString());
            List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(cs.getSteps(robotId.ToString(), vs));
            var il = (from j in steps where j.Id == stepId.ToString() select j).ToList();
            return currentStepType = il[0].Action_Id;
        }
        public int getINextStep(int robotId, int stepId, string flag)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string vs = getVersion(robotId.ToString());
                List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(cs.getSteps(robotId.ToString(), vs));
                List<LinkNodes> LinkNode = JsonConvert.DeserializeObject<List<LinkNodes>>(c.getStepsLinks(robotId.ToString()));
                var il = (from j in steps where j.Id == stepId.ToString() select j).ToList();
                var result = LinkNode.Find(item => item.StepId == il[0].Id);

                foreach (Steps step in steps)
                {
                    if (step.Action_Id + " , " + step.Order == flag)
                    {
                        Id = step.Id;
                    }
                }

                return int.Parse(Id);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return -1;
            }
        }
        public int getNextStep(int robotId, int stepId, string flag)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                int count = 0;
                string vs = getVersion(robotId.ToString());
                List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(cs.getSteps(robotId.ToString(), vs));
                List<LinkNodes> LinkNode = JsonConvert.DeserializeObject<List<LinkNodes>>(c.getStepsLinks(robotId.ToString()));
                if (steps.Count > 0 && LinkNode.Count > 0)
                {
                    if (stepId == 0)
                    {
                        var il = (from j in steps where j.Action_Id == "1" select j).ToList();
                        var result = LinkNode.Find(item => item.StepId == il[0].Id);
                        currentStepType = il[0].Action_Id;
                        string xchild = string.Empty;
                        if (result != null) xchild = result.StepId;

                        if (xchild.IndexOf('`') > -1)
                        {
                            Id = xchild.Split('`')[0];
                        }
                        else
                        {
                            Id = xchild;
                        }
                    }
                    else
                    {

                        var il = (from j in steps where j.Id == stepId.ToString() select j).ToList();
                        var result = LinkNode.Find(item => item.StepId == il[0].Id);


                        string xchild = string.Empty;
                        if (result != null) xchild = result.ChildStepIds;

                        if (xchild.IndexOf('`') > -1)
                        {

                            if (flag != string.Empty && flag != "N")
                            {
                                string[] childs = xchild.Split('`');
                                foreach (string child in childs)
                                {
                                    //flag
                                    //child
                                    if (child != string.Empty)
                                    {
                                        var il2 = (from j in steps where j.Id == child select j).ToList();
                                        if (il2[0].Action_Id + " , " + il2[0].Order == flag)
                                        {
                                            Id = il2[0].Id;
                                            count++;
                                        }
                                        if (count == 0)
                                        {
                                            foreach (Steps step in steps)
                                            {
                                                if (step.Action_Id + " , " + step.Order == flag)
                                                {
                                                    Id = step.Id;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (flag == "N")
                            {
                                Id = xchild.Split('`')[1];
                            }
                            else
                            {
                                Id = xchild.Split('`')[0];
                            }

                            var il1 = (from j in steps where j.Id == Id.ToString() select j).ToList();
                            var result1 = LinkNode.Find(item => item.StepId == il1[0].Id);
                            currentStepType = il1[0].Action_Id;
                        }
                    }
                    return int.Parse(Id);
                }
                return -1;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return -1;
            }
        }

        public List<Steps> getNexStep1(string RobotId, string ActionId, string cur_StepId)
        {
            return null;
        }
        public List<Steps> setStepsOrder(string RobotId, List<Steps> orig_Steps, bool isAsc)
        {
            return null;
        }

    }

}