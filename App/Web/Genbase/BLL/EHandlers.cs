﻿using Genbase.classes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Text.RegularExpressions;
using System.DirectoryServices;
using System.Security.Policy;
using System.Security;
using Renci.SshNet.Sftp;
using System.Security.Cryptography;
using Genbase.Classes;

namespace Genbase.BLL
{
    public class EHandlers
    {

    }
    public class MethodResult
    {
        public bool result { get; set; }
        public List<KeyValuePair<string, string>> message { get; set; }
        public List<DVariables> dynamicVariables { get; set; }
        public List<ELogs> elog { get; set; }
        public StatusModel Status { get; set; }
    }
    public class MethodResultHandler
    {
        ExecutionLog log = new ExecutionLog();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="messages"></param>
        /// <param name="dynamicVariables"></param>
        /// <returns></returns>
        public MethodResult createResultType(bool result, List<KeyValuePair<string, string>> messages, List<DVariables> dynamicVariables, StatusModel s)
        {
            MethodResult mr = new MethodResult();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                mr.dynamicVariables = dynamicVariables;
                mr.message = messages;
                mr.result = result;
                mr.Status = s;
                return mr;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return mr;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipname"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public ConnectionInfo CreateConnection(string ipname, int port, string username, string password)
        {
            ConnectionInfo connInfo = new ConnectionInfo(ipname, port, username,
                new AuthenticationMethod[] {
            new PasswordAuthenticationMethod(username, password),
                new PrivateKeyAuthenticationMethod(username, new PrivateKeyFile[]
                {
                    //new PrivateKeyFile(PrivateKeyFilePath, "passphrase")
                }),
            });
            try
            {
                return connInfo;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return connInfo;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="result"></param>
        /// <param name="step"></param>
        public void outputDisplay(SshCommand cmd, IAsyncResult result, Steps step)
        {
            try
            {
                using (var reader = new StreamReader(cmd.OutputStream, Encoding.UTF8, true, 1024, true))
                {
                    while (!result.IsCompleted || !reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        if (line != null)
                        {
                            log.add2Log(line, step.Robot_Id, step.Name, StringHelper.eventcode,false);
                        }
                    }
                }
                cmd.EndExecute(result);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public KeyValuePair<string, string> createMessage(string message, string type)
        {
            return new KeyValuePair<string, string>(message, type);
        }
    }
    public class VariableHandler
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="status"></param>
        /// <param name="isUpdate"></param>
        /// <param name="TYPE"></param>
        /// <returns></returns>
        public bool CreateVariable(string name, dynamic value, string status, bool isUpdate, string TYPE, string robotid, string ExecutionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                DVariables d = new DVariables();
                d.vlname = name;
                d.vlvalue = value;
                d.vltype = TYPE;
                d.vlstatus = StringHelper.truee;
                d.RobotID = robotid;
                d.ExecutionID = ExecutionID;

                if (isUpdate == true && checkVariableExists(name, ExecutionID) == true)
                {
                    foreach (var item in Variables.dynamicvariables.Where(w => w.vlname == name && w.ExecutionID == ExecutionID))
                    {
                        item.vlvalue = value;
                        item.vltype = TYPE;
                        item.vlstatus = StringHelper.truee;
                    }
                }
                else if (isUpdate == false && checkVariableExists(name, ExecutionID) == true)
                {
                    //deletion goes here
                    //creation goes here
                    Variables.dynamicvariables.Remove(d);
                    Variables.dynamicvariables.Add(d);
                }
                else
                {
                    //creation goes here
                    Variables.dynamicvariables.Add(d);
                }

                return true;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool checkVariableExists(string name, string ExecutionID)
        {
            return Variables.dynamicvariables.Any(w => w.vlname == name && w.ExecutionID == ExecutionID) || Variables.systemvariables.ContainsKey(name);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool DeleteVariable(string name, string ExecutionID)
        {
            try
            {
                foreach (var item in Variables.dynamicvariables.Where(w => w.vlname == name && w.ExecutionID == ExecutionID))
                {
                    Variables.dynamicvariables.Remove(item);
                }
                return true;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public DVariables getVariables(string name, string executionID)
        {
            DVariables dv = new DVariables();
            if (Variables.systemvariables.ContainsKey(name))
            {
                DataTable dt = new DataTable();
                dt.Columns.Add();
                dt.Rows.Add(Variables.systemvariables[name]);
                dv = new DVariables { vlname = name, vlvalue = JsonConvert.SerializeObject(dt), vltype = "Datatable", vlstatus = "true" };
            }
            else
            {
                dv = Variables.dynamicvariables.Find(w => w.vlname == name && w.vlstatus == "true" && w.ExecutionID==executionID);
            }

            return dv;
        }
        public string getVariableValue(DVariables dv)
        {
            DataTable dt = new DataTable();
            if (dv.vltype == "datatable")
            {
                dt = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
            }
            else if (dv.vltype == "string")
            {
                dt.Columns.Add();
                dt.Rows[0][0] = dv.vlvalue;
            }
            else if (dv.vltype == "list")
            {
                dt.Columns.Add();
                List<dynamic> list = JsonConvert.DeserializeObject<List<dynamic>>(dv.vlvalue);
                //todo convert this list to a column in datatable dt
            }
            else if (dv.vltype == "row-list")
            {
                List<dynamic> list = JsonConvert.DeserializeObject<List<dynamic>>(dv.vlvalue);
                list.ForEach(li => dt.Columns.Add());
                dt.Rows.Add(list.ToArray());
            }
            else if (dv.vltype == "keyvaluepair")
            {
                List<KeyValuePair<dynamic, dynamic>> list = JsonConvert.DeserializeObject<List<KeyValuePair<dynamic, dynamic>>>(dv.vlvalue);
                dt.Columns.Add("Key");
                dt.Columns.Add("Value");
                //todo convert this keyvaluepair to a column in datatable dt
            }
            else
            {
                try
                {
                    dt = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                }
                catch (Exception ex1)
                {
                    string h1 = ex1.Message;

                    try
                    {
                        dt.Columns.Add();
                        dt.Rows.Add(dv.vlvalue);
                    }
                    catch (Exception ex2)
                    {
                        string h2 = ex2.Message;


                    }
                }

            }
            return JsonConvert.SerializeObject(dt);
        }
    }
    public class FilePathHandler
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="delimeter"></param>
        /// <returns></returns>
        public string cutFilePath(string path, string delimeter)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (path.ToLower().IndexOf(delimeter) > -1)
                {
                    return path = path.Remove(path.ToLower().IndexOf(delimeter));
                }
                else
                {
                    return StringHelper.ffalse;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string cutFileStringinPath(string path)
        {
            try
            {
                if (path.ToLower().IndexOf("file:///") > -1)
                {
                    return path = path.Split(new string[] { "file:///" }, StringSplitOptions.None)[1];
                }
                else
                {
                    return StringHelper.ffalse;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return ex.Message;
            }
        }
    }
    public class TypeHandler
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                    dataTable.Columns.Add(prop.Name, type);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {
                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }
                return dataTable;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return dataTable;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataSet ToDataSet<T>(IList<T> list)
        {
            DataSet ds = new DataSet();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Type elementType = typeof(T);
                DataTable t = new DataTable();
                ds.Tables.Add(t);

                //add a column to table for each public property on T
                foreach (var propInfo in elementType.GetProperties())
                {
                    Type ColType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

                    t.Columns.Add(propInfo.Name, ColType);
                }

                //go through each property on T and add each value to the table
                foreach (T item in list)
                {
                    DataRow row = t.NewRow();

                    foreach (var propInfo in elementType.GetProperties())
                    {
                        row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                    }

                    t.Rows.Add(row);
                }

                return ds;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ds;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public string StringtoJsonD(string text)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<string> rows = new List<string>();
                List<string> HeaderRow = new List<string>();

                //text = text.Replace("?", "q"); text = text.Replace('?', 'q');

                text = text.TrimStart();
                //rows
                if (text.IndexOf("\n") > -1)
                {
                    rows = text.Split(new string[] { "\n" }, StringSplitOptions.None).ToList();
                }
                DataTable dt = new DataTable();


                dt.Columns.Add(" ");
                dt.Columns.Add(rows[0]);
                foreach (string row in rows)
                {
                    DataRow workRow = dt.NewRow();

                    string val = row.Trim();
                    if (!string.IsNullOrEmpty(val))
                    {
                        workRow[0] = "";
                        workRow[1] = row;
                        dt.Rows.Add(workRow);
                    }
                }
                return JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public string StringtoJson(string text)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<string> rows = new List<string>();
                List<string> HeaderRow = new List<string>();
                text = text.TrimStart();
                //rows
                if (text.IndexOf("\n") > -1)
                {
                    rows = text.Split(new string[] { "\n" }, StringSplitOptions.None).ToList();
                }
                //HeaderRow
                if (rows[0].IndexOf(" ") > -1)
                {
                    HeaderRow = rows[0].Split(new string[] { " ", "  ", "   ", "  " }, StringSplitOptions.None).ToList();
                }
                HeaderRow.ForEach(x => x.Trim()); HeaderRow.RemoveAll(x => x == string.Empty);


                DataTable dt = new DataTable(); dt.Columns.Add(" ");
                foreach (string head in HeaderRow)
                {
                    dt.Columns.Add(head);
                }

                foreach (string row in rows)
                {
                    List<string> rower = new List<string>();
                    rower = row.Split(new string[] { " ", "  ", "   ", "  " }, StringSplitOptions.None).ToList();
                    rower.ForEach(x => x.Trim()); rower.RemoveAll(x => x == string.Empty);
                    rower.Insert(0, "");
                    dt.Rows.Add(rower.ToArray());
                }
                return JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public bool IsValidJson(string strInput)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                strInput = strInput.Trim();
                if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                    (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
                {
                    try
                    {
                        var obj = JToken.Parse(strInput);
                        return true;
                    }
                    catch (JsonReaderException jex)
                    {
                        //Exception in parsing json
                        new Logger().LogException(jex);
                        return false;
                    }
                    catch (Exception ex) //some other exception
                    {
                        new Logger().LogException(ex);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public bool IsFormattedString(string strInput)
        {
            try
            {
                strInput = strInput.Trim();
                if (strInput.StartsWith("{") && strInput.EndsWith("}")) //For object
                {
                    try
                    {
                        return true;
                    }
                    catch (Exception ex) //some other exception
                    {
                        new Logger().LogException(ex);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="formattedstring"></param>
        /// <returns></returns>
        public string ConvertFormattedString(string formattedstring,string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string returns = string.Empty;

                List<string> varslist1 = formattedstring.Split('}').ToList();
                varslist1.RemoveAll(x => x.Trim() == string.Empty);
                string newString = formattedstring;
                List<string> varslist2 = new List<string>();

                foreach (string varsin in varslist1)
                {
                    varslist2.Add(varsin.Split('{')[1].Trim());
                }

                StringBuilder sr = new StringBuilder(newString);

                foreach (string varsin in varslist2)
                {
                    DVariables value = new VariableHandler().getVariables(varsin, executionID);
                    if (value != null && value.vlvalue.GetType() == typeof(string))
                    {
                        sr.Replace("{" + varsin + "}", value.vlvalue);
                    }
                }

                returns = sr.ToString();
                return returns;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="inputtype"></param>
        /// <returns></returns>
        public DataTable getInput(string input, string inputtype, string ExecutionID)
        {
            DataTable returnValue = new DataTable();

            if (inputtype.Trim().ToLower() == StringHelper.value)
            {
                List<string> SemiColonParts = new List<string>();
                SemiColonParts = input.Split(';').ToList();
                SemiColonParts.RemoveAll(co => co.Trim() == string.Empty);
                SemiColonParts.ForEach(co => returnValue.Columns.Add(co));

                List<string> ValueCells = new List<string>();
                foreach (string semicolonpart in SemiColonParts)
                {
                    List<string> ColonParts = new List<string>();
                    ColonParts = semicolonpart.Split(':').ToList();

                    for (int colpart = 0; colpart < ColonParts.Count; colpart++)
                    {
                        ValueCells.Add(getInput(ColonParts[colpart], ExecutionID).Rows[0][0].ToString());
                    }

                    
                }
                returnValue.Rows.Add(ValueCells.ToArray());

                foreach (var l in Variables.dynamicvariables)
                {
                    if ((l.vlname == input) && (l.vlstatus == StringHelper.truee))
                    {
                        l.vlvalue = JsonConvert.SerializeObject(returnValue);
                    }
                }
            }
            else if (inputtype.Trim().ToLower() == StringHelper.input)
            {
                List<string> SemiColonParts = new List<string>();
                SemiColonParts = input.Split(';').ToList();
                SemiColonParts.RemoveAll(co => co.Trim() == string.Empty);
                SemiColonParts.ForEach(co => returnValue.Columns.Add(co));

                List<string> ValueCells = new List<string>();
                //int counter = 0;
                foreach (string semicolonpart in SemiColonParts)
                {
                    List<string> ColonParts = new List<string>();
                    ColonParts = semicolonpart.Split(':').ToList();
                    
                    for (int colpart = 0; colpart < ColonParts.Count; colpart++)
                    {
                        ValueCells.Add(getInput(ColonParts[colpart], ExecutionID).Rows[0][0].ToString());
                    }


                    //if (counter == 0)
                    //{
                    //    for (int colpart=0;colpart<ColonParts.Count;colpart++)
                    //    {
                    //        returnValue.Columns.Add();
                    //        ColonParts[colpart] = getInput(ColonParts[colpart], ExecutionID).Rows[0][0].ToString();
                    //    }
                    //    returnValue.Rows.Add(ColonParts.ToArray());
                    //    counter = ColonParts.Count;
                    //}
                    //else if (counter == ColonParts.Count)
                    //{
                    //    returnValue.Rows.Add(ColonParts.ToArray());
                    //}
                }
                returnValue.Rows.Add(ValueCells.ToArray());

                foreach (var l in Variables.dynamicvariables)
                {
                    if ((l.vlname == input) && (l.vlstatus == StringHelper.truee))
                    {
                        l.vlvalue = JsonConvert.SerializeObject(returnValue);
                    }
                }
            }
            else if (inputtype.Trim().ToLower() == "reference")
            {
                returnValue.Columns.Add("Output");
                returnValue.Rows.Add(input);
            }
            else if (inputtype.Trim().ToLower() == StringHelper.variable)
            {
                VariableHandler vh = new VariableHandler();
                if (vh.checkVariableExists(input, ExecutionID))
                {
                    DVariables dv = vh.getVariables(input, ExecutionID);
                    if (dv.vltype != null && dv.vltype.ToLower() == "datatable")
                    {
                        returnValue = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                    }
                    else
                    {
                        if ((dv.vlvalue.StartsWith("{") && dv.vlvalue.EndsWith("}")) || (dv.vlvalue.StartsWith("[") && dv.vlvalue.EndsWith("]")))
                        {
                            returnValue = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                        }
                        else
                        {
                            string val = dv.vlvalue;
                            returnValue.Columns.Add("Output");
                            returnValue.Rows.Add(val);
                            foreach (var l in Variables.dynamicvariables)
                            {
                                if ((l.vlname == input) && (l.vlstatus == StringHelper.truee))
                                {
                                    l.vlvalue = JsonConvert.SerializeObject(returnValue);
                                }
                            }
                        }
                    }
                }
                else if (input.IndexOf("[") > -1 && input.IndexOf("]") > -1)
                {
                    string returns = string.Empty; DataTable inpt = new DataTable();
                    string itemName = input.Split('[')[0].Trim();
                    string itemNumber = input.Split('[')[1].Split(']')[0].Trim().Replace("\"", "").Replace("'", "");
                    returnValue.Columns.Add("Output");

                    if (new VariableHandler().checkVariableExists(itemName, ExecutionID))
                    {
                        DVariables dvs = new DVariables();
                        inpt = getInput(itemName, "variable", ExecutionID);

                        int y = 0;
                        if (int.TryParse(itemNumber, out y))
                        {
                            returns = inpt.Rows[y][0].ToString();
                        }
                        else
                        {
                            returns = getColumnAsListFromDataTable(inpt, itemNumber);
                            dynamic res = JsonConvert.DeserializeObject<dynamic>(JsonConvert.DeserializeObject<List<dynamic>>(returns)[0]);
                            for (int h = 2; h < input.Split('[').Length; h++)
                            {
                                itemNumber = input.Split('[')[h].Split(']')[0].Trim().Replace("\"", "").Replace("'", "");
                                res = ReturnDynamicKeyValue(res, itemNumber);
                            }
                            int r = 0;
                            try
                            {
                                while (r < res.Count)
                                {
                                    returnValue.Rows.Add(res[r]);
                                    r++;
                                }
                            }
                            catch (Exception e)
                            {
                                string ex = e.Message;
                                if (res != null)
                                    returnValue.Rows.Add(res.Value);
                            }
                        }
                    }
                    else
                    {
                        returns = input;
                    }
                }
                else
                {
                    returnValue.Columns.Add("Output");
                    returnValue.Rows.Add(input);
                }
            }
            return returnValue;
        }
        public string getInputJSON(string input, string ExecutionID)
        {
            string inp = input;
            try
            {
                if (input.Contains("+{") && input.Contains("}+"))
                {
                    string[] inputList = input.Split(new[] { "+{", "}+" }, StringSplitOptions.None);
                    input = "";
                    foreach (var l in inputList)
                    {
                        DataTable dt = getInput(l,"variable", ExecutionID);
                        foreach (DataRow dr in dt.Rows)
                        {
                            input += dr[0].ToString();
                        }
                    }
                    return input;
                }
                else
                    return inp;
            }
            catch(Exception e)
            {
                string ex = e.Message;
                return inp;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 
        public DataTable getInput(string input, string ExecutionID)
        {
            DataTable returnValue = new DataTable();
            VariableHandler vh = new VariableHandler();
            if ((input.IndexOf("{") > -1 && input.IndexOf("}") > -1))
            {
                if (vh.checkVariableExists(input, ExecutionID))
                {
                    DVariables dv = vh.getVariables(input, ExecutionID);
                    if (dv.vltype != null && dv.vltype.ToLower() == "datatable")
                    {
                        returnValue = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                    }
                    else
                    {
                        if ((dv.vlvalue.StartsWith("{") && dv.vlvalue.EndsWith("}")) || (dv.vlvalue.StartsWith("[") && dv.vlvalue.EndsWith("]")))
                        {
                            returnValue = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                        }
                        else
                        {
                            string val = dv.vlvalue;
                            returnValue.Columns.Add("Output");
                            returnValue.Rows.Add(val);
                            foreach (var l in Variables.dynamicvariables)
                            {
                                if ((l.vlname == input) && (l.vlstatus == StringHelper.truee))
                                {
                                    l.vlvalue = JsonConvert.SerializeObject(returnValue);
                                }
                            }
                        }
                    }
                }
                else if ((input.IndexOf("[") > -1 && input.IndexOf("]") > -1))
                {
                    List<string> SplitInputs = input.Split(']').ToList();
                    List<string> SelectColumn = new List<string>();
                    SelectColumn = SplitInputs[0].Split('[').ToList();
                    string inputv = SelectColumn[0].ToString() + "}";
                    if (vh.checkVariableExists(inputv, ExecutionID))
                    {
                        if (vh.getVariables(inputv, ExecutionID).vltype.ToLower() == "datatable")
                        {
                            DVariables dv = vh.getVariables(inputv, ExecutionID);
                            if (dv.vltype != null && dv.vltype.ToLower() == "datatable")
                            {
                                returnValue = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                            }
                        }
                        else
                        {
                            returnValue.Columns.Add("Output");
                            returnValue.Rows.Add(input);
                            foreach (var l in Variables.dynamicvariables)
                            {
                                if ((l.vlname == input) && (l.vlstatus == StringHelper.truee))
                                {
                                    l.vlvalue = JsonConvert.SerializeObject(returnValue);
                                }
                            }
                        }
                    }
                }
                else
                {
                    returnValue.Columns.Add("Output");
                    returnValue.Rows.Add(input);
                    foreach (var l in Variables.dynamicvariables)
                    {
                        if ((l.vlname == input) && (l.vlstatus == StringHelper.truee))
                        {
                            l.vlvalue = JsonConvert.SerializeObject(returnValue);
                        }
                    }
                }
            }
            else
            {
                returnValue.Columns.Add("Output");
                returnValue.Rows.Add(input);
                foreach (var l in Variables.dynamicvariables)
                {
                    if ((l.vlname == input) && (l.vlstatus == StringHelper.truee))
                    {
                        l.vlvalue = JsonConvert.SerializeObject(returnValue);
                    }
                }
            }
            return returnValue;
        }
        public dynamic ReturnDynamicKeyValue(dynamic res, dynamic key)
        {
            try
            {
                res = res[Convert.ToInt32(key)];
                return res;
            }
            catch (Exception e)
            {
                string ex = e.Message;
                return res[key];
            }
        }
        /// <summary>
        /// Get a Value for a given Variable 
        /// </summary>
        /// <param name="input">Variable</param>
        /// <param name="isColumn">Whether to Extract a 1D Datatable from 2D Datatable based on Column when true or Row when False</param>
        /// <returns></returns>
        public DataTable getInputValue(string input, bool isColumn,string executionID)
        {
            DataTable returnValue = new DataTable();
            VariableHandler vh = new VariableHandler();
            StringFunction sh = new StringFunction();

            input = input.Trim();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (input.IndexOf("{") > -1 && input.IndexOf("}") > -1) //variable 
                {
                    if (input.IndexOf("[") > -1 && input.IndexOf("]") > -1) //indexed variable {a[p]} / {a[p][q]}
                    {
                        int squaresopencount = Regex.Matches(input, "[[]").Count;
                        int squaresclosecount = Regex.Matches(input, "[]]").Count;
                        int flowersopencount = Regex.Matches(input, "[{]").Count;
                        int flowersclosecount = Regex.Matches(input, "[}]").Count;

                        if (squaresclosecount == squaresopencount && flowersclosecount == flowersopencount)
                        {
                            int squarestart = input.IndexOf("["); int squarestop = input.IndexOf("]");

                            string variableName = input.Substring(0, squarestart) + "}";
                            DVariables dv = vh.getVariables(variableName, executionID);

                            DataTable dt = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);

                            if (squaresopencount == 1 && squaresclosecount == 1) //indexed variable 1D {a[p]}
                            {
                                string indexName = input.Substring(squarestart); //column index
                                indexName = sh.removeString(indexName, "["); indexName = sh.removeString(indexName, "]}");

                                int index = 0;
                                if (int.TryParse(indexName, out index))
                                {

                                }

                                if (isColumn == true)
                                {
                                    returnValue.Columns.Add(dt.Columns[index].ColumnName, dt.Columns[index].DataType);

                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        returnValue.Rows.Add(dr[index]);
                                    }
                                }
                                else
                                {
                                    foreach (DataColumn dc in dt.Columns)
                                    {
                                        returnValue.Columns.Add(dc.ColumnName, dc.DataType);
                                    }

                                    returnValue.Rows.Add(dt.Rows[index]);
                                }
                            }
                            else //indexed variable 2D {a[p][q]}
                            {
                                string IndCollString = input.Substring(squarestart);

                                string rowString = IndCollString.Split(new string[] { "][" }, StringSplitOptions.None).ToList()[0];
                                string columnString = IndCollString.Split(new string[] { "][" }, StringSplitOptions.None).ToList()[1];

                                rowString = sh.removeString(rowString, "[");
                                columnString = sh.removeString(columnString, "]"); columnString = sh.removeString(columnString, "}");

                                int row = 0;
                                int column = 0;

                                if (int.TryParse(rowString, out row))
                                {
                                    if (int.TryParse(columnString, out column))
                                    {

                                    }
                                }

                                returnValue.Columns.Add(dt.Columns[column].ColumnName, dt.Columns[column].DataType);

                                returnValue.Rows.Add(dt.Rows[row][column]);
                            }
                        }
                        else //improper variable
                        {

                        }
                    }
                    else //unindexed variable {a}
                    {
                        string variableName = input;
                        DVariables dv = vh.getVariables(variableName, executionID);
                        returnValue = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                    }
                }
                else //value a
                {
                    returnValue.Columns.Add("Content");
                    returnValue.Rows.Add(input);
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return null;
            }


        }
        /// <summary>
        /// Convert a DataTable with single Column(1D) to DataTable with Multiple Columns(2D) Based on Seperator
        /// </summary>
        /// <param name="input">Variable</param>
        /// <param name="seperator">Seperator</param>
        /// <param name="inputColumnName">ColumnName in input to be extracted as 1D List Datatable</param>
        /// <param name="ResultColumnNames">Resultant Datatable ColumnNames</param>
        /// <returns></returns>
        public DataTable getInputValue(string input, string seperator, string inputColumnName, string executionID, params string[] ResultColumnNames)
        {
            DataTable returnValue = new DataTable();
            VariableHandler vh = new VariableHandler();
            StringFunction sh = new StringFunction();

            input = input.Trim();

            if (input.IndexOf("{") > -1 && input.IndexOf("}") > -1) //variable 
            {
                if (input.IndexOf("[") > -1 && input.IndexOf("]") > -1) //indexed variable {a[p]}
                {
                    int squaresopencount = Regex.Matches(input, "[[]").Count;
                    int squaresclosecount = Regex.Matches(input, "[]]").Count;
                    int flowersopencount = Regex.Matches(input, "[{]").Count;
                    int flowersclosecount = Regex.Matches(input, "[}]").Count;

                    if (squaresclosecount == squaresopencount && flowersclosecount == flowersopencount)
                    {
                        int squarestart = input.IndexOf("["); int squarestop = input.IndexOf("]");

                        string variableName = input.Substring(0, squarestart) + "}";

                        DVariables dv = vh.getVariables(variableName, executionID);
                        DataTable dt = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);

                        if (squaresopencount == 1 && squaresclosecount == 1) //indexed variable 1D {a[p]}
                        {
                            string indexName = input.Substring(squarestart); //column index
                            indexName = sh.removeString(indexName, "["); indexName = sh.removeString(indexName, "]}");

                            int index = 0;
                            if (int.TryParse(indexName, out index))
                            {

                            }


                            List<string> dtls = new List<string>(); //get datatable column as List
                            if(inputColumnName.Trim() != string.Empty)
                            {
                                dtls = dt.AsEnumerable().Select(x => x[inputColumnName].ToString()).ToList();
                            }
                            else
                            {
                                dtls = dt.AsEnumerable().Select(x => x[0].ToString()).ToList();
                            }

                            List<string> row = new List<string>();
                            foreach (string dtl in dtls) //build a 2D DataTable with a List based on Seperator
                            {
                                if (dtl.Trim() != seperator)
                                {
                                    row.Add(dtl);
                                }
                                else
                                {
                                    if (ResultColumnNames != null && ResultColumnNames.Length > 0)
                                    {
                                        ResultColumnNames.ToList().ForEach(cn => returnValue.Columns.Add(cn));
                                        if (ResultColumnNames.Length == row.Count)
                                        {
                                            returnValue.Rows.Add(row.ToArray());
                                        }
                                    }
                                    else
                                    {
                                        row.ForEach(cn => returnValue.Columns.Add());
                                        if (returnValue.Columns.Count == row.Count)
                                        {
                                            returnValue.Rows.Add(row.ToArray());
                                        }
                                    }
                                    row.Clear();
                                }
                            }
                        }
                    }
                }
            }
            return returnValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }
            foreach (T entity in list)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity);
                }

                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<PropertyInfo> FlattenType(Type type)
        {
            var properties = type.GetProperties();
            foreach (PropertyInfo info in properties)
            {
                if (info.PropertyType.Assembly.GetName().Name == "mscorlib")
                {
                    yield return info;
                }
                else
                {
                    foreach (var childInfo in FlattenType(info.PropertyType))
                    {
                        yield return childInfo;
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        public string cDataTableToString(DataTable dataTable)
        {
            if (dataTable != null)
            {
                var output = new StringBuilder();
                foreach (DataRow row in dataTable.Rows)
                {
                    for (int i = 0; i < dataTable.Columns.Count; i++)
                    {
                        output.AppendLine(dataTable.Columns[i].ColumnName + ":" + row[i]);
                    }
                    output.AppendLine();
                }
                return output.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        public string ConvertDataTableToString(DataTable dataTable)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (dataTable != null)
                {
                    var output = new StringBuilder();

                    var columnsWidths = new int[dataTable.Columns.Count];

                    // Get column widths
                    foreach (DataRow row in dataTable.Rows)
                    {
                        for (int i = 0; i < dataTable.Columns.Count; i++)
                        {
                            var length = row[i].ToString().Length;
                            if (columnsWidths[i] < length)
                                columnsWidths[i] = length;
                        }
                    }

                    // Get Column Titles
                    for (int i = 0; i < dataTable.Columns.Count; i++)
                    {
                        var length = dataTable.Columns[i].ColumnName.Length;
                        if (columnsWidths[i] < length)
                            columnsWidths[i] = length;
                    }

                    // Write Column titles
                    for (int i = 0; i < dataTable.Columns.Count; i++)
                    {
                        var text = dataTable.Columns[i].ColumnName;
                        output.Append("|" + PadCenter(text, columnsWidths[i] + 2));
                    }
                    output.Append("|\n" + new string('=', output.Length) + "\n");

                    // Write Rows
                    foreach (DataRow row in dataTable.Rows)
                    {
                        for (int i = 0; i < dataTable.Columns.Count; i++)
                        {
                            var text = row[i].ToString();
                            output.Append("|" + PadCenter(text, columnsWidths[i] + 2));
                        }
                        output.Append("|\n");
                    }
                    return output.ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return null;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        private string PadCenter(string text, int maxLength)
        {
            int diff = maxLength - text.Length;
            return new string(' ', diff / 2) + text + new string(' ', (int)(diff / 2.0 + 0.5));

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemArray"></param>
        /// <returns></returns>
        public string getItemfromArray(string itemArray, string ExecutionID)
        {
            string returns = string.Empty; DataTable inpt = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try {
                if (itemArray.IndexOf("[") > -1 && itemArray.IndexOf("]") > -1)
                {
                    string itemName = itemArray.Split('[')[0].Trim();
                    string itemNumber = itemArray.Split('[')[1].Split(']')[0].Trim().Replace("\"", "").Replace("'", "");

                    if (new VariableHandler().checkVariableExists(itemName, ExecutionID))
                    {
                        DVariables dvs = new DVariables();
                        inpt = new TypeHandler().getInput(itemName, StringHelper.variable, ExecutionID);

                        int y = 0;
                        if (int.TryParse(itemNumber, out y))
                        {
                            returns = inpt.Rows[y][0].ToString();
                        }
                        else
                        {
                            returns = getColumnAsListFromDataTable(inpt, itemNumber);
                        }
                    }
                    else
                    {
                        returns = itemArray;
                    }
                }
                else
                {
                    if (new VariableHandler().checkVariableExists(itemArray, ExecutionID))
                    {
                        returns = JsonConvert.SerializeObject(new TypeHandler().getInput(itemArray, StringHelper.variable, ExecutionID));
                    }
                    else
                    {
                        returns = itemArray;
                    }
                }
                return returns;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public DataTable ListtoDatatable(List<string> list)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("0");
            foreach (var l in list)
            {
                dataTable.Rows.Add(l);
            }

            return dataTable;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="InputColumn"></param>
        /// <returns></returns>
        public DataTable OneDConversionsVariable(string input, string InputColumn, string ExecutionID)
        {
            DataTable returnValue = new DataTable();
            DataTable dtr = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                dtr = getInput(input, ExecutionID);
                List<string> final = new List<string>();
                if (InputColumn != null)
                {
                    foreach (DataRow dr in dtr.Rows)
                    {
                        try
                        {
                            int n;
                            if (int.TryParse(InputColumn, out n))
                                final.Add(dr[Convert.ToInt32(InputColumn)].ToString());
                            else
                            {
                                if (new VariableHandler().checkVariableExists(InputColumn, ExecutionID))
                                {
                                    string t = new VariableHandler().getVariables(InputColumn, ExecutionID).vlvalue;
                                    if (t.Contains("{"))
                                    {
                                        DataTable dt = JsonConvert.DeserializeObject<DataTable>(t);
                                        int iter = Convert.ToInt32(dt.Rows[0][0]);
                                        final.Add(dr[iter].ToString());
                                    }
                                    else
                                    {
                                        int iter = Convert.ToInt32(t);
                                        final.Add(dr[iter].ToString());
                                    }
                                }
                                else
                                {
                                    final.Add(dr[InputColumn].ToString());
                                }
                            }
                        }
                        catch(Exception e)
                        {
                            string ex = e.Message;
                            final.Add(dr[InputColumn].ToString());
                        }

                 
                    }

                    returnValue = ListtoDatatable(final);
                }
                else
                {
                    foreach (DataRow dr in dtr.Rows)
                    {
                        final.Add(dr[0].ToString());
                    }

                    returnValue = ListtoDatatable(final);
                }
            }
            catch (Exception e)
            {
                string y = e.Message;
                new Logger().LogException(e);
                msgs.Add(new MethodResultHandler().createMessage(e.Message, StringHelper.exceptioncode));
            }
            return returnValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="InputColumn"></param>
        /// <returns></returns>
        public DataTable OneDConversionsVariableforRow(string input, string InputColumn, string ExecutionID)
        {
            DataTable returnValue = new DataTable();
            DataTable dtr = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                dtr = getInput(input, ExecutionID);
                List<string> final = new List<string>();

                foreach(DataColumn dc in dtr.Columns)
                {
                    returnValue.Columns.Add(dc.ColumnName, dc.DataType);
                }

                if (InputColumn != null)
                {
                    int n;
                    if (int.TryParse(InputColumn, out n))
                        returnValue.Rows.Add(dtr.Rows[n].ItemArray);
                    else
                    {
                        if (new VariableHandler().checkVariableExists(InputColumn, ExecutionID))
                        {
                            string t = new VariableHandler().getVariables(InputColumn, ExecutionID).vlvalue;
                            if (t.Contains("{"))
                            {
                                DataTable dt = JsonConvert.DeserializeObject<DataTable>(t);
                                int iter = Convert.ToInt32(dt.Rows[0][0]);
                                returnValue.Rows.Add(dtr.Rows[iter].ItemArray);
                            }
                            else
                            {
                                int iter = Convert.ToInt32(t);
                                returnValue.Rows.Add(dtr.Rows[iter].ItemArray);
                            }
                        }
                        else
                        {
                            returnValue.Rows.Add(dtr.Rows[Convert.ToInt32(InputColumn)].ItemArray);
                        }
                    }
                }
                else
                {
                    returnValue.Rows.Add(dtr.Rows[0].ItemArray);
                }
            }
            catch (Exception e)
            {
                string y = e.Message;
                new Logger().LogException(e);
                msgs.Add(new MethodResultHandler().createMessage(e.Message, StringHelper.exceptioncode));
            }
            return returnValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="inputRow"></param>
        /// <param name="inputColumn"></param>
        /// <returns></returns>
        public DataTable TwoDScalarConversionVariable(string input, string inputRow, string inputColumn, string ExecutionID)
        {
            DataTable returnValue = new DataTable();
            DataTable dtr = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                dtr = getInput(input, ExecutionID);
                List<string> final = new List<string>();
                if (inputColumn != null && inputRow != null)
                {
                    if (new VariableHandler().checkVariableExists(inputRow, ExecutionID) && new VariableHandler().checkVariableExists(inputColumn, ExecutionID))
                    {
                        string trow = new VariableHandler().getVariables(inputRow, ExecutionID).vlvalue;
                        string tcolumn = new VariableHandler().getVariables(inputColumn, ExecutionID).vlvalue;

                        int row = 0;
                        if (trow.Contains("{"))
                        {
                            DataTable dt = JsonConvert.DeserializeObject<DataTable>(trow);
                            row = Convert.ToInt32(dt.Rows[0][0]);
                        }
                        else
                        {
                            row = Convert.ToInt32(trow);
                        }

                        int column = 0;
                        if (tcolumn.Contains("{"))
                        {
                            DataTable dt = JsonConvert.DeserializeObject<DataTable>(tcolumn);
                            column = Convert.ToInt32(dt.Rows[0][0]);
                        }
                        else
                        {
                            column = Convert.ToInt32(tcolumn);
                        }
                        if (dtr.Rows.Count > 0 && isPermittedDataTable(row, column, dtr))
                        {
                            final.Add(dtr.Rows[row][column].ToString());
                        }
                    }
                    else if (new VariableHandler().checkVariableExists(inputRow, ExecutionID))
                    {
                        string trow = new VariableHandler().getVariables(inputRow, ExecutionID).vlvalue;

                        int row = 0;
                        if (trow.Contains("{"))
                        {
                            DataTable dt = JsonConvert.DeserializeObject<DataTable>(trow);
                            row = Convert.ToInt32(dt.Rows[0][0]);
                        }
                        else
                        {
                            row = Convert.ToInt32(trow);
                        }

                        if(dtr.Rows.Count > 0 && isPermittedDataTable(row, Int32.Parse(inputColumn), dtr))
                        {
                            final.Add(dtr.Rows[row][Int32.Parse(inputColumn)].ToString());
                        }
                    }
                    else if (new VariableHandler().checkVariableExists(inputColumn, ExecutionID))
                    {
                        string tcolumn = new VariableHandler().getVariables(inputColumn, ExecutionID).vlvalue;

                        int column = 0;
                        if (tcolumn.Contains("{"))
                        {
                            DataTable dt = JsonConvert.DeserializeObject<DataTable>(tcolumn);
                            column = Convert.ToInt32(dt.Rows[0][0]);
                        }
                        else
                        {
                            column = Convert.ToInt32(tcolumn);
                        }
                        if (dtr.Rows.Count > 0 && isPermittedDataTable(Int32.Parse(inputRow), column, dtr))
                        {
                            final.Add(dtr.Rows[Int32.Parse(inputRow)][tcolumn].ToString());
                        }
                    }
                    else
                    {
                        if (dtr.Rows.Count > 0 && isPermittedDataTable(Int32.Parse(inputRow), Int32.Parse(inputColumn), dtr))
                        {
                            final.Add(dtr.Rows[Int32.Parse(inputRow)][Int32.Parse(inputColumn)].ToString());
                        }
                    }
                }
                else
                {
                    if (dtr.Rows.Count > 0 && isPermittedDataTable(0, 0, dtr))
                    {
                        final.Add(dtr.Rows[0][0].ToString());
                    }
                }
                returnValue = ListtoDatatable(final);
            }
            catch (Exception e)
            {
                string ex = e.Message;
                new Logger().LogException(e);
                msgs.Add(new MethodResultHandler().createMessage(e.Message, StringHelper.exceptioncode));
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool isPermittedDataTable(int row, int column, DataTable dt)
        {
            if(row < dt.Rows.Count && column < dt.Columns.Count)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="inputRow"></param>
        /// <returns></returns>
        public DataTable OneDScalarConversionVariable(string input, string inputRow, string ExecutionID)
        {
            DataTable returnValue = new DataTable();
            DataTable dtr = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                dtr = getInput(input, ExecutionID);
                List<string> final = new List<string>();
                if (inputRow != null)
                {
                    if (new VariableHandler().checkVariableExists(inputRow, ExecutionID))
                    {
                        string tcolumn = new VariableHandler().getVariables(inputRow, ExecutionID).vlvalue;

                        int row = 0;
                        if (tcolumn.Contains("{"))
                        {
                            DataTable dt = JsonConvert.DeserializeObject<DataTable>(tcolumn);
                            row = Convert.ToInt32(dt.Rows[0][0]);
                        }
                        else
                        {
                            row = Convert.ToInt32(tcolumn);
                        }
                        final.Add(dtr.Rows[row][0].ToString());
                    }
                }
                else
                {
                    final.Add(dtr.Rows[0][0].ToString());
                }
                returnValue = ListtoDatatable(final);
            }
            catch (Exception e)
            {
                string y = e.Message;
                new Logger().LogException(e);
                msgs.Add(new MethodResultHandler().createMessage(e.Message, StringHelper.exceptioncode));
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="TwoDValue"></param>
        /// <returns></returns>
        //public DataTable OneD2DConversionVariable(string input, string TwoDValue, string ExecutionID)
        //{
        //    DataTable returnValue = new DataTable();
        //    DataTable returnValue1 = new DataTable();
        //    DataTable dtr = new DataTable();
        //    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
        //    try
        //    {
        //        dtr = getInput(input, ExecutionID);
        //        List<string> final = new List<string>();
        //        foreach (DataRow dr in dtr.Rows)
        //        {
        //            final.Add(dr[0].ToString());
        //        }
        //        int count = 0;
        //        foreach (var k in final)
        //        {
        //            count++;

        //            if (count > Int32.Parse(TwoDValue))
        //            {
        //                DataColumnCollection columns = returnValue.Columns;
        //                if (columns.Contains("0"))
        //                {
        //                    returnValue.Rows.Add(k);
        //                }
        //                else
        //                {
        //                    returnValue.Columns.Add("0");
        //                    returnValue.Rows.Add(k);
        //                }
        //            }
        //            else
        //            {
        //                DataColumnCollection columns = returnValue1.Columns;
        //                if (columns.Contains("0"))
        //                {
        //                    returnValue1.Rows.Add(k);
        //                }
        //                else
        //                {
        //                    returnValue1.Columns.Add("0");
        //                    returnValue1.Rows.Add(k);
        //                }
        //            }
        //        }
        //        returnValue1 = returnValue.Clone();
        //    }
        //    catch (Exception e)
        //    {
        //        string ex = e.Message;
        //        new Logger().LogException(e);
        //        msgs.Add(new MethodResultHandler().createMessage(e.Message, StringHelper.exceptioncode));
        //    }
        //    return returnValue1;
        //}

        public DataTable OneD2DConversionVariable(string input, string TwoDValue, string ExecutionID)
        {
            DataTable dtr = new DataTable();
            DataTable dataTable = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                dtr = getInput(input, ExecutionID);
                List<string> final = new List<string>();
                List<string> val = new List<string>();
                val = TwoDValue.Split(new string[] { "," }, StringSplitOptions.None).ToList();
                int row = Convert.ToInt32(val[0]);
                int col = Convert.ToInt32(val[1]);
                for (int i = 0; i < col; i++)
                {
                    dataTable.Columns.Add("");
                }
                int k = 0;
                foreach (DataRow dr in dtr.Rows)
                {
                    final.Add(dr[0].ToString());
                }
                //int count = 0;


                for (int i = 0; i < row; i++)
                {
                    object[] o = new object[col];
                    int j = 0;
                    for (j = 0; j < col; j++)
                    {
                        o[j] = final[k];
                        k++;
                    }
                    dataTable.Rows.Add(o);
                    o.Clone();
                }
            }
            catch (Exception e)
            {
                string ex = e.Message;
                new Logger().LogException(e);
                msgs.Add(new MethodResultHandler().createMessage(e.Message, StringHelper.exceptioncode));
            }
            return dataTable;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        /// 
        public string getColumnAsListFromDataTable(DataTable dt, string columnName)
        {
            List<string> returns = new List<string>();
            returns.AddRange(dt.AsEnumerable().Select(r => r.Field<string>(columnName)).ToList());

            return JsonConvert.SerializeObject(returns);
        }
    }
    public class ValidationHandler
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <returns></returns>
        public List<string> ExtractEmails(string Text)
        {
            List<string> outputlist = new List<string>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Regex emailRegex = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase);
                MatchCollection emailMatches = emailRegex.Matches(Text);
                StringBuilder sb = new StringBuilder();
                foreach (Match emailMatch in emailMatches)
                {
                    sb.AppendLine(emailMatch.Value);
                    outputlist.Add(emailMatch.Value);
                }
                return outputlist;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return outputlist;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <returns></returns>
        public List<string> ExtractPhoneNumbers(string Text)
        {
            List<string> output = new List<string>();
            try
            {
                return output;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return output;
            }
        }

    }
    public class StepPropertyHandler
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propName"></param>
        /// <param name="step"></param>
        /// <returns></returns>
        public string getPropertiesValue(string propName, Steps step)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string propValue = string.Empty;
                if (step.StepProperties != null)
                {
                    List<StepProperties> props = step.StepProperties.FindAll(s => s.StepProperty.Trim().ToLower() == propName.ToLower());
                    if (props.Count > 0)
                    {
                        return props[0].StepPropertyValue;
                    }
                    else
                    {
                        props = step.StepProperties.FindAll(s => Regex.Replace(s.StepProperty.Trim().ToLower(), @"\s", "") == Regex.Replace(propName.ToLower(), @"\s", ""));
                        if (props.Count > 0)
                        {
                            return props[0].StepPropertyValue;
                        }
                        else
                        {
                            return string.Empty;
                        }
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propName"></param>
        /// <param name="step"></param>
        /// <param name="propAttr"></param>
        /// <returns></returns>
        public StepProperties getPropertiesValue(string propName, Steps step, string propAttr)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string propValue = string.Empty;
                if (step.StepProperties != null)
                {
                    List<StepProperties> props = step.StepProperties.FindAll(s => s.StepProperty.ToLower() == propName.ToLower());
                    if (props.Count > 0)
                    {
                        return props[0];
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="properties"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> getPropertiesValue(List<StepProperties> properties)
        {
            string propValue = string.Empty;
            List<KeyValuePair<string, string>> lkvp = new List<KeyValuePair<string, string>>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (properties != null)
                {
                    properties.ForEach(prop => lkvp.Add(new KeyValuePair<string, string>(prop.StepProperty, prop.StepPropertyValue)));
                }
                else
                {

                }
                return lkvp;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return lkvp;
            }
        }

        public string getExceptionMessage(Steps step,string ex)
        {
            return "-SE" + step.Element_Id + "A" + step.Action_Id + "EX" + ex;
        }

        public class StepPropertyValidationHandler
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="step"></param>
            /// <returns></returns>
            public bool ValidateProperties(Steps step)
            {
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    if (step.StepProperties != null)
                    {
                        List<StepProperties> props = step.StepProperties.FindAll(s => s.StepPropertyValue.ToLower().Trim() == string.Empty);
                        props.ForEach(d => new Logger().LogException(Messages.EmptyStepProperty.Replace("---", d.StepProperty).Replace(StringHelper.star, step.Name)));
                        props.ForEach(d => new ExecutionLog().add2Log(Messages.EmptyStepProperty.Replace("---", d.StepProperty).Replace(StringHelper.star, step.Name), step.Robot_Id, step.Name, StringHelper.errorcode,false));
                        if (props.Count > 0)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    return true;
                }
            }
            public enum PasswordScore
            {
                Blank = 0,
                VeryWeak = 1,
                Weak = 2,
                Medium = 3,
                Strong = 4,
                VeryStrong = 5
            }
            public static PasswordScore CheckStrength(string password)
            {
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                int score = 0;

                if (password.Length < 1)
                    return PasswordScore.Blank;
                if (password.Length < 4)
                    return PasswordScore.VeryWeak;

                if ((password.Length >= 4) && (password.Length < 8))
                    score++;
                if (password.Length >= 8)
                    score++;

                Regex rg = new Regex(@"^[a-zA-Z0-9\s,]*$");
                if (Regex.Match(password, @"^[0-9]*$", RegexOptions.ECMAScript).Success)
                    score++;
                if (Regex.Match(password, @"^[a-z]*$", RegexOptions.ECMAScript).Success ||
                  Regex.Match(password, @"^[A-Z]*$", RegexOptions.ECMAScript).Success)
                    score++;
                if (Regex.Match(password, @"/.[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]/", RegexOptions.ECMAScript).Success)
                    score++;
                if (Regex.Match(password, @"^[a-zA-Z09!@#$%^&*(),]*$", RegexOptions.ECMAScript).Success)
                    return PasswordScore.VeryStrong;
                return (PasswordScore)score;
            }
            private bool ValidateEmail(string mailid)
            {
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(mailid);
                if (match.Success)
                    return true;
                else
                    return false;
            }
            public List<StepProperties> ValidateProperties(Steps step, bool returner)
            {
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    if (step.StepProperties != null)
                    {
                        List<StepProperties> stepProps = step.StepProperties;
                        List<StepProperties> props = new List<StepProperties>();
                        string actionid = step.Action_Id;

                        List<ElementProperties> actProps = JsonConvert.DeserializeObject<List<ElementProperties>>(new Classes.Action().getActionProperty(actionid));
                        List<ValidationTypes> validation = JsonConvert.DeserializeObject<List<ValidationTypes>>(new Classes.Action().getValidations());

                        foreach (StepProperties sp in stepProps)
                        {
                            foreach (ElementProperties actProp in actProps)
                            {
                                if (sp.Order == actProp.Order)
                                {
                                    if (actProp.CustomValidation != null)
                                    {
                                        string[] values = actProp.CustomValidation.Split(',');
                                        foreach (var i in values)
                                        {
                                            List<ValidationTypes> validate = validation.FindAll(s => s.ID == i);
                                            if (validate.Count > 0)
                                            {

                                                switch (validate[0].ValidationName.ToLower())
                                                {
                                                    case "requiredvalidator":
                                                        if (sp.StepPropertyValue.Trim() == string.Empty)
                                                        {
                                                            props.Add(sp);
                                                        }
                                                        break;
                                                    case "emailvalidator":
                                                        if (!ValidateEmail(sp.StepPropertyValue.Trim()))
                                                        {
                                                            props.Add(sp);
                                                        }
                                                        break;
                                                    case "passwordvalidator":

                                                        var ps = CheckStrength(sp.StepPropertyValue.Trim());
                                                        string scor = ps.ToString().ToLower();
                                                        if ((scor == "blank") || (scor == "veryweak") || (scor == "weak"))
                                                        {
                                                            props.Add(sp);
                                                        }
                                                        break;
                                                }

                                            }

                                        }
                                    }
                                }
                            }
                        }

                        props.ForEach(d => new Logger().LogException(Messages.EmptyStepProperty.Replace(StringHelper.dashedlines, d.StepProperty).Replace(StringHelper.star, step.Name)));
                        props.ForEach(d => new ExecutionLog().add2Log(Messages.EmptyStepProperty.Replace(StringHelper.dashedlines, d.StepProperty).Replace(StringHelper.star, step.Name), step.Robot_Id, step.Name, StringHelper.errorcode,false));

                        return props;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    return null;
                }
            }
        }
    }
    public class EncoderDecoderBase64
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public string Base64Encode(string plainText)
        {
            if (plainText == null || plainText.Trim() == string.Empty)
            {
                plainText = string.Empty;
            }
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="base64EncodedData"></param>
        /// <returns></returns>
        public string Base64Decode(string base64EncodedData)
        {
            if (base64EncodedData == null || base64EncodedData.Trim() == string.Empty)
            {
                base64EncodedData = string.Empty;
            }
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool IsBase64String(string value)
        {
            if (value == null || value.Length == 0 || value.Length % 4 != 0 || value.Contains(' ') || value.Contains('\t') || value.Contains('\r') || value.Contains('\n'))
                return false;
            var index = value.Length - 1;
            if (value[index] == '=')
                index--;
            if (value[index] == '=')
                index--;
            for (var i = 0; i <= index; i++)
                if (IsInvalid(value[i]))
                    return false;
            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private bool IsInvalid(char value)
        {
            var intValue = (Int32)value;
            if (intValue >= 48 && intValue <= 57)
                return false;
            if (intValue >= 65 && intValue <= 90)
                return false;
            if (intValue >= 97 && intValue <= 122)
                return false;
            return intValue != 43 && intValue != 47;
        }
    }
    public class ClassHandler
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public string getMethods(Type type)
        {
            //List<MethodInfo> methodsList = Type.GetType(type).GetMethods().ToList();
            List<MethodInfo> methodsList = type.GetMethods().ToList();
            List<MethodDefinition> methods = new List<MethodDefinition>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                foreach (MethodInfo method in methodsList)
                {
                    List<ParameterInfo> paramss = method.GetParameters().ToList();
                    List<string> paras = new List<string>();
                    if (paramss.Count > 0)
                        paramss.ForEach(p => paras.Add(p.ParameterType + "  " + p.Name));
                    MethodDefinition md = new MethodDefinition();
                    md.Name = method.Name;

                    if (method.ReturnType.Name.ToLower().IndexOf("list") > -1)
                    {
                        md.ReturnType = "List<" + method.ReturnType.GenericTypeArguments[0] + ">";
                    }
                    else if (method.ReturnType.Name.ToLower().IndexOf("dictionary") > -1)
                    {
                        md.ReturnType = "Dictionary<" + method.ReturnType.GenericTypeArguments[0] + "," + method.ReturnType.GenericTypeArguments[1] + " > ";
                    }
                    else
                    {
                        md.ReturnType = method.ReturnType.Name;
                    }

                    var attrs = method.GetCustomAttributesData().ToList();

                    string route = string.Empty;
                    string httptype = string.Empty;

                    //a => a.GetCustomAttributes(typeof(WebInvokeAttribute), true)

                    foreach (var ttr in attrs)
                    {
                        if (ttr.NamedArguments.Count > 0)
                        {
                            foreach (var attru in ttr.NamedArguments)
                            {
                                if (attru.MemberName.ToLower() == "uritemplate")
                                {
                                    route = attru.TypedValue.Value.ToString();
                                }
                                else if (attru.MemberName.ToLower() == "method")
                                {
                                    httptype = attru.TypedValue.Value.ToString();
                                }
                            }
                        }
                    }

                    if (type.IsInterface)
                    {
                        // We get the current assembly through the current class
                        var currentAssembly = this.GetType().GetTypeInfo().Assembly;

                        // we filter the defined classes according to the interfaces they implement
                        List<TypeInfo> iAssemblies = currentAssembly.DefinedTypes.Where(typer => typer.ImplementedInterfaces.Any(inter => inter == type)).ToList();

                        string Classes = string.Empty;
                        Classes = String.Join(", ", iAssemblies.Select(x => x.Name));
                        md.ClassName = Classes;
                    }
                    else
                    {
                        md.ClassName = type.Name;
                    }

                    md.Parameters = string.Join(" , ", paramss);
                    md.Route = route;
                    md.Flag = httptype;

                    methods.Add(md);
                }
                return JsonConvert.SerializeObject(methods);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return null;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="className"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string InvokeMethod(string methodName, string className, string obj)
        {
            string result = string.Empty;
            //Assembly assembly = this.GetType().GetTypeInfo().Assembly;
            Type typo = Type.GetType("Genbase.services." + className);
            //Type type = assembly.GetType("services." + className);
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (typo != null)
                {
                    MethodInfo methodInfo = typo.GetMethod(methodName);

                    if (methodInfo != null)
                    {

                        List<ParameterInfo> parameters = methodInfo.GetParameters().ToList();
                        object classInstance = Activator.CreateInstance(typo, null);

                        if (parameters.Count == 0)
                        {
                            // This works fine
                            result = JsonConvert.SerializeObject(methodInfo.Invoke(classInstance, null));
                        }
                        else
                        {
                            List<string> objers = new List<string>(); List<ParameterDefinitionFactor> ldf = new List<ParameterDefinitionFactor>();
                            objers = obj.Split(',').ToList();

                            object[] parametersArray = new object[] { }; List<object> objList = new List<object>();

                            foreach (var objer in objers)
                            {
                                string name = objer.Split(':')[0].Trim();
                                string value = objer.Split(':')[1].Trim();
                                string type = parameters.FindAll(par => par.Name == name)[0].ParameterType.Name;

                                if (type.Trim() == "String")
                                {
                                    objList.Add(value);
                                }
                                else if (type.Trim().ToLower().IndexOf("int") > -1)
                                {
                                    objList.Add(int.Parse(value));
                                }
                                else if (type.Trim().ToLower().IndexOf("bool") > -1)
                                {
                                    objList.Add(bool.Parse(value));
                                }
                            }


                            parametersArray = objList.ToArray();

                            // The invoke does NOT work;
                            // it throws "Object does not match target type"             
                            result = JsonConvert.SerializeObject(methodInfo.Invoke(classInstance, parametersArray));
                        }
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }

        }


        public class MethodDefinition
        {
            public string Name { get; set; }
            public string ReturnType { get; set; }
            public string Parameters { get; set; }
            public string Route { get; set; }
            public string Flag { get; set; }
            public string Description { get; set; }
            public string ClassName { get; set; }
        }
        public class ParameterDefinitionFactor
        {
            public string Name { get; set; }
            public string Type { get; set; }
            public string Value { get; set; }
        }
    }
    public class CurrentWindowsSystemFacts
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DetailsList getSystemDetails()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                DetailsList dl = new DetailsList();
                dl.Username = Environment.UserName;
                dl.MacAddress = NetworkInterface.GetAllNetworkInterfaces().Where(nic => nic.OperationalStatus == OperationalStatus.Up && nic.NetworkInterfaceType != NetworkInterfaceType.Loopback).Select(nic => nic.GetPhysicalAddress().ToString()).FirstOrDefault();

                dl.IPAddress = string.Empty;

                foreach (var ip in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        dl.IPAddress = ip.ToString();
                    }
                }
                dl.SystemName = System.Environment.MachineName;


                return dl;
            }
            catch (Exception ex)
            {
                string ht = ex.Message;
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SERVICENAME"></param>
        /// <returns></returns>
        public string thisSystemServices(string SERVICENAME)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                ServiceController sc = new ServiceController(SERVICENAME);

                switch (sc.Status)
                {
                    case ServiceControllerStatus.Running:
                        return StringHelper.Running;
                    case ServiceControllerStatus.Stopped:
                        return StringHelper.Stopped;
                    case ServiceControllerStatus.Paused:
                        return StringHelper.Paused;
                    case ServiceControllerStatus.StopPending:
                        return StringHelper.Stopping;
                    case ServiceControllerStatus.StartPending:
                        return StringHelper.Starting;
                    default:
                        return StringHelper.StatusChanging;
                }
            }
            catch (Exception ex)
            {
                string htr = ex.Message;
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return "No Such Service Exists";
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SERVICENAME"></param>
        /// <param name="MACHINENAME"></param>
        /// <returns></returns>
        public string remoteSystemServices(string SERVICENAME, string MACHINENAME)
        {
            try
            {
                ServiceController sc = new ServiceController(SERVICENAME, MACHINENAME);
                switch (sc.Status)
                {
                    case ServiceControllerStatus.Running:
                        return StringHelper.Running;
                    case ServiceControllerStatus.Stopped:
                        return StringHelper.Stopped;
                    case ServiceControllerStatus.Paused:
                        return StringHelper.Paused;
                    case ServiceControllerStatus.StopPending:
                        return StringHelper.Stopping;
                    case ServiceControllerStatus.StartPending:
                        return StringHelper.Starting;
                    default:
                        return StringHelper.StatusChanging;
                }
            }
            catch (Exception ex)
            {
                string htr = ex.Message;
                return "No Such Service Exists";
            }
        }
        public class DetailsList
        {
            public string Username { get; set; }
            public string MacAddress { get; set; }
            public string IPAddress { get; set; }
            public string SystemName { get; set; }
        }
    }
    public class HostedSitesList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetHostName()
        {
            return Dns.GetHostEntry(Dns.GetHostName()).HostName;
        }
    }
    public class DvariableHandler
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool CheckForBraces(string input)
        {
            if ((input.IndexOf("{") > -1 && input.IndexOf("}") > -1))
            {
                if (subStringCount(input, "{") == subStringCount(input, "}"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool CheckForComma(string input)
        {
            if (input.IndexOf(",") > -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="MainString"></param>
        /// <param name="match"></param>
        /// <returns></returns>
        public int subStringCount(string MainString, string match)
        {
            StringBuilder sb = new StringBuilder();
            int index = 0;
            do
            {
                index = MainString.IndexOf(match, index);
                if (index != -1)
                {
                    sb.Append(match);
                    index++;
                }
            } while (index != -1);

            string repeats = sb.ToString();
            return index;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public DataTable getValueFromVariable(string input, string ExecutionID)
        {
            DataTable returnValue = new DataTable();
            VariableHandler vh = new VariableHandler();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (vh.checkVariableExists(input, ExecutionID))
                {
                    DVariables dv = vh.getVariables(input, ExecutionID);
                    if (dv.vltype != null && dv.vltype.ToLower() == "datatable")
                    {
                        returnValue = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                    }
                    else
                    {
                        if ((dv.vlvalue.StartsWith("{") && dv.vlvalue.EndsWith("}")) || (dv.vlvalue.StartsWith("[") && dv.vlvalue.EndsWith("]")))
                        {
                            returnValue = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                        }
                        else
                        {
                            string val = dv.vlvalue;
                            returnValue.Columns.Add("Output");
                            returnValue.Rows.Add(val);
                            foreach (var l in Variables.dynamicvariables)
                            {
                                if ((l.vlname == input) && (l.vlstatus == StringHelper.truee))
                                {
                                    l.vlvalue = JsonConvert.SerializeObject(returnValue);
                                }
                            }
                        }
                    }
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool CheckForSquares(string input)
        {
            if (input.IndexOf("[") > -1 && input.IndexOf("]") > -1)
            {
                if (subStringCount(input, "[") == subStringCount(input, "]"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool CheckForColon(string input)
        {
            if (input.IndexOf(":") > -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool CheckForSemiColon(string input)
        {
            if (input.IndexOf(";") > -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public DataTable ConvertColonAndSemicolon(string input)
        {
            DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (input.IndexOf(";") > -1)
                {
                    List<string> SemiColonParts = new List<string>();
                    SemiColonParts = input.Split(';').ToList();
                    int counter = 0;
                    foreach (string semicolonpart in SemiColonParts)
                    {
                        List<string> ColonParts = new List<string>();
                        ColonParts = semicolonpart.Split(':').ToList();


                        if (counter == 0)
                        {
                            foreach (var colpart in ColonParts)
                            {
                                returnValue.Columns.Add();
                            }
                            returnValue.Rows.Add(ColonParts.ToArray());
                            counter = ColonParts.Count;
                        }
                        else if (counter == ColonParts.Count)
                        {
                            returnValue.Rows.Add(ColonParts.ToArray());
                        }

                    }

                }
                return returnValue;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public List<string> GetListFromDataTable(DataTable dataTable, string column)
        {
            List<string> NewList = new List<string>();
            if (dataTable != null)
            {
                NewList.AddRange(dataTable.AsEnumerable().Select(r => r.Field<string>(column)).ToList());
            }
            return NewList;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public string GetCellFromDataTable(DataTable dataTable, string column, string row)
        {
            string result = string.Empty;

            if (column.Trim() != string.Empty && row.Trim() != string.Empty)
            {
                result = dataTable.Rows[int.Parse(row)].Field<string>(column);
            }

            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetBracePositionData(string input)
        {
            List<KeyValuePair<string, string>> positions = new List<KeyValuePair<string, string>>();

            positions.AddRange(AllIndexesOf(input, "{"));
            positions.AddRange(AllIndexesOf(input, "}"));


            return positions;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static List<KeyValuePair<string, string>> AllIndexesOf(string str, string value)
        {
            if (String.IsNullOrEmpty(value))
            {

            }
            List<KeyValuePair<string, string>> indexes = new List<KeyValuePair<string, string>>();
            for (int index = 0; ; index += value.Length)
            {
                index = str.IndexOf(value, index);
                if (index == -1)
                    return indexes;
                indexes.Add(new KeyValuePair<string, string>(value, index.ToString()));
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> ArrangeBracePositionData(string input)
        {
            List<KeyValuePair<string, string>> positions = new List<KeyValuePair<string, string>>();
            return positions;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        public string DataTableSerialize(DataTable dataTable)
        {
            string result = JsonConvert.SerializeObject(dataTable);
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public DataTable DataTableDeserialize(string input)
        {
            DataTable returnValue = new DataTable();
            returnValue = JsonConvert.DeserializeObject<DataTable>(input);
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public DVariables Varibles(string input)
        {
            DVariables Variables = new DVariables();
            Variables = JsonConvert.DeserializeObject<List<DVariables>>(input)[0];
            return Variables;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="datatable"></param>
        /// <returns></returns>
        public string DataTableStringified(DataTable datatable)
        {
            string result = string.Empty;
            return result;
        }
        /// <summary>
        /// used for getting and replacing variables in string which is parameterized 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public string ParameterizedStringConvert(string input)
        {
            string result = string.Empty;



            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public string FullParameterizedIndexedStringCount(string input)
        {
            string result = string.Empty;
            return result;
        }
    }

    public class SCPTransfer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="localFilePath"></param>
        /// <param name="remoteFilePath"></param>
        /// <returns></returns>
        public bool UploadWinSCP(string host, string username, string password, string localFilePath, string remoteFilePath)
        {
            bool truer = true;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                using (ScpClient client = new ScpClient(host, username, password))
                {
                    client.Connect();

                    using (Stream localFile = File.OpenRead(localFilePath))
                    {
                        client.Upload(localFile, remoteFilePath);
                    }

                    client.Disconnect();
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                string exp = ex.Message;
                truer = false;
            }
            return truer;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="localFilePath"></param>
        /// <param name="remoteFilePath"></param>
        /// <returns></returns>
        public bool DownloadWinSCP(string host, string username, string password, string localFilePath, string remoteFilePath)
        {
            bool truer = true;
            try
            {
                using (ScpClient client = new ScpClient(host, username, password))
                {
                    client.Connect();

                    using (Stream localFile = File.Create(localFilePath))
                    {
                        client.Download(remoteFilePath, localFile);
                    }

                    client.Disconnect();
                }
            }
            catch (Exception ex)
            {
                string exp = ex.Message;
                truer = false;
            }
            return truer;
        }
    }
    public class SFTPTransfer
    {
        /*private string ftpPathSrcFolder = "/Path/Source/";//path should end with /
        private string ftpPathDestFolder = "/Path/Archive/";//path should end with /
        private string ftpServerIP = "Target IP";
        private int ftpPortNumber = 80;//change to appropriate port number
        private string ftpUserID = "FTP USer Name";//
        private string ftpPassword = "FTP Password";*/
        /// <summary>
        /// Move first file from one source folder to another. 
        /// Note: Modify code and add a for loop to move all files of the folder
        /// </summary>
        public void MoveFolderToArchive(string ftpServerIP, int ftpPortNumber, string ftpUserID, string ftpPassword, string ftpPathSrcFolder, string ftpPathDestFolder, bool isMulti)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (isMulti)
                {
                    using (SftpClient sftp = new SftpClient(ftpServerIP, ftpPortNumber, ftpUserID, ftpPassword))
                    {
                        List<SftpFile> RemoteFiles = sftp.ListDirectory(ftpPathSrcFolder).ToList();
                        foreach (SftpFile eachRemoteFile in RemoteFiles)
                        {
                            if (eachRemoteFile.IsRegularFile)//Move only file
                            {
                                bool eachFileExistsInArchive = CheckIfRemoteFileExists(sftp, ftpPathDestFolder, eachRemoteFile.Name);

                                //MoveTo will result in error if filename alredy exists in the target folder. Prevent that error by cheking if File name exists
                                string eachFileNameInArchive = eachRemoteFile.Name;

                                if (eachFileExistsInArchive)
                                {
                                    eachFileNameInArchive = eachFileNameInArchive + "_" + DateTime.Now.ToString("MMddyyyy_HHmmss");//Change file name if the file already exists
                                }
                                eachRemoteFile.MoveTo(ftpPathDestFolder + eachFileNameInArchive);
                            }
                        }
                    }
                }
                else
                {
                    using (SftpClient sftp = new SftpClient(ftpServerIP, ftpPortNumber, ftpUserID, ftpPassword))
                    {
                        SftpFile eachRemoteFile = sftp.ListDirectory(ftpPathSrcFolder).First();//Get first file in the Directory            
                        if (eachRemoteFile.IsRegularFile)//Move only file
                        {
                            bool eachFileExistsInArchive = CheckIfRemoteFileExists(sftp, ftpPathDestFolder, eachRemoteFile.Name);

                            //MoveTo will result in error if filename alredy exists in the target folder. Prevent that error by cheking if File name exists
                            string eachFileNameInArchive = eachRemoteFile.Name;

                            if (eachFileExistsInArchive)
                            {
                                eachFileNameInArchive = eachFileNameInArchive + "_" + DateTime.Now.ToString("MMddyyyy_HHmmss");//Change file name if the file already exists
                            }
                            eachRemoteFile.MoveTo(ftpPathDestFolder + eachFileNameInArchive);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
            }

        }

        /// <summary>
        /// Checks if Remote folder contains the given file name
        /// </summary>
        public bool CheckIfRemoteFileExists(SftpClient sftpClient, string remoteFolderName, string remotefileName)
        {
            bool isFileExists = sftpClient
                                .ListDirectory(remoteFolderName)
                                .Any(
                                        f => f.IsRegularFile &&
                                        f.Name.ToLower() == remotefileName.ToLower()
                                    );
            return isFileExists;
        }

    }
    public class Tokenizer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="posId"></param>
        /// <returns></returns>
        public string getPOSText(string posId)
        {
            string json = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory+"/PythonScripts/pos.json");
            Dictionary<string, string> PartsOfSpeech = JsonConvert.DeserializeObject<Dictionary<string,string>>(json);
            try
            {
                return PartsOfSpeech[posId];
            }
            catch(Exception e)
            {
                string ex = e.Message;
                return posId;
            }
        }
        private char[] delimiters_keep_digits = new char[] {
            '{', '}', '(', ')', '[', ']', '>', '<','-', '_', '=', '+',
            '|', '\\', ':', ';', ' ', ',', '.', '/', '?', '~', '!',
            '@', '#', '$', '%', '^', '&', '*', ' ', '\r', '\n', '\t' };

        // This will discard digits 
        private char[] delimiters_no_digits = new char[] {
            '{', '}', '(', ')', '[', ']', '>', '<','-', '_', '=', '+',
            '|', '\\', ':', ';', ' ', ',', '.', '/', '?', '~', '!',
            '@', '#', '$', '%', '^', '&', '*', ' ', '\r', '\n', '\t',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

        /// <summary>
        ///  Tokenizes text into an array of words, using whitespace and
        ///  all punctuation as delimiters.
        /// </summary>
        /// <param name="text"> the text to tokenize</param>
        /// <returns> an array of resulted tokens</returns>
        public string[] GreedyTokenize(string text)
        {
            char[] delimiters = new char[] {
            '{', '}', '(', ')', '[', ']', '>', '<','-', '_', '=', '+',
            '|', '\\', ':', ';', ' ', ',', '.', '/', '?', '~', '!',
            '@', '#', '$', '%', '^', '&', '*', ' ', '\r', '\n', '\t' };

            return text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
        }


        /// <summary>
        ///  Tokenizes a text into an array of words, using whitespace and
        ///  all punctuation except the apostrophe "'" as delimiters. Digits
        ///  are handled based on user choice.
        /// </summary>
        /// <param name="text"> the text to tokenize</param>
        /// <param name="keepDigits"> true to keep digits; false to discard digits.</param>
        /// <returns> an array of resulted tokens</returns>
        public string[] Tokenize(string text, bool keepDigits)
        {
            string[] tokens = null;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {


                if (keepDigits)
                    tokens = text.Split(delimiters_keep_digits, StringSplitOptions.RemoveEmptyEntries);
                else
                    tokens = text.Split(delimiters_no_digits, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < tokens.Length; i++)
                {
                    string token = tokens[i];

                    // Change token only when it starts and/or ends with "'" and the 
                    // toekn has at least 2 characters. 
                    if (token.Length > 1)
                    {
                        if (token.StartsWith("'") && token.EndsWith("'"))
                            tokens[i] = token.Substring(1, token.Length - 2); // remove the starting and ending "'" 

                        else if (token.StartsWith("'"))
                            tokens[i] = token.Substring(1); // remove the starting "'" 

                        else if (token.EndsWith("'"))
                            tokens[i] = token.Substring(0, token.Length - 1); // remove the last "'" 
                    }
                }

                return tokens;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return tokens;
            }

        }

        /// <summary>
        ///  Tokenizes a text into an array of words, using whitespace and
        ///  all punctuation except the apostrophe "'" as delimiters. Digits
        ///  are handled based on user choice.
        /// </summary>
        /// <param name="filePaht"> the path of the file to tokenize to tokenize</param>
        /// <param name="keepDigits"> true to keep digits; false to discard digits.</param>
        /// <returns> an array of resulted tokens</returns>
        public string[] TokenizeFile(string filePath, bool keepDigits)
        {
            string[] tokens = null;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (string.IsNullOrEmpty(filePath))
                    return null;

                if (!(new FileInfo(filePath)).Exists)
                    return null;



                try
                {
                    string text = File.ReadAllText(filePath);
                    tokens = Tokenize(text, keepDigits);
                }
                catch
                {
                }

                return tokens;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return tokens;
            }
        }
    }
    public static class Tool
    {
        private static Tokenizer tokenizer = new Tokenizer();

        /// <summary>
        ///  Splits an n-letter word into a string array. The array's size will be (n-1) * 2. 
        ///  For example, 'carried', a 7-letter word, will be split into a string array
        ///  containing the following (7-1) * 2 = 12 elements:
        ///  {c, arried, ca, rried, car, ried, carr, ied, carri, ed, carri, d}.
        /// </summary>
        /// <param name="word"> the word to split</param>
        /// <returns> all pairs of strings which the word can be split into</returns>
        public static string[] SplitWord(string word)
        {
            int arrSize = (word.Length - 1) * 2;
            string[] strArr = new string[arrSize];
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                //The word must have at least 2 letters 
                if (word.Length < 2)
                    return null;
                int index = 0;

                for (int cutPosition = 1; cutPosition < word.Length; cutPosition++)
                {
                    strArr[index] = word.Substring(0, cutPosition);
                    strArr[index + 1] = word.Substring(cutPosition, word.Length - cutPosition);

                    //Since we collect two elements per time, we need to increase the index by 2. 
                    index += 2;
                }
                return strArr;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return strArr;
            }
        }

        /// <summary>
        ///  Create a string-integer dictionary out of a linked list of tokens.
        /// </summary>
        /// <param name="tokens">  the tokens to create the frequency table. For this
        ///  method, there is no difference between List and LinkedList types. </param>
        private static Dictionary<string, int> BuildFreqTable(LinkedList<string> tokens)
        {
            Dictionary<string, int> token_freq_table = new Dictionary<string, int>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                foreach (string token in tokens)
                {
                    if (token_freq_table.ContainsKey(token))
                        token_freq_table[token]++;
                    else
                        token_freq_table.Add(token, 1);
                }

                return token_freq_table;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return token_freq_table;
            }
        }

        /// <summary>
        ///  Make a string-integer dictionary out of an array of words.
        /// </summary>
        /// <param name="words"> the words out of which to make the dictionary</param>
        /// <returns> a string-integer dictionary</returns>
        public static Dictionary<string, int> ToStrIntDict(string text)
        {
            if (text == null)
                return null;

            string[] words = tokenizer.Tokenize(text, false);
            Dictionary<string, int> dict = new Dictionary<string, int>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                foreach (string word in words)
                {
                    // if the word is in the dictionary, increment its freq. 
                    if (dict.ContainsKey(word))
                    {
                        dict[word]++;
                    }
                    // if not, add it to the dictionary and set its freq = 1 
                    else
                    {
                        dict.Add(word, 1);
                    }
                }

                return dict;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return dict;
            }
        }

        /// <summary>
        ///  Sort a string-int dictionary by its entries' values.
        /// </summary>
        /// <param name="strIntDict"> a string-int dictionary to sort</param>
        /// <param name="sortOrder"> one of the two enumerations: Ascending and Descending</param>
        /// <returns> a string-integer dictionary sorted by integer values</returns>
        public static Dictionary<string, int> ListWordsByFreq(Dictionary<string, int> strIntDict, bool isDescending)
        {
            Dictionary<string, int> dictByFreq = new Dictionary<string, int>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                // Copy keys and values to two arrays 
                string[] words = new string[strIntDict.Keys.Count];
                strIntDict.Keys.CopyTo(words, 0);

                int[] freqs = new int[strIntDict.Values.Count];
                strIntDict.Values.CopyTo(freqs, 0);

                //Sort by freqs: it sorts the freqs array, but it also rearranges 
                //the words array's elements accordingly (not sorting) 
                Array.Sort(freqs, words);

                // If sort order is descending, reverse the sorted arrays. 
                if (isDescending)
                {
                    //reverse both arrays 
                    Array.Reverse(freqs);
                    Array.Reverse(words);
                }

                //Copy freqs and words to a new Dictionary<string, int> 


                for (int i = 0; i < freqs.Length; i++)
                {
                    dictByFreq.Add(words[i], freqs[i]);
                }

                return dictByFreq;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return dictByFreq;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static double GetTypeTokenRatio(string text)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                double typeTokenRatio = 0.0;

                // Tokenize text into tokens, no digits or punctuations. 
                string[] tokens = tokenizer.Tokenize(text, false);

                // dump array of words into a HashSet of string.  
                HashSet<string> types = new HashSet<string>();

                // HashSet ignores duplicated elements which ensures for us that duplicated words be counted only once. 
                foreach (string token in tokens)
                {
                    types.Add(token);
                }

                // A sanity check: if types set is empty, set typeTokenRatio = double.NaN, i.e. Not a Number.  
                // Otherwise, we'll get a "divided by 0" Exception. 

                if (types.Count == 0)
                {
                    typeTokenRatio = double.NaN;
                }
                else
                {
                    // Be very aware that you need to cast either types.Count or tokens.Length into  
                    // double type; otherwise you'll always get 0 as the result. 
                    typeTokenRatio = (double)types.Count / tokens.Length;
                }

                return typeTokenRatio;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return 1;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static double GetFileTypeTokenRatio(string filePath)
        {
            try
            {
                return GetTypeTokenRatio(File.ReadAllText(filePath));
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return 1;
            }
        }

        /// <summary>
        ///  Computes the average type token ratio of an array of tokens, based on the window size.
        ///  Algorithm:
        ///  If windowSize >= tokens.Length, average type token ratio is the general type token
        ///  ratio of all tokens.
        /// </summary>
        /// <param name="tokens"> the array of the tokens to calculate the average type token ratio</param>
        /// <param name="windowSize"> the number of the tokens per window</param>
        private static double GetAverageTypeTokenRatio(string filePath, int windowSize)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                LinkedList<string> movingWindow = new LinkedList<string>();
                string[] tokens = tokenizer.TokenizeFile(filePath, false);

                int index = 0;
                while (index < windowSize)
                {
                    movingWindow.AddLast(tokens[index]);
                    index++;
                }

                // Build frequency table of this window of tokens 
                Dictionary<string, int> movingFreqTable = BuildFreqTable(movingWindow);

                // This type token ratio keeps changing 
                double finalTTR = (double)movingFreqTable.Count / movingWindow.Count;

                int windowCount = 1;

                // Now index stops at windowSize position of the tokens. 
                while (index < tokens.Length)
                {
                    // Check the first token of the moving window of tokens and remove it from the moving window. 
                    string firstToken = movingWindow.First.Value;
                    movingWindow.RemoveFirst();

                    // Check its frequency in the frequency table. If it is 1, it means that this token 
                    // occurs in the moving window only once, so we can safely remove it from the moving 
                    // window; otherwise, it appears more than once, so we cannot delete it but we can 
                    // reduce its frequency by 1. 
                    if (movingFreqTable[firstToken] == 1)
                        movingFreqTable.Remove(firstToken);
                    else
                        movingFreqTable[firstToken]--;

                    // Find the next available token. If it is in the moving frequency table, increase its 
                    // frequency value by 1; otherwise, add it as a new entry and set its frequency to 1. 
                    string newToken = tokens[index];

                    if (movingFreqTable.ContainsKey(newToken))
                        movingFreqTable[newToken]++;
                    else
                        movingFreqTable.Add(newToken, 1);

                    // Add this word to the moving window so that the window always has the same number of tokens. 
                    movingWindow.AddLast(newToken);

                    // Re-compute the type token ratio of this changed window. 
                    double thisTTR = (double)movingFreqTable.Count / windowSize;

                    // Add this new type token ratio to the final type token ratio. 
                    finalTTR += thisTTR;

                    // Update index position and window counters 
                    index++;
                    windowCount++;
                }

                // We need to divided the final type token ratio by the number of windows 
                finalTTR = finalTTR / windowCount;

                return finalTTR;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return 1;
            }
        }
    }
    public class StringFunction
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseString"></param>
        /// <param name="subString"></param>
        /// <returns></returns>
        public int countSubstring(string baseString, string subString)
        {
            return Regex.Matches(baseString, subString).Count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseString"></param>
        /// <param name="splitString"></param>
        /// <returns></returns>
        public List<string> splitString2List(string baseString, string splitString)
        {
            return baseString.Split(new string[] { splitString }, StringSplitOptions.None).ToList();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseString"></param>
        /// <param name="subString"></param>
        /// <returns></returns>
        public bool isSubString(string baseString, string subString)
        {
            return baseString.IndexOf(subString) > -1;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseString"></param>
        /// <param name="subString"></param>
        /// <returns></returns>
        public string removeString(string baseString, string subString)
        {
            int index = baseString.IndexOf(subString, StringComparison.Ordinal);
            return (index < 0)
                ? baseString
                : baseString.Remove(index, subString.Length);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="json"></param>
        public void printKeysAndValues(string json)
        {
            var jobject = (JObject)((JArray)JsonConvert.DeserializeObject(json))[0];

            List<KeyValuePair<object, object>> kvp = new List<KeyValuePair<object, object>>();

            foreach (var jproperty in jobject.Properties())
            {
                //Console.WriteLine("{0} - {1}", jproperty.Name, jproperty.Value);
                kvp.Add(new KeyValuePair<object, object>(jproperty.Name, jproperty.Value));
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string getFileNameFromPath(string path)
        {
            List<string> filenameparts = new StringFunction().splitString2List(path, "\\");
            string filename = filenameparts[filenameparts.Count - 1];
            return filename;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string getApplicationFolderPath()
        {
            string path = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)));
            path = path.Substring(6);
            return path;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string getApplicationRootFolderPath()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="datetime"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public string getDateInFormat(DateTime datetime, string format)
        {
            string result = string.Empty;
            try
            {
                result = datetime.ToString(format);
            }
            catch(Exception ex)
            {
                string exceptionresult = ex.Message;
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="seperator1"></param>
        /// <param name="seperator2"></param>
        /// <returns></returns>
        public List<string> getStringPart(string Text, string seperator1, string seperator2)
        {
            List<string> finds = new List<string>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                int counter = 0;

                foreach (string sampling in Text.Split(new string[] { seperator1 }, StringSplitOptions.None))
                {
                    if (counter == 0)
                    {

                    }
                    else
                    {
                        Regex reg = new Regex("[;\\\\/:*?\"<>|&']");
                        string str = reg.Replace(sampling, string.Empty);

                        //split at spaces

                        string result = str.Split(new string[] { seperator2, "\n", "\\n" }, StringSplitOptions.None)[0].Trim();
                        result = result.Split(' ')[0];
                        finds.Add(result);
                    }
                    counter++;
                }
            }
            catch(Exception ex)
            {
                string message = ex.Message;
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                finds.Add(message);
            }

            return finds;
        }

        public string getRandomString(string length)
        {
            Random random = new Random();
            string validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            int l = 0; var nonceString = new StringBuilder();
            try
            {
                if (int.TryParse(length, out l))
                {
                    
                }
                else
                {
                    l = 32;
                }

                for (int i = 0; i < int.Parse(length); i++)
                {
                    nonceString.Append(validChars[random.Next(0, validChars.Length - 1)]);
                }

                return nonceString.ToString();
            }
            catch(Exception ex)
            {
                string ext = ex.Message;
                return ext;
            }
        }

        public string getParameterizedString(string input, string executionID)
        {
            string output = input;
            
            if(input.IndexOf("{") > -1 && input.IndexOf("}") > -1)
            {
                List<DVariables> dvars = Variables.dynamicvariables.FindAll(d => d.ExecutionID == executionID);
                //dvars.RemoveAll(dvar => dvar.vlname.Trim() != string.Empty);

                foreach(DVariables dvar in dvars)
                {
                    if (dvar.vlname.Trim() != string.Empty || dvar.vlname.Trim () != "" || dvar.vlname.Trim().ToString().Contains("{"))
                    {
                        if (input.IndexOf(dvar.vlname) > -1)
                        {
                            DVariables der = new VariableHandler().getVariables(dvar.vlname, executionID);

                            if (der.vltype == "string")
                            {
                                string vo = der.vlvalue;
                                output = output.Replace(dvar.vlname, vo);
                            }
                            else if (der.vltype == "datatable")
                            {
                                if (input.IndexOf("[") > -1 && input.IndexOf("]") > -1)
                                {
                                    try
                                    {
                                        DataTable vo = JsonConvert.DeserializeObject<DataTable>("[" + der.vlvalue + "]");
                                        int DataIndex = int.Parse(Regex.Match(input, @"\[([^)]*)\]").Groups[1].Value);
                                        output = output.Replace("[" + DataIndex + "]", "");
                                        // output = output.Replace(dvar.vlname, vo.Rows[0][DataIndex].ToString());
                                        string str1 = string.Empty;
                                        str1 = vo.Rows[0][DataIndex].ToString();
                                        List<string> mailids = new List<string>();
                                        mailids = str1.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                                        string output1 = string.Empty;
                                        for (int i = 0; i < mailids.Count; i++)
                                        {
                                            output1 += "'" + mailids[i] + "'";
                                            if (i != mailids.Count - 1)
                                                output1 += ",";
                                        }
                                        output = output.Replace(dvar.vlname, output1);
                                        output = output.Replace("'(", "(");
                                        output = output.Replace(")'", ")");
                                    }
                                    catch(Exception ex)
                                    {
                                        string ext = ex.Message;
                                        return ext;
                                    }
                                }
                                else
                                {
                                    DataTable vo = JsonConvert.DeserializeObject<DataTable>(der.vlvalue);
                                    output = output.Replace(dvar.vlname, vo.Rows[0][0].ToString());
                                }
                            }
                        }
                    }
                }

            }
            else
            {
                output = input;
            }

            return output;
        }
    }
    public class DigestAuthFixer
    {
        private static string _host;
        private static string _user;
        private static string _password;
        private static string _realm;
        private static string _nonce;
        private static string _qop;
        private static string _cnonce;
        private static DateTime _cnonceDate;
        private static int _nc;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        public DigestAuthFixer(string host, string user, string password)
        {
            // TODO: Complete member initialization
            _host = host;
            _user = user;
            _password = password;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string CalculateMd5Hash(string input)
        {
            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hash = MD5.Create().ComputeHash(inputBytes);
            var sb = new StringBuilder();
            foreach (var b in hash)
                sb.Append(b.ToString("x2"));
            return sb.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="varName"></param>
        /// <param name="header"></param>
        /// <returns></returns>
        private string GrabHeaderVar(string varName, string header)
        {
            var regHeader = new Regex(string.Format(@"{0}=""([^""]*)""", varName));
            var matchHeader = regHeader.Match(header);
            if (matchHeader.Success)
                return matchHeader.Groups[1].Value;
            throw new ApplicationException(string.Format("Header {0} not found", varName));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        private string GetDigestHeader(string dir)
        {
            _nc = _nc + 1;

            var ha1 = CalculateMd5Hash(string.Format("{0}:{1}:{2}", _user, _realm, _password));
            var ha2 = CalculateMd5Hash(string.Format("{0}:{1}", "GET", dir));
            var digestResponse = CalculateMd5Hash(string.Format("{0}:{1}:{2:00000000}:{3}:{4}:{5}", ha1, _nonce, _nc, _cnonce, _qop, ha2));

            return string.Format("Digest username=\"{0}\", realm=\"{1}\", nonce=\"{2}\", uri=\"{3}\", " + "algorithm=MD5, response=\"{4}\", qop={5}, nc={6:00000000}, cnonce=\"{7}\"", _user, _realm, _nonce, dir, digestResponse, _qop, _nc, _cnonce);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        public string GrabResponse(string dir)
        {
            var url = _host + dir;
            var uri = new Uri(url);

            var request = (HttpWebRequest)WebRequest.Create(uri);

            // If we've got a recent Auth header, re-use it!
            if (!string.IsNullOrEmpty(_cnonce) && DateTime.Now.Subtract(_cnonceDate).TotalHours < 1.0)
            {
                request.Headers.Add("Authorization", GetDigestHeader(dir));
            }

            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                // Try to fix a 401 exception by adding a Authorization header
                if (ex.Response == null || ((HttpWebResponse)ex.Response).StatusCode != HttpStatusCode.Unauthorized)
                    throw;

                var wwwAuthenticateHeader = ex.Response.Headers["WWW-Authenticate"];
                _realm = GrabHeaderVar("realm", wwwAuthenticateHeader);
                _nonce = GrabHeaderVar("nonce", wwwAuthenticateHeader);
                _qop = GrabHeaderVar("qop", wwwAuthenticateHeader);

                _nc = 0;
                _cnonce = new Random().Next(123400, 9999999).ToString();
                _cnonceDate = DateTime.Now;

                var request2 = (HttpWebRequest)WebRequest.Create(uri);
                request2.Headers.Add("Authorization", GetDigestHeader(dir));
                response = (HttpWebResponse)request2.GetResponse();
            }
            var reader = new StreamReader(response.GetResponseStream());
            return reader.ReadToEnd();
        }
    }
    public class FileDownloadHandler
    {
        public KeyValuePair<string, byte[]> getFileNameandContent(string fileName, string foldername)
        {
            string filePath = Path.Combine(foldername, fileName);

            new Logger().LogException(Environment.CurrentDirectory);
            new Logger().LogException(Directory.GetCurrentDirectory());
            new Logger().LogException(System.Reflection.Assembly.GetExecutingAssembly().Location);
            new Logger().LogException(AppDomain.CurrentDomain.BaseDirectory);
            new Logger().LogException(Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase));

            string fullPath = Path.Combine(Environment.CurrentDirectory, filePath);

            if (System.IO.File.Exists(fullPath))
            {
                return new KeyValuePair<string, byte[]>(fullPath, System.IO.File.ReadAllBytes(fullPath));
            }
            else
            {
                return new KeyValuePair<string, byte[]>(fullPath, null);
            }
        }
    }
    public class DataTableHandler
    {
        public DataTable SeperatedByRowNumber(DataTable dty, int rowNumber, int resultDatatableNumber)
        {
            DataTable resultDT = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();

            dt1 = dty.Clone();
            dt2 = dty.Clone();

            for(int h=0; h<dty.Rows.Count; h++)
            {
                if(h < rowNumber)
                {
                    List<object> list = dty.Rows[h].ItemArray.ToList();
                    dt1.Rows.Add(list.ToArray());
                }
                else
                {
                    List<object> list = dty.Rows[h].ItemArray.ToList();
                    dt2.Rows.Add(list.ToArray());
                }
            }

            if(resultDatatableNumber == 1)
            {
                resultDT = dt2.Copy();
            }
            else
            {
                resultDT = dt1.Copy();
            }

            return resultDT;
        }
        public DataTable SeperatedByRow(DataTable dty, List<object> vs, int resultDatatableNumber)
        {
            DataTable resultDT = new DataTable();
            DataSet dataSet = new DataSet();
            DataTable dataTable = new DataTable();

            int counter = 0, tableCounter = 0;
            foreach(DataRow dry in dty.Rows)
            {
                if(counter == 0)
                {
                    if(dataTable.Rows.Count > 0)
                    {
                        dataTable.TableName = tableCounter.ToString();
                        DataTable data = new DataTable();
                        //data = dataTable.Copy();
                        data = dataTable.Clone();
                        foreach (DataRow dr in dataTable.Rows)
                        {
                            data.Rows.Add(dr.ItemArray);
                        }
                        dataSet.Tables.Add(data);
                        tableCounter++;
                    }
                    dataTable.Rows.Clear();
                    dataTable.Columns.Clear();
                    dataTable.Clear();
                    foreach (DataColumn dcy in dty.Columns)
                    {
                        dataTable.Columns.Add(dcy.ColumnName, dcy.DataType);
                    }
                }

                List<object> list = dry.ItemArray.ToList();
                if(list == vs)
                {
                    counter = 0;
                }
                else
                {
                    int rowCounter = 0;
                    for(int t=0; t < vs.Count; t++)
                    {
                        if(vs[t].ToString().ToLower().Trim() == list[t].ToString().ToLower().Trim())
                        {
                            rowCounter = rowCounter + 1;
                        }
                    }

                    if(rowCounter == vs.Count)
                    {
                        counter = 0;
                    }
                    else
                    {
                        dataTable.Rows.Add(list.ToArray());
                        counter++;
                    }
                }
            }

            if (dataTable.Rows.Count > 0)
            {
                dataTable.TableName = tableCounter.ToString();
                DataTable data = new DataTable();
                //data = dataTable.Copy();
                data = dataTable.Clone();
                foreach(DataRow dr in dataTable.Rows)
                {
                    data.Rows.Add(dr.ItemArray);
                }
                dataSet.Tables.Add(data);
                tableCounter++;
            }

            if(dataSet.Tables.Count > 0)
            {
                resultDT = dataSet.Tables[resultDatatableNumber].Copy();
            }

            return resultDT;
        }
    }
}