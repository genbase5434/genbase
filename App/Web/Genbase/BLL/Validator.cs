﻿using Genbase.classes;
using Genbase.Classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Genbase.BLL
{
    public class Validator
    {
        AppExecution ape = new BLL.AppExecution();
        Step cs = new Step();
        public string ValidateStep(Steps stp, string ExecutionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Element cde = new Element();
                Classes.Action ca = new Classes.Action();
                string elemName = cde.getElementById(stp.Element_Id);
                string actName = ca.getAction(stp.Action_Id);
                Type type = Type.GetType("Genbase.BLL.ElementsClasses." + elemName);
                object instance = Activator.CreateInstance(type);
                MethodInfo method = type.GetMethod(actName);

                object[] paramss = new object[2] { stp, ExecutionID };
                MethodResult mr = new MethodResult();
                mr = (MethodResult)method.Invoke(instance, paramss);
                if (mr.result == false)
                {
                    return StringHelper.failure;
                }
                else
                {
                    return StringHelper.success;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, stp, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return StringHelper.failed;
            }
        }
        public string DebugStep(int robotId, int stepid, string ExecutionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                int Stepid = ape.getNextStep(robotId, stepid, "");
                if (Stepid <= 0)
                {
                    return string.Empty;
                }
                Variables.vDebugger.ID = Stepid.ToString();
                string vs = ape.getVersion(robotId.ToString());
                Steps step = new Steps();
                List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(cs.getSteps(robotId.ToString(), vs));
                if (steps.Count > 0)
                {
                    if (Stepid > 0)
                    {
                        step = steps.Find(c => c.Id == Stepid.ToString());
                    }
                }
                ValidateStep(step, ExecutionID);
                return Stepid.ToString();
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return StringHelper.failed;
            }
        }

        // SAP
        public MethodResult StepExecution(int robotId, int stepid, string ExecutionID)
        {
            MethodResult mr = new MethodResult();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                int Stepid = stepid;
                Variables.vDebugger.ID = Stepid.ToString();
                Variables.robotid = robotId.ToString() ;
                string vs = ape.getVersion(robotId.ToString());

                Steps step = new Steps();
                List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(cs.getSteps(robotId.ToString(), vs));
                if (steps.Count > 0)
                {
                    if (Stepid > 0)
                    {
                        step = steps.Find(c => c.Id == Stepid.ToString());
                    }
                }

                Element cde = new Element();
                Classes.Action ca = new Classes.Action();
                string elemName = cde.getElementById(step.Element_Id);
                string actName = ca.getAction(step.Action_Id);
                Type type = Type.GetType("Genbase.BLL.ElementsClasses." + elemName);
                object instance = Activator.CreateInstance(type);
                MethodInfo method = type.GetMethod(actName);

                object[] paramss = new object[2] { step, ExecutionID };

                mr = (MethodResult)method.Invoke(instance, paramss);
                return mr;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                mr.result = false;
                return mr;
            }
        }
    }
}