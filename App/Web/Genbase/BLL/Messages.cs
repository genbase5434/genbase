﻿using System.Collections.Generic;

namespace Genbase.BLL
{
    public class Messages
    {
        public static string Success = "Success";
        public static string Failure = "Failure";
        public static string Insert = "Record Inserted Successfully";
        public static string Delete = "Record Deleted Successfully";
        public static string ServerError = "There has been a Internal Server Side Error";
        public static string FieldEmpty = " field is empty in ";
        public static string stepExecutionStarted = " is being Processed";

        //Mail
        public static string loggedin = "You are logged in as ---";
        public static string unreadmails = " is authenticated successfully and read --- emails";
        public static string authenticated = " is authenticated successfully";


        //NLP
        public static string matchResultMails = "Match Case is Applied on *** mails and extracted --- mails ";
        public static string matchResultTweets = "Match Case is Applied on *** tweets and extracted --- tweets";
        public static string matchResultFiles = "Match Case is Applied on *** files and extracted --- files";
        public static string matchResultPosts = "Match Case is Applied on *** posts and extracted --- posts";

        //File
        public static string filesValidated = "--- files are validated / authenticated or filtered";
        public static string filesAssigned = "---files are assigned to the --- department";

        //HTTP
        public static string CRMAuthenticated = "Successfully Connected to ---";
        public static string postedData = "Successfully posted data to ---";
        public static string getData = "Successfully got data from ---";

        //Database
        public static string ReadData = "Available Data:";

        //StepProperty
        public static string EmptyStepProperty = "--- Property Value is Empty For Step ***.";

    }
    public class Commands
    {
        public static string getAllUsersList = "compgen -u ";

        public static string createuser = "useradd username -s /bin/false";

        /*echo username : password >> create.txt --> create user in usertextfile

        chpasswd<create.txt --> password

        rm create.txt --> remove file*/

        public static string entermysqlasroot = "mysql -p";

        public static string createdb = "create database gallery;";

        public static string grantall = "grant all privileges on gallery.* to 'boot'@'localhost' ((uname)) identified by \"GanD1do\"; ((pswd)) flush privileges;";
        public static string exit = "exit;";


        public static string createdbuser = "CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';";


        public static string usedb = "USE books;";
        public static string createtable = "CREATE TABLE authors(id INT, name VARCHAR(20), email VARCHAR(20));";
        public static string sgowtables = "SHOW TABLES;";
        public static string inserttotables = "INSERT INTO authors(id, name, email) VALUES(1,\"Vivek\",\"xuz@abc.com\");";

        public static string servicestatus = "service mysqld status";
        public static string servicestop = "service mysqld stop";
        public static string servicestart = "service mysqld start";
    }

    public class StringHelper
    {
        //Arithmetic
        public const string dictionary = "dictionary";
        public const string CSource = "Source";
        public const string CDestination = "Destination";
        public const string CInput = "Input";
        public const string COutput = "Output";
        public const string success = "Success";
        public const string input = "input";
        public const string sentimentinputtype = "Sentiment Input Type";
        public const string sentimentinputfile = "Sentiment Input File";
        public const string sentenceinputtype = "SentenceFormation Input Type";
        public const string sentenceinputfile = "SentenceFormation Input File";
        public const string matchinputfile1 = "Match Input File1";
        public const string matchinputfile2 = "Match Input File2";
        public const string tesseractInput = "Tesseract Input"; 
        public const string excelInput = "excel input";
        public const string input1 = "input 1";
        public const string input2 = "input 2";
        public const string matchinput1 = "Match Input1";
        public const string matchinput2 = "Match Input2";
        public const string matchinputtype1 = "Match Input Type1";
        public const string matchinputtype2 = "Match Input Type2";
        public const string output = "output";
        public const string pattern = "pattern";
        public const string OriginalText = "Original String";
        public const string ReplaceText = "Replace String";
        public const string seperator = "seperator";
        public const string instance = "instance";
        public const string inputtype1 = "input type 1";
        public const string inputtype2 = "input type 2";
        public const string inputtype = "inputtype";
        public const string entityinputtype = "entity input type";
        public const string classifyinputtype = "classify input type";
        public const string keyinput = "Key Input";

        public const string IsHeader = "IsHeader";
        public const string input_type = "input type";
        public const string file_type = "File Selection Type";
        public const string outputtype = "outputtype";
        public const string output_type = "output type";
        public const string contenttype = "ContentType";
        public const string message = "message";
        public const string ismodulus = "ismodulus";
        public const string value = "value";
        public const string variable = "variable";
        public const string add = "Add";
        public const string subtract = "Subtract";
        public const string multiply = "Multiply";
        public const string modulus = "Modulus";
        public const string divide = "Divide";
        public const string colon = " : ";
        public const string ConnectionType = "ConnectionType";
        public const string notificationsTable = "Notification";


        //Captcha
        public const string pageurl = "pageurl";
        public const string elementxpath = "elementxpath";

        //Database
        public const string tablename = "table name";
        public const string ColumnHeader = "ColumnHeader";
        public const string tablecolumns = "tablecolumns";
        public const string sourcecolumns = "sourcecolumns";
        public const string dynamic = "dynamic";
        public const string columnscountmismatch = "Columns Count MisMatch Between Mentioned Names";
        public const string postgresql = "postgresql";
        public const string inbox = "inbox";
        public const string singlequottednew = "'New'";
        public const string New = "New";
        public const string orders = "orders";
        public const string singlequottedapproved = "'Approved'";
        public const string mentionedcolumnsmismatch = "Mentioned Columns MisMatch with Database";
        public const string singlequotteddefault = "default,";
        public const string insertobject = "Insert Object in Database is Executed";
        public const string failure = "Failure";
        public const string tname = "tablename";
        public const string columnname = "columnname";
        public const string columnvalue = "columnvalue";
        public const string readobject = "Read Object in Database is Executed";
        public const string dburl = "db url";
        public const string dbusername = "db username";
        public const string dbpassword = "db password";
        public const string dbport = "db port";
        public const string dbname = "db name";
        public const string dbtype = "db type";
        public const string mysql = "mysql";
        public const string nameofcolumn = "columnname(s)";
        public const string valueofcolumn = "columnvalue(s)";
        public const string wherecolumnname = "wherecolumnname(s)";
        public const string wherecolumnvalue = "wherecolumnvalue(s)";
        public const string harddelete = "harddelete";
        public const string charactervarying = "character varying";
        public const string varChar = "varchar";
        public const string strings = "string";
        public const string connection = "connection";
        public const string tablename1S = "tablename";
        public const string colpart = "columnpart";
        public const string Rows = "maxrowcount";
        public const string groupby = "groupby";
        public const string GroupFields = "GroupFields";
        public const string orderby = "orderby";
        public const string values = "values";
        public const string columns = "columns";
        public const string Column = "column";
        public const string isencoded = "isencoded";
        public const string returning = "returning";
        public const string set = "set";
        public const string isfulldelete = "isfulldelete";
        public const string deletepart = "deletepart";
        public const string twodvalue = "twodvalue";
        public const string Match = "Match";
        public const string match = "match";


        //Developer
        public const string sno = "SNo";
        public const string consolidatedtimesheets = "ConsolidatedTimeSheets";
        public const string ymdh = "yyyyMMddHHmmss";
        public const string lefttype = "lefttype";
        public const string righttype = "righttype";
        public const string leftvalue = "leftvalue";
        public const string rightvalue = "rightvalue";
        public const string condition = "condition";
        public const string loopstart = "loopstart";
        public const string loopstartid = "loopstartid";
        public const string start = "start";
        public const string step = "step";
        public const string starttype = "starttype";
        public const string forcount = "forcount";
        public const string forvalue = "forvalue";
        public const string source = "source";
        public const string filtercolumn = "filtercolumn";
        public const string filterstring = "filterstring";
        public const string filtercount = "filtercount";
        public const string sortorder = "sortorder";
        public const string sortordercolumn = "sortordercolumn";
        public const string result = "result";
        public const string systemstring = "System.String";
        public const string withheader = "withheader";
        public const string ffalse = "false";
        public const string answer = "answer : ";
        public const string length = "length";
        public const string typegenerated = "typegenerated";
        public const string initialvalue = "initialvalue";
        public const string iteratorcount = "iteratorcount";
        public const string stepoperator = "stepoperator";
        public const string stepvalue = "stepvalue";

        public const string RowSeparator = "RowSeparator";
        public const string ColumnSeparator = "ColumnSeparator";
        public const string Headers = "Headers";
        public const string HeadersSeparator = "HeadersSeparator";
        

        //public const string comparisiontype = "comparisiontype";
        //Excel
        public const string path = "path";
        public const string charttype = "charttype";
        public const string sheetname = "sheetname";
        public const string excelname = "excelname";
        public const string range = "range";
        public const string saveas = "saveas";
        public const string piechart = "piechart";
        public const string columnGraph = "ColumnGraph";
        public const string areaChart = "AreaChart";
        public const string lines = "Lines";
        public const string barofpie = "BarOfPie";
        public const string bargraph = "BarGraph";
        public const string doughnut = "Doughnut";
        public const string bubble = "Bubble";
        public const string scatterplot = "Scatterplot";
        public const string radar = "Radar";
        public const string cell = "cell(s)";
        public const string cells = "cells";
        public const string rowindex = "rowindex";
        public const string columnindex = "columnindex";
        public const string wrong = "FALSE";
        public const string ttrue = "TRUE";
        public const string withoutheader = "withoutheader";
        public const string uploads = "Uploads";
        public const string findinsheet = "findinsheet";
        public const string files = "Files";
        public const string Content = "Content";
        public const string filename = "filename";
        public const string fileNW = "File Selection Network";
        public const string fileLocal = "File Selection Local";
        public const string mdyhms = "MMddyyyyhhmmss";
        public const string matchnotfound = "Match Not Found";
        public const string novalueincell = "No value in the cell";

        //Facebbokpage
        public const string accesstoken = "accesstoken";
        public const string accesstokenn = "access token";
        public const string fbid = "fbid";
        public const string id = "ID";
        public const string postid = "PostId";
        public const string likes = "Likes";
        public const string comments = "Comments";
        public const string shares = "Shares";
        public const string accesstokens = "accessToken";
        public const string filepath = "filepath";
        public const string imagejpeg = "image/jpeg";
        public const string photos = "me/photos";
        public const string uploadusingsdk = "upload using Facebook SDK for .NET";

        //File
        public const string upload = "Upload";
        public const string hasbeenuploaded = " has been uploaded";
        public const string doc = "doc";
        public const string txt = "txt";
        public const string textlabels = "TextLabels";
        public const string pdfoutput = "PDFOutput";
        public const string Task = "task";
        public const string read = "read";
        public const string write = "write";
        public const string WildCardStar = "*";
        public const string SaveTo = "SaveTo";
        public const string DownloadIn = "Download In";
        public const string IsFolderUpload = "IsFolderUpload";

        //HRMS
        public const string serviceurl = "serviceurl";
        public const string username = "username";
        public const string password = "password";
        public const string operation = "operation";
        public const string ipadress = "ipadress";

        //HTTP
        public const string responseformat = "responseformat";
        public const string requestformat = "requestformat";
        public const string get = "GET";
        public const string authorization = "Authorization";
        public const string basic = "Basic ";
        public const string applicationjson = "application/json";
        public const string operationtype = "operationtype";
        public const string acknowledgement = "Acknowledgement";
        public const string tweetregistered = " tweet has been registered";
        public const string facebook = "Facebook";
        public const string education = "education";
        public const string eeducation = "Education";
        public const string forest = "forest";
        public const string fforest = "Forest";
        public const string labor = "labor";
        public const string llabor = "Labor";
        public const string transport = "Transport";
        public const string twitter = "twitter";
        public const string ttwitter = "Twitter";
        public const string email = "email";
        public const string electronicmail = "Email";
        public const string scanneddocument = "Scanned Document";
        public const string Unassigned = "Unassigned";
        public const string quottednew = "new";
        public const string high = "High";
        public const string question = "Question";
        public const string admin = "Admin";
        public const string csharp = "CSharp";
        public const string dashedlines = "---";
        public const string method = "method";
        public const string request = "request";
        public const string response = "response";
        public const string headers = "headers";
        public const string parameters = "parameters";
        //public const string timeout = "timeout";
        public const string authenticationtype = "authenticationtype";
        public const string authorizationtype = "authorizationtype";
        public const string httpmethod = "httpmethod";
        public const string token = "token";
        public const string encodingformat = "encodingformat";


        //LDAP
        public const string ausername = "adminusername";
        public const string apassword = "adminpassword";
        public const string organizationalunit = "organizationalunit(s)";
        public const string domaincomponent = "domaincomponent(s)";
        public const string ldapconnection = "ldapconnection";
        public const string newusername = "newusername";
        public const string newcreatetype = "newcreatetype";
        public const string newpassword = "newpassword";
        public const string groups = "group(s)";
        public const string accountname = "samaccountname";
        public const string givenname = "givenName";
        public const string sn = "sn";
        public const string displayname = "displayName";
        public const string userprincipalname = "userPrincipalName";
        public const string pwdlastset = "pwdLastSet";
        public const string useraccountcontrol = "userAccountControl";
        public const string statusid = "Status_Id";
        public const string title = "Title";
        public const string description = "Description";
        public const string requestforeob = "Request For Employee On Boarding";
        public const string eob = "employee on boarding";
        public const string requestforeoffb = "Request For Employee Off Boarding";
        public const string eoffb = "employee off boarding";
        public const string firstname = "first name";
        public const string lastname = "last name";
        public const string name = "name";

        //LinuxShell
        public const string port = "port";
        public const string url = "url";
        public const string protocol = "protocol";
        public const string ostype = "ostype";
        public const string ssh = "ssh";
        public const string linux = "Linux";
        public const string nocommand = "no command to execute";
        public const string command = "command";
        public const string filter = "filter";
        public const string timeline = "timeline";
        public const string domain = "domain";
        public const string timeout = "timeout";
        public const string nooutput = "no output to display";
        public const string rootusername = "rootusername";
        public const string rootpassword = "rootpassword";
        public const string rootcommand = "rootcommand";
        public const string repourl = "repourl";
        public const string repousername = "repousername";
        public const string repopassword = "repopassword";
        public const string untartodirectory = "untartodirectory";
        public const string installedusername = "installedusername";
        public const string installedusergroup = "installedusergroup";
        public const string installedservicename = "installedservicename";
        public const string islocaltar = "islocaltar";
        public const string localtaraddress = "localtaraddress";
        public const string customcommand = "customCommand";
        public const string wget = "wget";
        public const string xterm = "xterm";
        public const string gotos = "goto";

        //Mail
        public const string mailserveraddress = "mailserveraddress";
        public const string portnumber = "portnumber";
        public const string readtype = "readtype";
        public const string count = "count";
        public const string mails = "mails";
        public const string ismailtodatatable = "ismailtodatatable";
        public const string search = "search";
        public const string folder = "folder";
        public const string searchparameter = "searchparameter";
        public const string imap = "imap";
        public const string unread = "unread";
        public const string all = "all";
        public const string answered = "answered";
        public const string deleted = "deleted";
        public const string draft = "draft";
        public const string flagged = "flagged";
        public const string notanswered = "notanswered";
        public const string notdeleted = "notdeleted";
        public const string notdraft = "notdraft";
        public const string notflagged = "notflagged";
        public const string notrecent = "notrecent";
        public const string notseen = "notseen";
        public const string recent = "recent";
        public const string seen = "seen";
        public const string archive = "archive";
        public const string junk = "junk";
        public const string drafts = "drafts";
        public const string sent = "sent";
        public const string trash = "trash";
        public const string typeid = "Type_Id";
        public const string severityid = "Severity_Id";
        public const string CreateBy = "CreateBy";
        public const string createdatetime = "CreateDatetime";
        public const string department = "Department";
        public const string status = "Status";
        public const string truee = "true";
        public const string falsee = "false";
        public const string serverdoesnotsupport = "The POP3 server does not support UIDs!";
        public const string pop = "pop3";
        public const string mailid = "mailid";
        public const string subject = "subject";
        public const string isbodyhtml = "isbodyhtml";
        public const string destination = "destination";
        public const string sendmultiplemails = "sendmultiplemails";
        public const string attachments = "attachments";
        public const string attachment = "attachment";
        public const string mailmessage = "mailmessage";
        public const string senderusername = "senderusername";
        public const string senderpassword = "senderpassword";
        public const string smtpaddress = "smtpaddress";
        public const string template = "template";
        public const string tryit = "TryIt";
        public const string replyall = "replyall";
        public const string smtpserver = "smtpserver";
        public const string smtpport = "smtpport";
        public const string plain = "plain";
        public const string plaintext = "plaintext";
        public const string newemployee = "%new employee%";
        public const string eemail = "%email%";
        public const string user = "%user%";
        public const string empid = "%empid%";
        public const string sapid = "%sapid%";
        public const string neww = "new";
        public const string mailusername = "UserName:";
        public const string mailserver = "Mailserver:";
        public const string mailport = "Port:";
        public const string outputt = "Output:";
        public const string readtypee = "Readtype:";
        public const string searchh = "Search:";
        public const string folderr = "Folder:";
        public const string Protocoll = "Protocol:";
        public const string InboundMailserver = "inbound address";
        public const  string InboundPortNumber = "inbound port";
        public const string InboundEncryption = "inbound encryption";
        public const string OutboundMailserver = "outbound address";
        public const string OutboundPortNumber = "outbound port";
        public const string OutboundEncryption = "outbound encryption";
        public const string InboundInstance = "Inbound Instance";
        public const string OutboundInstance = "Outbound Instance";
        public const string folderto = "FolderTo";
        public const string uids = "MailUIDs";

        //NLP
        public const string comparisontype = "comparisontype";
        public const string sourcetype = "sourcetype";
        public const string comparison = "comparison";
        public const string inputvalue = "the input value: ";
        public const string matched = " times matched with given pattern";
        public const string labour = "labour";
        public const string apsrtc = "apsrtc";
        public const string keyword = "keyword";
        public const string key = "key";
        public const string namee = "Name";
        public const string label = "label(s)";
        public const string labeltype = "labeltype";
        public const string fields = "fields";
        public const string statusfield = "statusfield";
        public const string isemptyinfile = " is Empty in File ";

        //OCR
        public const string outputvariable = "outputvariable";

        //Shell
        public const string subostype = "subostype";
        public const string windows = "windows";
        public const string error = "error";
        public const string sheeliswrong = "Shell is wrong";
        public const string datatable = "DataTable";
        public const string datatable1 = "datatable";
        public const string root = "root";

        //Slack
        public const string hook = "hook";
        public const string invitetoken = "invitetoken";
        public const string channels = "channels";
        public const string hi = "HI";
        public const string emoji = "emoji";
        public const string channel = "channel";
        public const string vstrym = "VStrym";
        public const string post = "POST";

        //SalesforceAPI
        public const string securitytoken = "securitytoken";

        //Social
        public const string consumerkey = "consumerkey";
        public const string accesssecret = "accesssecret";
        public const string consumersecret = "consumersecret";
        public const string tweetsfound = " tweets found";
        public const string appid = "appid";
        public const string clientid = "clientid";
        public const string pageid = "pageid";
        public const string credentials = "client_credentials";
        public const string aadmin = "admin";
        public const string facebookk = "facebook";

        //SSH
        public const string ipaddress = "ipaddress";
        public const string adminusername = "adminusername";
        public const string createtype = "createtype";
        public const string createname = "createname";
        public const string deletetype = "deletetype";
        public const string deletename = "deletename";
        public const string notes = "notes";
        public const string users = "user";
        public const string role = "role";
        public const string group = "group";
        public const string file = "file";
        public const string entityinputfile = "Entity Input File";
        public const string classifyinputfile = "Classify Input File";

        public const string filetype = "filetype";
        public const string directory = "directory";
        public const string database = "database";
        public const string permissiontype = "permissiontype";
        public const string databasename = "dbname";
        public const string grantprivileges = "grant all privileges on ";
        public const string dbn = "dbname(s)";
        public const string backupfilename = "backupfilename";
        public const string backupfolderpath = "backupfolderpath";
        public const string allinone = "all-in-one";
        public const string sql = "mysql ";
        public const string createdb = "create database ";
        public const string dropdb = "drop database ";
        public const string move = "move";
        public const string dbconnectionDB = "dbconnection";
        public const string ConnectionString = "connectionstring";
        public const string IP = "IP";
        public const string Port = "Port";
        public const string UserName = "UserName";
        public const string FileName = "FileName";
        public const string Source = "Source";
        public const string Destination = "Destination";
        public const string Create = "Create";
        public const string Delete = "Delete";
        public const string MySQL = "mysql";
        public const string PostgreSQL = "postgresql";

        //WebScraping
        public const string xpath = "xpath";
        public const string includehtml = "includehtml";
        public const string conditionxpath = "conditionxpath";
        public const string ratingxpath = "ratingxpath";
        public const string morexpath = "morexpath";
        public const string conditionnumber = "conditionnumber";
        public const string ratings = "Ratings";
        public const string cclass = "class";
        public const string extract = "Extract";
        public const string urlplease = "Empty URL please";

        //Window
        public const string heading = "title";
        public const string displaymessage = "displaymessage";
        public const string buttons = "buttons";
        public const string messageformat = "messageformat";
        public const string User_Inp = "User";

        //WindowsShell
        public const string ip = "ip";
        public const string window = "Windows";
        public const string handles = "Handles";
        public const string npm = "NPM";
        public const string pm = "PM";
        public const string ws = "WS";
        public const string cpu = "CPU";
        public const string identity = "Id";
        public const string si = "SI";
        public const string processname = "ProcessName";
        public const string no = "No";
        public const string completed = " completed";
        public const string index = "Index";
        public const string time = "Time";
        public const string entrytype = "EntryType";
        public const string ssource = "Source";
        public const string instanceid = "InstanceID";
        public const string messages = "Message";
        public const string retain = "Retain";
        public const string overflowaction = "OverflowAction";
        public const string entries = "Entries";
        public const string log = "Log";
        public const string outstring = "Out-String";
        public const string Username = "Username";
        public const string Password = "Password";
        public const string URL = "URL";
        public const string Service = "Service";
        public const string ServiceStatus = "ServiceStatus";

        //APILayer
        public const string nosteps = "nosteps";
        public const string stop = "stop";
        public const string inprogress = "InProgress";
        public const string nostartorstop = "No Start or Stop Detected";
        public const string zero = "0";

        //EHandlers.cs
        public const string checkbox = "CHECKBOX";
        public const string combobox = "COMBOBOX";
        public const string listbox = "LISTBOX";
        public const string radiobutton = "RADIOBUTTON";
        public const string none = "NONE";
        public const string button = "BUTTON";
        public const string signature = "SIGNATURE";
        public const string textbox = "TEXTBOX";
        public const string star = "***";

        //Validator
        public const string failed = "failre";

        //Ccrud.cs
        public const string Menu = "MENU";
        public const string RoleMenu = "Role_Menu_Relation";
        public const string projectname = "ProjectName";
        public const string project = "PROJECT";
        public const string robot = "ROBOT";
        public const string startingtime = "StartingTime";
        public const string endingtime = "EndingTime";
        public const string screentable = "ScreenTable";
        public const string OBDN_Tracker = "OBDN_Tracker";
        public const string dbconnection = "DBConnection_CommandCenter";
        public const string mysqldb = "MySQL";
        public const string postgressql = "PostgreSQL";
        public const string fail = "Failed";
        public const string stepproperties = "STEPPROPERTIES";
        public const string iinbox = "Inbox";
        public const string CommandCenter_Inbox = "CommandCenter_Inbox";
        public const string processmangerbotdetails = "ProcessMangerBotDetails";
        public const string schedulercommandcenter = "Scheduler_CommandCenter";
        public const string scheduler = "Scheduler";
        public const string action = "ACTION";
        public const string actionproperties = "ActionProperties";
        public const string actionproperty = "ActionProperty";
        public const string element = "ELEMENT";
        public const string timesheets = "TIMESHEETS";
        public const string steps = "STEPS";
        public const string workflow = "WORKFLOW";
        public const string cases = "cases";
        public const string history = "History_CommandCenter";
        public const string removesystem = "RemoveSystem_CommandCenter";
        public const string processmanager = "ProcessManager_CommandCenter";
        public const string systems = "Systems_CommandCenter";
        public const string profile = "Profile_User";
        public const string connectionsetup = "DBConnection_SetUp";
        public const string existed = "Existed";
        public const string dbsetup = "DBSetUp_SetUp";
        public const string server = "Server";
        public const string nameoftheuser = "Username";
        public const string pword = "Password";
        public const string portt = "Port";
        public const string db = "Database";
        public const string dbinstall = "DBSetUp_Install";
        public const string append = "add";
        public const string pgsqlconn = "PgSqlConn";
        public const string connectionstring = "connectionString";
        public const string providername = "providerName";
        public const string npgsql = "Npgsql";
        public const string select = "Select";
        public const string setuppage = "SetUpPage_UserDetails";
        public const string fname = "FirstName";
        public const string lname = "LastName";
        public const string contact = "Contact";
        public const string notification = "Notification_Insert";
        public const string kpi = "KPI";
        public const string nkpi = "NKPI";
        public const string srole = "ROLE";
        public const string scharttype = "Chart_Type";
        public const string systemsrobots = "SystemsRobots";
        public const string ConnectivitySystems = "ConnectivityGraph_Systems";
        public const string ConnectivityRobots = "ConnectivityGraph_Robots";
        public const string ConnectivityServerDetails = "ConnectivityGraph_ServerDetails";
        public const string getscreenID = "GetScreenIDForRegeneration";
        public const string ConnectivityInfo = "ConnectivityInfo";
        public const string PendingSubscriptions = "PendingSubscriptions";
        public const string PendingSubscriptionsapproval = "PendingSubscriptionsapproval";
        public const string CustomActionProperties = "CustomActionProperties";



        //cDElement
        public const string notimplemented = "Not Implemented";

        //Messages.cs
        public const string defaultvalue = "defaultValue";
        public const string yes = "YES";
        public const string check = "CHECKED";

        //ExecutionLog
        public const string active = "Active";

        //cHtml
        public const string sname = "SName";
        public const string formtype = "formType";
        public const string tablen = "TName";
        public const string ainsert = "AInsert";
        public const string insights = "Insights";
        public const string columnorder = "columnOrder";
        public const string quottedactive = "active";
        public const string elementid = "elementId";
        public const string tbox = "TextBox";
        public const string widgettype = "WidgetType";
        public const string cbox = "CheckBox";
        public const string dropdown = "DropDown";
        public const string textarea = "Textarea";
        public const string textfile="file";
        public const string mandatory = "Mandatory";
        public const string required = "required";
        public const string quottednone = "none";
        public const string showform = "showForm";
        public const string initial = "initial";
        public const string aactive = "active";
        public const string cname = "columnName";
        public const string identitydocument = "id";
        public const string inline = "inline";
        public const string edit = "Edit";
        public const string Length = "Length";
        public const string lenval = "maxlength";
        public const string None = "None";
        //cLogin
        public const string uname = "UserName";
        public const string passwordrecovery = "Password Recovery";

        //Messages
        public const string connectionnotopened = "Connection is not open";

        //cScript
        public const string rcount = "RCount";
        public const string preferences = "Preferences";
        public const string uuser = "User";
        public const string adel = "ADelete";
        public const string aedit = "AEdit";
        public const string dsort = "DSort";
        public const string asc = "ASC";
        public const string desc = "DESC";
        public const string showgrid = "showGrid";

        //cSQLScripts
        public const string linknodes = "LinkNodes";
        public const string stepid = "StepId";
        public const string childstepids = "ChildStepIds";
        public const string updatedatetime = "UpdateDateTime";
        public const string declare = "declare";
        public const string robots = "Robots";
        public const string robotid = "Robot_Id";
        public const string stepsid = "Steps_Id";
        public const string projectid = "Project_Id";

        //cStep
        public const string sproperties = "StepProperties";

        //cSystem_Robots
        public const string systemid = "System_Id";
        public const string recordexists = "record exists";

        //Logs
        public const string ymd = "yyyyMMdd";

        //MysqlConnect
        public const string connectionfailure = "Connection Failure";

        //SqlServerConnection
        public const string scalar = "scalar";
        public const string unsuccessful = "failed";

        //Database.cs
        public const string selection = "select";
        public const string insert = "insert";
        public const string delete = "delete";
        public const string update = "update";
        public const string insertwithreturn = "insertwithreturn";
        public const string insertwithreturnprops = "insertwithreturnprops";
        public const string createtheproject = "createproject";
        public const string createtherobot = "createrobot";
        public const string updatelinks = "updateLinks";
        public const string selectwithnodes = "selectwithnodes";
        public const string selectwithnode = "selectwithnode";
        public const string selectrobotslogs = "selectRobotsLogs";

        //QueryUtil
        public const string columnnotexists = "column not exists";
        public const string bydefault = "default";
        public const string character = "character";
        public const string tdate = "date";
        public const string app_tables = "app_tables";
        public const string timeperiod = "time";
        public const string unevenfields = "uneven fields please check the number of fields entered";
        public const string like = "like";
        public const string equals = "equals";
        public const string notequals = "not equals";
        public const string lessthan = "less than";
        public const string greaterthan = "greater than";
        public const string quottedin = "in";
        public const string notin = "not in";
        public const string lessthanorequalto = "less than or equal to";
        public const string greaterthanorequalto = "greater than or equal to";
        public const string boolean = "bool";
        public const string integer = "int";
        public const string serial = "serial";
        public const string numeric = "numeric";
        public const string decimalnumber = "decimal";
        public const string doubleprecision = "double precision";
        public const string characters = "char";
        public const string text = "text";

        //cSave.cs
        public const string robotnotsavedaddanothersteps = "Robot not saved.Please add other steps ,only start and stop added.";
        public const string robotnotsavedfilltheproperties = "Robot not saved.Please fill the properties ";
        public const string robotupdated="robot updated";    
        public const string stepssaved = "steps saved";
        public const string versioncreated = "robot saved and version created";


        //Executionservice.svc.cs
        public const string createrobot = "Robot";
        public const string createproject="Project";
        public const string createworkflow = "Workflow";
        public const string createsystems = "Systems";
        public const string ProcessManager = "ProcessManager";
        public const string incorrect = "wrong";
        public const string deleterobot = "Robot";
        public const string deleteproject = "Project";
        public const string deleteworkflow = "Workflow";
        public const string deletelinknodes = "LinkNodes";
        public const string deletesteps = "Steps";
        public const string getallactions = "AllActions";
        public const string getaction = "Action";
        public const string getactionrow = "ActionRow";
        public const string getcategoryrobotsteps = "CategoryRobotSteps";
        public const string getallelements = "AllElements";
        public const string getallsteps = "AllSteps";
        public const string getallstepsprops = "AllStepsProperties";
        public const string getallworkflows = "AllWorkflows";
        public const string getallrobots = "AllRobots";
        public const string getrobotproperties = "RobotProperties";
        public const string getallprojects = "AllProjects";
        public const string getallactionproperties = "AllActionProperties";
        public const string getrobot = "Robot";
        public const string getproject = "Project";
        public const string getworkflow = "Workflow";
        public const string getalltheworkflows = "allWorkflows";
        public const string getalltherobots = "allRobots";
        public const string getstepproperties = "stepProperties";
        public const string getallstepproperties = "AllstepProperties";
        public const string linkallnodes = "LinkAllNodes";
        public const string alllobs = "AllLOBs";
        public const string allsystems = "AllSystems";
        public const string allsysrobots = "AllSysRobots";
        public const string allrobsystems = "AllRobSystems";
        public const string lob = "LOB";
        public const string lobprojects = "LOBProjects";
        public const string categoryprojects = "CategoryProjects";
        public const string systemrobotscollection = "SystemRobotsCollection";
        public const string getrobotsstepsstepprops = "getRobotsStepsStepProps";
        public const string getprojectid = "getProjectId";
        public const string getlog = "getLog";
        public const string getrobotslog = "getRobotsLog";
        public const string getsystemsrobotslist = "getSystemsRobotsList";
        public const string predefindedbotsbycategory = "PredefindedBotsByCategory";
        public const string loginauthentication = "loginAuthentication";
        public const string sucess = "success";
        public const string operationfailed= "failure";
        public const string getalllobs = "GETAllLOBs";
        public const string getallrobotsystemlist = "getAllRobotSystemList";
        public const string getallemailtemplates = "getAllEmailTemplates";
        public const string getallrobotversions = "getAllRobotVersions";
        public const string getrobotsstepssteppropsversion = "getRobotsStepsStepPropsVersion";
        public const string setsteps = "Steps";
        public const string setstepproperties = "stepproperties";
        public const string robotschedule = "robotSchedule";
        public const string saverobot = "SaveRobot";
        public const string savewarobot = "SaveWARobot";
        public const string getwarobot = "getWARobot";
        public const string getwarobotssteps = "getWARobotsSteps";
        public const string getwflobprojectrobots = "getWFLOBProjectRobots";
        public const string stepssavedcase = "stepssaved";
        public const string robotupdatedcase = "robotupdated";
        public const string robotsavedandversioncreated = "robotsavedandversioncreated";
        public const string execution = "Execution";
        public const string executioninprogress = "ExecutionInProgress";
        public const string robottosystem = "robot2System";
        public const string savepredefinedbot = "SavePredefinedBot";
        public const string updatesteps = "Steps";
        public const string updatestepproperties = "stepproperties";
        public const string updateworkflow = "workflow";
        public const string polling = "Polling";
        public const string updaterobot = "Robot";
        public const string renamerobot = "Robot";
        public const string renameproject = "Project";
        public const string renameworkflow = "Workflow";
        public const string getWAActionsActionProperties = "getWAActionsActionProperties";
        public const string getwfconnection = "getwfconnection";

        //Statuscodes
        public const string eventcode = "700";
        public const string exceptioncode = "701";
        public const string errorcode = "702";
        public const string warningcode = "703";
        public const string projectedcode = "704";
        public const string resultcode = "705";
        public const string inputcode = "720";
        public const string outputcode = "721";
        public const string successcode = "200";

        public const string ExecutionClassTypes = "Genbase.BLL.ElementsClasses.";

        public const string execsuccess = " is executed Successfully";
        public const string execclient = " is a client Action, so run it in Agent ";
        public const string datatablesmall = "datatable";
        public const string Running = "Running";
        public const string Stopped = "Stopped";
        public const string Paused = "Paused";
        public const string Stopping = "Stopping";
        public const string Starting = "Starting";
        public const string StatusChanging = "Status Changing";
        public const string comparisiontype = "comparisiontype";

        public const string switchvalue = "switchvalue";
        public const string casevalues = "casevalues";
        public const string ActionControl = "ActionControl";     

        //Desktopservice.svc.cs
        public const string wrongg = "wrong";
        public const string AllActionsByType = "AllActionsByType";
        public const string AllActions = "AllActions";
        public const string AllElements = "AllElements";
        public const string AllSteps = "AllSteps";

        //Crudservice.svc.cs
        public const string CommandCenterNKPI = "CommandCenterNKPI";
        public const string CommandCenterSystemNKPI = "CommandCenterSystemNKPI";
        public const string Type = "Type";

        //Excel Data
        public const string EmployeeName = "%Employee Name%";
        public const string Date = "%Date%";
        public const string Manager = "%Manager%";
        public const string ManagerName = "%Manager Name%";
        public const string SupervisorName = "%Supervisor Name%";
        public const string HRName = "%HR Name%";
        public const string Gender = "%Gender%";
        public const string TotalNumberOfYears = "%Total number of years%";
        public const string Department = "%Department%";
        public const string Expertise = "%Expertise%";
        public const string OtherInformation = "%Other information%";
        public const string EmployeePosition = "%Employee position%";
    }
    public static class ExceptionList
    {
        public static Dictionary<string, string> Exceptionlist = new Dictionary<string, string> {
            {"IndexOutOfRangeException","416 Requested Range Not Satisfiable"},
            { "AccessViolationException","403 Forbidden"},
            { "AggregateException","406 Not Acceptable"},
            { "AppDomainUnloadedException","417 Expectation Failed"},
            { "ApplicationException","510 Not Extended"},
            { "ArgumentException","428 Precondition Required"},
            { "ArgumentNullException","428 Precondition Required"},
            { "ArgumentOutOfRangeException","416 Requested Range Not Satisfiable"},
            { "ArithmeticException","406 Not Acceptable"},
            { "ArrayTypeMismatchException","415 Unsupported Media Type"},
            { "BadImageFormatException","415 Unsupported Media Type"},
            { "CannotUnloadAppDomainException","417 Expectation Failed"},
            { "ConstraintException","412 Precondition Failed"},
            { "ContextMarshalException","417 Expectation Failed"},
            { "DataException","401 Unauthorized"},
            { "DataMisalignedException","401 Unauthorized"},
            { "DBConcurrencyException","507 Insufficient Storage"},
            { "DeletedRowInaccessibleException","403 Forbidden"},
            { "DivideByZeroException","400 Bad Request"},
            { "DllNotFoundException","404 Not Found"},
            { "DuplicateNameException","409 Conflict"},
            { "DuplicateWaitObjectException","409 Conflict"},
            { "EntryPointNotFoundException","404 Not Found"},
            { "EvaluateException","406 Not Acceptable"},
            { "ExecutionEngineException","417 Expectation Failed"},
            { "FieldAccessException","403 Forbidden"},
            { "FormatException","415 Unsupported Media Type"},
            { "InRowChangingEventException","417 Expectation Failed"},
            { "InsufficientExecutionStackException","507 Insufficient Storage"},
            { "InsufficientMemoryException","507 Insufficient Storage"},
            { "InvalidCastException","502 Bad Gateway"},
            { "InvalidConstraintException","502 Bad Gateway"},
            { "InvalidExpressionException","502 Bad Gateway"},
            { "InvalidOperationException","502 Bad Gateway"},
            { "InvalidProgramException","502 Bad Gateway"},
            { "InvalidTimeZoneException","502 Bad Gateway"},
            { "JsonException","417 Expectation Failed"},
            { "JsonReaderException","417 Expectation Failed"},
            { "JsonSerializationException","417 Expectation Failed"},
            { "JsonWriterException","417 Expectation Failed"},
            { "KeyNotFoundException","404 Not Found"},
            { "MemberAccessException","417 Expectation Failed"},
            { "MethodAccessException","401 Unauthorized"},
            { "MissingFieldException","403 Forbidden"},
            { "MissingMemberException","403 Forbidden"},
            { "MissingMethodException","403 Forbidden"},
            { "MissingPrimaryKeyException","403 Forbidden"},
            { "MulticastNotSupportedException","406 Not Acceptable"},
            { "NoNullAllowedException","405 Method Not Allowed"},
            { "NotFiniteNumberException","405 Method Not Allowed"},
            { "NotImplementedException","406 Not Acceptable"},
            { "NotSupportedException","406 Not Acceptable"},
            { "NullReferenceException","428 Precondition Required"},
            { "ObjectDisposedException","500 Internal Server Error"},
            { "OperationAbortedException","500 Internal Server Error"},
            { "OperationCanceledException","417 Expectation Failed"},
            { "OutOfMemoryException","507 Insufficient Storage"},
            { "OverflowException","507 Insufficient Storage"},
            { "PlatformNotSupportedException","415 Unsupported Media Type"},
            { "RankException","510 Not Extended"},
            { "ReadOnlyException","406 Not Acceptable"},
            { "RowNotInTableException","417 Expectation Failed"},
            { "StackOverflowException","507 Insufficient Storage"},
            { "StrongTypingException","417 Expectation Failed"},
            { "SyntaxErrorException","400 Bad Request"},
            { "SystemException","400 Bad Request"},
            { "TimeoutException","408 Request Timeout"},
            { "TimeZoneNotFoundException","408 Request Timeout"},
            { "TypeAccessException","403 Forbidden"},
            { "TypedDataSetGeneratorException","417 Expectation Failed"},
            { "TypeInitializationException","417 Expectation Failed"},
            { "TypeLoadException","417 Expectation Failed"},
            { "TypeUnloadedException","417 Expectation Failed"},
            { "UnauthorizedAccessException","401 Unauthorized"},
            { "UriFormatException","414 URI Too Long"},
            { "UriTemplateMatchException","414 URI Too Long"},
            { "VersionNotFoundException","404 Not Found"} };
    }
}