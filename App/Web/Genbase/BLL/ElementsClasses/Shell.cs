﻿using Genbase.classes;
using Newtonsoft.Json;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Security;
using System.Text.RegularExpressions;
using Genbase.Classes;

namespace Genbase.BLL.ElementsClasses
{
    public class Shell
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult Connection( Steps step, string executionID)
        {
            MethodResult mr;
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string OSType = string.Empty, SubOSType = string.Empty, Password = string.Empty, URL = string.Empty, UserName = string.Empty, Protocol = string.Empty, Output = string.Empty, Port = string.Empty, Input = string.Empty;
            try
            {
                OSType = stp.getPropertiesValue(StringHelper.ostype, step);
                SubOSType = stp.getPropertiesValue(StringHelper.subostype, step);
                Password = stp.getPropertiesValue(StringHelper.password, step);
                URL = stp.getPropertiesValue(StringHelper.url, step);
                UserName = stp.getPropertiesValue(StringHelper.username, step);
                Protocol = stp.getPropertiesValue(StringHelper.protocol, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Port = stp.getPropertiesValue(StringHelper.port, step);

                DVariables value = new DVariables();
                if (Input.Trim() != string.Empty)
                {
                    value = new VariableHandler().getVariables(Input, executionID);
                }
                if (OSType.ToLower() == StringHelper.linux)
                {
                    if (Protocol.ToLower() == StringHelper.ssh)
                    {
                        using (var ssh = new SshClient(new MethodResultHandler().CreateConnection(URL.Trim(), int.Parse(Port.Trim()), UserName.Trim(), Password.Trim())))
                        {

                            ssh.Connect();

                            ShellConnection sc = new ShellConnection();
                            sc.ipname = URL;
                            sc.port = Port;
                            sc.pswd = Password;
                            sc.uname = UserName;

                            new VariableHandler().CreateVariable(Output, sc, "", true,"class",step.Robot_Id, executionID);


                            ssh.Disconnect();
                            ret = StringHelper.success;
                        }
                    }
                }
                else if (OSType.ToLower() == StringHelper.windows)
                {
                    ShellConnection sc = new ShellConnection();
                    sc.ipname = URL;
                    sc.port = Port;
                    sc.pswd = Password;
                    sc.uname = UserName;

                    new VariableHandler().CreateVariable(Output, sc, "", true,"class", step.Robot_Id, executionID);
                    ret = StringHelper.success;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Execute( Steps step, string executionID)
        {
            MethodResult mr;
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string Filter = string.Empty, Command = string.Empty, Domain = string.Empty, Error = string.Empty, Input = string.Empty, Output = string.Empty, OSType = string.Empty, SubOSType = string.Empty, rootUserName = string.Empty, rootPassword = string.Empty;
            try
            {

                Filter = stp.getPropertiesValue(StringHelper.filter, step);
                Command = stp.getPropertiesValue(StringHelper.command, step);
                Domain = stp.getPropertiesValue(StringHelper.domain, step);
                Error = stp.getPropertiesValue(StringHelper.error, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                OSType = stp.getPropertiesValue(StringHelper.ostype, step);
                SubOSType = stp.getPropertiesValue(StringHelper.subostype, step);
                rootUserName = stp.getPropertiesValue(StringHelper.rootusername, step);
                rootPassword = stp.getPropertiesValue(StringHelper.rootpassword, step);

                DVariables value = new DVariables();
                if (Input.Trim() != string.Empty)
                {
                    value = new VariableHandler().getVariables(Input, executionID);
                }

                if (OSType.ToLower() == StringHelper.windows)
                {
                    ShellConnection sc = (ShellConnection)value.vlvalue;
                    string Text = string.Empty;
                    SecureString password = new SecureString();
                    string runasUsername = sc.uname;
                    string runasPassword = sc.pswd;

                    foreach (char x in runasPassword)
                    {
                        password.AppendChar(x);
                    }
                    PSCredential credential = new PSCredential(runasUsername, password);
                    WSManConnectionInfo connectionInfo = new WSManConnectionInfo(false, sc.ipname, 5985, "/wsman", "http://schemas.microsoft.com/powershell/Microsoft.PowerShell", credential, 1 * 60 * 1000);
                    Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo);
                    runspace.Open();
                    using (PowerShell ps = PowerShell.Create())
                    {
                        ps.Runspace = runspace;
                        ps.AddScript(Command);
                        var results = ps.Invoke();
                        foreach (object str in results)
                        {
                            Text += str.ToString() + "\n";
                        }
                    }
                    if (Text.Trim() != string.Empty)
                    {
                        new ExecutionLog().add2Log(Text, step.Robot_Id, step.Name, StringHelper.eventcode, false);
                        new VariableHandler().CreateVariable(Output, Text, "", true, "string", step.Robot_Id, executionID);
                        ret = StringHelper.success;
                    }
                    else
                    {
                        new ExecutionLog().add2Log(StringHelper.sheeliswrong, step.Robot_Id, step.Name, StringHelper.exceptioncode, false);
                    }
                    runspace.Close();

                    ret = StringHelper.success;
                }
                else if (OSType.ToLower() == StringHelper.linux)
                {
                    ShellConnection sc = (ShellConnection)value.vlvalue;

                    using (var ssh = new SshClient(new MethodResultHandler().CreateConnection(sc.ipname.Trim(), int.Parse(sc.port.Trim()), sc.uname.Trim(), sc.pswd.Trim())))
                    {
                        string cmd = string.Empty, answer = string.Empty; SshCommand cmd1 = null; string noanswer = string.Empty;

                        if (Command.Trim() != string.Empty)
                        {
                            cmd = Command;
                        }
                        else
                        {
                            cmd = value.vlvalue;
                        }

                        ssh.Connect();
                        if (cmd.Trim() != string.Empty)
                        {
                            if (cmd.IndexOf("su") > -1 || cmd.IndexOf(StringHelper.start) > -1)
                            {
                                answer = ExpectSSH(sc.ipname.Trim(), sc.uname.Trim(), sc.pswd.Trim(), cmd);
                                answer = answer.Replace("[", "\n["); answer = answer.Replace(";", ";\n");
                            }
                            else
                            {
                                SshCommand ssc = ssh.CreateCommand(cmd);
                                ssc.Execute();
                                cmd1 = ssh.RunCommand(cmd);
                                answer = ssc.Result;
                                noanswer = StringtoJson(answer);
                            }
                        }
                        else
                        {
                            answer = StringHelper.nocommand;
                        }
                        if (answer.Trim() != string.Empty)
                        {
                            new ExecutionLog().add2Log(answer, step.Robot_Id, step.Name, StringHelper.eventcode, false);
                            new VariableHandler().CreateVariable(Output, answer, "", true,"class", step.Robot_Id, executionID);
                            new VariableHandler().CreateVariable(StringHelper.datatable, noanswer, "", true, "class", step.Robot_Id, executionID);
                            ret = StringHelper.success;
                        }
                        else
                        {
                            new ExecutionLog().add2Log(cmd1.Error, step.Robot_Id, step.Name, StringHelper.exceptioncode, false);
                        }
                        ssh.Disconnect();
                        ret = StringHelper.success;
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }


        public string ExpectSSH(string address, string login, string password, string command)
        {
            string output = string.Empty;
            try
            {
                SshClient sshClient = new SshClient(address, 22, login, password);

                sshClient.Connect();
                IDictionary<Renci.SshNet.Common.TerminalModes, uint> termkvp = new Dictionary<Renci.SshNet.Common.TerminalModes, uint>();
                termkvp.Add(Renci.SshNet.Common.TerminalModes.ECHO, 53);

                ShellStream shellStream = sshClient.CreateShellStream(StringHelper.xterm, 80, 24, 800, 600, 1024, termkvp);

                //Get logged in
                string rep = shellStream.Expect(new Regex(@"[$>]")); //expect user prompt
                output += rep;

                //send command
                shellStream.WriteLine(command);
                rep = shellStream.Expect(new Regex(@"([$#>:])")); //expect password or user prompt
                output += rep;

                //check to send password
                if (rep.Contains(":"))
                {
                    //send password
                    shellStream.WriteLine(StringHelper.root);
                    rep = shellStream.Expect(new Regex(@"[$#>]")); //expect user or root prompt
                    output += rep;
                }

                sshClient.Disconnect();
            }
            //try to open connection
            catch (Exception ex)
            {
                output = ex.Message;
            }
            return output;
        }
        public string StringtoJson(string text)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<string> rows = new List<string>();
                List<string> HeaderRow = new List<string>();

                //rows
                if (text.IndexOf("\n") > -1)
                {
                    rows = text.Split(new string[] { "\n" }, StringSplitOptions.None).ToList();
                }
                //HeaderRow
                if (rows[0].IndexOf(" ") > -1)
                {
                    HeaderRow = rows[0].Split(new string[] { " ", "  ", "   ", "  " }, StringSplitOptions.None).ToList();
                }
                HeaderRow.ForEach(x => x.Trim()); HeaderRow.RemoveAll(x => x == string.Empty);


                DataTable dt = new DataTable(); dt.Columns.Add("d");
                foreach (string head in HeaderRow)
                {
                    dt.Columns.Add(head);
                }

                foreach (string row in rows)
                {
                    List<string> rower = new List<string>();
                    rower = row.Split(new string[] { " ", "  ", "   ", "  " }, StringSplitOptions.None).ToList();
                    rower.ForEach(x => x.Trim()); rower.RemoveAll(x => x == string.Empty);
                    rower.Insert(0, "");
                    dt.Rows.Add(rower.ToArray());
                }
                //rows

                return JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }

    }
    public class ShellConnection
    {
        public string ipname { get; set; }
        public string uname { get; set; }
        public string pswd { get; set; }
        public string port { get; set; }
        public string ostype { get; set; }
        public string subostype { get; set; }
    }
}