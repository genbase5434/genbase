﻿using Genbase.classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Genbase.Classes;
using Microsoft.AspNetCore.Authentication;
//using System.DirectoryServices.AccountManagement;
using System.Xml;

namespace Genbase.BLL.ElementsClasses
{
    public class LDAP
    {
        StepPropertyHandler stp = new StepPropertyHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult Login( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string inputtype = string.Empty; string input = string.Empty; string output = string.Empty; string ous = string.Empty; string dcs = string.Empty; string ausername = string.Empty; string apassword = string.Empty; string ipaddress = string.Empty;
            string result = string.Empty;
            try
            {
                ausername = stp.getPropertiesValue(StringHelper.ausername, step);
                apassword = stp.getPropertiesValue(StringHelper.apassword, step);
                inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                ous = stp.getPropertiesValue(StringHelper.organizationalunit, step);
                dcs = stp.getPropertiesValue(StringHelper.domaincomponent, step);
                input = stp.getPropertiesValue(StringHelper.input, step);
                ipaddress = stp.getPropertiesValue(StringHelper.ipaddress, step);
                List<string> ouList = new List<string>();
                List<string> dcList = new List<string>();

                if (input.Trim() != string.Empty)
                {

                }
                else
                {
                    if (ous.Trim() != string.Empty)
                    {
                        if (ous.IndexOf(",") > -1)
                        {
                            ouList = ous.Split(',').ToList();
                        }
                        else
                        {
                            ouList.Add(ous);
                        }
                    }
                    if (dcs.Trim() != string.Empty)
                    {
                        if (dcs.IndexOf(",") > -1)
                        {
                            dcList = dcs.Split(',').ToList();
                        }
                        else
                        {
                            dcList.Add(dcs);
                        }
                    }
                    ouList.RemoveAll(x => x.Trim() == string.Empty); dcList.RemoveAll(x => x.Trim() == string.Empty);

                    var newouList = ouList.Select(x => "OU=" + x + ",").ToList();
                    var newdcList = dcList.Select(x => "DC=" + x + ",").ToList();

                    string OU = string.Join("", newouList); OU = OU.Remove(OU.Length - 1);
                    string DC = string.Join("", newdcList); DC = DC.Remove(DC.Length - 1);

                    string strHostName = "";
                    strHostName = System.Net.Dns.GetHostName();

                    IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);

                    IPAddress[] addr = ipEntry.AddressList;

                    string FinalIpAddress = addr[addr.Length - 1].ToString();

                    //using (var searcher = new PrincipalSearcher(new UserPrincipal(new PrincipalContext(ContextType.Domain, Environment.UserDomainName))))
                    //using (var searcher = new PrincipalSearcher(new UserPrincipal(new PrincipalContext(ContextType.Machine, FinalIpAddress, null, ContextOptions.Negotiate, @"DMZEPS\"+ausername, apassword))))
                    //{
                    //    List<UserPrincipal> users = searcher.FindAll().Select(u => (UserPrincipal)u).ToList();
                    //}

                    if (FinalIpAddress == ipaddress)
                    {

                        string path = "LDAP://" + OU + "," + DC;
                        List<string> orgUnits = new List<string>();

                        LDAPConnection ldapcon = new LDAPConnection();
                        string finalPath = Base64Encode(path);
                        ldapcon.LDAPPath = finalPath;
                        ldapcon.LDAPAdminUserName = ausername;
                        ldapcon.LDAPAdminPassword = apassword;
                        ldapcon.ConnectionDomain = "Current";
                        ldapcon.ldapURl = "LDAP://";
                        ldapcon.OU = OU + ",";
                        ldapcon.DC = DC;
                        new VariableHandler().CreateVariable(StringHelper.ldapconnection, ldapcon, "", false, "datatable", step.Robot_Id, executionID);

                        if (output != string.Empty)
                        {
                            new VariableHandler().CreateVariable(output, ldapcon, "", false, "datatable", step.Robot_Id, executionID);
                        }
                    }
                    else
                    {
                        string path = "LDAP://" + ipaddress + "/" + OU + "," + DC;

                        string finalPath = Base64Encode(path);
                        LDAPConnection ldapcon = new LDAPConnection();
                        ldapcon.LDAPPath = finalPath;
                        ldapcon.LDAPAdminUserName = ausername;
                        ldapcon.LDAPAdminPassword = apassword;
                        ldapcon.ConnectionDomain = "Other";
                        ldapcon.ldapURl = "LDAP://" + ipaddress + "/";
                        ldapcon.OU = OU + ",";
                        ldapcon.DC = DC;
                        new VariableHandler().CreateVariable(StringHelper.ldapconnection, ldapcon, "", true, "datatable", step.Robot_Id, executionID);

                        if (output != string.Empty)
                        {
                            new VariableHandler().CreateVariable(output, ldapcon, "", true, "datatable", step.Robot_Id, executionID);
                        }
                    }
                    result = "Success";
                    ret = StringHelper.success;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log("LDAP Login is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);
                //new VariableHandler().CreateVariable(output, result, "", true, "string");
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public MethodResult CreateUser( Steps step, string executionID)
        {
            string ret = string.Empty;
            int ticketexists = 0;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string inputtype = string.Empty; string input = string.Empty; string output = string.Empty;
            string newcreatetype = string.Empty; string newusername = string.Empty; string newpassword = string.Empty; string groups = string.Empty; string result = string.Empty;
            try
            {
                newusername = stp.getPropertiesValue(StringHelper.newusername, step);
                inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                newcreatetype = stp.getPropertiesValue(StringHelper.newcreatetype, step);
                newpassword = stp.getPropertiesValue(StringHelper.newpassword, step);
                groups = stp.getPropertiesValue(StringHelper.group, step);

                Variables.xuser.Clear();
                DVariables valuer = new DVariables();
                List<Dictionary<string, dynamic>> inputArray1 = new List<Dictionary<string, dynamic>>();

                if (input.Trim() != string.Empty)
                {
                    valuer = new VariableHandler().getVariables(input, executionID);
                    if (valuer != null && valuer.vlvalue.GetType() == typeof(string))
                    {
                        System.Data.DataTable dtr = JsonConvert.DeserializeObject<System.Data.DataTable>(valuer.vlvalue);
                        inputArray1 = dtr.AsEnumerable().Select(row => dtr.Columns.Cast<DataColumn>().ToDictionary(column => column.ColumnName, column => row[column] as dynamic)).ToList();
                    }
                }

                DVariables value = new DVariables();
                value = new VariableHandler().getVariables(StringHelper.ldapconnection, executionID);
                if (value != null && value.vlvalue.GetType() == typeof(LDAPConnection))
                {
                    string decodePath = ((LDAPConnection)value.vlvalue).LDAPPath;
                    string path = Base64Decode(decodePath);
                    string adminuser = ((LDAPConnection)value.vlvalue).LDAPAdminUserName;
                    string adminpswd = ((LDAPConnection)value.vlvalue).LDAPAdminPassword;
                    string DomainStatus = ((LDAPConnection)value.vlvalue).ConnectionDomain;

                    if (DomainStatus == "Current")
                    {

                        List<string> dcs = path.Split(new string[] { "DC=" }, StringSplitOptions.None).ToList();
                        dcs.RemoveAt(0);
                        string domain = string.Join(".", dcs);
                        domain = domain.Replace(",", "");


                        if (newusername != string.Empty)
                        {
                            xUser xu = new xUser();
                            xu.adusername = newusername;
                            Variables.xuser.Add(xu);
                            Variables.xusername = newusername;

                            DirectoryEntry deBase = new DirectoryEntry(path);
                            DirectoryEntry auser = deBase.Children.Add("cn=" + newusername, "user");
                            auser.CommitChanges();
                            auser.Properties[StringHelper.accountname].Value = newusername;
                            auser.Properties[StringHelper.givenname].Value = "A";
                            auser.Properties[StringHelper.sn].Value = newusername;
                            auser.Properties[StringHelper.displayname].Value = newusername;
                            auser.Properties[StringHelper.userprincipalname].Value = newusername + "@" + domain;
                            auser.Properties[StringHelper.pwdlastset].Value = 0;
                            auser.Properties[StringHelper.useraccountcontrol].Value = 544;
                            auser.CommitChanges();
                        }
                        else
                        {
                            foreach (var dict in inputArray1)
                            {
                                string firstname = string.Empty;
                                string name = string.Empty;
                                string lastname = string.Empty;
                                string emailid = string.Empty;
                                string emailpswd = string.Empty;
                                string slackid = string.Empty;
                                string slackpswd = string.Empty;
                                string adname = string.Empty;
                                string username = string.Empty;

                                if (dict.Keys.Contains(StringHelper.statusid) && dict[StringHelper.statusid] != null)
                                {
                                    string desc = dict[StringHelper.description];

                                    if (dict[StringHelper.statusid].ToString() == "1")
                                    {
                                        if (dict.Keys.Contains(StringHelper.title) && dict.Keys.Contains(StringHelper.description))
                                        {
                                            if (dict[StringHelper.title].ToLower().IndexOf(StringHelper.requestforeob) > -1 || dict[StringHelper.description].ToLower().IndexOf(StringHelper.eob) > -1)
                                            {
                                                List<string> slashns = desc.Split(new string[] { "\n", "\\n" }, StringSplitOptions.None).ToList();
                                                foreach (string slashn in slashns)
                                                {
                                                    try
                                                    {
                                                        if (slashn.Trim() != string.Empty && slashn.IndexOf(":") > -1)
                                                        {
                                                            string fpart = slashn.Split(':')[0].Trim();
                                                            string spart = slashn.Split(':')[1].Trim();

                                                            if (fpart.ToLower().Trim() == StringHelper.firstname)
                                                            {
                                                                firstname = spart;
                                                            }
                                                            else if (fpart.ToLower().Trim() == StringHelper.lastname)
                                                            {
                                                                lastname = spart;
                                                            }
                                                            else if (fpart.ToLower().Trim() == StringHelper.name)
                                                            {
                                                                name = spart;
                                                            }

                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        new Logger().LogException(ex);
                                                        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                                    }
                                                }
                                                try
                                                {
                                                    using (Npgsql.NpgsqlConnection con = new Npgsql.NpgsqlConnection(Variables.connString))
                                                    {
                                                        using (Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand("UPDATE public.\"Ticket\" set \"Status_Id\"=0 where \"ID\" = " + dict["ID"]))
                                                        {
                                                            cmd.Connection = con;
                                                            con.Open();
                                                            using (Npgsql.NpgsqlDataAdapter ndr = new Npgsql.NpgsqlDataAdapter(cmd))
                                                            {
                                                                cmd.ExecuteNonQuery();
                                                            }
                                                            con.Close();
                                                        }
                                                    }

                                                }
                                                catch (Exception ex)
                                                {
                                                    new Logger().LogException(ex);
                                                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (dict.Keys.Contains("file"))
                                    {
                                        var pdfReaderPath = dict["file"];
                                        string pdfRead = pdfReaderPath.ToString();
                                        var startTag = "First:"; int startIndex = pdfRead.IndexOf(startTag) + startTag.Length;
                                        int endIndex = pdfRead.IndexOf("Middle:", startIndex);
                                        firstname = pdfRead.Substring(startIndex, endIndex - startIndex);
                                        firstname = Regex.Replace(firstname, @"\t|\n|\r", "");
                                        startTag = "Last:"; startIndex = pdfRead.IndexOf(startTag) + startTag.Length;
                                        endIndex = pdfRead.IndexOf("Title:", startIndex);
                                        lastname = pdfRead.Substring(startIndex, endIndex - startIndex);
                                        lastname = Regex.Replace(lastname, @"\t|\n|\r", "");
                                    }
                                }
                                name = firstname + " " + lastname;
                                var regexItem = new Regex("^[a-zA-Z0-9 ]*$");

                                if (name.Trim() != string.Empty && regexItem.IsMatch(name))
                                {
                                    //emailid = name + "@#######inc.com";
                                    //emailpswd = name + "@#######inc.com";
                                    //slackid = name;
                                    //slackpswd = name;
                                    if (name.Trim().IndexOf(" ") > -1)
                                    {
                                        //username = name.Split(' ')[0][0] + name.Split(' ')[1];
                                        string fmail;
                                        string lmail;
                                        char[] seperator = { ' ' };
                                        String[] fnames = firstname.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                                        String[] lnames = lastname.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                                        if (fnames.Length >= 2)
                                        {
                                            fmail = fnames[0] + fnames[1];
                                        }
                                        else
                                        {
                                            fmail = firstname;
                                        }
                                        if (lnames.Length >= 2)
                                        {
                                            lmail = lnames[0] + lnames[1];
                                        }
                                        else
                                        {
                                            lmail = lastname;
                                        }

                                        username = fmail.ToLower() + '.' + lmail.ToLower();
                                        Variables.xusername = username;
                                        xUser xu = new xUser();
                                        xu.adusername = username;
                                        if (Variables.xuser.Count != 0)
                                        {
                                            Variables.xuser.Append(xu);
                                        }
                                        else
                                        {
                                            Variables.xuser.Add(xu);
                                        }
                                    }
                                    else
                                    {
                                        username = name;
                                        Variables.xusername = username;
                                        xUser xu = new xUser();
                                        xu.adusername = username;
                                        Variables.xuser.Add(xu);
                                    }
                                    try
                                    {
                                        DirectoryEntry deBase = new DirectoryEntry(path, adminuser, adminpswd);
                                        DirectoryEntries tChildren = deBase.Children;

                                        int counter = 0;
                                        foreach (DirectoryEntry tChild in tChildren)
                                        {
                                            if (tChild.Name.ToLower() == "cn=" + username.ToLower())
                                            {
                                                username = username + (counter).ToString();
                                                counter++;
                                            }
                                            else
                                            {

                                            }
                                        }

                                        DirectoryEntry auser = deBase.Children.Add("cn=" + username, "user");
                                        auser.CommitChanges();
                                        auser.Properties[StringHelper.accountname].Value = username;
                                        auser.Properties[StringHelper.givenname].Value = "A";
                                        auser.Properties[StringHelper.sn].Value = username;
                                        auser.Properties[StringHelper.displayname].Value = username;
                                        auser.Properties[StringHelper.userprincipalname].Value = username + "@" + domain;
                                        auser.Properties[StringHelper.pwdlastset].Value = 0;
                                        auser.Properties[StringHelper.useraccountcontrol].Value = 544;
                                        auser.CommitChanges();
                                    }
                                    catch (Exception ex)
                                    {
                                        new Logger().LogException(ex);
                                        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                    }
                                }
                            }
                        }
                    }
                    else if (DomainStatus == "Other")
                    {
                        //DirectoryEntry adUserFolder = new DirectoryEntry(path, adminuser, adminpswd);
                        //DirectoryEntry newUser = adUserFolder.Children.Add("CN=" + newusername, "User");
                        //try
                        //{
                        //    newUser.Properties[StringHelper.givenname].Value = newusername;
                        //    newUser.CommitChanges();
                        //    newUser.Invoke("setPassword", newpassword);
                        //    newUser.Properties[StringHelper.useraccountcontrol].Value = 0x0200;
                        //    newUser.CommitChanges();
                        //}
                        //catch (Exception ex)
                        //{
                        //    new Logger().LogException(ex);
                        //    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                        //    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                        //    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        //    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        //}
                        List<string> dcs = path.Split(new string[] { "DC=" }, StringSplitOptions.None).ToList();
                        dcs.RemoveAt(0);
                        string domain = string.Join(".", dcs);
                        domain = domain.Replace(",", "");


                        if (newusername != string.Empty)
                        {
                            xUser xu = new xUser();
                            xu.adusername = newusername;
                            Variables.xuser.Add(xu);
                            Variables.xusername = newusername;

                            DirectoryEntry deBase = new DirectoryEntry(path);
                            DirectoryEntry auser = deBase.Children.Add("cn=" + newusername, "user");
                            auser.CommitChanges();
                            auser.Properties[StringHelper.accountname].Value = newusername;
                            auser.Properties[StringHelper.givenname].Value = "A";
                            auser.Properties[StringHelper.sn].Value = newusername;
                            auser.Properties[StringHelper.displayname].Value = newusername;
                            auser.Properties[StringHelper.userprincipalname].Value = newusername + "@" + domain;
                            auser.Properties[StringHelper.pwdlastset].Value = 0;
                            auser.Properties[StringHelper.useraccountcontrol].Value = 544;
                            auser.CommitChanges();
                        }
                        else
                        {
                            foreach (var dict in inputArray1)
                            {
                                string firstname = string.Empty;
                                string name = string.Empty;
                                string lastname = string.Empty;
                                string emailid = string.Empty;
                                string emailpswd = string.Empty;
                                string slackid = string.Empty;
                                string slackpswd = string.Empty;
                                string adname = string.Empty;
                                string username = string.Empty;

                                if (dict.Keys.Contains(StringHelper.statusid) && dict[StringHelper.statusid] != null)
                                {
                                    string desc = dict[StringHelper.description];

                                    if (dict[StringHelper.statusid].ToString() == "1")
                                    {
                                        if (dict.Keys.Contains(StringHelper.title) && dict.Keys.Contains(StringHelper.description))
                                        {
                                            if (dict[StringHelper.title].ToLower().IndexOf(StringHelper.requestforeob) > -1 || dict[StringHelper.description].ToLower().IndexOf(StringHelper.eob) > -1)
                                            {
                                                List<string> slashns = desc.Split(new string[] { "\n", "\\n" }, StringSplitOptions.None).ToList();
                                                foreach (string slashn in slashns)
                                                {
                                                    try
                                                    {
                                                        if (slashn.Trim() != string.Empty && slashn.IndexOf(":") > -1)
                                                        {
                                                            string fpart = slashn.Split(':')[0].Trim();
                                                            string spart = slashn.Split(':')[1].Trim();

                                                            if (fpart.ToLower().Trim() == StringHelper.firstname)
                                                            {
                                                                firstname = spart;
                                                            }
                                                            else if (fpart.ToLower().Trim() == StringHelper.lastname)
                                                            {
                                                                lastname = spart;
                                                            }
                                                            else if (fpart.ToLower().Trim() == StringHelper.name)
                                                            {
                                                                name = spart;
                                                            }

                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        new Logger().LogException(ex);
                                                        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                                    }
                                                }
                                                try
                                                {
                                                    using (Npgsql.NpgsqlConnection con = new Npgsql.NpgsqlConnection(Variables.connString))
                                                    {
                                                        using (Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand("UPDATE public.\"Ticket\" set \"Status_Id\"=0 where \"ID\" = " + dict["ID"]))
                                                        {
                                                            cmd.Connection = con;
                                                            con.Open();
                                                            using (Npgsql.NpgsqlDataAdapter ndr = new Npgsql.NpgsqlDataAdapter(cmd))
                                                            {
                                                                cmd.ExecuteNonQuery();
                                                            }
                                                            con.Close();
                                                        }
                                                    }

                                                }
                                                catch (Exception ex)
                                                {
                                                    new Logger().LogException(ex);
                                                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                                }
                                                ticketexists = ticketexists++;
                                            }
                                            else
                                            {
                                                using (Npgsql.NpgsqlConnection con = new Npgsql.NpgsqlConnection(Variables.connString))
                                                {
                                                    using (Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand("UPDATE public.\"Ticket\" set \"Status_Id\"=1 where \"Status_Id\" =0 "))
                                                    {
                                                        cmd.Connection = con;
                                                        con.Open();
                                                        using (Npgsql.NpgsqlDataAdapter ndr = new Npgsql.NpgsqlDataAdapter(cmd))
                                                        {
                                                            cmd.ExecuteNonQuery();
                                                        }
                                                        con.Close();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (dict.Keys.Contains("file"))
                                    {
                                        var pdfReaderPath = dict["file"];
                                        string pdfRead = pdfReaderPath.ToString();
                                        var startTag = "First:"; int startIndex = pdfRead.IndexOf(startTag) + startTag.Length;
                                        int endIndex = pdfRead.IndexOf("Middle:", startIndex);
                                        firstname = pdfRead.Substring(startIndex, endIndex - startIndex);
                                        firstname = Regex.Replace(firstname, @"\t|\n|\r", "");
                                        startTag = "Last:"; startIndex = pdfRead.IndexOf(startTag) + startTag.Length;
                                        endIndex = pdfRead.IndexOf("Title:", startIndex);
                                        lastname = pdfRead.Substring(startIndex, endIndex - startIndex);
                                        lastname = Regex.Replace(lastname, @"\t|\n|\r", "");
                                    }
                                }
                                name = firstname + " " + lastname;
                                var regexItem = new Regex("^[a-zA-Z0-9 ]*$");

                                if (name.Trim() != string.Empty && regexItem.IsMatch(name))
                                {
                                    //emailid = name + "@#######inc.com";
                                    //emailpswd = name + "@#######inc.com";
                                    //slackid = name;
                                    //slackpswd = name;
                                    if (name.Trim().IndexOf(" ") > -1)
                                    {
                                        //username = name.Split(' ')[0][0] + name.Split(' ')[1];
                                        string fmail;
                                        string lmail;
                                        char[] seperator = { ' ' };
                                        String[] fnames = firstname.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                                        String[] lnames = lastname.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                                        if (fnames.Length >= 2)
                                        {
                                            fmail = fnames[0] + fnames[1];
                                        }
                                        else
                                        {
                                            fmail = firstname;
                                        }
                                        if (lnames.Length >= 2)
                                        {
                                            lmail = lnames[0] + lnames[1];
                                        }
                                        else
                                        {
                                            lmail = lastname;
                                        }

                                        username = fmail.ToLower() + '.' + lmail.ToLower();
                                        Variables.xusername = username;
                                        xUser xu = new xUser();
                                        xu.adusername = username;
                                        if (Variables.xuser.Count != 0)
                                        {
                                            Variables.xuser.Append(xu);
                                        }
                                        else
                                        {
                                            Variables.xuser.Add(xu);
                                        }
                                    }
                                    else
                                    {
                                        username = name;
                                        Variables.xusername = username;
                                        xUser xu = new xUser();
                                        xu.adusername = username;
                                        Variables.xuser.Add(xu);
                                    }
                                    try
                                    {
                                        DirectoryEntry deBase = new DirectoryEntry(path, adminuser, adminpswd);
                                        DirectoryEntries tChildren = deBase.Children;

                                        int counter = 0;
                                        foreach (DirectoryEntry tChild in tChildren)
                                        {
                                            if (tChild.Name.ToLower() == "cn=" + username.ToLower())
                                            {
                                                username = username + (counter).ToString();
                                                counter++;
                                            }
                                            else
                                            {

                                            }
                                        }

                                        DirectoryEntry auser = deBase.Children.Add("cn=" + username, "user");
                                        auser.CommitChanges();
                                        auser.Properties[StringHelper.accountname].Value = username;
                                        auser.Properties[StringHelper.givenname].Value = "A";
                                        auser.Properties[StringHelper.sn].Value = username;
                                        auser.Properties[StringHelper.displayname].Value = username;
                                        auser.Properties[StringHelper.userprincipalname].Value = username + "@" + domain;
                                        auser.Properties[StringHelper.pwdlastset].Value = 0;
                                        auser.Properties[StringHelper.useraccountcontrol].Value = 544;
                                        auser.CommitChanges();
                                    }
                                    catch (Exception ex)
                                    {
                                        new Logger().LogException(ex);
                                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                    }
                                    ticketexists = ticketexists++;
                                }
                            }
                        }
                    }
                    result = "Success";
                    ret = StringHelper.success;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(output, result, "", true, "datatable", step.Robot_Id, executionID);
                new ExecutionLog().add2Log("LDAP Create user is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            if (ticketexists==0) {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                //mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(output, result, "", true, "datatable", step.Robot_Id, executionID);
                new ExecutionLog().add2Log("\n There is no valid data or valid tickets \n", step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            return mr;
        }

        public MethodResult UpdateUser( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string inputtype = string.Empty; string input = string.Empty; string output = string.Empty; string username = string.Empty; string newpassword = string.Empty;
            string result = string.Empty;
            try
            {

                inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                username = stp.getPropertiesValue(StringHelper.username, step);
                newpassword = stp.getPropertiesValue(StringHelper.newpassword, step);
                input = stp.getPropertiesValue(StringHelper.input, step);


                Variables.xuser.Clear();
                DVariables valuer = new DVariables(); List<Dictionary<string, dynamic>> inputArray1 = new List<Dictionary<string, dynamic>>();
                if (input.Trim() != string.Empty)
                {
                    valuer = new VariableHandler().getVariables(input, executionID);
                    if (valuer != null && valuer.vlvalue.GetType() == typeof(string))
                    {
                        System.Data.DataTable dtr = JsonConvert.DeserializeObject<System.Data.DataTable>(valuer.vlvalue);
                        inputArray1 = dtr.AsEnumerable().Select(row => dtr.Columns.Cast<DataColumn>().ToDictionary(column => column.ColumnName, column => row[column] as dynamic)).ToList();
                    }
                }

                DVariables value = new DVariables();
                value = new VariableHandler().getVariables(StringHelper.ldapconnection, executionID);
                if (value != null && value.vlvalue.GetType() == typeof(LDAPConnection))
                {
                    string decodePath = ((LDAPConnection)value.vlvalue).LDAPPath;
                    string path = Base64Decode(decodePath);
                    string adminuser = ((LDAPConnection)value.vlvalue).LDAPAdminUserName;
                    string adminpswd = ((LDAPConnection)value.vlvalue).LDAPAdminPassword;
                    string DomainStatus = ((LDAPConnection)value.vlvalue).ConnectionDomain;
                    string ldapURL = ((LDAPConnection)value.vlvalue).ldapURl;
                    string OU = ((LDAPConnection)value.vlvalue).OU;
                    string DC = ((LDAPConnection)value.vlvalue).DC;

                    string FinalPath = ldapURL + username + "," + OU + DC;
                    DirectoryEntry uEntry = new DirectoryEntry(FinalPath, adminuser, adminpswd);
                    uEntry.Invoke("SetPassword", new object[] { newpassword });
                    uEntry.Properties["LockOutTime"].Value = 0; //unlock account

                    uEntry.Close();
                    result = "Success";
                }

                ret = StringHelper.success;
            }

            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(output, result, "", true, "datatable", step.Robot_Id, executionID);
                new ExecutionLog().add2Log("LDAP update user is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult DeleteUser( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string inputtype = string.Empty; string input = string.Empty; string output = string.Empty; string username = string.Empty; string newpassword = string.Empty;
            string result = string.Empty;
            try
            {

                inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                username = stp.getPropertiesValue(StringHelper.username, step);
                newpassword = stp.getPropertiesValue(StringHelper.newpassword, step);
                input = stp.getPropertiesValue(StringHelper.input, step);


                Variables.xuser.Clear();
                DVariables valuer = new DVariables(); List<Dictionary<string, dynamic>> inputArray1 = new List<Dictionary<string, dynamic>>();
                if (input.Trim() != string.Empty)
                {
                    valuer = new VariableHandler().getVariables(input, executionID);
                    if (valuer != null && valuer.vlvalue.GetType() == typeof(string))
                    {
                        System.Data.DataTable dtr = JsonConvert.DeserializeObject<System.Data.DataTable>(valuer.vlvalue);
                        inputArray1 = dtr.AsEnumerable().Select(row => dtr.Columns.Cast<DataColumn>().ToDictionary(column => column.ColumnName, column => row[column] as dynamic)).ToList();
                    }
                }

                DVariables value = new DVariables();
                value = new VariableHandler().getVariables(StringHelper.ldapconnection, executionID);
                if (value != null && value.vlvalue.GetType() == typeof(LDAPConnection) && username!= "")
                {
                    string decodePath = ((LDAPConnection)value.vlvalue).LDAPPath;
                    string path = Base64Decode(decodePath);
                    string adminuser = ((LDAPConnection)value.vlvalue).LDAPAdminUserName;
                    string adminpswd = ((LDAPConnection)value.vlvalue).LDAPAdminPassword;
                    string DomainStatus = ((LDAPConnection)value.vlvalue).ConnectionDomain;
                    string ldapURL = ((LDAPConnection)value.vlvalue).ldapURl;
                    string OU = ((LDAPConnection)value.vlvalue).OU;
                    string DC = ((LDAPConnection)value.vlvalue).DC;

                    string FinalPath = ldapURL + "CN=" + username + "," + OU + DC;
                    DirectoryEntry user = new DirectoryEntry(FinalPath, adminuser, adminpswd);
                    DirectoryEntry ou = user.Parent;
                    ou.Children.Remove(user);
                    ou.CommitChanges();

                    result = "Success";
                }
                else {
                    foreach (var dict in inputArray1)
                    {
                        string name = string.Empty;
                        if (dict.Keys.Contains(StringHelper.statusid) && dict[StringHelper.statusid] != null)
                        {
                            if (dict[StringHelper.statusid].ToString() == "1")
                            {
                                if (dict.Keys.Contains(StringHelper.title) && dict.Keys.Contains(StringHelper.description))
                                {
                                    if (dict[StringHelper.title].ToLower().IndexOf(StringHelper.requestforeoffb) >= -1 || dict[StringHelper.description].ToLower().IndexOf(StringHelper.requestforeoffb) >= -1)
                                    {
                                        string desc = dict[StringHelper.description];

                                        List<string> slashns = desc.Split(new string[] { "\n", "\\n" }, StringSplitOptions.None).ToList();
                                        foreach (string slashn in slashns)
                                        {
                                            try
                                            {
                                                if (slashn.Trim() != string.Empty && slashn.IndexOf(":") > -1)
                                                {
                                                    string fpart = slashn.Split(':')[0].Trim();
                                                    string spart = slashn.Split(':')[1].Trim();

                                                    if (fpart.ToLower().Trim() == StringHelper.name)
                                                    {
                                                        name = spart;
                                                    }

                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                new Logger().LogException(ex);
                                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                            }
                                        }
                                        try
                                        {
                                            using (Npgsql.NpgsqlConnection con = new Npgsql.NpgsqlConnection(Variables.connString))
                                            {
                                                using (Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand("UPDATE public.\"Ticket\" set \"Status_Id\"=0 where \"ID\" = " + dict["ID"]))
                                                {
                                                    cmd.Connection = con;
                                                    con.Open();
                                                    using (Npgsql.NpgsqlDataAdapter ndr = new Npgsql.NpgsqlDataAdapter(cmd))
                                                    {
                                                        cmd.ExecuteNonQuery();
                                                    }
                                                    con.Close();
                                                }
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            new Logger().LogException(ex);
                                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                        }
                                        
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (dict.Keys.Contains("Body"))
                            {
                                var pdfReaderPath = dict["Body"];
                                string[] strArray = pdfReaderPath.Split('\n');
    
                                foreach (string namePart in strArray)
                                {
                                    try
                                    {
                                        if (namePart.Trim() != string.Empty && namePart.IndexOf(":") > -1)
                                        {
                                            string fpart = namePart.Split(':')[0].Trim();
                                            string spart = namePart.Split(':')[1].Trim();

                                            if (fpart.ToLower().Trim() == StringHelper.name)
                                            {
                                                name = spart;
                                            }

                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        new Logger().LogException(ex);
                                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                    }
                                }
                            }
                        }

                        var regexItem = new Regex("^[a-zA-Z0-9 ].*$");

                        if (name.Trim() != string.Empty && regexItem.IsMatch(name))
                        {
                            try
                            {
                                string decodePath = ((LDAPConnection)value.vlvalue).LDAPPath;
                                string path = Base64Decode(decodePath);
                                string adminuser = ((LDAPConnection)value.vlvalue).LDAPAdminUserName;
                                string adminpswd = ((LDAPConnection)value.vlvalue).LDAPAdminPassword;
                                string DomainStatus = ((LDAPConnection)value.vlvalue).ConnectionDomain;
                                string ldapURL = ((LDAPConnection)value.vlvalue).ldapURl;
                                string OU = ((LDAPConnection)value.vlvalue).OU;
                                string DC = ((LDAPConnection)value.vlvalue).DC;

                                string FinalPath = ldapURL + "CN=" + name + "," + OU + DC;
                                DirectoryEntry user = new DirectoryEntry(FinalPath, adminuser, adminpswd);
                                DirectoryEntry ou = user.Parent;
                                ou.Children.Remove(user);
                                ou.CommitChanges();

                                result = "Success";
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                            }
                        }
                    }
                }

                ret = StringHelper.success;
            }

            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(output, result, "", true, "datatable", step.Robot_Id, executionID);
                new ExecutionLog().add2Log("LDAP delete user is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
    }


}
public class LDAPConnection
{
    public string LDAPPath { get; set; }
    public string LDAPAdminUserName { get; set; }
    public string LDAPAdminPassword { get; set; }
    public string ConnectionDomain { get; set; }
    public string ldapURl { get; set; }
    //Organizational Unit
    public string OU { get; set; }
    //Domain Component
    public string DC { get; set; }
}
