﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using Genbase.classes;
using Newtonsoft.Json;
using System.Diagnostics;
using Genbase.Classes;

namespace Genbase.BLL.ElementsClasses
{
    public class SharedFolder
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        VariableHandler vh = new VariableHandler();
        TypeHandler th = new TypeHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public byte[] DownloadFileByte(string SourceFile, string networkPath, NetworkCredential credentials)
        {
            byte[] fileBytes = null; string myNetworkPath = string.Empty;

            using (new ConnectToSharedFolder(networkPath, credentials))
            {

                myNetworkPath = System.IO.Path.Combine(networkPath, SourceFile);

                try
                {
                    fileBytes = System.IO.File.ReadAllBytes(myNetworkPath);
                }
                catch (Exception ex)
                {
                    string Message = ex.Message.ToString();
                }
            }

            return fileBytes;
        }

        public async void FileUpload(string UploadFilePath, string networkPath, NetworkCredential credentials)
        {
            string myNetworkPath = networkPath;

            List<string> fnameParts = UploadFilePath.Split(new string[] { "\\" }, StringSplitOptions.None).ToList();
            string filename = fnameParts[fnameParts.Count - 1];

            try
            {
                using (new ConnectToSharedFolder(networkPath, credentials))
                {
                    myNetworkPath = myNetworkPath + "\\" + filename;

                    byte[] file = System.IO.File.ReadAllBytes(UploadFilePath);

                    using (FileStream fileStream = System.IO.File.Create(myNetworkPath, file.Length))
                    {
                        await fileStream.WriteAsync(file, 0, file.Length);
                        fileStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string Message = ex.Message.ToString();
            }
        }

        public MethodResult Login( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            DataTable returnValue = new DataTable();

            string Input = string.Empty; string Output = string.Empty;
            string Username = string.Empty; string Password = string.Empty; string IPAddress = string.Empty;
            string networkPath = string.Empty;
            Input = stp.getPropertiesValue(StringHelper.input, step);
            Output = stp.getPropertiesValue(StringHelper.output, step);
            Username = stp.getPropertiesValue(StringHelper.username, step);
            Password = stp.getPropertiesValue(StringHelper.password, step);
            //IPAddress = stp.getPropertiesValue(StringHelper.ipaddress, step);
            IPAddress = stp.getPropertiesValue(StringHelper.ipadress, step);



            try
            {
                
                if((Input.Trim() !=string.Empty) && (vh.checkVariableExists(Input, executionID)))
                    {
                    DataTable inpt = new DataTable();
                    inpt = new TypeHandler().getInput(Input, executionID);

                    foreach (DataRow dr in inpt.Rows)
                    {
                        //Username = dr["Username"].ToString();
                        //Password = dr["Password"].ToString();
                        IPAddress = dr["Result"].ToString();
                    }
                    returnValue.Columns.Add("IPAddress"); returnValue.Columns.Add("UserName"); returnValue.Columns.Add("Password");

                    NetworkCredential credentials = new NetworkCredential(Username, Password);

                    List<string> fileList = new List<string>();

                    using (new ConnectToSharedFolder(IPAddress, credentials))
                    {
                        fileList = Directory.GetFiles(IPAddress).ToList();

                        returnValue.Rows.Add(IPAddress, Username, Password);
                    }
                }
                else {
                    

                    returnValue.Columns.Add("IPAddress"); returnValue.Columns.Add("UserName"); returnValue.Columns.Add("Password");

                    NetworkCredential credentials = new NetworkCredential(Username, Password);

                    List<string> fileList = new List<string>();

                    using (new ConnectToSharedFolder(IPAddress, credentials))
                    {
                        fileList = Directory.GetFiles(IPAddress).ToList();

                        returnValue.Rows.Add(IPAddress, Username, Password);
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Upload( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            DataTable returnValue = new DataTable();
            returnValue.Columns.Add("Files");

            string Input = string.Empty; string Output = string.Empty;
            string SourcewithFile = string.Empty; string Destination = string.Empty;
            string RemoveOriginal = string.Empty;

            try
            {
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                SourcewithFile = stp.getPropertiesValue("SourcewithFile", step);
                Destination = stp.getPropertiesValue(StringHelper.destination, step);
                RemoveOriginal = stp.getPropertiesValue("RemoveOriginal", step);

                if (vh.checkVariableExists(Input, executionID))
                {
                    DataTable inpt = new DataTable();
                    inpt = new TypeHandler().getInput(Input, executionID);

                    foreach (DataRow dr in inpt.Rows)
                    {
                        string Username = dr["Username"].ToString();
                        string Password = dr["Password"].ToString();
                        string IPAddress = dr["IPAddress"].ToString();

                        if (Destination.Trim() != string.Empty)
                        {
                            IPAddress = IPAddress + "\\" + Destination;
                        }

                        NetworkCredential credentials = new NetworkCredential(Username, Password);

                        if (vh.checkVariableExists(SourcewithFile, executionID))
                        {
                            DataTable files = new DataTable();
                            files = new TypeHandler().getInput(SourcewithFile, executionID);

                            foreach (DataRow filerow in files.Rows)
                            {
                                FileUpload(filerow["Files"].ToString(), IPAddress, credentials);
                            }
                        }
                        else
                        {
                            FileUpload(SourcewithFile, IPAddress, credentials);
                        }
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Download( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            DataTable returnValue = new DataTable();
            returnValue.Columns.Add("Files");

            List<string> SplitInputs = new List<string>();
            string Input = string.Empty; string Output = string.Empty;
            string Source = string.Empty; string Destination = string.Empty;
            string RemoveOriginal = string.Empty;
            Input = stp.getPropertiesValue(StringHelper.input, step);
            Output = stp.getPropertiesValue(StringHelper.output, step);
            Source = stp.getPropertiesValue("Source", step);
            //Destination = stp.getPropertiesValue(StringHelper.destination, step);
            Destination = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, StringHelper.uploads);
            RemoveOriginal = stp.getPropertiesValue("RemoveOriginal", step);
            try
            {
                


                if (vh.checkVariableExists(Input, executionID))
                {
                    DataTable inpt = new DataTable();
                    inpt = new TypeHandler().getInput(Input, executionID);

                    foreach (DataRow dr in inpt.Rows)
                    {

                        string Username = dr["Username"].ToString();
                        string Password = dr["Password"].ToString();
                        string IPAddress = dr["IPAddress"].ToString();

                        NetworkCredential credentials = new NetworkCredential(Username, Password);

                        List<string> fnameParts = Source.Split(new string[] { "\\" }, StringSplitOptions.None).ToList();
                        string filename = fnameParts[fnameParts.Count - 1];
                        // string filename = Source;
                        string DestZFileName = Path.Combine(Destination, filename);

                        //using (FileStream file = System.IO.File.Create(DestZFileName))
                        //{
                            
                        //}
                        
                        ////fnameParts.RemoveAt(fnameParts.Count);
                        ////string sourcewithoutfile = string.Join("\\", fnameParts.ToArray());

                        System.IO.File.WriteAllBytes(DestZFileName, DownloadFileByte(Source, IPAddress, credentials));

                        
                        //FileStream input = null;
                        //FileStream output = null;
                        //try
                        //{
                        //    input = new FileStream(Source, FileMode.Open);
                        //    output = new FileStream(DestZFileName, FileMode.Create, FileAccess.ReadWrite);

                        //    byte[] buffer = new byte[32768];
                        //    int read;
                        //    while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                        //    {
                        //        output.Write(buffer, 0, read);
                        //    }
                        //}
                        //catch (Exception e)
                        //{
                        //}
                        //finally
                        //{
                        //    input.Close();
                        //    input.Dispose();
                        //    output.Close();
                        //    output.Dispose();
                        //}
                        returnValue.Rows.Add(DestZFileName);
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult GetFolderDetails( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            DataTable returnValue = new DataTable();


            List<string> SplitInputs = new List<string>();
            string Input = string.Empty; string Output = string.Empty;
            string FileType = string.Empty; 

            try
            {
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                FileType = stp.getPropertiesValue(StringHelper.filetype, step);

                if (vh.checkVariableExists(Input, executionID))
                {
                    DataTable inpt = new DataTable();
                    inpt = new TypeHandler().getInput(Input, executionID);

                    foreach (DataRow dr in inpt.Rows)
                    {

                        string Username = dr["Username"].ToString();
                        string Password = dr["Password"].ToString();
                        string IPAddress = dr["IPAddress"].ToString();

                        NetworkCredential credentials = new NetworkCredential(Username, Password);

                        List<string> fileList = new List<string>();

                        using (new ConnectToSharedFolder(IPAddress, credentials))
                        {
                            //fileList = Directory.GetFiles(IPAddress).ToList();
                            //DirectoryInfo d = new DirectoryInfo(IPAddress);//Assuming Test is your Folder
                            //FileInfo[] Files = d.GetFiles("*.*"); //Getting All files

                            List<FileInfo> FilesList = new DirectoryInfo(IPAddress).GetFiles(FileType).OrderByDescending(f => f.LastWriteTime).ToList();

                            returnValue.Columns.Add("Files"); returnValue.Columns.Add("CreateTime"); returnValue.Columns.Add("DirectoryName");
                            returnValue.Columns.Add("Extension"); returnValue.Columns.Add("FullName"); returnValue.Columns.Add("LastAccessTime");
                            returnValue.Columns.Add("LastWriteTime"); returnValue.Columns.Add("Length"); returnValue.Columns.Add("Name");

                            FilesList.ForEach(file => returnValue.Rows.Add(file.FullName, file.CreationTime, file.DirectoryName, file.Extension, file.FullName, file.LastAccessTime, file.LastWriteTime, file.Length, file.Name));

                        }
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
    }
    public class ConnectToSharedFolder : IDisposable
    {
        readonly string _networkName;

        public ConnectToSharedFolder(string networkName, NetworkCredential credentials)
        {
            _networkName = networkName;

            var netResource = new NetResource
            {
                Scope = ResourceScope.GlobalNetwork,
                ResourceType = ResourceType.Disk,
                DisplayType = ResourceDisplaytype.Share,
                RemoteName = networkName
            };

            var userName = string.IsNullOrEmpty(credentials.Domain)
                ? credentials.UserName
                : string.Format(@"{0}\{1}", credentials.Domain, credentials.UserName);

            var result = WNetAddConnection2(
                netResource,
                credentials.Password,
                userName,
                0);

            if (result != 0)
            {
                //throw new Win32Exception(result, "Error connecting to remote share");
            }
        }

        ~ConnectToSharedFolder()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            WNetCancelConnection2(_networkName, 0, true);
        }

        [DllImport("mpr.dll")]
        private static extern int WNetAddConnection2(NetResource netResource,
            string password, string username, int flags);

        [DllImport("mpr.dll")]
        private static extern int WNetCancelConnection2(string name, int flags,
            bool force);

        [StructLayout(LayoutKind.Sequential)]
        public class NetResource
        {
            public ResourceScope Scope;
            public ResourceType ResourceType;
            public ResourceDisplaytype DisplayType;
            public int Usage;
            public string LocalName;
            public string RemoteName;
            public string Comment;
            public string Provider;
        }

        public enum ResourceScope : int
        {
            Connected = 1,
            GlobalNetwork,
            Remembered,
            Recent,
            Context
        };

        public enum ResourceType : int
        {
            Any = 0,
            Disk = 1,
            Print = 2,
            Reserved = 8,
        }

        public enum ResourceDisplaytype : int
        {
            Generic = 0x0,
            Domain = 0x01,
            Server = 0x02,
            Share = 0x03,
            File = 0x04,
            Group = 0x05,
            Network = 0x06,
            Root = 0x07,
            Shareadmin = 0x08,
            Directory = 0x09,
            Tree = 0x0a,
            Ndscontainer = 0x0b
        }
    }
    public class NetworkDrive
    {
        public enum ResourceScope
        {
            RESOURCE_CONNECTED = 1,
            RESOURCE_GLOBALNET,
            RESOURCE_REMEMBERED,
            RESOURCE_RECENT,
            RESOURCE_CONTEXT
        }

        public enum ResourceType
        {
            RESOURCETYPE_ANY,
            RESOURCETYPE_DISK,
            RESOURCETYPE_PRINT,
            RESOURCETYPE_RESERVED
        }

        public enum ResourceUsage
        {
            RESOURCEUSAGE_CONNECTABLE = 0x00000001,
            RESOURCEUSAGE_CONTAINER = 0x00000002,
            RESOURCEUSAGE_NOLOCALDEVICE = 0x00000004,
            RESOURCEUSAGE_SIBLING = 0x00000008,
            RESOURCEUSAGE_ATTACHED = 0x00000010,
            RESOURCEUSAGE_ALL = (RESOURCEUSAGE_CONNECTABLE | RESOURCEUSAGE_CONTAINER | RESOURCEUSAGE_ATTACHED),
        }

        public enum ResourceDisplayType
        {
            RESOURCEDISPLAYTYPE_GENERIC,
            RESOURCEDISPLAYTYPE_DOMAIN,
            RESOURCEDISPLAYTYPE_SERVER,
            RESOURCEDISPLAYTYPE_SHARE,
            RESOURCEDISPLAYTYPE_FILE,
            RESOURCEDISPLAYTYPE_GROUP,
            RESOURCEDISPLAYTYPE_NETWORK,
            RESOURCEDISPLAYTYPE_ROOT,
            RESOURCEDISPLAYTYPE_SHAREADMIN,
            RESOURCEDISPLAYTYPE_DIRECTORY,
            RESOURCEDISPLAYTYPE_TREE,
            RESOURCEDISPLAYTYPE_NDSCONTAINER
        }

        [StructLayout(LayoutKind.Sequential)]
        private class NETRESOURCE
        {
            public ResourceScope dwScope = 0;
            public ResourceType dwType = 0;
            public ResourceDisplayType dwDisplayType = 0;
            public ResourceUsage dwUsage = 0;
            public string lpLocalName = null;
            public string lpRemoteName = null;
            public string lpComment = null;
            public string lpProvider = null;
        }

        [DllImport("mpr.dll")]
        private static extern int WNetAddConnection2(NETRESOURCE lpNetResource, string lpPassword, string lpUsername, int dwFlags);

        public int MapNetworkDrive(string unc, string drive, string user, string password)
        {
            NETRESOURCE myNetResource = new NETRESOURCE();
            myNetResource.lpLocalName = drive;
            myNetResource.lpRemoteName = unc;
            myNetResource.lpProvider = null;
            int result = WNetAddConnection2(myNetResource, password, user, 0);
            return result;
        }
    }
    public class UNCAccess
    {
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        internal struct USE_INFO_2
        {
            internal String ui2_local;
            internal String ui2_remote;
            internal String ui2_password;
            internal UInt32 ui2_status;
            internal UInt32 ui2_asg_type;
            internal UInt32 ui2_refcount;
            internal UInt32 ui2_usecount;
            internal String ui2_username;
            internal String ui2_domainname;
        }

        [DllImport("NetApi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        internal static extern UInt32 NetUseAdd(
            String UncServerName,
            UInt32 Level,
            ref USE_INFO_2 Buf,
            out UInt32 ParmError);

        private string sUNCPath;
        private string sUser;
        private string sPassword;
        private string sDomain;
        private int iLastError;
        public UNCAccess()
        {
        }
        public UNCAccess(string UNCPath, string User, string Domain, string Password)
        {
            login(UNCPath, User, Domain, Password);
        }
        public int LastError
        {
            get { return iLastError; }
        }

        /// <summary>
        /// Connects to a UNC share folder with credentials
        /// </summary>
        /// <param name="UNCPath">UNC share path</param>
        /// <param name="User">Username</param>
        /// <param name="Domain">Domain</param>
        /// <param name="Password">Password</param>
        /// <returns>True if login was successful</returns>
        public bool login(string UNCPath, string User, string Domain, string Password)
        {
            sUNCPath = UNCPath;
            sUser = User;
            sPassword = Password;
            sDomain = Domain;
            return NetUseWithCredentials();
        }
        private bool NetUseWithCredentials()
        {
            uint returncode;
            try
            {
                USE_INFO_2 useinfo = new USE_INFO_2();

                useinfo.ui2_remote = sUNCPath;
                useinfo.ui2_username = sUser;
                useinfo.ui2_domainname = sDomain;
                useinfo.ui2_password = sPassword;
                useinfo.ui2_asg_type = 0;
                useinfo.ui2_usecount = 1;
                uint paramErrorIndex;
                returncode = NetUseAdd(null, 2, ref useinfo, out paramErrorIndex);
                iLastError = (int)returncode;
                return returncode == 0;
            }
            catch
            {
                iLastError = Marshal.GetLastWin32Error();
                return false;
            }
        }
    }
}