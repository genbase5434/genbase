﻿using Genbase.classes;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Tweetinvi;
using Tweetinvi.Logic.DTO;
using Newtonsoft.Json;
using System.Data;
using System.Security;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.IO;
using System.Diagnostics;
using System.Timers;
using System.Configuration;
using System.Collections.Specialized;
using System.Net;
using Genbase.Classes;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Genbase.BLL.ElementsClasses
{
    public class NLP
    {
        ExecutionLog log = new ExecutionLog();
        VariableHandler vh = new VariableHandler();
        DAL.PostgresConnect dbd = new DAL.PostgresConnect();
        string time = DateTime.Now.ToShortTimeString();
        string date = DateTime.Now.ToShortDateString();
        StepPropertyHandler stp = new StepPropertyHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        DataTable dt = new DataTable();

        public MethodResult MatchDefined(Steps step, string executionID)
        {
            string value = string.Empty; string match = string.Empty; string comparisontype = string.Empty;
            string source = string.Empty; string sourcetype = string.Empty; string comparison = string.Empty;
            string result = string.Empty; string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                comparisontype = stp.getPropertiesValue(StringHelper.comparisontype, step);
                source = stp.getPropertiesValue(StringHelper.source, step);
                sourcetype = stp.getPropertiesValue(StringHelper.sourcetype, step);
                comparison = stp.getPropertiesValue(StringHelper.comparison, step);
                result = stp.getPropertiesValue(StringHelper.result, step);
                if (!string.IsNullOrEmpty(source))
                {
                    //comparison
                    int times = 0;
                    string input = string.Empty;
                    foreach (var l in Variables.dynamicvariables)
                    {
                        if (source == l.vlname)
                        {
                            input = l.vlvalue;
                        }
                    }
                    if (comparisontype.ToLower() == StringHelper.variable)
                    {
                        if (comparison.IndexOf(",") > -1)
                        {
                            List<string> comparisonList = comparison.Split(',').ToList();
                            foreach (string compare in comparisonList)
                            {
                                if (input.ToLower().IndexOf(compare.ToLower()) > -1)
                                {
                                    times++;
                                }
                                else
                                {

                                }
                            }
                            Variables.output = StringHelper.inputvalue + input + " " + times + StringHelper.matched;
                        }
                        else
                        {
                            if (input.ToLower().IndexOf(comparison.ToLower()) > -1)
                            {
                                times++;
                            }
                            Variables.output = StringHelper.inputvalue + input + " " + times + StringHelper.matched;
                        }
                    }

                }
                //if (Variables.mails.Count <= 0 && Variables.isSocial.Count < 0)
                //{
                //    Variables.downloadattachments.Clear(); Variables.isSocial.Clear(); Variables.mails.Clear();
                //}
                //if (Variables.mails.Count > 0)
                //{
                //    Variables.downloadattachments.Clear(); Variables.isSocial.Clear();
                //    List<MimeMessage> atMails = new List<MimeMessage>();
                //    atMails = Variables.mails;
                //    int times = 0;
                //    foreach (MimeMessage message in atMails)
                //    {
                //        string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                //        if (comparisontype.ToLower() == StringHelper.value)
                //        {
                //            times = 0;
                //            if (comparison.IndexOf(",") > -1)
                //            {
                //                List<string> comparisonList = comparison.Split(',').ToList();
                //                foreach (string compare in comparisonList)
                //                {
                //                    if (messagepart.ToLower().IndexOf(compare.ToLower()) > -1)
                //                    {
                //                        times++;
                //                    }
                //                }
                //            }
                //            else
                //            {
                //                if (messagepart.ToLower().IndexOf(comparison.ToLower()) > -1)
                //                {
                //                    Variables.downloadattachments.Add(message);
                //                }
                //                else if (comparison.Trim() == "*")
                //                {
                //                    Variables.downloadattachments.Add(message);
                //                }
                //            }
                //            if (times >= 1)
                //            {
                //                Variables.downloadattachments.Add(message);
                //            }
                //        }
                //    }
                //    log.add2Log(Messages.matchResultMails.Replace("---", Variables.downloadattachments.Count.ToString()).Replace("***", Variables.mails.Count.ToString()), step.Robot_Id, step.Name, "700", false);
                //    Variables.mails.Clear();
                //}
                if (Variables.isSocial.Count >= 1)
                {
                    Variables.isSocialUpate.Clear(); int times = 0;
                    foreach (var tweet in Variables.isSocial)
                    {
                        times = 0;
                        if (comparison.IndexOf(",") > -1)
                        {
                            List<string> comparisonList = comparison.Split(',').ToList();
                            foreach (string compare in comparisonList)
                            {
                                if (tweet.Text.ToString().ToLower().IndexOf(compare.ToLower()) > -1)
                                {
                                    times++;
                                }
                            }
                        }
                        else
                        {
                            if (tweet.Text.ToString().ToLower().IndexOf(comparison.ToLower().Trim()) > -1)
                            {
                                Variables.isSocialUpate.Add(tweet);
                            }
                            else if (comparison.Trim() == "*")
                            {
                                Variables.isSocialUpate.Add(tweet);
                            }
                            else
                            {

                            }
                        }
                        if (times >= 1)
                        {
                            Variables.isSocialUpate.Add(tweet);
                        }
                    }
                    log.add2Log(Messages.matchResultTweets.Replace("---", Variables.isSocialUpate.Count.ToString()).Replace("***", Variables.isSocial.Count.ToString()), step.Robot_Id, step.Name, "700", false);
                    Variables.isSocial = Variables.isSocialUpate;
                }
                if (Variables.readOutput.Count >= 1)
                {
                    Variables.readOutputupdate.Clear(); int times = 0;
                    foreach (var text in Variables.readOutput)
                    {
                        times = 0;
                        if (comparison.IndexOf(",") > -1)
                        {
                            List<string> comparisonList = comparison.Split(',').ToList();
                            foreach (string compare in comparisonList)
                            {
                                if (text.ToString().ToLower().IndexOf(compare.ToLower()) > -1)
                                {
                                    times++;
                                }
                            }
                        }
                        else
                        {
                            if (text.ToString().ToLower().IndexOf(comparison.ToLower()) > -1)
                            {
                                Variables.readOutputupdate.Add(text);
                            }
                            else if (comparison.Trim() == "*")
                            {
                                Variables.readOutputupdate.Add(text);
                            }
                            else
                            {

                            }
                        }
                        if (times >= 1)
                        {
                            Variables.readOutputupdate.Add(text);
                        }
                    }

                    log.add2Log(Messages.matchResultFiles.Replace("---", Variables.readOutputupdate.Count.ToString()).Replace("***", Variables.readOutput.Count.ToString()), step.Robot_Id, step.Name, "700", false);
                }
                if (Variables.fbOutput.Count >= 1)
                {
                    int times = 0;
                    Variables.fbOutputUpdate.Clear();
                    foreach (var post in Variables.fbOutput)
                    {
                        times = 0;
                        string message = post.PostMessage + post.PostStory;
                        if (comparison.IndexOf(",") > -1)
                        {
                            List<string> comparisonList = comparison.Split(',').ToList();
                            foreach (string compare in comparisonList)
                            {
                                if (message.ToString().ToLower().IndexOf(compare.ToLower()) > -1)
                                {
                                    times++;
                                }
                            }
                        }
                        else
                        {
                            if (message.ToString().ToLower().IndexOf(comparison.ToLower()) > -1)
                            {
                                Variables.fbOutputUpdate.Add(post);
                            }
                            else if (comparison.Trim() == "*")
                            {
                                Variables.fbOutputUpdate.Add(post);
                            }
                        }
                        if (times >= 1)
                        {
                            Variables.fbOutputUpdate.Add(post);
                        }
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult ClassifyDefined(Steps step, string executionID)
        {

            Variables.classifiedData.Clear();
            List<KeyValuePair<string, string>> inputs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                foreach (MimeMessage message in Variables.downloadattachments)
                {
                    string messageSubject = message.Subject;
                    string messageDescription = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text;
                    string sender = string.Empty;

                    foreach (InternetAddress mailFrom in message.From)
                    {
                        sender = sender + " " + ((MimeKit.MailboxAddress)mailFrom).Address;
                    }

                    inputs.Add(new KeyValuePair<string, string>(sender + " | Email", messageSubject + messageDescription));
                    using (System.Net.Mail.MailMessage testMailMessage = new System.Net.Mail.MailMessage())
                    {
                        testMailMessage.From = new MailAddress("#######drive@gamil.com");


                        testMailMessage.To.Add(new MailAddress(sender));
                        testMailMessage.Subject = StringHelper.acknowledgement;


                        testMailMessage.Body = string.Format("Hi {0},<br /><br /><br /><br /> Your Complaint has been Registered, we will take it to concerned Department soon. <br /><br />Thank You.", sender, sender);
                        testMailMessage.IsBodyHtml = true;

                        testMailMessage.Priority = MailPriority.Normal;
                        testMailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
                        testMailMessage.BodyEncoding = System.Text.Encoding.UTF8;
                        System.Net.Mail.SmtpClient testSmtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
                        testSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        testSmtp.UseDefaultCredentials = false;
                        testSmtp.EnableSsl = true;

                        testSmtp.Credentials = new System.Net.NetworkCredential("test#######service@gmail.com", "test#######service!#$^");
                        testSmtp.Send(testMailMessage);
                    }
                }
                foreach (TweetDTO message in Variables.isSocial)
                {
                    string messageSender = message.CreatedBy.Name;
                    string messageDescription = message.Text;
                    inputs.Add(new KeyValuePair<string, string>(messageSender + " | Twitter", messageDescription));
                    Auth.SetUserCredentials("aQVZd0TYNDn2m0vhgEyeRu3Od", "IKCmKry1BpHTsFHZQcRUuhTgO5WJ4vJQraXHmd2ciJ0yQkDQEf", "963470373734895616-2QhgN7sDHl9TLSN2o4EkvPj53k9mdsa", "0phKl7pqBeuag9yc0zh7fgt6ApAIEY4jRvgofKa4oZPZs");
                    var user = Tweetinvi.User.GetAuthenticatedUser();
                    try
                    {
                        Tweet.PublishTweet("Hello @" + ((Tweetinvi.Logic.DTO.UserIdentifierDTO)message.CreatedBy).ScreenName + ", your tweet has been registered and we'll look into it");
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                    }
                }
                foreach (string message in Variables.readOutputupdate)
                {
                    inputs.Add(new KeyValuePair<string, string>(StringHelper.upload + " | Uploads", message));
                }

                foreach (Posts post in Variables.fbOutputUpdate)
                {
                    try
                    {
                        string messageSender = post.UserName;
                        string messageDescription = post.PostStory + " " + post.PostMessage;
                        inputs.Add(new KeyValuePair<string, string>(messageSender + " | Facebook", messageDescription));
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                    }
                }
                foreach (var input in inputs)
                {
                    string group = matchKeywordkey(input.Value);

                    Variables.classifiedData.Add(new KeyValuePair<string, KeyValuePair<string, string>>(group, input));

                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Sentiment(Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty; string input = string.Empty; string inputType = string.Empty;
            string output = String.Empty; string inputFile = String.Empty;
            DataTable outputdt = new DataTable();
            outputdt.Columns.Add("Sentence");
            outputdt.Columns.Add("Response");
            string het = String.Empty; bool isthere = false; Dictionary<string, object> outputDict = new Dictionary<string, object>();
            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                inputType = stp.getPropertiesValue(StringHelper.sentimentinputtype, step);
                inputFile = stp.getPropertiesValue(StringHelper.sentimentinputfile, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                string inp = input;
                string webResponse = string.Empty;
                Dictionary<string, string> result = new Dictionary<string, string>();
                dt.Columns.Add("val");
                try
                {
                    if (inputType.ToLower() != "file")
                    {
                        if (inputType.ToLower() == "value")
                        {
                            inp = input;
                        }
                        else if (inputType.ToLower() == "variable")
                        {
                            if (vh.checkVariableExists(input, executionID))
                            {
                                dt = new TypeHandler().getInput(input, StringHelper.variable, executionID);
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    inp = dt.Rows[i].ItemArray[0].ToString();
                                }
                                if (inp.Contains(".txt") || inp.Contains(".xls") || inp.Contains(".xlsx") || inp.Contains(".pdf") || inp.Contains(".doc") || inp.Contains(".docx"))
                                {
                                    outputDict = fileEntity("http://99.148.66.155:5002/NLP/Sentiment1", inp, executionID, step, input);

                                    foreach (var l in Variables.dynamicvariables)
                                    {
                                        if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                                        {
                                            isthere = true;
                                            dt.Rows.Add(l.vlvalue);
                                            //count = 1;
                                            l.vlvalue = dt;
                                            l.ExecutionID = executionID;
                                            l.RobotID = step.Robot_Id;
                                        }
                                    }
                                    if (!isthere)
                                    {
                                        DVariables d = new DVariables();
                                        d.vlname = output;
                                        d.vlstatus = StringHelper.truee;
                                        d.vlvalue = dt;
                                        d.vltype = StringHelper.datatable;
                                        d.ExecutionID = executionID;
                                        d.RobotID = step.Robot_Id;
                                        Variables.dynamicvariables.Add(d);
                                    }
                                    string resultString = String.Empty;
                                    foreach (var results in outputDict)
                                    {
                                        string sentence = String.Empty;
                                        outputdt.Rows.Add(results.Key, results.Value.ToString());
                                    }
                                    goto retur;
                                }
                            }
                            else
                            {
                                het = input;
                            }
                        }
                        Uri URL = new Uri("http://99.148.66.155:5002/NLP/sentiment?arg=" + Uri.EscapeDataString(inp));
                        WebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";
                        HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            webResponse = streamReader.ReadToEnd();
                        }
                        webResponse = webResponse.Replace('[', ' ');
                        webResponse = webResponse.Replace(']', ' ');
                        Dictionary<string, string> aa = JsonConvert.DeserializeObject<Dictionary<string, string>>(webResponse);
                        foreach (var dict in aa)
                        {
                            result.Add(dict.Key, dict.Value);
                            outputdt.Rows.Add(dict.Key, dict.Value);
                        }

                        foreach (var l in Variables.dynamicvariables)
                        {
                            if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                            {
                                isthere = true;
                                dt.Rows.Add(l.vlvalue);
                                l.vlvalue = dt;
                                l.ExecutionID = executionID;
                                l.RobotID = step.Robot_Id;
                            }
                        }
                        if (!isthere)
                        {
                            DVariables d = new DVariables();
                            d.vlname = output;
                            d.vlstatus = StringHelper.truee;
                            d.vlvalue = dt;
                            d.vltype = StringHelper.datatable;
                            d.ExecutionID = executionID;
                            d.RobotID = step.Robot_Id;
                            Variables.dynamicvariables.Add(d);
                        }
                    }
                    else
                    {

                    }
                retur: ret = StringHelper.success;
                }
                catch (Exception e)
                {
                    new Logger().LogException(e);
                    new ExecutionLog().add2Log(e, step, StringHelper.server, StringHelper.exceptioncode);
                    msgs.Add(new MethodResultHandler().createMessage(e.Message, StringHelper.exceptioncode));
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                }

            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(outputdt), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                het = new TypeHandler().ConvertDataTableToString(outputdt);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, true);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }

            return mr;
        }
        public DataTable calcSentiment(Steps step, string input, DataTable otp)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                SecureString password = new SecureString();
                NameValueCollection credentials = (NameValueCollection)ConfigurationManager.GetSection("PSServerCredentials");
                if (credentials["server"] != "" && new ML().GetLocalIPAddress() == credentials["server"] && credentials["username"] != "" && credentials["password"] != "")
                {
                    foreach (char x in credentials["password"])
                    {
                        password.AppendChar(x);
                    }
                    PSCredential credential = new PSCredential(credentials["username"], password);

                    WSManConnectionInfo connectionInfo = new WSManConnectionInfo(false, credentials["server"], 5985, "/wsman", "http://schemas.microsoft.com/powershell/Microsoft.PowerShell", credential, 1 * 60 * 1000);
                    Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo);
                    runspace.Open();
                    var rootDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    string path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)));
                    path = path.Substring(6);
                    using (PowerShell ps = PowerShell.Create())
                    {
                        ps.Runspace = runspace;
                        ps.AddScript("python \"" + rootDirectory + "PythonScripts\\sentiment.py\" \"" + path + "\\pythonLogs\\nlpLogs\\stanford-corenlp-full-2018-10-05\" \"" + input + "\"");
                        var results = ps.Invoke();
                        if (results.Count > 1)
                        {
                            var s = JsonConvert.SerializeObject(results);
                            string sq = "";
                            foreach (var r in results)
                            {
                                sq += r;

                            }
                            var result = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(sq);

                            string resultString = String.Empty;
                            foreach (var r in result["sentences"])
                            {
                                string sentence = String.Empty;
                                foreach (var word in r["tokens"])
                                {
                                    sentence += word["originalText"] + " ";
                                }
                                resultString += "\n" + sentence + "-->" + r["sentiment"];
                                otp.Rows.Add(input, sentence, r["sentiment"], r["sentimentValue"]);
                            }
                            new ExecutionLog().add2Log("Result" + resultString, step.Robot_Id, step.Name, StringHelper.exceptioncode, false);
                        }
                    }
                    runspace.Close();
                }
                else
                {
                    new ExecutionLog().add2Log("IP Incorrect.Modify Server Details in Configuration File", step.Robot_Id, step.Name, StringHelper.exceptioncode, false);
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            return otp;

        }
        public MethodResult Entity(Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty; string input = string.Empty; string output = String.Empty;
            string inputType = string.Empty; string het = string.Empty; string inputFile = String.Empty;
            DataTable outputdt = new DataTable(); DataTable returnValue = new DataTable();
            returnValue.Columns.Add(StringHelper.Content); string filee = String.Empty;
            Dictionary<string, object> outputDict = new Dictionary<string, object>();
            bool isthere = false;
            try
            {
                inputType = stp.getPropertiesValue(StringHelper.entityinputtype, step);
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                inputFile = stp.getPropertiesValue(StringHelper.entityinputfile, step);
                string inp = input;
                string webResponse = string.Empty;
                Dictionary<string, string> result = new Dictionary<string, string>();
                dt.Columns.Add("val");
                try
                {
                    if (inputType.ToLower() != "file")
                    {
                        if (inputType.ToLower() == "value")
                        {
                            inp = input;
                        }
                        else if (inputType.ToLower() == "variable")
                        {
                            if (vh.checkVariableExists(input, executionID))
                            {
                                dt = new TypeHandler().getInput(input, StringHelper.variable, executionID);
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    inp = dt.Rows[i].ItemArray[0].ToString();
                                }
                                if (inp.Contains(".txt") || inp.Contains(".xls") || inp.Contains(".xlsx") || inp.Contains(".pdf") || inp.Contains(".doc") || inp.Contains(".docx"))
                                {
                                    outputDict = fileEntity("http://99.148.66.155:5002/NLP/Entity1", inp, executionID, step, input);
                                    foreach (var l in Variables.dynamicvariables)
                                    {
                                        if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                                        {
                                            isthere = true;
                                            dt.Rows.Add(l.vlvalue);
                                            //count = 1;
                                            l.vlvalue = dt;
                                            l.ExecutionID = executionID;
                                            l.RobotID = step.Robot_Id;
                                        }
                                    }
                                    if (!isthere)
                                    {
                                        DVariables d = new DVariables();
                                        d.vlname = output;
                                        d.vlstatus = StringHelper.truee;
                                        d.vlvalue = dt;
                                        d.vltype = StringHelper.datatable;
                                        d.ExecutionID = executionID;
                                        d.RobotID = step.Robot_Id;
                                        Variables.dynamicvariables.Add(d);
                                    }
                                    outputdt.Columns.Add("Token");
                                    outputdt.Columns.Add("Named Entity Recognition");
                                    //outputdt.Columns.Add("Parts of Speech");
                                    foreach (var results in outputDict)
                                    {
                                        string sentence = String.Empty;
                                        //outputdt.Rows.Add(results.Key, results.Value.ToString().Split(':')[0], results.Value.ToString().Split(':')[1]);
                                        outputdt.Rows.Add(results.Key, results.Value.ToString().Split(':')[0]);
                                    }
                                    goto retur;
                                }
                            }
                            else
                            {
                                het = input;
                            }
                        }
                        Uri URL = new Uri("http://99.148.66.155:5002/NLP/entity?arg=" + Uri.EscapeDataString(inp));
                        WebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";
                        HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            webResponse = streamReader.ReadToEnd();
                        }

                        webResponse = webResponse.Replace('[', ' ');
                        webResponse = webResponse.Replace(']', ' ');
                        Dictionary<string, object> aa = JsonConvert.DeserializeObject<Dictionary<string, object>>(webResponse);

                        foreach (var l in Variables.dynamicvariables)
                        {
                            if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                            {
                                isthere = true;
                                dt.Rows.Add(l.vlvalue);
                                l.vlvalue = dt;
                                l.ExecutionID = executionID;
                                l.RobotID = step.Robot_Id;
                            }
                        }
                        if (!isthere)
                        {
                            DVariables d = new DVariables();
                            d.vlname = output;
                            d.vlstatus = StringHelper.truee;
                            d.vlvalue = dt;
                            d.vltype = StringHelper.datatable;
                            d.ExecutionID = executionID;
                            d.RobotID = step.Robot_Id;
                            Variables.dynamicvariables.Add(d);
                        }
                        outputdt.Columns.Add("Token");
                        outputdt.Columns.Add("Named Entity Recognition");
                        //outputdt.Columns.Add("Parts Of Speech");
                        foreach (var results in aa)
                        {
                            string sentence = String.Empty;
                            //outputdt.Rows.Add(results.Key, results.Value.ToString().Split(':')[0], results.Value.ToString().Split(':')[1]);
                            outputdt.Rows.Add(results.Key, results.Value.ToString().Split(':')[0]);
                        }
                        if (input.Trim() != string.Empty && new VariableHandler().checkVariableExists(input, executionID))
                        {
                            System.Data.DataTable inputdt = new TypeHandler().getInput(input, "variable", executionID);
                            try
                            {
                                input = String.Empty;
                                foreach (DataRow fr in inputdt.Rows)
                                {
                                    input += fr[inp].ToString();
                                }
                            }
                            catch (Exception e)
                            {
                                string exception = e.Message;
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                            }
                        }
                    }
                    else
                    {
                        if (inputType.ToLower() == "file")
                        {
                            DataTable dt = new TypeHandler().getInput(inputFile, StringHelper.variable, executionID);
                            List<string> files = dt.AsEnumerable().Select(r => r.Field<string>(StringHelper.files)).ToList();

                            foreach (string file in files)
                            {
                                outputDict = fileEntity("http://99.148.66.155:5002/NLP/Entity1", inp, executionID, step, input);
                                foreach (var l in Variables.dynamicvariables)
                                {
                                    if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                                    {
                                        isthere = true;
                                        dt.Rows.Add(l.vlvalue);
                                        //count = 1;
                                        l.vlvalue = dt;
                                        l.ExecutionID = executionID;
                                        l.RobotID = step.Robot_Id;
                                    }
                                }
                                if (!isthere)
                                {
                                    DVariables d = new DVariables();
                                    d.vlname = output;
                                    d.vlstatus = StringHelper.truee;
                                    d.vlvalue = dt;
                                    d.vltype = StringHelper.datatable;
                                    d.ExecutionID = executionID;
                                    d.RobotID = step.Robot_Id;
                                    Variables.dynamicvariables.Add(d);
                                }
                                outputdt.Columns.Add("Token");
                                outputdt.Columns.Add("Named Entity Recognition");
                                //outputdt.Columns.Add("Parts of Speech");
                                foreach (var results in outputDict)
                                {
                                    string sentence = String.Empty;
                                    outputdt.Rows.Add(results.Key, results.Value.ToString().Split(':')[0], results.Value.ToString().Split(':')[1]);
                                }
                                goto retur;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    string ec = e.Message;
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                }
            retur: ret = StringHelper.success;

            }
            catch (Exception ex)
            {
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(outputdt), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                het = new TypeHandler().ConvertDataTableToString(outputdt);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, true);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }

            return mr;
        }

        private Dictionary<string, object> fileEntity(string serviceURL, string filePath, string executionID, Steps step, string input)
        {
            DataTable outputdt = new DataTable(); DataTable returnValue = new DataTable();
            returnValue.Columns.Add(StringHelper.Content); string output = String.Empty;
            string filename = new StringFunction().getFileNameFromPath(filePath);
            string endPoint = serviceURL;
            string textinimage = HttpUploadFile(endPoint, filePath, "file", "application/json", new NameValueCollection());
            textinimage = textinimage.Replace('[', ' ');
            textinimage = textinimage.Replace(']', ' ');
            Dictionary<string, object> outputDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(textinimage);

            return outputDict;
        }
        private Dictionary<string, object> fileEntities(string serviceURL, string filePath1,string filePath2, string executionID, Steps step)
        {
            DataTable outputdt = new DataTable(); DataTable returnValue = new DataTable();
            returnValue.Columns.Add(StringHelper.Content); string output = String.Empty;
            string filename1 = new StringFunction().getFileNameFromPath(filePath1);
            string filename2 = new StringFunction().getFileNameFromPath(filePath2);
            string endPoint = serviceURL;
            string textinimage = HttpUploadFiles(endPoint, filePath1, filePath2, "file", "application/json", new NameValueCollection());
            textinimage = textinimage.Replace('[', ' ');
            textinimage = textinimage.Replace(']', ' ');
            Dictionary<string, object> outputDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(textinimage);

            return outputDict;
        }

        public string HttpUploadFile(string url, string file, string paramName, string contentType, NameValueCollection nvc)
        {
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = System.Net.CredentialCache.DefaultCredentials;
            System.IO.Stream rs = wr.GetRequestStream();
            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);
            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, file, contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);
            FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();
            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();
            WebResponse wresp = null;
            string result = string.Empty;
            try
            {
                wresp = wr.GetResponse();
                System.IO.Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                result = reader2.ReadToEnd();
            }
            catch (Exception ex)
            {
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                    result = ex.Message;
                }
            }
            finally
            {
                wr = null;
            }
            return result;
        }
        public string HttpUploadFiles(string url, string file1,string file2, string paramName, string contentType, NameValueCollection nvc)
        {
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = System.Net.CredentialCache.DefaultCredentials;
            System.IO.Stream rs = wr.GetRequestStream();
            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);
            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, file1, file2, contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);
            FileStream fileStream = new FileStream(file1, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();
            fileStream = new FileStream(file2, FileMode.Open, FileAccess.Read);
            buffer = new byte[4096];
            bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();
            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();
            WebResponse wresp = null;
            string result = string.Empty;
            try
            {
                wresp = wr.GetResponse();
                System.IO.Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                result = reader2.ReadToEnd();
            }
            catch (Exception ex)
            {
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                    result = ex.Message;
                }
            }
            finally
            {
                wr = null;
            }
            return result;
        }

        public MethodResult Match(Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty; string input1 = string.Empty; string output = String.Empty; string comparision = String.Empty;
            DataTable outputdt = new DataTable(); string inputType1 = String.Empty; string inputType2 = String.Empty;
            string input2 = String.Empty; string inputfile1 = String.Empty; string inputfile2 = String.Empty;
            Dictionary<string, object> outputDict = new Dictionary<string, object>();

            try
            {
                inputType1 = stp.getPropertiesValue(StringHelper.matchinputtype1, step);
                inputType2 = stp.getPropertiesValue(StringHelper.matchinputtype2, step);
                input1 = stp.getPropertiesValue(StringHelper.matchinput1, step);
                input2 = stp.getPropertiesValue(StringHelper.matchinput2, step);
                inputfile1 = stp.getPropertiesValue(StringHelper.matchinputfile1, step);
                inputfile2 = stp.getPropertiesValue(StringHelper.matchinputfile2, step);
                comparision = stp.getPropertiesValue(StringHelper.comparison, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                string inp1 = String.Empty;
                string inp2 = String.Empty;
                bool isthere = false;
                try
                {
                    if(inputType1.ToLower()!="file")
                    {
                        if (input1.Trim() != string.Empty && new VariableHandler().checkVariableExists(input1, executionID))
                        {
                            System.Data.DataTable inputdt = new TypeHandler().getInput(input1, "variable", executionID);
                            try
                            {
                                for (int i = 0; i < inputdt.Rows.Count; i++)
                                {
                                    inp1 = inputdt.Rows[i].ItemArray[0].ToString();
                                }
                            }
                            catch (Exception e)
                            {
                                string exception = e.Message;
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                            }
                        }
                        if (input2.Trim() != string.Empty && new VariableHandler().checkVariableExists(input2, executionID))
                        {
                            System.Data.DataTable inputdt = new TypeHandler().getInput(input2, "variable", executionID);
                            try
                            {
                                for (int i = 0; i < inputdt.Rows.Count; i++)
                                {
                                    inp2 = inputdt.Rows[i].ItemArray[0].ToString();
                                }
                            }
                            catch (Exception e)
                            {
                                string exception = e.Message;
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                            }
                        }
                        outputDict = fileEntities("http://99.148.66.155:5002/NLP/match", inp1, inp2, executionID, step);
                        foreach (var l in Variables.dynamicvariables)
                        {
                            if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                            {
                                isthere = true;
                                dt.Rows.Add(l.vlvalue);
                                l.vlvalue = dt;
                                l.ExecutionID = executionID;
                                l.RobotID = step.Robot_Id;
                            }
                        }
                        if (!isthere)
                        {
                            DVariables d = new DVariables();
                            d.vlname = output;
                            d.vlstatus = StringHelper.truee;
                            d.vlvalue = dt;
                            d.vltype = StringHelper.datatable;
                            d.ExecutionID = executionID;
                            d.RobotID = step.Robot_Id;
                            Variables.dynamicvariables.Add(d);
                        }
                        outputdt.Columns.Add("Matched words");
                        foreach (var results in outputDict)
                        {
                            string sentence = String.Empty;
                            outputdt.Rows.Add(results);
                        }
                        goto retur;
                    }
                    else
                    {
                        if (Variables.uploads != null && Variables.uploads.Count > 0)
                        {
                            for(int i=0;i<Variables.uploads.Count;i++)
                            {
                                if(i==0)
                                {
                                    inp1 = Variables.uploads[i];
                                }
                                else
                                {
                                    inp2 = Variables.uploads[i];
                                }
                            }
                        }
                        //DataTable dt = new TypeHandler().getInput(inputfile1, StringHelper.variable, executionID);
                        //List<string> files = dt.AsEnumerable().Select(r => r.Field<string>(StringHelper.files)).ToList();
                        //DataTable dt1 = new TypeHandler().getInput(inputfile2, StringHelper.variable, executionID);
                        //List<string> files1 = dt.AsEnumerable().Select(r => r.Field<string>(StringHelper.files)).ToList();
                        //foreach (string file in files)
                        //{
                        //    inp1 = file;
                        //}
                        //foreach (string file in files1)
                        //{
                        //    inp2 = file;
                        //}
                        outputDict = fileEntities("http://99.148.66.155:5002/NLP/match", inp1, inp2, executionID, step);
                        foreach (var l in Variables.dynamicvariables)
                        {
                            if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                            {
                                isthere = true;
                                dt.Rows.Add(l.vlvalue);
                                l.vlvalue = dt;
                                l.ExecutionID = executionID;
                                l.RobotID = step.Robot_Id;
                            }
                        }
                        if (!isthere)
                        {
                            DVariables d = new DVariables();
                            d.vlname = output;
                            d.vlstatus = StringHelper.truee;
                            d.vlvalue = dt;
                            d.vltype = StringHelper.datatable;
                            d.ExecutionID = executionID;
                            d.RobotID = step.Robot_Id;
                            Variables.dynamicvariables.Add(d);
                        }
                        outputdt.Columns.Add("Matched words");
                        foreach (var results in outputDict)
                        {
                            string sentence = String.Empty;
                            outputdt.Rows.Add(results);
                        }
                        goto retur;
                    }
                }
                catch (Exception e)
                {
                    string ec = e.Message;
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                }
            retur: ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(outputdt);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(outputdt), StringHelper.truee, true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }

            return mr;
        }
        public MethodResult Classify(Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty; string input = string.Empty; string output = String.Empty; string trainset = String.Empty;
            DataTable outputdt = new DataTable(); string inputType = string.Empty; string het = String.Empty; string inputFile = String.Empty;
            bool isthere = false; Dictionary<string, object> outputDict = new Dictionary<string, object>();
            try
            {
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | System.Net.SecurityProtocolType.Tls;
                input = stp.getPropertiesValue(StringHelper.input, step);
                inputType = stp.getPropertiesValue(StringHelper.classifyinputtype, step);
                inputFile = stp.getPropertiesValue(StringHelper.classifyinputfile, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                string inp = input;
                //outputdt.Columns.Add("Input");
                //outputdt.Columns.Add("Classify");
                string webResponse = string.Empty;
                Dictionary<string, string> result = new Dictionary<string, string>();
                dt.Columns.Add("val");

                try
                {
                    if (inputType.ToLower() != "file")
                    {
                        if (inputType.ToLower() == "value")
                        {
                            inp = input;
                        }
                        else if (inputType.ToLower() == "variable")
                        {
                            if (vh.checkVariableExists(input, executionID))
                            {
                                dt = new TypeHandler().getInput(input, StringHelper.variable, executionID);
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    inp = dt.Rows[i].ItemArray[0].ToString();
                                }
                                if (inp.Contains(".txt") || inp.Contains(".xls") || inp.Contains(".xlsx") || inp.Contains(".pdf") || inp.Contains(".doc") || inp.Contains(".docx"))
                                {
                                    outputDict = fileEntity("http://99.148.66.155:5002/NLP/Classify1", inp, executionID, step, input);
                                    foreach (var l in Variables.dynamicvariables)
                                    {
                                        if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                                        {
                                            isthere = true;
                                            dt.Rows.Add(l.vlvalue);
                                            l.vlvalue = dt;
                                            l.ExecutionID = executionID;
                                            l.RobotID = step.Robot_Id;
                                        }
                                    }
                                    if (!isthere)
                                    {
                                        DVariables d = new DVariables();
                                        d.vlname = output;
                                        d.vlstatus = StringHelper.truee;
                                        d.vlvalue = dt;
                                        d.vltype = StringHelper.datatable;
                                        d.ExecutionID = executionID;
                                        d.RobotID = step.Robot_Id;
                                        Variables.dynamicvariables.Add(d);
                                    }
                                    outputdt.Columns.Add("Token");
                                    outputdt.Columns.Add("Classification");
                                    foreach (var results in outputDict)
                                    {
                                        string sentence = String.Empty;
                                        outputdt.Rows.Add(results.Key, results.Value.ToString());
                                    }
                                    goto retur;
                                }
                            }
                            else
                            {
                                het = input;
                            }
                        }

                        Uri URL = new Uri("http://99.148.66.155:5002/NLP/classify?arg=" + Uri.EscapeDataString(inp));
                        WebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";
                        HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            webResponse = streamReader.ReadToEnd();
                        }
                        Dictionary<string, object> aa = JsonConvert.DeserializeObject<Dictionary<string, object>>(webResponse);
                        //string[] res = aa.Split("==>");
                        //result.Add(res[0].ToString(), res[1].ToString());
                        //outputdt.Rows.Add(res[0].ToString(), res[1].ToString());


                        foreach (var l in Variables.dynamicvariables)
                        {
                            if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                            {
                                isthere = true;
                                dt.Rows.Add(l.vlvalue);
                                l.vlvalue = dt;
                                l.ExecutionID = executionID;
                                l.RobotID = step.Robot_Id;
                            }
                        }
                        if (!isthere)
                        {
                            DVariables d = new DVariables();
                            d.vlname = output;
                            d.vlstatus = StringHelper.truee;
                            d.vlvalue = dt;
                            d.vltype = StringHelper.datatable;
                            d.ExecutionID = executionID;
                            d.RobotID = step.Robot_Id;
                            Variables.dynamicvariables.Add(d);
                        }
                        outputdt.Columns.Add("Token");
                        outputdt.Columns.Add("Classification");
                        foreach (var results in aa)
                        {
                            string sentence = String.Empty;
                            //outputdt.Rows.Add(results.Key, results.Value.ToString().Split(':')[0], results.Value.ToString().Split(':')[1]);
                            outputdt.Rows.Add(results.Key, results.Value.ToString().Split(':')[0]);
                        }
                        goto retur;

                    }
                    else
                    {
                        if (inputType.ToLower() == "file")
                        {
                            DataTable dt = new TypeHandler().getInput(inputFile, StringHelper.variable, executionID);
                            List<string> files = dt.AsEnumerable().Select(r => r.Field<string>(StringHelper.files)).ToList();

                            foreach (string file in files)
                            {
                                outputDict = fileEntity("http://99.148.66.155:5002/NLP/Classify1", inp, executionID, step, input);
                                foreach (var l in Variables.dynamicvariables)
                                {
                                    if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                                    {
                                        isthere = true;
                                        dt.Rows.Add(l.vlvalue);
                                        //count = 1;
                                        l.vlvalue = dt;
                                        l.ExecutionID = executionID;
                                        l.RobotID = step.Robot_Id;
                                    }
                                }
                                if (!isthere)
                                {
                                    DVariables d = new DVariables();
                                    d.vlname = output;
                                    d.vlstatus = StringHelper.truee;
                                    d.vlvalue = dt;
                                    d.vltype = StringHelper.datatable;
                                    d.ExecutionID = executionID;
                                    d.RobotID = step.Robot_Id;
                                    Variables.dynamicvariables.Add(d);
                                }
                                outputdt.Columns.Add("Token");
                                outputdt.Columns.Add("Classification");
                                foreach (var results in outputDict)
                                {
                                    string sentence = String.Empty;
                                    outputdt.Rows.Add(results.Key, results.Value.ToString());
                                }
                                goto retur;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    string ec = e.Message;
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                }
                string ts = string.Empty;
                if (trainset.Trim() != string.Empty)
                {
                    System.Data.DataTable dt = new TypeHandler().getInput(trainset, "Variable", executionID);
                    try
                    {
                        List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                        try
                        {
                            ts = files[0];
                        }
                        catch (Exception ex)
                        {
                            string ec = ex.Message;
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                    }
                    catch (Exception e)
                    {
                        string ec = e.Message;
                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                    }
                }
            retur: ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(outputdt), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                het = new TypeHandler().ConvertDataTableToString(outputdt);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, true);

            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }

            return mr;
        }

        public string matchKeywordkey(string text)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string deptname = string.Empty;
                string dep = getKeywordsbyDepartment();
                List<keybydepts> keysbydepts = JsonConvert.DeserializeObject<List<keybydepts>>(dep);
                List<KeyValuePair<string, int>> deptStrength = new List<KeyValuePair<string, int>>();
                foreach (keybydepts k in keysbydepts)
                {
                    List<string> deptp = new List<string>();
                    deptp = k.keyword.Split(',').ToList();
                    int counter = 0;
                    foreach (string detp in deptp)
                    {
                        if (text.ToLower().IndexOf(detp) > -1)
                        {
                            counter++;
                        }
                    }
                    deptStrength.Add(new KeyValuePair<string, int>(k.departmentname, counter));
                }

                int pair = deptStrength.Max(x => x.Value);
                foreach (var kvp in deptStrength)
                {
                    if (kvp.Value == pair)
                    {
                        deptname = kvp.Key;
                    }
                }

                return deptname;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return "";
            }
        }
        public string matchKeyword1(string text)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string[] labour = { "labour","labor","labour office","labor office","employee","employer","pf","provident fund","wages","minimum wages","salary","minimum salaries","labour enactment","workmen","work force","industrial establishment",
                            "statutory provisions","work beyond normal working hours","overtime","work hours","working hours","revision of wages","enactment","labour enforcement officer","inspector of factories","employment","employment commission",
                            "workers","insdustrial disputes act 1947","U/S 2A(2)","U/S 2K","Labour count","gratuity","TA","DA","states shops and establishments act","bonus act 1965",
                            "gratuity act 1972","white collar","blue collar","golden collar","resources","HR","human resources","child labour","organization sector","general tranfers"};

                string[] forest = {"IFS","Forestry service","Forest","Forestry","WoodLands","WetLands","Ramsar Convention","VrukshaNidhi","Timber","wood","trees","plants","sawmill","environment","vruksham","forest ministry","vruksh","forest resources",
                            "flora","fauna","sandal","honey","green","scenary","waterfalls","river","backwaters","ravine","heritage","nature","natural","wildlife","principal chief conservator of forests","environmental principal secretary","hunting","illegal hunting",
                            "smuggling","human animals conflict","tribal","animal","bird","sanctuary","wild","wild animals","poatching","trap","checkpost","check post", "afforestation", "deforestation"};

                string[] apsrtc = {"bus","jeep","dgt","fleet","kmpl","occupancy","kilometer/day","kilometer/hour","kilometer","km/day","kmph","occupancy ratio","earnings/day","earnings","average vehicle utilization","passengers","busstand","busstop",
                            "bus stand","bus stop","bus depo","departure","arrival","bus failure","diesel","bandh","hartal","super luxury","express","deluxe","metro","bus ticket","ticket","drivers","conductor","cancellation","reservation",
                            "bus pass","route pass","metro pass","metro bus","CAT","TAYL","Vanitha","pessengers","route map","bus number","bus no.","route","bus registration number","mobile tracking","city bus","suburban","pallevelugu","district service",
                             "passenger","non-stop","3-stop","4-stop","5-stop","refund","depo manager","divisional manager","regional manager","APSRTC MD","recovery vehicle","Ticket inspector" };

                string[] education = {"class","student","school","college","institution","university","study","learn","scholarship","Anganvaadi","hostels","warden","professional","professor","junior","fresher","fresh men","senior","convocation","teacher",
                            "lab-assistant","lecturer","play-grounds","Kendra vidyalaya","vidyalaya","manabadi","badi","ssc","intermediate","high school","degree","ebc","under-graduation","post-graduation","graduation","UG","PG","benches","board","PD","Physical Director","PhD",
                            "Doctorate","literacy","literature","literate","term fee","annual fee","school fee","college fee","university fee","examination fee","examination","exams","results","pass","fail","percentage","consolidation","attendance","headmaster","principal",
                            "dean","chancellor"};

                string[] rb = { "road", "widing", "building", "public", "works", "public works", "bad roads", "cement road", "metalled road", "road" };

                string[] sanitation = { "cleaning", "mosquitoes", "drains", "drainage", "roads", "public works", "bad roads", "road", "pollution", "neat" };

                List<string> Labour = labour.ToList();
                List<string> Forest = forest.ToList();
                List<string> APSRTC = apsrtc.ToList();
                List<string> Education = education.ToList();
                List<string> RB = apsrtc.ToList();
                List<string> Sanitation = education.ToList();

                Tokenizer tokenize = new Tokenizer();
                List<string> tokenizedText = tokenize.Tokenize(text, true).ToList();
                int countl = 0;
                int countf = 0;
                int counta = 0;
                int counte = 0;

                foreach (string labor in Labour)
                {
                    foreach (string tokenText in tokenizedText)
                    {
                        if (labor.ToLower().IndexOf(tokenText.ToLower()) > -1)
                        {
                            countl++;
                        }
                    }
                }
                foreach (string forestt in Forest)
                {
                    foreach (string tokenText in tokenizedText)
                    {
                        if (forestt.ToLower().IndexOf(tokenText.ToLower()) > -1)
                        {
                            countf++;
                        }
                    }
                }
                foreach (string apsrtcc in APSRTC)
                {
                    foreach (string tokenText in tokenizedText)
                    {
                        if (apsrtcc.ToLower().IndexOf(tokenText.ToLower()) > -1)
                        {
                            counta++;
                        }
                    }
                }
                foreach (string edu in Education)
                {
                    foreach (string tokenText in tokenizedText)
                    {
                        if (edu.ToLower().IndexOf(tokenText.ToLower()) > -1)
                        {
                            counte++;
                        }
                    }
                }


                int[] bn = { countl, countf, counta, counte };

                int rt = bn.Max(); string returns = string.Empty;

                if (rt == countl)
                {
                    returns = StringHelper.labour;
                }
                if (rt == counte)
                {
                    returns = StringHelper.education;
                }
                if (rt == countf)
                {
                    returns = StringHelper.forest;
                }
                if (rt == counta)
                {
                    returns = StringHelper.apsrtc;
                }
                return returns;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return "";
            }
        }
        public string matchKeyword(string text)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "select \"ID\",\"Name\" from \"Department\" where \"Status\"=true";
                Connection c = new Connection();
                string Departments = c.executeSQL(query, "Select");

                List<Dictionary<string, string>> dept = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(Departments);

                Dictionary<string, List<string>> deptKeywords = new Dictionary<string, List<string>>();
                foreach (Dictionary<string, string> deptDict in dept)
                {

                    string keywordsQuery = "Select \"keyword\" from \"Keyword\" where \"DepartmentId\"='" + deptDict["ID"] + "' and \"Status\"=true";
                    string keywordsResult = c.executeSQL(keywordsQuery, "Select");
                    List<Dictionary<string, string>> keywords = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(keywordsResult);
                    List<string> tempKeys = new List<string>();
                    foreach (Dictionary<string, string> keyword in keywords)
                    {
                        tempKeys.Add(keyword[StringHelper.keyword]);
                    }
                    deptKeywords.Add(deptDict[StringHelper.namee], tempKeys);
                }

                Tokenizer tokenize = new Tokenizer();
                List<string> tokenizedText = tokenize.Tokenize(text, true).ToList();

                Dictionary<string, int> DeptCount = new Dictionary<string, int>();
                foreach (KeyValuePair<string, List<string>> deptKey in deptKeywords)
                {
                    int count = 0;
                    foreach (string key in deptKey.Value)
                    {
                        foreach (string tokenText in tokenizedText)
                        {
                            if (key.ToLower().IndexOf(tokenText.ToLower()) > -1)
                            {
                                count++;
                            }
                        }
                    }
                    DeptCount.Add(deptKey.Key, count);
                }

                return DeptCount.OrderBy(y => y.Value).Last().Key.ToString().ToLower();

            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return "";
            }
        }
        public string getKeywordsbyDepartment()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "select x.\"Name\" as DepartmentName,string_agg(x.\"keyword\", ',') as keyword from ( select a.\"DepartmentId\", a.\"keyword\", b.\"Name\" from public.\"Keyword\" a,public.\"Department\" b where a.\"DepartmentId\"=b.\"ID\" and b.\"Status\"=true) x group by x.\"Name\" ";
                string ret = dbd.DbExecuteQuery(query, "select");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return "";
            }
        }
        //public MethodResult SentenceFormation(Steps step, string executionID)
        //{
        //    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
        //    string ret = string.Empty; string input = string.Empty; string output = String.Empty;
        //    DataTable outputdt = new DataTable();
        //    try
        //    {
        //        input = stp.getPropertiesValue(StringHelper.input, step);
        //        output = stp.getPropertiesValue(StringHelper.output, step);
        //        string inp = input;
        //        string webResponse = string.Empty;
        //        Dictionary<string, string> result = new Dictionary<string, string>();
        //        dt.Columns.Add("val");
        //        try
        //        {
        //            Uri URL = new Uri("http://99.148.66.155:5002/NLP/sentence?arg=" + Uri.EscapeDataString(inp));
        //            WebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
        //            httpWebRequest.ContentType = "application/json";
        //            httpWebRequest.Method = "POST";
        //            HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        //            using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
        //            {
        //                webResponse = streamReader.ReadToEnd();
        //            }

        //            webResponse = webResponse.Replace('[', ' ');
        //            webResponse = webResponse.Replace(']', ' ');
        //            Dictionary<string, string> aa = JsonConvert.DeserializeObject<Dictionary<string, string>>(webResponse);
        //            //string[] res = aa.Split(",");
        //            // result.Add(res[0].ToString(), res[1].ToString());
        //            //outputdt.Rows.Add(res[0].ToString(), res[1].ToString());

        //            bool isthere = false;
        //            foreach (var l in Variables.dynamicvariables)
        //            {
        //                if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
        //                {
        //                    isthere = true;
        //                    dt.Rows.Add(l.vlvalue);
        //                    //count = 1;
        //                    l.vlvalue = dt;
        //                    l.ExecutionID = executionID;
        //                    l.RobotID = step.Robot_Id;
        //                }
        //            }
        //            if (!isthere)
        //            {
        //                DVariables d = new DVariables();
        //                d.vlname = output;
        //                d.vlstatus = StringHelper.truee;
        //                d.vlvalue = dt;
        //                d.vltype = StringHelper.datatable;
        //                d.ExecutionID = executionID;
        //                d.RobotID = step.Robot_Id;
        //                Variables.dynamicvariables.Add(d);
        //            }
        //            string resultString = String.Empty;
        //            outputdt.Columns.Add("Token");
        //            //outputdt.Columns.Add("Named Entity Recognition");
        //            outputdt.Columns.Add("Parts Of Speech");
        //            foreach (var results in aa)
        //            {
        //                string sentence = String.Empty;
        //                //foreach (var word in r)
        //                //{
        //                outputdt.Rows.Add(results.Key, results.Value);
        //                //}

        //            }
        //            if (input.Trim() != string.Empty && new VariableHandler().checkVariableExists(input, executionID))
        //            {
        //                System.Data.DataTable inputdt = new TypeHandler().getInput(input, "variable", executionID);
        //                try
        //                {
        //                    input = String.Empty;
        //                    foreach (DataRow fr in inputdt.Rows)
        //                    {
        //                        input += fr[inp].ToString();
        //                    }
        //                }
        //                catch (Exception e)
        //                {
        //                    string exception = e.Message;
        //                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //                    s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
        //                }
        //            }

        //        }
        //        catch (Exception e)
        //        {
        //            string ec = e.Message;
        //            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //            s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
        //        }

        //        ret = StringHelper.success;

        //    }
        //    catch (Exception ex)
        //    {
        //        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
        //        new Logger().LogException(ex);
        //        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
        //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
        //    }
        //    MethodResult mr;
        //    if (ret == StringHelper.success)
        //    {
        //        new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(outputdt), "", true, "datatable", step.Robot_Id, executionID);
        //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //        s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
        //        mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
        //        string het = new TypeHandler().ConvertDataTableToString(outputdt);
        //        new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, true);
        //    }
        //    else
        //    {
        //        mr = new MethodResultHandler().createResultType(false, msgs, null, s);
        //    }

        //    return mr;
        //}

        public MethodResult SentenceFormation(Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty; string input = string.Empty; string inputType = string.Empty;
            string output = String.Empty; string inputFile = String.Empty;
            DataTable outputdt = new DataTable();
            string het = String.Empty; bool isthere = false; Dictionary<string, object> outputDict = new Dictionary<string, object>();
            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                inputType = stp.getPropertiesValue(StringHelper.sentenceinputtype, step);
                inputFile = stp.getPropertiesValue(StringHelper.sentenceinputfile, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                string inp = input;
                string webResponse = string.Empty;
                Dictionary<string, string> result = new Dictionary<string, string>();
                dt.Columns.Add("val");
                try
                {
                    if (inputType.ToLower() != "file")
                    {
                        if (inputType.ToLower() == "value")
                        {
                            inp = input;
                        }
                        else if (inputType.ToLower() == "variable")
                        {
                            if (vh.checkVariableExists(input, executionID))
                            {
                                dt = new TypeHandler().getInput(input, StringHelper.variable, executionID);
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    inp = dt.Rows[i].ItemArray[0].ToString();
                                }
                                if (inp.Contains(".txt") || inp.Contains(".xls") || inp.Contains(".xlsx") || inp.Contains(".pdf") || inp.Contains(".doc") || inp.Contains(".docx"))
                                {
                                    outputDict = fileEntity("http://99.148.66.155:5002/NLP/Sentence1", inp, executionID, step, input);

                                    foreach (var l in Variables.dynamicvariables)
                                    {
                                        if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                                        {
                                            isthere = true;
                                            dt.Rows.Add(l.vlvalue);
                                            //count = 1;
                                            l.vlvalue = dt;
                                            l.ExecutionID = executionID;
                                            l.RobotID = step.Robot_Id;
                                        }
                                    }
                                    if (!isthere)
                                    {
                                        DVariables d = new DVariables();
                                        d.vlname = output;
                                        d.vlstatus = StringHelper.truee;
                                        d.vlvalue = dt;
                                        d.vltype = StringHelper.datatable;
                                        d.ExecutionID = executionID;
                                        d.RobotID = step.Robot_Id;
                                        Variables.dynamicvariables.Add(d);
                                    }
                                    string resultString = String.Empty;
                                    outputdt.Columns.Add("Token");
                                    outputdt.Columns.Add("Parts of speech");
                                    foreach (var results in outputDict)
                                    {
                                        string sentence = String.Empty;
                                        outputdt.Rows.Add(results.Key, results.Value.ToString());
                                    }
                                    goto retur;
                                }
                            }
                            else
                            {
                                het = input;
                            }
                        }
                        Uri URL = new Uri("http://99.148.66.155:5002/NLP/sentence?arg=" + inp);
                        WebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";
                        HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            webResponse = streamReader.ReadToEnd();
                        }
                        webResponse = webResponse.Replace('[', ' ');
                        webResponse = webResponse.Replace(']', ' ');
                        Dictionary<string, string> aa = JsonConvert.DeserializeObject<Dictionary<string, string>>(webResponse);
                        outputdt.Columns.Add("Token");
                        outputdt.Columns.Add("Parts of speech");
                        foreach (var dict in aa)
                        {
                            result.Add(dict.Key, dict.Value);
                            outputdt.Rows.Add(dict.Key, dict.Value);
                        }

                        foreach (var l in Variables.dynamicvariables)
                        {
                            if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                            {
                                isthere = true;
                                dt.Rows.Add(l.vlvalue);
                                l.vlvalue = dt;
                                l.ExecutionID = executionID;
                                l.RobotID = step.Robot_Id;
                            }
                        }
                        if (!isthere)
                        {
                            DVariables d = new DVariables();
                            d.vlname = output;
                            d.vlstatus = StringHelper.truee;
                            d.vlvalue = dt;
                            d.vltype = StringHelper.datatable;
                            d.ExecutionID = executionID;
                            d.RobotID = step.Robot_Id;
                            Variables.dynamicvariables.Add(d);
                        }
                    }
                    else
                    {

                    }
                retur: ret = StringHelper.success;
                }
                catch (Exception e)
                {
                    string ec = e.Message;
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                }

            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(outputdt), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                het = new TypeHandler().ConvertDataTableToString(outputdt);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, true);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }

            return mr;
        }
        public MethodResult TextSummarize(Steps step, string executionID)
        {
            string ret = string.Empty;
            MethodResult mr = new MethodResult();
            DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string inp = String.Empty; bool isthere = false;
            string input = String.Empty;
            string output = String.Empty;
            returnValue.Columns.Add("Summarized Text");
            try
            {             
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                string outputString = "";
                try
                {
                    dt = new TypeHandler().getInput(input, StringHelper.variable, executionID);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        inp = dt.Rows[i].ItemArray[0].ToString();
                    }
                    if (inp.Contains(".txt"))
                    {

                        outputString = FileEntity("http://99.148.66.155:5002/NLP/Summarize", inp, executionID, step, input);

                        foreach (var l in Variables.dynamicvariables)
                        {
                            if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                            {
                                isthere = true;
                                dt.Rows.Add(l.vlvalue);
                                l.vlvalue = dt;
                                l.ExecutionID = executionID;
                                l.RobotID = step.Robot_Id;
                            }
                        }
                        if (!isthere)
                        {
                            DVariables d = new DVariables();
                            d.vlname = output;
                            d.vlstatus = StringHelper.truee;
                            d.vlvalue = dt;
                            d.vltype = StringHelper.datatable;
                            d.ExecutionID = executionID;
                            d.RobotID = step.Robot_Id;
                            Variables.dynamicvariables.Add(d);
                        }
                        goto retur;
                    }
                    retur: returnValue.Rows.Add(outputString);
                        ret = StringHelper.success;
                }
                catch (Exception e)
                {
                    new Logger().LogException(e);
                    new ExecutionLog().add2Log(e, step, StringHelper.server, StringHelper.exceptioncode);
                    msgs.Add(new MethodResultHandler().createMessage(e.Message, StringHelper.exceptioncode));
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        private string FileEntity(string serviceURL, string filePath, string executionID, Steps step, string input)
        {
            DataTable returnValue = new DataTable();
            returnValue.Columns.Add(StringHelper.Content); string output = String.Empty;
            string filename = new StringFunction().getFileNameFromPath(filePath);
            string endPoint = serviceURL;
            string textinimage = HttpUploadFile(endPoint, filePath, "file", "application/json", new NameValueCollection());
            textinimage = textinimage.Replace('[', ' ');
            textinimage = textinimage.Replace(']', ' ');
            return textinimage;
        }
    }
}
public class keybydepts
{
    public string departmentname { get; set; }
    public string keyword { get; set; }
}