﻿using Genbase.classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using Genbase.Classes;

namespace Genbase.BLL.ElementsClasses
{
    public class Window
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        VariableHandler vh = new VariableHandler();
        DataTable dt = new DataTable();
        EncoderDecoderBase64 baser = new EncoderDecoderBase64();
        string time = DateTime.Now.ToShortTimeString();
        string date = DateTime.Now.ToShortDateString();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult Input( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            
            string Buttons = string.Empty;
            string DisplayMessage = string.Empty;
            string VariableName = string.Empty;
            string Title = string.Empty;
            dt.Columns.Add("val");
            try
            {
                Result rs = new Result();
                VariableName = stp.getPropertiesValue(StringHelper.variable, step);
                Title = stp.getPropertiesValue(StringHelper.heading, step);
                DisplayMessage = stp.getPropertiesValue(StringHelper.displaymessage, step);
                Buttons = stp.getPropertiesValue(StringHelper.buttons, step);
                Variables.thisVariable = VariableName;

                foreach (var l in Variables.dynamicvariables)
                {
                    if ((l.vlname == VariableName) && (l.vlstatus == StringHelper.truee))
                    {
                        dt.Rows.Add(l.vlvalue);
                        //count = 1;
                        l.vlvalue = dt;
                    }
                }
                RuntimeUserInputRequest runtimeinput = new BLL.AppExecution().getRunTimeUserInputRequest(step,executionID);
                rs.requestFrom = Variables.requestFrom;
                rs.runtimerequest = runtimeinput;
                rs.RobotId = Convert.ToInt32(Variables.robotid);
                rs.SystemId = Convert.ToInt32(Variables.jobid);
                rs.executionID = executionID;
                new BLL.ElementsClasses.Window().createInbox(rs);
                
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables,s);
                new ExecutionLog().add2Log("Input has been Executed", step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null,s);
            }
            return mr;
        }
        public string createInbox(Result r)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                int dMessage = r.runtimerequest.step.StepProperties.FindIndex(i => i.StepProperty == "DisplayMessage" || i.StepProperty=="MessageFormat");
                int useridIndex = r.runtimerequest.step.StepProperties.FindIndex(i => i.StepProperty == "User");

                string values = baser.Base64Encode(r.RobotId.ToString()) + "," + baser.Base64Encode(r.runtimerequest.step.StepProperties[dMessage].StepPropertyValue) + "," + baser.Base64Encode(r.runtimerequest.step.Id) + "," + baser.Base64Encode("New") + "," + baser.Base64Encode(JsonConvert.SerializeObject(r).Replace("'","''")) + "," + baser.Base64Encode("postgres") + "," + baser.Base64Encode(date + " " + time) + "," + baser.Base64Encode("postgres") + "," + baser.Base64Encode(date + " " + time) + "," + baser.Base64Encode("true") + "," + baser.Base64Encode(r.SystemId.ToString()) + "," + baser.Base64Encode(r.runtimerequest.step.StepProperties[useridIndex].StepPropertyValue)+","+baser.Base64Encode(r.runtimerequest.step.Name);
                string result = new DAL.QueryUtil().insertTableData("public", "Inbox", values, "", true);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return "false";
            }
        }
        public MethodResult InputBox( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string Buttons = string.Empty;
            string DisplayMessage = string.Empty;
            string VariableName = string.Empty;
            string Title = string.Empty;
            string WidgetType = string.Empty;
            string User = string.Empty;
            dt.Columns.Add("val");
            try
            {
                VariableName = stp.getPropertiesValue(StringHelper.variable, step);
                Title = stp.getPropertiesValue(StringHelper.heading, step);
                DisplayMessage = stp.getPropertiesValue(StringHelper.displaymessage, step);
                Buttons = stp.getPropertiesValue(StringHelper.buttons, step);
                WidgetType = stp.getPropertiesValue(StringHelper.widgettype, step);
                User = stp.getPropertiesValue(StringHelper.User_Inp, step);
                Variables.thisVariable = VariableName;
                bool isthere = false;
                foreach (var l in Variables.dynamicvariables)
                {
                    if ((l.vlname == VariableName) && (l.vlstatus == StringHelper.truee))
                    {
                        isthere = true;
                        dt.Rows.Add(l.vlvalue);
                        //count = 1;
                        l.vlvalue = dt;
                        l.ExecutionID = executionID;
                        l.RobotID = step.Robot_Id;
                    }
                }
                if(!isthere)
                {
                    DVariables d = new DVariables();
                    d.vlname = VariableName;
                    d.vlstatus = StringHelper.truee;
                    d.vlvalue = dt;
                    d.vltype = StringHelper.datatable;
                    d.ExecutionID = executionID;
                    d.RobotID = step.Robot_Id;
                    Variables.dynamicvariables.Add(d);
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables,s);
                new ExecutionLog().add2Log("Input Box has been Executed", step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null,s);
            }
            return mr;
        }
        public MethodResult Output( Steps step, string executionID)
        {
            //Excel exc = new Excel();
            //exc.GetNetworkFiles();
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string Buttons = string.Empty;
            string Message = string.Empty;
            string VariableName = string.Empty;
            string Title = string.Empty;
            string OutputType = string.Empty;

            try
            {
                Result rs = new Result();
                string het = string.Empty;
                VariableName = stp.getPropertiesValue(StringHelper.value, step);
                Title = stp.getPropertiesValue(StringHelper.heading, step);
                Buttons = stp.getPropertiesValue(StringHelper.buttons, step);
                Message = stp.getPropertiesValue(StringHelper.messageformat, step);
                OutputType = stp.getPropertiesValue(StringHelper.output_type, step);
                System.Data.DataTable dt = new System.Data.DataTable();
                Variables.thisVariable = VariableName;
                if (OutputType.ToLower() == StringHelper.value)
                {
                    dt = new TypeHandler().getInput(VariableName, StringHelper.value, executionID);
                    het = new TypeHandler().ConvertDataTableToString(dt);
                }
                else
                {
                    if (vh.checkVariableExists(VariableName, executionID))
                    {
                        dt = new TypeHandler().getInput(VariableName, StringHelper.variable, executionID);
                        het = new TypeHandler().ConvertDataTableToString(dt);
                    }
                    else
                    {
                        het = VariableName;
                    }
                }
                RuntimeUserInputRequest runtimeinput = new BLL.AppExecution().getRunTimeUserInputRequest(step, executionID);
                rs.executionID = executionID;
                rs.runtimerequest = runtimeinput;
                rs.RobotId = Convert.ToInt32(Variables.robotid);
                rs.SystemId = Convert.ToInt32(Variables.jobid);
                new BLL.ElementsClasses.Window().createInbox(rs);

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                ret = StringHelper.ffalse;
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables,s);
                new ExecutionLog().add2Log("Output has been Executed", step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null,s);
            }
            return mr;
        }
        public MethodResult OutputBox( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string Buttons = string.Empty;
            string Message = string.Empty;
            string VariableName = string.Empty;
            string Title = string.Empty;
            string OutputType = string.Empty;
            string ContentType = string.Empty;

            try
            {
                string het = string.Empty;
                VariableName = stp.getPropertiesValue(StringHelper.input, step);
                Title = stp.getPropertiesValue(StringHelper.heading, step);
                Buttons = stp.getPropertiesValue(StringHelper.buttons, step);
                Message = stp.getPropertiesValue(StringHelper.messageformat, step);
                OutputType = stp.getPropertiesValue(StringHelper.outputtype, step);
                ContentType = stp.getPropertiesValue(StringHelper.contenttype, step);
                System.Data.DataTable dt = new System.Data.DataTable();

                if (VariableName.StartsWith("{"))
                {
                    OutputType = "Variable";
                }
                Variables.thisVariable = VariableName;
                if (OutputType.ToLower() == StringHelper.input)
                {
                    dt = new TypeHandler().getInput(VariableName, StringHelper.input, executionID);
                    het = new TypeHandler().ConvertDataTableToString(dt);
                    if (ContentType.Trim().ToLower() == StringHelper.plaintext)
                    {
                        het = new TypeHandler().cDataTableToString(dt);
                        new VariableHandler().CreateVariable(VariableName, het, "", true, StringHelper.strings, step.Robot_Id, executionID);
                    }
                }
                else
                {
                    if (vh.checkVariableExists(VariableName, executionID))
                    {
                        dt = new TypeHandler().getInput(VariableName, StringHelper.variable, executionID);
                        het = new TypeHandler().ConvertDataTableToString(dt);
                        if(ContentType.Trim().ToLower()==StringHelper.plaintext)
                        {
                            het = new TypeHandler().cDataTableToString(dt);

                            int semicoloncount = Regex.Matches(het, ":").Count;
                            if(semicoloncount == 1)
                            {
                                het = het.Split(':')[1];
                            }
                            else
                            {
                                
                            }
                            new VariableHandler().CreateVariable(VariableName, het, "", true, StringHelper.strings, step.Robot_Id, executionID);
                        }
                    }
                    else
                    {
                        het = VariableName;
						new ExecutionLog().add2Log(VariableName+" Does not Exits", step.Robot_Id, step.Name, StringHelper.eventcode, false);

					}
				}
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
				new ExecutionLog().add2Log(ex.Message, step.Robot_Id, step.Name, StringHelper.eventcode, false);
				msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                ret = StringHelper.ffalse;
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables,s);
                new ExecutionLog().add2Log("Output Box has been Executed", step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null,s);
            }
            return mr;
        }
    }
}