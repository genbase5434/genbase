﻿using Genbase.classes;
using Facebook;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Tweetinvi;
using Tweetinvi.Core.Extensions;
using Tweetinvi.Logic.DTO;
using Tweetinvi.Parameters;
using Tweetinvi.Models;
using Newtonsoft.Json.Linq;
using System.IO;
using Genbase.Classes;

namespace Genbase.BLL.ElementsClasses
{
    public class Social
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        VariableHandler vh = new VariableHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult TwitterLogin( Steps step, string executionID)
        {
            string accesstoken = string.Empty; string accesssecret = string.Empty; string consumersecret = string.Empty; string consumerkey = string.Empty; string ret = string.Empty; string output = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            DataTable twitterCredDtr = new DataTable();
            try
            {
                consumerkey = stp.getPropertiesValue(StringHelper.consumerkey, step);
                accesssecret = stp.getPropertiesValue(StringHelper.accesssecret, step);
                consumersecret = stp.getPropertiesValue(StringHelper.consumersecret, step);
                accesstoken = stp.getPropertiesValue(StringHelper.accesstoken, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                var cx = Auth.SetUserCredentials(consumerkey, consumersecret, accesstoken, accesssecret);

                twitterCredDtr.Columns.Add(StringHelper.consumerkey);
                twitterCredDtr.Columns.Add(StringHelper.consumersecret);
                twitterCredDtr.Columns.Add(StringHelper.accesstoken);
                twitterCredDtr.Columns.Add(StringHelper.accesssecret);
                twitterCredDtr.Rows.Add(consumerkey, consumersecret, accesstoken, accesssecret);

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(twitterCredDtr), "", false, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult TwitterGet( Steps step, string executionID)
        {
            string ret = string.Empty;
            string instance = string.Empty; string category = string.Empty; string readtype = string.Empty; string count = string.Empty; string output = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            DataTable result = new DataTable();
            try
            {
                instance = stp.getPropertiesValue(StringHelper.instance, step);
                category = stp.getPropertiesValue("category", step);
                readtype = stp.getPropertiesValue("read type", step);
                count = stp.getPropertiesValue(StringHelper.count, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                string getHomeTimelineQuery = "https://api.twitter.com/1.1/statuses/"; string latestsinceID = string.Empty; string sinceId = string.Empty;
                List<UserInstance> instances = getInstanceValue(step.Robot_Id, step.Id, category);
                if (instances.Count > 0)
                {
                    foreach (UserInstance instanc in instances)
                    {
                        sinceId = instanc.Value;
                    }
                }
                if (instance.Trim() != string.Empty && vh.checkVariableExists(instance, executionID))
                {
                    System.Data.DataTable dt = new TypeHandler().getInput(instance, "Variable", executionID);
                    var cx = Auth.SetUserCredentials(dt.Rows[0][StringHelper.consumerkey].ToString(), dt.Rows[0][StringHelper.consumersecret].ToString(), dt.Rows[0][StringHelper.accesstoken].ToString(), dt.Rows[0][StringHelper.accesssecret].ToString());
                    if (category.ToLower().Trim() == "home timeline")
                    {
                        if (readtype.ToLower().Trim() == "unread" && sinceId != string.Empty)
                        {
                            getHomeTimelineQuery += "home_timeline.json?since_id=" + sinceId + "&count=" + count;
                        }
                        else
                        {
                            getHomeTimelineQuery += "home_timeline.json?count=" + count;
                        }
                    }
                    else if (category.ToLower().Trim() == "mentions timeline")
                    {
                        if (readtype.ToLower().Trim() == "unread" && sinceId != string.Empty)
                        {
                            getHomeTimelineQuery += "mentions_timeline.json?since_id=" + sinceId + "&count=" + count;
                        }
                        else
                        {
                            getHomeTimelineQuery += "mentions_timeline.json?count=" + count;
                        }
                    }
                    else if (category.ToLower().Trim() == "user timeline")
                    {
                        if (readtype.ToLower().Trim() == "unread" && sinceId != string.Empty)
                        {
                            getHomeTimelineQuery += "user_timeline.json?since_id=" + sinceId + "&count=" + count;
                        }
                        else
                        {
                            getHomeTimelineQuery += "user_timeline.json?count=" + count;
                        }
                    }
                }
                // Execute Query can either return a json or a DTO interface
                var tweets = TwitterAccessor.ExecuteGETQuery<List<TweetDTO>>(getHomeTimelineQuery);

                result.Columns.Add("TweetId");
                result.Columns.Add("Text");
                result.Columns.Add("CreatedBy");
                result.Columns.Add("Source");
                result.Columns.Add("Language");
                result.Columns.Add("CreatedAt");
                result.Columns.Add("FavouriteCount");
                result.Columns.Add("RetweetCount");
                if (tweets.Count > 0)
                {
                    foreach (var t in tweets)
                    {
                        List<string> tweetL = new List<string>();
                        tweetL.Add(t.IdStr);
                        tweetL.Add(t.Text);
                        tweetL.Add(t.CreatedBy.ToString());
                        tweetL.Add(t.Source);
                        tweetL.Add(t.Language.ToString());
                        tweetL.Add(t.CreatedAt.ToString());
                        tweetL.Add(t.FavoriteCount.ToString());
                        tweetL.Add(t.RetweetCount.ToString());
                        result.Rows.Add(tweetL.ToArray());
                        tweetL.Clear();

                    }
                    var item = tweets[0];
                    latestsinceID = item.IdStr;
                    if (instances.Count <= 0)
                        setInstanceValue(StringHelper.admin, StringHelper.name, latestsinceID, step.Action_Id, StringHelper.twitter, step.Robot_Id, step.Id, category, true);
                    else
                        setInstanceValue(StringHelper.admin, StringHelper.name, latestsinceID, step.Action_Id, StringHelper.twitter, step.Robot_Id, step.Id, category, false);
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                vh.CreateVariable(output, JsonConvert.SerializeObject(result), StringHelper.truee, true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult TwitterPost( Steps step, string executionID)
        {
            string ret = string.Empty;
            string instance = string.Empty; string message = string.Empty; string attachment = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            DataTable result = new DataTable();
            try
            {
                instance = stp.getPropertiesValue(StringHelper.instance, step);
                message = stp.getPropertiesValue(StringHelper.message, step);
                attachment = stp.getPropertiesValue(StringHelper.attachment, step);
                string msg = String.Empty;
                if (instance.Trim() != string.Empty && vh.checkVariableExists(instance, executionID))
                {
                    System.Data.DataTable dt = new TypeHandler().getInput(instance, "Variable", executionID);
                    var cx = Auth.SetUserCredentials(dt.Rows[0][StringHelper.consumerkey].ToString(), dt.Rows[0][StringHelper.consumersecret].ToString(), dt.Rows[0][StringHelper.accesstoken].ToString(), dt.Rows[0][StringHelper.accesssecret].ToString());
                    try
                    {
                        if (message.Trim() != string.Empty && new VariableHandler().checkVariableExists(message, executionID))
                        {
                            System.Data.DataTable dtmsg = new TypeHandler().getInput(message, "variable", executionID);
                            message = String.Empty;
                            foreach (DataRow fr in dtmsg.Rows)
                            {
                                message += fr["Input" + msg].ToString();
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        string ec = e.Message;
                        message = msg;
                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                    }
                    List<string> files = new List<string>();
                    try
                    {
                        if (attachment.Trim() != string.Empty && new VariableHandler().checkVariableExists(attachment, executionID))
                        {
                            System.Data.DataTable dtattachments = new TypeHandler().getInput(attachment, "variable", executionID);
                            try
                            {
                                files = dtattachments.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                            }
                            catch (Exception e)
                            {
                                string exception = e.Message;
                                files = dtattachments.AsEnumerable().Select(r => r.Field<string>("Output")).ToList();
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        string ec = e.Message;
                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                    }
                    List<IMedia> media = new List<IMedia>();
                    foreach (var f in files)
                    {
                        //if (media.Count != 4)
                        //{
                        //    var fileBytes = System.IO.File.ReadAllBytes(f);
                        //    var filetype = System.Web.MimeMapping.GetMimeMapping(f);
                        //    if (filetype.ToLower().Split('/')[0] == "image")
                        //    {
                        //        media.Add(Upload.UploadImage(fileBytes));
                        //    }
                        //    else if (filetype.ToLower().Split('/')[0] == "video")
                        //    {
                        //        media.Add(Upload.UploadVideo(fileBytes));
                        //    }
                        //}

                    }
                    // var media = System.IO.File.ReadAllBytes(files[0]);
                    if (media.Count == 0)
                    {
                        var tweets = Tweet.PublishTweet(message);
                    }
                    else
                    {
                        var tweets = Tweet.PublishTweet(message, new PublishTweetOptionalParameters
                        {
                            Medias = media

                        });
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        /// <summary>
        /// https://developers.facebook.com/tools/access_token/ login facebook and add an app for app token and user token values
        /// https://lookup-id.com to find page id and userid for particular pages or users
        /// </summary>
        /// <param name="stepproperties"></param>
        /// <param name="memberName"></param>
        /// <param name="step"></param>
        /// <returns></returns>
        public MethodResult FacebookLogin( Steps step, string executionID)
        {
            string accesstoken = string.Empty; string pageid = string.Empty; string output = String.Empty;
            string ret = string.Empty;
            DataTable FbDt = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                output = stp.getPropertiesValue(StringHelper.output, step);
                accesstoken = stp.getPropertiesValue(StringHelper.accesstokenn, step);
                pageid = stp.getPropertiesValue(StringHelper.pageid, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                FbDt.Columns.Add(StringHelper.accesstoken);
                FbDt.Columns.Add(StringHelper.pageid);
                FbDt.Rows.Add(accesstoken, pageid);

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.errorcode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                vh.CreateVariable(output, JsonConvert.SerializeObject(FbDt), StringHelper.truee, true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult FacebookGet( Steps step, string executionID)
        {
            string instance = string.Empty; string output = String.Empty;
            string ret = string.Empty;
            DataTable dt = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                instance = stp.getPropertiesValue(StringHelper.instance, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                if (instance.Trim() != string.Empty && vh.checkVariableExists(instance, executionID))
                {
                    System.Data.DataTable instancedt = new TypeHandler().getInput(instance, "Variable", executionID);
                    var fbclient = new Facebook.FacebookClient(instancedt.Rows[0][StringHelper.accesstoken].ToString());
                    dynamic feed = fbclient.Get(instancedt.Rows[0][StringHelper.pageid] + "/feed");
                    JsonArray fbarray = feed[0];
                    List<PostDetails> Posts = new List<PostDetails>();
                    var F = fbarray.ToList();
                    dt.Columns.Add(StringHelper.id);
                    dt.Columns.Add(StringHelper.postid);
                    dt.Columns.Add(StringHelper.messages);
                    dt.Columns.Add(StringHelper.createdatetime);
                    dt.Columns.Add(StringHelper.likes);
                    dt.Columns.Add(StringHelper.comments);
                    for (int i = 0; i < F.Count; i++)
                    {
                        try
                        {
                            var post = (((Facebook.JsonObject)(F[i])).Values).ToList();
                            var postid = post[post.Count - 1];
                            dynamic postDetails = fbclient.Get(postid + "?fields= shares.summary(true),\n likes.summary(true), \n comments.summary(true),\n message.summary(true)");
                            var likes = postDetails["likes"]["summary"]["total_count"];
                            var comments = postDetails["comments"]["summary"]["total_count"];
                            var messages = post[1].ToString();
                            var id = postid.ToString();
                            PostDetails pd = new PostDetails();
                            pd.comments = comments.ToString();
                            pd.likes = likes.ToString();
                            pd.message = messages;
                            pd.postid = id;
                            Posts.Add(pd);
                            dt.Rows.Add(i, postid, messages, post[0].ToString(), likes, comments);
                        }
                        catch (Exception e)
                        {
                            new Logger().LogException(e);
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                        }
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.errorcode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                vh.CreateVariable(output, JsonConvert.SerializeObject(dt), StringHelper.truee, true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult FacebookPost( Steps step, string executionID)
        {
            string instance = string.Empty; string message = String.Empty; string attachment = String.Empty;
            string ret = string.Empty;
            DataTable dt = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                instance = stp.getPropertiesValue(StringHelper.instance, step);
                message = stp.getPropertiesValue(StringHelper.message, step);
                attachment = stp.getPropertiesValue(StringHelper.attachment, step);
                if (instance.Trim() != string.Empty && vh.checkVariableExists(instance, executionID))
                {
                    System.Data.DataTable instancedt = new TypeHandler().getInput(instance, "Variable", executionID);
                    var fbclient = new Facebook.FacebookClient(instancedt.Rows[0][StringHelper.accesstoken].ToString());
                    List<string> files = new List<string>();
                    try
                    {
                        if (attachment.Trim() != string.Empty && new VariableHandler().checkVariableExists(attachment, executionID))
                        {
                            System.Data.DataTable dtattachments = new TypeHandler().getInput(attachment, "variable", executionID);
                            try
                            {
                                files = dtattachments.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                            }
                            catch (Exception e)
                            {
                                string exception = e.Message;
                                files = dtattachments.AsEnumerable().Select(r => r.Field<string>("Output")).ToList();
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        string ec = e.Message;
                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                    }
                    if (files.Count != 0)
                    {
                        using (var file = new FacebookMediaStream
                        {
                            ContentType = StringHelper.imagejpeg,
                            FileName = Path.GetFileName(@files[0])
                        }.SetValue(System.IO.File.OpenRead(@files[0])))
                        {
                            dynamic result = fbclient.Post(StringHelper.photos, new { message = message, file });
                        }
                    }
                    else
                    {
                        dynamic result = fbclient.Post("/" + instancedt.Rows[0][StringHelper.pageid].ToString() + "/feed", new { message = message });
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.errorcode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        DAL.PostgresConnect dbd = new DAL.PostgresConnect();

        public List<UserInstance> getInstanceValue(string RobotID, string StepID, string timelinetype)
        {

            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "Select * from public.\"UserInstances\" where \"RobotID\"= " + RobotID + " and \"StepID\"= " + StepID + " and \"TimelineType\"='" + timelinetype + "' order by \"ID\" DESC";
                string result = dbd.DbExecuteQuery(query, "select");
                return JsonConvert.DeserializeObject<List<UserInstance>>(result);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return null;
            }
        }
        public bool setInstanceValue(string UserID, string Name, string Value, string ActivityID, string type, string RobotID, string StepID, string timelinetype, bool isInsert)
        {

            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (isInsert == false)
                {
                    string updatequery = "UPDATE public.\"UserInstances\" SET \"TimelineType\"='" + timelinetype + "',\"Value\" ='" + Value + "', \"UpdateDateTime\" = '" + DateTime.Now.ToString() + "' WHERE \"RobotID\" = " + RobotID + " and \"StepID\" = " + StepID + "";
                    string ret = dbd.DbExecuteQuery(updatequery, "update");
                }
                else
                {
                    string insertquery = "INSERT INTO public.\"UserInstances\" VALUES(default, '" + UserID + "', '" + Name + "', '" + Value + "', " + ActivityID + ", '" + type + "', " + RobotID + ", " + StepID + ",'" + timelinetype + "', 'admin', '" + DateTime.Now.ToString() + "', 'admin', '" + DateTime.Now.ToString() + "', true); ";
                    string ret = dbd.DbExecuteQuery(insertquery, "insert");
                }

                return false;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return false;
            }
        }
    }
    public class Posts
    {
        public string PostId { get; set; }
        public string PostStory { get; set; }
        public string PostMessage { get; set; }
        public string PostPicture { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
    }
    public class FacebookResult
    {
        public DateTime created_time { get; set; }
        public string message { get; set; }
        public string story { get; set; }
        public string id { get; set; }
    }
}