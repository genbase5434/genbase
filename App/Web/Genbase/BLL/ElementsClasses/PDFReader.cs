﻿using Genbase.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Newtonsoft.Json;
using System.Text;
using Genbase.Classes;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf;
using PdfSharp.Pdf.Content;
using PdfSharp.Pdf.Content.Objects;
using System.Collections.Specialized;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Xml;
//using Bytescout.PDFExtractor;

namespace Genbase.BLL.ElementsClasses
{
    public class PDFReader
    {
        StepPropertyHandler stp = new StepPropertyHandler();
        StatusModel s = new StatusModel();
        DataTable dt = new DataTable();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult GetText(Steps step, string executionID)
        {
            string ret = string.Empty; MethodResult mr = new MethodResult();

            DataTable returnValue = new DataTable();
            Dictionary<string, object> outputDict = new Dictionary<string, object>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string inp = String.Empty; bool isthere = false;
            returnValue.Columns.Add("Files"); returnValue.Columns.Add(StringHelper.Content);
            DataTable outputdt = new DataTable();
            string input = stp.getPropertiesValue(StringHelper.input, step);
            string output = stp.getPropertiesValue(StringHelper.output, step);
            string outputString = ""; var aa = "";

            try
            {
                dt = new TypeHandler().getInput(input, StringHelper.variable, executionID);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    inp = dt.Rows[i].ItemArray[0].ToString();
                }

                //// Create XMLExtractor instance
                //Bytescout.PDFExtractor.XMLExtractor extractor = new Bytescout.PDFExtractor.XMLExtractor();
                ////extractor.RegistrationName = "demo";
                ////extractor.RegistrationKey = "demo";

                //// Load sample PDF document
                //extractor.LoadDocumentFromFile(inp);
                ////extractor.LoadDocumentFromFile("C:/Users/raja.dubbaku/Desktop/RVM/sample.pdf");

                //// Get PDF document text as XML
                //string xmlText = extractor.GetXML();

                //// Load XML
                //XmlDocument xmlDocument = new XmlDocument();
                //xmlDocument.LoadXml(xmlText);

                //// Select all "control" nodes
                //XmlNodeList formControls = xmlDocument.SelectNodes("//control");

                //if (formControls.Count > 0)
                //{
                //    //outputString = xmlDocument.InnerText;
                //    aa = inp;
                //}
                //else
                //{
                    outputString = fileEntity("http://99.148.66.155:5002/pdf", inp, executionID, step, input);
                    aa = JsonConvert.DeserializeObject<string>(outputString);
                //}
               
                //FileProcess _process = new FileProcess();
                //_process = JsonConvert.DeserializeObject<FileProcess>(outputString);
                ////DataTable dtt = JsonConvert.DeserializeObject<DataTable>(outputString);
                foreach (var l in Variables.dynamicvariables)
                {
                    if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                    {
                        isthere = true;
                        dt.Rows.Add(l.vlvalue);
                        l.vlvalue = dt;
                        l.ExecutionID = executionID;
                        l.RobotID = step.Robot_Id;
                    }
                }
                if (!isthere)
                {
                    DVariables d = new DVariables();
                    d.vlname = output;
                    d.vlstatus = StringHelper.truee;
                    d.vlvalue = dt;
                    d.vltype = StringHelper.datatable;
                    d.ExecutionID = executionID;
                    d.RobotID = step.Robot_Id;
                    Variables.dynamicvariables.Add(d);
                }
                //outputdt.Columns.Add("Content");
                //foreach (var results in aa)
                //{
                //    string sentence = String.Empty;
                //    outputdt.Rows.Add(results.Key );
                //}
                goto retur;

            retur: returnValue.Rows.Add(aa);
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        private string fileEntity(string serviceURL, string filePath, string executionID, Steps step, string input)
        {
            DataTable outputdt = new DataTable(); DataTable returnValue = new DataTable();
            returnValue.Columns.Add(StringHelper.Content); string output = String.Empty;
            string filename = new StringFunction().getFileNameFromPath(filePath);
            string endPoint = serviceURL;
            string textinimage = HttpUploadFile(endPoint, filePath, "file", "application/json", new NameValueCollection());

            return textinimage;
        }
        public string HttpUploadFile(string url, string file, string paramName, string contentType, NameValueCollection nvc)
        {
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = System.Net.CredentialCache.DefaultCredentials;
            System.IO.Stream rs = wr.GetRequestStream();
            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);
            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, file, contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);
            FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();
            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();
            WebResponse wresp = null;
            string result = string.Empty;
            try
            {
                wresp = wr.GetResponse();
                System.IO.Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                result = reader2.ReadToEnd();
            }
            catch (Exception ex)
            {
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                    result = ex.Message;
                }
            }
            finally
            {
                wr = null;
            }
            return result;
        }
        public MethodResult AudiotoText(Steps step, string executionID)
        {
            string ret = string.Empty; MethodResult mr = new MethodResult(); string name = string.Empty;
            name = "new";



            DataTable returnValue = new DataTable();
            Dictionary<string, object> outputDict = new Dictionary<string, object>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string inp = String.Empty; bool isthere = false;
            //returnValue.Columns.Add(StringHelper.file);
            returnValue.Columns.Add(StringHelper.Content);
            DataTable outputdt = new DataTable();
            string input = stp.getPropertiesValue(StringHelper.input, step);
            string output = stp.getPropertiesValue(StringHelper.output, step);

            try
            {
                dt = new TypeHandler().getInput(input, StringHelper.variable, executionID);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    inp = dt.Rows[i].ItemArray[0].ToString();
                }
                string outputString = fileEntity("http://99.148.66.155:5002/Speech", inp, executionID, step, input);
                var aa = JsonConvert.DeserializeObject<string>(outputString);
                //FileProcess _process = new FileProcess();
                //_process = JsonConvert.DeserializeObject<FileProcess>(outputString);
                ////DataTable dtt = JsonConvert.DeserializeObject<DataTable>(outputString);
               
                


                foreach (var l in Variables.dynamicvariables)
                {
                    if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                    {
                        isthere = true;
                        dt.Rows.Add(l.vlvalue);
                        l.vlvalue = dt;
                        l.ExecutionID = executionID;
                        l.RobotID = step.Robot_Id;
                    }
                }
                if (!isthere)
                {
                    DVariables d = new DVariables();
                    d.vlname = output;
                    d.vlstatus = StringHelper.truee;
                    d.vlvalue = dt;
                    d.vltype = StringHelper.datatable;
                    d.ExecutionID = executionID;
                    d.RobotID = step.Robot_Id;
                    Variables.dynamicvariables.Add(d);
                }
                //outputdt.Columns.Add("Content");
                //foreach (var results in aa)
                //{
                //    string sentence = String.Empty;
                //    outputdt.Rows.Add(results.Key );
                //}
                goto retur;

            retur: returnValue.Rows.Add(aa);
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

    }

    public class FileProcess
    {
        public List<string> Columns { get; set; }
        public List<List<string>> Data { get; set; }
    }
}