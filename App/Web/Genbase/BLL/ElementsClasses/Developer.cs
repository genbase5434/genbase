﻿using Genbase.classes;
using Npgsql;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;
using Genbase.Classes;
using NetSuiteAnalyzer.classes;
using OfficeOpenXml;
using ClosedXML.Excel;
using System.Globalization;

namespace Genbase.BLL.ElementsClasses
{
    public class Developer
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        VariableHandler vh = new VariableHandler();
        TypeHandler th = new TypeHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public void Consolidation(Steps step, string executionID)
        {
            //try
            //{
            //    using (NpgsqlConnection con = new NpgsqlConnection("Server=192.168.1.9;Port=5432;User Id=genbase;Password=postgres;Database=Genbase"))
            //    {
            //        using (NpgsqlCommand cmd = new NpgsqlCommand("SELECT \"ResourceId\",\"ResourceName\",SUM(\"Hours\") as \"Total Hours\" FROM public.\"TIMESHEETS\" group by \"ResourceId\",\"ResourceName\""))
            //        {
            //            cmd.Connection = con;
            //            con.Open();

            //            using (NpgsqlDataAdapter ndr = new NpgsqlDataAdapter(cmd))
            //            {
            //                DataSet ds = new DataSet();
            //                System.Data.DataTable dt = new System.Data.DataTable();
            //                dt.Columns.AddRange(new DataColumn[1] { new DataColumn(StringHelper.sno) });
            //                dt.Columns[StringHelper.sno].AutoIncrement = true;
            //                dt.Columns[StringHelper.sno].AutoIncrementSeed = 1;
            //                dt.Columns[StringHelper.sno].AutoIncrementStep = 1;
            //                ndr.Fill(dt);
            //                cmd.ExecuteNonQuery();
            //                con.Close();
            //                //var excelApp = new Microsoft.Office.Interop.Excel.Application();
            //                var excelApp = null;
            //                excelApp.Workbooks.Add();

            //                // single worksheet
            //                //Microsoft.Office.Interop.Excel._Worksheet workSheet = excelApp.ActiveSheet;
            //                Microsoft.Office.Interop.Excel._Worksheet workSheet = null;


            //                for (var i = 0; i < dt.Columns.Count; i++)
            //                {
            //                    workSheet.Cells[1, i + 1] = dt.Columns[i].ColumnName;
            //                }

            //                // rows
            //                for (var i = 0; i < dt.Rows.Count; i++)
            //                {
            //                    // to do: format datetime values before printing
            //                    for (var j = 0; j < dt.Columns.Count; j++)
            //                    {
            //                        workSheet.Cells[i + 2, j + 1] = dt.Rows[i][j];
            //                    }
            //                }
            //                var excelFilePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\" + StringHelper.consolidatedtimesheets + DateTime.Now.ToString(StringHelper.ymdh) + ".xlsx";
            //                // check file path
            //                if (!string.IsNullOrEmpty(excelFilePath))
            //                {
            //                    try
            //                    {
            //                        workSheet.SaveAs(excelFilePath);
            //                        excelApp.Quit();
            //                        Marshal.ReleaseComObject(workSheet);
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        excelApp.Quit();
            //                        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
            //                    }
            //                }
            //                else
            //                { // no file path is given
            //                    excelApp.Quit();
            //                }
            //                Variables.namexl = excelFilePath;
            //            }
            //            con.Close();
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    new Logger().LogException(ex);
            //    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
            //    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            //}
        }
        public MethodResult ElseIf(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string LeftType = string.Empty; string RightType = string.Empty; string Input = string.Empty; string Output = string.Empty; string answer = string.Empty;
            string LeftValue = string.Empty; string RightValue = string.Empty; string Condition = string.Empty; string ILeftValue = string.Empty; string IRightValue = string.Empty;
            string success = string.Empty; string failure = string.Empty;
            try
            {
                LeftType = stp.getPropertiesValue(StringHelper.lefttype, step);
                RightType = stp.getPropertiesValue(StringHelper.righttype, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                LeftValue = stp.getPropertiesValue(StringHelper.leftvalue, step);
                RightValue = stp.getPropertiesValue(StringHelper.rightvalue, step);
                Condition = stp.getPropertiesValue(StringHelper.condition, step);
                success = stp.getPropertiesValue(StringHelper.success, step);
                failure = stp.getPropertiesValue(StringHelper.failure, step);

                DVariables value = new DVariables();
                DVariables Lvalue = new DVariables();
                DVariables Rvalue = new DVariables();
                ILeftValue = LeftValue;
                IRightValue = RightValue;
                if (Input.Trim() != string.Empty)
                {
                    value = new VariableHandler().getVariables(Input, executionID);
                }
                if (LeftType.Trim() != string.Empty && LeftType.Trim().ToLower() == StringHelper.variable)
                {
                    Lvalue = new VariableHandler().getVariables(LeftValue, executionID);
                    ILeftValue = Lvalue.vlvalue;
                }
                if (RightType.Trim() != string.Empty && RightType.Trim().ToLower() == StringHelper.variable)
                {
                    Rvalue = new VariableHandler().getVariables(RightValue, executionID);
                    IRightValue = Rvalue.vlvalue;
                }
                answer = "N";
                if (Condition == "!=")
                {
                    if (int.Parse(ILeftValue) != int.Parse(IRightValue))
                    {
                        answer = success;
                    }
                    else
                    {
                        answer = failure;
                    }
                }
                else if (Condition == "==")
                {
                    if (int.Parse(ILeftValue) == int.Parse(IRightValue))
                    {
                        answer = success;
                    }
                    else
                    {
                        answer = failure;
                    }
                }
                else if (Condition == "<")
                {
                    if (int.Parse(ILeftValue) < int.Parse(IRightValue))
                    {
                        answer = success;
                    }
                }
                else if (Condition == ">")
                {
                    if (int.Parse(ILeftValue) > int.Parse(IRightValue))
                    {
                        answer = success;
                    }
                    else
                    {
                        answer = failure;
                    }
                }
                else if (Condition == "<=")
                {
                    if (int.Parse(ILeftValue) <= int.Parse(IRightValue))
                    {
                        answer = success;
                    }
                    else
                    {
                        answer = failure;
                    }
                }
                else if (Condition == ">=")
                {
                    if (int.Parse(ILeftValue) >= int.Parse(IRightValue))
                    {
                        answer = success;
                    }
                    else
                    {
                        answer = failure;
                    }
                }

                new VariableHandler().CreateVariable(Output, answer, "", true, "string", step.Robot_Id, executionID);
                new ExecutionLog().add2Log(StringHelper.answer + answer, step.Robot_Id, step.Name, StringHelper.eventcode, false);
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult If(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>(); string[] domain;
            string LeftType = string.Empty; string RightType = string.Empty; string Input = string.Empty; string Output = string.Empty; string answer = string.Empty;
            string LeftValue = string.Empty; string RightValue = string.Empty; string Condition = string.Empty; string ILeftValue = string.Empty; string IRightValue = string.Empty;
            string success = string.Empty; string failure = string.Empty; string comparisiontype = string.Empty;

            new ExecutionLog().add2Log("we are in `IF` Step Right now comparing with Left Value", step.Robot_Id, step.Name, StringHelper.eventcode, false);

            try
            {
                LeftType = stp.getPropertiesValue(StringHelper.lefttype, step);
                RightType = stp.getPropertiesValue(StringHelper.righttype, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                LeftValue = stp.getPropertiesValue(StringHelper.leftvalue, step);
                RightValue = stp.getPropertiesValue(StringHelper.rightvalue, step);
                Condition = stp.getPropertiesValue(StringHelper.condition, step);
                success = stp.getPropertiesValue(StringHelper.success, step);
                failure = stp.getPropertiesValue(StringHelper.failure, step);
                comparisiontype = stp.getPropertiesValue(StringHelper.comparisiontype, step);
                //DVariables value = new DVariables();
                //Variables.dynamicvariables
                string value = "";
                DVariables Lvalue = new DVariables();
                DVariables Rvalue = new DVariables();
                ILeftValue = LeftValue;
                IRightValue = RightValue;
                DateTime outdate;
                if (Input.Trim() != string.Empty)
                {
                    DataTable dt = new TypeHandler().getInput(Input, "Variable", executionID);
                    if (dt.Rows.Count > 0)
                    {
                        value = dt.Rows[0][0].ToString();
                    }
                }
                if (LeftType.Trim() != string.Empty && LeftType.Trim().ToLower() == StringHelper.variable)
                {
                    Lvalue = new VariableHandler().getVariables(LeftValue, executionID);
                    if (Lvalue != null && Lvalue.vlvalue != null)
                    {
                        ILeftValue = Lvalue.vlvalue;
                        DataTable inputdt = new DataTable();
                        try
                        {
                            inputdt = JsonConvert.DeserializeObject<DataTable>(ILeftValue);
                            if (inputdt.Columns.Count > 0 && inputdt.Rows.Count > 0)
                            {
                                if (inputdt.Rows[0][0].ToString() != string.Empty)
                                {
                                    ILeftValue = inputdt.Rows[0][0].ToString();
                                }
                                else
                                {
                                    ILeftValue = "";
                                }
                            }
                            else
                            {
                                ILeftValue = "";
                            }
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                        }
                    }
                    else
                    {
                        ILeftValue = "";
                    }
                }
                if (RightType.Trim() != string.Empty && RightType.Trim().ToLower() == StringHelper.variable)
                {
                    Rvalue = new VariableHandler().getVariables(RightValue, executionID);

                    if (Rvalue != null && Rvalue.vlvalue != null)
                    {
                        IRightValue = Rvalue.vlvalue;
                        DataTable inputdt = new DataTable();
                        try
                        {
                            inputdt = JsonConvert.DeserializeObject<DataTable>(IRightValue);
                            if (inputdt.Columns.Count > 0 && inputdt.Rows.Count > 0)
                            {
                                IRightValue = inputdt.Rows[0][0].ToString();
                            }
                            else
                            {
                                IRightValue = "";
                            }
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                        }
                    }
                    else
                    {
                        IRightValue = "";
                    }
                }
                if (comparisiontype.ToLower().IndexOf("date") > -1)
                {
                    if (comparisiontype.ToLower().IndexOf("date") > -1 && value.Trim() != string.Empty)
                    {
                        if (DateTime.TryParse(value, out outdate))
                        {
                            answer = success;
                        }
                        else
                        {
                            answer = failure;
                        }
                    }
                    else
                    {
                        answer = failure;
                    }
                }
                else
                {
                    // Count
                    if (comparisiontype.ToLower() == "length")
                    {
                        if (LeftType.Trim() != string.Empty && LeftType.Trim().ToLower() == StringHelper.variable)
                        {
                            ILeftValue = ILeftValue.Length.ToString();
                        }
                        if (RightType.Trim() != string.Empty && RightType.Trim().ToLower() == StringHelper.variable)
                        {
                            IRightValue = IRightValue.Length.ToString();
                        }
                    }
                    //

                    if (Condition == "!=")
                    {
                        if (comparisiontype.ToLower() == "length")
                        {
                            if (int.Parse(ILeftValue) != int.Parse(IRightValue))
                            {
                                answer = success;
                            }
                            else
                            {
                                answer = failure;
                            }
                        }
                        else
                        {
                            //if (int.Parse(ILeftValue) != int.Parse(IRightValue))
                            if (ILeftValue != IRightValue)
                            {
                                answer = success;
                            }
                            else
                            {
                                answer = failure;
                            }
                        }
                    }
                    else if (Condition == "==")
                    {
                        if (comparisiontype.ToLower() == "length")
                        {
                            if (int.Parse(ILeftValue) == int.Parse(IRightValue))
                            {
                                answer = success;
                            }
                            else
                            {
                                answer = failure;
                            }
                        }
                        
                        else if (comparisiontype.ToLower().IndexOf("mailcomparision") > -1)
                        {
                            
                            if (Lvalue.vlvalue != null && Rvalue.vlvalue != null)
                            {
                                var Lrows = JsonConvert.DeserializeObject(Lvalue.vlvalue);
                                var from = JsonConvert.DeserializeObject(Rvalue.vlvalue);
                                int flag = 0;
                                if (Lrows.Count > 0 && from.Count > 0)
                                {
                                    for (int i = 0; i < Lrows.Count; i++)
                                    {
                                        string email = Lrows[i]["Sender"].Value;
                                        if (email.Contains('@'))
                                        {
                                            domain = email.Split('@');
                                        }
                                        else
                                        {
                                            string mail = email + "@";
                                            domain = mail.Split('@');
                                        }

                                        if (from[0]["From"].Value.Contains(domain[1]) && (from[0]["Subject"].Value.Contains(Lrows[i]["Keywords"].Value) || from[0]["Attachment"].Value.Contains(Lrows[i]["Attachments"].Value)))
                                        {
                                            answer = success;
                                            flag = 1;

                                            vh.CreateVariable(Input, JsonConvert.SerializeObject(Lrows[i]), StringHelper.truee, true, "datatable", step.Robot_Id, executionID);
                                        }
                                    }
                                }
                                if (flag == 0)
                                {
                                    answer = failure;
                                }
                            }
                            else
                            {
                                answer = failure;
                            }
                        }
                        else
                        {
                            //if (int.Parse(ILeftValue) == int.Parse(IRightValue))
                            if (ILeftValue.Trim() == IRightValue.Trim())
                            {
                                answer = success;
                            }
                            else
                            {
                                answer = failure;
                            }
                        }
                    }
                    else if (Condition == "<")
                    {
                        if (int.Parse(ILeftValue) < int.Parse(IRightValue))
                        {
                            answer = success;
                        }
                        else
                        {
                            answer = failure;
                        }
                    }
                    else if (Condition == ">")
                    {
                        if (int.Parse(ILeftValue) > int.Parse(IRightValue))
                        {
                            answer = success;
                        }
                        else
                        {
                            answer = failure;
                        }
                    }
                    else if (Condition == "<=")
                    {
                        if (int.Parse(ILeftValue) <= int.Parse(IRightValue))
                        {
                            answer = success;
                        }
                        else
                        {
                            answer = failure;
                        }
                    }
                    else if (Condition == ">=")
                    {
                        if (int.Parse(ILeftValue) >= int.Parse(IRightValue))
                        {
                            answer = success;
                        }
                        else
                        {
                            answer = failure;
                        }
                    }
                }

                new VariableHandler().CreateVariable(Output, answer, "", true, "string", step.Robot_Id, executionID);
                new ExecutionLog().add2Log(StringHelper.answer + answer, step.Robot_Id, step.Name, StringHelper.eventcode, false);
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult Else(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            //string Input = string.Empty;
            //string Output = string.Empty;
            string answer = string.Empty;
            //string success = string.Empty;
            //string failure = string.Empty;
            try
            {
                //Input = stp.getPropertiesValue(StringHelper.input, step);
                //Output = stp.getPropertiesValue(StringHelper.output, step);
                //success = stp.getPropertiesValue(StringHelper.success, step);
                //failure = stp.getPropertiesValue(StringHelper.failure, step);
                //DVariables value = new DVariables();
                //DVariables Rvalue = new DVariables();
                //if (Input.Trim() != string.Empty)
                //{
                //    value = new VariableHandler().getVariables(Input, executionID);
                //}
                answer = "Y";
               // new VariableHandler().CreateVariable("", answer, "", false, "string", step.Robot_Id, executionID);
                new ExecutionLog().add2Log(StringHelper.answer + answer, step.Robot_Id, step.Name, StringHelper.eventcode, false);
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult LoopStopold(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ILoopStart = string.Empty; string Input = string.Empty; string Output = string.Empty; string answer = string.Empty;
            try
            {
                ILoopStart = stp.getPropertiesValue(StringHelper.loopstartid, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult LoopStartold(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string InitialValue = string.Empty; string Input = string.Empty; string InputType = string.Empty;
            string Output = string.Empty; string IteratorCount = string.Empty; string Condition = string.Empty;
            string RightType = string.Empty; string RightValue = string.Empty; string LeftType = string.Empty; string LeftValue = string.Empty;
            string StepValue = string.Empty; string StepOperator = string.Empty; string success = string.Empty; string failure = string.Empty;
            DataTable input = new DataTable(); DataTable returnValues = new DataTable();

            try
            {


                InitialValue = stp.getPropertiesValue(StringHelper.initialvalue, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                InputType = stp.getPropertiesValue(StringHelper.inputtype, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                IteratorCount = stp.getPropertiesValue(StringHelper.iteratorcount, step);
                Condition = stp.getPropertiesValue(StringHelper.condition, step);
                RightType = stp.getPropertiesValue(StringHelper.righttype, step);
                RightValue = stp.getPropertiesValue(StringHelper.rightvalue, step);
                LeftType = stp.getPropertiesValue(StringHelper.lefttype, step);
                LeftValue = stp.getPropertiesValue(StringHelper.leftvalue, step);
                StepValue = stp.getPropertiesValue(StringHelper.stepvalue, step);
                StepOperator = stp.getPropertiesValue(StringHelper.stepoperator, step);
                success = stp.getPropertiesValue(StringHelper.success, step);
                failure = stp.getPropertiesValue(StringHelper.failure, step);
                int startv, endv, inpv;
                if (int.TryParse(LeftValue, out startv))
                {
                    LeftType = "Value";
                }
                else
                {
                    LeftType = "Variable";
                }
                if (int.TryParse(RightValue, out endv))
                {
                    RightType = "Value";
                }
                else
                {
                    RightType = "Variable";
                }
                if (int.TryParse(Input, out inpv))
                {
                    InputType = "Value";
                }
                else
                {
                    InputType = "Variable";
                }

                input = new TypeHandler().getInput(Input, InputType, executionID);


                DataTable rights = new DataTable();
                DataTable lefts = new DataTable();
                //LeftType = "Variable";
                //RightType = "Variable";
                rights = th.getInput(RightValue, RightType, executionID);
                lefts = th.getInput(LeftValue, LeftType, executionID);

                bool loopCompare = false;
                string leftvalue = "0";
                string initial = "0";
                int iteratestroke = 1;

                if (vh.checkVariableExists(IteratorCount, executionID))
                {
                    leftvalue = vh.getVariables(IteratorCount, executionID).vlvalue.ToString();
                }
                else
                {
                    if (InitialValue.Trim() != string.Empty)
                    {
                        leftvalue = InitialValue;
                        initial = InitialValue;
                    }
                    else
                    {
                        leftvalue = "0";
                    }
                }
                loopCompare = LoopCompare(leftvalue, rights.Rows.Count.ToString(), Condition);
                //lefts has single value;
                if (StepOperator.Trim() == "-")
                {
                    if (vh.checkVariableExists(IteratorCount, executionID)) leftvalue = vh.getVariables(IteratorCount, executionID).vlvalue.ToString();
                    else leftvalue = ((DataTable)(vh.getVariables(lefts.Rows[0][0].ToString(), executionID).vlvalue)).Rows.Count.ToString();
                    iteratestroke = intConverter(leftvalue) - intConverter(StepValue);
                    vh.CreateVariable(IteratorCount, iteratestroke, "", true, "int", step.Robot_Id, executionID);
                }
                else
                {
                    if (vh.checkVariableExists(IteratorCount, executionID)) leftvalue = vh.getVariables(IteratorCount, executionID).vlvalue.ToString();
                    else leftvalue = "0";
                    iteratestroke = intConverter(leftvalue) + intConverter(StepValue);
                    vh.CreateVariable(IteratorCount, iteratestroke, "", true, "int", step.Robot_Id, executionID);
                }

                string looper = string.Empty;

                if (loopCompare)
                {
                    //Success;
                    DataTable dt = new DataTable();
                    foreach (DataColumn dc in input.Columns)
                    {
                        dt.Columns.Add(dc.ColumnName);
                    }
                    dt.Rows.Add(input.Rows[intConverter(leftvalue)].ItemArray);

                    vh.CreateVariable(Output, JsonConvert.SerializeObject(dt), "", true, "datatable", step.Robot_Id, executionID);
                    looper = "Success";
                }
                else
                {
                    //Failure
                    if (InitialValue.Trim() != string.Empty)
                    {
                        leftvalue = InitialValue;
                    }
                    else
                    {
                        leftvalue = "0";
                    }
                    vh.CreateVariable(IteratorCount, leftvalue, "", true, "int", step.Robot_Id, executionID);
                    looper = "Failure";
                }
                vh.CreateVariable("loop" + step.Id, looper, "", true, "string", step.Robot_Id, executionID);

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult LoopStop(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ILoopStart = string.Empty; string Input = string.Empty; string Output = string.Empty; string answer = string.Empty;
            try
            {
                ILoopStart = stp.getPropertiesValue("loopstartid", step);
                Input = stp.getPropertiesValue("input", step);
                Output = stp.getPropertiesValue("output", step);

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult LoopStart(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string InitialValue = string.Empty;
            string Output = string.Empty; string Condition = string.Empty;
            string RightValue = string.Empty; string LeftValue = string.Empty;
            string StepVariable = string.Empty; string StepOperator = string.Empty; string success = string.Empty; string failure = string.Empty;

            DataTable input = new DataTable(); DataTable returnValues = new DataTable();

            try
            {
                InitialValue = stp.getPropertiesValue("initialvalue", step);
                Output = stp.getPropertiesValue("output", step);
                Condition = stp.getPropertiesValue("condition", step);
                RightValue = stp.getPropertiesValue("rightvalue", step);
                LeftValue = stp.getPropertiesValue("leftvalue", step);
                StepVariable = stp.getPropertiesValue("stepvalue", step);
                StepOperator = stp.getPropertiesValue("stepoperator", step);
                success = stp.getPropertiesValue("success", step);
                failure = stp.getPropertiesValue("failure", step);


                string initialvalue = new TypeHandler().getInput(InitialValue, executionID).Rows[0][0].ToString();


                string right = string.Empty; string left = string.Empty; int looper = 0;
                if (vh.checkVariableExists(RightValue, executionID)) //if right value is variable //<,<=
                {
                    right = new TypeHandler().getInput(RightValue, executionID).Rows[0][0].ToString();
                    if (!vh.checkVariableExists(StepVariable, executionID))
                    {
                        left = initialvalue;
                        looper = int.Parse(left);
                    }
                    else
                    {
                        left = new TypeHandler().getInput(StepVariable, executionID).Rows[0][0].ToString();
                        looper = int.Parse(left);
                    }
                }
                else if (vh.checkVariableExists(LeftValue, executionID)) //if left value is variable //>,>=
                {
                    left = new TypeHandler().getInput(LeftValue, executionID).Rows[0][0].ToString();
                    if (!vh.checkVariableExists(StepVariable, executionID))
                    {
                        right = initialvalue;
                        looper = int.Parse(right);
                    }
                    else
                    {
                        right = new TypeHandler().getInput(StepVariable, executionID).Rows[0][0].ToString();
                        looper = int.Parse(right);
                    }
                }
                else
                {
                    if (Condition.Contains(">"))
                    {
                        if (!vh.checkVariableExists(StepVariable, executionID))
                        {
                            right = RightValue;
                            looper = int.Parse(right);
                            left = LeftValue;
                        }
                        else
                        {
                            right = new TypeHandler().getInput(StepVariable, executionID).Rows[0][0].ToString();
                            looper = int.Parse(right);
                            left = LeftValue;
                        }
                    }
                    else
                    {
                        if (!vh.checkVariableExists(StepVariable, executionID))
                        {
                            left = LeftValue;
                            looper = int.Parse(left);
                            right = RightValue;
                        }
                        else
                        {
                            left = new TypeHandler().getInput(StepVariable, executionID).Rows[0][0].ToString();
                            looper = int.Parse(left);
                            right = RightValue;
                        }
                    }
                }

                if (LoopCompare(left, right, Condition)) //loop condition //success
                {
                    if (StepOperator.Trim() == "++" || StepOperator.Trim() == "+")
                    {
                        looper = looper + 1;
                    }
                    else if (StepOperator.Trim() == "--" || StepOperator.Trim() == "-")
                    {
                        looper = looper - 1;
                    }
                    else
                    {
                        looper = looper + 1;
                    }

                    DataTable dtv = new DataTable(); dtv.Columns.Add("Value"); dtv.Rows.Add(looper.ToString()); //next iteration number
                    vh.CreateVariable(StepVariable, JsonConvert.SerializeObject(dtv), "true", true, "datatable", step.Robot_Id, executionID);

                    vh.CreateVariable("IloopStart:" + step.Id, "Success", "true", true, "string", step.Robot_Id, executionID);

                    DataTable dto = new DataTable(); dto.Columns.Add("Value"); dto.Rows.Add(left); //current iteration number
                    vh.CreateVariable(Output, JsonConvert.SerializeObject(dto), "true", true, "datatable", step.Robot_Id, executionID);
                }
                else //loop condition //failure
                {
                    vh.CreateVariable("IloopStart:" + step.Id, "Failure", "true", true, "string", step.Robot_Id, executionID);
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }

            MethodResult mr;

            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult Filter(Steps step, string executionID)
        {
            string ret = string.Empty; string output = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string source = string.Empty, filtercolumn = string.Empty, filterstring = string.Empty, filtercount = string.Empty, sortorder = string.Empty, sortordercolumn = string.Empty, result = string.Empty;
            try
            {
                source = stp.getPropertiesValue(StringHelper.source, step);
                filtercolumn = stp.getPropertiesValue(StringHelper.filtercolumn, step);
                filterstring = stp.getPropertiesValue(StringHelper.filterstring, step);
                filtercount = stp.getPropertiesValue(StringHelper.filtercount, step);
                sortorder = stp.getPropertiesValue(StringHelper.sortorder, step);
                sortordercolumn = stp.getPropertiesValue(StringHelper.sortordercolumn, step);
                result = stp.getPropertiesValue(StringHelper.result, step);

                DVariables value = new DVariables();
                if (source.Trim() != string.Empty)
                {
                    value = new VariableHandler().getVariables(source, executionID);
                    if (value != null && JsonConvert.DeserializeObject<DataTable>(value.vlvalue).GetType() == typeof(DataTable))
                    {
                        DataTable dt = JsonConvert.DeserializeObject<DataTable>(value.vlvalue);
                        if (dt.Columns[filtercolumn] != null && (filtercolumn != string.Empty))
                        {

                            DataTable dtMarks1 = dt.Clone();
                            dtMarks1.Columns[filtercolumn].DataType = Type.GetType(StringHelper.systemstring);
                            int countRow = 0;
                            foreach (DataRow dr in dt.Rows)
                            {
                                int count = 0;
                                if (countRow != 0)
                                {
                                    for (int i = 0; i < dt.Columns.Count; i++)
                                    {
                                        if (dr.ItemArray[i].ToString().IndexOf(filterstring) > -1)
                                        {
                                            count++;
                                        }
                                    }
                                }
                                countRow++;
                                if (count > 0)
                                {
                                    dtMarks1.ImportRow(dr);
                                }

                            }

                            dtMarks1.AcceptChanges();
                            DataView dv = dtMarks1.DefaultView;
                            if (filtercolumn != string.Empty)
                            {
                                dv.Sort = filtercolumn;
                            }
                            dtMarks1 = dv.ToTable();
                            DataTable dtMarks2 = dt.Clone();
                            int n, x;
                            if (filtercount != string.Empty && int.TryParse(filtercount, out n))
                            {
                                DataRow dr1 = dt.Rows[0];
                                dtMarks2.ImportRow(dr1);
                                if (n <= dtMarks1.Rows.Count)
                                {
                                    x = n;
                                }
                                else
                                {
                                    x = dtMarks1.Rows.Count;
                                }
                                for (int k = 0; k < x; k++)
                                {
                                    dtMarks2.ImportRow(dtMarks1.Rows[k]);
                                }
                                dtMarks2.AcceptChanges();
                            }
                            output = JsonConvert.SerializeObject(dtMarks2);
                        }
                        else if (filterstring != string.Empty)
                        {
                            DataTable dtMarks1 = dt.Clone();
                            dtMarks1.Columns[1].DataType = Type.GetType(StringHelper.systemstring);
                            int countRow = 0;
                            foreach (DataRow dr in dt.Rows)
                            {
                                int count = 0;
                                if (countRow > 0)
                                {
                                    for (int i = 0; i < dt.Columns.Count; i++)
                                    {
                                        if (dr.ItemArray[i].ToString().IndexOf(filterstring) > -1)
                                        {
                                            count++;
                                        }
                                    }
                                }

                                countRow++;
                                if (count > 0)
                                {
                                    dtMarks1.ImportRow(dr);
                                }

                            }

                            dtMarks1.AcceptChanges();
                            DataView dv = dtMarks1.DefaultView;

                            dtMarks1 = dv.ToTable();
                            DataTable dtMarks2 = dt.Clone();
                            int n, x;
                            if (filtercount != string.Empty && int.TryParse(filtercount, out n))
                            {
                                DataRow dr1 = dt.Rows[0];
                                dtMarks2.ImportRow(dr1);
                                if (n <= dtMarks1.Rows.Count)
                                {
                                    x = n;
                                }
                                else
                                {
                                    x = dtMarks1.Rows.Count;
                                }
                                for (int k = 0; k < x; k++)
                                {
                                    dtMarks2.ImportRow(dtMarks1.Rows[k]);
                                }
                                dtMarks2.AcceptChanges();
                                output = JsonConvert.SerializeObject(dtMarks2);
                            }
                            else
                            {
                                output = JsonConvert.SerializeObject(dtMarks1);
                            }

                        }
                        else if (filtercount.Trim() != string.Empty && filterstring.Trim() == string.Empty)
                        {
                            int n;
                            DataTable dtxn = new DataTable();
                            if (int.TryParse(filtercount, out n))
                            {
                                dtxn = dt.Rows.Cast<System.Data.DataRow>().Take(n).CopyToDataTable();
                                output = JsonConvert.SerializeObject(dtxn);
                            }
                        }
                    }
                }
                new VariableHandler().CreateVariable(result, output, "", true, "datatable", step.Robot_Id, executionID);
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        //public MethodResult Tables(Steps step, string executionID)
        //{
        //    string ret = string.Empty;
        //    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
        //    string input = string.Empty; string withheader = string.Empty; string inputtype = string.Empty; string output = string.Empty; DataTable dtr = new DataTable();
        //    try
        //    {
        //        input = stp.getPropertiesValue(StringHelper.input, step);
        //        output = stp.getPropertiesValue(StringHelper.output, step);
        //        withheader = stp.getPropertiesValue(StringHelper.withheader, step);
        //        inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);

        //        List<string> rows = new List<string>();
        //        List<string> HeaderRow = new List<string>();

        //        string text = string.Empty;

        //        DVariables value = new DVariables();

        //        if (inputtype.ToLower() == StringHelper.variable)
        //        {
        //            if (input.Trim() != string.Empty)
        //            {
        //                value = new VariableHandler().getVariables(input, executionID);
        //                if (value != null && value.vlvalue.GetType() == typeof(string))
        //                {
        //                    text = value.vlvalue;
        //                }
        //            }
        //        }
        //        else if (inputtype.ToLower() == StringHelper.value)
        //        {
        //            text = input;
        //        }

        //        text = text.TrimStart();
        //        //rows
        //        if (text.IndexOf("\n") > -1)
        //        {
        //            rows = text.Split(new string[] { "\n" }, StringSplitOptions.None).ToList();
        //        }
        //        //HeaderRow
        //        if (rows[0].IndexOf(" ") > -1)
        //        {
        //            HeaderRow = rows[0].Split(new string[] { " ", "  ", "   ", "  " }, StringSplitOptions.None).ToList();
        //        }
        //        HeaderRow.ForEach(x => x.Trim()); HeaderRow.RemoveAll(x => x == string.Empty);


        //        DataTable dt = new DataTable();

        //        dt.Columns.Add(" "); //db id
        //        dt.Columns.Add("  "); //db runid
        //        foreach (string head in HeaderRow)
        //        {
        //            dt.Columns.Add(head);
        //        }

        //        foreach (string row in rows)
        //        {
        //            List<string> rower = new List<string>();
        //            rower = row.Split(new string[] { " ", "  ", "   ", "  " }, StringSplitOptions.None).ToList();
        //            rower.ForEach(x => x.Trim()); rower.RemoveAll(x => x == string.Empty);
        //            rower.Insert(0, "");
        //            rower.Insert(1, "");
        //            dt.Rows.Add(rower.ToArray());
        //        }
        //        //rows

        //        if (withheader.Trim().ToLower() == StringHelper.ffalse)
        //        {
        //            dt.Rows[0].Delete();
        //        }
        //        else
        //        {

        //        }
        //        dtr = dt;
        //        ret = StringHelper.success;
        //    }
        //    catch (Exception ex)
        //    {
        //        new Logger().LogException(ex);
        //        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
        //        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
        //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

        //    }
        //    MethodResult mr;
        //    if (ret == StringHelper.success && dtr != null)
        //    {
        //        new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(dtr), "", false, "datatable", step.Robot_Id, executionID);
        //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //        s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
        //        mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
        //    }
        //    else
        //    {
        //        mr = new MethodResultHandler().createResultType(false, msgs, null, s);
        //    }
        //    return mr;
        //}

        public MethodResult Tables(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string input = string.Empty; string RowSeparator = string.Empty;
            string Headers = string.Empty;
            string HeadersSeparator = string.Empty;
            int HeaderCount =0;
            string ColumnSeparator = string.Empty; string output = string.Empty; DataTable dtr = new DataTable(); DataTable dt = new DataTable();
            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                RowSeparator = stp.getPropertiesValue(StringHelper.RowSeparator, step);
                ColumnSeparator = stp.getPropertiesValue(StringHelper.ColumnSeparator, step);
                Headers = stp.getPropertiesValue(StringHelper.Headers, step);
                HeadersSeparator = stp.getPropertiesValue(StringHelper.HeadersSeparator, step);
                List<string> rows = new List<string>();
                List<string> HeaderRow = new List<string>();
                List<string> Header = new List<string>();
                DVariables value = new DVariables();

                if (input.StartsWith("{"))
                {
                    if (input.Trim() != string.Empty)
                    {
                        value = new VariableHandler().getVariables(input, executionID);
                        if (value != null && value.vlvalue.GetType() == typeof(string))
                        {
                            input = value.vlvalue;
                            rows = input.Split(new string[] { RowSeparator }, StringSplitOptions.None).ToList();
                        }
                    }
                }
                else
                {
                    rows = input.Split(new string[] { RowSeparator }, StringSplitOptions.None).ToList();
                }

                if (!string.IsNullOrEmpty(Headers))
                {
                    Header = Headers.Split(new string[] { HeadersSeparator }, StringSplitOptions.None).ToList();
                    HeaderCount = Header.Count();
                    foreach (string item in Header)
                    {
                        dt.Columns.Add(item);
                    }
                }

                else
                {
                    HeaderCount = rows.Count;
                    for (int a = 0; a < rows.Count; a++)
                    {
                        dt.Columns.Add();
                    } 
                    
                }

                int i = 0;
                foreach (string row in rows)
                {
                    List<string> Columns = new List<string>();
                    Columns = row.Split(new string[] { ColumnSeparator }, StringSplitOptions.None).ToList();
                   
                    if (HeaderCount == Columns.Count)
                    {
                        object[] o = new object[HeaderCount];
                        foreach (string item in Columns)
                        {
                            o[i] = item;
                            i++;
                        }
                        dt.Rows.Add(o);
                        o.Clone();
                        i = 0;
                    }
                    else if (HeaderCount < Columns.Count)
                    {
                        object[] o = new object[Columns.Count];
                        for (int j = HeaderCount; j < Columns.Count; j++)
                        {
                            dt.Columns.Add("");
                        }
                        foreach (string item in Columns)
                        {
                            o[i] = item;
                            i++;
                        }
                        dt.Rows.Add(o);
                        o.Clone();
                        i = 0;
                    }
                }

                dtr = dt;
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success && dtr != null)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(dtr), "", false, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult NonceGenerator(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string validChars = string.Empty; string typegenerated = string.Empty; string output = string.Empty; string length = string.Empty;

            Random random = new Random();

            DataTable returnValue = new DataTable(); returnValue.Columns.Add("Content");

            output = stp.getPropertiesValue(StringHelper.output, step);
            length = stp.getPropertiesValue(StringHelper.length, step);
            typegenerated = stp.getPropertiesValue(StringHelper.typegenerated, step);

            if (string.IsNullOrEmpty(length))
            {
                length = "8";
            }

            if ((typegenerated.Trim().ToLower() == "only alphabets(upper and lower)") ||(typegenerated.Trim()=="Aa"))
            {
                validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            }
            else if ((typegenerated.Trim().ToLower() == "only alphabets(upper and lower cases) and numbers")|| (typegenerated.Trim() == "Aa1"))
            {
                validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            }
            else if ((typegenerated.Trim().ToLower() == "only alphabets upper case")|| (typegenerated.Trim() == "A"))
            {
                validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            }
            else if ((typegenerated.Trim().ToLower() == "only alphabets lower case")|| (typegenerated.Trim() == "a"))
            {
                validChars = "abcdefghijklmnopqrstuvwxyz";
            }
            else if ((typegenerated.Trim().ToLower() == "only alphabets upper case and numbers")|| (typegenerated.Trim() == "A1"))
            {
                validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            }
            else if ((typegenerated.Trim().ToLower() == "only alphabets lower case and numbers")|| (typegenerated.Trim() == "a1"))
            {
                validChars = "abcdefghijklmnopqrstuvwxyz0123456789";
            }
            else if ((typegenerated.Trim().ToLower() == "only numbers")|| (typegenerated.Trim() == "1"))
            {
                validChars = "0123456789";
            }
            //else if (typegenerated.Trim().ToLower() == "alphanumeric without special characters")
            //{
            //    validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            //}
            //else if ((typegenerated.Trim().ToLower() == "alphanumeric with special characters")|| (typegenerated.Trim() == "Aa1_"))
            //{
            //    validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~!@#$%^&*()_+-={}[]:\"|;'\\<>?,./ ";
            //}
            else
            {
                validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~!@#$%^&*()_+-={}[]:\"|;'\\<>?,./ ";
            }

            int l = 0; var nonceString = new StringBuilder();


            try
            {
                
                if (int.TryParse(length, out l))
                {
                    for (int i = 0; i < int.Parse(length); i++)
                    {
                        nonceString.Append(validChars[random.Next(0, validChars.Length - 1)]);
                    }
                }

                new ExecutionLog().add2Log(nonceString.ToString(), step.Robot_Id, step.Name, StringHelper.eventcode, false);

                returnValue.Rows.Add(nonceString.ToString());

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "string", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);

            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        private bool LoopCompare(string left, string right, string condition)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (condition.Trim() == "==" || condition.Trim() == "=" || condition.Trim().ToLower() == "equalsto")
                {
                    if (intConverter(left) == intConverter(right))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (condition.Trim() == "<" || condition.Trim().ToLower() == "lesserthan")
                {
                    if (intConverter(left) < intConverter(right))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (condition.Trim() == ">" || condition.Trim().ToLower() == "greaterthan")
                {
                    if (intConverter(left) > intConverter(right))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (condition.Trim() == "<=" || condition.Trim().ToLower() == "lesserthanandequalto")
                {
                    if (intConverter(left) <= intConverter(right))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (condition.Trim() == ">=" || condition.Trim().ToLower() == "greaterthanandequalto")
                {
                    if (intConverter(left) >= intConverter(right))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (condition.Trim() == "!=" || condition.Trim().ToLower() == "notequalsto")
                {
                    if (intConverter(left) != intConverter(right))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return true;
            }
        }

        private int intConverter(string stringer)
        {
            int returns = 0;
            if (int.TryParse(stringer, out returns))
            {
                return returns;
            }
            else
            {
                return -1;
            }
        }

        public MethodResult TwoDto1D(Steps step, string executionID)
        {
            string ret = string.Empty;
            DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<string> SplitInputs = new List<string>();
            string Input = string.Empty; string Output = string.Empty;
            string ReadType = string.Empty;

            try
            {
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                ReadType = stp.getPropertiesValue(StringHelper.readtype, step);

                if (ReadType.Trim().ToLower().IndexOf("column") > -1)
                {
                    if (Input.IndexOf("[") > -1)
                    {
                        SplitInputs = Input.Split(']').ToList();
                        List<string> SelectColumn = new List<string>();
                        SelectColumn = SplitInputs[0].Split('[').ToList();
                        string InputColumn = SelectColumn[1].ToString();
                        Input = SelectColumn[0].ToString() + "}";
                        returnValue = new TypeHandler().OneDConversionsVariable(Input, InputColumn, executionID);
                    }
                    else
                    {
                        returnValue = new TypeHandler().OneDConversionsVariable(Input, null, executionID);
                    }
                }
                else if (ReadType.Trim().ToLower().IndexOf("row") > -1)
                {
                    if (Input.IndexOf("[") > -1)
                    {
                        SplitInputs = Input.Split(']').ToList();
                        List<string> SelectColumn = new List<string>();
                        SelectColumn = SplitInputs[0].Split('[').ToList();
                        string InputColumn = SelectColumn[1].ToString();
                        Input = SelectColumn[0].ToString() + "}";
                        returnValue = new TypeHandler().OneDConversionsVariableforRow(Input, InputColumn, executionID);
                    }
                    else
                    {
                        returnValue = new TypeHandler().OneDConversionsVariableforRow(Input, null, executionID);
                    }
                }
                else
                {
                    if (Input.IndexOf("[") > -1)
                    {
                        SplitInputs = Input.Split(']').ToList();
                        List<string> SelectColumn = new List<string>();
                        SelectColumn = SplitInputs[0].Split('[').ToList();
                        string InputColumn = SelectColumn[1].ToString();
                        Input = SelectColumn[0].ToString() + "}";
                        returnValue = new TypeHandler().OneDConversionsVariable(Input, InputColumn, executionID);
                    }
                    else
                    {
                        returnValue = new TypeHandler().OneDConversionsVariable(Input, null, executionID);
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult TwoDtoScalar(Steps step, string executionID)
        {
            string ret = string.Empty;

            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<string> SplitInputs = new List<string>();
            DataTable returnValue = new DataTable();

            string Input = string.Empty; string Output = string.Empty;
            try
            {
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                int BraceCount = Input.ToCharArray().Count(c => c == '[');
                if (BraceCount > 1)
                {
                    SplitInputs = Input.Split(']').ToList();
                    List<string> SelectColumn = new List<string>();
                    List<string> SelectRow = new List<string>();
                    SelectRow = SplitInputs[0].Split('[').ToList();
                    SelectColumn = SplitInputs[1].Split('[').ToList();
                    string InputColumn = SelectColumn[1].ToString();
                    string InputRow = SelectRow[1].ToString();
                    returnValue = new TypeHandler().TwoDScalarConversionVariable(Input, InputRow, InputColumn, executionID);

                }
                else
                {
                    if (Input.IndexOf("[") > -1)
                    {
                        SplitInputs = Input.Split(']').ToList();
                        List<string> SelectRow = new List<string>();
                        SelectRow = SplitInputs[0].Split('[').ToList();
                        string InputRow = SelectRow[1].ToString();
                        returnValue = new TypeHandler().TwoDScalarConversionVariable(Input, InputRow, "0", executionID);
                    }
                    else
                    {
                        returnValue = new TypeHandler().TwoDScalarConversionVariable(Input, null, null, executionID);
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult OneDtoScalar(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<string> SplitInputs = new List<string>();
            DataTable returnValue = new DataTable();
            string Input = string.Empty; string Output = string.Empty;
            try
            {
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                if (Input.IndexOf("[") > -1)
                {
                    SplitInputs = Input.Split(']').ToList();
                    List<string> SelectRow = new List<string>();
                    SelectRow = SplitInputs[0].Split('[').ToList();
                    string InputRow = SelectRow[1].ToString();
                    returnValue = new TypeHandler().OneDScalarConversionVariable(Input, InputRow, executionID);
                }
                else
                {
                    returnValue = new TypeHandler().OneDScalarConversionVariable(Input, null, executionID);
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult OneDto2D(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            DataTable returnValue = new DataTable();
            List<string> SplitInputs = new List<string>();
            string Input = string.Empty; string Output = string.Empty; string TwoDValue = string.Empty;
            try
            {
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                TwoDValue = stp.getPropertiesValue(StringHelper.twodvalue, step);
                if (TwoDValue != null)
                {
                    returnValue = new TypeHandler().OneD2DConversionVariable(Input, TwoDValue, executionID);
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult Concat(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            returnValue.Columns.Add("Result");
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string input = string.Empty; string output = string.Empty; string seperator = string.Empty;
            string result = string.Empty;
            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                seperator = stp.getPropertiesValue(StringHelper.seperator, step);

                string[] inputs = input.Split(new string[] { "+" }, StringSplitOptions.None);
                List<string> words = new List<string>();

                for (var i = 0; i < inputs.Length; i++)
                {
                    List<string> SplitInputs = new List<string>();
                    int BraceCount = inputs[i].Trim().ToCharArray().Count(c => c == '[');
                    if (BraceCount > 1)
                    {

                        SplitInputs = inputs[i].Trim().Split(']').ToList();
                        List<string> SelectColumn = new List<string>();
                        List<string> SelectRow = new List<string>();
                        SelectRow = SplitInputs[0].Split('[').ToList();
                        SelectColumn = SplitInputs[1].Split('[').ToList();
                        string InputColumn = SelectColumn[1].ToString();
                        string InputRow = SelectRow[1].ToString();
                        DataTable _dt = new DataTable();
                        _dt = new TypeHandler().TwoDScalarConversionVariable(inputs[i], InputRow, InputColumn, executionID);
                        if (_dt.Columns.Count > 0 && _dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in _dt.Rows)
                            {
                                words.Add(dr[0].ToString());
                            }
                        }

                    }
                    else
                    {

                        if (vh.checkVariableExists(inputs[i].Trim(), executionID))
                        {
                            DataTable dt = new TypeHandler().getInput(inputs[i].Trim(), executionID);

                            if (dt.Columns.Count > 0 && dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    words.Add(dr[0].ToString().Trim());
                                }
                            }

                        }
                        else
                        {
                            words.Add(inputs[i]);
                        }
                    }
                }

                words.RemoveAll(w => w.Trim() == string.Empty);

                seperator.Replace(@"+", "");
                result = string.Join(seperator.ToString().Trim(), words);
                returnValue.Rows.Add(result);

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult GetValueByKey(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string input = string.Empty; string output = string.Empty; string seperator = string.Empty;
            string IsNumeric = string.Empty; string Trim = string.Empty; string Validation = string.Empty;
            string keyset = string.Empty;

            string result = string.Empty;
            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                seperator = stp.getPropertiesValue(StringHelper.seperator, step);
                keyset = stp.getPropertiesValue(StringHelper.key, step);

                if (keyset.Length > 0)
                {
                    if (keyset.ToLower() != "row")
                    {

                        returnValue.Columns.Add("Key");
                        returnValue.Columns.Add("Value");
                        List<string> Keys = keyset.Split(';').ToList();

                        if (vh.checkVariableExists(input, executionID))
                        {
                            DataTable dtr = new DataTable();
                            dtr = new TypeHandler().getInput(input, executionID);

                            foreach (DataRow dr in dtr.Rows)
                            {
                                string Text = dr[0].ToString();
                                foreach (string Key in Keys)
                                {
                                    List<string> resultant = getText(Text, Key.Trim(), seperator);
                                    if (resultant.Count > 0)
                                    {
                                        for (int i = 0; i < resultant.Count; i++)
                                        {
                                            returnValue.Rows.Add(Key.Trim(), resultant[i]);
                                        }
                                        //returnValue.Rows.Add(Key.Trim(), resultant[0]);
                                    }
                                    else
                                        returnValue.Rows.Add(Key.Trim(), resultant[0]);
                                }
                            }
                        }
                        else
                        {
                            foreach (string Key in Keys)
                            {
                                List<string> resultant = getText(input, Key.Trim(), seperator);
                                returnValue.Rows.Add(Key.Trim(), resultant[0]);
                            }
                        }
                    }
                    else
                    {
                        ////


                        if (vh.checkVariableExists(input, executionID))
                        {
                            DataTable dtr = new DataTable();
                            dtr = new TypeHandler().getInput(input, executionID);

                            DataTable dtr_sep = new DataTable();
                            dtr_sep = new TypeHandler().getInput(seperator, executionID);

                            DataTable _objdt = new DataTable();
                            if (dtr.Rows.Count > 1)
                            {
                                int i = 1;
                                int col = dtr.Columns.Count;
                                foreach (DataRow dr in dtr.Rows)
                                {
                                    if (i == 1)
                                    {
                                        for (int s = 0; s < col; s++)
                                        {
                                            if (_objdt.Columns.Contains(dr[s].ToString()))
                                            {
                                                _objdt.Columns.Add(dr[s].ToString() + " ");
                                                returnValue.Columns.Add(dr[s].ToString() + " ");
                                            }
                                            else
                                            {
                                                _objdt.Columns.Add(dr[s].ToString());

                                                returnValue.Columns.Add(dr[s].ToString());
                                            }
                                        }

                                    }
                                    i++;

                                    _objdt.Rows.Add(dr.ItemArray);
                                }

                                if (_objdt.Columns.Count > 0 && _objdt.Rows.Count > 0)
                                {
                                    foreach (DataRow row in _objdt.Rows)
                                    {
                                        int chk_s = 0;
                                        foreach (DataColumn column in _objdt.Columns)
                                        {
                                            if (column.ToString().ToLower() == "order ref")
                                            {
                                                string snumber = ((dtr_sep.Rows[0])["Value"]).ToString().Trim();
                                                if (Convert.ToString(row[column]).Contains(snumber))
                                                {
                                                    chk_s = 1;

                                                }
                                            }
                                        }

                                        if (chk_s == 1)
                                        {
                                            returnValue.Rows.Add(row.ItemArray);
                                            break;
                                        }
                                    }
                                }

                            }

                        }
                        //////
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult GetKeyValuePair(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string dictionary = string.Empty; string value = string.Empty; string key = string.Empty;

            try
            {
                dictionary = stp.getPropertiesValue(StringHelper.dictionary, step);
                value = stp.getPropertiesValue(StringHelper.value, step);
                key = stp.getPropertiesValue(StringHelper.key, step);

                DataTable dt = new DataTable();
                dt.Columns.Add("Value");
                string xkey = string.Empty;
                string xvalue = string.Empty;

                if (vh.checkVariableExists(key, executionID))
                {
                    DVariables ykey = vh.getVariables(key, executionID);
                    xkey = JsonConvert.DeserializeObject<DataTable>(ykey.vlvalue).Rows[0][0];
                }
                else
                {
                    xkey = key;
                }

                DataTable inpdt = new TypeHandler().getInput(dictionary, executionID);

                foreach (DataRow drinp in inpdt.Rows)
                {
                    try
                    {
                        if (drinp[0].ToString().Trim() == xkey.Trim())
                        {
                            xvalue = drinp[1].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                    }
                }

                returnValue.Columns.Add("Value");
                returnValue.Rows.Add(xvalue);
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(value, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public List<string> getText(string text, string key, string seperator)
        {
            List<string> Result = new List<string>(); List<string> Lines = new List<string>();
            List<string> res = new List<string>();
            string text1=  text.Replace(":\n\n", ":").Replace(":\n\n☐",":").Replace("\n☐",",").Replace("☐","");
          
            Lines = text1.Split(new string[] { "\\n", "\n" }, StringSplitOptions.None).ToList();
            Lines.RemoveAll(x => x.Trim() == string.Empty);
            
            foreach (string Line in Lines)
            {
                if (Line.IndexOf(key) > -1)
                {
                    //int keystartpart = Line.IndexOf(key);
                    //int keystoppart = keystartpart + (key.Length - 1);
                    int keycount = Regex.Matches(Line, key).Count;

                    List<string> LineParts = Line.Split(new string[] { key }, StringSplitOptions.None).ToList();
                    LineParts.RemoveAll(x => x.Trim() == string.Empty);

                    for (int i = 0; i < LineParts.Count; i++)
                    {
                        Regex reg = new Regex("[;\\\\/:*?\"<>|&']");
                        string str = reg.Replace(LineParts[i], string.Empty);
                        if (str.StartsWith(','))
                        {
                            str.Replace(",","");
                        }
                        res = str.Trim().Split(',').ToList();

                        foreach (string item in res)
                        {
                            Result.Add(item);
                        }
                       
                    }
                }
            }

            return Result;
        }
        public MethodResult AddKeyValuePair(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string dictionary = string.Empty; string key = string.Empty; string value = string.Empty;

            try
            {
                dictionary = stp.getPropertiesValue(StringHelper.dictionary, step);

                key = stp.getPropertiesValue(StringHelper.key, step);
                value = stp.getPropertiesValue(StringHelper.value, step);

                DVariables inp = vh.getVariables(dictionary, executionID);
                DataTable dt = new DataTable();
                dt.Columns.Add("Key"); dt.Columns.Add("Value");

                if (vh.checkVariableExists(key, executionID) && vh.checkVariableExists(value, executionID))
                {
                    DataTable ykey = new TypeHandler().getInput(key, executionID);
                    DataTable yvalue = new TypeHandler().getInput(value, executionID);

                    dt.Rows.Add(ykey.Rows[0][0], yvalue.Rows[0][0]);
                }
                else if (vh.checkVariableExists(key, executionID) && !vh.checkVariableExists(value, executionID))
                {
                    DataTable ykey = new TypeHandler().getInput(key, executionID);

                    dt.Rows.Add(ykey.Rows[0][0], value);
                }
                else if (!vh.checkVariableExists(key, executionID) && vh.checkVariableExists(value, executionID))
                {
                    DataTable yvalue = new TypeHandler().getInput(value, executionID);

                    dt.Rows.Add(key, yvalue.Rows[0][0]);
                }
                else
                {
                    dt.Rows.Add(key, value);
                }

                inp.vlvalue = JsonConvert.SerializeObject(dt);
                inp.vltype = "datatable";
                inp.RobotID = step.Robot_Id;

                returnValue = dt;
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        //public MethodResult GetKeyValuePair(Steps step, string executionID)
        //{
        //    string ret = string.Empty; DataTable returnValue = new DataTable();
        //    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

        //    string dictionary = string.Empty; string value = string.Empty; string key = string.Empty;

        //    try
        //    {
        //        dictionary = stp.getPropertiesValue(StringHelper.dictionary, step);
        //        value = stp.getPropertiesValue(StringHelper.value, step);
        //        key = stp.getPropertiesValue(StringHelper.key, step);

        //        DataTable dt = new DataTable();
        //        dt.Columns.Add("Value");
        //        string xkey = string.Empty;
        //        string xvalue = string.Empty;

        //        if (vh.checkVariableExists(key, executionID))
        //        {
        //            DVariables ykey = vh.getVariables(key, executionID);
        //            xkey = JsonConvert.DeserializeObject<DataTable>(ykey.vlvalue).Rows[0][0];
        //        }
        //        else
        //        {
        //            xkey = key;
        //        }

        //        DataTable inpdt = new TypeHandler().getInput(dictionary, executionID);

        //        foreach (DataRow drinp in inpdt.Rows)
        //        {
        //            try
        //            {
        //                if (drinp[0].ToString().Trim() == xkey.Trim())
        //                {
        //                    xvalue = drinp[1].ToString();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                new Logger().LogException(ex);
        //            }
        //        }

        //        returnValue.Columns.Add("Value");
        //        returnValue.Rows.Add(xvalue);
        //        ret = StringHelper.success;
        //    }
        //    catch (Exception ex)
        //    {
        //        new Logger().LogException(ex);
        //        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
        //        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
        //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

        //    }
        //    MethodResult mr;
        //    if (ret == StringHelper.success)
        //    {
        //        new VariableHandler().CreateVariable(value, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
        //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //        s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
        //        mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
        //    }
        //    else
        //    {
        //        mr = new MethodResultHandler().createResultType(false, msgs, null, s);
        //    }
        //    return mr;
        //}

        public MethodResult TWODColumnChange(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();

            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string source = string.Empty; string destination = string.Empty; string input = string.Empty; string output = string.Empty;

            try
            {
                source = stp.getPropertiesValue(StringHelper.Source, step);
                destination = stp.getPropertiesValue(StringHelper.Destination, step);
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);

                List<string> sourcecols = new List<string>();
                List<string> destcols = new List<string>();

                if (source.IndexOf(":") > -1)
                {
                    sourcecols = source.Split(new string[] { ":" }, StringSplitOptions.None).ToList();
                }
                else
                {
                    sourcecols.Add(source);
                }

                if (destination.IndexOf(":") > -1)
                {
                    destcols = destination.Split(new string[] { ":" }, StringSplitOptions.None).ToList();
                }
                else
                {
                    destcols.Add(destination);
                }
                sourcecols.RemoveAll(sc => sc.Trim() == string.Empty);
                destcols.RemoveAll(sc => sc.Trim() == string.Empty);

                if (sourcecols.Count != destcols.Count)
                    ret = StringHelper.failure;
                else
                {
                    destcols.ForEach(dc => returnValue.Columns.Add(dc));
                    if (vh.checkVariableExists(input, executionID))
                    {
                        DataTable dt = new TypeHandler().getInput(input, executionID);
                        //returnValue.Load(dt.CreateDataReader());


                        //var result = string.Join(",", sourcecols.ToArray());
                        //DataView view = new DataView(dt);
                        //DataTable table2 = view.ToTable(false, result);
                        foreach (DataRow dr in dt.Rows)
                        {
                            List<string> row = new List<string>();
                            if (sourcecols[0] != dr[sourcecols[0]].ToString())
                            {
                                foreach (string sourcecol in sourcecols)
                                {
                                    row.Add(dr[sourcecol].ToString());
                                }
                                returnValue.Rows.Add(row.ToArray());
                            }
                        }

                        ret = StringHelper.success;
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;

        }
        public DataTable RemoveDuplicateRows(DataTable table, string DistinctColumn)
        {
            try
            {
                ArrayList UniqueRecords = new ArrayList();
                ArrayList DuplicateRecords = new ArrayList();
                foreach (DataRow dRow in table.Rows)
                {
                    if (UniqueRecords.Contains(dRow[DistinctColumn]))
                        DuplicateRecords.Add(dRow);
                    else
                        UniqueRecords.Add(dRow[DistinctColumn]);
                }

                // Remove duplicate rows from DataTable added to DuplicateRecords
                foreach (DataRow dRow in DuplicateRecords)
                {
                    table.Rows.Remove(dRow);
                }

                // Return the clean DataTable which contains unique records.
                return table;
            }
            catch (Exception ex)
            {
                string ys = ex.Message;
                return null;
            }
        }
        public MethodResult RemoveDuplicates(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string input = string.Empty; string output = string.Empty; string Column = string.Empty;
            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                Column = stp.getPropertiesValue(StringHelper.Column, step);
                DataTable dt = new TypeHandler().getInput(input, executionID);
                //DataView dv = new DataView(dt);
                //DataTable newdt = dv.ToTable(true);
                //returnValue = newdt.DefaultView.ToTable(true);
                returnValue = RemoveDuplicateRows(dt, Column);

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult PatternExtract(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string input = string.Empty; string output = string.Empty;
            string patterns = string.Empty;
            string original = string.Empty; string replace = string.Empty;

            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                patterns = stp.getPropertiesValue(StringHelper.pattern, step);
                original = stp.getPropertiesValue(StringHelper.OriginalText, step);
                replace = stp.getPropertiesValue(StringHelper.ReplaceText, step);

                returnValue.Columns.Add(StringHelper.Content);

                List<Match> matches = new List<Match>();

                if (patterns.Contains(";"))
                {
                    foreach (string pattern in patterns.Split(';').ToList())
                    {
                        if (vh.checkVariableExists(input, executionID))
                        {
                            DVariables dv = vh.getVariables(input, executionID);
                            DataTable inp = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                            foreach (DataRow drinp in inp.Rows)
                            {
                                MatchCollection mc = Regex.Matches(drinp[StringHelper.Content].ToString(), pattern);
                                foreach (Match m in mc)
                                {
                                    matches.Add(m);
                                }

                            }
                        }
                        else
                        {
                            MatchCollection mc = Regex.Matches(input, pattern);
                            foreach (Match m in mc)
                            {
                                matches.Add(m);
                            }
                        }
                    }
                }
                else
                {
                    if (vh.checkVariableExists(input, executionID))
                    {
                        DVariables dv = vh.getVariables(input, executionID);
                        DataTable inp = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                        foreach (DataRow drinp in inp.Rows)
                        {
                            MatchCollection mc = Regex.Matches(drinp[StringHelper.Content].ToString(), patterns);
                            foreach (Match m in mc)
                            {
                                matches.Add(m);
                            }
                        }
                    }
                    else
                    {
                        MatchCollection mc = Regex.Matches(input, patterns);
                        foreach (Match m in mc)
                        {
                            matches.Add(m);
                        }
                    }
                }

                if (replace.Trim() != string.Empty && original.Trim() != string.Empty)
                {
                    char result;
                    if (char.TryParse(replace, out result))
                    {
                        foreach (Match match in matches)
                        {
                            string[] s2 = match.ToString().Split(':');
                            char[] characters = s2[1].ToCharArray();
                            for (int i = 0; i < characters.Length; i++)
                            {
                                if (characters[i] == characters[1])
                                {
                                    characters[1] = 'S';
                                }
                            }

                            returnValue.Rows.Add(new string(characters).Trim());
                        }
                    }
                    else
                    {
                        matches.ForEach(m => m.ToString().Replace(original, replace));
                        matches.ForEach(m => returnValue.Rows.Add(m.ToString()));
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult TwoDRowMatch(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string input = string.Empty; string output = string.Empty;
            string match = string.Empty; string column = string.Empty;

            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                match = stp.getPropertiesValue(StringHelper.match, step);
                column = stp.getPropertiesValue(StringHelper.Column, step);

                if (input.Trim() != string.Empty && vh.checkVariableExists(input, executionID))
                {
                    DVariables dv = vh.getVariables(input, executionID);
                    DataTable inp = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);

                    foreach (DataColumn col in inp.Columns)
                    {
                        returnValue.Columns.Add(col.ColumnName);
                    }

                    foreach (DataRow drinp in inp.Rows)
                    {
                        if (column.Trim() != string.Empty)
                        {
                            if (column.Contains(";")) //many columns
                            {
                                List<string> columnitems = column.Split(';').ToList();
                                columnitems.RemoveAll(m => m.Trim() == string.Empty);

                                foreach (string columns in columnitems)
                                {
                                    int intcolumn = 0;
                                    if (int.TryParse(columns, out intcolumn)) //indexer
                                    {
                                        if (match.Contains(";")) // many matches
                                        {
                                            List<string> matchitems = match.Split(';').ToList();
                                            matchitems.RemoveAll(m => m.Trim() == string.Empty);

                                            foreach (string matches in matchitems)
                                            {
                                                string pattern = string.Empty;

                                                if (vh.checkVariableExists(matches, executionID))
                                                {
                                                    DataTable dt = new TypeHandler().getInput(matches, executionID);
                                                    pattern = dt.Rows[0][0].ToString();
                                                }
                                                else
                                                {
                                                    pattern = matches;
                                                }

                                                if (drinp[intcolumn].ToString().IndexOf(pattern) > -1)
                                                {
                                                    var tr = drinp.ItemArray;
                                                    returnValue.Rows.Add(tr);
                                                }
                                            }
                                        }
                                        else //single match
                                        {
                                            string pattern = string.Empty;

                                            if (vh.checkVariableExists(match, executionID))
                                            {
                                                DataTable dt = new TypeHandler().getInput(match, executionID);
                                                pattern = dt.Rows[0][0].ToString();
                                            }
                                            else
                                            {
                                                pattern = match;
                                            }

                                            if (drinp[intcolumn].ToString().IndexOf(pattern) > -1)
                                            {
                                                var tr = drinp.ItemArray;
                                                returnValue.Rows.Add(tr);
                                            }
                                        }
                                    }
                                    else //column name
                                    {
                                        if (match.Contains(";")) // many matches
                                        {
                                            List<string> matchitems = match.Split(';').ToList();
                                            matchitems.RemoveAll(m => m.Trim() == string.Empty);

                                            foreach (string matches in matchitems)
                                            {
                                                string pattern = string.Empty;

                                                if (vh.checkVariableExists(matches, executionID))
                                                {
                                                    DataTable dt = new TypeHandler().getInput(matches, executionID);
                                                    pattern = dt.Rows[0][0].ToString();
                                                }
                                                else
                                                {
                                                    pattern = matches;
                                                }

                                                if (drinp[columns].ToString().IndexOf(pattern) > -1)
                                                {
                                                    var tr = drinp.ItemArray;
                                                    returnValue.Rows.Add(tr);
                                                }
                                            }
                                        }
                                        else //single match
                                        {
                                            string pattern = string.Empty;

                                            if (vh.checkVariableExists(match, executionID))
                                            {
                                                DataTable dt = new TypeHandler().getInput(match, executionID);
                                                pattern = dt.Rows[0][0].ToString();
                                            }
                                            else
                                            {
                                                pattern = match;
                                            }

                                            //if (drinp[columns].ToString().IndexOf(pattern) > -1)
                                            if (drinp[columns].ToString().Contains(pattern))
                                            {
                                                var tr = drinp.ItemArray;
                                                returnValue.Rows.Add(tr);
                                            }
                                        }
                                    }
                                }
                            }
                            else //single column
                            {
                                int intcolumn = 0;
                                if (int.TryParse(column, out intcolumn)) //indexer
                                {
                                    if (match.Contains(";")) // many matches
                                    {
                                        List<string> matchitems = match.Split(';').ToList();
                                        matchitems.RemoveAll(m => m.Trim() == string.Empty);

                                        foreach (string matches in matchitems)
                                        {
                                            string pattern = string.Empty;

                                            if (vh.checkVariableExists(matches, executionID))
                                            {
                                                DataTable dt = new TypeHandler().getInput(matches, executionID);
                                                pattern = dt.Rows[0][0].ToString();
                                            }
                                            else
                                            {
                                                pattern = matches;
                                            }

                                            if (drinp[intcolumn].ToString().IndexOf(pattern) > -1)
                                            {
                                                var tr = drinp.ItemArray;
                                                returnValue.Rows.Add(tr);
                                            }

                                        }
                                    }
                                    else //single match
                                    {
                                        string pattern = string.Empty;

                                        if (vh.checkVariableExists(match, executionID))
                                        {
                                            DataTable dt = new TypeHandler().getInput(match, executionID);
                                            pattern = dt.Rows[0][0].ToString();
                                        }
                                        else
                                        {
                                            pattern = match;
                                        }

                                        if (drinp[intcolumn].ToString().IndexOf(pattern) > -1)
                                        {
                                            var tr = drinp.ItemArray;
                                            returnValue.Rows.Add(tr);
                                        }

                                    }
                                }
                                else
                                {
                                    if (match.Contains(";")) // many matches
                                    {
                                        List<string> matchitems = match.Split(';').ToList();
                                        matchitems.RemoveAll(m => m.Trim() == string.Empty);

                                        foreach (string matches in matchitems)
                                        {
                                            string pattern = string.Empty;

                                            if (vh.checkVariableExists(matches, executionID))
                                            {
                                                DataTable dt = new TypeHandler().getInput(matches, executionID);
                                                pattern = dt.Rows[0][0].ToString();
                                            }
                                            else
                                            {
                                                pattern = matches;
                                            }

                                            //if (drinp[column].ToString().IndexOf(pattern) > -1)
                                            if (drinp[column].ToString().IndexOf(pattern) > -1)
                                            {
                                                var tr = drinp.ItemArray;
                                                returnValue.Rows.Add(tr);
                                            }
                                        }
                                    }
                                    else //single match
                                    {
                                        string pattern = string.Empty;

                                        if (vh.checkVariableExists(match, executionID))
                                        {
                                            DataTable dt = new TypeHandler().getInput(match, executionID);
                                            pattern = dt.Rows[0][0].ToString();
                                        }
                                        else
                                        {
                                            pattern = match;
                                        }

                                        //if (drinp[column].ToString().IndexOf(pattern) > -1)
                                        if (drinp[column].ToString().All(pattern.Contains))
                                        {
                                            var tr = drinp.ItemArray;
                                            returnValue.Rows.Add(tr);
                                        }


                                    }
                                }
                            }
                        }
                        else //NO COLUMN VALUE GIVEN
                        {
                            if (match.Contains(";")) // many matches
                            {
                                foreach (DataColumn dc in inp.Columns)
                                {
                                    List<string> matchitems = match.Split(';').ToList();
                                    matchitems.RemoveAll(m => m.Trim() == string.Empty);

                                    foreach (string matches in matchitems)
                                    {
                                        string pattern = string.Empty;

                                        if (vh.checkVariableExists(matches, executionID))
                                        {
                                            DataTable dt = new TypeHandler().getInput(matches, executionID);
                                            pattern = dt.Rows[0][0].ToString();
                                        }
                                        else
                                        {
                                            pattern = matches;
                                        }
                                        string strName = drinp[dc].ToString().Replace(",", "");
                                        //if (drinp[dc].ToString().IndexOf(matches) > -1)
                                        if (strName.Contains(pattern))
                                        {
                                            var tr = drinp.ItemArray;
                                            returnValue.Rows.Add(tr);
                                        }
                                    }
                                }
                            }
                            else //single match
                            {
                                foreach (DataColumn dc in inp.Columns)
                                {
                                    string pattern = string.Empty;

                                    if (vh.checkVariableExists(match, executionID))
                                    {
                                        DataTable dt = new TypeHandler().getInput(match, executionID);
                                        pattern = dt.Rows[0][0].ToString();
                                    }
                                    else
                                    {
                                        pattern = match;
                                    }
                                    if (drinp[dc].ToString().IndexOf(pattern) > -1)
                                    {
                                        var tr = drinp.ItemArray;
                                        returnValue.Rows.Add(tr);
                                    }
                                }
                            }
                        }
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult GetCount(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string input = string.Empty; string output = string.Empty;

            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);

                returnValue.Columns.Add(StringHelper.count);

                if (input.Trim() != string.Empty && vh.checkVariableExists(input, executionID))
                {
                    DVariables dv = vh.getVariables(input, executionID);
                    DataTable inp = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);

                    returnValue.Rows.Add(inp.Rows.Count.ToString());
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult WordsCount(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string input = string.Empty; string output = string.Empty; string Word = string.Empty;
            int count = 0; bool wasInWord = false; bool inWord = false;
            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                returnValue.Columns.Add(StringHelper.count);
                if (input.Trim() != string.Empty && vh.checkVariableExists(input, executionID))
                {
                    DVariables dv = vh.getVariables(input, executionID);
                    DataTable inp = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                    Word = inp.Rows[0][0].ToString();
                    for (int i = 0; i < Word.Length; i++)
                    {
                        if (inWord)
                        {
                            wasInWord = true;
                        }

                        if (Char.IsWhiteSpace(Word[i]))
                        {
                            if (wasInWord)
                            {
                                count++;
                                wasInWord = false;
                            }
                            inWord = false;
                        }
                        else
                        {
                            inWord = true;
                        }
                    }

                    // Check to see if we got out with seeing a word
                    if (wasInWord)
                    {
                        count++;
                    }
                    returnValue.Rows.Add(count);

                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult Switch(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            DataTable returnValue = new DataTable();
            string output = string.Empty;

            try
            {
                string ComparisonType = stp.getPropertiesValue(StringHelper.comparisontype, step);
                string Condition = stp.getPropertiesValue(StringHelper.condition, step);
                string SwitchValue = stp.getPropertiesValue(StringHelper.switchvalue, step);
                string CaseValues = stp.getPropertiesValue(StringHelper.casevalues, step);
                string DefaultValue = stp.getPropertiesValue(StringHelper.defaultvalue, step);
                output = stp.getPropertiesValue(StringHelper.output, step);

                List<KeyValuePair<string, List<string>>> cases = new List<KeyValuePair<string, List<string>>>();
                List<KeyValuePair<string, List<string>>> defaultcases = new List<KeyValuePair<string, List<string>>>();

                if (vh.checkVariableExists(CaseValues, executionID))
                {
                    DVariables dv = vh.getVariables(CaseValues, executionID);
                    DataTable inprt = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);

                    string duplicat_casevalues = string.Empty;
                    foreach (DataRow inpr in inprt.Rows)
                    {
                        duplicat_casevalues = duplicat_casevalues + string.Join(":", inpr.ItemArray) + ";";
                    }

                    CaseValues = duplicat_casevalues;
                }

                if (vh.checkVariableExists(DefaultValue, executionID))
                {
                    DVariables dv = vh.getVariables(DefaultValue, executionID);
                    DataTable inprt = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);

                    string duplicat_defaultvalues = string.Empty;
                    foreach (DataRow inpr in inprt.Rows)
                    {
                        duplicat_defaultvalues = duplicat_defaultvalues + string.Join(":", inpr.ItemArray) + ";";
                    }

                    DefaultValue = duplicat_defaultvalues;
                }

                if (CaseValues.IndexOf(";") > -1)
                {
                    List<string> longlines = CaseValues.Split(';').ToList();
                    longlines.RemoveAll(ll => ll.Trim() == string.Empty);

                    foreach (string longline in longlines)
                    {
                        List<string> shortlines = longline.Split(':').ToList();
                        shortlines.RemoveAll(ll => ll.Trim() == string.Empty);
                        if (shortlines[0] != string.Empty)
                        {
                            string key = shortlines[0];
                            List<string> values = shortlines;
                            if (key == values.ElementAt(0))
                            {
                                values.RemoveAt(0);
                                cases.Add(new KeyValuePair<string, List<string>>(key, values));
                            }
                        }
                    }
                }
                else
                {
                    List<string> shortlines = CaseValues.Split(':').ToList();
                    shortlines.RemoveAll(ll => ll.Trim() == string.Empty);
                    if (shortlines[0] != string.Empty)
                    {
                        string key = shortlines[0];
                        List<string> values = shortlines;
                        if (key == values.ElementAt(0))
                        {
                            values.RemoveAt(0);
                            cases.Add(new KeyValuePair<string, List<string>>(key, values));
                        }
                    }
                }


                if (DefaultValue.IndexOf(";") > -1)
                {
                    List<string> longlines = DefaultValue.Split(';').ToList();
                    longlines.RemoveAll(ll => ll.Trim() == string.Empty);

                    foreach (string longline in longlines)
                    {
                        List<string> shortlines = longline.Split(':').ToList();
                        shortlines.RemoveAll(ll => ll.Trim() == string.Empty);
                        if (shortlines[0] != string.Empty)
                        {
                            string key = shortlines[0];
                            List<string> values = shortlines;
                            if (key == values.ElementAt(0))
                            {
                                values.RemoveAt(0);
                                defaultcases.Add(new KeyValuePair<string, List<string>>(key, values));
                            }
                        }
                    }
                }
                else
                {
                    List<string> shortlines = DefaultValue.Split(':').ToList();
                    shortlines.RemoveAll(ll => ll.Trim() == string.Empty);
                    if (shortlines[0] != string.Empty)
                    {
                        string key = shortlines[0];
                        List<string> values = shortlines;
                        if (key == values.ElementAt(0))
                        {
                            values.RemoveAt(0);
                            defaultcases.Add(new KeyValuePair<string, List<string>>(key, values));
                        }
                    }
                }

                string switcher = string.Empty;

                if (SwitchValue.Trim() != string.Empty && vh.checkVariableExists(SwitchValue, executionID))
                {
                    DVariables dv = vh.getVariables(SwitchValue, executionID);
                    DataTable inp = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);

                    switcher = inp.Rows[0][0].ToString();
                }
                else //value
                {
                    switcher = SwitchValue;
                }

                int cnt_case = 0;
                if (cases.Count > 0) //cases there
                {
                    foreach (var icase in cases)
                    {
                        if (switcher.Trim() == icase.Key.Trim())
                        {
                            icase.Value.ForEach(v => returnValue.Columns.Add());
                            returnValue.Rows.Add(icase.Value.ToArray());
                            cnt_case += 1;
                            break;
                        }
                    }
                }

                //else
                //{
                //    if (defaultcases.Count > 0) //only default cases there
                //    {
                //        foreach (var defaultcase in defaultcases)
                //        {
                //            defaultcase.Value.ForEach(v => returnValue.Columns.Add());
                //            returnValue.Rows.Add(defaultcase.Value.ToArray());
                //            break;
                //        }
                //    }
                //}

                if (cnt_case == 0)
                {
                    if (defaultcases.Count > 0) //only default cases there
                    {
                        foreach (var defaultcase in defaultcases)
                        {
                            defaultcase.Value.ForEach(v => returnValue.Columns.Add());
                            returnValue.Rows.Add(defaultcase.Value.ToArray());
                            break;
                        }
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        //public MethodResult TwoDCompare(Steps step, string executionID)
        //{
        //    string ret = string.Empty; DataTable returnValue = new DataTable();
        //    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
        //    string LeftSource = string.Empty; string RightSource = string.Empty;
        //    string LeftSourceCompareColumn = string.Empty; string RightSourceCompareColumn = string.Empty;
        //    string ComparisonType = string.Empty;
        //    string LeftSourceResultColumn = string.Empty; string RightSourceResultColumn = string.Empty;
        //    string Result = string.Empty;

        //    try
        //    {
        //        LeftSource = stp.getPropertiesValue("LeftSource", step);
        //        RightSource = stp.getPropertiesValue("RightSource", step);
        //        LeftSourceCompareColumn = stp.getPropertiesValue("LeftSourceCompareColumn", step);
        //        RightSourceCompareColumn = stp.getPropertiesValue("RightSourceCompareColumn", step);
        //        ComparisonType = stp.getPropertiesValue(StringHelper.comparisontype, step);
        //        LeftSourceResultColumn = stp.getPropertiesValue("LeftSourceResultColumn", step);
        //        RightSourceResultColumn = stp.getPropertiesValue("RightSourceResultColumn", step);
        //        Result = stp.getPropertiesValue("Result", step);

        //        DataTable LSource = new DataTable();
        //        DataTable RSource = new DataTable();

        //        DataTable LDSource = new DataTable();
        //        DataTable RDSource = new DataTable();

        //        if (vh.checkVariableExists(LeftSource,executionID))
        //        {
        //            DVariables dv = vh.getVariables(LeftSource, executionID);
        //            LSource = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
        //        }
        //        else
        //        {
        //            LSource = new TypeHandler().getInput(LeftSource, StringHelper.value, executionID);
        //        }

        //        if (vh.checkVariableExists(RightSource, executionID))
        //        {
        //            DVariables dv = vh.getVariables(RightSource, executionID);
        //            RSource = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
        //        }
        //        else
        //        {
        //            RSource = new TypeHandler().getInput(RightSource, StringHelper.value, executionID);
        //        }

        //        List<string> LeftSourceCompareColumnList = new List<string>();
        //        if (LeftSourceCompareColumn.Contains(";"))
        //        {
        //            LeftSourceCompareColumnList.AddRange(LeftSourceCompareColumn.Split(';').ToList());
        //        }
        //        else
        //        {
        //            LeftSourceCompareColumnList.Add(LeftSourceCompareColumn);
        //        }
        //        List<string> RightSourceCompareColumnList = new List<string>();
        //        if (RightSourceCompareColumn.Contains(";"))
        //        {
        //            RightSourceCompareColumnList.AddRange(RightSourceCompareColumn.Split(';').ToList());
        //        }
        //        else
        //        {
        //            RightSourceCompareColumnList.Add(RightSourceCompareColumn);
        //        }
        //        List<string> LeftSourceResultColumnList = new List<string>();
        //        if (LeftSourceResultColumn.Contains(";"))
        //        {
        //            LeftSourceResultColumnList.AddRange(LeftSourceResultColumn.Split(';').ToList());
        //        }
        //        else
        //        {
        //            LeftSourceResultColumnList.Add(LeftSourceResultColumn);
        //        }
        //        List<string> RightSourceResultColumnList = new List<string>();
        //        if (RightSourceResultColumn.Contains(";"))
        //        {
        //            RightSourceResultColumnList.AddRange(RightSourceResultColumn.Split(';').ToList());
        //        }
        //        else
        //        {
        //            RightSourceResultColumnList.Add(RightSourceResultColumn);
        //        }

        //        if (LSource != null && LSource.Rows.Count > 0) //left source not null and has values
        //        {
        //            if (RSource != null && RSource.Rows.Count > 0) //right source not null and has values
        //            {
        //                if (LSource.Columns.Count == RSource.Columns.Count) //check column count equal for both left and right sources
        //                {
        //                    int isEqual = 0;int isInEqual = 0;
        //                    foreach(DataColumn lDC in LSource.Columns)
        //                    {
        //                        foreach (DataColumn rDC in RSource.Columns)
        //                        {
        //                            if(lDC.ColumnName.Trim().ToLower() == rDC.ColumnName.Trim().ToLower()) //check column names are equal on both datatables
        //                            {
        //                                isEqual++;
        //                            }
        //                            else
        //                            {
        //                                isInEqual++;
        //                            }
        //                        }
        //                    }

        //                    if(isEqual == LSource.Columns.Count) //if equal number of datacolumns count 
        //                    {
        //                        for(int dc = 0; dc < LSource.Columns.Count; dc++)
        //                        {
        //                            LDSource.Columns.Add(RSource.Columns[dc].ColumnName);
        //                        }
        //                        for (int dc = 0; dc < RSource.Columns.Count; dc++)
        //                        {
        //                            RDSource.Columns.Add(RSource.Columns[dc].ColumnName);
        //                        }
        //                        if ((LeftSourceCompareColumnList.Count == 1 && LeftSourceCompareColumnList[0].Trim() == "*") && (RightSourceCompareColumnList.Count == 1 && RightSourceCompareColumnList[0].Trim() == "*")) //compare all cells
        //                        {
        //                            for (int i = 0; i < LSource.Rows.Count; i++)
        //                            {
        //                                for (int j = 0; j < LSource.Columns.Count; j++)
        //                                {
        //                                    if (LSource.Rows[i][j].ToString() == RSource.Rows[i][j].ToString())
        //                                    {
        //                                        var l = LSource.Rows[i].ItemArray.ToList();
        //                                        var r = RSource.Rows[i].ItemArray.ToList();

        //                                        if(LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                        {

        //                                        }
        //                                        else
        //                                        {
        //                                            LDSource.Rows.Add(l);
        //                                            RDSource.Rows.Add(r);
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        else if ((LeftSourceCompareColumnList.Count == 1 && LeftSourceCompareColumnList[0].Trim() == "*") && (RightSourceCompareColumnList.Count > 1)) //compare all cells
        //                        {
        //                            for (int i = 0; i < LSource.Rows.Count; i++)
        //                            {
        //                                foreach (string rsccl in RightSourceCompareColumnList)
        //                                {
        //                                    for (int j = 0; j < LSource.Columns.Count; j++)
        //                                    {
        //                                        if (RSource.Columns.Contains(rsccl))
        //                                        {
        //                                            if (LSource.Rows[i][j].ToString() == RSource.Rows[i][rsccl].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            if (LSource.Rows[i][Convert.ToInt32(j)].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                }

        //                            }
        //                        }
        //                        else if ((LeftSourceCompareColumnList.Count > 1) && (RightSourceCompareColumnList.Count == 1 && RightSourceCompareColumnList[0].Trim() == "*")) //compare all cells
        //                        {
        //                            for (int i = 0; i < RSource.Rows.Count; i++)
        //                            {
        //                                foreach (string lsccl in LeftSourceCompareColumnList)
        //                                {
        //                                    for (int j = 0; j < RSource.Columns.Count; j++)
        //                                    {
        //                                        if (LSource.Columns.Contains(lsccl))
        //                                        {
        //                                            if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][j].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][Convert.ToInt32(j)].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        else //compare only selected cells
        //                        {
        //                            for (int i = 0; i < LSource.Rows.Count; i++)
        //                            {
        //                                foreach(string lsccl in LeftSourceCompareColumnList)
        //                                {
        //                                    foreach (string rsccl in RightSourceCompareColumnList)
        //                                    {

        //                                        if (LSource.Columns.Contains(lsccl) && RSource.Columns.Contains(rsccl))
        //                                        {
        //                                            if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][rsccl].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                        else if(LSource.Columns.Contains(lsccl) && !RSource.Columns.Contains(rsccl))
        //                                        {
        //                                            if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                        else if (!LSource.Columns.Contains(lsccl) && RSource.Columns.Contains(rsccl))
        //                                        {
        //                                            if (LSource.Rows[i][Convert.ToInt32(lsccl)].ToString() == RSource.Rows[i][rsccl].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            if (LSource.Rows[i][Convert.ToInt32(lsccl)].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else //unequal datacolumns count
        //                    {
        //                        for (int dc = 0; dc < LSource.Columns.Count; dc++)
        //                        {
        //                            LDSource.Columns.Add(RSource.Columns[dc].ColumnName);
        //                        }
        //                        for (int dc = 0; dc < RSource.Columns.Count; dc++)
        //                        {
        //                            RDSource.Columns.Add(RSource.Columns[dc].ColumnName);
        //                        }

        //                        if ((LeftSourceCompareColumnList.Count == 1 && LeftSourceCompareColumnList[0].Trim() == "*") && (RightSourceCompareColumnList.Count == 1 && RightSourceCompareColumnList[0].Trim() == "*")) //compare all cells
        //                        {
        //                            for (int i = 0; i < LSource.Rows.Count; i++)
        //                            {
        //                                for (int j = 0; j < LSource.Columns.Count; j++)
        //                                {
        //                                    if (LSource.Rows[i][j].ToString() == RSource.Rows[i][j].ToString())
        //                                    {
        //                                        var l = LSource.Rows[i].ItemArray.ToList();
        //                                        var r = RSource.Rows[i].ItemArray.ToList();

        //                                        if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                        {

        //                                        }
        //                                        else
        //                                        {
        //                                            LDSource.Rows.Add(l);
        //                                            RDSource.Rows.Add(r);
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        else if ((LeftSourceCompareColumnList.Count == 1 && LeftSourceCompareColumnList[0].Trim() == "*") && (RightSourceCompareColumnList.Count > 1)) //compare all cells
        //                        {
        //                            for (int i = 0; i < LSource.Rows.Count; i++)
        //                            {
        //                                foreach (string rsccl in RightSourceCompareColumnList)
        //                                {
        //                                    for (int j = 0; j < LSource.Columns.Count; j++)
        //                                    {
        //                                        if (RSource.Columns.Contains(rsccl))
        //                                        {
        //                                            if (LSource.Rows[i][j].ToString() == RSource.Rows[i][rsccl].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            if (LSource.Rows[i][Convert.ToInt32(j)].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                }

        //                            }
        //                        }
        //                        else if ((LeftSourceCompareColumnList.Count > 1) && (RightSourceCompareColumnList.Count == 1 && RightSourceCompareColumnList[0].Trim() == "*")) //compare all cells
        //                        {
        //                            for (int i = 0; i < RSource.Rows.Count; i++)
        //                            {
        //                                foreach (string lsccl in LeftSourceCompareColumnList)
        //                                {
        //                                    for (int j = 0; j < RSource.Columns.Count; j++)
        //                                    {
        //                                        if (LSource.Columns.Contains(lsccl))
        //                                        {
        //                                            if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][j].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][Convert.ToInt32(j)].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        else //compare only selected cells
        //                        {
        //                            for (int i = 0; i < LSource.Rows.Count; i++)
        //                            {
        //                                foreach (string lsccl in LeftSourceCompareColumnList)
        //                                {
        //                                    foreach (string rsccl in RightSourceCompareColumnList)
        //                                    {

        //                                        if (LSource.Columns.Contains(lsccl) && RSource.Columns.Contains(rsccl))
        //                                        {
        //                                            if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][rsccl].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                        else if (LSource.Columns.Contains(lsccl) && !RSource.Columns.Contains(rsccl))
        //                                        {
        //                                            if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                        else if (!LSource.Columns.Contains(lsccl) && RSource.Columns.Contains(rsccl))
        //                                        {
        //                                            if (LSource.Rows[i][Convert.ToInt32(lsccl)].ToString() == RSource.Rows[i][rsccl].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            if (LSource.Rows[i][Convert.ToInt32(lsccl)].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
        //                                            {
        //                                                var l = LSource.Rows[i].ItemArray.ToList();
        //                                                var r = RSource.Rows[i].ItemArray.ToList();

        //                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                                {

        //                                                }
        //                                                else
        //                                                {
        //                                                    LDSource.Rows.Add(l);
        //                                                    RDSource.Rows.Add(r);
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }                         
        //                }
        //                else //even uneven count similar columns compare
        //                {
        //                    for (int dc = 0; dc < LSource.Columns.Count; dc++)
        //                    {
        //                        LDSource.Columns.Add(RSource.Columns[dc].ColumnName);
        //                    }
        //                    for (int dc = 0; dc < RSource.Columns.Count; dc++)
        //                    {
        //                        RDSource.Columns.Add(RSource.Columns[dc].ColumnName);
        //                    }
        //                    if ((LeftSourceCompareColumnList.Count == 1 && LeftSourceCompareColumnList[0].Trim() == "*") && (RightSourceCompareColumnList.Count == 1 && RightSourceCompareColumnList[0].Trim() == "*")) //compare all cells
        //                    {
        //                        for (int i = 0; i < LSource.Rows.Count; i++)
        //                        {
        //                            for (int j = 0; j < LSource.Columns.Count; j++)
        //                            {
        //                                if (LSource.Rows[i][j].ToString() == RSource.Rows[i][j].ToString())
        //                                {
        //                                    var l = LSource.Rows[i].ItemArray.ToList();
        //                                    var r = RSource.Rows[i].ItemArray.ToList();

        //                                    if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                    {

        //                                    }
        //                                    else
        //                                    {
        //                                        LDSource.Rows.Add(l);
        //                                        RDSource.Rows.Add(r);
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if ((LeftSourceCompareColumnList.Count == 1 && LeftSourceCompareColumnList[0].Trim() == "*") && (RightSourceCompareColumnList.Count > 1)) //compare all cells
        //                    {
        //                        for (int i = 0; i < LSource.Rows.Count; i++)
        //                        {
        //                            foreach (string rsccl in RightSourceCompareColumnList)
        //                            {
        //                                for (int j = 0; j < LSource.Columns.Count; j++)
        //                                {
        //                                    if (RSource.Columns.Contains(rsccl))
        //                                    {
        //                                        if (LSource.Rows[i][j].ToString() == RSource.Rows[i][rsccl].ToString())
        //                                        {
        //                                            var l = LSource.Rows[i].ItemArray.ToList();
        //                                            var r = RSource.Rows[i].ItemArray.ToList();

        //                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                            {

        //                                            }
        //                                            else
        //                                            {
        //                                                LDSource.Rows.Add(l);
        //                                                RDSource.Rows.Add(r);
        //                                            }
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        if (LSource.Rows[i][Convert.ToInt32(j)].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
        //                                        {
        //                                            var l = LSource.Rows[i].ItemArray.ToList();
        //                                            var r = RSource.Rows[i].ItemArray.ToList();

        //                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                            {

        //                                            }
        //                                            else
        //                                            {
        //                                                LDSource.Rows.Add(l);
        //                                                RDSource.Rows.Add(r);
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }

        //                        }
        //                    }
        //                    else if ((LeftSourceCompareColumnList.Count > 1) && (RightSourceCompareColumnList.Count == 1 && RightSourceCompareColumnList[0].Trim() == "*")) //compare all cells
        //                    {
        //                        for (int i = 0; i < RSource.Rows.Count; i++)
        //                        {
        //                            foreach (string lsccl in LeftSourceCompareColumnList)
        //                            {
        //                                for (int j = 0; j < RSource.Columns.Count; j++)
        //                                {
        //                                    if (LSource.Columns.Contains(lsccl))
        //                                    {
        //                                        if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][j].ToString())
        //                                        {
        //                                            var l = LSource.Rows[i].ItemArray.ToList();
        //                                            var r = RSource.Rows[i].ItemArray.ToList();

        //                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                            {

        //                                            }
        //                                            else
        //                                            {
        //                                                LDSource.Rows.Add(l);
        //                                                RDSource.Rows.Add(r);
        //                                            }
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][Convert.ToInt32(j)].ToString())
        //                                        {
        //                                            var l = LSource.Rows[i].ItemArray.ToList();
        //                                            var r = RSource.Rows[i].ItemArray.ToList();

        //                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                            {

        //                                            }
        //                                            else
        //                                            {
        //                                                LDSource.Rows.Add(l);
        //                                                RDSource.Rows.Add(r);
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else //compare only selected cells
        //                    {
        //                        for (int i = 0; i < LSource.Rows.Count; i++)
        //                        {
        //                            foreach (string lsccl in LeftSourceCompareColumnList)
        //                            {
        //                                foreach (string rsccl in RightSourceCompareColumnList)
        //                                {

        //                                    if (LSource.Columns.Contains(lsccl) && RSource.Columns.Contains(rsccl))
        //                                    {
        //                                        if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][rsccl].ToString())
        //                                        {
        //                                            var l = LSource.Rows[i].ItemArray.ToList();
        //                                            var r = RSource.Rows[i].ItemArray.ToList();

        //                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                            {

        //                                            }
        //                                            else
        //                                            {
        //                                                LDSource.Rows.Add(l);
        //                                                RDSource.Rows.Add(r);
        //                                            }
        //                                        }
        //                                    }
        //                                    else if (LSource.Columns.Contains(lsccl) && !RSource.Columns.Contains(rsccl))
        //                                    {
        //                                        if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
        //                                        {
        //                                            var l = LSource.Rows[i].ItemArray.ToList();
        //                                            var r = RSource.Rows[i].ItemArray.ToList();

        //                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                            {

        //                                            }
        //                                            else
        //                                            {
        //                                                LDSource.Rows.Add(l);
        //                                                RDSource.Rows.Add(r);
        //                                            }
        //                                        }
        //                                    }
        //                                    else if (!LSource.Columns.Contains(lsccl) && RSource.Columns.Contains(rsccl))
        //                                    {
        //                                        if (LSource.Rows[i][Convert.ToInt32(lsccl)].ToString() == RSource.Rows[i][rsccl].ToString())
        //                                        {
        //                                            var l = LSource.Rows[i].ItemArray.ToList();
        //                                            var r = RSource.Rows[i].ItemArray.ToList();

        //                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                            {

        //                                            }
        //                                            else
        //                                            {
        //                                                LDSource.Rows.Add(l);
        //                                                RDSource.Rows.Add(r);
        //                                            }
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        if (LSource.Rows[i][Convert.ToInt32(lsccl)].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
        //                                        {
        //                                            var l = LSource.Rows[i].ItemArray.ToList();
        //                                            var r = RSource.Rows[i].ItemArray.ToList();

        //                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
        //                                            {

        //                                            }
        //                                            else
        //                                            {
        //                                                LDSource.Rows.Add(l);
        //                                                RDSource.Rows.Add(r);
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }

        //                DataTable dummyData = new DataTable();
        //                dummyData = LDSource.Copy();
        //                dummyData = RDSource.Copy();

        //                if ((LeftSourceResultColumnList.Count() == 1 && LeftSourceResultColumnList[0].Trim() == "*") && (RightSourceResultColumnList.Count() == 1 && RightSourceResultColumnList[0].Trim() == "*"))
        //                {
        //                    returnValue = LDSource.Copy();
        //                    returnValue = RDSource.Copy();
        //                }
        //                else if ((LeftSourceResultColumnList.Count() == 1 && LeftSourceResultColumnList[0].Trim() == "*") && RightSourceResultColumnList.Count() > 1)
        //                {
        //                    DataTable RightFilteredData = dummyData.DefaultView.ToTable(false, RightSourceResultColumnList.ToArray());
        //                    returnValue = LDSource.Copy();
        //                    returnValue = RightFilteredData.Copy();
        //                }
        //                else if (LeftSourceResultColumnList.Count > 1 && (RightSourceResultColumnList.Count() == 1 && RightSourceResultColumnList[0].Trim() == "*"))
        //                {
        //                    DataTable LeftFilteredData = dummyData.DefaultView.ToTable(false, LeftSourceResultColumnList.ToArray());
        //                    returnValue = RDSource.Copy();
        //                    returnValue = LeftFilteredData.Copy();
        //                }
        //                else
        //                {
        //                    DataTable RightFilteredData = dummyData.DefaultView.ToTable(false, RightSourceResultColumnList.ToArray());
        //                    returnValue = LDSource.Copy();
        //                    returnValue = RightFilteredData.Copy();

        //                    DataTable LeftFilteredData = dummyData.DefaultView.ToTable(false, LeftSourceResultColumnList.ToArray());
        //                    returnValue = RDSource.Copy();
        //                    returnValue = LeftFilteredData.Copy();
        //                }

        //                ret = StringHelper.success;
        //            }
        //            else
        //            {

        //            }
        //        }
        //        else
        //        {

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        new Logger().LogException(ex);
        //        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
        //        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
        //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
        //    }
        //    MethodResult mr;
        //    if (ret == StringHelper.success)
        //    {
        //        new VariableHandler().CreateVariable(Result, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
        //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //        s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
        //        mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
        //    }
        //    else
        //    {
        //        mr = new MethodResultHandler().createResultType(false, msgs, null, s);
        //    }
        //    return mr;
        //}

        public MethodResult TwoDCompare(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string LeftSource = string.Empty; string RightSource = string.Empty;
            string LeftSourceCompareColumn = string.Empty; string RightSourceCompareColumn = string.Empty;
            string ComparisonType = string.Empty;
            string LeftSourceResultColumn = string.Empty; string RightSourceResultColumn = string.Empty;
            string Result = string.Empty;


            try
            {
                LeftSource = stp.getPropertiesValue("LeftSource", step);
                RightSource = stp.getPropertiesValue("RightSource", step);
                LeftSourceCompareColumn = stp.getPropertiesValue("LeftSourceCompareColumn", step);
                RightSourceCompareColumn = stp.getPropertiesValue("RightSourceCompareColumn", step);
                ComparisonType = stp.getPropertiesValue(StringHelper.comparisontype, step);
                LeftSourceResultColumn = stp.getPropertiesValue("LeftSourceResultColumn", step);
                RightSourceResultColumn = stp.getPropertiesValue("RightSourceResultColumn", step);
                Result = stp.getPropertiesValue("Result", step);

                DataTable LSource = new DataTable();
                DataTable RSource = new DataTable();

                DataTable LDSource = new DataTable();
                DataTable RDSource = new DataTable();

                int s = 0;
                if (ComparisonType != "")
                {
                    if (ComparisonType.ToLower() == "excel")
                    {
                        Excel _objExcel = new Excel();
                        s++;

                        if (vh.checkVariableExists(LeftSource, executionID))
                        {
                            DVariables dv = vh.getVariables(LeftSource, executionID);
                            // LSource = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);

                            ///////////                           


                            DataTable dtr = new DataTable();
                            DataTable dt = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                            List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                            //Excel _objExcel = new Excel();

                            foreach (string file in files)
                            {
                                byte[] fbytes = System.IO.File.ReadAllBytes(file);
                                dtr.Merge(_objExcel.ExcelToDataTable(fbytes, true));

                            }

                            LSource = dtr.Copy();



                            ///////////
                        }
                        else
                        {
                            LSource = new TypeHandler().getInput(LeftSource, StringHelper.value, executionID);
                        }

                        if (vh.checkVariableExists(RightSource, executionID))
                        {
                            DVariables dv = vh.getVariables(RightSource, executionID);
                            // RSource = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);

                            ///////////                           


                            DataTable dtr = new DataTable();
                            DataTable dt = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                            List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();


                            foreach (string file in files)
                            {
                                byte[] fbytes = System.IO.File.ReadAllBytes(file);
                                dtr.Merge(_objExcel.ExcelToDataTable(fbytes, true));

                            }

                            RSource = dtr.Copy();
                        }
                        else
                        {
                            RSource = new TypeHandler().getInput(RightSource, StringHelper.value, executionID);
                        }


                        var Join =
                from PX in LSource.AsEnumerable()
                join PY in RSource.AsEnumerable()
                on PX["Ref. Doc."]
                equals PY["Sales Doc."]
                select new { PX, PY };

                        if (vh.checkVariableExists(LeftSource, executionID))
                        {
                            DVariables dv = vh.getVariables(LeftSource, executionID);
                            LSource = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                        }
                        else
                        {
                            LSource = new TypeHandler().getInput(LeftSource, StringHelper.value, executionID);
                        }

                        if (vh.checkVariableExists(RightSource, executionID))
                        {
                            DVariables dv = vh.getVariables(RightSource, executionID);
                            RSource = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                        }
                        else
                        {
                            RSource = new TypeHandler().getInput(RightSource, StringHelper.value, executionID);
                        }

                        List<string> LeftSourceCompareColumnList = new List<string>();
                        if (LeftSourceCompareColumn.Contains(";"))
                        {
                            LeftSourceCompareColumnList.AddRange(LeftSourceCompareColumn.Split(';').ToList());
                        }
                        else
                        {
                            LeftSourceCompareColumnList.Add(LeftSourceCompareColumn);
                        }
                        List<string> RightSourceCompareColumnList = new List<string>();
                        if (RightSourceCompareColumn.Contains(";"))
                        {
                            RightSourceCompareColumnList.AddRange(RightSourceCompareColumn.Split(';').ToList());
                        }
                        else
                        {
                            RightSourceCompareColumnList.Add(RightSourceCompareColumn);
                        }
                        List<string> LeftSourceResultColumnList = new List<string>();
                        if (LeftSourceResultColumn.Contains(";"))
                        {
                            LeftSourceResultColumnList.AddRange(LeftSourceResultColumn.Split(';').ToList());
                        }
                        else
                        {
                            LeftSourceResultColumnList.Add(LeftSourceResultColumn);
                        }
                        List<string> RightSourceResultColumnList = new List<string>();
                        if (RightSourceResultColumn.Contains(";"))
                        {
                            RightSourceResultColumnList.AddRange(RightSourceResultColumn.Split(';').ToList());
                        }
                        else
                        {
                            RightSourceResultColumnList.Add(RightSourceResultColumn);
                        }


                        //// equal compare Comapre

                        DataTable joinedTable = new DataTable();
                        foreach (string colName in LeftSourceResultColumnList)
                        {
                            joinedTable.Columns.Add(colName);
                        }
                        foreach (string colName in RightSourceResultColumnList)
                        {
                            joinedTable.Columns.Add(colName);
                        }

                        foreach (var item in Join)
                        {
                            DataRow newRow = joinedTable.NewRow();
                            foreach (DataColumn col in joinedTable.Columns)
                            {
                                var pullXValue = item.PX.Table.Columns.Contains(col.ColumnName) ? item.PX[col.ColumnName] : string.Empty;
                                var pullYValue = item.PY.Table.Columns.Contains(col.ColumnName) ? item.PY[col.ColumnName] : string.Empty;
                                newRow[col.ColumnName] = (pullXValue == null || string.IsNullOrEmpty(pullXValue.ToString())) ? pullYValue : pullXValue;
                            }
                            joinedTable.Rows.Add(newRow);
                        }


                        DataSet ds = new DataSet();
                        DataTable tablu = new DataTable();
                        tablu.Columns.Add("Files");

                        // string homePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                        string homePath = System.IO.Directory.GetCurrentDirectory();
                        string filesFolder = homePath + StringHelper.files;

                        ds.Tables.Add(joinedTable);
                        string filesPath = filesFolder + "\\Deliveries -" + DateTime.Now.ToString(StringHelper.mdyhms) + ".xlsx";
                        if (_objExcel.ExportDataSet(ds, filesPath))
                        {
                            tablu.Rows.Add(filesPath);
                        }
                        returnValue = tablu;
                        ret = StringHelper.success;
                    }
                }
                // end Excel

                else
                {
                    if (vh.checkVariableExists(LeftSource, executionID))
                    {
                        DVariables dv = vh.getVariables(LeftSource, executionID);
                        LSource = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                    }
                    else
                    {
                        LSource = new TypeHandler().getInput(LeftSource, StringHelper.value, executionID);
                    }

                    if (vh.checkVariableExists(RightSource, executionID))
                    {
                        DVariables dv = vh.getVariables(RightSource, executionID);
                        RSource = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                    }
                    else
                    {
                        RSource = new TypeHandler().getInput(RightSource, StringHelper.value, executionID);
                    }


                    List<string> LeftSourceCompareColumnList = new List<string>();
                    if (LeftSourceCompareColumn.Contains(";"))
                    {
                        LeftSourceCompareColumnList.AddRange(LeftSourceCompareColumn.Split(';').ToList());
                    }
                    else
                    {
                        LeftSourceCompareColumnList.Add(LeftSourceCompareColumn);
                    }
                    List<string> RightSourceCompareColumnList = new List<string>();
                    if (RightSourceCompareColumn.Contains(";"))
                    {
                        RightSourceCompareColumnList.AddRange(RightSourceCompareColumn.Split(';').ToList());
                    }
                    else
                    {
                        RightSourceCompareColumnList.Add(RightSourceCompareColumn);
                    }
                    List<string> LeftSourceResultColumnList = new List<string>();
                    if (LeftSourceResultColumn.Contains(";"))
                    {
                        LeftSourceResultColumnList.AddRange(LeftSourceResultColumn.Split(';').ToList());
                    }
                    else
                    {
                        LeftSourceResultColumnList.Add(LeftSourceResultColumn);
                    }
                    List<string> RightSourceResultColumnList = new List<string>();
                    if (RightSourceResultColumn.Contains(";"))
                    {
                        RightSourceResultColumnList.AddRange(RightSourceResultColumn.Split(';').ToList());
                    }
                    else
                    {
                        RightSourceResultColumnList.Add(RightSourceResultColumn);
                    }

                    if (LSource != null && LSource.Rows.Count > 0) //left source not null and has values
                    {
                        if (RSource != null && RSource.Rows.Count > 0) //right source not null and has values
                        {
                            if (LSource.Columns.Count == RSource.Columns.Count) //check column count equal for both left and right sources
                            {
                                int isEqual = 0; int isInEqual = 0;
                                foreach (DataColumn lDC in LSource.Columns)
                                {
                                    foreach (DataColumn rDC in RSource.Columns)
                                    {
                                        if (lDC.ColumnName.Trim().ToLower() == rDC.ColumnName.Trim().ToLower()) //check column names are equal on both datatables
                                        {
                                            isEqual++;
                                        }
                                        else
                                        {
                                            isInEqual++;
                                        }
                                    }
                                }

                                if (isEqual == LSource.Columns.Count) //if equal number of datacolumns count 
                                {
                                    for (int dc = 0; dc < LSource.Columns.Count; dc++)
                                    {
                                        LDSource.Columns.Add(RSource.Columns[dc].ColumnName);
                                    }
                                    for (int dc = 0; dc < RSource.Columns.Count; dc++)
                                    {
                                        RDSource.Columns.Add(RSource.Columns[dc].ColumnName);
                                    }
                                    if ((LeftSourceCompareColumnList.Count == 1 && LeftSourceCompareColumnList[0].Trim() == "*") && (RightSourceCompareColumnList.Count == 1 && RightSourceCompareColumnList[0].Trim() == "*")) //compare all cells
                                    {
                                        for (int i = 0; i < LSource.Rows.Count; i++)
                                        {
                                            for (int j = 0; j < LSource.Columns.Count; j++)
                                            {
                                                if (LSource.Rows[i][j].ToString() == RSource.Rows[i][j].ToString())
                                                {
                                                    var l = LSource.Rows[i].ItemArray.ToList();
                                                    var r = RSource.Rows[i].ItemArray.ToList();

                                                    if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                    {

                                                    }
                                                    else
                                                    {
                                                        LDSource.Rows.Add(l);
                                                        RDSource.Rows.Add(r);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if ((LeftSourceCompareColumnList.Count == 1 && LeftSourceCompareColumnList[0].Trim() == "*") && (RightSourceCompareColumnList.Count > 1)) //compare all cells
                                    {
                                        for (int i = 0; i < LSource.Rows.Count; i++)
                                        {
                                            foreach (string rsccl in RightSourceCompareColumnList)
                                            {
                                                for (int j = 0; j < LSource.Columns.Count; j++)
                                                {
                                                    if (RSource.Columns.Contains(rsccl))
                                                    {
                                                        if (LSource.Rows[i][j].ToString() == RSource.Rows[i][rsccl].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (LSource.Rows[i][Convert.ToInt32(j)].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                    }
                                    else if ((LeftSourceCompareColumnList.Count > 1) && (RightSourceCompareColumnList.Count == 1 && RightSourceCompareColumnList[0].Trim() == "*")) //compare all cells
                                    {
                                        for (int i = 0; i < RSource.Rows.Count; i++)
                                        {
                                            foreach (string lsccl in LeftSourceCompareColumnList)
                                            {
                                                for (int j = 0; j < RSource.Columns.Count; j++)
                                                {
                                                    if (LSource.Columns.Contains(lsccl))
                                                    {
                                                        if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][j].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][Convert.ToInt32(j)].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else //compare only selected cells
                                    {
                                        for (int i = 0; i < LSource.Rows.Count; i++)
                                        {
                                            foreach (string lsccl in LeftSourceCompareColumnList)
                                            {
                                                foreach (string rsccl in RightSourceCompareColumnList)
                                                {

                                                    if (LSource.Columns.Contains(lsccl) && RSource.Columns.Contains(rsccl))
                                                    {
                                                        if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][rsccl].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                    else if (LSource.Columns.Contains(lsccl) && !RSource.Columns.Contains(rsccl))
                                                    {
                                                        if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                    else if (!LSource.Columns.Contains(lsccl) && RSource.Columns.Contains(rsccl))
                                                    {
                                                        if (LSource.Rows[i][Convert.ToInt32(lsccl)].ToString() == RSource.Rows[i][rsccl].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (LSource.Rows[i][Convert.ToInt32(lsccl)].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else //unequal datacolumns count
                                {
                                    for (int dc = 0; dc < LSource.Columns.Count; dc++)
                                    {
                                        LDSource.Columns.Add(RSource.Columns[dc].ColumnName);
                                    }
                                    for (int dc = 0; dc < RSource.Columns.Count; dc++)
                                    {
                                        RDSource.Columns.Add(RSource.Columns[dc].ColumnName);
                                    }

                                    if ((LeftSourceCompareColumnList.Count == 1 && LeftSourceCompareColumnList[0].Trim() == "*") && (RightSourceCompareColumnList.Count == 1 && RightSourceCompareColumnList[0].Trim() == "*")) //compare all cells
                                    {
                                        for (int i = 0; i < LSource.Rows.Count; i++)
                                        {
                                            for (int j = 0; j < LSource.Columns.Count; j++)
                                            {
                                                if (LSource.Rows[i][j].ToString() == RSource.Rows[i][j].ToString())
                                                {
                                                    var l = LSource.Rows[i].ItemArray.ToList();
                                                    var r = RSource.Rows[i].ItemArray.ToList();

                                                    if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                    {

                                                    }
                                                    else
                                                    {
                                                        LDSource.Rows.Add(l);
                                                        RDSource.Rows.Add(r);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if ((LeftSourceCompareColumnList.Count == 1 && LeftSourceCompareColumnList[0].Trim() == "*") && (RightSourceCompareColumnList.Count > 1)) //compare all cells
                                    {
                                        for (int i = 0; i < LSource.Rows.Count; i++)
                                        {
                                            foreach (string rsccl in RightSourceCompareColumnList)
                                            {
                                                for (int j = 0; j < LSource.Columns.Count; j++)
                                                {
                                                    if (RSource.Columns.Contains(rsccl))
                                                    {
                                                        if (LSource.Rows[i][j].ToString() == RSource.Rows[i][rsccl].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (LSource.Rows[i][Convert.ToInt32(j)].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                    }
                                    else if ((LeftSourceCompareColumnList.Count > 1) && (RightSourceCompareColumnList.Count == 1 && RightSourceCompareColumnList[0].Trim() == "*")) //compare all cells
                                    {
                                        for (int i = 0; i < RSource.Rows.Count; i++)
                                        {
                                            foreach (string lsccl in LeftSourceCompareColumnList)
                                            {
                                                for (int j = 0; j < RSource.Columns.Count; j++)
                                                {
                                                    if (LSource.Columns.Contains(lsccl))
                                                    {
                                                        if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][j].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][Convert.ToInt32(j)].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else //compare only selected cells
                                    {
                                        for (int i = 0; i < LSource.Rows.Count; i++)
                                        {
                                            foreach (string lsccl in LeftSourceCompareColumnList)
                                            {
                                                foreach (string rsccl in RightSourceCompareColumnList)
                                                {

                                                    if (LSource.Columns.Contains(lsccl) && RSource.Columns.Contains(rsccl))
                                                    {
                                                        if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][rsccl].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                    else if (LSource.Columns.Contains(lsccl) && !RSource.Columns.Contains(rsccl))
                                                    {
                                                        if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                    else if (!LSource.Columns.Contains(lsccl) && RSource.Columns.Contains(rsccl))
                                                    {
                                                        if (LSource.Rows[i][Convert.ToInt32(lsccl)].ToString() == RSource.Rows[i][rsccl].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (LSource.Rows[i][Convert.ToInt32(lsccl)].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
                                                        {
                                                            var l = LSource.Rows[i].ItemArray.ToList();
                                                            var r = RSource.Rows[i].ItemArray.ToList();

                                                            if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                            {

                                                            }
                                                            else
                                                            {
                                                                LDSource.Rows.Add(l);
                                                                RDSource.Rows.Add(r);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else //even uneven count similar columns compare
                            {
                                for (int dc = 0; dc < LSource.Columns.Count; dc++)
                                {
                                    LDSource.Columns.Add(LSource.Columns[dc].ColumnName);
                                }
                                for (int dc = 0; dc < RSource.Columns.Count; dc++)
                                {
                                    RDSource.Columns.Add(RSource.Columns[dc].ColumnName);
                                }
                                if ((LeftSourceCompareColumnList.Count == 1 && LeftSourceCompareColumnList[0].Trim() == "*") && (RightSourceCompareColumnList.Count == 1 && RightSourceCompareColumnList[0].Trim() == "*")) //compare all cells
                                {
                                    for (int i = 0; i < LSource.Rows.Count; i++)
                                    {
                                        for (int j = 0; j < LSource.Columns.Count; j++)
                                        {
                                            if (LSource.Rows[i][j].ToString() == RSource.Rows[i][j].ToString())
                                            {
                                                var l = LSource.Rows[i].ItemArray.ToList();
                                                var r = RSource.Rows[i].ItemArray.ToList();

                                                if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                {

                                                }
                                                else
                                                {
                                                    LDSource.Rows.Add(l);
                                                    RDSource.Rows.Add(r);
                                                }
                                            }
                                        }
                                    }
                                }
                                else if ((LeftSourceCompareColumnList.Count == 1 && LeftSourceCompareColumnList[0].Trim() == "*") && (RightSourceCompareColumnList.Count > 1)) //compare all cells
                                {
                                    for (int i = 0; i < LSource.Rows.Count; i++)
                                    {
                                        foreach (string rsccl in RightSourceCompareColumnList)
                                        {
                                            for (int j = 0; j < LSource.Columns.Count; j++)
                                            {
                                                if (RSource.Columns.Contains(rsccl))
                                                {
                                                    if (LSource.Rows[i][j].ToString() == RSource.Rows[i][rsccl].ToString())
                                                    {
                                                        var l = LSource.Rows[i].ItemArray.ToList();
                                                        var r = RSource.Rows[i].ItemArray.ToList();

                                                        if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                        {

                                                        }
                                                        else
                                                        {
                                                            LDSource.Rows.Add(l);
                                                            RDSource.Rows.Add(r);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (LSource.Rows[i][Convert.ToInt32(j)].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
                                                    {
                                                        var l = LSource.Rows[i].ItemArray.ToList();
                                                        var r = RSource.Rows[i].ItemArray.ToList();

                                                        if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                        {

                                                        }
                                                        else
                                                        {
                                                            LDSource.Rows.Add(l);
                                                            RDSource.Rows.Add(r);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                                else if ((LeftSourceCompareColumnList.Count > 1) && (RightSourceCompareColumnList.Count == 1 && RightSourceCompareColumnList[0].Trim() == "*")) //compare all cells
                                {
                                    for (int i = 0; i < RSource.Rows.Count; i++)
                                    {
                                        foreach (string lsccl in LeftSourceCompareColumnList)
                                        {
                                            for (int j = 0; j < RSource.Columns.Count; j++)
                                            {
                                                if (LSource.Columns.Contains(lsccl))
                                                {
                                                    if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][j].ToString())
                                                    {
                                                        var l = LSource.Rows[i].ItemArray.ToList();
                                                        var r = RSource.Rows[i].ItemArray.ToList();

                                                        if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                        {

                                                        }
                                                        else
                                                        {
                                                            LDSource.Rows.Add(l);
                                                            RDSource.Rows.Add(r);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][Convert.ToInt32(j)].ToString())
                                                    {
                                                        var l = LSource.Rows[i].ItemArray.ToList();
                                                        var r = RSource.Rows[i].ItemArray.ToList();

                                                        if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                        {

                                                        }
                                                        else
                                                        {
                                                            LDSource.Rows.Add(l);
                                                            RDSource.Rows.Add(r);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else //compare only selected cells
                                {
                                    for (int i = 0; i < LSource.Rows.Count; i++)
                                    {
                                        foreach (string lsccl in LeftSourceCompareColumnList)
                                        {
                                            foreach (string rsccl in RightSourceCompareColumnList)
                                            {

                                                if (LSource.Columns.Contains(lsccl) && RSource.Columns.Contains(rsccl))
                                                {
                                                    if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][rsccl].ToString())
                                                    {
                                                        var l = LSource.Rows[i].ItemArray.ToList();
                                                        var r = RSource.Rows[i].ItemArray.ToList();

                                                        if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                        {

                                                        }
                                                        else
                                                        {
                                                            LDSource.Rows.Add(l);
                                                            RDSource.Rows.Add(r);
                                                        }
                                                    }
                                                }
                                                else if (LSource.Columns.Contains(lsccl) && !RSource.Columns.Contains(rsccl))
                                                {
                                                    if (LSource.Rows[i][lsccl].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
                                                    {
                                                        var l = LSource.Rows[i].ItemArray.ToList();
                                                        var r = RSource.Rows[i].ItemArray.ToList();

                                                        if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                        {

                                                        }
                                                        else
                                                        {
                                                            LDSource.Rows.Add(l);
                                                            RDSource.Rows.Add(r);
                                                        }
                                                    }
                                                }
                                                else if (!LSource.Columns.Contains(lsccl) && RSource.Columns.Contains(rsccl))
                                                {
                                                    if (LSource.Rows[i][Convert.ToInt32(lsccl)].ToString() == RSource.Rows[i][rsccl].ToString())
                                                    {
                                                        var l = LSource.Rows[i].ItemArray.ToList();
                                                        var r = RSource.Rows[i].ItemArray.ToList();

                                                        if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                        {

                                                        }
                                                        else
                                                        {
                                                            LDSource.Rows.Add(l);
                                                            RDSource.Rows.Add(r);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (LSource.Rows[i][Convert.ToInt32(lsccl)].ToString() == RSource.Rows[i][Convert.ToInt32(rsccl)].ToString())
                                                    {
                                                        var l = LSource.Rows[i].ItemArray.ToList();
                                                        var r = RSource.Rows[i].ItemArray.ToList();

                                                        if (LDSource.Rows[LDSource.Rows.Count - 1] == LSource.Rows[i] || RDSource.Rows[RDSource.Rows.Count - 1] == RSource.Rows[i])
                                                        {

                                                        }
                                                        else
                                                        {
                                                            LDSource.Rows.Add(l);
                                                            RDSource.Rows.Add(r);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            DataTable dummyData = new DataTable();
                            dummyData = LDSource.Copy();
                            dummyData = RDSource.Copy();

                            if ((LeftSourceResultColumnList.Count() == 1 && LeftSourceResultColumnList[0].Trim() == "*") && (RightSourceResultColumnList.Count() == 1 && RightSourceResultColumnList[0].Trim() == "*"))
                            {
                                returnValue = LDSource.Copy();
                                returnValue = RDSource.Copy();
                            }
                            else if ((LeftSourceResultColumnList.Count() == 1 && LeftSourceResultColumnList[0].Trim() == "*") && RightSourceResultColumnList.Count() > 1)
                            {
                                DataTable RightFilteredData = dummyData.DefaultView.ToTable(false, RightSourceResultColumnList.ToArray());
                                returnValue = LDSource.Copy();
                                returnValue = RightFilteredData.Copy();
                            }
                            else if (LeftSourceResultColumnList.Count > 1 && (RightSourceResultColumnList.Count() == 1 && RightSourceResultColumnList[0].Trim() == "*"))
                            {
                                DataTable LeftFilteredData = dummyData.DefaultView.ToTable(false, LeftSourceResultColumnList.ToArray());
                                returnValue = RDSource.Copy();
                                returnValue = LeftFilteredData.Copy();
                            }
                            else
                            {
                                DataTable RightFilteredData = dummyData.DefaultView.ToTable(false, RightSourceResultColumnList.ToArray());
                                returnValue = LDSource.Copy();
                                returnValue = RightFilteredData.Copy();

                                DataTable LeftFilteredData = dummyData.DefaultView.ToTable(false, LeftSourceResultColumnList.ToArray());
                                returnValue = RDSource.Copy();
                                returnValue = LeftFilteredData.Copy();
                            }

                            ret = StringHelper.success;
                        }
                        else
                        {

                        }
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(Result, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult SplitString(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string input = string.Empty; string output = string.Empty; string isBullet = string.Empty;
            string rowsSeperator = string.Empty; string columnSeperator = string.Empty;
            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                rowsSeperator = stp.getPropertiesValue("rowseperator", step);
                columnSeperator = stp.getPropertiesValue("columnseperator", step);
                isBullet = stp.getPropertiesValue("isbullet", step);

                List<string> inputs = new List<string>();

                if (input.Trim() != string.Empty && vh.checkVariableExists(input, executionID))
                {
                    DataTable dt = JsonConvert.DeserializeObject<DataTable>(new VariableHandler().getVariables(input, executionID).vlvalue);
                    //inputs = dt.AsEnumerable().Select(r => r.Field<string>("Content")).ToList();
                    inputs = dt.AsEnumerable().Select(x => x[0].ToString()).ToList();
                }
                else
                {
                    inputs.Add(input);
                }

                inputs.RemoveAll(i => i.Trim() == string.Empty);

                if (isBullet.Trim().ToLower() == "true")
                {
                    returnValue.Columns.Add();
                    foreach (string inp in inputs)
                    {
                        List<string> bullets = new List<string>();
                        bullets = makeBulletTable(inp, rowsSeperator, columnSeperator);
                        bullets.ForEach(b => returnValue.Rows.Add(b));
                    }
                }
                else
                {
                    foreach (string inp in inputs)
                    {
                        returnValue = makeTable(inp, rowsSeperator, columnSeperator);
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        private DataTable makeTable(string input, string rowSeperator, string columnSeperator)
        {
            DataTable dt = new DataTable();
            List<string> rows = new List<string>(); List<string> columns = new List<string>();

            if (rowSeperator.Trim().Length > 0)
            {
                if (rowSeperator.Trim() == "\\n")
                {
                    rowSeperator = "\n";
                }
                rows = input.Split(new string[] { rowSeperator }, StringSplitOptions.None).ToList();
            }
            else
            {
                rows.Add(input);
            }

            int colCount = 0;
            foreach(string row in rows)
            {
                if (columnSeperator.Trim().Length > 0)
                {
                    if (columnSeperator.Trim() == "\\n")
                    {
                        columnSeperator = "\n";
                    }
                    columns = row.Split(new string[] { columnSeperator }, StringSplitOptions.None).ToList();
                }
                else
                {
                    columns.Add(row);
                }
                if(colCount < columns.Count)
                {
                    colCount = columns.Count();
                }
            }

            if(colCount > 0)
            {
                for(int cols = 0; cols < colCount; cols++)
                {
                    dt.Columns.Add();
                }
                foreach (string row in rows)
                {
                    if (columnSeperator.Trim().Length > 0)
                    {
                        dt.Rows.Add(row.Split(new string[] { columnSeperator }, StringSplitOptions.None));
                    }
                    else
                    {
                        dt.Rows.Add(row);
                    }
                }
            }

            return dt;
        }
        private List<string> makeBulletTable(string input, string rowSeperator, string columnSeperator)
        {
            List<string> bullets = new List<string>();

            if (columnSeperator.Trim().Contains("\\n"))
            {
                columnSeperator = columnSeperator.Replace("\\n","\n");
            }
            else if(columnSeperator.Trim() == "\\n")
            {
                columnSeperator = "\n";
            }


            if (rowSeperator.Trim().Contains("\\n"))
            {
                rowSeperator = rowSeperator.Replace("\\n", "\n");
            }
            else if (rowSeperator.Trim() == "\\n")
            {
                rowSeperator = "\n";
            }

            List<string> rows = new List<string>(); List<string> columns = new List<string>();

            if (rowSeperator.Length > 0)
            {
                rows = input.Split(new string[] { rowSeperator }, StringSplitOptions.None).ToList();
            }
            else
            {
                rows.Add(input);
            }

            //rows.RemoveAt(0);
            rows.RemoveAll(r => r.Trim() == string.Empty);

            foreach (string row in rows)
            {
                if (columnSeperator.Length > 0)
                {
                    bullets.Add(row.Split(new string[] { columnSeperator }, StringSplitOptions.None)[0]);
                }
                else
                {
                    bullets.Add(row);
                }
            }

            return bullets;
        }
        public MethodResult TwoDSplit(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string input = string.Empty; string output = string.Empty;
            string seperator = string.Empty;
            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                seperator = stp.getPropertiesValue(StringHelper.seperator, step);

                DataTable indt = new DataTable();
                int subDT = 0;

                if(input.Contains("[") && input.Contains("]"))
                {
                    int pFrom = input.IndexOf("[") + 1;
                    int pTo = input.LastIndexOf("]");

                    string result = input.Substring(pFrom, pTo - pFrom);
                    subDT = int.Parse(result);
                    input = input.Substring(0, pFrom - 1) + "}";
                }

                if (input.Trim() != string.Empty && vh.checkVariableExists(input, executionID))
                {
                    indt = JsonConvert.DeserializeObject<DataTable>(new VariableHandler().getVariables(input, executionID).vlvalue);
                }

                if(seperator.Trim() != string.Empty && vh.checkVariableExists(seperator, executionID))
                {
                    DataTable sindt = JsonConvert.DeserializeObject<DataTable>(new VariableHandler().getVariables(input, executionID).vlvalue);
                    if(sindt.Columns.Count > 0)
                    {
                        List<object> srow = sindt.Rows[0].ItemArray.ToList();
                        DataTable resulte = new DataTableHandler().SeperatedByRow(indt, srow, subDT);
                        returnValue = resulte.Copy();
                    }
                    else
                    {
                        if (sindt.Rows[0][0].ToString().Contains(';'))
                        {
                            List<object> srow = sindt.Rows[0][0].ToString().Split(';').ToList().OfType<object>().ToList();
                            DataTable resulte = new DataTableHandler().SeperatedByRow(indt, srow, subDT);
                            returnValue = resulte.Copy();
                        }
                        else
                        {
                            int rownum = 0;
                            if (int.TryParse(sindt.Rows[0][0].ToString(), out rownum))
                            {
                                DataTable resulte = new DataTableHandler().SeperatedByRowNumber(indt, rownum, subDT);
                                returnValue = resulte.Copy();
                            }
                            else
                            {
                                DataTable resulte = new DataTableHandler().SeperatedByRowNumber(indt, 0, subDT);
                                returnValue = resulte.Copy();
                            }
                        }
                    }
                }
                else
                {
                    if (seperator.Contains(';'))
                    {
                        List<object> srow = seperator.Split(';').ToList().OfType<object>().ToList();
                        DataTable resulte = new DataTableHandler().SeperatedByRow(indt, srow, subDT);
                        returnValue = resulte.Copy();
                    }
                    else
                    {
                        int rownum = 0;
                        if (int.TryParse(seperator, out rownum))
                        {
                            DataTable resulte = new DataTableHandler().SeperatedByRowNumber(indt, rownum, subDT);
                            returnValue = resulte.Copy();
                        }
                        else
                        {
                            DataTable resulte = new DataTableHandler().SeperatedByRowNumber(indt, rownum, subDT);
                            returnValue = resulte.Copy();
                        }
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        
        public static int printIndex(String str, String s, int occurance)
        {
            bool flag = false;
            for (int i = 0, count = 0; i < str.Length - s.Length + 1; i++)
            {
                if (str.Substring(i,s.Length).Equals(s))
                {
                    if (++count == occurance)
                    {
                        //Console.Write(i + " ");
                        flag = true;
                        return i;
                    }
                }
            }
            return -1;
        }
        public MethodResult Suite(Steps step, string executionID)
        {
            //string excelpaths = Path.Combine(Directory.GetCurrentDirectory(), StringHelper.files, "excels", "NetsuiteComponents.xlsx");
            string base64Guid = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            MethodResult mr = new MethodResult();
            string ret = string.Empty;
            DataTable inpt = new DataTable();
            DataTable tablu = new DataTable(); tablu.Columns.Add("Files");
            DataTable excelu = new DataTable();
            string Input = string.Empty;
            string Input1 = string.Empty;
            string Output = string.Empty;
            string excelpaths = String.Empty;
            List<string> allofdotstrings = new List<string>();
            List<SuiteGuide> allThings = new List<SuiteGuide>();
            List<SuiteGuide> fileIndexes = new List<SuiteGuide>();
            List<string> DWNLDS = new List<string>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            Input1 = stp.getPropertiesValue(StringHelper.input1, step);
            excelu = new TypeHandler().getInput(Input1, StringHelper.variable, executionID);
            for (int i = 0; i < excelu.Rows.Count; i++)
            {
                excelpaths = excelu.Rows[i].ItemArray[0].ToString();
            }
            try
            {
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                inpt = new TypeHandler().getInput(Input, StringHelper.variable, executionID);
                for (int i = 0; i < inpt.Rows.Count; i++)
                {
                    Input = inpt.Rows[i].ItemArray[0].ToString();
                    TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                    Input = textInfo.ToTitleCase(Input);
                }
                List<string> splitModulesRAW = new List<string>();
                splitModulesRAW = Input.Split(new string[] { " · " }, StringSplitOptions.None).ToList();
                splitModulesRAW.RemoveAt(0);

                string DescriptionsRaw = string.Empty;

                //modules, submodules and extended submodules
                for (int k = 0; k < splitModulesRAW.Count; k++)
                {
                    List<string> rawModulesData = new List<string>();
                    if (k == splitModulesRAW.Count - 1)
                    {
                        string firstPart = splitModulesRAW[k].Split(new string[] { "\n\n" + fileIndexes[0].module }, StringSplitOptions.None)[0];
                        string secondPart = splitModulesRAW[k].Substring(firstPart.Length);
                        DescriptionsRaw = fileIndexes[0].module + secondPart;
                        rawModulesData = firstPart.Split(new string[] { "\n", "\\n" }, StringSplitOptions.None).ToList();
                    }
                    else
                    {
                        rawModulesData = splitModulesRAW[k].Split(new string[] { "\n", "\\n" }, StringSplitOptions.None).ToList();
                    }

                    DescriptionsRaw = Regex.Replace(DescriptionsRaw, @"\n\nReleasenotes_2020.[0-9].0.Pdf — Subject To Change\n\n.*[0-9]\n\n\f.*[a-zA-Z0-9()]\n\n.*[0-9]\n\n", "\n");
                    DescriptionsRaw = Regex.Replace(DescriptionsRaw, @"\n\nReleasenotes_2020.[0-9].0.Pdf — Subject To Change\n\n.*[0-9]\n\n.*[a-zA-Z0-9()]\n\n", "\n");
                    DescriptionsRaw = Regex.Replace(DescriptionsRaw, @"\n\nReleasenotes_2020.[0-9].0.Pdf — Subject To Change\n\n\f.*[a-zA-Z0-9()]\n\n.*[0-9]\n\n", "\n");
                    DescriptionsRaw = Regex.Replace(DescriptionsRaw, @"\n\nReleasenotes_2020.[0-9].0.Pdf — Subject To Change\n\n\f.*[a-zA-Z0-9()]\n\n", "\n");
                    DescriptionsRaw = Regex.Replace(DescriptionsRaw, @"\n\nReleasenotes_2020.[0-9].0.Pdf — Subject To Change\n\n", "\n");
                    string module = rawModulesData[0];
                    string submodule = string.Empty;
                    bool isdone = true;

                    foreach (string rawModuleData in rawModulesData)
                    {
                        if (isdone==true)
                        {
                            SuiteGuide sg = new SuiteGuide();
                            sg.index = 0;
                            sg.instance = base64Guid;
                            sg.module = module;
                            sg.submodule = string.Empty;
                            sg.extendedsubmodule = string.Empty;
                            sg.description = string.Empty;
                            sg.features = "N/A";
                            sg.billingcomponents = "N/A";
                            sg.addonbundles = "N/A";
                            sg.addonmodules = "N/A";
                            sg.count = "N/A";
                            sg.counter = 0;
                            sg.releasable = string.Empty;
                            sg.billable = string.Empty;
                            fileIndexes.Add(sg);
                            isdone = false;
                        }
                        if (rawModuleData.Contains("■"))
                        {
                            submodule = rawModuleData.Split('■')[1];

                            SuiteGuide sg = new SuiteGuide();
                            sg.index = 0;
                            sg.instance = base64Guid;
                            sg.module = module;
                            sg.submodule = submodule;
                            sg.extendedsubmodule = string.Empty;
                            sg.description = string.Empty;
                            sg.features = "N/A";
                            sg.billingcomponents = "N/A";
                            sg.addonbundles = "N/A";
                            sg.addonmodules = "N/A";
                            sg.count = "N/A";
                            sg.counter = 0;
                            sg.releasable = string.Empty;
                            sg.billable = string.Empty;
                            fileIndexes.Add(sg);
                        }
                        else if (rawModuleData.Contains("□"))
                        {
                            string extsubmodule = rawModuleData.Split('□')[1];

                            SuiteGuide sg = new SuiteGuide();
                            sg.index = 0;
                            sg.instance = base64Guid;
                            sg.module = module;
                            sg.submodule = submodule;
                            sg.extendedsubmodule = extsubmodule;
                            sg.description = string.Empty;
                            sg.features = "N/A";
                            sg.billingcomponents = "N/A";
                            sg.addonbundles = "N/A";
                            sg.addonmodules = "N/A";
                            sg.count = "N/A";
                            sg.counter = 0;
                            sg.releasable = string.Empty;
                            sg.billable = string.Empty;
                            fileIndexes.Add(sg);
                        }
                    }

                }

                fileIndexes = fileIndexes.Distinct().ToList();

                //description
                int counter = 0;
                foreach (SuiteGuide fileIndex in fileIndexes)
                {
                    int occurances = Regex.Matches(DescriptionsRaw, fileIndex.submodule).Count;
                    if (DescriptionsRaw.Trim() != string.Empty)
                    {
                        string sub = string.Empty;
                        int startIndex = 0;
                        int length = 0;

                        if (fileIndex.extendedsubmodule.Trim() == string.Empty&& fileIndex.submodule.Trim()!= string.Empty)
                        {
                            //not an extended submodule
                            sub = fileIndex.submodule;
                            startIndex = DescriptionsRaw.ToLower().LastIndexOf(Truncate("\n" + sub.ToLower().Trim(), 40).Trim());
                            try
                            {
                                for (int l = counter; l < fileIndexes.Count; l++)
                                {
                                    if (fileIndexes[l].submodule.Trim() != sub.Trim())
                                    {
                                        if (occurances > 3)
                                        {
                                            int startindexes = 0;
                                            int lengthy = 0;
                                            startindexes = DescriptionsRaw.ToLower().LastIndexOf(Truncate("■ " + fileIndex.submodule.ToLower().Trim(), 40).Trim());
                                            string data = DescriptionsRaw.Substring(startindexes + fileIndex.submodule.Length, DescriptionsRaw.Length - startindexes - fileIndex.submodule.Length);
                                            lengthy = data.ToLower().IndexOf(Truncate("\n" + fileIndex.submodule.ToLower().Trim() + "\n", 40).Trim());
                                            startIndex = lengthy + startindexes + fileIndex.submodule.Length;
                                        }
                                        length = DescriptionsRaw.ToLower().LastIndexOf("\n" + Truncate(fileIndexes[l].submodule.ToLower().Trim(), 40)) - startIndex;

                                        if (sub.Length > 40)
                                        {
                                            string sub_rest = sub.Substring(40, sub.Length - 40);
                                            sub = fileIndex.submodule.Substring(0, 40);
                                            int index = DescriptionsRaw.Substring(DescriptionsRaw.ToLower().LastIndexOf(Truncate("\n" + sub.ToLower().Trim(), 40).Trim()), 100).IndexOf("\n") + startIndex;
                                            //string cleanPath = (index < 0)
                                            //    ? DescriptionsRaw
                                            //    : DescriptionsRaw.Remove(index, 1);
                                            char[] array = DescriptionsRaw.ToCharArray();
                                            array[index] = ' ';
                                            string cleanPath = new string(array);
                                            if (cleanPath.Substring(startIndex, fileIndex.submodule.Length).Trim(' ') == (sub + sub_rest).Trim() + "\n")
                                            {
                                                startIndex = startIndex;
                                                if (DescriptionsRaw.Substring(startIndex, 40) == fileIndex.submodule.Trim(' ').Substring(0, 40))
                                                    startIndex = startIndex + (sub + sub_rest).Trim().Length + 1;
                                            }
                                            else
                                            {
                                                List<string> items_new = DescriptionsRaw.Split("\n" + sub.Trim(), StringSplitOptions.RemoveEmptyEntries).ToList();
                                                items_new.RemoveAt(0);
                                                foreach (string item_new in items_new)
                                                {
                                                    int sample = DescriptionsRaw.IndexOf(item_new) - sub.Trim().Length;
                                                    index = sample + DescriptionsRaw.Substring(sample, 100).IndexOf("\n");
                                                    array = DescriptionsRaw.ToCharArray();
                                                    array[index] = ' ';
                                                    cleanPath = new string(array);
                                                    if ((cleanPath.Substring(sample, fileIndex.submodule.Length).Trim(' ') == (sub + sub_rest).Trim() + "\n") || (cleanPath.Substring(sample, fileIndex.submodule.Length).Trim(' ') == (sub + sub_rest).Trim()))
                                                    {
                                                        startIndex = sample + (sub + sub_rest).Trim().Length + 1;
                                                    }
                                                    else if (cleanPath.Substring(sample, fileIndex.submodule.Length - 1).Trim(' ') == (sub + sub_rest).Trim())
                                                    {
                                                        startIndex = sample + (sub + sub_rest).Trim().Length;
                                                    }
                                                }
                                            }
                                        }
                                        else {
                                            int index = DescriptionsRaw.ToLower().LastIndexOf(Truncate("\n" + sub.ToLower().Trim() + "\n", 40)) + 1;
                                            if (DescriptionsRaw.Substring(index, DescriptionsRaw.Substring(index, 100).Trim().IndexOf("\n") + 1) == fileIndex.submodule.Trim() + "\n") {
                                                startIndex = index;
                                            }
                                        }
                                        if (DescriptionsRaw.Substring(startIndex, 100).Trim().StartsWith(Truncate(fileIndex.submodule.Trim(), 40)))
                                        {
                                            if (DescriptionsRaw.Substring(startIndex, DescriptionsRaw.Substring(startIndex, 100).Trim().IndexOf("\n")) == fileIndex.submodule.Trim())
                                            {
                                                startIndex = startIndex + DescriptionsRaw.Substring(startIndex, 100).Trim().IndexOf("\n") + 1;
                                            }
                                            else
                                            {
                                                int index = DescriptionsRaw.Substring(startIndex, 100).Trim().IndexOf("\n") + startIndex;
                                                char[] array = DescriptionsRaw.ToCharArray();
                                                array[index] = ' ';
                                                string cleanPath = new string(array);
                                                if (cleanPath.Substring(startIndex, cleanPath.Substring(startIndex, 100).Trim().IndexOf("\n")) == fileIndex.submodule.Trim())
                                                {
                                                    startIndex = startIndex + cleanPath.Substring(startIndex, 100).Trim().IndexOf("\n") + 1;
                                                }
                                            }
                                        }
                                        length = DescriptionsRaw.ToLower().LastIndexOf("\n" + Truncate(fileIndexes[l].submodule.ToLower().Trim(), 40)) - startIndex;
                                        if (length < 1)
                                        {
                                            string rawLength = DescriptionsRaw.ToLower().Split(new string[] { Truncate("\n\n" + sub.ToLower().Trim(), 40) }, StringSplitOptions.None)[1];
                                            length = rawLength.Split(new string[] { "\n\n" }, StringSplitOptions.None)[0].Length;
                                            if (DescriptionsRaw.Substring(startIndex, length).Contains("\n\n"))
                                            {
                                                length = DescriptionsRaw.Substring(startIndex, length).IndexOf("\n\n");
                                            }
                                        }
                                        break;
                                    }
                                }
                                if (counter == fileIndexes.Count - 1)
                                {
                                    length = DescriptionsRaw.Length - startIndex;
                                }
                            }
                            catch
                            {
                                length = DescriptionsRaw.Length - startIndex;
                            }
                        }
                        else if(fileIndex.extendedsubmodule.Trim() != string.Empty)
                        {
                            int occurance = Regex.Matches(DescriptionsRaw, fileIndex.extendedsubmodule).Count;
                            sub = fileIndex.extendedsubmodule;
                            startIndex = DescriptionsRaw.ToLower().LastIndexOf(Truncate("\n" + sub.ToLower().Trim(), 40).Trim());
                            string nextSub = string.Empty;
                            try
                            {
                                if (fileIndexes[counter + 1].extendedsubmodule.Trim() == string.Empty)
                                {
                                    nextSub = fileIndexes[counter + 1].submodule;
                                }
                                else
                                {
                                    nextSub = fileIndexes[counter + 1].extendedsubmodule;
                                }
                                if (nextSub != string.Empty)
                                {
                                    if (occurance >= 3)
                                    {
                                        int startindexes = 0;
                                        int startindexes1 = 0;
                                        int lengthy = 0;
                                        int lengthy1 = 0;
                                        startindexes = DescriptionsRaw.ToLower().LastIndexOf(Truncate("■ " + fileIndex.extendedsubmodule.ToLower().Trim(), 40).Trim());
                                        startindexes1 = DescriptionsRaw.ToLower().LastIndexOf(Truncate("□ " + fileIndex.extendedsubmodule.ToLower().Trim(), 40).Trim());
                                        string data = DescriptionsRaw.Substring(startindexes + fileIndex.extendedsubmodule.Length, DescriptionsRaw.Length - startindexes - fileIndex.extendedsubmodule.Length);
                                        string data1 = DescriptionsRaw.Substring(startindexes1 + fileIndex.extendedsubmodule.Length, DescriptionsRaw.Length - startindexes1 - fileIndex.extendedsubmodule.Length);
                                        lengthy = data.ToLower().IndexOf(Truncate("\n" + fileIndex.extendedsubmodule.ToLower().Trim() + "\n", 40).Trim());
                                        lengthy1 = data1.ToLower().IndexOf(Truncate("\n" + fileIndex.extendedsubmodule.ToLower().Trim() + "\n", 40).Trim());
                                        if (lengthy != -1)
                                        {
                                            startIndex = lengthy + startindexes + fileIndex.extendedsubmodule.Length;
                                        }
                                        if (lengthy1 != -1)
                                        {
                                            startIndex = lengthy1 + startindexes1 + fileIndex.extendedsubmodule.Length;
                                        }
                                    }
                                    length = DescriptionsRaw.ToLower().LastIndexOf("\n" + Truncate(fileIndexes[counter + 1].extendedsubmodule.ToLower().Trim(), 40)) - startIndex;
                                    if (sub.Length > 40)
                                    {
                                        string sub_rest = sub.Substring(40, sub.Length - 40);
                                        sub = fileIndex.extendedsubmodule.Substring(0, 40);
                                        int index = DescriptionsRaw.Substring(DescriptionsRaw.ToLower().LastIndexOf(Truncate("\n" + sub.ToLower().Trim(), 40).Trim()), 100).IndexOf("\n") + startIndex;
                                        //string cleanPath = (index < 0)
                                        //    ? DescriptionsRaw
                                        //    : DescriptionsRaw.Remove(index, 1);
                                        char[] array = DescriptionsRaw.ToCharArray();
                                        array[index] = ' ';
                                        string cleanPath = new string(array);
                                        if (cleanPath.Substring(startIndex, fileIndex.extendedsubmodule.Length+1).Trim(' ') == (sub + sub_rest).Trim() + "\n")
                                        {
                                            if (DescriptionsRaw.Substring(startIndex, 40)== fileIndex.extendedsubmodule.Trim(' ').Substring(0,40))
                                            startIndex = startIndex + (sub + sub_rest).Trim().Length + 1;
                                        }
                                        else
                                        {
                                            List<string> items_new = DescriptionsRaw.Split("\n" + sub.Trim(), StringSplitOptions.RemoveEmptyEntries).ToList();
                                            items_new.RemoveAt(0);
                                            foreach (string item_new in items_new)
                                            {
                                                int sample = DescriptionsRaw.IndexOf(item_new) - sub.Trim().Length;
                                                index = sample + DescriptionsRaw.Substring(sample, 100).IndexOf("\n");
                                                //if(DescriptionsRaw.ToLower().Substring(DescriptionsRaw.IndexOf(item_new), 40).StartsWith(sub_rest.ToLower().Trim()))
                                                //{
                                                //    index

                                                //}
                                                array = DescriptionsRaw.ToCharArray();
                                                array[index] = ' ';
                                                cleanPath = new string(array);
                                                if ((cleanPath.Substring(sample, fileIndex.extendedsubmodule.Length).Trim(' ') == (sub + sub_rest).Trim() + "\n") || (cleanPath.Substring(sample, fileIndex.extendedsubmodule.Length).Trim(' ') == (sub + sub_rest).Trim()))
                                                {
                                                    startIndex = sample + (sub + sub_rest).Trim().Length + 1;
                                                }
                                                else if (cleanPath.Substring(sample, fileIndex.extendedsubmodule.Length - 1).Trim(' ') == (sub + sub_rest).Trim()) {
                                                    startIndex = sample + (sub + sub_rest).Trim().Length;
                                                }
                                            }
                                        }
                                    }
                                    if (DescriptionsRaw.Substring(startIndex, 100).Trim().StartsWith(Truncate(fileIndex.extendedsubmodule.Trim(), 40)))
                                    {
                                        if (DescriptionsRaw.Substring(startIndex, DescriptionsRaw.Substring(startIndex, 100).Trim().IndexOf("\n")) == fileIndex.extendedsubmodule.Trim())
                                        {
                                            startIndex = startIndex + DescriptionsRaw.Substring(startIndex, 100).Trim().IndexOf("\n") + 1;
                                        }
                                        else
                                        {
                                            int index = DescriptionsRaw.Substring(startIndex, 100).Trim().IndexOf("\n") + startIndex;
                                            char[] array = DescriptionsRaw.ToCharArray();
                                            array[index] = ' ';
                                            string cleanPath = new string(array);
                                            if (cleanPath.Substring(startIndex, cleanPath.Substring(startIndex, 100).Trim().IndexOf("\n")) == fileIndex.submodule.Trim())
                                            {
                                                startIndex = startIndex + cleanPath.Substring(startIndex, 100).Trim().IndexOf("\n") + 1;
                                            }
                                        }

                                    }

                                    if (nextSub != "")
                                        length = DescriptionsRaw.ToLower().LastIndexOf("\n" + Truncate(nextSub.ToLower().Trim(), 40)) - startIndex;
                                    else
                                        length = DescriptionsRaw.ToLower().LastIndexOf("\n" + Truncate(nextSub.ToLower().Trim(), 40)) - startIndex;

                                    if (length < 1)
                                    {
                                        string rawLength = DescriptionsRaw.ToLower().Split(new string[] { Truncate("\n" + sub.ToLower().Trim(), 40) }, StringSplitOptions.None)[1];
                                        length = rawLength.Split(new string[] { "\n\n" }, StringSplitOptions.None)[0].Length;
                                        if (DescriptionsRaw.Substring(startIndex, length).Contains("\n\n")) {
                                            length = DescriptionsRaw.Substring(startIndex, length).IndexOf("\n\n");
                                        }
                                    }
                                }
                                else
                                {
                                    length = DescriptionsRaw.Length - startIndex;
                                }
                            }
                            catch
                            {
                                length = DescriptionsRaw.Length - startIndex;
                                string dummy = DescriptionsRaw.Substring(startIndex, length);
                                length = (dummy.Split("\n\n")[0] + dummy.Split("\n\n")[1]).Length + 5;
                            }
                        }

                        if (startIndex > 0 && length > 0)
                        {
                            fileIndex.description = DescriptionsRaw.Substring(startIndex, length);
                        }

                    }
                    try
                    {
                        if (fileIndexes[counter].description.Length > 32000)
                        {
                            fileIndexes[counter].description = fileIndexes[counter].description.ToString().Substring(0, 32000);
                        }
                        else
                        {
                            fileIndexes[counter].description = fileIndexes[counter].description.ToString();
                        }

                        DataTable f = new Util().getExcel(excelpaths, "Features");
                        foreach (DataRow dataRow in f.Rows)
                        {
                            if (fileIndexes[counter].description.ToLower().IndexOf(dataRow[0].ToString().Trim().ToLower()) > -1 || fileIndexes[counter].submodule.ToLower().IndexOf(dataRow[0].ToString().Trim().ToLower()) > -1)
                            {
                                if (fileIndexes[counter].features.Trim() == "N/A")
                                {
                                    fileIndexes[counter].features = string.Empty;
                                }
                                else
                                {
                                    List<string> at = fileIndexes[counter].features.Split('.').ToList();
                                    if (at.Contains(dataRow[0].ToString().Trim()))
                                    {

                                    }
                                    else
                                    {
                                        fileIndexes[counter].features = fileIndexes[counter].features + dataRow[0].ToString().Trim() + ".";
                                        if (dataRow[1].ToString().Trim() != string.Empty && dataRow[1].ToString().Trim() == "Yes")
                                        {
                                            fileIndexes[counter].count = "Yes";
                                            fileIndexes[counter].billable = fileIndexes[counter].billable + dataRow[0].ToString().Trim() + ".";
                                        }
                                        else
                                        {
                                            fileIndexes[counter].releasable = fileIndexes[counter].releasable + dataRow[0].ToString().Trim() + ".";
                                        }

                                        fileIndexes[counter].counter++;
                                    }
                                }
                            }
                        }
                        DataTable bc = new Util().getExcel(excelpaths, "BillableComponents");

                        foreach (DataRow dataRow in bc.Rows)
                        {
                            try
                            {

                                if (fileIndexes[counter].description.ToLower().IndexOf(dataRow[0].ToString().Trim().ToLower()) > -1 || fileIndexes[counter].submodule.ToLower().IndexOf(dataRow[0].ToString().Trim().ToLower()) > -1)
                                {

                                    if (fileIndexes[counter].billingcomponents.Trim() == "N/A")
                                    {
                                        fileIndexes[counter].billingcomponents = fileIndexes[counter].billingcomponents + dataRow[0].ToString().Trim() + ".";
                                        fileIndexes[counter].billingcomponents = fileIndexes[counter].billingcomponents.Replace("N/A", "");

                                    }
                                    else
                                    {
                                        List<string> at = fileIndexes[counter].features.Split('.').ToList();
                                        if (at.Contains(dataRow[0].ToString().Trim()))
                                        {
                                            //allThings[k].releasable = allThings[k].releasable + dataRow[0].ToString().Trim() + ".";
                                        }
                                        else
                                        {
                                            fileIndexes[counter].features = fileIndexes[counter].features + dataRow[0].ToString().Trim() + ".";
                                            if (dataRow[1].ToString().Trim() != string.Empty && dataRow[1].ToString().Trim() == "Yes")
                                            {
                                                fileIndexes[counter].count = "Yes";
                                                fileIndexes[counter].billable = fileIndexes[counter].billable + dataRow[0].ToString().Trim() + ".";
                                            }
                                            else
                                            {
                                                fileIndexes[counter].releasable = fileIndexes[counter].releasable + dataRow[0].ToString().Trim() + ".";
                                            }

                                        }
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                string h = ex.Message;
                            }
                        }

                        DataTable am = new Util().getExcel(excelpaths, "AddonModules");
                        foreach (DataRow dataRow in am.Rows)
                        {
                            if (fileIndexes[counter].description.ToLower().IndexOf(dataRow[0].ToString().Trim().ToLower()) > -1 || fileIndexes[counter].submodule.ToLower().IndexOf(dataRow[0].ToString().Trim().ToLower()) > -1)
                            {

                                if (fileIndexes[counter].addonmodules.Trim() == "N/A")
                                {
                                    fileIndexes[counter].addonmodules = fileIndexes[counter].addonmodules + dataRow[0].ToString().Trim() + ".";
                                    fileIndexes[counter].addonmodules = fileIndexes[counter].addonmodules.Replace("N/A", "");
                                }
                                else
                                {
                                    List<string> at = fileIndexes[counter].features.Split('.').ToList();
                                    if (at.Contains(dataRow[0].ToString().Trim()))
                                    {
                                        // allThings[k].releasable = allThings[k].releasable + dataRow[0].ToString().Trim() + ".";
                                    }
                                    else
                                    {
                                        fileIndexes[counter].features = fileIndexes[counter].features + dataRow[0].ToString().Trim() + ".";
                                        if (dataRow[1].ToString().Trim() != string.Empty && dataRow[1].ToString().Trim() == "Yes")
                                        {
                                            fileIndexes[counter].count = "Yes";
                                            fileIndexes[counter].billable = fileIndexes[counter].billable + dataRow[0].ToString().Trim() + ".";
                                        }
                                        else
                                        {
                                            fileIndexes[counter].releasable = fileIndexes[counter].releasable + dataRow[0].ToString().Trim() + ".";
                                        }
                                        fileIndexes[counter].counter++;
                                    }
                                }
                            }
                        }
                        DataTable ab = new Util().getExcel(excelpaths, "AddonBundles");
                        foreach (DataRow dataRow in ab.Rows)
                        {
                            if (fileIndexes[counter].description.ToLower().IndexOf(dataRow[0].ToString().Trim().ToLower()) > -1 || fileIndexes[counter].submodule.ToLower().IndexOf(dataRow[0].ToString().Trim().ToLower()) > -1)
                            {
                                if (fileIndexes[counter].addonbundles.Trim() == "N/A")
                                {
                                    fileIndexes[counter].addonbundles = fileIndexes[counter].addonbundles + dataRow[0].ToString().Trim() + ".";
                                    fileIndexes[counter].addonbundles = fileIndexes[counter].addonbundles.Replace("N/A", "");
                                }
                                else
                                {
                                    List<string> at = fileIndexes[counter].features.Split('.').ToList();
                                    if (at.Contains(dataRow[0].ToString().Trim()))
                                    {
                                        // allThings[k].releasable = allThings[k].releasable + dataRow[0].ToString().Trim() + ".";
                                    }
                                    else
                                    {
                                        fileIndexes[counter].features = fileIndexes[counter].features + dataRow[0].ToString().Trim() + ".";
                                        if (dataRow[1].ToString().Trim() != string.Empty && dataRow[1].ToString().Trim() == "Yes")
                                        {
                                            fileIndexes[counter].count = "Yes";
                                            fileIndexes[counter].billable = fileIndexes[counter].billable + dataRow[0].ToString().Trim() + ".";
                                        }
                                        else
                                        {
                                            fileIndexes[counter].releasable = fileIndexes[counter].releasable + dataRow[0].ToString().Trim() + ".";
                                        }
                                        fileIndexes[counter].counter++;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string h = ex.Message;
                    }
                    fileIndex.index = counter + 1;
                    counter++;                    
                }

                DataTable consignTable = new DataTable();
                consignTable.Columns.Add("SNO");
                consignTable.Columns.Add("Module");
                consignTable.Columns.Add("Sub-Module");
                consignTable.Columns.Add("Extended Sub-Module");
                consignTable.Columns.Add("Release notes Keywords");
                consignTable.Columns.Add("Action");
                consignTable.Columns.Add("Features");
                consignTable.Columns.Add("Billing Components");
                consignTable.Columns.Add("Add-on Modules");
                consignTable.Columns.Add("Add-on Bundles");
                consignTable.Columns.Add("Billable Keywords");
                consignTable.Columns.Add("Description");

                foreach (SuiteGuide allThing in fileIndexes)
                {
                    if (allThing.billable.Contains(".") && allThing.releasable.Trim() != string.Empty)
                    {
                        allThing.releasable = string.Join(".", allThing.releasable.Split('.').ToList().Except(allThing.billable.Split('.').ToList()));
                    }
                    allThing.description = RemoveSpecialCharacters(allThing.description);
                    consignTable.Rows.Add(allThing.index.ToString(), allThing.module, allThing.submodule, allThing.extendedsubmodule, allThing.releasable.ToString(), allThing.count, allThing.features, allThing.billingcomponents, allThing.addonmodules, allThing.addonbundles, allThing.billable.ToString(), allThing.description);
                }

                //consignTable.TableName = "Detailed Sheet";
                consignTable.TableName = "Detailed";

                DataTable sumTable = new DataTable();
                sumTable.Columns.Add("SNO");
                sumTable.Columns.Add("Module");
                sumTable.Columns.Add("Sub-Module");
                sumTable.Columns.Add("Extended Sub-Module");
                sumTable.Columns.Add("Release notes Keywords");
                sumTable.Columns.Add("Action");
                sumTable.Columns.Add("Billable Keywords");
                sumTable.Columns.Add("Description");
                //sumTable.Columns.Add("Detail"); 
                //sumTable.Columns.Add("Module"); 
                //sumTable.Columns.Add("Sub-Module"); 
                //sumTable.Columns.Add("Release notes Keywords"); 


                foreach (SuiteGuide allThing in fileIndexes)
                {
                    if (allThing.billable.Contains(".") && allThing.releasable.Trim() != string.Empty)
                    {
                        allThing.releasable = string.Join(";", allThing.releasable.Split('.').ToList().Except(allThing.billable.Split('.').ToList()));
                    }
                    allThing.description = RemoveSpecialCharacters(allThing.description);
                    //sumTable.Rows.Add(allThing.index.ToString(), allThing.billable.ToString(), allThing.count, allThing.module, allThing.submodule, allThing.releasable.ToString(), allThing.description);
                    sumTable.Rows.Add(allThing.index.ToString(), allThing.module, allThing.submodule, allThing.extendedsubmodule, allThing.releasable.ToString(), allThing.count, allThing.billable.ToString(), allThing.description);
                }

                //sumTable.TableName = "Summary";
                sumTable.TableName = "Summary";

                string DestinationSummary = Path.Combine(Directory.GetCurrentDirectory(), StringHelper.uploads, "NSReleaseNotes_DetailSheet_2020.2.0_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xlsx");
                string DestinationDetailedsheet = Path.Combine(Directory.GetCurrentDirectory(), StringHelper.uploads, "NSReleaseNotes_SummarySheet_2020.2.0_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xlsx");
                var wb1 = new XLWorkbook();
                var wb2 = new XLWorkbook();
                IXLWorksheet sheet = wb1.Worksheets.Add(consignTable);
                wb1.Worksheet(1).Columns("L").Style.Alignment.WrapText = true;
                wb1.Worksheet(1).Columns().AdjustToContents();
                wb1.Worksheet(1).Rows().AdjustToContents();
                sheet.Columns("B").Width = 20;
                sheet.Columns("C").Width = 48;
                sheet.Columns("D").Width = 57;
                sheet.Columns("E").Width = 40;
                sheet.Columns("F").Width = 10;
                sheet.Columns("G").Width = 55;
                sheet.Columns("H").Width = 20;
                sheet.Columns("I").Width = 25;
                sheet.Columns("K").Width = 47;
                sheet.Columns("L").Width = 59;
                IXLWorksheet sheet1 = wb2.Worksheets.Add(sumTable);
                wb2.Worksheet(1).Columns("H").Style.Alignment.WrapText = true;
                wb2.Worksheet(1).Columns().AdjustToContents();
                wb2.Worksheet(1).Rows().AdjustToContents();
                sheet1.Columns("B").Width = 18;
                sheet1.Columns("C").Width = 61;
                sheet1.Columns("D").Width = 60;
                sheet1.Columns("E").Width = 40;
                sheet1.Columns("F").Width = 10;
                sheet1.Columns("G").Width = 59;
                sheet1.Columns("H").Width = 59;
                wb1.SaveAs(DestinationSummary);
                wb2.SaveAs(DestinationDetailedsheet);
                // DWNLDS.AddRange(Output.AsEnumerable().Select(r => r.Field<string>("Files")).ToList());
                //DWNLDS.AddRange(Output.AsEnumerable().Select(r => r.Field<string>("Files")).ToList());
                tablu.Rows.Add(DestinationSummary);
                tablu.Rows.Add(DestinationDetailedsheet);
                ret = StringHelper.success;

            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(tablu), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public List<string> GetSubstringBasedonPositions(List<int> positions, string input)
        {

            List<string> result = new List<string>();
            int startPos = 0;
            Array.Sort(positions.ToArray());
            for (int i = 0; i < positions.Count; i++)
            {
                startPos = positions[i];
                int length;
                if (i == positions.Count - 1)
                {
                    length = input.Length - positions[i];
                }
                else
                {
                    length = positions[i + 1] - positions[i];
                }
                result.Add(input.Substring(startPos, length));
            }
            return result;
        }

        public List<int> AllIndexesOf(string str, string value)
        {
            if (String.IsNullOrEmpty(value))
                throw new ArgumentException("the string to find may not be empty", "value");
            List<int> indexes = new List<int>();
            for (int index = 0; ; index += value.Length)
            {
                index = str.IndexOf(value, index);
                if (index == -1)
                    return indexes;
                indexes.Add(index);
            }
        }
        public class SuiteGuide
        {
            public int index { get; set; }
            public string instance { get; set; }
            public string module { get; set; }
            public string submodule { get; set; }
            public string extendedsubmodule { get; set; }
            public string description { get; set; }
            public string features { get; set; }
            public string billingcomponents { get; set; }
            public string addonmodules { get; set; }
            public string addonbundles { get; set; }
            public string count { get; set; }
            public int counter { get; set; }
            public string category { get; set; }
            public string billable { get; set; }
            public string releasable { get; set; }
        }
        public string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_., :;]+", "", RegexOptions.Compiled);
        }
        public string Truncate(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }



    }
}