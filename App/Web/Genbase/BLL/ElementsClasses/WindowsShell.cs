﻿using Genbase.classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Security;
using System.ServiceProcess;
using System.Text;
using Genbase.Classes;

namespace Genbase.BLL.ElementsClasses
{
    public class WindowsShell
    {
        StepPropertyHandler stp = new StepPropertyHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult Connection( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            DataTable connect = new DataTable();
            connect.Columns.Add(StringHelper.URL);
            connect.Columns.Add(StringHelper.Username);
            connect.Columns.Add(StringHelper.Password);

            string Username = string.Empty; string Password = string.Empty; string Input = string.Empty; string Output = string.Empty;
            string Ip = string.Empty;
            try
            {
                Username = stp.getPropertiesValue(StringHelper.username, step);
                Password = stp.getPropertiesValue(StringHelper.password, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                Ip = stp.getPropertiesValue(StringHelper.ip, step);

                ShellConnection sc = new ShellConnection();
                sc.ipname = Ip;

                sc.pswd = Password;
                sc.uname = Username;
                sc.ostype = StringHelper.window;
                sc.subostype = null;

                connect.Rows.Add(Ip, Username, Password);
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(connect), "", true, "class", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables,s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null,s);
            }
            return mr;
        }
        public MethodResult Execute( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            DataTable resultDt = new DataTable();
            string Command = string.Empty; string Input = string.Empty; string Output = string.Empty; string Filter = string.Empty; List<EventLogsDesc> abcd = new List<EventLogsDesc>();
            string Timeline = string.Empty; string Domain = string.Empty;
            try
            {

                Command = stp.getPropertiesValue(StringHelper.command, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                Filter = stp.getPropertiesValue(StringHelper.filter, step);
                Timeline = stp.getPropertiesValue(StringHelper.timeline, step);
                Domain = stp.getPropertiesValue(StringHelper.domain, step);

                DVariables value = new DVariables();
                if (Input.Trim() != string.Empty)
                {
                    value = new VariableHandler().getVariables(Input, executionID);
                }

                DataTable connect = new TypeHandler().getInput(Input, StringHelper.variable, executionID);

                string Text = string.Empty; string outer = string.Empty; string noanswer = string.Empty;
                SecureString password = new SecureString();
                string stringdtr = string.Empty;
                string runasUsername = connect.Rows[0].Field<string>(StringHelper.Username).Trim();
                string runasPassword = connect.Rows[0].Field<string>(StringHelper.Password).Trim();
                Dictionary<dynamic, dynamic> xx = new Dictionary<dynamic, dynamic>();


                foreach (char x in runasPassword)
                {
                    password.AppendChar(x);
                }
                PSCredential credential = new PSCredential(runasUsername, password);
                WSManConnectionInfo connectionInfo = new WSManConnectionInfo(false, connect.Rows[0].Field<string>(StringHelper.URL).Trim(), 5985, "/wsman", "http://schemas.microsoft.com/powershell/Microsoft.PowerShell", credential, 1 * 60 * 1000);
                Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo);
                runspace.Open();
                using (PowerShell ps = PowerShell.Create())
                {

                    ps.Runspace = runspace;
                    ps.AddScript(Command);
                    var results = ps.Invoke();

                    //int i = 0;
                    var prop = new List<string>();
                    resultDt.Columns.Add("0");                      
                    foreach (var str in results)
                    {
                        resultDt.Rows.Add(str);

                    }
                    noanswer = new TypeHandler().StringtoJsonD(stringdtr);
                    Text = step.Name + StringHelper.completed;
                }
                if (Text.Trim() != string.Empty && stringdtr != string.Empty)
                {
                    new ExecutionLog().add2Log(Text, step.Robot_Id, step.Name, StringHelper.eventcode,false);
                    new ExecutionLog().add2Log(stringdtr, step.Robot_Id, step.Name, StringHelper.eventcode,false);
                    new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(resultDt), "", true, "datatable", step.Robot_Id, executionID);
                    ret = StringHelper.success;
                }
                else
                {
                    new ExecutionLog().add2Log(StringHelper.sheeliswrong, step.Robot_Id, step.Name, StringHelper.exceptioncode,false);
                }
                runspace.Close();

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex, "Robot ID:-" + step.Robot_Id, "Step ID:-" + step.Action_Id + " Step Name:-" + step.Name, StringHelper.exceptioncode);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables,s);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(resultDt), "", true, "datatable", step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null,s);
            }
            return mr;
        }
        public MethodResult ServiceManagement( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string Command = string.Empty; string Input = string.Empty; string Output = string.Empty;
            try
            {

                Command = stp.getPropertiesValue(StringHelper.command, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);

                DVariables value = new DVariables();
                if (Input.Trim() != string.Empty)
                {
                    value = new VariableHandler().getVariables(Input, executionID);
                }
                DataTable connect = new TypeHandler().getInput(Input, StringHelper.variable, executionID);

                if (connect.Rows[0].Field<string>(StringHelper.URL).Trim() != new CurrentWindowsSystemFacts().getSystemDetails().IPAddress.Trim())
                {

                    string Text = string.Empty; string outer = string.Empty; string noanswer = string.Empty;
                    SecureString password = new SecureString();
                    string stringdtr = string.Empty;
                    string runasUsername = connect.Rows[0].Field<string>(StringHelper.Username).Trim();
                    string runasPassword = connect.Rows[0].Field<string>(StringHelper.Password).Trim();
                    foreach (char x in runasPassword)
                    {
                        password.AppendChar(x);
                    }
                    PSCredential credential = new PSCredential(runasUsername, password);
                    WSManConnectionInfo connectionInfo = new WSManConnectionInfo(false, connect.Rows[0].Field<string>(StringHelper.URL).Trim(), 5985, "/wsman", "http://schemas.microsoft.com/powershell/Microsoft.PowerShell", credential, 1 * 60 * 1000);
                    Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo);
                    runspace.Open();
                    using (PowerShell ps = PowerShell.Create())
                    {
                        Pipeline pipeline = runspace.CreatePipeline();
                        pipeline.Commands.AddScript(Command);
                        pipeline.Commands.Add(StringHelper.outstring);

                        // execute the script

                        Collection<PSObject> results = pipeline.Invoke();

                        // close the runspace

                        runspace.Close();

                        // convert the script result into a single string

                        StringBuilder stringBuilder = new StringBuilder();
                        foreach (PSObject obj in results)
                        {
                            stringBuilder.AppendLine(obj.ToString());
                        }

                        noanswer = stringBuilder.ToString();

                        List<string> noanswerlist = noanswer.Split(new string[] { "\r\n" }, StringSplitOptions.None).ToList();
                        DataTable dtr = new DataTable();

                        noanswerlist.RemoveAll(x => x.IndexOf("----") > -1);
                        noanswerlist.RemoveAll(x => x.Trim() == string.Empty);
                        dtr.Columns.Add(" ");
                        dtr.Columns.Add("  ");

                        for (int i = 0; i < noanswerlist.Count; i++)
                        {
                            List<string> items = noanswerlist[i].Split().ToList();
                            items.RemoveAll(x => x.Trim() == string.Empty);
                            for (int k = 0; k < items.Count; k++)
                            {
                                if (i == 0)
                                {
                                    dtr.Columns.Add(items[k]);
                                }
                                else
                                {
                                    int colcnt = dtr.Columns.Count;
                                    List<string> datarow = new List<string>();
                                    datarow.Add("");
                                    datarow.Add("");
                                    for (int h = 0; h < items.Count; h++)
                                    {
                                        if (items[h].Trim() != string.Empty && h < colcnt - 3)
                                        {
                                            datarow.Add(items[h].Trim());
                                        }
                                        else if (items[h].Trim() != string.Empty && h == colcnt - 3)
                                        {
                                            List<string> sublist = items.GetRange(h, items.Count - h);
                                            string substring = string.Join(" ", sublist);
                                            datarow.Add(substring);
                                        }
                                    }
                                    if (datarow != null)
                                    {
                                        dtr.Rows.Add(datarow.ToArray());
                                    }
                                }
                            }
                        }
                        string dtrstring = JsonConvert.SerializeObject(dtr);
                        noanswer = dtrstring;

                        string het = new TypeHandler().ConvertDataTableToString(dtr);
                        new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
                    }
                    if (noanswer.Trim() != string.Empty)
                    {
                        new ExecutionLog().add2Log(noanswer, step.Robot_Id, step.Name, StringHelper.eventcode, false);
                        new VariableHandler().CreateVariable(Output, noanswer, "", true, "datatable", step.Robot_Id, executionID);
                        ret = StringHelper.success;
                    }
                    else
                    {
                        new ExecutionLog().add2Log(StringHelper.sheeliswrong, step.Robot_Id, step.Name, StringHelper.exceptioncode, false);
                    }
                    runspace.Close();
                    ret = StringHelper.success;
                }
                else
                {
                    string ser_status = new CurrentWindowsSystemFacts().thisSystemServices(Command);

                    DataTable ftr = new DataTable();
                    ftr.Columns.Add(StringHelper.Service); ftr.Columns.Add(StringHelper.ServiceStatus);
                    ftr.Rows.Add(Command, ser_status);

                    string noanswer = JsonConvert.SerializeObject(ftr);

                    new VariableHandler().CreateVariable(Output, noanswer, "", true, "class", step.Robot_Id, executionID);

                    string het = new TypeHandler().ConvertDataTableToString(ftr);
                    new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);

                    ret = StringHelper.success;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables,s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null,s);
            }
            return mr;
        }
        public MethodResult GetLogs( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>(); List<EventLogsDesc> abcd = new List<EventLogsDesc>();
            string Command = string.Empty; string Input = string.Empty; string Output = string.Empty; string Filter = string.Empty;
            DataTable resultDt = new DataTable();
            try
            {

                Command = stp.getPropertiesValue(StringHelper.command, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                Filter = stp.getPropertiesValue(StringHelper.filter, step);

                DVariables value = new DVariables();
                if (Input.Trim() != string.Empty)
                {
                    value = new VariableHandler().getVariables(Input, executionID);
                }
                DataTable connect = new TypeHandler().getInput(Input, StringHelper.variable, executionID);

                string Text = string.Empty;
                SecureString password = new SecureString();
                string noanswer = string.Empty;
                string stringdtr = string.Empty;
                string runasUsername = connect.Rows[0].Field<string>(StringHelper.Username).Trim();
                string runasPassword = connect.Rows[0].Field<string>(StringHelper.Password).Trim();

                foreach (char x in runasPassword)
                {
                    password.AppendChar(x);
                }
                PSCredential credential = new PSCredential(runasUsername, password);
                WSManConnectionInfo connectionInfo = new WSManConnectionInfo(false, connect.Rows[0].Field<string>(StringHelper.URL).Trim(), 5985, "/wsman", "http://schemas.microsoft.com/powershell/Microsoft.PowerShell", credential, 1 * 60 * 1000);
                Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo);
                runspace.Open();

                using (PowerShell ps = PowerShell.Create())
                {
                    ps.Runspace = runspace;
                    ps.AddScript(Command);
                    var results = ps.Invoke();
                    int i = 0;
                    var prop = new List<string>();
                   
                    foreach (var pr in prop)
                    {
                        stringdtr = stringdtr + pr + " ";
                    }
                    stringdtr = stringdtr + "\n";
                    foreach (var str in results)
                    {
                        if (prop.Count == 0)
                        {
                            foreach (var mem in str.Members)
                            {
                                resultDt.Columns.Add(mem.Name);
                                prop.Add(mem.Name);
                            }
                            foreach (var pr in prop)
                            {
                                stringdtr = stringdtr + pr + " ";
                            }
                            stringdtr = stringdtr + "\n";
                        }
                        List<string> r = new List<string>();
                        foreach (var pr in prop)
                        {

                            string sProp = "";
                            if (str.Members[pr] != null && str.Members[pr].Value != null)
                            {
                                sProp = str.Members[pr].Value.ToString();
                                if (sProp.IndexOf(' ') > -1)
                                {
                                    sProp = sProp.Replace(" ", "");
                                    if (string.IsNullOrEmpty(sProp))
                                    {
                                        sProp = StringHelper.no;
                                    }
                                }
                                stringdtr = stringdtr + sProp + " ";
                            }
                            else if (str.Members[pr] != null && (str.Members[pr].Value == null))
                            {
                                sProp = StringHelper.no;
                                stringdtr = stringdtr + StringHelper.no + " ";
                            }
                            else
                            {
                                sProp = StringHelper.no;
                                stringdtr = stringdtr + StringHelper.no + " ";
                            }
                            r.Add(sProp);
                        }
                        resultDt.Rows.Add(r.ToArray<string>());
                        stringdtr = stringdtr + "\n";
                        i++;
                    }
                    noanswer = new TypeHandler().StringtoJsonD(stringdtr);

                    Text = step.Name + StringHelper.completed;
                    if (Text.Trim() != string.Empty && stringdtr != string.Empty)
                    {
                        new ExecutionLog().add2Log(Text, step.Robot_Id, step.Name, StringHelper.eventcode,false);
                        new ExecutionLog().add2Log(stringdtr, step.Robot_Id, step.Name, StringHelper.eventcode,false);
                        new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(resultDt), "", true,"datatable", step.Robot_Id, executionID);
                        ret = StringHelper.success;
                    }
                    else
                    {
                        new ExecutionLog().add2Log(StringHelper.sheeliswrong, step.Robot_Id, step.Name, StringHelper.exceptioncode,false);
                    }
                    runspace.Close();

                    ret = StringHelper.success;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables,s);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(resultDt), "", true,"datatable", step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null,s);
            }
            return mr;
        }
        public class ReplacementStrings
        {
            public string CliXml { get; set; }
        }
        public string prop()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                var prop = new List<string> { "Name", "SI","Handles","VM","WS","PM","NPM","Path","Company","CPU","FileVersion","ProductVersion","Description",
                              "Product", "__NounName","BasePriority","HasExited","Handle","SafeHandle","HandleCount","Id","MachineName","MainWindowHandle","MainWindowTitle",
                              "MainModule","MaxWorkingSet","MinWorkingSet", "NonpagedSystemMemorySize","NonpagedSystemMemorySize64","PagedMemorySize","PagedMemorySize64",
                              "PagedSystemMemorySize","PagedSystemMemorySize64","PeakPagedMemorySize","PeakPagedMemorySize64","PeakWorkingSet",
                              "PeakWorkingSet64", "PeakVirtualMemorySize","PeakVirtualMemorySize64","PriorityBoostEnabled","PriorityClass","PrivateMemorySize","PrivateMemorySize64",
                              "ProcessName","ProcessorAffinity","Responding","SessionId","StartInfo","StartTime",
                              "SynchronizingObject","VirtualMemorySize","VirtualMemorySize64","EnableRaisingEvents","WorkingSet","WorkingSet64",
                               "Site","Container"
                            };
                string x = JsonConvert.SerializeObject(prop);
                return x;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return string.Empty;
            }
        }
        public class EventLogsDesc
        {
            public string Name { get; set; }
            public string SI { get; set; }
            public string Handles { get; set; }
            public string VM { get; set; }
            public string WS { get; set; }
            public string PM { get; set; }
            public string NPM { get; set; }
            public string Path { get; set; }
            public string Company { get; set; }
            public string CPU { get; set; }
            public string FileVersion { get; set; }
            public string ProductVersion { get; set; }
            public string Description { get; set; }
            public string Product { get; set; }
            public string __NounName { get; set; }
            public string BasePriority { get; set; }
            public bool HasExited { get; set; }
            public string Handle { get; set; }
            public string SafeHandle { get; set; }
            public string HandleCount { get; set; }
            public string Id { get; set; }
            public string MachineName { get; set; }
            public string MainWindowHandle { get; set; }
            public string MainWindowTitle { get; set; }
            public string MainModule { get; set; }
            public string MaxWorkingSet { get; set; }
            public string MinWorkingSet { get; set; }
            public Modules Modules { get; set; }
            public string NonpagedSystemMemorySize { get; set; }
            public string NonpagedSystemMemorySize64 { get; set; }
            public string PagedMemorySize { get; set; }
            public string PagedMemorySize64 { get; set; }
            public string PagedSystemMemorySize { get; set; }
            public string PagedSystemMemorySize64 { get; set; }
            public string PeakPagedMemorySize { get; set; }
            public string PeakPagedMemorySize64 { get; set; }
            public string PeakWorkingSet { get; set; }
            public string PeakWorkingSet64 { get; set; }
            public string PeakVirtualMemorySize { get; set; }
            public string PeakVirtualMemorySize64 { get; set; }
            public bool PriorityBoostEnabled { get; set; }
            public string PriorityClass { get; set; }
            public string PrivateMemorySize { get; set; }
            public string PrivateMemorySize64 { get; set; }
            public string PrivilegedProcessorTime { get; set; }
            public string ProcessName { get; set; }
            public string ProcessorAffinity { get; set; }
            public bool Responding { get; set; }
            public string SessionId { get; set; }
            public string StartInfo { get; set; }
            public DateTime StartTime { get; set; }
            public object SynchronizingObject { get; set; }
            public Threads Threads { get; set; }
            public string TotalProcessorTime { get; set; }
            public string UserProcessorTime { get; set; }
            public string VirtualMemorySize { get; set; }
            public string VirtualMemorySize64 { get; set; }
            public bool EnableRaisingEvents { get; set; }
            public string WorkingSet { get; set; }
            public string WorkingSet64 { get; set; }
            public object Site { get; set; }
            public object Container { get; set; }
            public string EventID { get; set; }
            public string Data { get; set; }
            public string Index { get; set; }
            public string Category { get; set; }
            public string CategoryNumber { get; set; }
            public string EntryType { get; set; }
            public string Message { get; set; }
            public string Source { get; set; }
            public ReplacementStrings ReplacementStrings { get; set; }
            public string InstanceId { get; set; }
            public DateTime TimeGenerated { get; set; }
            public DateTime TimeWritten { get; set; }
            public string UserName { get; set; }
        }
        public class Modules
        {
            public string CliXml { get; set; }
        }
        public class Threads
        {
            public string CliXml { get; set; }
        }
        class PeopleList
        {
            public List<string> inputs { get; set; }
        }

    }

}
