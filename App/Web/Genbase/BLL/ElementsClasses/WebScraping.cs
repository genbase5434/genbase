﻿using Genbase.classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Genbase.Classes;
using System.Text;
using System.Data;

namespace Genbase.BLL.ElementsClasses
{
    public class WebScraping
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult Extract( Steps step, string executionID)
        {
            string page = string.Empty; string xpath = string.Empty; string inkludeHTML = string.Empty;
            string Condition = string.Empty, ConditionXpath = string.Empty, RatingXpath = string.Empty, MoreXpath = string.Empty, ConditionNumber = string.Empty;
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string output = string.Empty;
            try
            {

                page = stp.getPropertiesValue(StringHelper.url, step);
                xpath = stp.getPropertiesValue(StringHelper.xpath, step);
                inkludeHTML = stp.getPropertiesValue(StringHelper.includehtml, step);
                Condition = stp.getPropertiesValue(StringHelper.condition, step);
                ConditionXpath = stp.getPropertiesValue(StringHelper.conditionxpath, step);
                RatingXpath = stp.getPropertiesValue(StringHelper.ratingxpath, step);
                MoreXpath = stp.getPropertiesValue(StringHelper.morexpath, step);
                ConditionNumber = stp.getPropertiesValue(StringHelper.conditionnumber, step);
                output = stp.getPropertiesValue(StringHelper.output, step);

                WebClient webClient = new WebClient();
                page = webClient.DownloadString(page);
                if (page.Trim() != string.Empty)
                {
                    HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                    doc.LoadHtml(page);
                    List<string> tabs = new List<string>();
                    if (inkludeHTML.ToLower() == StringHelper.truee)
                    {
                        tabs = doc.DocumentNode.SelectNodes(xpath).Select(x => x.InnerHtml).ToList();
                    }
                    else
                    {
                        tabs = doc.DocumentNode.SelectNodes(xpath).Select(x => x.InnerText).ToList();
                    }
                    RegexOptions options = RegexOptions.None;
                    Regex regex = new Regex("[ ]{2,}", options);

                    System.Data.DataTable dts = new System.Data.DataTable();
                    //dts.Columns.Add("");
                    dts.Columns.Add(StringHelper.title);
                    dts.Columns.Add(StringHelper.description);
                    dts.Columns.Add(StringHelper.namee);
                    dts.Columns.Add(StringHelper.ratings);
                    dts.Columns.Add("Status");
                    var headings = doc.DocumentNode.SelectNodes("//*[contains(@class,'noQuotes')]");
                    var bodies = doc.DocumentNode.SelectNodes("//*[contains(@class,'partial_entry')]");
                    var ratings = doc.DocumentNode.SelectNodes("//span[contains(@class,'ui_bubble_rating')]");
                    var names = doc.DocumentNode.SelectNodes("//div[contains(@class,'info_text')]");

                    List<string> bodiex = new List<string>();

                    for (int f = 0; f < bodies.Count; f++)
                    {
                        if (f == 0)
                        {
                            bodiex.Add(bodies[f].InnerText);
                        }
                        else if (f % 2 > 0)
                        {

                        }
                        else
                        {
                            bodiex.Add(bodies[f].InnerText);
                        }
                    }

                    for (int f = 0; f < headings.Count; f++)
                    {
                        var rClasses = ratings[f + 2].Attributes;
                        string rating = string.Empty;
                        foreach (var rClas in rClasses)
                        {
                            if (rClas.Name.ToLower() == StringHelper.cclass)
                            {
                                if (rClas.Value.IndexOf("50") > -1)
                                {
                                    rating = "5";
                                }
                                else if (rClas.Value.IndexOf("40") > -1)
                                {
                                    rating = "4";
                                }
                                else if (rClas.Value.IndexOf("30") > -1)
                                {
                                    rating = "3";
                                }
                                else if (rClas.Value.IndexOf("20") > -1)
                                {
                                    rating = "2";
                                }
                                else if (rClas.Value.IndexOf("10") > -1)
                                {
                                    rating = "1";
                                }
                                else if (rClas.Value.IndexOf("15") > -1)
                                {
                                    rating = "1.5";
                                }
                                else if (rClas.Value.IndexOf("35") > -1)
                                {
                                    rating = "3.5";
                                }
                                else if (rClas.Value.IndexOf("25") > -1)
                                {
                                    rating = "2.5";
                                }
                                else if (rClas.Value.IndexOf("45") > -1)
                                {
                                    rating = "4.5";
                                }
                                else
                                {
                                    rating = "0";
                                }
                            }
                        }
                        if (f < bodiex.Count && f < names.Count)
                        {
                            dts.Rows.Add(headings[f].InnerText, bodiex[f], names[f].InnerText, rating, true);
                        }

                    }


                    for (int t = 1; t < int.Parse(ConditionNumber); t++)
                    {
                        string path = Condition.Replace("{v}", (t * 5).ToString());
                        WebClient webClientAlt = new WebClient();
                        string pageAlt = webClientAlt.DownloadString(path);
                        if (pageAlt.Trim() != string.Empty)
                        {
                            HtmlAgilityPack.HtmlDocument docAlt = new HtmlAgilityPack.HtmlDocument();
                            docAlt.LoadHtml(pageAlt);
                            List<string> tabsAlt = new List<string>();
                            if (inkludeHTML.ToLower() == StringHelper.truee)
                            {
                                tabsAlt = doc.DocumentNode.SelectNodes(xpath).Select(x => x.InnerHtml).ToList();
                            }
                            else
                            {
                                tabsAlt = doc.DocumentNode.SelectNodes(xpath).Select(x => x.InnerText).ToList();
                            }
                            //RegexOptions optionsAlt = RegexOptions.None;
                            Regex regexAlt = new Regex("[ ]{2,}", options);


                            var headingsAlt = docAlt.DocumentNode.SelectNodes("//*[contains(@class,'noQuotes')]");
                            var bodiesAlt = docAlt.DocumentNode.SelectNodes("//*[contains(@class,'partial_entry')]");
                            var ratingsAlt = docAlt.DocumentNode.SelectNodes("//span[contains(@class,'ui_bubble_rating')]");
                            var namesAlt = docAlt.DocumentNode.SelectNodes("//div[contains(@class,'info_text')]");

                            List<string> bodiexAlt = new List<string>();

                            for (int f = 0; f < bodiesAlt.Count; f++)
                            {
                                if (f == 0)
                                {
                                    bodiexAlt.Add(bodiesAlt[f].InnerText);
                                }
                                else if (f % 2 > 0)
                                {

                                }
                                else
                                {
                                    bodiexAlt.Add(bodiesAlt[f].InnerText);
                                }
                            }

                            for (int f = 0; f < headingsAlt.Count; f++)
                            {
                                var rClasses = ratingsAlt[f + 2].Attributes;
                                string rating = string.Empty;
                                foreach (var rClas in rClasses)
                                {
                                    if (rClas.Name.ToLower() == StringHelper.cclass)
                                    {
                                        if (rClas.Value.IndexOf("50") > -1)
                                        {
                                            rating = "5";
                                        }
                                        else if (rClas.Value.IndexOf("40") > -1)
                                        {
                                            rating = "4";
                                        }
                                        else if (rClas.Value.IndexOf("30") > -1)
                                        {
                                            rating = "3";
                                        }
                                        else if (rClas.Value.IndexOf("20") > -1)
                                        {
                                            rating = "2";
                                        }
                                        else if (rClas.Value.IndexOf("10") > -1)
                                        {
                                            rating = "1";
                                        }
                                        else if (rClas.Value.IndexOf("15") > -1)
                                        {
                                            rating = "1.5";
                                        }
                                        else if (rClas.Value.IndexOf("35") > -1)
                                        {
                                            rating = "3.5";
                                        }
                                        else if (rClas.Value.IndexOf("25") > -1)
                                        {
                                            rating = "2.5";
                                        }
                                        else if (rClas.Value.IndexOf("45") > -1)
                                        {
                                            rating = "4.5";
                                        }
                                        else
                                        {
                                            rating = "0";
                                        }
                                    }
                                }


                                string head = string.Empty; string body = string.Empty; string name = string.Empty;
                                if (f < headingsAlt.Count)
                                {
                                    head = headingsAlt[f].InnerText;
                                }
                                else
                                {
                                    head = "";
                                }
                                if (f < bodiexAlt.Count)
                                {
                                    body = bodiexAlt[f];
                                }
                                else
                                {
                                    body = "";
                                }
                                if (f < namesAlt.Count)
                                {
                                    name = namesAlt[f].InnerText;
                                }
                                else
                                {
                                    name = "";
                                }


                                dts.Rows.Add(head, body, name, rating, true);
                            }
                        }
                    }

                    foreach (string tab in tabs)
                    {
                        string text = tab;
                        text = regex.Replace(text, " ").Trim();

                        log.add2Log("\n" + text, step.Robot_Id, StringHelper.extract, StringHelper.eventcode,false);
                        if (output != string.Empty)
                        {
                            new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(dts), "", true, "datatable", step.Robot_Id, executionID);
                        }
                    }
                }
                else
                {
                    log.add2Log(StringHelper.urlplease, step.Robot_Id, StringHelper.extract, StringHelper.exceptioncode,false);
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;

        }
        public MethodResult Selector(Steps step, string executionID)
        {
            string ret = string.Empty;
            string Input = string.Empty; string Output = string.Empty; string FinalText = string.Empty;
            DataTable returnValue = new DataTable();

            returnValue.Columns.Add("URL");
            returnValue.Columns.Add("Content");

            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                string Tag = stp.getPropertiesValue("tag", step);
                string Selector = stp.getPropertiesValue("selector", step);
                string SelectorType = stp.getPropertiesValue("selectortype", step);

                try
                {
                    WebClient webClient = new WebClient();
                    if (new VariableHandler().checkVariableExists(Input,executionID))
                    {
                        DataTable dtr = new TypeHandler().getInput(Input, executionID);
                        Input = dtr.Rows[0]["URL"].ToString();
                    }

                    string page = webClient.DownloadString(Input);
                    if (page.Trim() != string.Empty)
                    {
                        HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                        doc.LoadHtml(page);

                        
                        if(Tag.Trim() == "*" || Tag.Trim().ToLower() == "body" || Tag.Trim().ToLower() == "document")
                        {
                            var root = doc.DocumentNode;
                            var sb = new StringBuilder();
                            foreach (var node in root.DescendantsAndSelf())
                            {
                                if (!node.HasChildNodes)
                                {
                                    string text = node.InnerText;
                                    if (!string.IsNullOrEmpty(text))
                                        sb.AppendLine(text.Trim());
                                }
                            }
                            FinalText = sb.ToString();
                        }
                        else
                        {
                            var root = doc.DocumentNode;
                            string itemName = string.Empty;int itemNumber = -1;

                            if(SelectorType.Trim().ToLower() != "xpath")
                            {
                                if (Selector.IndexOf("[") > -1 && Selector.IndexOf("]") > -1)
                                {
                                    itemName = Selector.Split('[')[0].Trim();
                                    itemNumber = Convert.ToInt32(Selector.Split('[')[1].Split(']')[0].Trim().Replace("\"", "").Replace("'", ""));
                                }
                                else
                                {
                                    itemName = Selector;
                                }
                            }
                            else
                            {
                                itemName = Selector;
                            }

                            if (SelectorType.Trim().ToLower() == "id")
                            {
                                HtmlAgilityPack.HtmlNode hn = doc.GetElementbyId(Selector);
                                FinalText = hn.InnerText;
                            }
                            else if(SelectorType.Trim().ToLower() == "xpath")
                            {
                                FinalText = string.Join("\n", root.SelectNodes(Selector).Select(x => x.InnerText).ToList());
                            }
                            else
                            {
                                List<HtmlAgilityPack.HtmlNode> hnodes = root.Descendants(Tag).ToList();

                                if (itemNumber == -1)
                                {
                                    foreach (var hnode in hnodes)
                                    {
                                        if (hnode.Attributes[SelectorType] != null && hnode.Attributes[SelectorType].Value.ToLower().Trim() == itemName.Trim().ToLower())
                                        {
                                            FinalText = "\n" + hnode.InnerText;
                                        }
                                    }
                                }
                                else
                                {
                                    List<HtmlAgilityPack.HtmlNode> Filteredhnodes = hnodes.FindAll(hnode => hnode.Attributes[SelectorType] != null && hnode.Attributes[SelectorType].Value.ToLower().Trim() == itemName.Trim().ToLower());
                                    FinalText = Filteredhnodes[itemNumber].InnerText;
                                }
                            }
                        }

                    }

                    returnValue.Rows.Add(Input, FinalText);
                    ret = StringHelper.success;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                }
                MethodResult mr;
                if (ret == StringHelper.success)
                {
                    string het = new TypeHandler().ConvertDataTableToString(returnValue);
                    new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                    mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                    new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
                }
                else
                {
                    mr = new MethodResultHandler().createResultType(false, msgs, null, s);
                }
                return mr;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                return null;
            }
        }
    }
}