﻿using Genbase.classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using Genbase.Classes;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;

namespace Genbase.BLL.ElementsClasses
{
    public class HRMS
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        VariableHandler vh = new VariableHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult CreateUser(Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty;
            string ServiceURL = string.Empty; string UserName = string.Empty; string Password = string.Empty; string Operation = string.Empty;
            string Input = string.Empty; string Output = string.Empty; DataTable otpt = new DataTable(); string pdfPath = String.Empty;
            try
            {

                ServiceURL = stp.getPropertiesValue(StringHelper.serviceurl, step);
                UserName = stp.getPropertiesValue(StringHelper.username, step);
                Password = stp.getPropertiesValue(StringHelper.password, step);
                Operation = stp.getPropertiesValue(StringHelper.operation, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);

                System.Data.DataTable inpt = new System.Data.DataTable();
                List<Dictionary<string, dynamic>> inputArray1 = new List<Dictionary<string, dynamic>>();
                List<string> orcas = new List<string>();

                if (vh.checkVariableExists(Input.Trim(), executionID))
                {
                    inpt = new TypeHandler().getInput(Input, StringHelper.variable, executionID);

                    inputArray1 = inpt.AsEnumerable().Select(row => inpt.Columns.Cast<DataColumn>().ToDictionary(column => column.ColumnName, column => row[column] as dynamic)).ToList();

                }

                SqlServerConnection csql = new SqlServerConnection(ServiceURL, "3306", "ep_hrms", UserName, Password);
                //List<string> orcas = inpt.AsEnumerable().Select(dr => dr.Field<string>("Description")).ToList();

                foreach (DataRow dr in inpt.Rows)
                {
                    string firstname = string.Empty;
                    string name = string.Empty;
                    string lastname = string.Empty;
                    string dob = string.Empty;
                    string contactno = string.Empty;
                    string emailid = string.Empty;
                    string emailpswd = string.Empty;
                    string hrmsid = string.Empty;
                    string slackid = string.Empty;
                    string slackpswd = string.Empty;
                    string adname = string.Empty;
                    string username = string.Empty;
                    string orca = string.Empty;

                    if (dr.ItemArray[0].ToString().Contains("EMPLOYEE INFORMATION"))
                    {
                        foreach (var dict in inputArray1)
                        {
                            if (dict.Keys.Contains("file"))
                            {
                                var pdfReaderPath = dict["file"];
                                string pdfRead = pdfReaderPath.ToString();
                                var startTag = "First:"; int startIndex = pdfRead.IndexOf(startTag) + startTag.Length;
                                int endIndex = pdfRead.IndexOf("Middle:", startIndex);
                                firstname = pdfRead.Substring(startIndex, endIndex - startIndex);
                                firstname = Regex.Replace(firstname, @"\t|\n|\r", "");
                                startTag = "Last:"; startIndex = pdfRead.IndexOf(startTag) + startTag.Length;
                                endIndex = pdfRead.IndexOf("Title:", startIndex);
                                lastname = pdfRead.Substring(startIndex, endIndex - startIndex);
                                lastname = Regex.Replace(lastname, @"\t|\n|\r", "");
                            }
                        }
                    }
                    else
                    {
                        orca = dr["Description"].ToString();
                        if (orca.ToLower().IndexOf(StringHelper.requestforeob) > -1 || orca.ToLower().IndexOf(StringHelper.eob) > -1)
                        {
                            string desc = orca;
                            List<string> slashns = desc.Split(new string[] { "\n", "\\n" }, StringSplitOptions.None).ToList();
                            foreach (string slashn in slashns)
                            {
                                try
                                {
                                    if (slashn.Trim() != string.Empty && slashn.IndexOf(":") > -1)
                                    {
                                        string fpart = slashn.Split(':')[0].Trim();
                                        string spart = slashn.Split(':')[1].Trim();

                                        if (fpart.ToLower().Trim() == StringHelper.firstname)
                                        {
                                            firstname = spart;
                                        }
                                        else if (fpart.ToLower().Trim() == StringHelper.lastname)
                                        {
                                            lastname = spart;
                                        }
                                        else if (fpart.ToLower().Trim() == "date of birth")
                                        {
                                            dob = spart;
                                        }
                                        else if (fpart.ToLower().Trim() == "contact number")
                                        {
                                            contactno = spart;
                                        }
                                        else if (fpart.ToLower().Trim() == StringHelper.name)
                                        {
                                            name = spart;
                                        }

                                    }
                                }
                                catch (Exception ex)
                                {
                                    new Logger().LogException(ex);
                                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                }
                            }
                        }
                    }
                    name = firstname.Trim() + " " + lastname.Trim();
                    var regexItem = new Regex("^[a-zA-Z0-9 ].*$");

                    if (name.Trim() != string.Empty && regexItem.IsMatch(name))
                    {
                        if (name.Trim().IndexOf(" ") > -1)
                        {
                            string fmail;
                            string lmail;
                            char[] seperator = { ' ' };
                            String[] fnames = firstname.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                            String[] lnames = lastname.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                            if (fnames.Length >= 2)
                            {
                                fmail = fnames[0] + fnames[1];
                            }
                            else
                            {
                                fmail = firstname;
                            }
                            if (lnames.Length >= 2)
                            {
                                lmail = lnames[0] + lnames[1];
                            }
                            else
                            {
                                lmail = lastname;
                            }

                            username = fmail.ToLower() + '.' + lmail.ToLower();
                            emailid = username + "@epsbot.onmicrosoft.com";
                        }
                        else
                        {
                            username = name;
                            Variables.xusername = username;
                            xUser xu = new xUser();
                            xu.adusername = username;
                            Variables.xuser.Add(xu);
                        }

                        try
                        {
                            string emails_query = "select emailaddress from `ep_hrms`.`main_users`";
                            string emails_existing = csql.executeSQL(emails_query, "Select");
                            emails_existing = Regex.Replace(emails_existing, @"[^0-9a-zA-Z:,@.]+", "");
                            emails_existing = emails_existing.Replace("emailaddress:", "");
                            char[] charSeparators = new char[] { ',' };
                            String[] strlist = emails_existing.Split(charSeparators, emails_existing.Split(',').Length, StringSplitOptions.None);
                            bool exists=false;
                            foreach (var eml in strlist)
                            {
                                if (emailid == eml)
                                {
                                    exists = true;
                                }
                            }
                            if (!exists)
                            {
                                string max = "select max(id)+1 from `ep_hrms`.`main_users`";
                                string maxv = csql.executeSQL(max, "scalar");
                                string empids = "EPSE50" + maxv;
                                string empquery = "INSERT INTO `ep_hrms`.`main_users` VALUES (" + maxv + ", '35', 'old', '" + firstname + "', '" + lastname + "', '" + name + "', '" + emailid + "', '" + contactno + "', NULL, 'Yet to start', '0', NULL, NULL, '3d385877d3cdda5e1b56bda8ea8c5783', 1, 1, now(), now(), '1', '" + empids + "', 'Direct', NULL, NULL, NULL, CURDATE(), NULL, NULL, NULL, '40', '1', 'default')";
                                csql.executeSQL(empquery, "");
                                string maxSummary = "select max(id)+1 from `ep_hrms`.`main_employees`";
                                string maxS = csql.executeSQL(maxSummary, "scalar");
                                string summaryquery = "INSERT INTO `ep_hrms`.`main_employees` VALUES(" + maxS + "," + maxv + ",'2018-06-08','2018-06-08','8','1','1','4','40','25',NULL,NULL,'1',NULL,NULL,NULL,'1','1',now(),now(),'1','0')";
                                csql.executeSQL(summaryquery, "");
                                hrmsid = empids;
                                xUser xu = new xUser();
                                xu.hrmsusername = empids;
                                Variables.xuser.Add(xu);
                            }
                            else {
                                hrmsid = "(duplicate record)";
                                xUser xu = new xUser();
                                xu.hrmsusername = hrmsid;
                                Variables.xuser.Add(xu);
                            }
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                        try
                        {
                            using (Npgsql.NpgsqlConnection con = new Npgsql.NpgsqlConnection(Variables.connString))
                            {
                                string tickettable = Variables.datatbname;
                                if (string.IsNullOrEmpty(tickettable))
                                {
                                    tickettable = "Ticket";
                                }
                                using (Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand("UPDATE public.\"Ticket\" set \"Status_Id\"=(select \"ID\" from \"Ticket_Status\" WHERE \"Ticket_Status\"='Closed' and \"Status\"=true) where \"ID\" = " + dr["ID"]))
                                {
                                    cmd.Connection = con;
                                    con.Open();
                                    using (Npgsql.NpgsqlDataAdapter ndr = new Npgsql.NpgsqlDataAdapter(cmd))
                                    {
                                        cmd.ExecuteNonQuery();
                                    }
                                    con.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                    }
                }
                otpt = inpt;
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                vh.CreateVariable(Output, JsonConvert.SerializeObject(otpt), "", true, "datatable", step.Robot_Id, executionID);
                new ExecutionLog().add2Log("Create user is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult DeleteUser(Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty; string bodyExist = string.Empty;
            string ServiceURL = string.Empty; string UserName = string.Empty; string Password = string.Empty; string Operation = string.Empty;
            string Input = string.Empty; string Output = string.Empty; DataTable otpt = new DataTable();
            try
            {

                ServiceURL = stp.getPropertiesValue(StringHelper.serviceurl, step);
                UserName = stp.getPropertiesValue(StringHelper.username, step);
                Password = stp.getPropertiesValue(StringHelper.password, step);
                Operation = stp.getPropertiesValue(StringHelper.operation, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);

                System.Data.DataTable inpt = new System.Data.DataTable();
                List<Dictionary<string, dynamic>> inputArray1 = new List<Dictionary<string, dynamic>>();
                List<string> orcas = new List<string>();

                if (vh.checkVariableExists(Input.Trim(), executionID))
                {
                    inpt = new TypeHandler().getInput(Input, StringHelper.variable, executionID);

                    inputArray1 = inpt.AsEnumerable().Select(row => inpt.Columns.Cast<DataColumn>().ToDictionary(column => column.ColumnName, column => row[column] as dynamic)).ToList();
                    foreach (var dict in inputArray1)
                    {
                        bodyExist = dict["Body"];
                    }
                }

                SqlServerConnection csql = new SqlServerConnection(ServiceURL, "3306", "ep_hrms", UserName, Password);
                //List<string> orcas = inpt.AsEnumerable().Select(dr => dr.Field<string>("Description")).ToList();

                string hrmsid = string.Empty;

                foreach (DataRow dr in inpt.Rows)
                {
                    if ((bodyExist != "") || (bodyExist != null))
                    {
                        foreach (var dict in inputArray1)
                        {
                            var pdfReaderPath = dict["Body"];
                            string[] strArray = pdfReaderPath.Split('\n');

                            foreach (string namePart in strArray)
                            {
                                try
                                {
                                    if (namePart.Trim() != string.Empty && namePart.IndexOf(":") > -1)
                                    {
                                        string fpart = namePart.Split(':')[0].Trim();
                                        string spart = namePart.Split(':')[1].Trim();

                                        if (fpart.ToLower().Trim() == "hrmsid")
                                        {
                                            hrmsid = spart;
                                        }

                                    }
                                }
                                catch (Exception ex)
                                {
                                    new Logger().LogException(ex);
                                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                }
                            }
                        }
                    }
                    else
                    {
                        string orca = dr["Description"].ToString();
                        if (orca.ToLower().IndexOf(StringHelper.requestforeoffb) > -1 || orca.ToLower().IndexOf(StringHelper.eoffb) > -1)
                        {
                            string desc = orca;

                            List<string> slashns = desc.Split(new string[] { "\n", "\\n" }, StringSplitOptions.None).ToList();
                            foreach (string slashn in slashns)
                            {
                                try
                                {
                                    if (slashn.Trim() != string.Empty && slashn.IndexOf(":") > -1)
                                    {
                                        string fpart = slashn.Split(':')[0].Trim();
                                        string spart = slashn.Split(':')[1].Trim();

                                        if (fpart.ToLower().Trim() == "hrmsid")
                                        {
                                            hrmsid = spart;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    new Logger().LogException(ex);
                                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                }
                            }
                        }
                    }
                    var regexItem = new Regex("^[a-zA-Z0-9 ]");

                    if (hrmsid.Trim() != string.Empty && regexItem.IsMatch(hrmsid))
                    {
                        try
                        {
                            string summaryquery = "update ep_hrms.main_users set `isactive`=0 where `employeeId`='" + hrmsid + "'";
                            csql.executeSQL(summaryquery, "");
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }

                    }
                }

                otpt = inpt;
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                vh.CreateVariable(Output, JsonConvert.SerializeObject(otpt), "", true, "datatable", step.Robot_Id, executionID);
                new ExecutionLog().add2Log("Delete user is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
    }
}