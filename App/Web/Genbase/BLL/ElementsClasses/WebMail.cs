﻿using Genbase.classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Renci.SshNet;
using MySql.Data.MySqlClient;
using Genbase.Classes;

using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Genbase.BLL.ElementsClasses
{
    public class WebMail
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        VariableHandler vh = new VariableHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult CreateMail(Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty;
            string URL = string.Empty; string UserName = string.Empty; string Password = string.Empty; string Operation = string.Empty;
            string Input = string.Empty; string Output = string.Empty; string pdfPath = string.Empty;
            DataTable otpt = new DataTable();
            try
            {
                URL = stp.getPropertiesValue(StringHelper.url, step);
                UserName = stp.getPropertiesValue(StringHelper.username, step);
                Password = stp.getPropertiesValue(StringHelper.password, step);

                Operation = stp.getPropertiesValue(StringHelper.operation, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);

                System.Data.DataTable inpt = new System.Data.DataTable();

                otpt.Columns.Add("desc");
                otpt.Columns.Add("slackusername");
                otpt.Columns.Add("slackdpassword");
                otpt.Columns.Add("hrmsusername");
                otpt.Columns.Add("hrmsdpassword");
                otpt.Columns.Add("mailid");
                otpt.Columns.Add("mailpassword");
                otpt.Columns.Add("name");

                List<Dictionary<string, dynamic>> inputArray1 = new List<Dictionary<string, dynamic>>();
                List<string> orcas = new List<string>();

                if (vh.checkVariableExists(Input.Trim(), executionID))
                {
                    inpt = new TypeHandler().getInput(Input, StringHelper.variable, executionID);

                    inputArray1 = inpt.AsEnumerable().Select(row => inpt.Columns.Cast<DataColumn>().ToDictionary(column => column.ColumnName, column => row[column] as dynamic)).ToList();
                   
                }

                foreach (DataRow dr in inpt.Rows)
                {
                    string firstname = string.Empty;
                    string name = string.Empty;
                    string lastname = string.Empty;
                    string emailid = string.Empty;
                    string emailpswd = string.Empty;
                    string slackid = string.Empty;
                    string slackpswd = string.Empty;
                    string adname = string.Empty;
                    string username = string.Empty;
                    string orca = string.Empty;

                    if (dr.ItemArray[0].ToString().Contains("EMPLOYEE INFORMATION"))
                    {
                        foreach (var dict in inputArray1)
                        {
                            if (dict.Keys.Contains("file"))
                            {
                                var pdfReaderPath = dict["file"];
                                string pdfRead = pdfReaderPath.ToString();
                                var startTag = "First:"; int startIndex = pdfRead.IndexOf(startTag) + startTag.Length;
                                int endIndex = pdfRead.IndexOf("Middle:", startIndex);
                                firstname = pdfRead.Substring(startIndex, endIndex - startIndex);
                                firstname = Regex.Replace(firstname, @"\t|\n|\r", "");
                                startTag = "Last:"; startIndex = pdfRead.IndexOf(startTag) + startTag.Length;
                                endIndex = pdfRead.IndexOf("Title:", startIndex);
                                lastname = pdfRead.Substring(startIndex, endIndex - startIndex);
                                lastname = Regex.Replace(lastname, @"\t|\n|\r", "");
                            }
                        }
                    }
                    else
                    {
                        orca = dr["Description"].ToString();
                        if (orca.ToLower().IndexOf(StringHelper.requestforeob) > -1 || orca.ToLower().IndexOf(StringHelper.eob) > -1)
                        {
                            string desc = orca;

                            List<string> slashns = desc.Split(new string[] { "\n", "\\n" }, StringSplitOptions.None).ToList();
                            foreach (string slashn in slashns)
                            {
                                try
                                {
                                    if (slashn.Trim() != string.Empty && slashn.IndexOf(":") > -1)
                                    {
                                        string fpart = slashn.Split(':')[0].Trim();
                                        string spart = slashn.Split(':')[1].Trim();

                                        if (fpart.ToLower().Trim() == StringHelper.firstname)
                                        {
                                            firstname = spart;
                                        }
                                        else if (fpart.ToLower().Trim() == StringHelper.lastname)
                                        {
                                            lastname = spart;
                                        }
                                        else if (fpart.ToLower().Trim() == StringHelper.name)
                                        {
                                            name = spart;
                                        }

                                    }
                                }
                                catch (Exception ex)
                                {
                                    new Logger().LogException(ex);
                                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                }
                            }
                        }
                    }
                    name = firstname.Trim() + " " + lastname.Trim();
                    var regexItem = new Regex("^[a-zA-Z0-9 ].*$");

                    if (name.Trim() != string.Empty && regexItem.IsMatch(name))
                    {
                        if (name.Trim().IndexOf(" ") > -1)
                        {
                            //username = name.Split(' ')[0][0] + name.Split(' ')[1];
                            string fmail;
                            string lmail;
                            char[] seperator = { ' ' };
                            String[] fnames = firstname.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                            String[] lnames = lastname.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                            if (fnames.Length >= 2)
                            {
                                fmail = fnames[0] + fnames[1];
                            }
                            else
                            {
                                fmail = firstname;
                            }
                            if (lnames.Length >= 2)
                            {
                                lmail = lnames[0] + lnames[1];
                            }
                            else
                            {
                                lmail = lastname;
                            }

                            username = fmail.ToLower() + '.' + lmail.ToLower();
                            emailid = username + "@epsbot.onmicrosoft.com";
                            emailpswd = username + "@epsbot.onmicrosoft.com";
                            Variables.xusername = username;
                            xUser xu = new xUser();
                            xu.mailid = emailid;
                            xu.sapuserid = fmail.ToUpper() + lmail.ToUpper().ToString().Substring(0, 1);
                            //xu.adusername = username;
                            if (Variables.xuser.Count != 0)
                            {
                                Variables.xuser.Add(xu);
                            }
                            else
                            {
                                Variables.xuser.Add(xu);
                            }
                        }
                        else
                        {
                            username = name;
                            Variables.xusername = username;
                            xUser xu = new xUser();
                            xu.adusername = username;
                            Variables.xuser.Add(xu);
                        }

                        try
                        {

                            //PasswordConnectionInfo connectionInfo = new PasswordConnectionInfo(URL, 22, "root", "Genbase!@#");
                            //connectionInfo.Timeout = TimeSpan.FromSeconds(30);
                            //var client = new SshClient(connectionInfo);
                            //client.Connect();
                            //ForwardedPortLocal portFwld = new ForwardedPortLocal("127.0.0.1", Convert.ToUInt32(3306), "127.0.0.1", Convert.ToUInt32(3306));

                            //client.AddForwardedPort(portFwld);

                            //portFwld.Start();

                            //MySqlConnectionStringBuilder msb = new MySqlConnectionStringBuilder();
                            //msb.UserID = UserName;
                            //msb.Password = Password;
                            //msb.Server = "127.0.0.1";
                            //msb.Port = 3306;
                            //msb.Database = "mysql";
                            //msb.ConnectionTimeout = 1000000;

                            //using (MySqlConnection con = new MySqlConnection(msb.ToString()))
                            //{
                            //    con.Open();
                            //    try
                            //    {
                            //        string query = "insert into `vmail`.`mailbox` values('" + emailid + "','" + emailid + "', '" + emailid + "', 'en-US', '', '', '', 10, '#######.com', '', 'IT', '', 'EPSE50540', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '','','', 1);";
                            //        MySqlCommand cmd = new MySqlCommand(query, con);
                            //        int reult = cmd.ExecuteNonQuery();

                            //        DataTable dt = new DataTable();
                            //    }
                            //    catch (Exception ex)
                            //    {
                            //        new Logger().LogException(ex);
                            //        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            //        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            //        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                            //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            //        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                            //    }
                            //    finally
                            //    {
                            //        con.Close();
                            //    }

                            //    con.Close();
                            //}
                            //client.Disconnect();

                            using (var client = new HttpClient())
                            {
                                string token = getGraphAPIAccessToken();
                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                                UserCreation uc = new UserCreation
                                {
                                    accountEnabled = true,
                                    displayName = firstname,
                                    mailNickName = lastname,
                                    passwordProfile = new PasswordProfile { forceChangePasswordNextSignIn = false, password = "Welcome@123" },
                                    userPrincipalName = username + "@epsbot.onmicrosoft.com"
                                };

                                string json = JsonConvert.SerializeObject(uc);
                                var content = new StringContent(json, Encoding.UTF8, "application/json");

                                HttpResponseMessage response12 = client.PostAsync("https://graph.microsoft.com/v1.0/users", content).Result;
                            }
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                        otpt.Rows.Add(orca, slackid, slackpswd, "", "", emailid, emailpswd, name);
                        try
                        {
                            using (Npgsql.NpgsqlConnection con = new Npgsql.NpgsqlConnection(Variables.connString))
                            {
                                string tickettable = Variables.datatbname;
                                if (string.IsNullOrEmpty(tickettable))
                                {
                                    tickettable = "Ticket";
                                }
                                using (Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand("UPDATE public.\"Ticket\" set \"Status_Id\"=(select \"ID\" from \"Ticket_Status\" WHERE \"Ticket_Status\"='Closed' and \"Status\"=true) where \"ID\" = " + dr["ID"]))
                                {
                                    cmd.Connection = con;
                                    con.Open();
                                    using (Npgsql.NpgsqlDataAdapter ndr = new Npgsql.NpgsqlDataAdapter(cmd))
                                    {
                                        cmd.ExecuteNonQuery();
                                    }
                                    con.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                vh.CreateVariable(Output, JsonConvert.SerializeObject(otpt), "", true, "datatable", step.Robot_Id, executionID);
                new ExecutionLog().add2Log("Create mail is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);

            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        private static string getGraphAPIAccessToken()
        {
            string accessToken = string.Empty;
            string result = string.Empty;
            HttpClient client = null;
            HttpRequestMessage httpRequestMessage = null;
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                string tenantName = "0e0b00db-7b47-4352-b089-9c09c6a6aa70";
                string requesrUrl = "https://login.microsoftonline.com/{TENANATNAME}/oauth2/token";
                requesrUrl = requesrUrl.Replace("{TENANATNAME}", tenantName);
                var values = new Dictionary<string, string>
                {
                    { "grant_type", "password" },
                    { "client_id", "d377ace8-0583-4919-841b-bb59170260a8"},
                    { "client_secret", ":/Elf1_3HvKGVPO:3CLZczVKNONvFuY5"},
                    { "resource","https://graph.microsoft.com" },
                    { "userName","admin@genbase.onmicrosoft.com" },
                    { "password","Genbase!@#"},
                };
                var content = new FormUrlEncodedContent(values);
                client = new HttpClient();
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, new Uri(requesrUrl));
                client.BaseAddress = new Uri(requesrUrl);
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded");
                httpRequestMessage.Content = content;
                httpResponseMessage = client.SendAsync(httpRequestMessage).Result;
                result = httpResponseMessage.Content.ReadAsStringAsync().Result;
                var resultObj = JsonConvert.DeserializeObject<dynamic>(result);
                if (resultObj != null)
                    accessToken = Convert.ToString(resultObj.access_token);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                client = null;
                httpRequestMessage = null;
                httpResponseMessage = null;
            }
            return accessToken;
        }
        public MethodResult DeleteUser(Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty;
            string URL = string.Empty; string UserName = string.Empty; string Password = string.Empty; string Operation = string.Empty;
            string Input = string.Empty; string Output = string.Empty; string pdfPath = string.Empty; string bodyExist = string.Empty;
            DataTable otpt = new DataTable();
            try
            {
                URL = stp.getPropertiesValue(StringHelper.url, step);
                UserName = stp.getPropertiesValue(StringHelper.username, step);
                Password = stp.getPropertiesValue(StringHelper.password, step);
                Operation = stp.getPropertiesValue(StringHelper.operation, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);

                System.Data.DataTable inpt = new System.Data.DataTable();
                List<Dictionary<string, dynamic>> inputArray1 = new List<Dictionary<string, dynamic>>();
                List<string> orcas = new List<string>();

                otpt.Columns.Add("desc");
                otpt.Columns.Add("slackusername");
                otpt.Columns.Add("hrmsusername");
                otpt.Columns.Add("mailid");
                otpt.Columns.Add("name");

                if (vh.checkVariableExists(Input.Trim(), executionID))
                {
                    inpt = new TypeHandler().getInput(Input, StringHelper.variable, executionID);

                    inputArray1 = inpt.AsEnumerable().Select(row => inpt.Columns.Cast<DataColumn>().ToDictionary(column => column.ColumnName, column => row[column] as dynamic)).ToList();
                    foreach (var dict in inputArray1)
                    {
                        bodyExist = dict["Body"];
                    }
                }
                else
                {
                    SqlServerConnection csql = new SqlServerConnection(URL, "3306", "vmail", UserName, Password);
                    string summaryquery = "update `vmail`.`mailbox` set `active`=0 where where `username`='" + Input + "' ";
                    csql.executeSQL(summaryquery, "");
                }

                //List<string> orcas = inpt.AsEnumerable().Select(dr => dr.Field<string>("Description")).ToList();
                string firstname = string.Empty;
                string name = string.Empty;
                string hrmsid = string.Empty;
                string lastname = string.Empty;
                string emailid = string.Empty;
                string slackid = string.Empty;
                string adname = string.Empty;
                string username = string.Empty;

                foreach (DataRow dr in inpt.Rows)
                {
                    if((bodyExist != "") || (bodyExist != null))
                    {
                        foreach (var dict in inputArray1)
                        {
                            var pdfReaderPath = dict["Body"];
                            string[] strArray = pdfReaderPath.Split('\n');

                            foreach (string namePart in strArray)
                            {
                                try
                                {
                                    if (namePart.Trim() != string.Empty && namePart.IndexOf(":") > -1)
                                    {
                                        string fpart = namePart.Split(':')[0].Trim();
                                        string spart = namePart.Split(':')[1].Trim();

                                        if (fpart.ToLower().Trim() == "emailid")
                                        {
                                            emailid = spart;
                                        }
                                        else if (fpart.ToLower().Trim() == "name")
                                        {
                                            name = spart;
                                        }

                                    }
                                }
                                catch (Exception ex)
                                {
                                    new Logger().LogException(ex);
                                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                }
                            }
                        }
                    }
                    else
                    {
                        string orca = dr["Description"].ToString();
                        if (orca.ToLower().IndexOf(StringHelper.requestforeoffb) > -1 || orca.ToLower().IndexOf(StringHelper.eoffb) > -1)
                        {
                            string desc = orca;

                            List<string> slashns = desc.Split(new string[] { "\n", "\\n" }, StringSplitOptions.None).ToList();
                            foreach (string slashn in slashns)
                            {
                                try
                                {
                                    if (slashn.Trim() != string.Empty && slashn.IndexOf(":") > -1)
                                    {
                                        string fpart = slashn.Split(':')[0].Trim();
                                        string spart = slashn.Split(':')[1].Trim();

                                        if (fpart.ToLower().Trim() == "hrmsid")
                                        {
                                            hrmsid = spart;
                                        }
                                        else if (fpart.ToLower().Trim() == "emailid")
                                        {
                                            emailid = spart;
                                        }
                                        else if (fpart.ToLower().Trim() == "slackid")
                                        {
                                            slackid = spart;
                                        }
                                        else if (fpart.ToLower().Trim() == "name")
                                        {
                                            name = spart;
                                        }

                                    }
                                }
                                catch (Exception ex)
                                {
                                    new Logger().LogException(ex);
                                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                }
                            }
                            otpt.Rows.Add(orca, slackid, hrmsid, emailid, name);
                        }
                    }
                    var regexItem = new Regex("^[a-zA-Z0-9 ].*$");

                    if (name.Trim() != string.Empty && regexItem.IsMatch(name))
                    {
                        emailid = name + "@epsbot.onmicrosoft.com";

                        if (name.Trim().IndexOf(" ") > -1)
                        {
                            username = name.Split(' ')[0][0] + name.Split(' ')[1];
                            Variables.xusername = username;
                            xUser xu = new xUser();
                            xu.mailid = emailid;
                            Variables.xuser.Add(xu);

                        }
                        else
                        {
                            username = name;
                            Variables.xusername = username;
                            xUser xu = new xUser();
                            xu.adusername = username;
                            Variables.xuser.Add(xu);
                        }
                        try
                        {
                            //using (var ssh = new SshClient(new MethodResultHandler().CreateConnection(URL, 22, "root", "Genbase!@#")))
                            //{
                            //    //string cmd = string.Empty, answer = string.Empty; SshCommand cmd1 = null; string noanswer = string.Empty;
                            //    //cmd = Command;
                            //    ssh.Connect();
                            //    var portFwld = new ForwardedPortLocal("127.0.0.1", 3306, "127.0.0.1", 3306);
                            //    ssh.AddForwardedPort(portFwld);
                            //    portFwld.Start();
                            //    if (portFwld.IsStarted)
                            //    {
                            //        SqlServerConnection csql = new SqlServerConnection(URL, "3306", "vmail", UserName, Password);
                            //        //string maxSummary = "select `id` from `vmail`.`mailbox` where `username`='" + emailid + "'";
                            //        //string maxS = csql.executeSQL(maxSummary, "scalar");
                            //        string summaryquery = "update `vmail`.`mailbox` set `active`=0 where where `username`='" + emailid + "' ";
                            //        csql.executeSQL(summaryquery, "");

                            //        //string query = "insert into `vmail`.`mailbox` values('" + emailid + "','" + emailid + "', '" + emailid + "', 'en-US', '', '', '', 10, '#######.com', '', 'IT', '', 'EPSE50540', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1)";
                            //        //SqlServerConnection csql = new SqlServerConnection(URL, "3306", "vmail", UserName, Password);
                            //        //csql.executeSQL(query, "");
                            //    }
                            //    portFwld.Stop();
                            //    ssh.Disconnect();
                            //}
                            using (var client = new HttpClient())
                            {
                                string token = getGraphAPIAccessToken();
                                //var deleteUser = "john.wesley@epsBot.onmicrosoft.com";
                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                                HttpResponseMessage response1 = client.DeleteAsync("https://graph.microsoft.com/v1.0/users/" + emailid).Result;
                                Console.WriteLine(response1.Content.ReadAsStringAsync().Result);
                            }
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }

                        try
                        {
                            using (Npgsql.NpgsqlConnection con = new Npgsql.NpgsqlConnection(Variables.connString))
                            {
                                string tickettable = Variables.datatbname;
                                if (string.IsNullOrEmpty(tickettable))
                                {
                                    tickettable = "Ticket";
                                }
                                using (Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand("UPDATE public.\"Ticket\" set \"Status_Id\"=5 where \"ID\" = " + dr["ID"]))
                                {
                                    cmd.Connection = con;
                                    con.Open();
                                    using (Npgsql.NpgsqlDataAdapter ndr = new Npgsql.NpgsqlDataAdapter(cmd))
                                    {
                                        cmd.ExecuteNonQuery();
                                    }
                                    con.Close();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                    }

                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                vh.CreateVariable(Output, JsonConvert.SerializeObject(otpt), "", true, "datatable", step.Robot_Id, executionID);
                new ExecutionLog().add2Log("Delete User is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);

            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
    }
    class UserCreation
    {
        public bool accountEnabled { get; internal set; }
        public string displayName { get; internal set; }
        public string mailNickName { get; internal set; }
        public string userPrincipalName { get; internal set; }
        public PasswordProfile passwordProfile { get; internal set; }
    }
    public class PasswordProfile
    {
        public string password { get; set; }
        public bool? forceChangePasswordNextSignIn { get; set; }
        public bool? forceChangePasswordNextSignInWithMfa { get; set; }
    }
}