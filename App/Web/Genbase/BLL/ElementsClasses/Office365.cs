﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Genbase.classes;
using System.Data;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Renci.SshNet;
using MySql.Data.MySqlClient;
using Genbase.Classes;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json.Linq;

namespace Genbase.BLL.ElementsClasses
{
    public class Office365
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        VariableHandler vh = new VariableHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        string eventUserName = string.Empty; string employeeName = string.Empty;

        public MethodResult CreateO365Mail(Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty;
            string URL = string.Empty; string UserName = string.Empty; string Password = string.Empty; string Operation = string.Empty;
            string Input = string.Empty; string Output = string.Empty; string pdfPath = string.Empty;
            DataTable otpt = new DataTable();
            try
            {
                URL = stp.getPropertiesValue(StringHelper.url, step);
                UserName = stp.getPropertiesValue(StringHelper.username, step);
                Password = stp.getPropertiesValue(StringHelper.password, step);

                Operation = stp.getPropertiesValue(StringHelper.operation, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);

                System.Data.DataTable inpt = new System.Data.DataTable();

                otpt.Columns.Add("desc");
                otpt.Columns.Add("slackusername");
                otpt.Columns.Add("slackdpassword");
                otpt.Columns.Add("hrmsusername");
                otpt.Columns.Add("hrmsdpassword");
                otpt.Columns.Add("mailid");
                otpt.Columns.Add("mailpassword");
                otpt.Columns.Add("name");

                List<Dictionary<string, dynamic>> inputArray1 = new List<Dictionary<string, dynamic>>();
                List<string> orcas = new List<string>();

                if (vh.checkVariableExists(Input.Trim(), executionID))
                {
                    inpt = new TypeHandler().getInput(Input, StringHelper.variable, executionID);

                    inputArray1 = inpt.AsEnumerable().Select(row => inpt.Columns.Cast<DataColumn>().ToDictionary(column => column.ColumnName, column => row[column] as dynamic)).ToList();

                }

                //foreach (DataRow dr in inpt.Rows)
                {
                    string firstname = string.Empty;
                    string name = string.Empty;
                    string lastname = string.Empty;
                    string emailid = string.Empty;
                    string emailpswd = string.Empty;
                    string slackid = string.Empty;
                    string slackpswd = string.Empty;
                    string adname = string.Empty;
                    string username = string.Empty;
                    string orca = string.Empty;
                    xUser xu = new xUser();

                    foreach (var inputData in inputArray1)
                    {
                        //if (inputData.Keys.Contains("Assumptions") && inputData.Values.Contains("Preferred Professional Name"))
                        if (inputData.Values.Contains("Preferred Professional Name"))
                        {
                            //var empName = inputData["Information"];
                            var empName = inputData["Column3"];
                            string[] names = empName.Split(' ');

                            firstname = names[0];
                            lastname = names[1];
                            xu.EmployeeName = empName;
                        }
                        else if (inputData.Values.Contains("Start Date"))
                        {
                            xu.Date = inputData["Column3"];
                        }
                        else if (inputData.Values.Contains("Manager Name"))
                        {
                            xu.ManagerName = inputData["Column3"];
                            string manager = inputData["Column3"];
                            string[] arrManager = manager.Split(" ");
                            xu.Manager = arrManager[0].ToLower() + '.' + arrManager[1].ToLower();
                        }
                        else if (inputData.Values.Contains("Supervisor"))
                        {
                            xu.SupervisorName = inputData["Column3"];
                        }
                        else if (inputData.Values.Contains("HR Name"))
                        {
                            xu.HRName = inputData["Column3"];
                        }
                        else if (inputData.Values.Contains("Gender"))
                        {
                            xu.Gender = inputData["Column3"];
                        }
                        else if (inputData.Values.Contains("Department"))
                        {
                            xu.Department = inputData["Column3"];
                        }
                        else if (inputData.Values.Contains("Employee position"))
                        {
                            xu.EmployeePosition = inputData["Column3"];
                        }
                        //else if (inputData.Values.Contains("Total number of years"))
                        //{
                        //    xu.TotalNumberOfYears = "4 Years";
                        //}
                        //else if (inputData.Values.Contains("Expertise"))
                        //{
                        //    xu.Expertise = "Expertise";
                        //}
                        //else if (inputData.Values.Contains("Other information"))
                        //{
                        //    xu.OtherInformation = "Other Information";
                        //}
                    }

                    xu.TotalNumberOfYears = "4 Years";
                    xu.Expertise = ".Net Core";
                    xu.OtherInformation = "Angular";

                    //if (dr.ItemArray[0].ToString().Contains("EMPLOYEE INFORMATION"))
                    //{
                    //    foreach (var dict in inputArray1)
                    //    {
                    //        if (dict.Keys.Contains("file"))
                    //        {
                    //            var pdfReaderPath = dict["file"];
                    //            string pdfRead = pdfReaderPath.ToString();
                    //            var startTag = "First:"; int startIndex = pdfRead.IndexOf(startTag) + startTag.Length;
                    //            int endIndex = pdfRead.IndexOf("Middle:", startIndex);
                    //            firstname = pdfRead.Substring(startIndex, endIndex - startIndex);
                    //            firstname = Regex.Replace(firstname, @"\t|\n|\r", "");
                    //            startTag = "Last:"; startIndex = pdfRead.IndexOf(startTag) + startTag.Length;
                    //            endIndex = pdfRead.IndexOf("Title:", startIndex);
                    //            lastname = pdfRead.Substring(startIndex, endIndex - startIndex);
                    //            lastname = Regex.Replace(lastname, @"\t|\n|\r", "");
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    orca = dr["Description"].ToString();
                    //    if (orca.ToLower().IndexOf(StringHelper.requestforeob) > -1 || orca.ToLower().IndexOf(StringHelper.eob) > -1)
                    //    {
                    //        string desc = orca;

                    //        List<string> slashns = desc.Split(new string[] { "\n", "\\n" }, StringSplitOptions.None).ToList();
                    //        foreach (string slashn in slashns)
                    //        {
                    //            try
                    //            {
                    //                if (slashn.Trim() != string.Empty && slashn.IndexOf(":") > -1)
                    //                {
                    //                    string fpart = slashn.Split(':')[0].Trim();
                    //                    string spart = slashn.Split(':')[1].Trim();

                    //                    if (fpart.ToLower().Trim() == StringHelper.firstname)
                    //                    {
                    //                        firstname = spart;
                    //                    }
                    //                    else if (fpart.ToLower().Trim() == StringHelper.lastname)
                    //                    {
                    //                        lastname = spart;
                    //                    }
                    //                    else if (fpart.ToLower().Trim() == StringHelper.name)
                    //                    {
                    //                        name = spart;
                    //                    }

                    //                }
                    //            }
                    //            catch (Exception ex)
                    //            {
                    //                new Logger().LogException(ex);
                    //                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                    //                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    //                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    //                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                    //            }
                    //        }
                    //    }
                    //}
                    name = firstname.Trim() + " " + lastname.Trim();
                    var regexItem = new Regex("^[a-zA-Z0-9 ].*$");

                    if (name.Trim() != string.Empty && regexItem.IsMatch(name))
                    {
                        if (name.Trim().IndexOf(" ") > -1)
                        {
                            string fmail;
                            string lmail;
                            char[] seperator = { ' ' };
                            String[] fnames = firstname.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                            String[] lnames = lastname.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                            if (fnames.Length >= 2)
                            {
                                fmail = fnames[0] + fnames[1];
                            }
                            else
                            {
                                fmail = firstname;
                            }
                            if (lnames.Length >= 2)
                            {
                                lmail = lnames[0] + lnames[1];
                            }
                            else
                            {
                                lmail = lastname;
                            }

                            username = fmail.ToLower() + '.' + lmail.ToLower();
                            emailid = username + "@genbase.onmicrosoft.com";
                            emailpswd = username + "@genbase.onmicrosoft.com";
                            Variables.xusername = username;
                            //xUser xu = new xUser();
                            xu.mailid = emailid;
                            xu.sapuserid = fmail.ToUpper() + lmail.ToUpper().ToString().Substring(0, 1);
                            if (Variables.xuser.Count != 0)
                            {
                                Variables.xuser.Add(xu);
                            }
                            else
                            {
                                Variables.xuser.Add(xu);
                            }
                        }
                        else
                        {
                            username = name;
                            Variables.xusername = username;
                            //xUser xu = new xUser();
                            xu.adusername = username;
                            Variables.xuser.Add(xu);
                        }

                        try
                        {

                            using (var client = new HttpClient())
                            {
                                string token = getGraphAPIAccessToken();
                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                                UserCreation uc = new UserCreation
                                {
                                    accountEnabled = true,
                                    displayName = firstname,
                                    mailNickName = lastname,
                                    passwordProfile = new PasswordProfile { forceChangePasswordNextSignIn = false, password = "Welcome@123" },
                                    userPrincipalName = username + "@genbase.onmicrosoft.com"
                                };

                                string json = JsonConvert.SerializeObject(uc);
                                var content = new StringContent(json, Encoding.UTF8, "application/json");

                                HttpResponseMessage response12 = client.PostAsync("https://graph.microsoft.com/v1.0/users", content).Result;
                            }
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                vh.CreateVariable(Output, JsonConvert.SerializeObject(otpt), "", true, "datatable", step.Robot_Id, executionID);
                new ExecutionLog().add2Log("Create mail is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);

            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        private static string getGraphAPIAccessToken()
        {
            string accessToken = string.Empty;
            string result = string.Empty;
            HttpClient client = null;
            HttpRequestMessage httpRequestMessage = null;
            HttpResponseMessage httpResponseMessage = null;
            try
            {
                string tenantName = "42c1291d-6f95-4ea4-a1b1-459312a24fac";
                string requesrUrl = "https://login.microsoftonline.com/{TENANATNAME}/oauth2/token";
                requesrUrl = requesrUrl.Replace("{TENANATNAME}", tenantName);
                var values = new Dictionary<string, string>
                {
                    { "grant_type", "password" },
                    { "client_id", "bbf8cd78-eec3-4cc1-9732-d33d22b77c7a"},
                    { "client_secret", "FGlXqUzEZ8tKGKN/5xZgbmd9RviC9?:]"},
                    { "resource","https://graph.microsoft.com" },
                    { "userName","admin@genbase.onmicrosoft.com" },
                    { "password","Genbase!@#"},
                };
                var content = new FormUrlEncodedContent(values);
                client = new HttpClient();
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, new Uri(requesrUrl));
                client.BaseAddress = new Uri(requesrUrl);
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded");
                httpRequestMessage.Content = content;
                httpResponseMessage = client.SendAsync(httpRequestMessage).Result;
                result = httpResponseMessage.Content.ReadAsStringAsync().Result;
                var resultObj = JsonConvert.DeserializeObject<dynamic>(result);
                if (resultObj != null)
                    accessToken = Convert.ToString(resultObj.access_token);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                client = null;
                httpRequestMessage = null;
                httpResponseMessage = null;
            }
            return accessToken;
        }
        public MethodResult AddEvents(Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty;
            string URL = string.Empty; string UserName = string.Empty; string Password = string.Empty; string Operation = string.Empty;
            string Input = string.Empty; string Output = string.Empty; string pdfPath = string.Empty;
            DataTable otpt = new DataTable();
            try
            {
                URL = stp.getPropertiesValue(StringHelper.url, step);
                UserName = stp.getPropertiesValue(StringHelper.username, step);
                Password = stp.getPropertiesValue(StringHelper.password, step);

                Operation = stp.getPropertiesValue(StringHelper.operation, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);

                System.Data.DataTable inpt = new System.Data.DataTable();

                otpt.Columns.Add("desc");
                otpt.Columns.Add("slackusername");
                otpt.Columns.Add("slackdpassword");
                otpt.Columns.Add("hrmsusername");
                otpt.Columns.Add("hrmsdpassword");
                otpt.Columns.Add("mailid");
                otpt.Columns.Add("mailpassword");
                otpt.Columns.Add("name");

                List<Dictionary<string, dynamic>> inputArray1 = new List<Dictionary<string, dynamic>>();
                List<string> orcas = new List<string>();

                if (vh.checkVariableExists(Input.Trim(), executionID))
                {
                    inpt = new TypeHandler().getInput(Input, StringHelper.variable, executionID);

                    inputArray1 = inpt.AsEnumerable().Select(row => inpt.Columns.Cast<DataColumn>().ToDictionary(column => column.ColumnName, column => row[column] as dynamic)).ToList();

                }

                //foreach (DataRow dr in inpt.Rows)
                {
                    string firstname = string.Empty;
                    string name = string.Empty;
                    string lastname = string.Empty;
                    string emailid = string.Empty;
                    string emailpswd = string.Empty;
                    string slackid = string.Empty;
                    string slackpswd = string.Empty;
                    string adname = string.Empty;
                    string username = string.Empty;
                    string orca = string.Empty;
                    xUser xu = new xUser();
                    string[] distNames = { };

                    foreach (var inputData in inputArray1)
                    {
                        //if (inputData.Keys.Contains("Assumptions") && inputData.Values.Contains("Preferred Professional Name"))
                        if (inputData.Values.Contains("Preferred Professional Name"))
                        {
                            var empName = inputData["Column3"];
                            string[] names = empName.Split(' ');

                            firstname = names[0];
                            lastname = names[1];
                            //xu.EmployeeName = empName;
                        }
                        else if (inputData.Values.Contains("Email Distribution List(s)"))
                        {
                            var emailDistList = inputData["Column3"];
                            distNames = emailDistList.Split(',');
                        }
                    }

                    name = firstname.Trim() + " " + lastname.Trim();
                    employeeName = name;
                    var regexItem = new Regex("^[a-zA-Z0-9 ].*$");

                    if (name.Trim() != string.Empty && regexItem.IsMatch(name))
                    {
                        if (name.Trim().IndexOf(" ") > -1)
                        {
                            string fmail;
                            string lmail;
                            char[] seperator = { ' ' };
                            String[] fnames = firstname.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                            String[] lnames = lastname.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                            if (fnames.Length >= 2)
                            {
                                fmail = fnames[0] + fnames[1];
                            }
                            else
                            {
                                fmail = firstname;
                            }
                            if (lnames.Length >= 2)
                            {
                                lmail = lnames[0] + lnames[1];
                            }
                            else
                            {
                                lmail = lastname;
                            }

                            username = fmail.ToLower() + '.' + lmail.ToLower();
                            eventUserName = username;
                            emailid = username + "@genbase.onmicrosoft.com";
                            emailpswd = username + "@genbase.onmicrosoft.com";
                            Variables.xusername = username;
                            //xUser xu = new xUser();
                            xu.mailid = emailid;
                            xu.sapuserid = fmail.ToUpper() + lmail.ToUpper().ToString().Substring(0, 1);
                            if (Variables.xuser.Count != 0)
                            {
                                Variables.xuser.Add(xu);
                            }
                            else
                            {
                                Variables.xuser.Add(xu);
                            }
                        }
                        else
                        {
                            username = name;
                            Variables.xusername = username;
                            //xUser xu = new xUser();
                            xu.adusername = username;
                            Variables.xuser.Add(xu);
                        }

                        try
                        {
                            using (HttpClient client = new HttpClient())
                            {
                                string token = getGraphAPIAccessToken();

                                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                                //with your properties from above except for "Token"
                                //ToOutlookCalendar toOutlookCalendar = CreateObject();
                                ToOutlookCalendar toOutlookCalendar = UpdateObject();

                                string json = JsonConvert.SerializeObject(toOutlookCalendar);
                                var content = new StringContent(json, Encoding.UTF8, "application/json");
                                string getURL = "https://graph.microsoft.com/v1.0/me/calendars/AAMkAGUwYjEyZWI5LTE1NzYtNDAzZC1hYWU2LTRhNThmN2FkZDNhNgBGAAAAAAB8AiPpv90kT6qgOVDfJbk-BwARz2hlGDPOSZpR6ZS4ItkkAAAAAAEGAAARz2hlGDPOSZpR6ZS4ItkkAAABQ9wUAAA=/events";
                                HttpResponseMessage getResponse = client.GetAsync(getURL).Result;

                                getResponse.EnsureSuccessStatusCode();
                                //var ser = JsonConvert.SerializeObject(getResponse.Content.ReadAsStringAsync());
                                //var dese = JsonConvert.DeserializeObject(getResponse.Content.ReadAsStringAsync().Result);
                                var getEvents = getResponse.Content.ReadAsStringAsync().Result;

                                JObject jObject = JObject.Parse(getEvents);
                                JArray jUser = (JArray)jObject["value"];
                                string responseBody = "";

                                for (int i = 0; i < jUser.Count; i++)
                                {
                                    var eventValue = jUser[i].ToString();
                                    JObject jObject1 = JObject.Parse(eventValue);
                                    var eventId = jObject1["id"];

                                    string updateEventURL = getURL + "/" + eventId;

                                    var method = new HttpMethod("PATCH");
                                    string approvveReleaseMetaData = "{\"attendees\":[{\"emailAddress\":{ \"address\":\"" + eventUserName + "@genbase.onmicrosoft.com\", \"name\": \"" + employeeName + "\"},\"type\": \"required\"}]}";

                                    var request = new HttpRequestMessage(method, string.Format(updateEventURL))
                                    {
                                        Content = new StringContent(approvveReleaseMetaData, Encoding.UTF8, "application/json")
                                    };

                                    using (HttpResponseMessage response = client.SendAsync(request).Result)
                                    {
                                        response.EnsureSuccessStatusCode();
                                        responseBody = response.Content.ReadAsStringAsync().Result;
                                    }
                                }

                                if (responseBody != "")
                                {
                                    //foreach(var distibutionName in distNames)
                                    {
                                        string updateEventURL = "https://graph.microsoft.com/v1.0/groups/56b0c96d-e415-497b-bd3c-e65a6b9b7e12/members/$ref";

                                        string memberURI = "https://graph.microsoft.com/v1.0/users/" + eventUserName + "@genbase.onmicrosoft.com";
                                        string addMemberData = "{\"@odata.id\": \"" + memberURI + "\"}";

                                        var content1 = new StringContent(addMemberData, Encoding.UTF8, "application/json");
                                        HttpResponseMessage response12 = client.PostAsync(updateEventURL, content1).Result;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                vh.CreateVariable(Output, JsonConvert.SerializeObject(otpt), "", true, "datatable", step.Robot_Id, executionID);
                new ExecutionLog().add2Log("Create mail is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);

            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public class ToOutlookCalendar
        {
            public ToOutlookCalendar()
            {
                Attendees = new List<Attendee>();
            }
            [JsonProperty("subject")]
            public string Subject { get; set; }

            [JsonProperty("body")]
            public Body Body { get; set; }

            [JsonProperty("start")]
            public End Start { get; set; }

            [JsonProperty("end")]
            public End End { get; set; }

            [JsonProperty("attendees")]
            public List<Attendee> Attendees { get; set; }

            [JsonProperty("location")]
            public LocationName Location { get; set; }
        }

        public class Attendee
        {
            [JsonProperty("emailAddress")]
            public EmailAddress EmailAddress { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }
        }

        public class EmailAddress
        {
            [JsonProperty("address")]
            public string Address { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }
        }

        public class Body
        {
            [JsonProperty("contentType")]
            public string ContentType { get; set; }

            [JsonProperty("content")]
            public string Content { get; set; }
        }

        public class LocationName
        {
            [JsonProperty("displayName")]
            public string DisplayName { get; set; }
        }

        public class End
        {
            [JsonProperty("dateTime")]
            public string DateTime { get; set; }

            [JsonProperty("timeZone")]
            public string TimeZone { get; set; }
        }

        public ToOutlookCalendar CreateObject()
        {
            ToOutlookCalendar toOutlookCalendar = new ToOutlookCalendar
            {
                Subject = "Employee test",
                Body = new Body
                {
                    ContentType = "HTML",
                    Content = "Testing outlook service"
                },
                Start = new End
                {
                    DateTime = "2020-04-12T12:00:00",
                    TimeZone = "Pacific Standard Time"
                },
                End = new End
                {
                    DateTime = "2020-04-12T15:00:00",
                    TimeZone = "Pacific Standard Time"
                },
                Location = new LocationName
                {
                    DisplayName = "Teams"
                },
                Attendees = new List<Attendee>
                {
                   new Attendee
                   {
                       EmailAddress = new EmailAddress
                       {
                           Address = eventUserName + "@genbase.onmicrosoft.com",
                           Name = employeeName
                       },
                       Type = "Required"
                   }
                }


            };
            return toOutlookCalendar;
        }

        public ToOutlookCalendar UpdateObject()
        {
            ToOutlookCalendar toOutlookCalendar = new ToOutlookCalendar
            {
                Attendees = new List<Attendee>
                {
                   new Attendee
                   {
                       EmailAddress = new EmailAddress
                       {
                           Address = eventUserName + "@genbase.onmicrosoft.com",
                           Name = employeeName
                       },
                       Type = "Required"
                   }
                }


            };
            return toOutlookCalendar;
        }
    }
}
