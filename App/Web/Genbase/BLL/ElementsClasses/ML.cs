﻿using Genbase.classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Web;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Configuration;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Security;
using System.Net;
using System.Net.Sockets;
using System.Collections.Specialized;
using Genbase.Classes;

namespace Genbase.BLL.ElementsClasses
{
    /// <summary>
    /// Need to execute Anaconda/Python 3 and install tensorflow,numpy 1.16.1 packages
    /// Python script reference url https://www.tensorflow.org/hub/tutorials/image_retraining
    /// Reference-url for executing python script using process https://stackoverflow.com/questions/49082312/activating-conda-environment-from-c-sharp-code-or-what-is-the-differences-betwe
    /// </summary>
    public class ML
    {
        //VariableHandler vh = new VariableHandler();
        StepPropertyHandler stp = new StepPropertyHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        DataTable dt = new DataTable();
        /// <summary>
        /// training script-python D:\Stable\trunk\AppGenerator\Genbase\\Files\retrain.py --image_dir D:\Stable\trunk\AppGenerator\Genbase\ML\flower_photos
        /// </summary>
        /// <param name="stepproperties"></param>
        /// <param name="memberName"></param>
        /// <param name="step"></param>
        /// <returns></returns>
        //public MethodResult TrainImages( Steps step, string executionID)
        //{
        //    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
        //    string ret = string.Empty; string input = string.Empty;
        //    try
        //    {
        //        input = stp.getPropertiesValue(StringHelper.input, step);
        //        List<string> files = new List<string>();
        //        try
        //        {
        //            if (input.Trim() != string.Empty && new VariableHandler().checkVariableExists(input, executionID))
        //            {
        //                System.Data.DataTable dtattachments = new TypeHandler().getInput(input, "variable", executionID);
        //                try
        //                {
        //                    files = dtattachments.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
        //                }
        //                catch (Exception e)
        //                {
        //                    string exception = e.Message;
        //                    files = dtattachments.AsEnumerable().Select(r => r.Field<string>("Output")).ToList();
        //                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //                    s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
        //                }
        //            }

        //        }
        //        catch (Exception e)
        //        {
        //             new Logger().LogException(e);
        //        new ExecutionLog().add2Log(e, step, StringHelper.server, StringHelper.exceptioncode);
        //        msgs.Add(new MethodResultHandler().createMessage(e.Message, StringHelper.exceptioncode));
        //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //        s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
        //        }
        //        SecureString password = new SecureString();
        //        NameValueCollection credentials = (NameValueCollection)ConfigurationManager.GetSection("PSServerCredentials");
        //        if (credentials["server"] != "" && new ML().GetLocalIPAddress() == credentials["server"] && credentials["username"] != "" && credentials["password"] != "")
        //        {
        //            foreach (char x in credentials["password"])
        //            {
        //                password.AppendChar(x);
        //            }
        //            PSCredential credential = new PSCredential(credentials["username"], password);

        //            WSManConnectionInfo connectionInfo = new WSManConnectionInfo(false, credentials["server"], 5985, "/wsman", "http://schemas.microsoft.com/powershell/Microsoft.PowerShell", credential, 1 * 60 * 1000);
        //            Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo);
        //            runspace.Open();

        //            var rootDirectory = AppDomain.CurrentDomain.BaseDirectory;
        //            string path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)));
        //            path = path.Substring(6);

        //            using (PowerShell ps = PowerShell.Create())
        //            {

        //                ps.Runspace = runspace;
        //                foreach (var file in files)
        //                {
        //                    new ExecutionLog().add2Log("ML TrainImages--->Adding Script", step.Robot_Id, step.Name, StringHelper.exceptioncode,false);
        //                    ps.AddScript("python \"" + rootDirectory + "PythonScripts\\retrain.py\" --image_dir \"" + file + "\" --output_graph=\"" + path + "\\pythonLogs\\mlLogs\\tmp\\output_graph.pb" + "\" --intermediate_output_graphs_dir=\"" + path + "\\pythonLogs\\mlLogs\\\\" + "\" --output_labels=\"" + path + "\\pythonLogs\\mlLogs\\tmp\\output_labels.txt" + "\" --summaries_dir=\"" + path + "\\pythonLogs\\mlLogs\\retrain_logs\\\\" + "\" --bottleneck=\"" + path + "\\pythonLogs\\mlLogs\\bottleneck\\\\" + "\"");
        //                }
        //                new ExecutionLog().add2Log("ML TrainImages--->Script Invoked", step.Robot_Id, step.Name, StringHelper.exceptioncode,false);
        //                var results = ps.Invoke();
        //                new ExecutionLog().add2Log("ML TrainImages--->" + results.ToString(), step.Robot_Id, step.Name, StringHelper.exceptioncode,false);
        //            }
        //            runspace.Close();
        //            ret = StringHelper.success;
        //        }
        //        else
        //        {
        //            msgs.Add(new MethodResultHandler().createMessage("Server not found.Modify Server Details in Configuration File", StringHelper.exceptioncode));
        //            new ExecutionLog().add2Log("IP Incorrect.Modify Server Details in Configuration File", step.Robot_Id, step.Name, StringHelper.exceptioncode,false);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
        //        new Logger().LogException(ex);
        //        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
        //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
        //    }
        //    MethodResult mr;
        //    if (ret == StringHelper.success)
        //    {
        //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //        s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
        //        mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
        //    }
        //    else
        //    {
        //        mr = new MethodResultHandler().createResultType(false, msgs, null, s);
        //    }

        //    return mr;
        //}
        ///// <summary>
        /// Classifies the Images using the trained data
        /// classify script-python D:\Stable\trunk\AppGenerator\Genbase\Files\label_image.py --graph=/tmp/output_graph.pb --labels=/tmp/output_labels.txt --input_layer=Placeholder --output_layer=final_result --image=D:\Stable\trunk\AppGenerator\Genbase\ML\flower_photos\daisy\n.jpg
        /// </summary>
        /// <param name="stepproperties"></param>
        /// <param name="memberName"></param>
        /// <param name="step"></param>
        /// <returns></returns>
        //public MethodResult ClassifyImage( Steps step, string executionID)
        //{
        //    StatusModel s = new StatusModel();
        //    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
        //    string ret = string.Empty; string input = string.Empty; string output = string.Empty;
        //    DataTable resultString = new DataTable();
        //    DataTable resultdt = new DataTable();
        //    string result = String.Empty;
        //    StringBuilder st = new StringBuilder();
        //    resultdt.Columns.Add("Output");
        //    resultString.Columns.Add("Output");
        //    try
        //    {
        //        input = stp.getPropertiesValue(StringHelper.input, step);
        //        output = stp.getPropertiesValue(StringHelper.output, step);
        //        List<string> files = new List<string>();
        //        try
        //        {
        //            if (input.Trim() != string.Empty && new VariableHandler().checkVariableExists(input, executionID))
        //            {
        //                System.Data.DataTable dtattachments = new TypeHandler().getInput(input, "variable", executionID);
        //                try
        //                {
        //                    files = dtattachments.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
        //                }
        //                catch (Exception e)
        //                {
        //                    string exception = e.Message;
        //                    files = dtattachments.AsEnumerable().Select(r => r.Field<string>("Output")).ToList();
        //                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //                    s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
        //                }
        //            }

        //        }
        //        catch (Exception e)
        //        {
        //            string ec = e.Message;
        //            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //            s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
        //        }
        //        var rootDirectory = AppDomain.CurrentDomain.BaseDirectory;
        //        var process = new Process
        //        {
        //            StartInfo = new ProcessStartInfo
        //            {
        //                FileName = "cmd.exe",
        //                RedirectStandardInput = true,
        //                UseShellExecute = false,
        //                RedirectStandardOutput = true,
        //                WorkingDirectory = Environment.GetFolderPath(Environment.SpecialFolder.System)
        //            }
        //        };
        //        string path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)));
        //        path = path.Substring(6);
        //        process.Start();
        //        // Pass multiple commands to cmd.exe
        //        using (var sw = process.StandardInput)
        //        {
        //            if (sw.BaseStream.CanWrite)
        //            {
        //                // Vital to activate Anaconda
        //                //sw.WriteLine("\""+ConfigurationManager.AppSettings["pythonLink"].ToString() + "\\activate.bat\"");
        //                // Any other commands you want to run

        //                sw.WriteLine("set KERAS_BACKEND=tensorflow");

        //                // run your script. You can also pass in arguments
        //                foreach (var file in files)
        //                {
        //                    sw.WriteLine("python \"" + rootDirectory + "\\PythonScripts\\mlscripts\\label_image.py\" --graph=" + path + "pythonLogs/mlScripts/tmp/output_graph.pb --labels=" + path + "pythonLogs/mlScripts/tmp/output_labels.txt --input_layer=Placeholder --output_layer=final_result --image=\"" + file + "\"");
        //                }

        //            }
        //        }

        //        for (var i = 0; !process.StandardOutput.EndOfStream; i++)
        //        {
        //            var line = process.StandardOutput.ReadLine();
        //            if (i >= 6)
        //            {
        //                if (line.IndexOf('>') == -1)
        //                    resultString.Rows.Add(line);
        //            }
        //            if (i == 6)
        //            {
        //                if (line == "")
        //                    line = "No Matches!";
        //                resultdt.Rows.Add(line);
        //            }

        //        }
        //        ret = StringHelper.success;
        //    }
        //    catch (Exception ex)
        //    {
        //        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
        //        new Logger().LogException(ex);
        //        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
        //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
        //    }
        //    MethodResult mr;
        //    if (ret == StringHelper.success)
        //    {
        //        vh.CreateVariable(output, JsonConvert.SerializeObject(resultdt), StringHelper.truee, true, "datatable", step.Robot_Id, executionID);
        //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
        //        s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
        //        mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
        //        string het = new TypeHandler().ConvertDataTableToString(resultString);
        //        new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
        //    }
        //    else
        //    {
        //        mr = new MethodResultHandler().createResultType(false, msgs, null, s);
        //    }

        //    return mr;
        //}
        public string GetLocalIPAddress()
        {
            string result = string.Empty;
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    result = ip.ToString();
                }
            }
            return result;
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
        public MethodResult TrainImages(Steps step, string executionID)
        {
            string ret = string.Empty;
            MethodResult mr = new MethodResult();
            DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string inp = String.Empty; bool isthere = false;
            string input = String.Empty;
            string output = String.Empty;
            returnValue.Columns.Add("Training Response");
            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                string outputString = "";
                try
                {
                    dt = new TypeHandler().getInput(input, StringHelper.variable, executionID);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        inp = dt.Rows[i].ItemArray[0].ToString();
                    }
                    if (inp.Contains(".zip"))
                    {

                        outputString = FileEntity("http://99.148.66.155:5002/ML/Train", inp, executionID, step, input);

                        foreach (var l in Variables.dynamicvariables)
                        {
                            if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                            {
                                isthere = true;
                                dt.Rows.Add(l.vlvalue);
                                l.vlvalue = dt;
                                l.ExecutionID = executionID;
                                l.RobotID = step.Robot_Id;
                            }
                        }
                        if (!isthere)
                        {
                            DVariables d = new DVariables();
                            d.vlname = output;
                            d.vlstatus = StringHelper.truee;
                            d.vlvalue = dt;
                            d.vltype = StringHelper.datatable;
                            d.ExecutionID = executionID;
                            d.RobotID = step.Robot_Id;
                            Variables.dynamicvariables.Add(d);
                        }
                        goto retur;
                    }
                retur: returnValue.Rows.Add(outputString);
                    ret = StringHelper.success;
                }
                catch (Exception e)
                {
                    new Logger().LogException(e);
                    new ExecutionLog().add2Log(e, step, StringHelper.server, StringHelper.exceptioncode);
                    msgs.Add(new MethodResultHandler().createMessage(e.Message, StringHelper.exceptioncode));
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult ClassifyImage(Steps step, string executionID)
        {
            string ret = string.Empty;
            MethodResult mr = new MethodResult();
            DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string inp = String.Empty; bool isthere = false;
            string input = String.Empty;
            string output = String.Empty;
            returnValue.Columns.Add("Classified Image Response");
            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                string outputString = "";
                var imageTag = string.Empty;
                try
                {
                    dt = new TypeHandler().getInput(input, StringHelper.variable, executionID);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        inp = dt.Rows[i].ItemArray[0].ToString();
                    }
                    if (inp.Contains(".jpg") || inp.Contains(".png") || inp.Contains(".jpeg"))
                    {

                        outputString = FileEntity("http://99.148.66.155:5002/ML/FaceRecognize", inp, executionID, step, input);
                        var imgSrc = String.Format("data:image/jpeg;base64,{0}", outputString);
                        imageTag = "<img src=" + imgSrc + "  height='300px' width='400px'/>";

                        foreach (var l in Variables.dynamicvariables)
                        {
                            if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                            {
                                isthere = true;
                                dt.Rows.Add(l.vlvalue);
                                l.vlvalue = dt;
                                l.ExecutionID = executionID;
                                l.RobotID = step.Robot_Id;
                            }
                        }
                        if (!isthere)
                        {
                            DVariables d = new DVariables();
                            d.vlname = output;
                            d.vlstatus = StringHelper.truee;
                            d.vlvalue = dt;
                            d.vltype = StringHelper.datatable;
                            d.ExecutionID = executionID;
                            d.RobotID = step.Robot_Id;
                            Variables.dynamicvariables.Add(d);
                        }
                        goto retur;
                    }
                retur: returnValue.Rows.Add(imageTag);
                    ret = StringHelper.success;
                }
                catch (Exception e)
                {
                    new Logger().LogException(e);
                    new ExecutionLog().add2Log(e, step, StringHelper.server, StringHelper.exceptioncode);
                    msgs.Add(new MethodResultHandler().createMessage(e.Message, StringHelper.exceptioncode));
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        private string FileEntity(string serviceURL, string filePath, string executionID, Steps step, string input)
        {
            DataTable returnValue = new DataTable();
            returnValue.Columns.Add(StringHelper.Content); string output = String.Empty;
            string filename = new StringFunction().getFileNameFromPath(filePath);
            string endPoint = serviceURL;
            string textinimage = HttpUploadFile(endPoint, filePath, "file", "application/json", new NameValueCollection());
            textinimage = textinimage.Replace('[', ' ');
            textinimage = textinimage.Replace(']', ' ');
            return textinimage;
        }
        public string HttpUploadFile(string url, string file, string paramName, string contentType, NameValueCollection nvc)
        {
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = System.Net.CredentialCache.DefaultCredentials;
            Stream rs = wr.GetRequestStream();
            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);
            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, file, contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);
            FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();
            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();
            WebResponse wresp = null;
            string result = string.Empty;
            try
            {
                wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader reader2 = new StreamReader(stream2, encode);
                result = reader2.ReadToEnd();

            }
            catch (Exception ex)
            {
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                    result = ex.Message;
                }
            }
            finally
            {
                wr = null;
            }
            return result;
        }

    }
}