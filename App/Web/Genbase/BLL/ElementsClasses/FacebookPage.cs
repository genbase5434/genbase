﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Genbase.classes;
using Facebook;
using Newtonsoft.Json;
using System.Data;
using Genbase.Classes;

namespace Genbase.BLL.ElementsClasses
{
    public class FacebookPage
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult Get( Steps step, string executionID)
        {
            //string AccessToken = string.Empty; string fbid = string.Empty; string Input = string.Empty; string Output = string.Empty;
            //string ret = string.Empty;
            //List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            //try
            //{

            //    AccessToken = stp.getPropertiesValue(StringHelper.accesstoken, step);
            //    fbid = stp.getPropertiesValue(StringHelper.fbid, step);
            //    Input = stp.getPropertiesValue(StringHelper.input, step);
            //    Output = stp.getPropertiesValue(StringHelper.output, step);

            //    DVariables value = new DVariables();
            //    if (Input.Trim() != string.Empty)
            //    {
            //        value = new VariableHandler().getVariables(Input);
            //    }


            //    var fb = new FacebookClient(AccessToken);
            //    dynamic feed = fb.Get(fbid + "/posts");
            //    JsonArray fbarray = feed.data;

            //    List<DFB> fbPosts = new List<DFB>();
            //    List<PostDetails> Posts = new List<PostDetails>();

            //    var F = fbarray.ToList();
            //    DataTable dt = new DataTable();
            //    dt.Columns.Add(StringHelper.id);
            //    dt.Columns.Add(StringHelper.postid);
            //    dt.Columns.Add(StringHelper.likes);
            //    dt.Columns.Add(StringHelper.likes);
            //    dt.Columns.Add(StringHelper.shares);
            //    for (int i = 0; i < F.Count; i++)
            //    {
            //        var post = (((Facebook.JsonObject)(F[i])).Values).ToList();
            //        var postid = post[2];

            //        dynamic postDetails = fb.Get(postid + "?fields= shares,\n likes.summary(true), \n comments.summary(true),\n message.summary(true)");

            //        JsonArray postArray = postDetails.data;

            //        var likes = postDetails.likes.summary;
            //        Likes ikes = new Likes();
            //        ikes.total_count = Convert.ToInt32(likes[0]);
            //        ikes.can_like = likes[1];
            //        ikes.has_liked = likes[2];

            //        var comments = postDetails.comments.summary;
            //        Comments omment = new Comments();
            //        omment.total_count = Convert.ToInt32(comments[1]);
            //        omment.order = comments[0];
            //        omment.can_comment = comments[2];

            //        var messages = postDetails.message;
            //        var id = postDetails.id;

            //        PostDetails pd = new PostDetails();
            //        pd.comments = omment;
            //        pd.likes = ikes;
            //        pd.message = messages;
            //        pd.postid = id;
            //        Posts.Add(pd);

            //        dt.Rows.Add(i, postid, ikes.total_count.ToString(), omment.total_count.ToString(), "0");

            //    }
            //    ret = JsonConvert.SerializeObject(dt);
            //    if (Output != string.Empty)
            //    {
            //        new VariableHandler().CreateVariable(Output, ret, "", true);
            //    }

            //    ret = StringHelper.success;
            //}
            //catch (Exception ex)
            //{
            //    new Logger().LogException(ex);
            //    new ExecutionLog().add2Log(ex.Message, step.Robot_Id, step.Name, StringHelper.exceptioncode);
            //    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
            //}
            MethodResult mr = new MethodResult();
            //if (ret == StringHelper.success)
            //{
            //    mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables);
            //}
            //else
            //{
            //    mr = new MethodResultHandler().createResultType(false, msgs, null);
            //}
            return mr;
        }

        public MethodResult Post( Steps step, string executionID)
        {
            string AccessToken = string.Empty; string message = string.Empty; string filepath = string.Empty;
            string Input = string.Empty; string Output = string.Empty;
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                AccessToken = stp.getPropertiesValue(StringHelper.accesstokens, step);
                message = stp.getPropertiesValue(StringHelper.message, step);
                filepath = stp.getPropertiesValue(StringHelper.filepath, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);

                var fb = new FacebookClient(AccessToken);
                using (var file = new FacebookMediaStream
                {
                    ContentType = StringHelper.imagejpeg,
                    FileName = Path.GetFileName(@filepath)
                }.SetValue(System.IO.File.OpenRead(@filepath)))
                {
                    dynamic result = fb.Post(StringHelper.photos, new { message = StringHelper.uploadusingsdk, file });
                }

                if (Output != string.Empty)
                {
                    //new VariableHandler().CreateVariable(Output, feed, "", true);
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.errorcode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

    }
}