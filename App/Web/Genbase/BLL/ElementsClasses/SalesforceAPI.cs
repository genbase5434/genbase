﻿using System;
using System.Collections.Generic;
using Genbase.classes;
using System.Data;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Salesforce.Force;
using System.Net;
using System.Net.Http.Headers;
using System.Linq;
using System.Text;
using Genbase.Classes;

namespace Genbase.BLL.ElementsClasses
{
    public class SalesforceAPI
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        EncoderDecoderBase64 baser = new EncoderDecoderBase64();
        DAL.QueryUtil query = new DAL.QueryUtil();
        DAL.PostgresConnect db = new DAL.PostgresConnect();
        DAL.MySQL sql = new DAL.MySQL();

        string sfdcConsumerKey = "3MVG9KsVczVNcM8w2UBdsOIPDpPP0TUL_IZk4IByAQ.KZ8t.A_shTnTE5YmP7ZhCpTSiT9zZwnw==";

        string sfdcConsumerSecret = "DD2EA86E308FCA5DCE1501A791749A78AF2C8E0AB2CDEC64606241CB569923A4";
        DataTable dt = new DataTable();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult Login( Steps step, string executionID)
        {
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string securitytoken = string.Empty; string username = string.Empty; string password = string.Empty;
            string input = string.Empty; string output = string.Empty;

            securitytoken = stp.getPropertiesValue(StringHelper.securitytoken, step);
            username = stp.getPropertiesValue(StringHelper.username, step);
            password = stp.getPropertiesValue(StringHelper.password, step);
            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);


            DataTable returnValue = new DataTable();
            returnValue.Columns.Add(StringHelper.Username);
            returnValue.Columns.Add(StringHelper.Password);
            returnValue.Columns.Add(StringHelper.securitytoken);

            try
            {
                SalesforceCredentials Sc = new SalesforceCredentials();
                Sc.username = username;
                Sc.password = password;
                Sc.securitytoken = securitytoken;

                returnValue.Rows.Add(username, Base64Encode(password), Base64Encode(securitytoken));

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, null, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult CreateCase( Steps step, string executionID)
        {
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string columnname = string.Empty; string valuee = string.Empty;
            string input = string.Empty; string inputtype = string.Empty; string output = string.Empty;



            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
            columnname = stp.getPropertiesValue(StringHelper.columnname, step);
            valuee = stp.getPropertiesValue(StringHelper.value, step);

            DVariables value = new DVariables();
            if (input.Trim() != string.Empty)
            {
                value = new VariableHandler().getVariables(input, executionID);
            }

            DataTable returnValue = new TypeHandler().getInput(input, StringHelper.variable, executionID);
            string sfdcusername = returnValue.Rows[0].Field<string>(StringHelper.Username).Trim();
            string sfdcpwd = returnValue.Rows[0].Field<string>(StringHelper.Password).Trim();
            string sfdcsectoken = returnValue.Rows[0].Field<string>(StringHelper.Password).Trim();
            string sfdcpassword = Base64Decode(sfdcpwd);
            string sfdcsecuritytoken = Base64Decode(sfdcsectoken);
            string sfdcloginPassword = sfdcpassword + sfdcsecuritytoken;
            try
            {
                //var auth = new Salesforce.Common.AuthenticationClient();
                //auth.UsernamePasswordAsync(sfdcConsumerKey, sfdcConsumerSecret, sfdcusername, sfdcloginPassword).Wait();
                //var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                //var results = client.QueryAsync<dynamic>("SELECT AccountId,AssetId,CaseNumber,ClosedDate,CreatedById FROM Case");
                //results.Wait();
                //var newCase = new { Subject = "My System Not Working from a day" };
                //var clientId = client.CreateAsync("Case", newCase);
                //clientId.Wait();

                string authToken = "";

                string ServiceURL = "http://login.salesforce.com";

                var dictionaryForUrl = new Dictionary<string, string>

                 {

                   {"grant_type","password" },

                   {"client_id",sfdcConsumerKey },

                   {"client_secret",sfdcConsumerSecret },

                   {"username",sfdcusername },

                   {"password",sfdcloginPassword }

               };

                HttpClient authc = new HttpClient();

                HttpContent httpContent = new FormUrlEncodedContent(dictionaryForUrl);

                Task<HttpResponseMessage> httpresponse = authc.PostAsync("https://na85.salesforce.com/services/oauth2/token", httpContent);

                httpresponse.Wait();

                Task<string> message = httpresponse.Result.Content.ReadAsStringAsync();

                message.Wait();

                JObject jsonObj = JObject.Parse(message.Result);

                authToken = (string)jsonObj["access_token"];

                ServiceURL = (string)jsonObj["instance_url"];

                HttpClient apiCallClient = new HttpClient();

                var newCase = new { Subject = "My System Not Working from a day" };
                var values = new Dictionary<string, string>();
                values["Subject"] = "My System Not Working from a day";


                var content = new StringContent(JsonConvert.SerializeObject(values), Encoding.UTF8, "application/json");
                var json = JsonConvert.SerializeObject(newCase);
                String restCallURL = ServiceURL + "/services/data/v44.0/sobjects/Case/";
                HttpRequestMessage apirequest = new HttpRequestMessage(HttpMethod.Post, restCallURL);
                apirequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                apirequest.Headers.Add("authorization", "Bearer " + authToken);
                apirequest.Content = content;
                Task<HttpResponseMessage> apiCallResponse = apiCallClient.SendAsync(apirequest);
                apiCallResponse.Wait();
                Task<string> requestresponse = apiCallResponse.Result.Content.ReadAsStringAsync();
                requestresponse.Wait();

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, null, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult UpdateCase( Steps step, string executionID)
        {
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string columnname = string.Empty; string valuee = string.Empty; string condition = string.Empty;
            string input = string.Empty; string inputtype = string.Empty; string output = string.Empty;



            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
            columnname = stp.getPropertiesValue(StringHelper.columnname, step);
            valuee = stp.getPropertiesValue(StringHelper.value, step);
            condition = stp.getPropertiesValue(StringHelper.condition, step);


            DataTable result = new DataTable();
            result.Columns.Add("Result");
            DVariables value = new DVariables();
            if (input.Trim() != string.Empty)
            {
                value = new VariableHandler().getVariables(input, executionID);
            }

            DataTable returnValue = new TypeHandler().getInput(input, StringHelper.variable, executionID);
            string sfdcusername = returnValue.Rows[0].Field<string>(StringHelper.Username).Trim();
            string sfdcpwd = returnValue.Rows[0].Field<string>(StringHelper.Password).Trim();
            string sfdcsectoken = returnValue.Rows[0].Field<string>(StringHelper.securitytoken).Trim();
            string sfdcpassword = Base64Decode(sfdcpwd);
            string sfdcsecuritytoken = Base64Decode(sfdcsectoken);
            string sfdcloginPassword = sfdcpassword + sfdcsecuritytoken;

            try
            {

                string authToken = "";

                string ServiceURL = "http://login.salesforce.com";
                var dictionaryForUrl = new Dictionary<string, string>
                   {

                     {"grant_type","password" },

                     {"client_id",sfdcConsumerKey },

                      {"client_secret",sfdcConsumerSecret },

                      {"username",sfdcusername },

                     {"password",sfdcloginPassword }

                   };

                ServicePointManager.Expect100Continue = true;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpClient authc = new HttpClient();

                HttpContent httpContent = new FormUrlEncodedContent(dictionaryForUrl);
                Task<HttpResponseMessage> httpresponse = authc.PostAsync("https://na85.salesforce.com/services/oauth2/token", httpContent);
                httpresponse.Wait();
                Task<string> message = httpresponse.Result.Content.ReadAsStringAsync();
                message.Wait();
                JObject jsonObj = JObject.Parse(message.Result);

                authToken = (string)jsonObj["access_token"];

                ServiceURL = (string)jsonObj["instance_url"];

                HttpClient apiCallClient = new HttpClient();

                var values = new Dictionary<string, string>();

                values["Subject"] = "My System Not Working Properly";

                var content = new StringContent(JsonConvert.SerializeObject(values), Encoding.UTF8, "application/json");

                String restCallURL = ServiceURL + "/services/data/v44.0/sobjects/Case/5001U000007OsNRQA0";

                HttpRequestMessage apirequest = new HttpRequestMessage(new HttpMethod("PATCH"), restCallURL);

                apirequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                apirequest.Headers.Add("authorization", "Bearer " + authToken);

                apirequest.Content = content;

                Task<HttpResponseMessage> apiCallResponse = apiCallClient.SendAsync(apirequest);
                apiCallResponse.Wait();
                Task<string> requestresponse = apiCallResponse.Result.Content.ReadAsStringAsync();
                requestresponse.Wait();

                ret = StringHelper.success;

            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, null, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult DeleteCase( Steps step, string executionID)
        {
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string dburl = string.Empty; string username = string.Empty; string password = string.Empty;
            string input = string.Empty; string inputtype = string.Empty; string output = string.Empty;


            dburl = stp.getPropertiesValue(StringHelper.dburl, step);
            username = stp.getPropertiesValue(StringHelper.dbusername, step);
            password = stp.getPropertiesValue(StringHelper.dbpassword, step);
            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);


            DataTable returnValue = new DataTable();

            try
            {

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, null, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult GetCase( Steps step, string executionID)
        {
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string input = string.Empty; string inputtype = string.Empty; string output = string.Empty;

            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
            DataTable result = new DataTable();
            result.Columns.Add("Result");
            DVariables value = new DVariables();
            if (input.Trim() != string.Empty)
            {
                value = new VariableHandler().getVariables(input, executionID);
            }

            DataTable returnValue = new TypeHandler().getInput(input, StringHelper.variable, executionID);
            string sfdcusername = returnValue.Rows[0].Field<string>(StringHelper.Username).Trim();
            string sfdcpwd = returnValue.Rows[0].Field<string>(StringHelper.Password).Trim();
            string sfdcsectoken = returnValue.Rows[0].Field<string>(StringHelper.securitytoken).Trim();
            string sfdcpassword = Base64Decode(sfdcpwd);
            string sfdcsecuritytoken = Base64Decode(sfdcsectoken);
            string sfdcloginPassword = sfdcpassword + sfdcsecuritytoken;
            string authToken = "";
            string ServiceURL = "http://login.salesforce.com";

            try
            {
                var dictionaryForUrl = new Dictionary<string, string>
            {
                {"grant_type","password" },
                {"client_id",sfdcConsumerKey },
                {"client_secret",sfdcConsumerSecret },
                {"username",sfdcusername },
                {"password",sfdcloginPassword }
            };
                ServicePointManager.Expect100Continue = true;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpClient authc = new HttpClient();

                HttpContent httpContent = new FormUrlEncodedContent(dictionaryForUrl);
                Task<HttpResponseMessage> httpresponse = authc.PostAsync("https://na85.salesforce.com/services/oauth2/token", httpContent);
                httpresponse.Wait();
                Task<string> message = httpresponse.Result.Content.ReadAsStringAsync();
                message.Wait();

                JObject jsonObj = JObject.Parse(message.Result);
                authToken = (string)jsonObj["access_token"];
                ServiceURL = (string)jsonObj["instance_url"];

                HttpClient apiCallClient = new HttpClient();
                String restCallURL = ServiceURL + "/services/data/v44.0/query/?q=Select+Subject+from+Case";
                HttpRequestMessage apirequest = new HttpRequestMessage(HttpMethod.Get, restCallURL);
                apirequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                apirequest.Headers.Add("authorization", "Bearer " + authToken);
                Task<HttpResponseMessage> apiCallResponse = apiCallClient.SendAsync(apirequest);
                apiCallResponse.Wait();
                Task<string> requestresponse = apiCallResponse.Result.Content.ReadAsStringAsync();
                requestresponse.Wait();
                List<String> sObjLst = new List<String>();
                if (apiCallResponse.Result.IsSuccessStatusCode)
                {
                    JObject sObjJObj = JObject.Parse(requestresponse.Result);
                    JToken tokens = sObjJObj["records"];
                    if (tokens.Children().Count() == 0)
                    {
                        Console.WriteLine(tokens.ToString());
                    }
                    else if (tokens.Children().Count() > 0)
                    {
                        foreach (JToken jt in tokens.Children())
                        {
                            foreach (JProperty jp in jt)
                            {
                                if (jp.Name != "attributes")
                                {
                                    sObjLst.Add(jp.Value.ToString());

                                }
                            }
                        }
                        dt = ListtoDatatable(sObjLst);
                        new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(dt), "", true, "datatable", step.Robot_Id, executionID);
                    }
                }
                else if (!(apiCallResponse.Result.IsSuccessStatusCode))
                {
                }

                //ServicePointManager.Expect100Continue = true;

                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                //var auth = new Salesforce.Common.AuthenticationClient();

                //auth.UsernamePasswordAsync(sfdcConsumerKey, sfdcConsumerSecret, sfdcusername, sfdcloginPassword).Wait();
                //var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                //var results = client.QueryAsync<dynamic>("SELECT AccountId,AssetId,CaseNumber,ClosedDate,CreatedById FROM Case");
                //results.Wait();
                //result.Rows.Add(results.Result.Records);
                //var newCase = new { Subject = "My System Not Working from a day" };
                //var clientId = client.CreateAsync("Case", newCase);
                //clientId.Wait();

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(dt);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(dt), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, null, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult CreateUser( Steps step, string executionID)
        {
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string columnname = string.Empty; string valuee = string.Empty;
            string input = string.Empty; string inputtype = string.Empty; string output = string.Empty;




            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
            columnname = stp.getPropertiesValue(StringHelper.columnname, step);
            valuee = stp.getPropertiesValue(StringHelper.value, step);


            DataTable result = new DataTable();
            result.Columns.Add("Result");
            DVariables value = new DVariables();
            if (input.Trim() != string.Empty)
            {
                value = new VariableHandler().getVariables(input, executionID);
            }

            DataTable returnValue = new TypeHandler().getInput(input, StringHelper.variable, executionID);
            string sfdcusername = returnValue.Rows[0].Field<string>(StringHelper.Username).Trim();
            string sfdcpwd = returnValue.Rows[0].Field<string>(StringHelper.Password).Trim();
            string sfdcsectoken = returnValue.Rows[0].Field<string>(StringHelper.securitytoken).Trim();
            string sfdcpassword = Base64Decode(sfdcpwd);
            string sfdcsecuritytoken = Base64Decode(sfdcsectoken);
            string sfdcloginPassword = sfdcpassword + sfdcsecuritytoken;
            try
            {
                //ServicePointManager.Expect100Continue = true;

                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                //var auth = new Salesforce.Common.AuthenticationClient();

                //auth.UsernamePasswordAsync(sfdcConsumerKey, sfdcConsumerSecret, sfdcusername, sfdcloginPassword).Wait();
                //var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                //var results = client.QueryAsync<dynamic>("SELECT AccountId,AssetId,CaseNumber,ClosedDate,CreatedById FROM Case");
                //results.Wait();
                //result.Rows.Add(results.Result.Records);
                //var newCase = new { Subject = "My System Not Working from a day" };
                //var clientId = client.CreateAsync("Case", newCase);
                //clientId.Wait();

                string authToken = "";

                string ServiceURL = "http://login.salesforce.com";

                var dictionaryForUrl = new Dictionary<string, string>

                 {

                   {"grant_type","password" },

                   {"client_id",sfdcConsumerKey },

                   {"client_secret",sfdcConsumerSecret },

                   {"username",sfdcusername },

                   {"password",sfdcloginPassword }

               };

                HttpClient authc = new HttpClient();

                HttpContent httpContent = new FormUrlEncodedContent(dictionaryForUrl);

                Task<HttpResponseMessage> httpresponse = authc.PostAsync("https://na85.salesforce.com/services/oauth2/token", httpContent);

                httpresponse.Wait();

                Task<string> message = httpresponse.Result.Content.ReadAsStringAsync();

                message.Wait();

                JObject jsonObj = JObject.Parse(message.Result);

                authToken = (string)jsonObj["access_token"];

                ServiceURL = (string)jsonObj["instance_url"];


                HttpClient apiCallClient = new HttpClient();

                var newCase = new { Name = "New User Name" };
                var values = new Dictionary<string, string>();
                values["Name"] = "New User Name";


                var content = new StringContent(JsonConvert.SerializeObject(values), Encoding.UTF8, "application/json");
                var json = JsonConvert.SerializeObject(newCase);
                String restCallURL = ServiceURL + "/services/data/v44.0/sobjects/User/";
                HttpRequestMessage apirequest = new HttpRequestMessage(HttpMethod.Post, restCallURL);
                apirequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                apirequest.Headers.Add("authorization", "Bearer " + authToken);
                apirequest.Content = content;
                Task<HttpResponseMessage> apiCallResponse = apiCallClient.SendAsync(apirequest);
                apiCallResponse.Wait();
                Task<string> requestresponse = apiCallResponse.Result.Content.ReadAsStringAsync();
                requestresponse.Wait();


                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, null, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult UpdateUser( Steps step, string executionID)
        {
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string columnname = string.Empty; string valuee = string.Empty;
            string input = string.Empty; string inputtype = string.Empty; string output = string.Empty;
            string condition = string.Empty;



            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
            columnname = stp.getPropertiesValue(StringHelper.columnname, step);
            columnname = stp.getPropertiesValue(StringHelper.columnname, step);
            valuee = stp.getPropertiesValue(StringHelper.value, step);
            condition = stp.getPropertiesValue(StringHelper.condition, step);


            DataTable result = new DataTable();
            result.Columns.Add("Result");
            DVariables value = new DVariables();
            if (input.Trim() != string.Empty)
            {
                value = new VariableHandler().getVariables(input, executionID);
            }

            DataTable returnValue = new TypeHandler().getInput(input, StringHelper.variable, executionID);
            string sfdcusername = returnValue.Rows[0].Field<string>(StringHelper.Username).Trim();
            string sfdcpwd = returnValue.Rows[0].Field<string>(StringHelper.Password).Trim();
            string sfdcsectoken = returnValue.Rows[0].Field<string>(StringHelper.securitytoken).Trim();
            string sfdcpassword = Base64Decode(sfdcpwd);
            string sfdcsecuritytoken = Base64Decode(sfdcsectoken);
            string sfdcloginPassword = sfdcpassword + sfdcsecuritytoken;
            try
            {
                string authToken = "";

                string ServiceURL = "http://login.salesforce.com";
                var dictionaryForUrl = new Dictionary<string, string>
      {

        {"grant_type","password" },

        {"client_id",sfdcConsumerKey },

        {"client_secret",sfdcConsumerSecret },

        {"username",sfdcusername },

        {"password",sfdcloginPassword }

      };

                ServicePointManager.Expect100Continue = true;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpClient authc = new HttpClient();

                HttpContent httpContent = new FormUrlEncodedContent(dictionaryForUrl);
                Task<HttpResponseMessage> httpresponse = authc.PostAsync("https://na85.salesforce.com/services/oauth2/token", httpContent);
                httpresponse.Wait();
                Task<string> message = httpresponse.Result.Content.ReadAsStringAsync();
                message.Wait();
                JObject jsonObj = JObject.Parse(message.Result);

                authToken = (string)jsonObj["access_token"];

                ServiceURL = (string)jsonObj["instance_url"];

                HttpClient apiCallClient = new HttpClient();

                var values = new Dictionary<string, string>();

                values["Name"] = "New User";

                var content = new StringContent(JsonConvert.SerializeObject(values), Encoding.UTF8, "application/json");

                String restCallURL = ServiceURL + "/services/data/v44.0/sobjects/User/0051U0000056WTBQA2";

                HttpRequestMessage apirequest = new HttpRequestMessage(new HttpMethod("PATCH"), restCallURL);

                apirequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                apirequest.Headers.Add("authorization", "Bearer " + authToken);

                apirequest.Content = content;

                Task<HttpResponseMessage> apiCallResponse = apiCallClient.SendAsync(apirequest);
                apiCallResponse.Wait();
                Task<string> requestresponse = apiCallResponse.Result.Content.ReadAsStringAsync();
                requestresponse.Wait();

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, null, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult DeleteUser( Steps step, string executionID)
        {
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string dburl = string.Empty; string username = string.Empty; string password = string.Empty;
            string input = string.Empty; string inputtype = string.Empty; string output = string.Empty;


            dburl = stp.getPropertiesValue(StringHelper.dburl, step);
            username = stp.getPropertiesValue(StringHelper.dbusername, step);
            password = stp.getPropertiesValue(StringHelper.dbpassword, step);
            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);


            DataTable returnValue = new DataTable();

            try
            {

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, null, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult GetUser( Steps step, string executionID)
        {
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string input = string.Empty; string inputtype = string.Empty; string output = string.Empty; string columnname = string.Empty; string valuee = string.Empty;

            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
            columnname = stp.getPropertiesValue(StringHelper.columnname, step);
            valuee = stp.getPropertiesValue(StringHelper.value, step);
            DataTable result = new DataTable();
            result.Columns.Add("Result");
            DVariables value = new DVariables();
            if (input.Trim() != string.Empty)
            {
                value = new VariableHandler().getVariables(input, executionID);
            }

            DataTable returnValue = new TypeHandler().getInput(input, StringHelper.variable, executionID);
            string sfdcusername = returnValue.Rows[0].Field<string>(StringHelper.Username).Trim();
            string sfdcpwd = returnValue.Rows[0].Field<string>(StringHelper.Password).Trim();
            string sfdcsectoken = returnValue.Rows[0].Field<string>(StringHelper.securitytoken).Trim();
            string sfdcpassword = Base64Decode(sfdcpwd);
            string sfdcsecuritytoken = Base64Decode(sfdcsectoken);
            string sfdcloginPassword = sfdcpassword + sfdcsecuritytoken;
            //salesforcegetuser(sfdcusername, sfdcloginPassword, output);
            try
            {
                string authToken = "";
                string ServiceURL = "http://login.salesforce.com";


                var dictionaryForUrl = new Dictionary<string, string>
            {
                {"grant_type","password" },
                {"client_id",sfdcConsumerKey },
                {"client_secret",sfdcConsumerSecret },
                {"username",sfdcusername },
                {"password",sfdcloginPassword }
            };
                ServicePointManager.Expect100Continue = true;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpClient authc = new HttpClient();

                HttpContent httpContent = new FormUrlEncodedContent(dictionaryForUrl);
                Task<HttpResponseMessage> httpresponse = authc.PostAsync("https://na85.salesforce.com/services/oauth2/token", httpContent);
                httpresponse.Wait();
                Task<string> message = httpresponse.Result.Content.ReadAsStringAsync();
                message.Wait();


                JObject jsonObj = JObject.Parse(message.Result);
                authToken = (string)jsonObj["access_token"];
                ServiceURL = (string)jsonObj["instance_url"];

                HttpClient apiCallClient = new HttpClient();
                String restCallURL = ServiceURL + "/services/data/v44.0/query/?q=Select+Subject+from+Case";
                HttpRequestMessage apirequest = new HttpRequestMessage(HttpMethod.Get, restCallURL);
                apirequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                apirequest.Headers.Add("authorization", "Bearer " + authToken);
                Task<HttpResponseMessage> apiCallResponse = apiCallClient.SendAsync(apirequest);
                apiCallResponse.Wait();
                Task<string> requestresponse = apiCallResponse.Result.Content.ReadAsStringAsync();
                requestresponse.Wait();
                List<String> sObjLst = new List<String>();
                if (apiCallResponse.Result.IsSuccessStatusCode)
                {
                    JObject sObjJObj = JObject.Parse(requestresponse.Result);
                    JToken tokens = sObjJObj["records"];
                    if (tokens.Children().Count() == 0)
                    {
                        Console.WriteLine(tokens.ToString());
                    }
                    else if (tokens.Children().Count() > 0)
                    {
                        foreach (JToken jt in tokens.Children())
                        {
                            foreach (JProperty jp in jt)
                            {
                                if (jp.Name != "attributes")
                                {
                                    sObjLst.Add(jp.Value.ToString());

                                }
                            }
                        }
                        dt = ListtoDatatable(sObjLst);
                        new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(dt), "", true, "datatable", step.Robot_Id, executionID);
                    }
                }
                else if (!(apiCallResponse.Result.IsSuccessStatusCode))
                {
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(dt);
                new VariableHandler().CreateVariable(output, "Connected Succesfully", "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, null, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public DataTable ListtoDatatable(List<string> list)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("0");
            foreach (var l in list)
            {
                dataTable.Rows.Add(l);
            }

            return dataTable;

        }
    }
    public class SalesforceCredentials
    {
        public string username { get; set; }
        public string password { get; set; }
        public string securitytoken { get; set; }

    }
}