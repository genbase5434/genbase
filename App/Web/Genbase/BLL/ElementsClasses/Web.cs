﻿using Genbase.classes;
using System;
using System.Collections.Generic;
using Genbase.Classes;

namespace Genbase.BLL.ElementsClasses
{
    public class Web
    {
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult Open( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            try
            {

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execclient, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Close( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            try
            {

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execclient, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Navigate( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            try
            {

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execclient, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Fill( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            try
            {

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execclient, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Sleep( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            try
            {

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execclient, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult ScreenShot( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            try
            {

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execclient, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Click( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            try
            {

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execclient, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Element( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            try
            {

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execclient, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Submit( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            try
            {

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execclient, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
		public MethodResult KeysStrocks(Steps step, string executionID)
		{
			string ret = string.Empty;
			List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

			try
			{

				ret = StringHelper.success;
			}
			catch (Exception ex)
			{
				new Logger().LogException(ex);
				new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
				msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
				Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
				s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
			}

			MethodResult mr;
			if (ret == StringHelper.success)
			{
				Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
				s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execclient, Method, StringHelper.truee);
				mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
			}
			else
			{
				mr = new MethodResultHandler().createResultType(false, msgs, null, s);
			}
			return mr;
		}
		public MethodResult TabStrock(Steps step, string executionID)
		{
			string ret = string.Empty;
			List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

			try
			{

				ret = StringHelper.success;
			}
			catch (Exception ex)
			{
				new Logger().LogException(ex);
				new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
				msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
				Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
				s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
			}

			MethodResult mr;
			if (ret == StringHelper.success)
			{
				Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
				s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execclient, Method, StringHelper.truee);
				mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
			}
			else
			{
				mr = new MethodResultHandler().createResultType(false, msgs, null, s);
			}
			return mr;
		}
		public MethodResult Enter(Steps step, string executionID)
		{
			string ret = string.Empty;
			List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

			try
			{

				ret = StringHelper.success;
			}
			catch (Exception ex)
			{
				new Logger().LogException(ex);
				new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
				msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
				Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
				s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
			}

			MethodResult mr;
			if (ret == StringHelper.success)
			{
				Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
				s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execclient, Method, StringHelper.truee);
				mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
			}
			else
			{
				mr = new MethodResultHandler().createResultType(false, msgs, null, s);
			}
			return mr;
		}
		public MethodResult ShiftTab(Steps step, string executionID)
		{
			string ret = string.Empty;
			List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

			try
			{

				ret = StringHelper.success;
			}
			catch (Exception ex)
			{
				new Logger().LogException(ex);
				new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
				msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
				Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
				s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
			}

			MethodResult mr;
			if (ret == StringHelper.success)
			{
				Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
				s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execclient, Method, StringHelper.truee);
				mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
			}
			else
			{
				mr = new MethodResultHandler().createResultType(false, msgs, null, s);
			}
			return mr;
		}
	}
}