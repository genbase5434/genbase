﻿using Genbase.classes;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Genbase.Classes;
using System.IO;
//using ExcelDataReader;
using OfficeOpenXml;
using System.Xml;
using ClosedXML.Excel;

namespace Genbase.BLL.ElementsClasses
{
    public class Excel
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        VariableHandler varh = new VariableHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        string SheetExists = string.Empty;
        public MethodResult BasicExcel( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string Input = string.Empty; string Output = string.Empty;
            string InputType = string.Empty; string Cells = string.Empty;
            string filename = string.Empty; string sheetname = string.Empty;
            string Task = string.Empty;

            DataTable returnValue = new DataTable();
            try
            {
                filename = stp.getPropertiesValue(StringHelper.filename, step);
                sheetname = stp.getPropertiesValue(StringHelper.sheetname, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                InputType = stp.getPropertiesValue(StringHelper.inputtype, step);
                Cells = stp.getPropertiesValue(StringHelper.cells, step);
                Task = stp.getPropertiesValue(StringHelper.Task, step);


                System.Data.DataTable dt = new TypeHandler().getInput(Input, InputType, executionID);


                if (Task.ToLower() == StringHelper.read)
                {
                    List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                    if (Cells.Trim() == StringHelper.WildCardStar)
                    {
                        foreach (string file in files)
                        {
                            returnValue.Merge(ReadAsDataTable(file, false, sheetname));
                        }
                    }
                    else if (Cells.Trim() != string.Empty)
                    {
                        foreach (string file in files)
                        {
                            if (Cells.IndexOf(":") > -1)
                            {
                                string Cells1 = Cells.Split(':')[0];
                                string Cells2 = Cells.Split(':')[1];

                                string fromrow = string.Empty;
                                string torow = string.Empty;
                                string fromcolumn = string.Empty;
                                string tocolumn = string.Empty;

                                if (Cells1.Length == 2 && Cells2.Length == 2)
                                {
                                    fromrow = Cells1.Select(c => c.ToString()).ToArray()[1];
                                    fromcolumn = Cells1.Select(c => c.ToString()).ToArray()[0];
                                    torow = Cells2.Select(c => c.ToString()).ToArray()[1];
                                    tocolumn = Cells2.Select(c => c.ToString()).ToArray()[0];

                                    returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, sheetname));
                                }
                                else if (Cells1.Length > 2 && Cells2.Length == 2)
                                {
                                    StringBuilder rows = new StringBuilder(); StringBuilder columns = new StringBuilder();
                                    foreach (string cells1 in Cells1.Select(c => c.ToString()).ToArray())
                                    {
                                        int bog = 0;
                                        if (int.TryParse(cells1, out bog))
                                        {
                                            rows.Append(bog);
                                        }
                                        else
                                        {
                                            columns.Append(cells1);
                                        }
                                    }

                                    fromrow = rows.ToString();
                                    fromcolumn = columns.ToString();
                                    torow = Cells2.Select(c => c.ToString()).ToArray()[1];
                                    tocolumn = Cells2.Select(c => c.ToString()).ToArray()[0];

                                    returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, sheetname));
                                }
                                else if (Cells1.Length == 2 && Cells2.Length > 2)
                                {
                                    StringBuilder rows = new StringBuilder(); StringBuilder columns = new StringBuilder();
                                    foreach (string cells2 in Cells2.Select(c => c.ToString()).ToArray())
                                    {
                                        int bog = 0;
                                        if (int.TryParse(cells2, out bog))
                                        {
                                            rows.Append(bog);
                                        }
                                        else
                                        {
                                            columns.Append(cells2);
                                        }
                                    }

                                    fromrow = Cells1.Select(c => c.ToString()).ToArray()[1];
                                    fromcolumn = Cells1.Select(c => c.ToString()).ToArray()[0];
                                    torow = rows.ToString();
                                    tocolumn = columns.ToString();

                                    returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, sheetname));
                                }
                                else if (Cells1.Length > 2 && Cells2.Length > 2)
                                {
                                    StringBuilder rows1 = new StringBuilder(); StringBuilder columns1 = new StringBuilder();
                                    foreach (string cells1 in Cells1.Select(c => c.ToString()).ToArray())
                                    {
                                        int bog = 0;
                                        if (int.TryParse(cells1, out bog))
                                        {
                                            rows1.Append(bog);
                                        }
                                        else
                                        {
                                            columns1.Append(cells1);
                                        }
                                    }


                                    StringBuilder rows2 = new StringBuilder(); StringBuilder columns2 = new StringBuilder();
                                    foreach (string cells2 in Cells2.Select(c => c.ToString()).ToArray())
                                    {
                                        int bog = 0;
                                        if (int.TryParse(cells2, out bog))
                                        {
                                            rows2.Append(bog);
                                        }
                                        else
                                        {
                                            columns2.Append(cells2);
                                        }
                                    }

                                    fromrow = rows1.ToString();
                                    fromcolumn = columns1.ToString();
                                    torow = rows2.ToString();
                                    tocolumn = columns2.ToString();

                                    returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, sheetname));
                                }
                                else
                                {

                                }
                            }
                            else
                            {

                            }
                        }
                    }
                    ret = StringHelper.success;
                }
                else if (Task.ToLower() == StringHelper.write)
                {
                    DataSet ds = new DataSet();
                    DataTable tablu = new DataTable();
                    tablu.Columns.Add("Files");

                    string homePath = Directory.GetCurrentDirectory();
                   // string homePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                    string filesFolder = homePath + StringHelper.files;

                    ds.Tables.Add(dt);
                    string filesPath = filesFolder + "\\" + filename + DateTime.Now.ToString(StringHelper.mdyhms) + ".xls";
                    if (ExportDataSet(ds, filesPath))
                    {
                        tablu.Rows.Add(filesPath);
                    }
                    returnValue = tablu;
                    ret = StringHelper.success;
                }
                else if (Task.ToLower() == StringHelper.update)
                {
                    DataTable udt = new DataTable();

                    DataTable tablu = new DataTable();
                    tablu.Columns.Add("Files");

                    if (Cells.IndexOf(":") > -1)
                    {
                        udt = new TypeHandler().getInput(Cells, "Value", executionID);
                    }
                    else
                    {
                        udt = new TypeHandler().getInput(Cells, executionID);
                    }

                    if (udt.Columns.Count > 0)
                    {
                        //string homePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                        string homePath = Directory.GetCurrentDirectory();
                        string filesFolder = homePath + StringHelper.files;

                        foreach (DataRow dtr in dt.Rows)
                        {
                            filename = dtr["Files"].ToString();
                            string filesPath = filename;
                            if (udt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in udt.Rows)
                                {
                                    if (dr[0] != null && dr[1] != null)
                                    {
                                        var udtaddress = dr[0].ToString();
                                        var udtdata = dr[1].ToString();

                                        StringBuilder rows = new StringBuilder(); StringBuilder columns = new StringBuilder();
                                        foreach (string cells1 in udtaddress.Select(c => c.ToString()).ToArray())
                                        {
                                            int bog = 0;
                                            if (int.TryParse(cells1, out bog))
                                            {
                                                rows.Append(bog);
                                            }
                                            else
                                            {
                                                columns.Append(cells1);
                                            }
                                        }

                                        UpdateCell(filesPath, udtdata, Convert.ToUInt32(int.Parse(rows.ToString())), columns.ToString(), sheetname);
                                    }
                                }
                            }
                            tablu.Rows.Add(filesPath);
                        }
                    }

                    returnValue = tablu;
                    ret = StringHelper.success;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult CreateExcel( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string Input = string.Empty; string Output = string.Empty;
            string InputType = string.Empty; string Cells = string.Empty;
            string filename = string.Empty; string sheetname = string.Empty;
            string Task = string.Empty;

            DataTable returnValue = new DataTable();
            try
            {
                if (stp.getPropertiesValue(StringHelper.filename, step).Trim().ToLower() == string.Empty)
                {
                    filename = stp.getPropertiesValue("file name", step);
                }
                else
                {
                    filename = stp.getPropertiesValue(StringHelper.filename, step);
                }
                if (stp.getPropertiesValue(StringHelper.sheetname, step).Trim().ToLower() == string.Empty)
                {
                    sheetname = stp.getPropertiesValue("sheet name", step);
                }
                else
                {
                    sheetname = stp.getPropertiesValue(StringHelper.sheetname, step);
                }

                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                InputType = stp.getPropertiesValue(StringHelper.inputtype, step);
                Cells = stp.getPropertiesValue(StringHelper.cells, step);

                DataTable dt = new DataTable(); DataSet ds = new DataSet();

                DataTable tablu = new DataTable();
                tablu.Columns.Add("Files");

                string homePath = Directory.GetCurrentDirectory();
               // string homePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                //string filesFolder = homePath +  StringHelper.files;
                string filesFolder = homePath + "\\" + StringHelper.files;
                if (!Directory.Exists(filesFolder))
                {
                    Directory.CreateDirectory(filesFolder);
                }
                
                string filesPath = filesFolder + "\\" + filename + DateTime.Now.ToString(StringHelper.mdyhms) + ".xlsx";
                //string homePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;

                //DirectoryInfo directory = new DirectoryInfo(filesFolder);
                //FileInfo[] files = directory.GetFiles();

                //bool fileFound = false;
                //foreach (FileInfo file in files)
                //{
                //    if (String.Compare(file.Name, filename + ".xlsx") == 0)
                //    {
                //        fileFound = true;
                //    }
                //}
                //string filesPath = filesFolder + "\\" + filename + ".xlsx";
                //if (fileFound)
                //{
                //    log.add2Log("An Excel file with name " + filename + ".xlsx already exists \n", step.Robot_Id, step.Name, StringHelper.eventcode, false);
                //    filesPath = filesFolder + "\\" + filename + DateTime.Now.ToString(StringHelper.mdyhms) + ".xlsx";
                //}
                List<string> thisList = Cells.Split(':').ToList();
                if (thisList.Capacity > 1)
                {
                    for (int i = 0; i < thisList.Capacity; i++)
                    {
                        dt.Columns.Add(thisList[i]);
                    }
                }               
                else
                {
                   dt.Columns.Add(Cells);
                }
                List<string> thisList1 = Input.Split(';').ToList();                
                if (thisList1.Capacity > 1) {
                    for (int i = 0; i < thisList1.Capacity; i++)
                    {
                        dt.Rows.Add(thisList1[i]);
                    }
                }
                else
                {
                    dt.Rows.Add(Input);
                }
                if (new VariableHandler().checkVariableExists(Input, executionID))
                {
                    //dt = new TypeHandler().getInput(Input, executionID);
                    dt = new TypeHandler().getInput(Input, InputType, executionID);
                    ds.Tables.Add(dt);
                }
                else
                {
                    if (Input.Trim().ToLower() == "empty" && sheetname.Trim().ToLower() == "empty")
                    {
                        dt.TableName = "EMPTY";
                        //dt.Columns.Add();
                        dt.Rows.Add(string.Empty);
                    }
                    else if (sheetname == "" && Input!="")
                    {
                        dt.TableName = "Sheet1";                       
                       // dt.Columns.Add(Cells);
                       // dt.Rows.Add(Input);
                    }
                    else
                    {
                        dt.TableName = sheetname;
                       // dt.Columns.Add();
                       // dt.Rows.Add(Input);
                    }
                    ds.Tables.Add(dt);
                }

                if (ExportDataSet(ds, filesPath))
                {
                    tablu.Rows.Add(filesPath);
                }
                //UpdateCell(filesPath, value, Convert.ToUInt32(int.Parse(rows.ToString())), columns.ToString(), SheetName);
                returnValue = tablu;
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        private static int CellReferenceToIndex(Cell cell)
        {
            int index = 0;
            string reference = cell.CellReference.ToString().ToUpper();
            foreach (char ch in reference)
            {
                if (Char.IsLetter(ch))
                {
                    int value = (int)ch - (int)'A';
                    index = (index == 0) ? value : ((index + 1) * 26) + value;
                }
                else
                    return index;
            }
            return index;
        }

        public DataTable ReadAsDataTable(string fileName, bool allowHeader, string SHEETNAME)
        {
            DataTable dataTable = new DataTable();
            try
            {
                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileName, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    List<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().ToList();

                    if (SHEETNAME != string.Empty)
                    {
                        sheets = sheets.FindAll(sheet => sheet.Name == SHEETNAME);
                    }

                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;

                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();
                    //DataTable chkdata = new DataTable();
                    //string RowStart, string ColStart, string RowEnd, string ColEnd, string filename, string sheetname
                    //chkdata = RangeData("1", "A", "2", "K", fileName, SHEETNAME);
                    //if ((chkdata.Rows[0][0].ToString() != string.Empty) && (chkdata.Rows[0][5].ToString() != string.Empty) && (chkdata.Rows[1][0].ToString() != string.Empty) && (chkdata.Rows[1][2].ToString() != string.Empty))
                    //{

                    //}

                    long longCount = 0;
                    if (rows.ElementAt(0).ToString() != string.Empty)
                    {
                        if (allowHeader)
                        {
                            foreach (Cell cell in rows.ElementAt(0))
                            {
                                dataTable.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                            }
                        }
                        else
                        {
                            foreach (Row row in rows)
                            {
                                if (longCount < row.Elements().LongCount())
                                {
                                    longCount = row.Elements().LongCount();
                                }
                            }
                            for(long y=0; y<longCount; y++)
                            {
                                dataTable.Columns.Add();
                            }
                        }
                    }

                    foreach (Row row in rows)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                        {
                            try
                            {
                                if (i < dataTable.Columns.Count)
                                {
                                    dataRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                                    //dataRow[i] = GetFormattedCellValue(workbookPart, row.Descendants<Cell>().ElementAt(i));
                                    //Cell cell = row.Descendants<Cell>().ElementAt(i);
                                    //int actualCellIndex = CellReferenceToIndex(cell);
                                    //dataRow[actualCellIndex] = GetCellValue(spreadSheetDocument, cell);
                                }
                            }
                            catch (Exception ex)
                            {
                                string ec = ex.Message;
                                dataRow[i] = string.Empty;
                            }
                        }
                        dataTable.Rows.Add(dataRow);
                    }
                    
                }
                return dataTable;
            }
            catch (Exception ex)
            {
                string ec = ex.Message;
                DataTable otdt = new DataTable();
                try
                {
                    if (fileName.IndexOf(".csv") > -1 || fileName.IndexOf(".CSV") > -1)
                    {
                        List<int> colCount = new List<int>(); 
                        using (var reader = new StreamReader(fileName))
                        {
                            var prev_line = "";
                            var append_line = "close";
                            while (!reader.EndOfStream)
                            {
                                var line = reader.ReadLine();
                                if (line != "")
                                {
                                    if ((line.Split('\"').Length - 1) % 2 == 0 && append_line == "close")
                                    {
                                        if (line.Contains(",\"") || line.First().ToString() == "\"")
                                        {
                                            line = Replacetext(line);
                                        }
                                        colCount.Add(line.Split(",").Length);
                                    }
                                    else
                                    {
                                        prev_line = prev_line + line;
                                        append_line = "open";
                                        if (prev_line.Contains("\""))
                                        {
                                            if ((prev_line.Split('\"').Length - 1) % 2 == 0)
                                            {
                                                if (prev_line.Contains(",\"") || line.First().ToString() == "\"")
                                                {
                                                    prev_line = Replacetext(prev_line);
                                                }
                                                colCount.Add(prev_line.Split(",").Length);
                                                prev_line = ""; append_line = "close";
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator
                        int cols = colCount.Max();

                        for (int y = 1; y <= cols; y++)
                        {
                            otdt.Columns.Add();
                        }

                        using (var reader = new StreamReader(fileName))
                        {
                            var prev_line = "";
                            var append_line = "close";
                            while (!reader.EndOfStream)
                            {
                                var line = reader.ReadLine();
                                if (line != "")
                                {
                                    if ((line.Split('\"').Length - 1) % 2 == 0 && append_line == "close")
                                    {
                                        if (line.Contains(",\"") || line.First().ToString() == "\"")
                                        {
                                            line = Replacetext(line);
                                        }
                                        var arrayRow = line.Split(",");
                                        for (int i = 0; i < arrayRow.Length; i++)
                                        {
                                            arrayRow[i] = arrayRow[i].Replace("|", ",");
                                            arrayRow[i] = arrayRow[i].Replace("\"", "");
                                        }
                                        otdt.Rows.Add(arrayRow);
                                    }
                                    else
                                    {
                                        prev_line = prev_line + line;
                                        append_line = "open";
                                        if (prev_line.Contains("\""))
                                        {
                                            if ((prev_line.Split('\"').Length - 1) % 2 == 0)
                                            {
                                                if (prev_line.Contains(",\"") || line.First().ToString() == "\"")
                                                {
                                                    prev_line = Replacetext(prev_line);
                                                }
                                                var arrayRow = prev_line.Split(",");
                                                for (int i = 0; i < arrayRow.Length; i++)
                                                {
                                                    arrayRow[i] = arrayRow[i].Replace("|", ",");
                                                    arrayRow[i] = arrayRow[i].Replace("\"", "");
                                                }
                                                otdt.Rows.Add(arrayRow);
                                                prev_line = ""; append_line = "close";
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }

                        //foreach (DataColumn dc in otdt.Columns)
                        //{
                        //    try
                        //    {
                        //        dc.ColumnName = otdt.Rows[0][dc.ColumnName].ToString();
                        //    }
                        //    catch(Exception f)
                        //    {
                        //        string fu = f.Message;
                        //    }
                        //}
                        //otdt.Rows[0].Delete();
                    }
                }
                catch(Exception fu)
                {
                    string f = fu.Message;
                }
                return otdt;
            }
        }

        //public static DataTable MyExcelData(string filepath, string SHEETNAME,bool ColumnHeader = true, bool _Isemptyheader = false)
        //{
        //    DataTable dt = new DataTable();

        //    using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filepath, false))
        //    {

        //        WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
        //        //IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
        //        List<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().ToList();

        //        if (SHEETNAME != string.Empty)
        //        {
        //            sheets = sheets.FindAll(sheet => sheet.Name == SHEETNAME);
        //        }

        //        string relationshipId = sheets.First().Id.Value;
        //        //string relationshipId = sheets.ElementAt(0).Id.Value; //sheets.First().Id.Value;
        //        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
        //        Worksheet workSheet = worksheetPart.Worksheet;
        //        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
        //        int rowCount = sheetData.Descendants<Row>().Count();
        //        if (rowCount == 0)
        //        {
        //            return dt;
        //        }
        //        IEnumerable<Row> rows = sheetData.Descendants<Row>();
        //        int cnt = sheetData.Descendants<Column>().Count();

        //        var charcolumn = 'A';
        //        foreach (Cell cell in rows.ElementAt(0))
        //        {
        //            if (GetCellValue(spreadSheetDocument, cell).ToString() != "" || _Isemptyheader)
        //            {
        //                if (ColumnHeader)
        //                    dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
        //                else
        //                {
        //                    dt.Columns.Add(charcolumn.ToString());
        //                    charcolumn++;
        //                }
        //            }
        //        }
        //        foreach (Row row in rows) //this will also include your header row...
        //        {
        //            DataRow tempRow = dt.NewRow();
        //            int columnIndex = 0;
        //            foreach (Cell cell in row.Descendants<Cell>())
        //            {
        //                // Gets the column index of the cell with data
        //                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
        //                cellColumnIndex--; //zero based index
        //                if (columnIndex < cellColumnIndex)
        //                {
        //                    do
        //                    {
        //                        //tempRow[columnIndex] = ""; //Insert blank data here;
        //                        columnIndex++;
        //                    }
        //                    while (columnIndex < cellColumnIndex);
        //                }
        //                if (columnIndex < dt.Columns.Count)
        //                {
        //                    tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);
        //                    columnIndex++;
        //                }
        //                //tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);
        //                //columnIndex++;
        //            }
        //            dt.Rows.Add(tempRow);
        //        }
        //    }
        //    if (ColumnHeader)
        //        dt.Rows.RemoveAt(0); //...so i'm taking it out here.
        //    return dt;
        //}
        private static int? GetColumnIndexFromName(string columnNameOrCellReference)
        {
            int columnIndex = 0;
            int factor = 1;
            for (int pos = columnNameOrCellReference.Length - 1; pos >= 0; pos--) // R to L
            {
                if (Char.IsLetter(columnNameOrCellReference[pos])) // for letters (columnName)
                {
                    columnIndex += factor * ((columnNameOrCellReference[pos] - 'A') + 1);
                    factor *= 26;
                }
            }
            return columnIndex;

        }
       
        private static double CalculateSumOfCellRange(string docName, string worksheetName, string firstCellName, string lastCellName, string resultCell)
        {
            // Open the document for editing.
            using (SpreadsheetDocument document = SpreadsheetDocument.Open(docName, true))
            {
                IEnumerable<Sheet> sheets = document.WorkbookPart.Workbook.Descendants<Sheet>().Where(s => s.Name == worksheetName);
                if (sheets.Count() == 0)
                {
                    // The specified worksheet does not exist.
                    return 0;
                }

                WorksheetPart worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheets.First().Id);
                Worksheet worksheet = worksheetPart.Worksheet;

                // Get the row number and column name for the first and last cells in the range.
                uint firstRowNum = GetRowIndex(firstCellName);
                uint lastRowNum = GetRowIndex(lastCellName);
                string firstColumn = GetColumnName(firstCellName);
                string lastColumn = GetColumnName(lastCellName);

                double sum = 0;

                // Iterate through the cells within the range and add their values to the sum.
                foreach (Row row in worksheet.Descendants<Row>().Where(r => r.RowIndex.Value >= firstRowNum && r.RowIndex.Value <= lastRowNum))
                {
                    foreach (Cell cell in row)
                    {
                        string columnName = GetColumnName(cell.CellReference.Value);
                        if (CompareColumn(columnName, firstColumn) >= 0 && CompareColumn(columnName, lastColumn) <= 0)
                        {
                            sum += double.Parse(cell.CellValue.Text);
                        }
                    }
                }

                return sum;
            }
        }
        private string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
           
            if (cell != null && cell.CellValue != null)
            {
                try
                {
                    string value = cell.CellValue.InnerXml;
                    if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                    {
                        return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                    }
                    else if (cell.DataType != null && cell.DataType.Value == CellValues.Date)
                    {
                        return DateTime.FromOADate(Convert.ToDouble(value)).ToString();
                    }
                    else
                    {
                        DateTime dateTime;
                        if (cell.StyleIndex != null && DateTime.TryParse(DateTime.FromOADate(Convert.ToDouble(value)).ToString(), out dateTime))
                        {
                            return GetFormattedCellValue(document.WorkbookPart, cell);
                        }
                        else
                        {
                            return value;
                        }
                    }
                }
                catch (Exception ex)
                {
                    string ec = ex.Message;
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }
        public DataTable RangeData(string RowStart, string ColStart, string RowEnd, string ColEnd, string filename, string sheetname)
        {
            DataTable dataTable = new DataTable(); List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filename, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    List<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().ToList();

                    if (sheetname != string.Empty)
                    {
                        sheets = sheets.FindAll(sheet => sheet.Name == sheetname);
                    }
                    if (sheets.Count!=0 && sheets[0].Name==sheetname) { 
                        //List<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().ToList();
                        string relationshipId = sheets.First().Id.Value;
                        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                        Worksheet workSheet = worksheetPart.Worksheet;
                        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                        List<Row> rows = sheetData.Descendants<Row>().ToList();

                        uint firstRowNum = GetRowIndex(ColStart + RowStart);
                        uint lastRowNum = GetRowIndex(ColEnd + RowEnd);
                        string firstColumn = GetColumnName(ColStart + RowStart);
                        string lastColumn = GetColumnName(ColEnd + RowEnd);

                        int u = 0;
                        uint last_rowNum = GetRowIndex(ColEnd + RowStart);
                        string last_column = GetColumnName(ColEnd + RowStart);

                        List<string> colNamesData = new List<string>();
                        foreach (Row row in sheetData.Elements<Row>().Where(r => r.RowIndex.Value >= firstRowNum && r.RowIndex.Value <= last_rowNum))
                        {
                            foreach (Cell cell in row)
                            {
                                string columnName = GetColumnName(cell.CellReference.Value);
                                if (CompareColumn(columnName, firstColumn) >= 0 && CompareColumn(columnName, last_column) <= 0)
                                {
                                    colNamesData.Add(columnName);
                                }
                            }
                        }

                        foreach (Row row in sheetData.Elements<Row>().Where(r => r.RowIndex.Value >= firstRowNum && r.RowIndex.Value <= lastRowNum))
                        {
                            List<string> rowData = new List<string>();
                            List<string> rowCol = new List<string>();
                            int s = 0;
                            foreach (Cell cell in row)
                            {
                                string columnName = GetColumnName(cell.CellReference.Value);
                                if (CompareColumn(columnName, firstColumn) >= 0 && CompareColumn(columnName, lastColumn) <= 0)
                                {
                                    //rowData.Add(cell.CellValue.Text);
                                    if (colNamesData[s] != columnName)
                                    {
                                        for (int p = s; p < colNamesData.Count; p++)
                                        {
                                            if (colNamesData[p] != columnName)
                                            {
                                                rowData.Add("");
                                                s++;
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }

                                    }

                                    if (rowsAndColumnscellval(workbookPart, cell) != null)
                                    {
                                        rowData.Add(rowsAndColumnscellval(workbookPart, cell));
                                    }
                                    else
                                    {
                                        rowData.Add(string.Empty);
                                    }

                                    s++;

                                    if (u == 0)
                                    {
                                        var colList = cell.CellReference.Value.ToCharArray().ToList();
                                        string numPart = string.Empty; string letPart = string.Empty;

                                        foreach (char r in colList)
                                        {
                                            int tre = 0;
                                            if (int.TryParse(r.ToString(), out tre))
                                            {
                                                numPart = numPart + r.ToString();
                                            }
                                            else
                                            {
                                                letPart = letPart + r.ToString();
                                            }
                                        }
                                        var headerCell = GetCell(workSheet, letPart, 1);
                                        var headerCellAlt = GetCell(workSheet, letPart, 2);
                                        //var headerCellAltr = GetCell(workSheet, letPart, 3);

                                        string headerCellVal = GetCellValue(spreadSheetDocument, headerCell);
                                        string headerCellValAlt = GetCellValue(spreadSheetDocument, headerCell);

                                        if (headerCellVal.Trim() != string.Empty)
                                        {
                                            //dataTable.Columns.Add(headerCellVal);
                                            dataTable.Columns.Add(cell.CellReference);
                                        }
                                        else
                                        {
                                            dataTable.Columns.Add(cell.CellReference);
                                        }
                                    }


                                }
                            }
                            if (rowData.Count == dataTable.Columns.Count)
                            {
                                dataTable.Rows.Add(rowData.ToArray());
                            }
                            else if (rowData.Count < dataTable.Columns.Count)
                            {
                                int diff = dataTable.Columns.Count - rowData.Count;
                                //for (int i = 0; i <= diff; i++)
                                for (int i = 0; i < diff; i++)
                                {
                                    rowData.Add("Empty Cells");
                                }
                                dataTable.Rows.Add(rowData.ToArray());
                            }
                            else if (rowData.Count > dataTable.Columns.Count)
                            {
                                int diff = rowData.Count - dataTable.Columns.Count;
                                rowData.RemoveRange(dataTable.Columns.Count, diff);
                            }
                            else
                            {

                            }
                            u++;
                        }
                        
                    }
                    else
                    {
                        SheetExists = "Sheet " + sheetname + " doesnot exist in the file";                        
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

            }
            //dataTable.Rows.RemoveAt(0);          
            return dataTable;
        }

        public DataTable GetCellData(string RowStart, string ColStart, string filename, string sheetname)
        {
            DataTable dataTable = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filename, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    List<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().ToList();
                    string relationshipId = sheets.First().Id.Value;

                    if (sheetname != string.Empty)
                    {
                        sheets = sheets.FindAll(sheet => sheet.Name == sheetname);
                    }
                    if (sheets.Count != 0 && sheets[0].Name == sheetname)
                    {
                        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                        Worksheet workSheet = worksheetPart.Worksheet;
                        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                        List<Row> rows = sheetData.Descendants<Row>().ToList();

                        uint firstRowNum = GetRowIndex(ColStart + RowStart);
                        uint lastRowNum = GetRowIndex(ColStart + RowStart);
                        string firstColumn = GetColumnName(ColStart + RowStart);
                        string lastColumn = GetColumnName(ColStart + RowStart);

                        int u = 0;

                        foreach (Row row in sheetData.Elements<Row>().Where(r => r.RowIndex.Value >= firstRowNum && r.RowIndex.Value <= lastRowNum))
                        {
                            List<string> rowData = new List<string>();
                            foreach (Cell cell in row)
                            {
                                string columnName = GetColumnName(cell.CellReference.Value);
                                if (CompareColumn(columnName, firstColumn) >= 0 && CompareColumn(columnName, lastColumn) <= 0)
                                {
                                    //rowData.Add(cell.CellValue.Text);
                                    if (rowsAndColumnscellval(workbookPart, cell) != null)
                                    {
                                        rowData.Add(rowsAndColumnscellval(workbookPart, cell));
                                    }
                                    else
                                    {
                                        rowData.Add(string.Empty);
                                    }
                                    if (u == 0)
                                    {
                                        var colList = cell.CellReference.Value.ToCharArray().ToList();

                                        string numPart = string.Empty; string letPart = string.Empty;

                                        foreach (char r in colList)
                                        {
                                            int tre = 0;
                                            if (int.TryParse(r.ToString(), out tre))
                                            {
                                                numPart = numPart + r.ToString();
                                            }
                                            else
                                            {
                                                letPart = letPart + r.ToString();
                                            }
                                        }
                                        var headerCell = GetCell(workSheet, letPart, 1);
                                        var headerCellAlt = GetCell(workSheet, letPart, 2);
                                        //var headerCellAltr = GetCell(workSheet, letPart, 3);

                                        string headerCellVal = GetCellValue(spreadSheetDocument, headerCell);
                                        string headerCellValAlt = GetCellValue(spreadSheetDocument, headerCell);

                                        if (headerCellVal.Trim() != string.Empty && headerCellVal != rowData[0])
                                        {
                                            dataTable.Columns.Add(headerCellVal);
                                        }
                                        else if (headerCellVal == rowData[0])
                                        {
                                            dataTable.Columns.Add(headerCell.CellReference);
                                        }
                                        else
                                        {
                                            dataTable.Columns.Add(headerCellValAlt);
                                        }
                                    }
                                }
                            }
                            if (rowData.Count == dataTable.Columns.Count)
                            {
                                dataTable.Rows.Add(rowData.ToArray());
                            }
                            else if (rowData.Count < dataTable.Columns.Count)
                            {
                                int diff = dataTable.Columns.Count - rowData.Count;
                                for (int i = 0; i <= diff; i++)
                                {
                                    rowData.Add("Empty Cells");
                                }
                                dataTable.Rows.Add(rowData.ToArray());
                            }
                            else if (rowData.Count > dataTable.Columns.Count)
                            {
                                int diff = rowData.Count - dataTable.Columns.Count;
                                rowData.RemoveRange(dataTable.Columns.Count, diff);
                            }
                            else
                            {

                            }
                            u++;
                        }
                    }
                    else {
                        SheetExists = "Sheet " + sheetname + " doesnot exist in the file";
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

            }
            //dataTable.Rows.RemoveAt(0);          
            return dataTable;
        }

        /// <summary>
        /// Given a cell name, parses the specified cell to get the row index.
        /// </summary>
        /// <param name="cellName"></param>
        /// <returns></returns>
        private static uint GetRowIndex(string cellName)
        {
            // Create a regular expression to match the row index portion the cell name.
            Regex regex = new Regex(@"\d+");
            Match match = regex.Match(cellName);

            return uint.Parse(match.Value);
        }
        /// <summary>
        ///  Given a cell name, parses the specified cell to get the column name.
        /// </summary>
        /// <param name="cellName"></param>
        /// <returns></returns>
        private static string GetColumnName(string cellName)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellName);

            return match.Value;
        }
        /// <summary>
        /// Given two columns, compares the columns.
        /// </summary>
        /// <param name="column1"></param>
        /// <param name="column2"></param>
        /// <returns></returns>
        private static int CompareColumn(string column1, string column2)
        {
            if (column1.Length > column2.Length)
            {
                return 1;
            }
            else if (column1.Length < column2.Length)
            {
                return -1;
            }
            else
            {
                return string.Compare(column1, column2, true);
            }
        }

        public static string rowsAndColumnscellval(WorkbookPart wb, Cell c)
        {
            string value = c.InnerText;
            if (c.DataType != null)
            {
                switch (c.DataType.Value)
                {
                    case CellValues.SharedString:
                        var stringTable = wb.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                        if (stringTable != null)
                        {
                            value =
                                stringTable.SharedStringTable
                                .ElementAt(int.Parse(value)).InnerText;
                        }
                        break;

                    case CellValues.Boolean:
                        switch (value)
                        {
                            case "0":
                                value = "FALSE";
                                break;
                            default:
                                value = "TRUE";
                                break;
                        }
                        break;
                    default:
                        value = "";
                        break;
                }
            }
            return value;
        }
        public bool ExportDataSet(DataSet ds, string destination)
        {
            try
            {
                using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
                {
                    var workbookPart = workbook.AddWorkbookPart();
                    workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();
                    workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

                    uint sheetId = 1;

                    foreach (System.Data.DataTable table in ds.Tables)
                    {
                        var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                        var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                        sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                        DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                        string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                        if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                        {
                            sheetId =
                                sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                        }

                        DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                        sheets.Append(sheet);

                        DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                        List<String> columns = new List<string>();
                        foreach (DataColumn column in table.Columns)
                        {
                            if(column.ColumnName=="Column1")
                            {
                                columns.Add(column.ColumnName);
                                DocumentFormat.OpenXml.Spreadsheet.Cell cell1 = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                                cell1.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                cell1.CellValue = null;
                                headerRow.AppendChild(cell1);
                            }
                            else {
                                columns.Add(column.ColumnName);
                                DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                                headerRow.AppendChild(cell);
                            }
                        }

                        sheetData.AppendChild(headerRow);
                        //int rowCount = 0;
                        foreach (DataColumn col in table.Columns)
                        {
                            DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                            foreach (DataRow dsrow in table.Rows)
                            {
                                DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString()); 
                                newRow.AppendChild(cell);
                            }
                            sheetData.AppendChild(newRow);
                        }
                    }

                    workbook.Close();
                    workbook.Dispose();
                }
                return true;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return false;
            }
        }


        public void UpdateCell(string docName, string text, uint rowIndex, string columnName, string sheetname)
        {
            // Open the document for editing.
            /*using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
            {
                WorksheetPart worksheetPart = GetWorksheetPartByName(spreadSheet, sheetname);

                if (worksheetPart != null)
                {
                    Cell cell = GetCell(worksheetPart.Worksheet, columnName, rowIndex);

                    cell.CellValue = new CellValue(text);
                    cell.DataType = new EnumValue<CellValues>(CellValues.SharedString);

                    
                    // Save the worksheet.
                    worksheetPart.Worksheet.Save();
                    
                }
            }*/
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                {
                    // Get the SharedStringTablePart. If it does not exist, create a new one.
                    SharedStringTablePart shareStringPart;
                    WorksheetPart worksheetPart = GetWorksheetPartByName(spreadSheet, sheetname);

                    if (spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().Count() > 0)
                    {
                        shareStringPart = spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First();
                    }
                    else
                    {
                        shareStringPart = spreadSheet.WorkbookPart.AddNewPart<SharedStringTablePart>();
                    }
                    if (worksheetPart != null)
                    {
                        // Insert the text into the SharedStringTablePart.
                        int index = InsertSharedStringItem(text, shareStringPart);

                        // Insert a new worksheet.
                        //WorksheetPart worksheetPart = InsertWorksheet(spreadSheet.WorkbookPart);

                        // Insert cell A1 into the new worksheet.
                        Cell cell = InsertCellInWorksheet(columnName, rowIndex, worksheetPart);

                        // Set the value of cell A1.
                        cell.CellValue = new CellValue(index.ToString());
                        cell.DataType = new EnumValue<CellValues>(CellValues.SharedString);

                        // Save the new worksheet.
                        worksheetPart.Worksheet.Save();
                    }
                    else {
                        SheetExists = "Sheet " + sheetname + " doesnot exist in the file";
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

            }

        }

        public void UpdateCells(string docName, List<string> values, List<string> cells, string sheetname)
        {
            List<KeyValuePair<string, string>> cellsandvalues = new List<KeyValuePair<string, string>>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                {
                    try
                    {
                        if (sheetname.Length > 30)
                        {
                            sheetname = sheetname.Substring(0, Math.Min(sheetname.Length, 30));
                        }
                        for (int y = 0; y < cells.Count; y++)
                        {
                            string cell = string.Empty; string value = string.Empty;

                            if (y <= values.Count)
                            {
                                try
                                {
                                    cellsandvalues.Add(new KeyValuePair<string, string>(cells[y], values[y]));
                                }
                                catch
                                {
                                    cellsandvalues.Add(new KeyValuePair<string, string>(cells[y], string.Empty));
                                }
                            }
                            else
                            {
                                cellsandvalues.Add(new KeyValuePair<string, string>(cells[y], string.Empty));
                            }
                        }

                        foreach (var cellandvalue in cellsandvalues)
                        {
                            if (cellandvalue.Value.ToString().Trim() != string.Empty && IsValidXmlString(cellandvalue.Value.ToString().Trim()))
                            {
                                string cellandvaluefiltered = RemoveInvalidXmlChars(cellandvalue.Value.ToString().Trim());
                                SharedStringTablePart shareStringPart;
                                WorksheetPart worksheetPart = GetWorksheetPartByName(spreadSheet, sheetname);
                                try
                                {
                                    if (worksheetPart != null)
                                    {

                                        if (spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().Count() > 0)
                                        {
                                            shareStringPart = spreadSheet.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First();
                                        }
                                        else
                                        {
                                            shareStringPart = spreadSheet.WorkbookPart.AddNewPart<SharedStringTablePart>();
                                        }

                                        // Insert the text into the SharedStringTablePart.
                                        int index = InsertSharedStringItem(cellandvaluefiltered, shareStringPart);

                                        // Insert a new worksheet.
                                        //WorksheetPart worksheetPart = InsertWorksheet(spreadSheet.WorkbookPart);

                                        StringBuilder rows = new StringBuilder();
                                        StringBuilder columns = new StringBuilder();

                                        foreach (char cells2 in cellandvalue.Key.ToCharArray())
                                        {
                                            int bog = 0;
                                            if (int.TryParse(cells2.ToString(), out bog))
                                            {
                                                rows.Append(bog);
                                            }
                                            else
                                            {
                                                columns.Append(cells2);
                                            }
                                        }

                                        // Insert cell A1 into the new worksheet.
                                        //Cell cell = InsertCellInWorksheet(columns.ToString(), Convert.ToUInt32(rows[0].ToString()), worksheetPart);
                                        Cell cell = InsertCellInWorksheet(columns.ToString(), Convert.ToUInt32(rows.ToString()), worksheetPart);

                                        // Set the value of cell A1.
                                        cell.CellValue = new CellValue(index.ToString());
                                        cell.DataType = new EnumValue<CellValues>(CellValues.SharedString);

                                        worksheetPart.Worksheet.Save();
                                    }
                                    else
                                    {
                                        SheetExists = "Sheet "+ sheetname +" doesnot exist in the file";
                                        worksheetPart.Worksheet.Save();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    new Logger().LogException(ex);
                                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                    worksheetPart.Worksheet.Save();
                                    spreadSheet.Save();
                                    spreadSheet.Close();

                                    //if above code fails in openxml, lets do it closed xml :-p
                                    var workBook = new XLWorkbook(docName);
                                    try
                                    {
                                        var ws1 = workBook.Worksheet(sheetname);
                                        var cell = ws1.Cell(cellandvalue.Key);
                                        cell.Value = cellandvaluefiltered;
                                        cell.DataType = XLDataType.Text;
                                    }
                                    catch
                                    {
                                        new Logger().LogException(ex);
                                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                    }
                                    workBook.Save();
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    spreadSheet.Save();
                    spreadSheet.Close();
                    Regex re = new Regex(@"([a-zA-Z]+)(\d+)");
                    Match reresult1 = re.Match(cells[0]);
                    Match reresult2 = re.Match(cells[cells.Count - 1]);

                    string numberpart1 = reresult1.Groups[2].Value;
                    string numberpart2 = reresult2.Groups[2].Value;

                    string alphapart1 = reresult1.Groups[1].Value;
                    string alphapart2 = reresult2.Groups[1].Value;

                    
                    //row insert //header //static
                    var workbook = new XLWorkbook(docName);
                    try
                    {
                        if (numberpart1 == numberpart2 && numberpart1.Trim() == "1")
                        {
                            var ws1 = workbook.Worksheet(sheetname);
                            var row = ws1.Range(cells[0] + ":" + cells[cells.Count - 1]);
                            row.Style.Fill.BackgroundColor = XLColor.FromArgb(229, 249, 245);
                            row.Style.Font.FontColor = XLColor.FromArgb(20, 21, 28);
                            row.Style.Font.Bold = true;
                            ws1.Columns().AdjustToContents();
                            row.SetAutoFilter(true);
                        }
                        else
                        {
                            var ws1 = workbook.Worksheet(sheetname);
                            ws1.Columns().AdjustToContents();
                        }
                    }
                    catch
                    {

                    }
                    workbook.Save();
                    
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
            }
        }

        private int InsertSharedStringItem(string text, SharedStringTablePart shareStringPart)
        {
            // If the part does not contain a SharedStringTable, create one.

            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(text);
            text = System.Convert.ToBase64String(toEncodeAsBytes);
            text = System.Text.Encoding.Default.GetString(System.Convert.FromBase64String(text));

            if (shareStringPart.SharedStringTable == null)
            {
                shareStringPart.SharedStringTable = new SharedStringTable();
            }

            int i = 0;

            // Iterate through all the items in the SharedStringTable. If the text already exists, return its index.
            foreach (SharedStringItem item in shareStringPart.SharedStringTable.Elements<SharedStringItem>())
            {
                if (item.InnerText == text)
                {
                    return i;
                }

                i++;
            }

            // The text does not exist in the part. Create the SharedStringItem and return its index.
            shareStringPart.SharedStringTable.AppendChild(new SharedStringItem(new DocumentFormat.OpenXml.Spreadsheet.Text(text)));
            shareStringPart.SharedStringTable.Save();

            return i;
        }

        // Given a WorkbookPart, inserts a new worksheet.
        private WorksheetPart InsertWorksheet(WorkbookPart workbookPart, string sheetName)
        {
            // Add a new worksheet part to the workbook.
            WorksheetPart newWorksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            newWorksheetPart.Worksheet = new Worksheet(new SheetData());
            newWorksheetPart.Worksheet.Save();

            Sheets sheets = workbookPart.Workbook.GetFirstChild<Sheets>();
            string relationshipId = workbookPart.GetIdOfPart(newWorksheetPart);

            // Get a unique ID for the new sheet.
            uint sheetId = 1;
            if (sheets.Elements<Sheet>().Count() > 0)
            {
                sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
            }

            // Append the new worksheet and associate it with the workbook.
            Sheet sheet = new Sheet() { Id = relationshipId, SheetId = sheetId, Name = sheetName };
            sheets.Append(sheet);
            workbookPart.Workbook.Save();

            return newWorksheetPart;
        }
        private Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {

            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (cell.CellReference.Value.Length == cellReference.Length)
                    {
                        if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                        {
                            refCell = cell;
                            break;
                        }
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }

        private static WorksheetPart GetWorksheetPartByName(SpreadsheetDocument document, string sheetName)
        {
            IEnumerable<Sheet> sheets = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == sheetName);
            if (sheets.Count() == 0)
            {
                // The specified worksheet does not exist.

                return null;
            }

            string relationshipId = sheets.First().Id.Value;
            WorksheetPart worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(relationshipId);
            return worksheetPart;
        }

        // Given a worksheet, a column name, and a row index, 
        // gets the cell at the specified column and 
        private static Cell GetCell(Worksheet worksheet, string columnName, uint rowIndex)
        {
            try
            {
                Row row = GetRow(worksheet, rowIndex);

                if (row == null)
                    return null;

                return row.Elements<Cell>().Where(c => string.Compare(c.CellReference.Value, columnName + rowIndex, true) == 0).First();
            }
            catch (Exception ex)
            {
                string ec = ex.Message;
                Cell cell = new Cell();
                return cell;
            }
        }


        // Given a worksheet and a row index, return the row.
        private static Row GetRow(Worksheet worksheet, uint rowIndex)
        {
            return worksheet.GetFirstChild<SheetData>().
              Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
        }

        public MethodResult Open( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string FileName = string.Empty;
            string Output = string.Empty;
            string Input = string.Empty;
            string InputType = string.Empty;
            string FileType = string.Empty;
            string FileNW = string.Empty;
            string FileLocal = string.Empty;
            DataTable returnValue = new DataTable(); returnValue.Columns.Add("Files");
            try
            {
                Input = stp.getPropertiesValue(StringHelper.excelInput, step);
                InputType = stp.getPropertiesValue(StringHelper.input_type, step);
                FileType = stp.getPropertiesValue(StringHelper.file_type, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                FileNW = stp.getPropertiesValue(StringHelper.fileNW, step);
                FileLocal = stp.getPropertiesValue(StringHelper.fileLocal, step);
                FileName = stp.getPropertiesValue(StringHelper.filename, step);

                //FileName = Variables.attaches[0];
                if (InputType.Trim().ToLower() == "input")
                {
                    if (FileName.Trim() != string.Empty)
                    {
                        if (FileName.IndexOf("xls") > -1 || FileName.IndexOf("csv") > -1)
                        {
                            returnValue.Rows.Add(FileName);
                        }

                    }
                    else
                    {
                        DataTable Filesvar = new TypeHandler().getInput(Input, executionID);

                        foreach (DataRow dr in Filesvar.Rows)
                        {
                            if (checkFileSuitable(dr[0].ToString()))
                            {
                                string fname = dr[0].ToString();
                                returnValue.Rows.Add(fname);
                            }
                        }
                    }
                }
                else if (InputType.Trim().ToLower() == "file selection control")
                {
                    if (FileType.Trim().ToLower() == "local")
                    {
                        if (Variables.uploads != null && Variables.uploads.Count > 0)
                        {
                            foreach (string file in Variables.uploads)
                            {
                                log.add2Log(file + StringHelper.hasbeenuploaded, step.Robot_Id, step.Name, StringHelper.eventcode,false);
                                returnValue.Rows.Add(file);
                            }
                        }
                        ret = StringHelper.success;
                    }
                    else if (FileType.Trim().ToLower() == "network")
                    {
                        string path = Path.Combine(Directory.GetCurrentDirectory(), "Uploads\\");
                        path = path + FileNW;
                        DataTable Filesvar = new TypeHandler().getInput(path, "value", executionID);

                        foreach (DataRow dr in Filesvar.Rows)
                        {
                            if (dr[0].ToString().IndexOf("xls") > -1)
                            {
                                string fname = dr[0].ToString();
                                returnValue.Rows.Add(fname);
                            }
                        }
                    }
                }


                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public bool checkFileSuitable(string filename)
        {
            if(filename.IndexOf("xls") > -1)
            {
                return true;
            }
            else if(filename.IndexOf("XLS") > -1)
            {
                return true;
            }
            else if (filename.IndexOf("XLSX") > -1)
            {
                return true;
            }
            else if (filename.ToString().IndexOf("xlsx") > -1)
            {
                return true;
            }
            else if (filename.ToString().IndexOf("csv") > -1)
            {
                return true;
            }
            else if (filename.ToString().IndexOf("CSV") > -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public MethodResult GetCell( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string FileName = string.Empty; DataTable returnValue = new DataTable(); DataTable dtr = new DataTable();
            string Reference = string.Empty;
            string Input = string.Empty;
            string InputType = string.Empty;
            string Output = string.Empty;
            string SheetName = string.Empty;
            string isheader = string.Empty;

            try
            {
                Input = stp.getPropertiesValue(StringHelper.instance, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                InputType = stp.getPropertiesValue(StringHelper.inputtype, step);
                isheader = stp.getPropertiesValue(StringHelper.IsHeader, step);
                DataTable dt = new TypeHandler().getInput(Input, executionID);
                Reference = stp.getPropertiesValue("Reference", step);
                string Cells = Reference;
                //SheetName = stp.getPropertiesValue("Sheet Name", step);
                SheetName = stp.getPropertiesValue(StringHelper.sheetname, step);

                List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                if (Cells.Trim() == StringHelper.WildCardStar)
                {
                    foreach (string file in files)
                    {
                        byte[] fbytes = System.IO.File.ReadAllBytes(file);
                        dtr.Merge(ExcelToDataTable(fbytes, true));
                    }
                    if (isheader.Trim().ToLower() == StringHelper.truee)
                    {
                        DataTable fdt = new DataTable();
                        dtr.Rows.RemoveAt(0);
                        for (int i = 0; i < dtr.Columns.Count; i++)
                        {
                            string names = dtr.Rows[0][i].ToString();
                            fdt.Columns.Add(names);
                            returnValue.Columns.Add(names);
                        }
                        foreach (DataRow row in dtr.Rows)
                        {
                            var tr = row.ItemArray;
                            fdt.Rows.Add(tr);

                        }
                        fdt.Rows.RemoveAt(0);
                        foreach (DataRow row in fdt.Rows)
                        {
                            var tr = row.ItemArray;
                            returnValue.Rows.Add(tr);
                        }
                     
                    }
                    else
                    {
                        returnValue = dtr.Copy();
                    }
                }
                else if (Cells.Trim() != string.Empty)
                {
                    foreach (string file in files)
                    {
                        if (Cells.Length >= 2)
                        {
                            Regex re = new Regex(@"([a-zA-Z]+)(\d+)");
                            Match result = re.Match(Cells);
                            string fromrow = result.Groups[1].Value;
                            string fromcolumn = result.Groups[2].Value;


                            returnValue.Merge(GetCellData(fromrow, fromcolumn, file, SheetName));
                        }
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                if(SheetExists!=String.Empty)
                het = het + SheetExists + "\n";
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult SetCell( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string FileName = string.Empty;
            DataTable returnValue = new DataTable();

            string Reference = string.Empty;
            string Input = string.Empty;
            string InputType = string.Empty;
            string Output = string.Empty;
            string SheetName = string.Empty;
            string File = string.Empty;

            try
            {
                SheetName = stp.getPropertiesValue(StringHelper.sheetname, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                InputType = stp.getPropertiesValue(StringHelper.input_type, step);
                Reference = stp.getPropertiesValue("Reference", step);
                File = stp.getPropertiesValue(StringHelper.instance, step);

                System.Data.DataTable dt = new DataTable();
                if (varh.checkVariableExists(File, executionID))
                {
                    dt = new TypeHandler().getInput(File, executionID);

                }
                else
                {
                    dt = new TypeHandler().getInput(File, "Value", executionID);
                }
                returnValue = dt;
                //if InputType reference then
                //InputType = "reference";
                System.Data.DataTable dtv = new TypeHandler().getInput(Input, InputType, executionID);
                bool refstate = false;
                if (InputType.ToLower() == "reference")
                {
                    refstate = true;
                }

                DataTable udt = new DataTable();

                DataTable tablu = new DataTable();
                tablu.Columns.Add("Files");

                string Cells = Reference;

                string value = string.Empty, obtained = string.Empty;
                List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                foreach (DataRow drrr in dtv.Rows)
                {
                    foreach (var item in drrr.ItemArray)
                    {
                        value += item.ToString();
                    }

                }
                string filename = string.Empty;
                if (refstate)
                {
                    if (value.IndexOf(";") > -1)
                    {
                        string[] vals = value.Split(';');
                        foreach (string cel in vals)
                        {
                            obtained = cel;
                            string fromrow = string.Empty;
                            string fromcolumn = string.Empty;

                            if (obtained.Length == 2)
                            {
                                fromrow = obtained.Select(c => c.ToString()).ToArray()[1];
                                fromcolumn = obtained.Select(c => c.ToString()).ToArray()[0];
                                foreach (string file in files)
                                {
                                    filename = file;
                                }
                                returnValue = GetCellData(fromrow, fromcolumn, filename, SheetName);
                                value += (returnValue.Rows[0].ItemArray)[0].ToString();
                            }
                            //GetCellData(fromrow, fromcolumn, file, SheetName)
                        }
                    }
                }
                //List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                foreach (string file in files)
                {

                    StringBuilder rows = new StringBuilder(); StringBuilder columns = new StringBuilder();

                    for (int y = 0; y < Cells.ToCharArray().Count(); y++)
                    {
                        int bog = 0;
                        if (int.TryParse(Cells.ToCharArray()[y].ToString(), out bog))
                        {
                            rows.Append(bog);
                        }
                        else
                        {
                            columns.Append(Cells.ToCharArray()[y].ToString());
                        }
                    }

                    string columnName = columns.ToString();
                    string rowIndex = rows.ToString();

                    UpdateCell(file, value, Convert.ToUInt32(int.Parse(rows.ToString())), columns.ToString(), SheetName);


                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                if (SheetExists != String.Empty)
                    het = het + SheetExists + "\n";
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult GetRange( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string FileName = string.Empty; DataTable returnValue = new DataTable();
            string Reference = string.Empty;
            string Input = string.Empty;
            string InputType = string.Empty;
            string Output = string.Empty;
            string SheetName = string.Empty;

            try
            {
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                InputType = stp.getPropertiesValue(StringHelper.inputtype, step);
                DataTable dt = new TypeHandler().getInput(Input, executionID);
                Reference = stp.getPropertiesValue("Reference", step);
                string Cells = Reference;
                SheetName = stp.getPropertiesValue(StringHelper.sheetname, step);

                List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                if (Cells.Trim() == StringHelper.WildCardStar)
                {
                    foreach (string file in files)
                    {
                        returnValue.Merge(ReadAsDataTable(file, false, SheetName));
                    }
                }
                else if (Cells.Trim() != string.Empty)
                {
                    foreach (string file in files)
                    {
                        if (Cells.IndexOf(":") > -1)
                        {
                            string Cells1 = Cells.Split(':')[0];
                            string Cells2 = Cells.Split(':')[1];

                            string fromrow = string.Empty;
                            string torow = string.Empty;
                            string fromcolumn = string.Empty;
                            string tocolumn = string.Empty;

                            if (Cells1.Length == 2 && Cells2.Length == 2)
                            {
                                fromrow = Cells1.Select(c => c.ToString()).ToArray()[1];
                                fromcolumn = Cells1.Select(c => c.ToString()).ToArray()[0];
                                torow = Cells2.Select(c => c.ToString()).ToArray()[1];
                                tocolumn = Cells2.Select(c => c.ToString()).ToArray()[0];

                                returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, SheetName));
                            }
                            else if (Cells1.Length > 2 && Cells2.Length == 2)
                            {
                                StringBuilder rows = new StringBuilder(); StringBuilder columns = new StringBuilder();
                                foreach (string cells1 in Cells1.Select(c => c.ToString()).ToArray())
                                {
                                    int bog = 0;
                                    if (int.TryParse(cells1, out bog))
                                    {
                                        rows.Append(bog);
                                    }
                                    else
                                    {
                                        columns.Append(cells1);
                                    }
                                }

                                fromrow = rows.ToString();
                                fromcolumn = columns.ToString();
                                torow = Cells2.Select(c => c.ToString()).ToArray()[1];
                                tocolumn = Cells2.Select(c => c.ToString()).ToArray()[0];

                                returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, SheetName));
                            }
                            else if (Cells1.Length == 2 && Cells2.Length > 2)
                            {
                                StringBuilder rows = new StringBuilder(); StringBuilder columns = new StringBuilder();
                                foreach (string cells2 in Cells2.Select(c => c.ToString()).ToArray())
                                {
                                    int bog = 0;
                                    if (int.TryParse(cells2, out bog))
                                    {
                                        rows.Append(bog);
                                    }
                                    else
                                    {
                                        columns.Append(cells2);
                                    }
                                }

                                fromrow = Cells1.Select(c => c.ToString()).ToArray()[1];
                                fromcolumn = Cells1.Select(c => c.ToString()).ToArray()[0];
                                torow = rows.ToString();
                                tocolumn = columns.ToString();

                                returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, SheetName));
                            }
                            else if (Cells1.Length > 2 && Cells2.Length > 2)
                            {
                                StringBuilder rows1 = new StringBuilder(); StringBuilder columns1 = new StringBuilder();
                                foreach (string cells1 in Cells1.Select(c => c.ToString()).ToArray())
                                {
                                    int bog = 0;
                                    if (int.TryParse(cells1, out bog))
                                    {
                                        rows1.Append(bog);
                                    }
                                    else
                                    {
                                        columns1.Append(cells1);
                                    }
                                }


                                StringBuilder rows2 = new StringBuilder(); StringBuilder columns2 = new StringBuilder();
                                foreach (string cells2 in Cells2.Select(c => c.ToString()).ToArray())
                                {
                                    int bog = 0;
                                    if (int.TryParse(cells2, out bog))
                                    {
                                        rows2.Append(bog);
                                    }
                                    else
                                    {
                                        columns2.Append(cells2);
                                    }
                                }

                                fromrow = rows1.ToString();
                                fromcolumn = columns1.ToString();
                                torow = rows2.ToString();
                                tocolumn = columns2.ToString();

                                returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, SheetName));
                            }
                            else
                            {

                            }
                        }
                        else
                        {

                        }
                    }
                }



                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult GetRow( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string FileName = string.Empty; DataTable returnValue = new DataTable();

            string Range = string.Empty;
            string Input = string.Empty;
            string InputType = string.Empty;
            string Output = string.Empty;
            string SheetName = string.Empty;

            try
            {
                Input = stp.getPropertiesValue(StringHelper.instance, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                InputType = stp.getPropertiesValue(StringHelper.inputtype, step);
                DataTable dt = new TypeHandler().getInput(Input, executionID);
                Range = stp.getPropertiesValue("Range", step);
                string Cells = Range;
                SheetName = stp.getPropertiesValue(StringHelper.sheetname, step);

                List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                if (Cells.Trim() == StringHelper.WildCardStar)
                {
                    foreach (string file in files)
                    {
                        returnValue.Merge(ReadAsDataTable(file, false, SheetName));
                    }
                }
                else if (Cells.Trim() != string.Empty)
                {
                    if (files.Count == 0)
                    {
                        throw new ArgumentNullException("filenotfound", "File not Found");
                    }
                    foreach (string file in files)
                    {
                        if (Cells.IndexOf(":") > -1)
                        {
                            string Cells1 = Cells.Split(':')[0];
                            string Cells2 = Cells.Split(':')[1];

                            string fromrow = string.Empty;
                            string torow = string.Empty;
                            string fromcolumn = string.Empty;
                            string tocolumn = string.Empty;

                            if (Cells1.Length == 2 && Cells2.Length == 2)
                            {
                                fromrow = Cells1.Select(c => c.ToString()).ToArray()[1];
                                fromcolumn = Cells1.Select(c => c.ToString()).ToArray()[0];
                                torow = Cells2.Select(c => c.ToString()).ToArray()[1];
                                tocolumn = Cells2.Select(c => c.ToString()).ToArray()[0];

                                returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, SheetName));
                            }
                            else if (Cells1.Length > 2 && Cells2.Length == 2)
                            {
                                StringBuilder rows = new StringBuilder(); StringBuilder columns = new StringBuilder();
                                foreach (string cells1 in Cells1.Select(c => c.ToString()).ToArray())
                                {
                                    int bog = 0;
                                    if (int.TryParse(cells1, out bog))
                                    {
                                        rows.Append(bog);
                                    }
                                    else
                                    {
                                        columns.Append(cells1);
                                    }
                                }

                                fromrow = rows.ToString();
                                fromcolumn = columns.ToString();
                                torow = Cells2.Select(c => c.ToString()).ToArray()[1];
                                tocolumn = Cells2.Select(c => c.ToString()).ToArray()[0];

                                returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, SheetName));
                            }
                            else if (Cells1.Length == 2 && Cells2.Length > 2)
                            {
                                StringBuilder rows = new StringBuilder(); StringBuilder columns = new StringBuilder();
                                foreach (string cells2 in Cells2.Select(c => c.ToString()).ToArray())
                                {
                                    int bog = 0;
                                    if (int.TryParse(cells2, out bog))
                                    {
                                        rows.Append(bog);
                                    }
                                    else
                                    {
                                        columns.Append(cells2);
                                    }
                                }

                                fromrow = Cells1.Select(c => c.ToString()).ToArray()[1];
                                fromcolumn = Cells1.Select(c => c.ToString()).ToArray()[0];
                                torow = rows.ToString();
                                tocolumn = columns.ToString();

                                returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, SheetName));
                            }
                            else if (Cells1.Length > 2 && Cells2.Length > 2)
                            {
                                StringBuilder rows1 = new StringBuilder(); StringBuilder columns1 = new StringBuilder();
                                foreach (string cells1 in Cells1.Select(c => c.ToString()).ToArray())
                                {
                                    int bog = 0;
                                    if (int.TryParse(cells1, out bog))
                                    {
                                        rows1.Append(bog);
                                    }
                                    else
                                    {
                                        columns1.Append(cells1);
                                    }
                                }


                                StringBuilder rows2 = new StringBuilder(); StringBuilder columns2 = new StringBuilder();
                                foreach (string cells2 in Cells2.Select(c => c.ToString()).ToArray())
                                {
                                    int bog = 0;
                                    if (int.TryParse(cells2, out bog))
                                    {
                                        rows2.Append(bog);
                                    }
                                    else
                                    {
                                        columns2.Append(cells2);
                                    }
                                }

                                fromrow = rows1.ToString();
                                fromcolumn = columns1.ToString();
                                torow = rows2.ToString();
                                tocolumn = columns2.ToString();

                                returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, SheetName));
                            }
                            else
                            {

                            }
                        }
                        else
                        {

                        }
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                if(SheetExists!=String.Empty)
                het = het + SheetExists + "\n";
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult SetRow( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string FileName = string.Empty; DataTable returnValue = new DataTable();

            string Range = string.Empty;
            string Input = string.Empty;
            string InputType = string.Empty;
            string Output = string.Empty;
            string SheetName = string.Empty;
            string File = string.Empty;

            try
            {
                SheetName = stp.getPropertiesValue(StringHelper.sheetname, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                InputType = stp.getPropertiesValue(StringHelper.input_type, step);
                Range = stp.getPropertiesValue("Range", step);
                File = stp.getPropertiesValue(StringHelper.instance, step);

                List<string> cells = new List<string>();
                List<string> values = new List<string>();

                DataTable tablu = new DataTable();
                tablu.Columns.Add("Files");

                string Cells = Range;

                //if (Cells.IndexOf(":") > -1)
                //{
                //    udt = new TypeHandler().getInput(Cells, "Value", executionID);
                //}
                //else
                //{
                //    udt = new TypeHandler().getInput(Cells, "Variable", executionID);
                //}
                DataTable dt = new DataTable();

                if (varh.checkVariableExists(File, executionID))
                {
                    dt = new TypeHandler().getInput(File, executionID);
                }
                else
                {
                    dt = new TypeHandler().getInput(File, "Value", executionID);
                }

                if (Cells.Trim() == "*")
                {
                    DataTable dtr = new DataTable();
                    DataSet ds = new DataSet();
                    if (varh.checkVariableExists(Input, executionID))
                    {
                        dtr = new TypeHandler().getInput(Input, executionID);
                        ds.Tables.Add(dtr);
                    }
                    else
                    {

                    }
                    foreach (DataRow dr in dt.Rows)
                    {
                        string filesPath = dr["Files"].ToString();

                        for(int i=0; i< dtr.Rows.Count; i++)
                        {
                            for (int j = 0; j < dtr.Columns.Count; j++)
                            {
                                string colLetter = ColumnIndexToColumnLetter(j+1);
                                string cellAddress = colLetter + (i + 1).ToString();
                                cells.Add(cellAddress);
                                values.Add(dtr.Rows[i][j].ToString());
                            }
                        }
                        UpdateCells(filesPath, values, cells, SheetName);
                    }
                }
                else
                { 
                    if (varh.checkVariableExists(Cells, executionID))
                    {
                        DataTable dtr = new TypeHandler().getInput(Cells, executionID);
                        //cells = dtr.AsEnumerable().Select(r => r.Field<string>("Range")).ToList();
                        List<string> thisList = dtr.AsEnumerable().Select(r => r.Field<string>("Range")).ToList();
                        if (thisList[0].Contains(":"))
                        {
                            string ranger = thisList[0];
                            List<string> rangeLetters = ranger.Split(':').ToList();
                            KeyValuePair<int, int> initValue = ColumnNameToNumber(rangeLetters[0]);
                            KeyValuePair<int, int> finalValue = ColumnNameToNumber(rangeLetters[1]);

                            for (int y = initValue.Value; y <= finalValue.Value; y++)
                            {
                                for (int z = initValue.Key; z <= finalValue.Key; z++)
                                {
                                    cells.Add(ColumnIndexToColumnLetter(z) + y.ToString());
                                }
                            }
                        }
                        else
                        {
                            cells = thisList;
                        }
                    }
                    else
                    {
                        //cells.AddRange(Cells.Split(';').ToList());
                        List<string> thisList = Cells.Split(';').ToList();
                        if (thisList[0].Contains(":"))
                        {
                            string ranger = thisList[0];
                            List<string> rangeLetters = ranger.Split(':').ToList();
                            KeyValuePair<int, int> initValue = ColumnNameToNumber(rangeLetters[0]);
                            KeyValuePair<int, int> finalValue = ColumnNameToNumber(rangeLetters[1]);

                            for (int y = initValue.Value; y <= finalValue.Value; y++)
                            {
                                for (int z = initValue.Key; z <= finalValue.Key; z++)
                                {
                                    cells.Add(ColumnIndexToColumnLetter(z) + y.ToString());
                                }
                            }
                        }
                        else
                        {
                            cells.AddRange(thisList);
                        }
                    }
                    if (varh.checkVariableExists(Input, executionID))
                    {
                        DataTable dtr = new TypeHandler().getInput(Input, executionID);
                        try
                        {
                            values = dtr.AsEnumerable().Select(r => r.Field<string>("Range")).ToList();
                        }
                        catch
                        {
                            values = dtr.AsEnumerable().Select(x => x[0].ToString()).ToList();
                        }
                    }
                    else
                    {
                        values.AddRange(Input.Split(';').ToList());
                    }

                    if (varh.checkVariableExists(SheetName, executionID))
                    {
                        DataTable dtr = new TypeHandler().getInput(SheetName, executionID);
                        if (dtr.Rows.Count > 0)
                        {
                            SheetName = dtr.Rows[0][0].ToString();
                        }
                    }


                    //string homePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                    string homePath = Directory.GetCurrentDirectory();
                    string filesFolder = homePath + StringHelper.files;

                    foreach (DataRow dtr in dt.Rows)
                    {
                        string filesPath = dtr["Files"].ToString();

                        UpdateCells(filesPath, values, cells, SheetName);

                        tablu.Rows.Add(filesPath);
                    }

                }

                returnValue = tablu;

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                if (SheetExists != String.Empty)
                    het = het + SheetExists + "\n";
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public FileInfo[] GetNetworkFiles()
        {
            //string dataFile = HostingEnvironment.ApplicationPhysicalPath + "\\Uploads";
            string dataFile = Path.Combine(Directory.GetCurrentDirectory(), "\\Uploads");
            DirectoryInfo d = new DirectoryInfo(dataFile);
            FileInfo[] Files = d.GetFiles("*.xlsx");
            return Files;
        }
        public DataTable getExcel(string file, string sheetName)
        {
            DataTable returns = new DataTable();
            //using (var stream = System.IO.File.Open(file, FileMode.Open, FileAccess.Read))
            //{
            //    using (var reader = ExcelReaderFactory.CreateReader(stream))
            //    {
            //        // Choose one of either 1 or 2:

            //        // 1. Use the reader methods
            //        do
            //        {
            //            while (reader.Read())
            //            {
            //                // reader.GetDouble(0);
            //            }
            //        } while (reader.NextResult());

            //        // 2. Use the AsDataSet extension method
            //        var result = reader.AsDataSet();
            //        returns.Merge(result.Tables[sheetName]);
            //    }
            //}

            //if(returns.Rows.Count > 0)
            //{
            //    foreach (DataColumn dc in returns.Columns)
            //    {
            //        try
            //        {
            //            dc.ColumnName = returns.Rows[0][dc.ColumnName].ToString();
            //        }
            //        catch (Exception f)
            //        {
            //            string fu = f.Message;
            //        }
            //    }
            //    //returns.Rows[0].Delete();
            //}

            return returns;
        }

        public DataTable ExcelToDataTable(byte[] excelDocumentAsBytes, bool hasHeaderRow)
        {
            DataTable dt = new DataTable();
            string errorMessages = "";

            //create a new Excel package in a memorystream
            using (MemoryStream stream = new MemoryStream(excelDocumentAsBytes))
            using (ExcelPackage excelPackage = new ExcelPackage(stream))
            {
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets[1];

                //check if the worksheet is completely empty
                if (worksheet.Dimension == null)
                {
                    return dt;
                }

                //add the columns to the datatable
                for (int j = worksheet.Dimension.Start.Column; j <= worksheet.Dimension.End.Column; j++)
                {
                    string columnName = "Column " + j;
                    var excelCell = worksheet.Cells[1, j].Value;

                    if (excelCell != null)
                    {
                        var excelCellDataType = excelCell;

                        //if there is a headerrow, set the next cell for the datatype and set the column name
                        if (hasHeaderRow == true)
                        {
                            excelCellDataType = worksheet.Cells[2, j].Value;

                            columnName = excelCell.ToString();

                            //check if the column name already exists in the datatable, if so make a unique name
                            if (dt.Columns.Contains(columnName) == true)
                            {
                                columnName = columnName + "_" + j;
                            }
                        }

                        //try to determine the datatype for the column (by looking at the next column if there is a header row)
                        if (excelCellDataType is DateTime)
                        {
                            dt.Columns.Add(columnName, typeof(DateTime));
                        }
                        else if (excelCellDataType is Boolean)
                        {
                            dt.Columns.Add(columnName, typeof(String));
                        }
                        else if (excelCellDataType is Double)
                        {
                            //determine if the value is a decimal or int by looking for a decimal separator
                            //not the cleanest of solutions but it works since excel always gives a double
                            if (excelCellDataType.ToString().Contains(".") || excelCellDataType.ToString().Contains(","))
                            {
                                dt.Columns.Add(columnName, typeof(String));
                            }
                            else
                            {
                                dt.Columns.Add(columnName, typeof(String));
                            }
                        }
                        else
                        {
                            dt.Columns.Add(columnName, typeof(String));
                        }
                    }
                    else
                    {
                        dt.Columns.Add(columnName, typeof(String));
                    }
                }

                //start adding data the datatable here by looping all rows and columns
                for (int i = worksheet.Dimension.Start.Row + Convert.ToInt32(hasHeaderRow); i <= worksheet.Dimension.End.Row; i++)
                {
                    //create a new datatable row
                    DataRow row = dt.NewRow();

                    //loop all columns
                    for (int j = worksheet.Dimension.Start.Column; j <= worksheet.Dimension.End.Column; j++)
                    {
                        var excelCell = worksheet.Cells[i, j].Value;

                        //add cell value to the datatable
                        if (excelCell != null)
                        {
                            try
                            {
                                row[j - 1] = excelCell;
                            }
                            catch
                            {
                                row[j - 1] = excelCell.ToString();
                                errorMessages += "Row " + (i - 1) + ", Column " + j + ". Invalid " + dt.Columns[j - 1].DataType.ToString().Replace("System.", "") + " value:  " + excelCell.ToString() + "<br>";
                            }
                        }
                    }

                    //add the new row to the datatable
                    dt.Rows.Add(row);
                }
            }
            return dt;
        }
        public MethodResult AddSheet(Steps step, string executionID)
        {

            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string Output = string.Empty;
            string filename = string.Empty; string sheetname = string.Empty;

            DataTable returnValue = new DataTable();
            try
            {
                if (stp.getPropertiesValue(StringHelper.filename, step).Trim().ToLower() == string.Empty)
                {
                    filename = stp.getPropertiesValue("file name", step);
                }
                else
                {
                    filename = stp.getPropertiesValue(StringHelper.filename, step);
                }
                if (stp.getPropertiesValue(StringHelper.sheetname, step).Trim().ToLower() == string.Empty)
                {
                    sheetname = stp.getPropertiesValue("sheet name", step);
                }
                else
                {
                    sheetname = stp.getPropertiesValue(StringHelper.sheetname, step);
                }
                Output = stp.getPropertiesValue(StringHelper.output, step);

                List<string> Files = new List<string>();

                if(new VariableHandler().checkVariableExists(filename, executionID))
                {
                    DataTable dt = JsonConvert.DeserializeObject<DataTable>(new VariableHandler().getVariables(filename, executionID).vlvalue);
                    Files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                }
                else
                {
                    Files.Add(filename);
                }

                List<string> Sheets = new List<string>();

                if (new VariableHandler().checkVariableExists(sheetname, executionID))
                {
                    DataTable dt = JsonConvert.DeserializeObject<DataTable>(new VariableHandler().getVariables(sheetname, executionID).vlvalue);
                    Sheets = dt.AsEnumerable().Select(x => x[0].ToString()).ToList();
                }
                else
                {
                    if (sheetname.Contains(";"))
                    {
                        Sheets.AddRange(sheetname.Split(';').ToList());
                        //Sheets.RemoveAll(s => s.Trim() == string.Empty);
                    }
                    else
                    {
                        Sheets.Add(sheetname);
                    }
                }

                returnValue.Columns.Add("Files");

                foreach (string file in Files)
                {
                    foreach(string sheet in Sheets)
                    {
                        addSheet(file, sheet);
                    }
                    DeleteAWorkSheet(file, "EMPTY");
                    returnValue.Rows.Add(file);
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        private bool addSheet(string docName, string sheetname)
        {
            bool isDone = false;
            try
            {
                using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                {
                    try
                    {
                        // Add a blank WorksheetPart.
                        WorksheetPart newWorksheetPart = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
                        newWorksheetPart.Worksheet = new Worksheet(new SheetData());

                        Sheets sheets = spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                        string relationshipId = spreadSheet.WorkbookPart.GetIdOfPart(newWorksheetPart);

                        // Get a unique ID for the new worksheet.
                        uint sheetId = 1;
                        if (sheets.Elements<Sheet>().Count() > 0)
                        {
                            sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                        }

                        // Give the new worksheet a name.
                        string sheetName = "Sheet" + sheetId;

                        if (sheetname.Trim() != string.Empty)
                        {
                            sheetName = sheetname;
                        }

                        if (sheetname.Length > 30)
                        {
                            sheetName = sheetname.Substring(0, Math.Min(sheetname.Length, 30));
                        }

                        // Append the new worksheet and associate it with the workbook.
                        Sheet sheet = new Sheet() { Id = relationshipId, SheetId = sheetId, Name = sheetName };
                        sheets.Append(sheet);
                        isDone = true;

                        spreadSheet.WorkbookPart.Workbook.Save();
                    }
                    catch { }
                    spreadSheet.Save();
                    spreadSheet.Close();
                }
            }
            catch(Exception ezx)
            {
                string h = ezx.Message;
            }
            return isDone;
        }
        private void DeleteAWorkSheet(string fileName, string sheetToDelete)
        {
            string Sheetid = "";
            //Open the workbook
            using (SpreadsheetDocument document = SpreadsheetDocument.Open(fileName, true))
            {
                WorkbookPart wbPart = document.WorkbookPart;

                // Get the pivot Table Parts
                IEnumerable<PivotTableCacheDefinitionPart> pvtTableCacheParts = wbPart.PivotTableCacheDefinitionParts;
                Dictionary<PivotTableCacheDefinitionPart, string> pvtTableCacheDefinationPart = new Dictionary<PivotTableCacheDefinitionPart, string>();
                foreach (PivotTableCacheDefinitionPart Item in pvtTableCacheParts)
                {
                    PivotCacheDefinition pvtCacheDef = Item.PivotCacheDefinition;
                    //Check if this CacheSource is linked to SheetToDelete
                    var pvtCahce = pvtCacheDef.Descendants<CacheSource>().Where(s => s.WorksheetSource.Sheet == sheetToDelete);
                    if (pvtCahce.Count() > 0)
                    {
                        pvtTableCacheDefinationPart.Add(Item, Item.ToString());
                    }
                }
                foreach (var Item in pvtTableCacheDefinationPart)
                {
                    wbPart.DeletePart(Item.Key);
                }
                //Get the SheetToDelete from workbook.xml
                Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().Where(s => s.Name == sheetToDelete).FirstOrDefault();
                if (theSheet != null)
                {
                    // The specified sheet doesn't exist.

                    //Store the SheetID for the reference
                    Sheetid = theSheet.SheetId;

                    // Remove the sheet reference from the workbook.
                    WorksheetPart worksheetPart = (WorksheetPart)(wbPart.GetPartById(theSheet.Id));
                    theSheet.Remove();

                    // Delete the worksheet part.
                    wbPart.DeletePart(worksheetPart);

                    //Get the DefinedNames
                    var definedNames = wbPart.Workbook.Descendants<DefinedNames>().FirstOrDefault();
                    if (definedNames != null)
                    {
                        List<DefinedName> defNamesToDelete = new List<DefinedName>();

                        foreach (DefinedName Item in definedNames)
                        {
                            // This condition checks to delete only those names which are part of Sheet in question
                            if (Item.Text.Contains(sheetToDelete + "!"))
                                defNamesToDelete.Add(Item);
                        }

                        foreach (DefinedName Item in defNamesToDelete)
                        {
                            Item.Remove();
                        }

                    }
                    // Get the CalculationChainPart 
                    //Note: An instance of this part type contains an ordered set of references to all cells in all worksheets in the 
                    //workbook whose value is calculated from any formula

                    CalculationChainPart calChainPart;
                    calChainPart = wbPart.CalculationChainPart;
                    if (calChainPart != null)
                    {
                        var calChainEntries = calChainPart.CalculationChain.Descendants<CalculationCell>().Where(c => c.SheetId == Sheetid);
                        List<CalculationCell> calcsToDelete = new List<CalculationCell>();
                        foreach (CalculationCell Item in calChainEntries)
                        {
                            calcsToDelete.Add(Item);
                        }

                        foreach (CalculationCell Item in calcsToDelete)
                        {
                            Item.Remove();
                        }

                        if (calChainPart.CalculationChain.Count() == 0)
                        {
                            wbPart.DeletePart(calChainPart);
                        }
                    }

                    // Save the workbook.
                    wbPart.Workbook.Save();
                }
            }
        }
        private string ColumnIndexToColumnLetter(int colIndex)
        {
            int div = colIndex;
            string colLetter = String.Empty;
            int mod = 0;

            while (div > 0)
            {
                mod = (div - 1) % 26;
                colLetter = (char)(65 + mod) + colLetter;
                div = (int)((div - mod) / 26);
            }
            return colLetter;
        }
        private string Replacetext(string line)
        {
            int startindex = 0;
            int Endindex = 0;
            if (line.First().ToString() == "\"")
            {
                startindex = line.IndexOf("\"");
            }
            else if (line.Contains(",\""))
            {
                startindex = line.IndexOf(",\"");
            }
            else
            {
                startindex = line.IndexOf("\"");
            }
            if (line.Contains("\","))
            {
                Endindex = line.IndexOf("\",");
            }
            else if (line.Last().ToString() == "\"")
            {
                Endindex = line.LastIndexOf("\"");
            }
            else {
                Endindex = line.IndexOf("\"");
            }
            if (startindex < Endindex)
            {
                string outputstring = string.Empty;
                if (startindex == 0)
                {
                    outputstring = "\"" + line.Substring(startindex + 1, Endindex - startindex - 1) + "\"";
                }
                else {
                    outputstring = line.Substring(startindex + 1, Endindex - startindex - 1) + "\"";
                }
                string finaltext = outputstring;
                finaltext = finaltext.Replace(",", "|");
                finaltext = finaltext.Replace("\"", "");
                line = line.Replace(outputstring, finaltext);
            }
            else {

            }
            if (line.Contains(",\""))
            {
                line = Replacetext(line);
            }
            return line;
        }
    private KeyValuePair<int, int> ColumnNameToNumber(string col_name)
        {
            int result = 0;

            Regex re = new Regex(@"([a-zA-Z]+)(\d+)");
            Match reresult = re.Match(col_name);

            string alphaPart = reresult.Groups[1].Value;
            string numberPart = reresult.Groups[2].Value;

            // Process each letter.
            for (int i = 0; i < alphaPart.Length; i++)
            {
                result *= 26;
                char letter = alphaPart[i];

                // See if it's out of bounds.
                if (letter < 'A') letter = 'A';
                if (letter > 'Z') letter = 'Z';

                // Add in the value of this letter.
                result += (int)letter - (int)'A' + 1;
            }
            return new KeyValuePair<int, int>(result, Convert.ToInt32(numberPart));
        }
        public MethodResult GetColumn(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string FileName = string.Empty; DataTable returnValue = new DataTable();

            string Range = string.Empty;
            string Input = string.Empty;
            string Column = string.Empty;
            string Output = string.Empty;
            string SheetName = string.Empty;

            try
            {
                Input = stp.getPropertiesValue(StringHelper.instance, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                DataTable dt = new TypeHandler().getInput(Input, executionID);
                Range = stp.getPropertiesValue("Range", step);
                string Cells = Range;
                SheetName = stp.getPropertiesValue(StringHelper.sheetname, step);
                Column = stp.getPropertiesValue(StringHelper.Column, step);

                List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                if (Cells.Trim() == StringHelper.WildCardStar)
                {
                    foreach (string file in files)
                    {
                        returnValue.Merge(ReadAsDataTable(file, false, SheetName));
                    }
                }
                else if (Cells.Trim() != string.Empty)
                {
                    if (files.Count == 0)
                    {
                        throw new ArgumentNullException("filenotfound", "File not Found");
                    }
                    foreach (string file in files)
                    {
                        if (Cells.IndexOf(":") > -1)
                        {
                            string Cells1 = Cells.Split(':')[0];
                            string Cells2 = Cells.Split(':')[1];

                            string fromrow = string.Empty;
                            string torow = string.Empty;
                            string fromcolumn = string.Empty;
                            string tocolumn = string.Empty;

                            if (Cells1.Length == 2 && Cells2.Length == 2)
                            {
                                fromrow = Cells1.Select(c => c.ToString()).ToArray()[1];
                                fromcolumn = Cells1.Select(c => c.ToString()).ToArray()[0];
                                torow = Cells2.Select(c => c.ToString()).ToArray()[1];
                                tocolumn = Cells2.Select(c => c.ToString()).ToArray()[0];

                                returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, SheetName));
                            }
                            else if (Cells1.Length > 2 && Cells2.Length == 2)
                            {
                                StringBuilder rows = new StringBuilder(); StringBuilder columns = new StringBuilder();
                                foreach (string cells1 in Cells1.Select(c => c.ToString()).ToArray())
                                {
                                    int bog = 0;
                                    if (int.TryParse(cells1, out bog))
                                    {
                                        rows.Append(bog);
                                    }
                                    else
                                    {
                                        columns.Append(cells1);
                                    }
                                }

                                fromrow = rows.ToString();
                                fromcolumn = columns.ToString();
                                torow = Cells2.Select(c => c.ToString()).ToArray()[1];
                                tocolumn = Cells2.Select(c => c.ToString()).ToArray()[0];

                                returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, SheetName));
                            }
                            else if (Cells1.Length == 2 && Cells2.Length > 2)
                            {
                                StringBuilder rows = new StringBuilder(); StringBuilder columns = new StringBuilder();
                                foreach (string cells2 in Cells2.Select(c => c.ToString()).ToArray())
                                {
                                    int bog = 0;
                                    if (int.TryParse(cells2, out bog))
                                    {
                                        rows.Append(bog);
                                    }
                                    else
                                    {
                                        columns.Append(cells2);
                                    }
                                }

                                fromrow = Cells1.Select(c => c.ToString()).ToArray()[1];
                                fromcolumn = Cells1.Select(c => c.ToString()).ToArray()[0];
                                torow = rows.ToString();
                                tocolumn = columns.ToString();

                                returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, SheetName));
                            }
                            else if (Cells1.Length > 2 && Cells2.Length > 2)
                            {
                                StringBuilder rows1 = new StringBuilder(); StringBuilder columns1 = new StringBuilder();
                                foreach (string cells1 in Cells1.Select(c => c.ToString()).ToArray())
                                {
                                    int bog = 0;
                                    if (int.TryParse(cells1, out bog))
                                    {
                                        rows1.Append(bog);
                                    }
                                    else
                                    {
                                        columns1.Append(cells1);
                                    }
                                }


                                StringBuilder rows2 = new StringBuilder(); StringBuilder columns2 = new StringBuilder();
                                foreach (string cells2 in Cells2.Select(c => c.ToString()).ToArray())
                                {
                                    int bog = 0;
                                    if (int.TryParse(cells2, out bog))
                                    {
                                        rows2.Append(bog);
                                    }
                                    else
                                    {
                                        columns2.Append(cells2);
                                    }
                                }

                                fromrow = rows1.ToString();
                                fromcolumn = columns1.ToString();
                                torow = rows2.ToString();
                                tocolumn = columns2.ToString();

                                returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, SheetName));
                            }
                            else
                            {

                            }
                        }
                        else
                        {

                        }
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult SetColumn(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string FileName = string.Empty; DataTable returnValue = new DataTable();

            string Range = string.Empty;
            string Input = string.Empty;
            string column = string.Empty;
            string Values = string.Empty;
            string SheetName = string.Empty;

            try
            {
                SheetName = stp.getPropertiesValue(StringHelper.sheetname, step);
                Input = stp.getPropertiesValue(StringHelper.instance, step);
                Values = stp.getPropertiesValue(StringHelper.values, step);
                column = stp.getPropertiesValue(StringHelper.Column, step);
                Range = stp.getPropertiesValue("Range", step);

                List<string> cells = new List<string>();
                List<string> values = new List<string>();

                DataTable tablu = new DataTable();
                tablu.Columns.Add("Files");

                string Cells = Range;

                DataTable dt = new DataTable();

                if (varh.checkVariableExists(Input, executionID))
                {
                    dt = new TypeHandler().getInput(Input, executionID);
                }
                else
                {
                    dt = new TypeHandler().getInput(Input, "Value", executionID);
                }
                if (Cells.Trim() == "*")
                {
                    DataTable dtr = new DataTable();
                    DataSet ds = new DataSet();
                    if (varh.checkVariableExists(Input, executionID))
                    {
                        dtr = new TypeHandler().getInput(Input, executionID);
                        ds.Tables.Add(dtr);
                    }
                    else
                    {

                    }
                    foreach (DataRow dr in dt.Rows)
                    {
                        string filesPath = dr["Files"].ToString();

                        for (int i = 0; i < dtr.Rows.Count; i++)
                        {
                            for (int j = 0; j < dtr.Columns.Count; j++)
                            {
                                string colLetter = ColumnIndexToColumnLetter(j + 1);
                                string cellAddress = colLetter + (i + 1).ToString();
                                cells.Add(cellAddress);
                                values.Add(dtr.Rows[i][j].ToString());
                            }
                        }
                        UpdateCells(filesPath, values, cells, SheetName);
                    }
                }
                else
                {
                    if (varh.checkVariableExists(Cells, executionID))
                    {
                        DataTable dtr = new TypeHandler().getInput(Cells, executionID);
                        List<string> thisList = dtr.AsEnumerable().Select(r => r.Field<string>("Range")).ToList();
                        if (thisList[0].Contains(":"))
                        {
                            string ranger = thisList[0];
                            List<string> rangeLetters = ranger.Split(':').ToList();
                            KeyValuePair<int, int> initValue = ColumnNameToNumber(rangeLetters[0]);
                            KeyValuePair<int, int> finalValue = ColumnNameToNumber(rangeLetters[1]);

                            for (int y = initValue.Key; y < finalValue.Key; y++)
                            {
                                cells.Add(ColumnIndexToColumnLetter(y) + initValue.Value);
                            }
                        }
                        else
                        {
                            cells = thisList;
                        }
                    }
                    else
                    {
                        List<string> thisList = Cells.Split(';').ToList();
                        if (thisList[0].Contains(":"))
                        {
                            string ranger = thisList[0];
                            List<string> rangeLetters = ranger.Split(':').ToList();
                            KeyValuePair<int, int> initValue = ColumnNameToNumber(rangeLetters[0]);
                            KeyValuePair<int, int> finalValue = ColumnNameToNumber(rangeLetters[1]);

                            for (int y = initValue.Value; y <= finalValue.Value; y++)
                            {
                                cells.Add(ColumnIndexToColumnLetter(initValue.Key) + y.ToString());
                            }
                        }
                        else
                        {
                            cells.AddRange(thisList);
                        }
                    }
                    if (varh.checkVariableExists(Values, executionID))
                    {
                        DataTable dtr = new TypeHandler().getInput(Values, executionID);
                        try
                        {
                            values = dtr.AsEnumerable().Select(r => r.Field<string>("Range")).ToList();
                        }
                        catch
                        {
                            if (dtr.Rows.Count > 0)
                            {
                                values = dtr.AsEnumerable().Select(x => x[0].ToString()).ToList();
                            }
                        }
                    }
                    else
                    {
                        values.AddRange(Values.Split(';').ToList());
                    }

                    string sheetname = string.Empty;
                    if (varh.checkVariableExists(SheetName, executionID))
                    {
                        DataTable dtr = new TypeHandler().getInput(SheetName, executionID);
                        if (dtr.Rows.Count > 0)
                        {
                            sheetname = dtr.Rows[0][0].ToString();
                        }
                    }
                    else
                    {
                        sheetname = SheetName;
                    }


                    //string homePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                    string homePath = Directory.GetCurrentDirectory();
                    string filesFolder = homePath + StringHelper.files;

                    foreach (DataRow dtr in dt.Rows)
                    {
                        string filesPath = dtr["Files"].ToString();

                        if (sheetname.Trim() != string.Empty && (values.Count > 0 && cells.Count > 0))
                        {
                            UpdateCells(filesPath, values, cells, sheetname);
                        }

                        tablu.Rows.Add(filesPath);
                    }

                }
                returnValue = tablu;

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        private bool IsValidXmlString(string text)
        {
            try
            {
                XmlConvert.VerifyXmlChars(text);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private string RemoveInvalidXmlChars(string text)
        {
            var validXmlChars = text.Where(ch => XmlConvert.IsXmlChar(ch)).ToArray();
            return new string(validXmlChars);
        }
        private static string GetFormattedCellValue(WorkbookPart workbookPart, Cell cell)
        {
            if (cell == null)
            {
                return null;
            }

            string value = "";
            if (cell.DataType == null) // number & dates
            {
                int styleIndex = (int)cell.StyleIndex.Value;
                CellFormat cellFormat = (CellFormat)workbookPart.WorkbookStylesPart.Stylesheet.CellFormats.ElementAt(styleIndex);
                uint formatId = cellFormat.NumberFormatId.Value;

                if ((formatId == (uint)Formats.DateShort || formatId == (uint)Formats.DateIndian) || (formatId == (uint)Formats.Date || formatId == (uint)Formats.DateLong))
                {
                    double oaDate;
                    if (double.TryParse(cell.InnerText, out oaDate))
                    {
                        value = DateTime.FromOADate(oaDate).ToShortDateString();
                    }
                }
                else
                {
                    value = cell.InnerText;
                }
            }
            else // Shared string or boolean
            {
                switch (cell.DataType.Value)
                {
                    case CellValues.SharedString:
                        SharedStringItem ssi = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(int.Parse(cell.CellValue.InnerText));
                        value = ssi.Text.Text;
                        break;
                    case CellValues.Boolean:
                        value = cell.CellValue.InnerText == "0" ? "false" : "true";
                        break;
                    default:
                        value = cell.CellValue.InnerText;
                        break;
                }
            }

            return value;
        }
        private enum Formats
        {
            General = 0,
            Number = 1,
            Decimal = 2,
            Currency = 164,
            Accounting = 44,
            DateShort = 14,
            DateLong = 165,
            Time = 166,
            Percentage = 10,
            Fraction = 12,
            Scientific = 11,
            Text = 49,
            Date = 68,
            DateIndian = 58
        }
        /*  0 = 'General';
            1 = '0';
            2 = '0.00';
            3 = '#,##0';
            4 = '#,##0.00';
            5 = '$#,##0;\-$#,##0';
            6 = '$#,##0;[Red]\-$#,##0';
            7 = '$#,##0.00;\-$#,##0.00';
            8 = '$#,##0.00;[Red]\-$#,##0.00';
            9 = '0%';
            10 = '0.00%';
            11 = '0.00E+00';
            12 = '# ?/?';
            13 = '# ??/??';
            14 = 'mm-dd-yy';
            15 = 'd-mmm-yy';
            16 = 'd-mmm';
            17 = 'mmm-yy';
            18 = 'h:mm AM/PM';
            19 = 'h:mm:ss AM/PM';
            20 = 'h:mm';
            21 = 'h:mm:ss';
            22 = 'm/d/yy h:mm';

            37 = '#,##0 ;(#,##0)';
            38 = '#,##0 ;[Red](#,##0)';
            39 = '#,##0.00;(#,##0.00)';
            40 = '#,##0.00;[Red](#,##0.00)';

            44 = '_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)';
            45 = 'mm:ss';
            46 = '[h]:mm:ss';
            47 = 'mmss.0';
            48 = '##0.0E+0';
            49 = '@';

            27 = '[$-404]e/m/d';
            30 = 'm/d/yy';
            36 = '[$-404]e/m/d';
            50 = '[$-404]e/m/d';
            57 = '[$-404]e/m/d';

            59 = 't0';
            60 = 't0.00';
            61 = 't#,##0';
            62 = 't#,##0.00';
            67 = 't0%';
            68 = 't0.00%';
            69 = 't# ?/?';
            70 = 't# ??/??';
         */
    }
}



