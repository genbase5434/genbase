﻿using Genbase.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Newtonsoft.Json;
using Genbase.Classes;

namespace Genbase.BLL.ElementsClasses
{
    public class Arithmetic
    {
        StepPropertyHandler stp = new StepPropertyHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        /// <summary>
        /// Add Method for Arithmetic elements Tools set
        /// </summary>
        /// <param name="step">step</param>
        /// <param name="executionID">executionID</param>
        /// <returns></returns>
        public MethodResult Add(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            returnValue.Columns.Add("Operand1"); returnValue.Columns.Add("Operand2"); returnValue.Columns.Add("Operator"); returnValue.Columns.Add("Result");
            returnValue.Columns.Add("Status");
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
            string input1 = string.Empty; string input2 = string.Empty; string outputlist = string.Empty; string inputtype1 = string.Empty; string outputtype = string.Empty; string inputtype2 = string.Empty;
            string result = string.Empty;
            try
            {
                input1 = stp.getPropertiesValue(StringHelper.input1, step);
                input2 = stp.getPropertiesValue(StringHelper.input2, step);
                outputlist = stp.getPropertiesValue(StringHelper.output, step);
                inputtype1 = stp.getPropertiesValue(StringHelper.inputtype1, step);
                inputtype2 = stp.getPropertiesValue(StringHelper.inputtype2, step);
                List<string> a = getArthimeticInputs(inputtype1, input1, inputtype2, input2, executionID);
                result = OperationD(a, StringHelper.add);
                if (outputlist.Trim() != string.Empty)
                {
                    List<string> inputPart1 = new List<string>();
                    foreach (string vals in a)
                    {
                        inputPart1.Add(vals);
                    }
                    if (inputPart1[0] == "")
                    {
                        returnValue.Rows.Add("", inputPart1[1], StringHelper.add, result, "Success");
                    }
                    else if (inputPart1[1] == "")
                    {
                        returnValue.Rows.Add(inputPart1[0], "", StringHelper.add, result, "Success");
                    }
                    else
                    {
                        returnValue.Rows.Add(inputPart1[0], inputPart1[1], StringHelper.add, result, "Success");
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(outputlist, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        /// <summary>
        /// getArthimeticInputs Method for Arithmetic elements Tools set
        /// </summary>
        /// <param name="inputtype1">inputtype1</param>
        /// <param name="input1">input1</param>
        /// <param name="inputtype2">inputtype2</param>
        /// <param name="input2">input2</param>
        /// <param name="executionID">executionID</param>
        /// <returns></returns>
        public List<string> getArthimeticInputs(string inputtype1, string input1, string inputtype2, string input2,string executionID)
        {
            List<string> a = new List<string>();
            List<ArithmeticInput> vinputs = new List<ArithmeticInput>();
            ArithmeticInput vinps = new ArithmeticInput();
            vinps.type = inputtype1;
            vinps.value = input1;
            vinputs.Add(vinps);
            vinps = new ArithmeticInput();
            vinps.type = inputtype2;
            vinps.value = input2;
            vinputs.Add(vinps);
            vinputs.RemoveAll(x => x.value.Trim() == string.Empty);
            double cd = 0; //int ci = 0;
            string inputsresult = string.Empty;

            for (int i = 0; i < vinputs.Count; i++)
            {
                string b = string.Empty, c = string.Empty;
                DVariables value = new DVariables();
                if (vinputs[i].value.Trim() != string.Empty)
                {
                    if (vinputs[i].type.ToLower() == StringHelper.value)
                    {
                        cd = 0;
                        if (double.TryParse(vinputs[i].value, out cd))
                        {
                            a.Add(vinputs[i].value);
                        }
                    }
                    else
                    {
                        value = new VariableHandler().getVariables(vinputs[i].value, executionID);

                        if (value != null && value.vlvalue.GetType() == typeof(string))
                        {
                            DataTable dt = JsonConvert.DeserializeObject<DataTable>(new VariableHandler().getVariableValue(value));
                            int countCol = dt.Columns.Count;
                            string input = (dt.Rows[0][0]).ToString();
                            if (input == "")
                            {
                                a.Add(input);
                            }
                            if (double.TryParse(input, out cd))
                            {
                                a.Add(input);
                            }
                        }

                    }
                }
            }
            return a;
        }
        /// <summary>
        /// Subtract Method for Arithmetic elements Tools set
        /// </summary>
        /// <param name="step">step</param>
        /// <param name="executionID">executionID</param>
        /// <returns></returns>
        public MethodResult Subtract(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            returnValue.Columns.Add("Operand1"); returnValue.Columns.Add("Operand2"); returnValue.Columns.Add("Operator"); returnValue.Columns.Add("Result");
            returnValue.Columns.Add("Status");
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string input1 = string.Empty; string input2 = string.Empty; string outputlist = string.Empty; string inputtype1 = string.Empty; string inputtype2 = string.Empty; string outputtype = string.Empty;
            string result = string.Empty;
            try
            {
                input1 = stp.getPropertiesValue(StringHelper.input1, step);
                input2 = stp.getPropertiesValue(StringHelper.input2, step);
                outputlist = stp.getPropertiesValue(StringHelper.output, step);
                inputtype1 = stp.getPropertiesValue(StringHelper.inputtype1, step);
                inputtype2 = stp.getPropertiesValue(StringHelper.inputtype2, step);
                List<string> a = getArthimeticInputs(inputtype1, input1, inputtype2, input2, executionID);
                result = OperationD(a, StringHelper.subtract);
                if (outputlist.Trim() != string.Empty)
                {
                    List<string> inputPart1 = new List<string>();
                    foreach (string vals in a)
                    {
                        inputPart1.Add(vals);
                    }
                    if (inputPart1[0] == "")
                    {
                        returnValue.Rows.Add("", inputPart1[1], StringHelper.subtract, result, "Success");
                    }
                    else if (inputPart1[1] == "")
                    {
                        returnValue.Rows.Add(inputPart1[0], "", StringHelper.subtract, result, "Success");
                    }
                    else
                    {
                        returnValue.Rows.Add(inputPart1[0], inputPart1[1], StringHelper.subtract, result, "Success");
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(outputlist, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        /// <summary>
        /// Multiply Method for Arithmetic elements Tools set
        /// </summary>
        /// <param name="step"></param>
        /// <param name="executionID"></param>
        /// <returns></returns>
        public MethodResult Multiply(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            returnValue.Columns.Add("Operand1"); returnValue.Columns.Add("Operand2"); returnValue.Columns.Add("Operator"); returnValue.Columns.Add("Result");
            returnValue.Columns.Add("Status");
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string input1 = string.Empty; string input2 = string.Empty; string outputlist = string.Empty; string inputtype1 = string.Empty; string inputtype2 = string.Empty; string outputtype = string.Empty;
            string result = string.Empty;
            try
            {
                input1 = stp.getPropertiesValue(StringHelper.input1, step);
                input2 = stp.getPropertiesValue(StringHelper.input2, step);
                outputlist = stp.getPropertiesValue(StringHelper.output, step);
                inputtype1 = stp.getPropertiesValue(StringHelper.inputtype1, step);
                inputtype2 = stp.getPropertiesValue(StringHelper.inputtype2, step);
                List<string> a = getArthimeticInputs(inputtype1, input1, inputtype2, input2, executionID);
                result = OperationD(a, StringHelper.multiply);
                if (outputlist.Trim() != string.Empty)
                {
                    List<string> inputPart1 = new List<string>();
                    foreach (string vals in a)
                    {
                        inputPart1.Add(vals);
                    }
                    if (inputPart1[0] == "")
                    {
                        returnValue.Rows.Add("", inputPart1[1], StringHelper.multiply, result, "Success");
                    }
                    else if (inputPart1[1] == "")
                    {
                        returnValue.Rows.Add(inputPart1[0], "", StringHelper.multiply, result, "Success");
                    }
                    else
                    {
                        returnValue.Rows.Add(inputPart1[0], inputPart1[1], StringHelper.multiply, result, "Success");
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(outputlist, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        /// <summary>
        /// Divide Method for Arithmetic elements Tools set
        /// </summary>
        /// <param name="step">step</param>
        /// <param name="executionID">executionID</param>
        /// <returns></returns>
        public MethodResult Divide(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            returnValue.Columns.Add("Operand1"); returnValue.Columns.Add("Operand2"); returnValue.Columns.Add("Operator"); returnValue.Columns.Add("Result");
            returnValue.Columns.Add("Status");
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string input1 = string.Empty; string input2 = string.Empty; string outputlist = string.Empty; string inputtype1 = string.Empty; string inputtype2 = string.Empty; string outputtype = string.Empty;
            string ismodulus = string.Empty; string result = string.Empty;

            try
            {
                input1 = stp.getPropertiesValue(StringHelper.input1, step);
                input2 = stp.getPropertiesValue(StringHelper.input2, step);
                outputlist = stp.getPropertiesValue(StringHelper.output, step);
                inputtype1 = stp.getPropertiesValue(StringHelper.inputtype1, step);
                inputtype2 = stp.getPropertiesValue(StringHelper.inputtype2, step);
                ismodulus = stp.getPropertiesValue(StringHelper.ismodulus, step);
                List<string> a = getArthimeticInputs(inputtype1, input1, inputtype2, input2, executionID);
                string operation = "";
                if (ismodulus.Trim().ToLower() == "false")
                {
                    result = OperationD(a, StringHelper.divide);
                    operation = StringHelper.divide;
                }
                else if (ismodulus.Trim().ToLower() == "true")
                {
                    result = OperationD(a, StringHelper.modulus);
                    operation = StringHelper.modulus;
                }
                else
                {
                    result = OperationD(a, StringHelper.divide);
                    operation = StringHelper.divide;
                }
                if (outputlist.Trim() != string.Empty)
                {
                    List<string> inputPart1 = new List<string>();
                    foreach (string vals in a)
                    {
                        inputPart1.Add(vals);
                    }
                    if (inputPart1[0] == "")
                    {
                        returnValue.Rows.Add("", inputPart1[1], operation, result, "Success");
                    }
                    else if (inputPart1[1] == "")
                    {
                        returnValue.Rows.Add(inputPart1[0], "", operation, result, "Success");
                    }
                    else
                    {
                        returnValue.Rows.Add(inputPart1[0], inputPart1[1], operation, result, "Success");
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(outputlist, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        /// <summary>
        /// OperationD  Method for Arithmetic elements Tools set
        /// </summary>
        /// <param name="primero">primero</param>
        /// <param name="operatorr">operatorr</param>
        /// <returns></returns>
        public string OperationD(List<string> primero, string operatorr)
        {
            try
            {
                double result = 0.0; List<double> a = new List<double>();
                if (primero.Count > 0)
                {
                    foreach (string val in primero)
                    {
                        if (val != "")
                        {
                            a.Add(Convert.ToDouble(val));
                        }
                    }
                }

                switch (operatorr)
                {
                    case StringHelper.add:
                        foreach (double val in a)
                        {
                            result += val;
                        }
                        break;
                    case StringHelper.subtract:
                        foreach (double val in a)
                        {
                            if (result != 0.0)
                            {
                                result = result - val;
                            }
                            else
                            {
                                result = val;
                            }
                        }
                        break;
                    case StringHelper.multiply:
                        result = 1.0;
                        foreach (double val in a)
                        {
                            result *= val;
                        }
                        break;
                    case StringHelper.divide:
                        foreach (double val in a)
                        {
                            if (result != 0.0)
                            {
                                if (val != 0)
                                {
                                    result = result / val;
                                }
                                else
                                {
                                    return "Operand2 should not be zero";
                                }
                            }
                            else
                            {
                                result = val;
                            }
                        }
                        break;
                    case StringHelper.modulus:
                        foreach (double val in a)
                        {
                            if (result != 0.0)
                            {
                                if (val != 0)
                                {
                                    result = result % val;
                                }
                                else
                                {
                                    return "Operand2 should not be zero";
                                }
                            }
                            else
                            {
                                result = val;
                            }
                        }

                        break;
                    default:
                        result = -1 / -1 * -1;
                        break;
                }

                return result.ToString();
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return "";
            }
        }
        /// <summary>
        /// OperationI Method for Arithmetic elements Tools set
        /// </summary>
        /// <param name="primero">primero</param>
        /// <param name="segoon">segoon</param>
        /// <param name="operatorr">operatorr</param>
        /// <returns></returns>
        public string OperationI(List<string> primero, string segoon, string operatorr)
        {
            try
            {
                int result = 0; List<int> a = new List<int>(); int b = 0;
                if (segoon == "" && primero.Count > 1)
                {
                    foreach (string val in primero)
                    {
                        a.Add(Convert.ToInt32(val));
                    }
                }
                else
                {
                    foreach (string val in primero)
                    {
                        a.Add(Convert.ToInt32(val));
                    }
                    b = Convert.ToInt32(segoon);
                }

                switch (operatorr)
                {
                    case StringHelper.add:
                        foreach (int val in a)
                        {
                            result += val;
                        }
                        if (segoon == "" && primero.Count > 0)
                        {
                            result += b;
                        }
                        break;
                    case StringHelper.subtract:
                        foreach (int val in a)
                        {
                            result -= val;
                        }
                        if (segoon == "" && primero.Count > 0)
                        {
                            result -= b;
                        }
                        break;
                    case StringHelper.multiply:
                        foreach (int val in a)
                        {
                            result *= val;
                        }
                        if (segoon == "" && primero.Count > 0)
                        {
                            result *= b;
                        }
                        break;
                    case StringHelper.divide:
                        foreach (int val in a)
                        {
                            result /= val;
                        }
                        if (segoon == "" && primero.Count > 0)
                        {
                            result /= b;
                        }
                        break;
                    case StringHelper.modulus:
                        foreach (int val in a)
                        {
                            result %= val;
                        }
                        if (segoon == "" && primero.Count > 0)
                        {
                            result %= b;
                        }
                        break;
                    default:
                        result = -1 / -1 * -1;
                        break;
                }

                return result.ToString();
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return "";
            }
        }
    }
}