﻿using Genbase.classes;
using Newtonsoft.Json;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Genbase.Classes;

namespace Genbase.BLL.ElementsClasses
{
    public class LinuxShell
    {
        StepPropertyHandler stp = new StepPropertyHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult Connection( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string Username = string.Empty; string Password = string.Empty; string Input = string.Empty; string Output = string.Empty;
            string Port = string.Empty; string URL = string.Empty; string Protocol = string.Empty; string OS = string.Empty; string loopcount = string.Empty;

            DataTable returnValue = new DataTable();

            try
            {
                Username = stp.getPropertiesValue(StringHelper.username, step);
                Password = stp.getPropertiesValue(StringHelper.password, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                Port = stp.getPropertiesValue(StringHelper.port, step);
                URL = stp.getPropertiesValue(StringHelper.url, step);
                Protocol = stp.getPropertiesValue(StringHelper.protocol, step);
                OS = stp.getPropertiesValue(StringHelper.ostype, step);
                loopcount = stp.getPropertiesValue(StringHelper.gotos, step);

                if (Input == string.Empty && URL != string.Empty)
                {
                    DataTable connect = new DataTable();
                    connect.Columns.Add("URL");
                    connect.Columns.Add("Username");
                    connect.Columns.Add("Password");
                    connect.Columns.Add("Port");
                    connect.Columns.Add("Protocol");
                    connect.Columns.Add("OS");

                    connect.Rows.Add(URL, Username, Password, Port, Protocol, OS);
                    returnValue.Merge(connect);
                }
                else
                {
                    DataTable dt = new TypeHandler().getInput(Input, StringHelper.variable, executionID);

                    DataTable connect = new DataTable();
                    connect.Columns.Add("URL");
                    connect.Columns.Add("Username");
                    connect.Columns.Add("Password");
                    connect.Columns.Add("Port");
                    connect.Columns.Add("Protocol");
                    connect.Columns.Add("OS");

                    int fstep = 0;

                    URL = dt.Rows[fstep].Field<string>("URL");
                    Username = dt.Rows[fstep].Field<string>("Username");
                    Password = dt.Rows[fstep].Field<string>("Password");
                    Port = dt.Rows[fstep].Field<string>("Port");
                    Protocol = dt.Rows[fstep].Field<string>("Protocol");
                    OS = dt.Rows[fstep].Field<string>("OS");

                    connect.Rows.Add(URL, Username, Password, Port, Protocol, OS);
                    returnValue.Merge(connect);
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                string tmp = het.Split('|')[10];
                het = het.Replace(tmp, "******");
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Execute( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string Command = string.Empty; string Input = string.Empty; string Output = string.Empty; string Filter = string.Empty;
            string Timeline = string.Empty; string Domain = string.Empty; DataTable returnValue = new DataTable();
            try
            {
                Command = stp.getPropertiesValue(StringHelper.command, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                Filter = stp.getPropertiesValue(StringHelper.filter, step);
                Timeline = stp.getPropertiesValue(StringHelper.timeline, step);
                Domain = stp.getPropertiesValue(StringHelper.domain, step);

                DataTable connect = new TypeHandler().getInput(Input, StringHelper.variable, executionID);
                if (connect != null)
                {
                    string URL = connect.Rows[0].Field<string>("URL");
                    string Username = connect.Rows[0].Field<string>("Username");
                    string Password = connect.Rows[0].Field<string>("Password");
                    string Port = connect.Rows[0].Field<string>("Port");
                    string Protocol = connect.Rows[0].Field<string>("Protocol");
                    string OS = connect.Rows[0].Field<string>("OS");


                    using (var ssh = new SshClient(new MethodResultHandler().CreateConnection(URL.Trim(), int.Parse(Port.Trim()), Username.Trim(), Password.Trim())))
                    {
                        string cmd = string.Empty, answer = string.Empty; SshCommand cmd1 = null; string noanswer = string.Empty;
                        cmd = Command;
                        ssh.Connect();

                        SshCommand ssc = ssh.CreateCommand(cmd);
                        ssc.Execute();
                        cmd1 = ssh.RunCommand(cmd);
                        answer = ssc.Result;

                        returnValue = convertToTable(answer, true);

                        ssh.Disconnect();
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public MethodResult ServiceManagement( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>(); DataTable returnValue = new DataTable();
            //Command, Input,Output,timeout
            string Command = string.Empty; string Input = string.Empty; string Output = string.Empty; string Timeout = string.Empty;
            try
            {

                Command = stp.getPropertiesValue(StringHelper.command, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                Timeout = stp.getPropertiesValue(StringHelper.timeout, step);

                DataTable connect = new TypeHandler().getInput(Input, StringHelper.variable, executionID);
                if (connect != null)
                {
                    string URL = connect.Rows[0].Field<string>("URL");
                    string Username = connect.Rows[0].Field<string>("Username");
                    string Password = connect.Rows[0].Field<string>("Password");
                    string Port = connect.Rows[0].Field<string>("Port");
                    string Protocol = connect.Rows[0].Field<string>("Protocol");
                    string OS = connect.Rows[0].Field<string>("OS");
                    using (var ssh = new SshClient(new MethodResultHandler().CreateConnection(URL.Trim(), int.Parse(Port.Trim()), Username.Trim(), Password.Trim())))
                    {
                        string command = string.Empty, answer = string.Empty; SshCommand cmd = null; string noanswer = string.Empty;

                        command = Command;

                        ssh.Connect();
                        if (command.Trim() != string.Empty)
                        {
                            cmd = ssh.CreateCommand(command);
                            var result = cmd.BeginExecute();

                            using (var reader = new StreamReader(cmd.OutputStream, Encoding.UTF8, true, 1024, true))
                            {
                                while (!result.IsCompleted || !reader.EndOfStream)
                                {
                                    string line = reader.ReadLine();
                                    if (line != null)
                                    {
                                        answer += line + "\n";
                                    }
                                }
                            }

                            cmd.EndExecute(result);
                            if (string.IsNullOrEmpty(answer.Trim()))
                            {
                                answer = StringHelper.nooutput;
                            }

                        }
                        else
                        {
                            answer = StringHelper.nocommand;
                        }

                        returnValue.Columns.Add(step.Name);
                        returnValue.Rows.Add(new string[] { answer });

                        ssh.Disconnect();
                        ret = StringHelper.success;

                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Sudo( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string Command = string.Empty; string Input = string.Empty; string Output = string.Empty;
            string Rootusername = string.Empty; string Rootpassword = string.Empty; string Rootcommand = string.Empty;
            DataTable returnValue = new DataTable();
            try
            {
                Command = stp.getPropertiesValue(StringHelper.command, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                Rootusername = stp.getPropertiesValue(StringHelper.rootusername, step);
                Rootpassword = stp.getPropertiesValue(StringHelper.rootpassword, step);
                Rootcommand = stp.getPropertiesValue(StringHelper.rootcommand, step);

                DataTable connect = new TypeHandler().getInput(Input, StringHelper.variable, executionID);
                if (connect != null)
                {
                    string URL = connect.Rows[0].Field<string>("URL");
                    string Username = connect.Rows[0].Field<string>("Username");
                    string Password = connect.Rows[0].Field<string>("Password");
                    string Port = connect.Rows[0].Field<string>("Port");
                    string Protocol = connect.Rows[0].Field<string>("Protocol");
                    string OS = connect.Rows[0].Field<string>("OS");
                    using (var ssh = new SshClient(new MethodResultHandler().CreateConnection(URL.Trim(), int.Parse(Port.Trim()), Username.Trim(), Password.Trim())))
                    {
                        string cmd = string.Empty, answer = string.Empty; string noanswer = string.Empty;
                        cmd = Command;

                        ssh.Connect();
                        if (cmd.Trim() != string.Empty)
                        {
                            if (cmd.IndexOf("su") > -1 || cmd.IndexOf(StringHelper.start) > -1)
                            {
                                answer = ExpectSSH(URL.Trim(), Username.Trim(), Password.Trim(), cmd, Rootusername, Rootpassword, Rootcommand);
                                answer = answer.Replace("[", "\n["); answer = answer.Replace(";", ";\n");
                            }
                        }
                        else
                        {
                            answer = StringHelper.nocommand;
                        }

                        returnValue.Columns.Add(step.Name);
                        returnValue.Rows.Add(new string[] { answer });

                        ssh.Disconnect();
                        ret = StringHelper.success;
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Patch( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string Command = string.Empty; string Input = string.Empty; string Repopassword = string.Empty; string Repousername = string.Empty;
            string Rootusername = string.Empty; string Rootpassword = string.Empty; string RepoURL = string.Empty; string Timeout = string.Empty;
            string Untar2directory = string.Empty; string Iusername = string.Empty; string Iusergroup = string.Empty; string Iservicename = string.Empty; string islocaltar = string.Empty; string LocalTarAddress = string.Empty;
            DataTable returnValue = new DataTable(); string output = string.Empty;
            try
            {
                RepoURL = stp.getPropertiesValue(StringHelper.repourl, step);
                Repousername = stp.getPropertiesValue(StringHelper.repousername, step);
                Repopassword = stp.getPropertiesValue(StringHelper.repopassword, step);
                Timeout = stp.getPropertiesValue(StringHelper.timeout, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Rootusername = stp.getPropertiesValue(StringHelper.rootusername, step);
                Rootpassword = stp.getPropertiesValue(StringHelper.rootpassword, step);
                Untar2directory = stp.getPropertiesValue(StringHelper.untartodirectory, step);
                Iusername = stp.getPropertiesValue(StringHelper.installedusername, step);
                Iusergroup = stp.getPropertiesValue(StringHelper.installedusergroup, step);
                Iservicename = stp.getPropertiesValue(StringHelper.installedservicename, step);
                islocaltar = stp.getPropertiesValue(StringHelper.islocaltar, step);
                LocalTarAddress = stp.getPropertiesValue(StringHelper.localtaraddress, step);


                DataTable connect = new TypeHandler().getInput(Input, StringHelper.variable, executionID);
                if (connect != null)
                {
                    string URL = connect.Rows[0].Field<string>("URL");
                    string Username = connect.Rows[0].Field<string>("Username");
                    string Password = connect.Rows[0].Field<string>("Password");
                    string Port = connect.Rows[0].Field<string>("Port");
                    string Protocol = connect.Rows[0].Field<string>("Protocol");
                    string OS = connect.Rows[0].Field<string>("OS");
                    SshClient sshClient = new SshClient(new MethodResultHandler().CreateConnection(URL.Trim(), int.Parse(Port.Trim()), Username.Trim(), Password.Trim()));

                    sshClient.Connect();
                    IDictionary<Renci.SshNet.Common.TerminalModes, uint> termkvp = new Dictionary<Renci.SshNet.Common.TerminalModes, uint>();
                    termkvp.Add(Renci.SshNet.Common.TerminalModes.ECHO, 53);
                    ShellStream shellStream = sshClient.CreateShellStream(StringHelper.customcommand, 80, 24, 800, 600, 1024);
                    string mat = RepoURL;
                    if (RepoURL.IndexOf(StringHelper.wget) > -1)
                    {
                        mat = mat.Replace(StringHelper.wget, "");
                        mat = mat.Replace("https://", "");
                        mat = mat.Substring(mat.LastIndexOf('/') + 1);
                        var wget = sendCommand(RepoURL, shellStream);
                        output += wget.ToString();

                        mat = "yum install -y " + mat;
                        wget = sendCommand(mat, shellStream);
                        output += wget.ToString();

                    }
                    else
                    {
                        if (RepoURL.Trim() != string.Empty)
                        {
                            var wget = sendCommand(RepoURL, shellStream);
                            output += wget.ToString();
                        }
                    }
                    returnValue.Columns.Add(step.Name);
                    returnValue.Rows.Add(new string[] { output });


                    sshClient.Disconnect();
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public string ExpectSSH(string address, string login, string password, string command, string rootusername, string rootpassword, string rootcommand)
        {
            string output = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                SshClient sshClient = new SshClient(address, 22, login, password);

                sshClient.Connect();
                IDictionary<Renci.SshNet.Common.TerminalModes, uint> termkvp = new Dictionary<Renci.SshNet.Common.TerminalModes, uint>();
                termkvp.Add(Renci.SshNet.Common.TerminalModes.ECHO, 53);

                ShellStream shellStream = sshClient.CreateShellStream(StringHelper.xterm, 80, 24, 800, 600, 1024, termkvp);
                //Get logged in
                string rep = shellStream.Expect(new Regex(@"[$>]")); //expect user prompt
                output += rep;

                //send command
                shellStream.WriteLine(command);
                rep = shellStream.Expect(new Regex(@"([$#>:])")); //expect password or user prompt
                output += rep;

                //check to send password
                if (rep.Contains(":"))
                {
                    //send password
                    shellStream.WriteLine(rootpassword);
                    rep = shellStream.Expect(new Regex(@"[$#>:]")); //expect user or root prompt
                    output += rep;
                }
                if (rep.Contains("#"))
                {
                    shellStream.WriteLine(rootcommand);
                    rep = shellStream.Expect(new Regex(@"[$#>]"));
                    output += rep;
                }

                sshClient.Disconnect();
            }
            //try to open connection
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                output = ex.Message;
            }
            return output;
        }

        public string ExpectSSHnew(string address, string login, string password, string command, string rootusername, string rootpassword, string rootcommand)
        {
            string output = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                SshClient sshClient = new SshClient(address, 22, rootusername, rootpassword);

                sshClient.Connect();
                IDictionary<Renci.SshNet.Common.TerminalModes, uint> termkvp = new Dictionary<Renci.SshNet.Common.TerminalModes, uint>();
                termkvp.Add(Renci.SshNet.Common.TerminalModes.ECHO, 53);

                ShellStream shellStream = sshClient.CreateShellStream(StringHelper.xterm, 80, 24, 800, 600, 1024, termkvp);
                //Get logged in
                string rep = string.Empty; //
                //send command
                shellStream.WriteLine(command);
                rep = shellStream.Expect(new Regex(@"([$#>:])")); //expect password or user prompt
                output += rep;

                //check to send password
                if (rep.Contains(":"))
                {
                    //send password
                    shellStream.WriteLine(password);
                    rep = shellStream.Expect(new Regex(@"[$#>]"));

                    //expect user or root prompt
                    shellStream.WriteLine(password);
                    rep = shellStream.Expect(new Regex(@"[$#>]"));
                    output += rep;
                }
                sshClient.Disconnect();
            }
            //try to open connection
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                output = ex.Message;
            }
            return output;
        }

        public ShellStream ExecuteCommand(ConnectionInfo ci, string command)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                SshClient sshclient = new SshClient(ci);
                sshclient.Connect();
                ShellStream stream = sshclient.CreateShellStream(command, 80, 24, 800, 600, 1024);
                return stream;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return null;
            }
        }

        public StringBuilder sendCommand(string customCMD, ShellStream stream)
        {
            try
            {
                StringBuilder answer;

                var reader = new StreamReader(stream);
                var writer = new StreamWriter(stream);
                writer.AutoFlush = true;
                WriteStream(customCMD, writer, stream);
                answer = ReadStream(reader);
                return answer;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return null;
            }
        }

        private void WriteStream(string cmd, StreamWriter writer, ShellStream stream)
        {
            try
            {
                writer.WriteLine(cmd);

                while (stream.Length == 0)
                {
                    Thread.Sleep(1500);
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
            }
        }

        private StringBuilder ReadStream(StreamReader reader)
        {
            try
            {
                StringBuilder result = new StringBuilder();

                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    result.AppendLine(line);
                    Thread.Sleep(500);
                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return null;
            }
        }
        private DataTable convertToTable(string text, bool withheader)
        {
            List<string> rows = new List<string>();
            List<string> HeaderRow = new List<string>();
            List<string> HeaderRow1 = new List<string>();
            List<string> HeaderRow2 = new List<string>();

            DVariables value = new DVariables();
            text = text.TrimStart();
            //rows
            if (text.IndexOf("\n") > -1)
            {
                rows = text.Split(new string[] { "\n" }, StringSplitOptions.None).ToList();
            }
            //HeaderRow
            if (rows[0].IndexOf(" ") > -1)
            {
                HeaderRow = rows[0].Split(new string[] { " ", "  ", "   ", "  " }, StringSplitOptions.None).ToList();
                HeaderRow.ForEach(x => x.Trim()); HeaderRow.RemoveAll(x => x == string.Empty);

                if (rows.Count > 1)
                {
                    HeaderRow1 = rows[1].Split(new string[] { " ", "  ", "   ", "  " }, StringSplitOptions.None).ToList();
                    HeaderRow2 = rows[2].Split(new string[] { " ", "  ", "   ", "  " }, StringSplitOptions.None).ToList();
                    HeaderRow1.ForEach(x => x.Trim()); HeaderRow1.RemoveAll(x => x == string.Empty);
                    HeaderRow2.ForEach(x => x.Trim()); HeaderRow2.RemoveAll(x => x == string.Empty);
                }

            }
            //HeaderRow.ForEach(x => x.Trim()); HeaderRow.RemoveAll(x => x == string.Empty);


            DataTable dt = new DataTable();

            dt.Columns.Add(" "); //db id
            dt.Columns.Add("  "); //db runid

            if (HeaderRow.Count >= HeaderRow1.Count && HeaderRow1.Count == HeaderRow2.Count)
            {
                foreach (string head in HeaderRow)
                {
                    dt.Columns.Add(head);
                }
                foreach (string row in rows)
                {
                    List<string> rower = new List<string>();
                    rower = row.Split(new string[] { " ", "  ", "   ", "  " }, StringSplitOptions.None).ToList();
                    rower.ForEach(x => x.Trim()); rower.RemoveAll(x => x == string.Empty);
                    rower.Insert(0, "");
                    rower.Insert(1, "");
                    dt.Rows.Add(rower.ToArray());
                }
            }
            else
            {
                dt.Columns.Add("Description");
                foreach (string row in rows)
                {
                    List<string> rower = new List<string>();
                    //rower = row.Split(new string[] { " ", "  ", "   ", "  " }, StringSplitOptions.None).ToList();
                    //rower.ForEach(x => x.Trim()); rower.RemoveAll(x => x == string.Empty);
                    rower.Insert(0, "");
                    rower.Insert(1, "");
                    rower.Insert(2, row);
                    dt.Rows.Add(rower.ToArray());
                }
            }

            if (withheader == false)
            {
                dt.Rows[0].Delete();
            }
            return dt;
        }
    }
}