﻿using Genbase.classes;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Net.Pop3;
using MailKit.Search;
using MailKit.Security;
using MimeKit;
using System;
using System.Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.IO.Compression;
//using EASendMail;
using Newtonsoft.Json;
using System.Text;
using System.Reflection;
using Newtonsoft.Json.Linq;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Net;
using Genbase.Classes;
using EASendMail;

namespace Genbase.BLL.ElementsClasses
{
    public class Email
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        ResultStepDebug rdebug = new ResultStepDebug();
        VariableHandler vh = new VariableHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult EmailLogin(Steps step, string executionID)
        {
            string Input = string.Empty; string username = string.Empty; string password = string.Empty; string mDomain = string.Empty;
            string InboundMailserver = string.Empty; string InboundPortNumber = string.Empty; string InboundEncryption = string.Empty;
            string OutboundMailserver = string.Empty; string OutboundPortNumber = string.Empty; string OutboundEncryption = string.Empty;
            string InboundInstance = string.Empty;
            string OutboundInstance = string.Empty;
            DataTable inbounddtr = new DataTable();
            DataTable outbounddtr = new DataTable();
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<string> input = new List<string>();
                Variables.username = stp.getPropertiesValue(StringHelper.username, step);
                Variables.password = stp.getPropertiesValue(StringHelper.password, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                if (Input.Trim() != string.Empty && vh.checkVariableExists(Input, executionID))
                {

                }
                username = stp.getPropertiesValue(StringHelper.username, step);
                password = stp.getPropertiesValue(StringHelper.password, step);
                InboundMailserver = stp.getPropertiesValue(StringHelper.InboundMailserver, step);
                InboundPortNumber = stp.getPropertiesValue(StringHelper.InboundPortNumber, step);
                InboundEncryption = stp.getPropertiesValue(StringHelper.InboundEncryption, step);
                OutboundMailserver = stp.getPropertiesValue(StringHelper.OutboundMailserver, step);
                OutboundPortNumber = stp.getPropertiesValue(StringHelper.OutboundPortNumber, step);
                OutboundEncryption = stp.getPropertiesValue(StringHelper.OutboundEncryption, step);
                InboundInstance = stp.getPropertiesValue(StringHelper.InboundInstance, step);
                OutboundInstance = stp.getPropertiesValue(StringHelper.OutboundInstance, step);
                if (Variables.vDebugger.ID == step.Id)
                {

                    rdebug.StepName = step.Name;
                    input.Add(StringHelper.mailusername + Variables.username.ToString());
                    input.Add(StringHelper.mailserver + Variables.mailserver);
                    input.Add(StringHelper.mailport + Variables.port);
                    rdebug.Input = input;
                }

                log.add2Log(Messages.stepExecutionStarted, step.Robot_Id, step.Name, StringHelper.eventcode, false);
                log.add2Log(Messages.loggedin.Replace("---", Variables.username), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                using (var client = new ImapClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(InboundMailserver, Int32.Parse(InboundPortNumber), SecureSocketOptions.Auto);
                    client.Authenticate(username, password);

                    inbounddtr.Columns.Add("MailId");
                    inbounddtr.Columns.Add("Password");
                    inbounddtr.Columns.Add("InboundMailServer");
                    inbounddtr.Columns.Add("InboundPort");
                    inbounddtr.Columns.Add("InboundEncrytion");

                    inbounddtr.Rows.Add(username, password, InboundMailserver, InboundPortNumber, InboundEncryption);

                    outbounddtr.Columns.Add("MailId");
                    outbounddtr.Columns.Add("Password");
                    outbounddtr.Columns.Add("OutboundMailServer");
                    outbounddtr.Columns.Add("OutboundPort");
                    outbounddtr.Columns.Add("OutboundEncrytion");

                    outbounddtr.Rows.Add(username, password, OutboundMailserver, OutboundPortNumber, OutboundEncryption);

                }
                ret = StringHelper.success;
                string classname = this.GetType().Name;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                vh.CreateVariable(InboundInstance, JsonConvert.SerializeObject(inbounddtr), "", false, "datatable", step.Robot_Id, executionID);
                vh.CreateVariable(OutboundInstance, JsonConvert.SerializeObject(outbounddtr), "", false, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            List<string> output = new List<string>();

            output.Add(StringHelper.outputt + mr.result.ToString());
            rdebug.Output = output;
            Variables.vDebugger.Description = JsonConvert.SerializeObject(rdebug);
            return mr;
        }

        public MethodResult ReadEmail(Steps step, string executionID)
        {
            string ret = string.Empty; DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<UniqueId> mUIDs = new List<UniqueId>();
            string readtype = string.Empty;
            string mailscount = string.Empty; string output = string.Empty;
            string search = string.Empty; string folder = string.Empty; string parameter = string.Empty; string protocol = string.Empty;
            string Inbound = string.Empty; string Attachment = string.Empty; string folderPath = string.Empty; string attachmentname = string.Empty;

            DataTable dtat = new DataTable();
            dtat.Columns.Add("Files");

            DataTable mailtable = new DataTable();

            mailtable.Columns.Add("MessageID");
            mailtable.Columns.Add("Subject");
            mailtable.Columns.Add("Body");
            mailtable.Columns.Add("BCC");
            mailtable.Columns.Add("CC");
            mailtable.Columns.Add("Date");
            mailtable.Columns.Add("From");
            mailtable.Columns.Add("To");
            mailtable.Columns.Add("HTMLBody");
            mailtable.Columns.Add("Importance");
            mailtable.Columns.Add("Priority");
            mailtable.Columns.Add("ReplyTo");
            mailtable.Columns.Add("TextMessage");
            mailtable.Columns.Add("Sender");
            mailtable.Columns.Add("ResentBCC");
            mailtable.Columns.Add("ResentCC");
            mailtable.Columns.Add("ResentDate");
            mailtable.Columns.Add("ResentFrom");
            mailtable.Columns.Add("ResentMessageID");
            mailtable.Columns.Add("ResentReplyTo");
            mailtable.Columns.Add("ResentSender");
            mailtable.Columns.Add("ResentTo");
            mailtable.Columns.Add("XPriority");
            mailtable.Columns.Add("Domain");
            mailtable.Columns.Add("Attachment");
            mailtable.Columns.Add("uid");
            mailtable.Columns.Add("Attachments");
            mailtable.Columns.Add("References");

            List<MimeMessage> mimes = new List<MimeMessage>();
            try
            {
                List<string> input = new List<string>();
                string Mainpath = string.Empty;
                output = stp.getPropertiesValue(StringHelper.output, step);
                readtype = stp.getPropertiesValue(StringHelper.readtype, step);
                mailscount = stp.getPropertiesValue(StringHelper.count, step);
                search = stp.getPropertiesValue(StringHelper.search, step);
                folder = stp.getPropertiesValue(StringHelper.folder, step);
                parameter = stp.getPropertiesValue(StringHelper.searchparameter, step);
                protocol = stp.getPropertiesValue(StringHelper.protocol, step);
                Inbound = stp.getPropertiesValue(StringHelper.instance, step);
                Attachment = stp.getPropertiesValue(StringHelper.attachment, step);

                if (Inbound.Trim() != string.Empty && vh.checkVariableExists(Inbound, executionID))
                {
                    System.Data.DataTable dt = new TypeHandler().getInput(Inbound, "Variable", executionID);
                    using (var client = new ImapClient())
                    {
                        try
                        {
                            string uname = string.Empty; string passw = string.Empty; string imape = string.Empty; string port = string.Empty; string enc = string.Empty;

                            uname = dt.Rows[0]["MailId"].ToString();
                            passw = dt.Rows[0]["Password"].ToString();
                            imape = dt.Rows[0]["InboundMailServer"].ToString();
                            port = dt.Rows[0]["InboundPort"].ToString();
                            enc = dt.Rows[0]["InboundEncrytion"].ToString();

                            client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                            client.Connect(imape, Int32.Parse(port), SecureSocketOptions.Auto);
                            client.Authenticate(uname, passw);

                            if ((client.Capabilities & (ImapCapabilities.SpecialUse | ImapCapabilities.XList)) != 0)
                            {
                                IList<UniqueId> uids = null;
                                List<string> mails = new List<string>();

                                if (folder.ToLower() == StringHelper.inbox.ToLower())
                                {
                                    try
                                    {
                                        client.Inbox.Open(FolderAccess.ReadWrite);

                                        uids = getMailUniqueIDs(readtype, client.Inbox);

                                        int y = uids.Count();
                                        log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                        List<string> lskey = new List<string>();
                                        int count = 1, maincount = 0;
                                        if (mailscount != string.Empty)
                                        {
                                            if (int.TryParse(mailscount.Trim(), out maincount))
                                            {
                                                maincount = int.Parse(mailscount);
                                            }
                                            if (maincount == 0)
                                            {
                                                maincount = uids.Count;
                                            }
                                        }
                                        else
                                        {
                                            maincount = uids.Count;
                                        }

                                        foreach (UniqueId uid in uids)
                                        {
                                            if (count > maincount)
                                            {
                                                break;
                                            }
                                            MimeMessage message = client.Inbox.GetMessage(uid);
                                            string messagepart = string.Empty;
                                            if (message.BodyParts.OfType<TextPart>() != null)
                                            {
                                                messagepart += message.BodyParts.OfType<TextPart>().FirstOrDefault().Text;
                                            }
                                            if (message.Subject != null)
                                            {
                                                messagepart += message.Subject.ToString();
                                            }

                                            if (search.Trim() != string.Empty)
                                            {
                                                List<string> keys = new List<string>();
                                                if (search.IndexOf(',') > -1)
                                                {
                                                    string[] searchkeys = search.Split(',');
                                                    keys = searchkeys.ToList();
                                                }
                                                else
                                                {
                                                    keys.Add(search);
                                                }

                                                if (FindinText(messagepart, keys) && messagepart != string.Empty)
                                                {
                                                    mUIDs.Add(uid);
                                                    mimes.Add(message);
                                                    mails.Add(messagepart);
                                                    client.Inbox.AddFlags(uid, MessageFlags.Seen, true);
                                                    count++;
                                                }
                                            }
                                            else
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                                client.Inbox.AddFlags(uid, MessageFlags.Seen, true);
                                                count++;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        string exe = ex.Message;
                                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                    }
                                }
                                else if (folder.ToLower() == StringHelper.archive)
                                {
                                    try
                                    {
                                        client.GetFolder(SpecialFolder.Archive).Open(FolderAccess.ReadWrite);

                                        uids = getMailUniqueIDs(readtype, client.GetFolder(SpecialFolder.Archive));

                                        foreach (UniqueId uid in uids)
                                        {
                                            int y = uids.Count();
                                            log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                            MimeMessage message = client.GetFolder(SpecialFolder.Archive).GetMessage(uid);
                                            string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                                            if (search.Trim() != string.Empty && messagepart.IndexOf(search) > -1)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                            }
                                            else if (search.Trim() == string.Empty)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        new Logger().LogException(ex);
                                        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                    }
                                }
                                else if (folder.ToLower() == StringHelper.all)
                                {
                                    try
                                    {
                                        client.GetFolder(SpecialFolder.All).Open(FolderAccess.ReadWrite);

                                        uids = getMailUniqueIDs(readtype, client.GetFolder(SpecialFolder.All));

                                        foreach (UniqueId uid in uids)
                                        {
                                            int y = uids.Count();
                                            log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                            MimeMessage message = client.GetFolder(SpecialFolder.All).GetMessage(uid);
                                            string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                                            if (search.Trim() != string.Empty && messagepart.IndexOf(search) > -1)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                            }
                                            else if (search.Trim() == string.Empty)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        string exe = ex.Message;
                                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                    }
                                }
                                else if (folder.ToLower() == StringHelper.drafts)
                                {
                                    try
                                    {
                                        client.GetFolder(SpecialFolder.Drafts).Open(FolderAccess.ReadWrite);

                                        uids = getMailUniqueIDs(readtype, client.GetFolder(SpecialFolder.Drafts));

                                        foreach (UniqueId uid in uids)
                                        {
                                            int y = uids.Count();
                                            log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                            MimeMessage message = client.GetFolder(SpecialFolder.Drafts).GetMessage(uid);
                                            string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                                            if (search.Trim() != string.Empty && messagepart.IndexOf(search) > -1)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                            }
                                            else if (search.Trim() == string.Empty)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        string exe = ex.Message;
                                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                    }
                                }
                                else if (folder.ToLower() == StringHelper.junk)
                                {
                                    try
                                    {
                                        client.GetFolder(SpecialFolder.Junk).Open(FolderAccess.ReadWrite);

                                        uids = getMailUniqueIDs(readtype, client.GetFolder(SpecialFolder.Junk));

                                        foreach (UniqueId uid in uids)
                                        {
                                            int y = uids.Count();
                                            log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                            MimeMessage message = client.GetFolder(SpecialFolder.Junk).GetMessage(uid);
                                            string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                                            if (search.Trim() != string.Empty && messagepart.IndexOf(search) > -1)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                            }
                                            else if (search.Trim() == string.Empty)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        string exe = ex.Message;
                                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                    }
                                }
                                else if (folder.ToLower() == StringHelper.sent)
                                {
                                    try
                                    {
                                        client.GetFolder(SpecialFolder.Sent).Open(FolderAccess.ReadWrite);

                                        uids = getMailUniqueIDs(readtype, client.GetFolder(SpecialFolder.Sent));

                                        foreach (UniqueId uid in uids)
                                        {
                                            int y = uids.Count();
                                            log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                            MimeMessage message = client.GetFolder(SpecialFolder.Sent).GetMessage(uid);
                                            string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                                            if (search.Trim() != string.Empty && messagepart.IndexOf(search) > -1)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                            }
                                            else if (search.Trim() == string.Empty)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        string exe = ex.Message;
                                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                    }
                                }
                                else if (folder.ToLower() == StringHelper.trash)
                                {
                                    try
                                    {
                                        client.GetFolder(SpecialFolder.Trash).Open(FolderAccess.ReadWrite);

                                        uids = getMailUniqueIDs(readtype, client.GetFolder(SpecialFolder.Trash));

                                        foreach (UniqueId uid in uids)
                                        {
                                            int y = uids.Count();
                                            log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                            MimeMessage message = client.GetFolder(SpecialFolder.Trash).GetMessage(uid);
                                            string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                                            if (search.Trim() != string.Empty && messagepart.IndexOf(search) > -1)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                            }
                                            else if (search.Trim() == string.Empty)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        string exe = ex.Message;
                                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                    }
                                }
                                else if (folder.ToLower() == StringHelper.flagged)
                                {
                                    try
                                    {
                                        client.GetFolder(SpecialFolder.Flagged).Open(FolderAccess.ReadWrite);

                                        uids = getMailUniqueIDs(readtype, client.GetFolder(SpecialFolder.Flagged));

                                        foreach (UniqueId uid in uids)
                                        {
                                            int y = uids.Count();
                                            log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                            MimeMessage message = client.GetFolder(SpecialFolder.Flagged).GetMessage(uid);
                                            string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                                            if (search.Trim() != string.Empty && messagepart.IndexOf(search) > -1)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                            }
                                            else if (search.Trim() == string.Empty)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        string exe = ex.Message;
                                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                    }
                                }
                            }
                            else
                            {
                                var xpecialFolders = client.GetFolder(client.PersonalNamespaces[0]).GetSubfolders(false).ToList();
                                var xpecialFolder = client.GetFolder(client.PersonalNamespaces[0]).GetSubfolders(false).ToList().Find(xp => xp.Name.ToLower() == folder.ToLower());
                                xpecialFolder.Open(FolderAccess.ReadWrite);

                                IList<UniqueId> uids = null;
                                List<string> mails = new List<string>();

                                if (folder.ToLower() == StringHelper.inbox.ToLower())
                                {
                                    uids = getMailUniqueIDs(readtype, xpecialFolder);

                                    int y = uids.Count();
                                    log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                    List<string> lskey = new List<string>();
                                    int count = 1, maincount = 0;
                                    if (mailscount != string.Empty)
                                    {
                                        if (int.TryParse(mailscount.Trim(), out maincount))
                                        {
                                            maincount = int.Parse(mailscount);
                                        }
                                        if (maincount == 0)
                                        {
                                            maincount = uids.Count;
                                        }
                                    }
                                    else
                                    {
                                        maincount = uids.Count;
                                    }

                                    foreach (UniqueId uid in uids)
                                    {
                                        if (count > maincount)
                                        {
                                            break;
                                        }
                                        MimeMessage message = xpecialFolder.GetMessage(uid);
                                        string messagepart = string.Empty;
                                        if (message.BodyParts.OfType<TextPart>() != null)
                                        {
                                            messagepart += message.BodyParts.OfType<TextPart>().FirstOrDefault().Text;
                                        }

                                        if (message.Subject != null)
                                        {
                                            messagepart += message.Subject.ToString();
                                        }
                                        if (search.Trim() != string.Empty)
                                        {
                                            List<string> keys = new List<string>();
                                            if (search.IndexOf(',') > -1)
                                            {
                                                string[] searchkeys = search.Split(',');
                                                keys = searchkeys.ToList();
                                            }
                                            else
                                            {
                                                keys.Add(search);
                                            }

                                            if (FindinText(messagepart, keys) && messagepart != string.Empty)
                                            {
                                                mUIDs.Add(uid);
                                                mimes.Add(message);
                                                mails.Add(messagepart);
                                                client.Inbox.AddFlags(uid, MessageFlags.Seen, true);
                                                count++;
                                            }
                                        }
                                        else
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                            client.Inbox.AddFlags(uid, MessageFlags.Seen, true);
                                            count++;
                                        }
                                    }
                                }
                                else if (folder.ToLower() == StringHelper.archive)
                                {
                                    uids = getMailUniqueIDs(readtype, xpecialFolder);

                                    foreach (UniqueId uid in uids)
                                    {
                                        int y = uids.Count();
                                        log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                        MimeMessage message = xpecialFolder.GetMessage(uid);
                                        string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                                        if (search.Trim() != string.Empty && messagepart.IndexOf(search) > -1)
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                        }
                                        else if (search.Trim() == string.Empty)
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                        }
                                    }
                                }
                                else if (folder.ToLower() == StringHelper.all)
                                {
                                    uids = getMailUniqueIDs(readtype, xpecialFolder);

                                    foreach (UniqueId uid in uids)
                                    {
                                        int y = uids.Count();
                                        log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                        MimeMessage message = xpecialFolder.GetMessage(uid);
                                        string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                                        if (search.Trim() != string.Empty && messagepart.IndexOf(search) > -1)
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                        }
                                        else if (search.Trim() == string.Empty)
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                        }
                                    }
                                }
                                else if (folder.ToLower() == StringHelper.drafts)
                                {
                                    uids = getMailUniqueIDs(readtype, xpecialFolder);

                                    foreach (UniqueId uid in uids)
                                    {
                                        int y = uids.Count();
                                        log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                        MimeMessage message = xpecialFolder.GetMessage(uid);
                                        string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                                        if (search.Trim() != string.Empty && messagepart.IndexOf(search) > -1)
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                        }
                                        else if (search.Trim() == string.Empty)
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                        }
                                    }
                                }
                                else if (folder.ToLower() == StringHelper.junk)
                                {
                                    uids = getMailUniqueIDs(readtype, xpecialFolder);

                                    foreach (UniqueId uid in uids)
                                    {
                                        int y = uids.Count();
                                        log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                        MimeMessage message = xpecialFolder.GetMessage(uid);
                                        string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                                        if (search.Trim() != string.Empty && messagepart.IndexOf(search) > -1)
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                        }
                                        else if (search.Trim() == string.Empty)
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                        }
                                    }
                                }
                                else if (folder.ToLower() == StringHelper.sent)
                                {
                                    uids = getMailUniqueIDs(readtype, xpecialFolder);

                                    foreach (UniqueId uid in uids)
                                    {
                                        int y = uids.Count();
                                        log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                        MimeMessage message = xpecialFolder.GetMessage(uid);
                                        string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                                        if (search.Trim() != string.Empty && messagepart.IndexOf(search) > -1)
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                        }
                                        else if (search.Trim() == string.Empty)
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                        }
                                    }
                                }
                                else if (folder.ToLower() == StringHelper.trash)
                                {
                                    uids = getMailUniqueIDs(readtype, xpecialFolder);

                                    foreach (UniqueId uid in uids)
                                    {
                                        int y = uids.Count();
                                        log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                        MimeMessage message = xpecialFolder.GetMessage(uid);
                                        string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                                        if (search.Trim() != string.Empty && messagepart.IndexOf(search) > -1)
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                        }
                                        else if (search.Trim() == string.Empty)
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                        }
                                    }
                                }
                                else if (folder.ToLower() == StringHelper.flagged)
                                {

                                    uids = getMailUniqueIDs(readtype, xpecialFolder);

                                    foreach (UniqueId uid in uids)
                                    {
                                        int y = uids.Count();
                                        log.add2Log(step.Name + Messages.unreadmails.Replace(StringHelper.dashedlines, y.ToString()), step.Robot_Id, step.Name, StringHelper.eventcode, false);
                                        MimeMessage message = xpecialFolder.GetMessage(uid);
                                        string messagepart = message.BodyParts.OfType<TextPart>().FirstOrDefault().Text + message.Subject.ToString();
                                        if (search.Trim() != string.Empty && messagepart.IndexOf(search) > -1)
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                        }
                                        else if (search.Trim() == string.Empty)
                                        {
                                            mUIDs.Add(uid);
                                            mimes.Add(message);
                                            mails.Add(messagepart);
                                        }
                                    }
                                }
                            }

                            if (mimes.Count > 0)
                            {
                                foreach (var m in mimes)
                                {
                                    List<dynamic> mailk = new List<dynamic>();

                                    mailk.Add(m.MessageId);
                                    mailk.Add(m.Subject);
                                    mailk.Add(m.BodyParts.OfType<TextPart>().FirstOrDefault().Text);
                                    mailk.Add(string.Join(";", m.Bcc));
                                    mailk.Add(string.Join(";", m.Cc));
                                    mailk.Add(m.Date.ToString());
                                    mailk.Add(string.Join(";", m.From));
                                    mailk.Add(string.Join(";", m.To));

                                    string replacement = "";
                                    string htmlString = "";
                                    try
                                    {
                                        if (m.HtmlBody != null && m.HtmlBody.Trim() != string.Empty)
                                        {
                                            htmlString = m.HtmlBody;


                                            HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
                                            htmlDocument.LoadHtml(htmlString);

                                            var sb = new StringBuilder();
                                            IEnumerable<HtmlNode> nodes = htmlDocument.DocumentNode.Descendants().Where(n => n.NodeType == HtmlNodeType.Text && n.ParentNode.Name != "script" && n.ParentNode.Name != "style");
                                            foreach (HtmlNode node in nodes)
                                            {
                                                if (!node.HasChildNodes)
                                                {
                                                    string text = node.InnerText;
                                                    if (!string.IsNullOrEmpty(text))
                                                        sb.AppendLine(text.Trim());
                                                }
                                            }

                                            string us = sb.ToString().Trim();
                                            replacement = Regex.Replace(us, @"\t|\n|\r", "");
                                            replacement = Regex.Replace(replacement, "&nbsp;", " ");
                                        }
                                        else if (m.TextBody != null && m.TextBody != string.Empty)
                                        {
                                            htmlString = m.TextBody;
                                        }
                                        else
                                        {
                                            htmlString = m.BodyParts.OfType<TextPart>().FirstOrDefault().Text;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        new Logger().LogException(ex);
                                        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                    }
                                    string mDomain = string.Empty;
                                    if (m.From.ToString().Contains('"'))
                                    {
                                        mDomain = m.From.ToString().Substring(m.From.ToString().IndexOf('@') + 1, m.From.ToString().Length - (m.From.ToString().IndexOf('@') + 2));
                                    }
                                    else
                                    {
                                        mDomain = m.From.ToString().Substring(m.From.ToString().IndexOf('@') + 1, m.From.ToString().Length - (m.From.ToString().IndexOf('@') + 1));
                                    }
                                    mailk.Add(htmlString);
                                    mailk.Add(m.Importance.ToString());
                                    mailk.Add(m.Priority.ToString());
                                    mailk.Add(string.Join(";", m.ReplyTo));
                                    mailk.Add(replacement);
                                    var filelist = m.Attachments.ToList();
                                    folderPath = Path.Combine(Directory.GetCurrentDirectory(), "TempAttachments" + "\\");

                                    string mAttachment = string.Empty;
                                    if (filelist.Count > 0)
                                    {
                                        foreach (var attachment1 in m.Attachments)
                                        {
                                            var fileName = attachment1.ContentDisposition?.FileName ?? attachment1.ContentType.Name;
                                            mAttachment = mAttachment + fileName + ',';
                                            Mainpath = folderPath + fileName;
                                            if (!Directory.Exists(folderPath))
                                            {
                                                Directory.CreateDirectory(folderPath);
                                            }
                                            using (var stream = System.IO.File.Create(Mainpath))
                                            {
                                                if (attachment1 is MessagePart)
                                                {
                                                    var rfc822 = (MessagePart)attachment1;

                                                    rfc822.Message.WriteTo(stream);
                                                }
                                                else
                                                {
                                                    var part = (MimeKit.MimePart)attachment1;

                                                    part.Content.DecodeTo(stream);
                                                }
                                            }
                                        }
                                    }

                                    mailk.Add(string.Join(";", m.Sender));
                                    mailk.Add(string.Join(";", m.ResentBcc));
                                    mailk.Add(string.Join(";", m.ResentCc));
                                    mailk.Add(string.Join(";", m.ResentDate));
                                    mailk.Add(string.Join(";", m.ResentFrom));
                                    mailk.Add(string.Join(";", m.ResentMessageId));
                                    mailk.Add(string.Join(";", m.ResentReplyTo));
                                    mailk.Add(string.Join(";", m.ResentSender));
                                    mailk.Add(string.Join(";", m.ResentTo));
                                    mailk.Add(m.XPriority.ToString());
                                    mailk.Add(mDomain);
                                    mailk.Add(mAttachment);
                                    mailk.Add(mUIDs[0].ToString());
                                    mailk.Add(attachmentname);
                                    mailk.Add(string.Join(";", m.References));

                                    mailtable.Rows.Add(mailk.ToArray());

                                    mailk.Clear();
                                }
                            }

                            string mailsstring = JsonConvert.SerializeObject(mailtable);
                            vh.CreateVariable(output, mailsstring, StringHelper.truee, true, "datatable", step.Robot_Id, executionID);

                            DataTable dtc = new DataTable();
                            dtc.Columns.Add("Count");
                            dtc.Rows.Add(mimes.Count.ToString());
                            string countstring = JsonConvert.SerializeObject(dtc);
                            vh.CreateVariable(mailscount, countstring, StringHelper.truee, true, "datatable", step.Robot_Id, executionID);

                            if (Attachment.Trim() != string.Empty)
                            {
                                if (mimes.Count > 0)
                                {
                                    foreach (MimeMessage mail in mimes)
                                    {
                                        var attachments = new List<MimeKit.MimePart>();
                                        var multiparts = new List<Multipart>();

                                        Random rnd = new Random();
                                        string ifilename = string.Empty;
                                        using (var iter = new MimeIterator(mail))
                                        {
                                            // collect our list of attachments and their parent multiparts
                                            while (iter.MoveNext())
                                            {
                                                var multipart = iter.Parent as Multipart;
                                                var part = iter.Current as MimeKit.MimePart;

                                                if (multipart != null && part != null && part.IsAttachment)
                                                {
                                                    // keep track of each attachment's parent multipart
                                                    multiparts.Add(multipart);
                                                    attachments.Add(part);
                                                }
                                            }

                                            foreach (MimeEntity attachment in mail.Attachments)
                                            {
                                                var fileName = attachment.ContentDisposition?.FileName ?? attachment.ContentType.Name;

                                                int random = rnd.Next(1, 10000);
                                                fileName = fileName.Split('.')[0] + "." + fileName.Split('.')[1];

                                                fileName = Mainpath;
                                                using (var stream = System.IO.File.Create(fileName))
                                                {
                                                    if (attachment is MessagePart)
                                                    {
                                                        var rfc822 = (MessagePart)attachment;

                                                        rfc822.Message.WriteTo(stream);
                                                        Variables.attaches.Add(fileName);
                                                        dtat.Rows.Add(fileName);
                                                        ifilename = fileName;
                                                    }
                                                    else
                                                    {
                                                        var part = (MimeKit.MimePart)attachment;
                                                        part.Content.DecodeTo(stream);
                                                        dtat.Rows.Add(fileName);
                                                        Variables.attaches.Add(fileName);
                                                        ifilename = fileName;

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                vh.CreateVariable(Attachment, JsonConvert.SerializeObject(dtat), StringHelper.truee, true, "datatable", step.Robot_Id, executionID);
                            }
                            ret = StringHelper.success;
                        }
                        catch (Exception ex)
                        {
                            string y = ex.Message;
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                    }
                }
                else
                {

                }
                string classname = this.GetType().Name;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                string het = new TypeHandler().ConvertDataTableToString(mailtable);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, true);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }

            Variables.vDebugger.Description = JsonConvert.SerializeObject(rdebug);
            return mr;
        }


        public MethodResult MoveEmail(Steps step, string executionID)
        {
            MethodResult mr = new MethodResultHandler().createResultType(false, null, null, s);
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<string> foldernames = new List<string>();
            string output = string.Empty;
            output = stp.getPropertiesValue(StringHelper.output, step);
            string FolderTo = string.Empty;
            FolderTo = stp.getPropertiesValue(StringHelper.folderto, step);

            string MailUIDs = string.Empty;
            MailUIDs = stp.getPropertiesValue(StringHelper.uids, step);
            System.Data.DataTable UIDs = new TypeHandler().getInput(MailUIDs, executionID);

            UniqueId mUID = new UniqueId();
            if (FolderTo.Trim() != string.Empty)
            {
                string fid = FolderTo;
                if (fid.Contains("{") && fid.Contains("}"))
                {
                    System.Data.DataTable Folder_To = new TypeHandler().getInput(FolderTo, executionID);
                    FolderTo = "";
                    for (int im = 0; im < Folder_To.Rows.Count; im++)
                    {
                        FolderTo += Folder_To.Rows[im][0].ToString() + ";";
                    }
                }
            }
            string Inbound = string.Empty;
            Inbound = stp.getPropertiesValue(StringHelper.instance, step);

            if (Inbound.Trim() != string.Empty && vh.checkVariableExists(Inbound, executionID) && FolderTo != string.Empty && FolderTo != "")
            {
                System.Data.DataTable dt = new TypeHandler().getInput(Inbound, "Variable", executionID);
                using (var client = new ImapClient())
                {
                    try
                    {
                        string uname = string.Empty; string passw = string.Empty; string imape = string.Empty; string port = string.Empty; string enc = string.Empty;

                        uname = dt.Rows[0]["MailId"].ToString();
                        passw = dt.Rows[0]["Password"].ToString();
                        imape = dt.Rows[0]["InboundMailServer"].ToString();
                        port = dt.Rows[0]["InboundPort"].ToString();
                        enc = dt.Rows[0]["InboundEncrytion"].ToString();

                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                        client.Connect(imape, Int32.Parse(port), SecureSocketOptions.Auto);
                        client.Authenticate(uname, passw);

                        var folders = client.Inbox.GetSubfolders();
                        mUID = new UniqueId(uint.Parse(UIDs.Rows[0]["uid"].ToString()));
                        foldernames = FolderTo.Split(';').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                        var lastname = foldernames.Last().Trim();
                        if (foldernames.Count > 1)
                        {
                            foreach (var folder in foldernames)
                            {
                                var IFolder = client.Inbox.GetSubfolder(folder);
                                IFolder.Open(FolderAccess.ReadWrite);
                                client.Inbox.Open(FolderAccess.ReadWrite);
                                if (folder.Equals(lastname))
                                {
                                    client.Inbox.MoveTo(mUID, IFolder);
                                }
                                else
                                {
                                    client.Inbox.CopyTo(mUID, IFolder);
                                }
                                IFolder.Open(FolderAccess.ReadWrite);
                                IList<UniqueId> mid = IFolder.Search(SearchQuery.Recent);
                                IFolder.SetFlags(mid, MessageFlags.UserDefined, false);
                            }
                        }
                        else
                        {
                            var IFolder = client.Inbox.GetSubfolder(foldernames[0]);
                            IFolder.Open(FolderAccess.ReadWrite);
                            client.Inbox.Open(FolderAccess.ReadWrite);
                            client.Inbox.MoveTo(mUID, IFolder);
                            IFolder.Open(FolderAccess.ReadWrite);
                            IList<UniqueId> mid = IFolder.Search(SearchQuery.Recent);
                            IFolder.SetFlags(mid, MessageFlags.UserDefined, false);
                        }

                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                        mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);

                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

                    }
                }
                return mr;
            }
            else return mr;
        }
        public MethodResult SendEmail(Steps step, string executionID)
        {
            MethodResult mr = new MethodResultHandler().createResultType(false, null, null, s);
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string mailId = string.Empty; string subject = string.Empty; string destination = string.Empty; string isbodyhtml = string.Empty; string sendmultiplemails = string.Empty; string attachments = string.Empty; string Input = string.Empty; string Output = string.Empty; string MailMessage = string.Empty;
            string senderusername = string.Empty; string senderpassword = string.Empty; string smtpaddress = string.Empty; string portnumber = string.Empty; string template = string.Empty; string outbound = string.Empty; string outboundEncrypt = string.Empty;
            try
            {

                mailId = stp.getPropertiesValue(StringHelper.mailid, step);
                subject = stp.getPropertiesValue(StringHelper.subject, step);
                isbodyhtml = stp.getPropertiesValue(StringHelper.isbodyhtml, step);
                destination = stp.getPropertiesValue(StringHelper.destination, step);
                sendmultiplemails = stp.getPropertiesValue(StringHelper.sendmultiplemails, step);
                attachments = stp.getPropertiesValue(StringHelper.attachments, step);
                if(attachments.Trim() == string.Empty)
                {
                    attachments = stp.getPropertiesValue("variables", step);
                }
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                MailMessage = stp.getPropertiesValue(StringHelper.mailmessage, step);
                template = stp.getPropertiesValue(StringHelper.template, step);
                outbound = stp.getPropertiesValue(StringHelper.instance, step);
                UniqueId mUID = new UniqueId();
                int UIDflag = 0;
                System.Data.DataTable outbounddt = new TypeHandler().getInput(outbound, executionID);
                senderusername = outbounddt.Rows[0]["MailId"].ToString();
                senderpassword = outbounddt.Rows[0]["Password"].ToString();
                smtpaddress = outbounddt.Rows[0]["OutboundMailServer"].ToString();
                portnumber = outbounddt.Rows[0]["OutboundPort"].ToString();
                outboundEncrypt = outbounddt.Rows[0]["OutboundEncrytion"].ToString();
                List<string> attachs = new List<string>();
                DVariables value = new DVariables();
                List<string> mailIds = new List<string>();
                if (mailId.Trim() != string.Empty)
                {
                    string mid = mailId;
                    if (mid.Contains("{") && mid.Contains("}"))
                    {
                        System.Data.DataTable mailIddt = new TypeHandler().getInput(mailId, executionID);
                        mailId = "";
                        for (int im = 0; im < mailIddt.Rows.Count; im++)
                        {
                            mailId += mailIddt.Rows[im][0].ToString() + ";";
                        }
                    }
                    else
                    {
                        try
                        {
                            mailId = string.Join(";", JsonConvert.DeserializeObject<List<string>>(new TypeHandler().getItemfromArray(mailId, executionID)));
                        }
                        catch (Exception e)
                        {
                            string ec = e.Message;
                            if (!vh.checkVariableExists(mid, executionID))
                            {
                                mailId = mid;
                            }
                            else
                            {
                                string ms = string.Empty;
                                if (mid.Contains(";"))
                                {
                                    foreach (string m in mid.Split(';').ToList())
                                    {
                                        ms = ms + JsonConvert.DeserializeObject<DataTable>(vh.getVariables(m, executionID).vlvalue).Rows[0][0].ToString() + ";";
                                    }
                                }
                                else
                                {
                                    ms = JsonConvert.DeserializeObject<DataTable>(vh.getVariables(mid, executionID).vlvalue).Rows[0][0].ToString();
                                }
                                mailId = ms;
                            }
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                        }
                    }
                }
                string msg = string.Empty;
                string subj = subject;
                if (subject.Trim() != string.Empty)
                {
                    try
                    {
                        if (new VariableHandler().checkVariableExists(subject, executionID))
                        {
                            value = new VariableHandler().getVariables(subject, executionID);
                            System.Data.DataTable dt = new TypeHandler().getInput(subject, "Variable", executionID);
                            subject = String.Empty;
                            foreach (DataRow fr in dt.Rows)
                            {
                                subject += fr["Input" + subj].ToString();
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        string ec = e.Message;
                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                        subject = subj;
                    }
                }

                if (MailMessage.Trim() != string.Empty)
                {
                    try
                    {
                        string msg1 = MailMessage;
                        if (msg1.Contains("{") && msg1.Contains("}"))
                        {                                                    
                            String ret1=String.Empty;
                            List<string> names = new List<string>();                          
                            string folderPath = Path.Combine(Directory.GetCurrentDirectory(), "TempAttachments" + "\\");
                            string attachname = string.Empty;
                            System.Data.DataTable dt = new TypeHandler().getInput(MailMessage,executionID);
                            try
                            {
                                using (MailMessage mail = new MailMessage())
                                {
                                    mail.From = new System.Net.Mail.MailAddress(senderusername);
                                    mail.Subject = dt.Rows[0]["Subject"].ToString();
                                    if (dt.Rows[0]["HTMLBody"].ToString() != "")
                                    {
                                        mail.Body = dt.Rows[0]["HTMLBody"].ToString();
                                    }
                                    else
                                    {
                                        mail.Body = dt.Rows[0]["Body"].ToString();
                                    }
                                    mail.IsBodyHtml = true;
                                    attachname = dt.Rows[0]["Attachment"].ToString();

                                    names = attachname.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                                    foreach (var name in names)
                                    {
                                        mail.Attachments.Add(new System.Net.Mail.Attachment(folderPath + name));
                                    }
                                    using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(smtpaddress, Convert.ToInt32(portnumber)))
                                    {
                                        smtp.Credentials = new NetworkCredential(senderusername, senderpassword);
                                        smtp.EnableSsl = true;
                                        mailIds = mailId.Split(';').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                                        for(int i=0;i<mailIds.Count;i++)
                                        {
                                            mail.To.Add(mailIds[i]);
                                        }
                                        smtp.Send(mail);
                                        mail.Dispose();
                                        foreach (var name in names)
                                        {
                                            System.IO.File.Delete(folderPath + name);
                                        }
                                    }
                                }
                                ret1 = StringHelper.success;
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);

                            }
                            
                            if (ret1 == StringHelper.success)
                            {
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                                new ExecutionLog().add2Log("Forward mail has been Executed", step.Robot_Id, step.Name, StringHelper.eventcode, false);
                            }
                            else
                            {
                                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
                            }
                            return mr;
                        }
                        else
                        {
                            if (new VariableHandler().checkVariableExists(MailMessage, executionID))
                            {
                                value = new VariableHandler().getVariables(MailMessage, executionID);
                                System.Data.DataTable dt = new TypeHandler().getInput(MailMessage, "Variable", executionID);
                                foreach (DataRow fr in dt.Rows)
                                {
                                    msg += fr["Input" + MailMessage].ToString();
                                }
                            }
                            else
                            {
                                msg = MailMessage;
                            }
                        }
                   }
                    catch (Exception e)
                    {
                        string ec = e.Message;
                        try
                        {
                            System.Data.DataTable dt = new TypeHandler().getInput(MailMessage, "Variable", executionID);
                            foreach (DataRow fr in dt.Rows)
                            {
                                mUID = new UniqueId(uint.Parse(fr["uid"].ToString()));
                                UIDflag = 1;
                            }
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                        }
                        catch (Exception e2)
                        {
                            s = new StatusModel(StringHelper.exceptioncode, e2.Message, Method, StringHelper.falsee);
                            msg = MailMessage;
                        }
                    }
                }

                if (Input.Trim() != string.Empty)
                {
                    value = new VariableHandler().getVariables(Input, executionID);
                    System.Data.DataTable dt = new TypeHandler().getInput(Input, executionID);
                    try
                    {
                        if (dt.Columns.Contains("Files"))
                        {
                            List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                            // get the file attributes for file or directory
                            try
                            {
                                FileAttributes attr = System.IO.File.GetAttributes(@files[0]);
                                attachs = files;
                            }
                            catch (Exception ex)
                            {
                                string ec = ex.Message;
                                msg = MailMessage + files[0];
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                            }
                        }
                        else
                        {
                            msg = new TypeHandler().cDataTableToString(dt);
                        }
                    }
                    catch (Exception e)
                    {
                        string ec = e.Message;
                        msg = dt.Rows[0][0].ToString();
                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                    }
                }

                if (attachments.Trim() != string.Empty)
                {
                    value = new VariableHandler().getVariables(Input, executionID);
                    System.Data.DataTable dt = new TypeHandler().getInput(attachments, executionID);
                    try
                    {
                        List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                        try
                        {
                            FileAttributes attr = System.IO.File.GetAttributes(@files[0]);
                            attachs = files;
                        }
                        catch (Exception ex)
                        {
                            string ec = ex.Message;
                            msg = MailMessage + files[0];
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                    }
                    catch (Exception e)
                    {
                        string ec = e.Message;
                        msg = dt.Rows[0][0].ToString();
                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                    }
                }

                string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                string folder = "";

                string full_path = path + folder;
                List<string> MailIds = new List<string>();
                if (mailId.IndexOf(",") > -1 && sendmultiplemails.ToLower() == StringHelper.truee)
                {
                    MailIds = mailId.Split(',').ToList();
                    foreach (string MailId in MailIds)
                    {
                        try
                        {
                            if (template.Trim() == string.Empty)
                            {
                                sendMaile(subject, MailId, Input, msg, attachs, step, senderusername, senderpassword, smtpaddress, portnumber, executionID);
                            }
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                    }
                }
                else if (mailId.IndexOf(",") > -1 && sendmultiplemails.ToLower() == StringHelper.ffalse)
                {
                    MailIds = mailId.Split(',').ToList();
                    foreach (string MailId in MailIds)
                    {
                        try
                        {
                            if (template.Trim() == string.Empty)
                            {
                                sendMaile(subject, MailId, Input, msg, attachs, step, senderusername, senderpassword, smtpaddress, portnumber, executionID);
                            }
                            else
                            {

                            }
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                    }
                }
                else if (UIDflag == 1)
                {
                    MailIds = mailId.Split(';').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                    if (MailIds.Count > 1) sendMailbyMsgID(Output, MailIds, Input, senderusername, senderpassword, MailMessage, smtpaddress, portnumber, executionID);
                }
                else
                {
                    try
                    {
                        if (template.Trim() == "select an option" || template.Trim() == string.Empty)//string.Empty)
                        {
                            sendMaile(subject, mailId, Input, msg, attachs, step, senderusername, senderpassword, smtpaddress, portnumber, executionID);
                        }
                        else
                        {
                            if (Input.Trim() != string.Empty && template.Trim() == "select an option" || template.Trim() == string.Empty)
                            {
                                sendMaile(template, mailId, Input, msg, attachs, step, senderusername, senderpassword, smtpaddress, portnumber, executionID);
                            }
                            else
                            {
                                sendeTemplate(template, mailId, msg, attachs, step, senderusername, senderpassword, smtpaddress, portnumber, executionID);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                        new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            //MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log("Send mail has been Executed", step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        Mail cm = new Mail();

        public void sendMailbyMsgID(string mailFolder, List<string> MailIds, string Input, string senderusername, string senderpassword, string MailMessage, string smtpaddress, string portnumber, string executionID)
        {
            List<string> names = new List<string>();
            string folderpath = Path.Combine(Directory.GetCurrentDirectory() + "~/TempAttachments/");
            string attachname = string.Empty;
            System.Data.DataTable dt = new TypeHandler().getInput(MailMessage, "Variable", executionID);
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new System.Net.Mail.MailAddress(senderusername);
                    mail.Subject = dt.Rows[0]["Subject"].ToString();
                    if (dt.Rows[0]["HTMLBody"].ToString() != "")
                    {
                        mail.Body = dt.Rows[0]["HTMLBody"].ToString();
                    }
                    else
                    {
                        mail.Body = dt.Rows[0]["Body"].ToString();
                    }
                    mail.IsBodyHtml = true;
                    attachname = dt.Rows[0]["Attachment"].ToString();

                    names = attachname.Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                    foreach (var name in names)
                    {
                        mail.Attachments.Add(new System.Net.Mail.Attachment(folderpath + name));
                    }
                    using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(smtpaddress, Convert.ToInt32(portnumber)))
                    {
                        smtp.Credentials = new NetworkCredential(senderusername, senderpassword);
                        smtp.EnableSsl = true;
                        foreach (var MailId in MailIds)
                        {
                            mail.To.Add(MailId);
                        }
                        smtp.Send(mail);
                        mail.Dispose();
                        foreach (var name in names)
                        {
                            System.IO.File.Delete(folderpath + name);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);

            }

            //}

        }
        public void sendMaile(string subject, string mailId, string Input, string MailMessage, List<string> attachments, Steps step, string senderusername, string senderpassword, string smtpaddress, string portnumber, string executionID)
        {
            try
            {
                // string emailtemplates = cm.GetAllMailItems();

                string emailtemplates = cm.GetMailTemplate(subject);
                using (System.Net.Mail.MailMessage testMailMessage = new System.Net.Mail.MailMessage())
                {
                    testMailMessage.From = new System.Net.Mail.MailAddress(senderusername);
                    testMailMessage.To.Add(new System.Net.Mail.MailAddress(mailId));
                    testMailMessage.Subject = subject;
                    string mailmessage = string.Empty;
                    DVariables vvalue = new DVariables();


                    try
                    {
                        if (attachments != null && attachments.Count > 0)
                        {

                            foreach (string attachment in attachments)
                            {

                                //ZipFile.CreateFromDirectory(attachment, zipFile);
                                System.Net.Mail.Attachment atch_data = new System.Net.Mail.Attachment(attachment, MediaTypeNames.Application.Octet);
                                testMailMessage.Attachments.Add(atch_data);
                            }
                            mailmessage = "Hello <br>" + MailMessage + "<br> Regards, <br>Genbase";
                        }
                        else
                        {
                            //DVariables value = new DVariables();
                            if (Input.Trim() != string.Empty)
                            {
                                vvalue = new VariableHandler().getVariables(Input, executionID);

                                //string outer = Input;
                                //outer.Replace("\n", "<br>"); outer.Replace("    ", "");
                                //mailmessage = "Hello <br>" + MailMessage + outer + "<br> Regards, <br>Genbase";

                                if (vvalue != null && vvalue.vlvalue != null)
                                {
                                    string outer = vvalue.vlvalue;
                                    outer.Replace("\n", "<br>"); outer.Replace("    ", "");
                                    //mailmessage = "Hello <br>" + MailMessage + outer + "<br> Regards, <br>Genbase";
                                }
                                else if (vvalue == null)
                                {
                                    mailmessage = "Hello <br> Error in Reporting, Server Error <br> Regards, <br>Genbase";
                                }
                                mailmessage = mailmessage + MailMessage;
                            }
                            else if (MailMessage.Trim() != string.Empty)
                            {
                                mailmessage = MailMessage.Replace("\n", "<br>");
                            }
                            mailmessage = mailmessage.Replace("\n", "<br>");
                        }




                        if (emailtemplates.Trim().Length > 0)
                        {

                            List<EmailTemplateModel> objListEmailTemp = new List<EmailTemplateModel>();
                            try
                            {
                                objListEmailTemp = JsonConvert.DeserializeObject<List<EmailTemplateModel>>(emailtemplates);
                            }
                            catch (Exception)
                            {

                            }

                            foreach (EmailTemplateModel objEmailTemp in objListEmailTemp)
                            {
                                if (objEmailTemp.Body.Contains("%POD_Details%"))
                                {
                                    string objTmp_bnsf = string.Empty;
                                    string objTmp_chr = string.Empty;
                                    if (vvalue != null && vvalue.vlvalue != null)
                                    {
                                        string outer = vvalue.vlvalue;
                                        List<EmailDefineModel> objListData = JsonConvert.DeserializeObject<List<EmailDefineModel>>(outer);

                                        foreach (EmailDefineModel _objtmp in objListData.Where(x => x.Type.ToLower() == "bnsf"))
                                        {
                                            objTmp_bnsf = objTmp_bnsf + "<br>" + _objtmp.Snumber;
                                        }

                                        foreach (EmailDefineModel _objtmp in objListData.Where(x => x.Type.ToLower() == "chr"))
                                        {
                                            objTmp_chr = objTmp_chr + "<br>" + _objtmp.Snumber;
                                        }

                                        if (objTmp_bnsf.Trim().Length > 0)
                                        {
                                            objTmp_bnsf = "BNSF" + "<br>" + objTmp_bnsf;
                                        }

                                        if (objTmp_chr.Trim().Length > 0)
                                        {
                                            objTmp_chr = "CHR" + "<br>" + objTmp_chr;
                                        }

                                        MailMessage = "<br> <br>" + objTmp_bnsf + "<br> <br>" + objTmp_chr + "<br> <br>";

                                    }


                                    mailmessage = objEmailTemp.Body.Replace("%POD_Details%", MailMessage);
                                    mailmessage = mailmessage.Replace("%POD_Date%", DateTime.Today.ToShortDateString());
                                    subject = objEmailTemp.Subject.ToString();
                                }

                                if (objEmailTemp.Body.Contains("%SNumber%"))
                                {
                                    string objTmp_bnsf = string.Empty;

                                    if (vvalue != null && vvalue.vlvalue != null)
                                    {
                                        string outer = vvalue.vlvalue;
                                        string snumber = string.Empty;
                                        if (outer != null)
                                        {
                                            DataTable inputdt = JsonConvert.DeserializeObject<DataTable>(outer);
                                            if (inputdt.Columns.Count > 0 && inputdt.Rows.Count > 0)
                                            {
                                                snumber = inputdt.Rows[0][0].ToString();
                                                snumber = snumber.Trim().ToString();
                                            }

                                        }

                                        MailMessage = "<br> <br>" + snumber + "<br> <br>";

                                    }

                                    mailmessage = objEmailTemp.Body.Replace("%SNumber%", MailMessage);
                                    subject = objEmailTemp.Subject.ToString();
                                }

                            }
                        }


                        testMailMessage.Body = mailmessage;
                        testMailMessage.IsBodyHtml = true;
                        testMailMessage.Priority = System.Net.Mail.MailPriority.Normal;
                        testMailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
                        testMailMessage.BodyEncoding = System.Text.Encoding.UTF8;

                        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                        smtp.Host = smtpaddress;
                        smtp.Credentials = new System.Net.NetworkCredential(senderusername, senderpassword);
                        smtp.Port = int.Parse(portnumber);
                        smtp.EnableSsl = true;
                        sendeMailEASendMail(senderusername, mailId, subject, mailmessage, senderusername, senderpassword, smtpaddress, attachments, int.Parse(portnumber));
                        //smtp.Send(testMailMessage);
                        log.add2Log(Messages.stepExecutionStarted, step.Robot_Id, step.Name, StringHelper.eventcode, false);
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
        }
        public string updateApplicationVaraibles(String msg)

        {
            //var x1 = Variables.dynamicvariables;
            List<DVariables> dList = new List<DVariables>();
            if (Variables.dynamicvariables != null)
            {
                if (Variables.dynamicvariables.Count > 0)
                {
                    DVariables _objD = new DVariables();
                    for (int i = 0; i < Variables.dynamicvariables.Count; i++)
                    {
                        _objD.vlname = Variables.dynamicvariables[i].vlname;
                        if (_objD.vlname == "Contact")
                        {
                            string cvalue = _objD.vlvalue;
                            msg.Replace("%%ContactInfo%%", cvalue);
                        }
                    }
                }
            }

            return msg;
        }

        public static string TextToHtml(string text)
        {
            text = HttpUtility.HtmlEncode(text);
            text = text.Replace("\r\n", "\r");
            text = text.Replace("\n", "\r");
            text = text.Replace("\n\n", "\r");
            text = text.Replace("\r", "<br>\r\n");
            text = text.Replace("  ", " &nbsp;");
            text = text.Replace("%", " ");
            return text;
        }

        public void sendeMailEASendMail(string from, string to, string subject, string message, string uname, string pswd, string smtp, List<string> attachments, int port)
        {
            try
            {
                //SmtpMail oMail = new SmtpMail(StringHelper.tryit);
                //EASendMail.SmtpClient oSmtp = new EASendMail.SmtpClient();

                //// Set sender email address, please change it to yours
                //oMail.From = from;

                //// Set recipient email address, please change it to yours
                //oMail.To = to;

                //// Set email subject
                //oMail.Subject = subject;

                //// Set email body
                ////message = message.Replace("\n", "<br>");
                //oMail.HtmlBody = message;



                //foreach (string attachment in attachments)
                //{
                //    oMail.AddAttachment(attachment);
                //}

                //// Your SMTP server address
                //SmtpServer oServer = new SmtpServer(smtp);

                //// User and password for ESMTP authentication, if your server doesn't require
                //// User authentication, please remove the following codes.
                //oServer.User = uname;
                //oServer.Password = pswd;

                //// If your smtp server requires SSL connection, please add this line
                //oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;
                //// Set SSL port
                //oServer.Port = port;

                MailMessage oMail = new MailMessage();
                oMail.From = new System.Net.Mail.MailAddress(from);
                oMail.To.Add(new System.Net.Mail.MailAddress(to));
                oMail.Subject = subject;
                oMail.Body = message;
                oMail.IsBodyHtml = true;
                foreach (string attachment in attachments)
                {
                    oMail.Attachments.Add(new System.Net.Mail.Attachment(attachment));
                }
                try
                {
                    NetworkCredential nc = new NetworkCredential(uname, pswd);
                    System.Net.Mail.SmtpClient oSmtp = new System.Net.Mail.SmtpClient(smtp, port);
                    oSmtp.Credentials = nc;
                    oSmtp.EnableSsl = true;
                    oSmtp.Send(oMail);
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, "SendMail"));
                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
            }
        }

        public MethodResult Reply(Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string Input = string.Empty; string Output = string.Empty; string Message = string.Empty; string replyall = string.Empty;
            string smtpserver = string.Empty; string smtpport = string.Empty; string outbound = string.Empty; string outboundEncrypt = string.Empty; string attachment = string.Empty;
            string MailId = string.Empty; string Password = string.Empty;

            try
            {
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                Message = stp.getPropertiesValue(StringHelper.message, step);
                replyall = stp.getPropertiesValue(StringHelper.replyall, step);
                outbound = stp.getPropertiesValue(StringHelper.instance, step);
                attachment = stp.getPropertiesValue(StringHelper.attachment, step);
                List<string> attaches = new List<string>();
                System.Data.DataTable outbounddt = new TypeHandler().getInput(outbound, "Variable", executionID);
                smtpserver = outbounddt.Rows[0]["OutboundMailServer"].ToString();
                smtpport = outbounddt.Rows[0]["OutboundPort"].ToString();
                outboundEncrypt = outbounddt.Rows[0]["OutboundEncrytion"].ToString();
                MailId = outbounddt.Rows[0]["MailId"].ToString();
                Password = outbounddt.Rows[0]["Password"].ToString();
                DataTable indt = new DataTable();

                DVariables value = new DVariables();
                if (Input.Trim() != string.Empty)
                {
                    value = new VariableHandler().getVariables(Input, executionID);
                    //if (value != null && value.vlvalue.GetType() == typeof(List<MailMessage>))
                    //{
                    //    messages = value.vlvalue;
                    //}
                    if (value != null)
                    {
                        indt = new TypeHandler().getInput(Input, "Variable", executionID);
                    }
                    //try
                    //{
                    //    List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                    //    try
                    //    {
                    //        FileAttributes attr = System.IO.File.GetAttributes(@files[0]);
                    //        attaches = files;
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        string ec = ex.Message;
                    //        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    //        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                    //    }
                    //}
                    //catch (Exception e)
                    //{
                    //    string ec = e.Message;
                    //    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    //    s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                    //}
                }
                if (attachment.Trim() != string.Empty)
                {
                    try
                    {
                        System.Data.DataTable dt = new TypeHandler().getInput(attachment, "Variable", executionID);

                        List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                        try
                        {
                            FileAttributes attr = System.IO.File.GetAttributes(@files[0]);
                            attaches = files;
                        }
                        catch (Exception ex)
                        {
                            string ec = ex.Message;
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                    }
                    catch (Exception e)
                    {
                        string ec = e.Message;
                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                    }
                }

                foreach (DataRow message in indt.Rows)
                {

                    InternetAddressList ReplyTo = GetIAList(message["ReplyTo"].ToString());
                    InternetAddressList From = GetIAList(message["From"].ToString());
                    MailboxAddress Sender = new MailboxAddress(string.Empty);
                    Sender = MailboxAddress.Parse(MailId);
                    if (ReplyTo.Count == 0)
                    {
                        ReplyTo = From;
                    }
                    InternetAddressList To = GetIAList(message["To"].ToString());
                    InternetAddressList CC = GetIAList(message["CC"].ToString());
                    InternetAddressList BCC = GetIAList(message["BCC"].ToString());
                    string Subject = message["Subject"].ToString();
                    string Body = message["Body"].ToString();
                    string MessageId = message["MessageID"].ToString();
                    MessageIdList Reference = GetMIDList(message["References"].ToString());
                    ReplyMessage(ReplyTo, From, Sender, true, To, CC, BCC, Subject, Body, Message, MessageId, Reference, smtpserver, smtpport, outboundEncrypt, MailId, Password);
                    // ReplyMessage(ReplyTo, From, Sender, true, To, CC, BCC, Subject, Message, MessageId, Reference, smtpserver, smtpport, outboundEncrypt, MailId, Password);
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);

            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log("Reply mail has been Executed", step.Robot_Id, step.Name, StringHelper.eventcode, false);

            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public InternetAddressList GetIAList(string addresseswithsemicolons)
        {
            InternetAddressList ialist = new InternetAddressList();
            if (addresseswithsemicolons.Trim() != string.Empty)
            {
                if (addresseswithsemicolons.Contains(";"))
                {
                    List<string> stinger = addresseswithsemicolons.Split(';', StringSplitOptions.None).ToList();
                    stinger.ForEach(sting => ialist.Add(InternetAddress.Parse(sting)));
                }
                else
                {
                    ialist.Add(InternetAddress.Parse(addresseswithsemicolons));
                }
            }
            else
            {

            }
            return ialist;
        }
        public MessageIdList GetMIDList(string midswithsemicolons)
        {
            MessageIdList ialist = new MessageIdList();
            if (midswithsemicolons.Trim() != string.Empty)
            {
                if (midswithsemicolons.Contains(";"))
                {
                    List<string> stinger = midswithsemicolons.Split(';', StringSplitOptions.None).ToList();
                    stinger.ForEach(sting => ialist.Add(sting));
                }
                else
                {
                    ialist.Add(midswithsemicolons);
                }
            }
            else
            {

            }
            return ialist;
        }
        public void ReplyMessage(InternetAddressList ReplyTo, InternetAddressList From, MailboxAddress Sender, bool replyToAll, InternetAddressList To, InternetAddressList CC, InternetAddressList BCC, string Subject, string Body, string Message, string MessageId, MessageIdList Reference, string smtpserver, string smtpport, string outboundEncrypt, string uname, string password)
        {
            try
            {
                #region
                var reply = new MimeMessage(); MailboxAddress sender = new MailboxAddress(string.Empty);
                if (ReplyTo.Count > 0)
                {
                    reply.To.AddRange(ReplyTo);
                    reply.To.AddRange(To);
                    reply.ReplyTo.AddRange(ReplyTo);
                }
                else if (From.Count > 0)
                {
                    reply.To.AddRange(From);
                }
                else if (Sender != null)
                {
                    reply.To.Add(Sender);
                }

                if (replyToAll)
                {
                    reply.To.AddRange(To);
                    reply.Cc.AddRange(CC);
                    reply.Bcc.AddRange(BCC);
                }

                if (!Subject.StartsWith("Re:", StringComparison.OrdinalIgnoreCase))
                    reply.Subject = "Re:" + Subject;
                else
                    reply.Subject = Subject;

                if (!string.IsNullOrEmpty(MessageId))
                {
                    reply.InReplyTo = MessageId;
                    foreach (var id in Reference)
                        reply.References.Add(id);
                    reply.References.Add(MessageId);
                }
                using (var quoted = new StringWriter())
                {
                    if (Sender != null && Sender.Address != string.Empty)
                    {
                        sender = Sender;
                    }
                    else
                    {
                        if (From != null && From.ToList().Count > 0)
                        {
                            sender = From.Mailboxes.FirstOrDefault();
                        }
                        else
                        {
                            sender = new MailboxAddress(uname.Trim());
                        }
                    }

                    using (var reader = new StringReader(Message))
                    {
                        string line;

                        while ((line = reader.ReadLine()) != null)
                        {
                            quoted.WriteLine(line);
                        }
                    }

                    reply.Body = new TextPart("plain")
                    {
                        Text = quoted.ToString()
                    };
                    if (reply.Sender == null)
                    {
                        reply.Sender = Sender;
                    }
                }
                #endregion
                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect(smtpserver.Trim(), int.Parse(smtpport.Trim()));

                    client.Authenticate(uname.Trim(), password.Trim());

                    client.Send(reply);

                    client.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
            }
        }


        public void ReplyMessage(MimeMessage message, bool replyToAll, List<string> attachments, string sendmessage, string port, string server)
        {
            try
            {
                //#region
                //SmtpMail oMail = new SmtpMail(StringHelper.tryit);
                //EASendMail.SmtpClient oSmtp = new EASendMail.SmtpClient();
                //if (message.ReplyTo.Count > 0)
                //{
                //    oMail.To.Add(message.ReplyTo.ToString());
                //}
                //else if (message.From.Count > 0)
                //{
                //    oMail.To.Add(message.From.ToString());
                //}
                //else if (message.Sender != null)
                //{
                //    oMail.To.Add(string.Join(";", message.Sender));
                //}
                //if (replyToAll)
                //{
                //    oMail.To.Add(string.Join(";", message.To));
                //    oMail.Cc.Add(string.Join(";", message.Cc));
                //}

                //oMail.From = message.To.ToString();
                //if (!message.Subject.StartsWith("Re:", StringComparison.OrdinalIgnoreCase))
                //    oMail.Subject = "Re:" + message.Subject;
                //if (!string.IsNullOrEmpty(message.MessageId))
                //{
                //    oMail.Headers.Insert(0, new HeaderItem("Message-ID", message.MessageId));
                //}
                //foreach (string attachment in attachments)
                //{
                //    oMail.AddAttachment(attachment);
                //}
                //using (var quoted = new StringWriter())
                //{
                //    var sender = message.Sender ?? message.From.Mailboxes.FirstOrDefault();
                //    quoted.WriteLine("On {0}, {1} wrote:", message.Date.ToString("f"), !string.IsNullOrEmpty(sender.Name) ? sender.Name : sender.Address);
                //    try
                //    {
                //        using (var reader = new StringReader(message.TextBody))
                //        {
                //            string line;
                //            while ((line = reader.ReadLine()) != null)
                //            {
                //                quoted.Write("> ");
                //                quoted.WriteLine(line);
                //            }
                //        }

                //    }
                //    catch (Exception ex)
                //    {
                //        new Logger().LogException(ex);
                //    }
                //    oMail.HtmlBody = sendmessage + "\n" + quoted.ToString();

                //}
                //SmtpServer oServer = new SmtpServer(server);
                //oServer.User = Variables.username;
                //oServer.Password = Variables.password;
                //oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;
                //oServer.Port = Convert.ToInt32(port);
                //try
                //{
                //    oSmtp.SendMail(oServer, oMail);
                //}
                //catch (Exception ex)
                //{
                //    new Logger().LogException(ex);
                //}
                //#endregion

                #region
                var reply = new MimeMessage();
                if (message.ReplyTo.Count > 0)
                {
                    reply.To.AddRange(message.ReplyTo);
                }
                else if (message.From.Count > 0)
                {
                    reply.To.AddRange(message.From);
                }
                else if (message.Sender != null)
                {
                    reply.To.Add(message.Sender);
                }

                if (replyToAll)
                {
                    reply.To.AddRange(message.To);
                    reply.Cc.AddRange(message.Cc);
                }

                if (!message.Subject.StartsWith("Re:", StringComparison.OrdinalIgnoreCase))
                    reply.Subject = "Re:" + message.Subject;
                else
                    reply.Subject = message.Subject;

                if (!string.IsNullOrEmpty(message.MessageId))
                {
                    reply.InReplyTo = message.MessageId;
                    foreach (var id in message.References)
                        reply.References.Add(id);
                    reply.References.Add(message.MessageId);
                }

                using (var quoted = new StringWriter())
                {
                    var sender = message.Sender ?? message.From.Mailboxes.FirstOrDefault();

                    using (var reader = new StringReader(message.TextBody))
                    {
                        string line;

                        while ((line = reader.ReadLine()) != null)
                        {
                            quoted.Write("> ");
                            quoted.WriteLine(line);
                        }
                    }

                    reply.Body = new TextPart("plain")
                    {
                        Text = quoted.ToString()
                    };
                }
                #endregion

            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);

            }
        }
        public void sendeMailkit(MimeMessage msg, string port, string server)
        {
            try
            {
                var client = new MailKit.Net.Smtp.SmtpClient();
                client.Connect(server.Trim(), Convert.ToInt32(port.Trim()), true);
                client.Authenticate(Variables.username, Variables.password);
                client.Send(msg);
                client.Disconnect(true);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);

            }
        }
        public void sendeTemplate(string template, string mailid, string inputvar, List<string> attaches, Steps steps, string senderusrname, string senderpswd, string smtpaddress, string port, string executionID)
        {
            try
            {
                string gemail = cm.GetAllMailItems();
                List<EmailTemplate> templateslist = JsonConvert.DeserializeObject<List<EmailTemplate>>(gemail);
                string globalConfiguration = cm.GetGlobalConfiguration();
                List<Configuration> configuartionList = JsonConvert.DeserializeObject<List<Configuration>>(globalConfiguration);
                EmailTemplate templater = new EmailTemplate();
                foreach (EmailTemplate item in templateslist)
                {
                    if (item.Subject.Trim() == template.Trim() || item.Id == template)
                    {
                        if (Variables.xuser.Count > 0)
                        {
                            var body = item.Body;
                            foreach (xUser cu in Variables.xuser)
                            {
                                if (cu.adusername != null)
                                {
                                    body = body.Replace(StringHelper.newemployee, cu.adusername);
                                    body = body.Replace(StringHelper.user, cu.adusername);
                                }
                                if (cu.mailid != null)
                                    body = body.Replace(StringHelper.eemail, cu.mailid);
                                if (cu.hrmsusername != null)
                                    body = body.Replace(StringHelper.empid, cu.hrmsusername);
                                if (cu.sapuserid != null)
                                    body = body.Replace(StringHelper.sapid, cu.sapuserid);
                                //%email%,%user%,%empid%
                                if (cu.EmployeeName != null)
                                    body = body.Replace(StringHelper.EmployeeName, cu.EmployeeName);
                                if (cu.SupervisorName != null)
                                    body = body.Replace(StringHelper.SupervisorName, cu.SupervisorName);
                                if (cu.Manager != null)
                                    body = body.Replace(StringHelper.Manager, cu.Manager);
                                if (cu.ManagerName != null)
                                    body = body.Replace(StringHelper.ManagerName, cu.ManagerName);
                                if (cu.HRName != null)
                                    body = body.Replace(StringHelper.HRName, cu.HRName);
                                if (cu.Department != null)
                                    body = body.Replace(StringHelper.Department, cu.Department);
                                if (cu.Gender != null)
                                {
                                    if (cu.Gender == "Male")
                                        body = body.Replace(StringHelper.Gender, "He");
                                    else
                                        body = body.Replace(StringHelper.Gender, "She");
                                }
                                if (cu.Date != null)
                                    body = body.Replace(StringHelper.Date, cu.Department);
                                if (cu.TotalNumberOfYears != null)
                                    body = body.Replace(StringHelper.TotalNumberOfYears, "4 Years");
                                if (cu.Expertise != null)
                                    body = body.Replace(StringHelper.Expertise, "Expertise");
                                if (cu.OtherInformation != null)
                                    body = body.Replace(StringHelper.OtherInformation, "Other Information");
                                if (cu.EmployeePosition != null)
                                    body = body.Replace(StringHelper.EmployeePosition, cu.EmployeePosition);
                            }

                            sendMaile(item.Subject, mailid, inputvar, body, attaches, steps, senderusrname, senderpswd, smtpaddress, port, executionID);
                        }
                        else
                        {
                            //sendMaile(item.Subject, mailid, inputvar, body, attaches, steps, senderusrname, senderpswd, smtpaddress, port);
                            if (item.Body != null && item.Body != string.Empty)
                            {
                                foreach (Configuration config in configuartionList)
                                {
                                    if (config.Name != String.Empty)
                                    {
                                        item.Body = item.Body.Replace("%" + config.Name.Trim() + "%", config.Value);
                                        item.Subject = item.Subject.Replace("%%" + config.Name.Trim() + "%%", config.Value);
                                    }
                                }

                                List<DVariables> vars = Variables.dynamicvariables.FindAll(v => v.ExecutionID == executionID);

                                DataTable dt = ToDataTable(vars);
                                //DataTable dt = JsonConvert.DeserializeObject<System.Data.DataTable>(Variables.dynamicvariables.vlvalue);

                                //DataTable dt1;
                                foreach (var element in Variables.system_Variables)
                                {
                                    item.Body = item.Body.Replace("%%" + element.Key.Trim() + "%%", element.Value);
                                    item.Subject = item.Subject.Replace("%%" + element.Key.Trim() + "%%", element.Value);
                                }
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    try
                                    {
                                        if (dt.Rows.Count > 0 && dt.Rows[0][0].ToString() != string.Empty)
                                        {
                                            if (dt.Rows[i]["vltype"].ToString().ToLower() == "datatable")
                                            {
                                                string stringdt = dt.Rows[i]["vlvalue"].ToString();
                                            if (stringdt == string.Empty || stringdt == null || stringdt == "[]")
                                                stringdt = "[{\"0\":\"0\"}]";
                                            DataTable DataTabledt = JsonConvert.DeserializeObject<DataTable>(stringdt);
                                            item.Body = item.Body.Replace("%%" + dt.Rows[i]["vlname"].ToString() + "%%", DataTabledt.Rows[0][0].ToString());
                                            item.Subject = item.Subject.Replace("%%" + dt.Rows[i]["vlname"].ToString() + "%%", DataTabledt.Rows[0][0].ToString());
                                            }
                                            else // Replacing Variable from Robot
                                            {
                                                string stringdt = dt.Rows[i]["vlname"].ToString();
                                                string stringdv = dt.Rows[i]["vlvalue"].ToString();
                                                string ob = "{"; string cb="}"; 
                                                if(stringdt.Contains(ob) && stringdt.Contains(cb) && stringdt!="")
                                                item.Body = item.Body.Replace(stringdt, stringdv);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        new Logger().LogException(ex);
                                    }
                                }

                            }
                            sendeMailEASendMail(senderusrname, mailid, item.Subject.Trim(), TextToHtml(item.Body.Trim()), senderusrname, senderpswd, smtpaddress, attaches, int.Parse(port));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);

            }
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                    dataTable.Columns.Add(prop.Name, type);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {
                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }
                return dataTable;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return dataTable;
            }
        }

        public IList<UniqueId> getMailUniqueIDs(string readtype, IMailFolder xpecialFolder)
        {
            IList<UniqueId> uids = new List<UniqueId>();

            if (readtype.ToLower() == StringHelper.unread)
            {
                uids = xpecialFolder.Search(SearchQuery.NotSeen);
            }
            else if (readtype.ToLower() == StringHelper.all)
            {
                uids = xpecialFolder.Search(SearchQuery.All);
            }
            else if (readtype.ToLower() == StringHelper.answered)
            {
                uids = xpecialFolder.Search(SearchQuery.Answered);
            }
            else if (readtype.ToLower() == StringHelper.deleted)
            {
                uids = xpecialFolder.Search(SearchQuery.Deleted);
            }
            else if (readtype.ToLower() == StringHelper.quottednew)
            {
                uids = xpecialFolder.Search(SearchQuery.New);
            }
            else if (readtype.ToLower() == StringHelper.recent)
            {
                uids = xpecialFolder.Search(SearchQuery.Recent);
            }
            else if (readtype.ToLower() == StringHelper.seen)
            {
                uids = xpecialFolder.Search(SearchQuery.Seen);
            }
            else
            {
                uids = xpecialFolder.Search(SearchQuery.All);
            }

            return uids;
        }

        public bool FindinText(string content, List<string> keys)
        {
            bool returner = false;
            foreach (string key in keys)
            {
                if (content.ToLower().IndexOf(key.ToLower().Trim()) > -1)
                {
                    returner = true;
                }
                else
                {
                    returner = false;
                }
            }
            return returner;
        }

    }
}
