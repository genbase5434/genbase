﻿using Genbase.classes;
using Genbase.Classes;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Genbase.BLL.ElementsClasses
{
    public class Database
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        EncoderDecoderBase64 baser = new EncoderDecoderBase64();
        DAL.QueryUtil query = new DAL.QueryUtil();
        DAL.PostgresConnect db = new DAL.PostgresConnect();
        DAL.MySQL sql = new DAL.MySQL();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult Connection(Steps step, string executionID)
        {
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string dburl = string.Empty; string dbusername = string.Empty; string dbpassword = string.Empty;
            string dbport = string.Empty; string dbname = string.Empty; string dbtype = string.Empty;
            string input = string.Empty; string inputtype = string.Empty; string output = string.Empty;
            string connectiontype = string.Empty;


            dburl = stp.getPropertiesValue(StringHelper.dburl, step);
            dbusername = stp.getPropertiesValue(StringHelper.dbusername, step);
            dbpassword = stp.getPropertiesValue(StringHelper.dbpassword, step);
            dbport = stp.getPropertiesValue(StringHelper.dbport, step);
            dbname = stp.getPropertiesValue(StringHelper.dbname, step);
            dbtype = stp.getPropertiesValue(StringHelper.dbtype, step);
            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
            connectiontype = stp.getPropertiesValue(StringHelper.ConnectionType, step);

            DataTable returnValue = new DataTable();
            returnValue.Columns.Add("ConnectionString");
            returnValue.Columns.Add("DataBaseServerType");

            try
            {
                if (dbtype.Trim().ToLower() == "postgresql" || dbtype.Trim().ToLower() == "3")
                {
                    if ((dburl != string.Empty && dbport != string.Empty) && (dbusername != string.Empty && dbpassword != string.Empty))
                    {
                        returnValue.Rows.Add(FormConnectionStringPostgreSQL(dbtype, dburl, dbport, dbusername, dbpassword, dbname), "PostgreSQL");
                    }
                    else
                    {
                        if (connectiontype == "runtimeinput")
                        {
                            //DataTable dt = new TypeHandler().getInput(input, inputtype);
                            DataTable dt = new TypeHandler().getInput(input, executionID);
                            foreach (DataRow dtr in dt.Rows)
                            {
                                if (dtr != null)
                                {
                                    returnValue.Rows.Add(FormConnectionStringPostgreSQL(dtr[0].ToString(), dtr[1].ToString(), dtr[2].ToString(), dtr[3].ToString(), dtr[4].ToString(), dtr[5].ToString()), "PostgreSQL");
                                }
                            }
                        }
                    }
                }
                else if (dbtype.Trim().ToLower() == "mysql" || dbtype.Trim().ToLower() == "2")
                {
                    MySqlConnectionStringBuilder conn_string = new MySqlConnectionStringBuilder();
                    conn_string.Server = dburl;
                    conn_string.UserID = dbusername;
                    conn_string.Password = dbpassword;
                    conn_string.Database = dbname;
                    // conn_string.Port = UInt32.Parse(dbport);

                    returnValue.Rows.Add(JsonConvert.SerializeObject(conn_string), "MySQL");
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
				new ExecutionLog().add2Log(ex.Message, step.Robot_Id, step.Name, StringHelper.eventcode, false);
			}
			MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                string tmp = het.Split(';')[3];
                het = het.Replace(tmp, "Password=******");
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, null, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
				string het = new TypeHandler().ConvertDataTableToString(returnValue);
				mr = new MethodResultHandler().createResultType(false, msgs, null, s);
				new ExecutionLog().add2Log(het+" please check connection", step.Robot_Id, step.Name, StringHelper.eventcode, false);
			}
            return mr;
        }
        public MethodResult Read(Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty; DataTable returnValue = new DataTable();

            string connection = string.Empty; string tablename = string.Empty;
            string input = string.Empty; string inputtype = string.Empty; string output = string.Empty;
            string wherepart = string.Empty; string columnpart = string.Empty; string maxrowcount = string.Empty;
            string groupby = string.Empty; string orderby = string.Empty; string GroupFields = string.Empty;

            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
                connection = stp.getPropertiesValue(StringHelper.connection, step);
                tablename = stp.getPropertiesValue(StringHelper.tablename1S, step);
                maxrowcount = stp.getPropertiesValue(StringHelper.Rows, step);
                groupby = stp.getPropertiesValue(StringHelper.groupby, step);
                orderby = stp.getPropertiesValue(StringHelper.orderby, step);
                wherepart = stp.getPropertiesValue(StringHelper.condition, step);
                columnpart = stp.getPropertiesValue(StringHelper.fields, step);
                //GroupFields = stp.getPropertiesValue(StringHelper.GroupFields, step);
                System.Data.DataTable dt = new DataTable();
                // System.Data.DataTable dtconnect = new TypeHandler().getInput(connection,"Variable");
                System.Data.DataTable dtconnect = new TypeHandler().getInput(connection, executionID);

                foreach (DataRow drt in dtconnect.Rows)
                {
                    string schemaname = string.Empty;

                    string dbconn = drt["ConnectionString"].ToString();
                    string dbtype = drt["DataBaseServerType"].ToString();
                    if (dbtype.Trim().ToLower() == StringHelper.postgresql)
                    {
                        if (tablename.IndexOf(".") > -1)
                        {
                            tablename = tablename.Split('.')[1];
                            schemaname = tablename.Split('.')[0];
                        }

                        wherepart = wherepart.Replace(";", "||").Replace("\b::\b","").Replace(":", "|");
                        columnpart = columnpart.Replace(";", ",");
                        //List<string> cols = columns_to_display.Split(',').ToList();
                        //List<string> cols2 = new List<string>();
                        //foreach (string col in cols)
                        //{
                        //    if (!col.Contains('('))
                        //        cols2.Add("\"" + col + "\"");
                        //    else
                        //        cols.Add(col);
                        //}
                        int y = 0;
                        if (int.TryParse(maxrowcount, out y))
                        { }
                        else
                        { }
                        Variables.connString = dbconn;
                        Variables.datatbname = tablename.Trim();
                        string yquery = getTableDataPostgreSQL(tablename, wherepart, columnpart, GroupFields, y, schemaname, orderby, groupby, dbconn, executionID);
                        returnValue = JsonConvert.DeserializeObject<DataTable>(yquery);
                    }
                    else if (dbtype.Trim().ToLower() == StringHelper.mysql)
                    {
                        
                        wherepart = wherepart.Replace(";", "||").Replace(":", "|");
                        
                        columnpart =  columnpart.Replace(";", ",");
                        int y = 0;
                        if (int.TryParse(maxrowcount, out y))
                        { }
                        else
                        { }
                        Variables.connString = dbconn;
                        Variables.datatbname = tablename.Trim();
                        string yquery = getTableDataMySQL(tablename, wherepart, columnpart, y, orderby, groupby, dbconn, executionID);
                        returnValue = JsonConvert.DeserializeObject<DataTable>(yquery);
                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                if (het == "|\n\n")
                {
                    new ExecutionLog().add2Log("please add a Records Or Tickets!! Because We can't Find any new Records or new tickets", step.Robot_Id, step.Name, StringHelper.eventcode, false);
                }
                else
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }

            return mr;
        }
        public MethodResult Insert(Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty; DataTable returnValue = new DataTable(); DataTable returnValueAlt = new DataTable();

            string connection = string.Empty; string tablename = string.Empty; string returning = string.Empty; string ColumnHeader=string.Empty;
            string input = string.Empty; string inputtype = string.Empty; string output = string.Empty;
            string rowvalues = string.Empty; string columnvalues = string.Empty; string isencoded = string.Empty;

            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
            connection = stp.getPropertiesValue(StringHelper.connection, step);
            tablename = stp.getPropertiesValue(StringHelper.tablename1S, step);
            rowvalues = stp.getPropertiesValue(StringHelper.values, step);
            columnvalues = stp.getPropertiesValue(StringHelper.columns, step);
            isencoded = stp.getPropertiesValue(StringHelper.isencoded, step);
            returning = stp.getPropertiesValue(StringHelper.returning, step);
            
            bool isDefaultColumn = false; string returned = string.Empty;

            try
            {
                System.Data.DataTable dt = new DataTable();
                System.Data.DataTable dtconnect = new TypeHandler().getInput(connection, executionID);
                // System.Data.DataTable dtconnect = new TypeHandler().getInput(connection,"Variable");
                foreach (DataRow drt in dtconnect.Rows)
                {
                    string dbconn = drt["ConnectionString"].ToString();
                    string dbtype = drt["DataBaseServerType"].ToString();


                    if (dbtype.Trim().ToLower() == StringHelper.postgresql)
                    {
                        List<TableSchemaPostgreSQL> schems = getTableSchemaPostgreSQL(tablename, dbconn);
                        List<TableSchemaPostgreSQL> isPrimaryNotNull = getTablePrimaryKey(tablename, dbconn);

                        string schemaname = string.Empty;
                        if (tablename.IndexOf(".") > -1)
                        {
                            schemaname = tablename.Split('.')[0];
                        }
                        else
                        {
                            tablename = "public.\"" + tablename + "\"";
                        }

                        List<List<InsertSchemaPostgreSQL>> inserter = new List<List<InsertSchemaPostgreSQL>>();

                        if (input != string.Empty && inputtype != string.Empty)
                        {

                            dt = new TypeHandler().getInput(input, inputtype, executionID);
                            if (ColumnHeader != string.Empty)
                            {
                                dt.Columns.Add("rowheader");
                                foreach (DataRow r in dt.Rows)
                                {
                                    r["rowheader"] = ColumnHeader.Trim();
                                }
                            }
                            //int i = 0;
                            /// }
                            else
                            {
                                if (rowvalues != string.Empty && columnvalues != string.Empty)
                                {
                                    if (columnvalues.Trim() == "*")
                                    {
                                        if (new VariableHandler().checkVariableExists(rowvalues, executionID))
                                        {
                                            dt = new TypeHandler().getInput(rowvalues, "variable", executionID);
                                        }
                                        else
                                        {
                                            dt = new TypeHandler().getInput(rowvalues, "Value", executionID);
                                        }
                                        if (dt.Columns.Count == schems.Count)
                                        {
                                            for (int i = 0; i < dt.Columns.Count; i++)
                                            {
                                                dt.Columns[i].ColumnName = schems[i].column_name;
                                                dt.AcceptChanges();
                                            }
                                            isDefaultColumn = true;
                                        }
                                        else
                                        {

                                        }
                                    }
                                    else
                                    {
                                        if (new VariableHandler().checkVariableExists(rowvalues, executionID))
                                        {
                                            dt = new TypeHandler().getInput(rowvalues, "variable", executionID);
                                        }
                                        else
                                        {
                                            dt = new TypeHandler().getInput(rowvalues, "Value", executionID);
                                        }
                                        List<string> colNames = columnvalues.Split(':').ToList();
                                        if (dt.Columns.Count <= schems.Count && dt.Columns.Count == colNames.Count)
                                        {
                                            for (int i = 0; i < dt.Columns.Count; i++)
                                            {
                                                dt.Columns[i].ColumnName = colNames[i];
                                                dt.AcceptChanges();
                                            }
                                        }
                                        else
                                        {

                                        }
                                    }
                                }
                            }



                            foreach (DataRow fr in dt.Rows)
                            {
                                List<InsertSchemaPostgreSQL> rowinserter = new List<InsertSchemaPostgreSQL>();
                                if (fr.Table.Columns.Count <= schems.Count)
                                {
                                    string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                                    //if (columnNames.Contains(";"))
                                    //{
                                    //    List<string> cr=  columnNames.split(";")
                                    //}
                                    List<string> cr = columnNames.ToList();
                                    cr.RemoveAll(g => g.Trim() == string.Empty);

                                    if (isPrimaryNotNull.Count > 0)
                                    {
                                        isDefaultColumn = true;
                                        InsertSchemaPostgreSQL isp = new InsertSchemaPostgreSQL();
                                        isp.db_column_name = isPrimaryNotNull[0].column_name;
                                        isp.db_column_type = isPrimaryNotNull[0].data_type;
                                        isp.dt_value = "default";
                                        isp.dt_value_type = "string";
                                        rowinserter.Add(isp);
                                    }
                                    else
                                    {
                                        isDefaultColumn = false;
                                    }

                                    foreach (string c in cr)
                                    {
                                        if (schems.Find(q => q.column_name.Trim().ToLower() == c.Trim().ToLower()) != null)
                                        {
                                            InsertSchemaPostgreSQL isp = new InsertSchemaPostgreSQL();
                                            isp.db_column_name = (schems.Find(q => q.column_name.Trim().ToLower() == c.Trim().ToLower())).column_name;
                                            isp.db_column_type = (schems.Find(q => q.column_name.Trim().ToLower() == c.Trim().ToLower())).data_type;
                                            isp.dt_value = fr[c].ToString();
                                            isp.dt_value_type = c;
                                            rowinserter.Add(isp);
                                        }
                                    }

                                    //if (schems.Find(q => q.column_name.Trim().ToLower() == "createby") != null)
                                    //{
                                    //    InsertSchemaPostgreSQL isp = new InsertSchemaPostgreSQL();
                                    //    isp.db_column_name = schems.Find(q => q.column_name.Trim().ToLower() == "createby").column_name;
                                    //    isp.db_column_type = schems.Find(q => q.column_name.Trim().ToLower() == "createby").data_type;
                                    //    isp.dt_value = new VariableHandler().getVariables("ExecutedUser").vlvalue.ToString();
                                    //    isp.dt_value_type = "string";
                                    //    rowinserter.Add(isp);
                                    //}

                                    //if (schems.Find(q => q.column_name.Trim().ToLower() == "createdatetime") != null)
                                    //{
                                    //    InsertSchemaPostgreSQL isp = new InsertSchemaPostgreSQL();
                                    //    isp.db_column_name = schems.Find(q => q.column_name.Trim().ToLower() == "createdatetime").column_name;
                                    //    isp.db_column_type = schems.Find(q => q.column_name.Trim().ToLower() == "createdatetime").data_type;
                                    //    isp.dt_value = DateTime.Now.ToLongDateString() + " " +DateTime.Now.ToLongTimeString();
                                    //    isp.dt_value_type = "string";
                                    //    rowinserter.Add(isp);
                                    //}

                                    //if (schems.Find(q => q.column_name.Trim().ToLower() == "updateby") != null)
                                    //{
                                    //    InsertSchemaPostgreSQL isp = new InsertSchemaPostgreSQL();
                                    //    isp.db_column_name = schems.Find(q => q.column_name.Trim().ToLower() == "updateby").column_name;
                                    //    isp.db_column_type = schems.Find(q => q.column_name.Trim().ToLower() == "updateby").data_type;
                                    //    isp.dt_value = new VariableHandler().getVariables("ExecutedUser").vlvalue.ToString();
                                    //    isp.dt_value_type = "string";
                                    //    rowinserter.Add(isp);
                                    //}

                                    //if (schems.Find(q => q.column_name.Trim().ToLower() == "updatedatetime") != null)
                                    //{
                                    //    InsertSchemaPostgreSQL isp = new InsertSchemaPostgreSQL();
                                    //    isp.db_column_name = schems.Find(q => q.column_name.Trim().ToLower() == "updatedatetime").column_name;
                                    //    isp.db_column_type = schems.Find(q => q.column_name.Trim().ToLower() == "updatedatetime").data_type;
                                    //    isp.dt_value = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
                                    //    isp.dt_value_type = "string";
                                    //    rowinserter.Add(isp);
                                    //}

                                    //if (schems.Find(q => q.column_name.Trim().ToLower() == "status") != null)
                                    //{
                                    //    InsertSchemaPostgreSQL isp = new InsertSchemaPostgreSQL();
                                    //    isp.db_column_name = schems.Find(q => q.column_name.Trim().ToLower() == "status").column_name;
                                    //    isp.db_column_type = schems.Find(q => q.column_name.Trim().ToLower() == "status").data_type;
                                    //    isp.dt_value = "true";
                                    //    isp.dt_value_type = "string";
                                    //    rowinserter.Add(isp);
                                    //}

                                    //if (fr.Table.Columns.Count == schems.Count) isDefaultColumn = true;
                                }
                                inserter.Add(rowinserter);
                            }


                            foreach (List<InsertSchemaPostgreSQL> row in inserter)
                            {
                                string tablePart = "insert into " + tablename + "(";
                                string columnPart = string.Empty;
                                string valuesPart = ") values( ";
                                string columnValuesPart = string.Empty;
                                string returningPart = "Returning \"" + returning + "\"";
                                string queryPart = string.Empty;

                                //if (isDefaultColumn == false)
                                //{
                                //    valuesPart = valuesPart.Replace("default,", "");
                                //}

                                foreach (InsertSchemaPostgreSQL cell in row)
                                {
                                    if (cell.db_column_type.Trim().ToLower().IndexOf("int") > -1 || cell.db_column_type.Trim().ToLower().IndexOf("serial") > -1 || cell.db_column_type.Trim().ToLower().IndexOf("numeric") > -1 || cell.db_column_type.Trim().ToLower().IndexOf("decimal") > -1 || cell.db_column_type.Trim().ToLower().IndexOf("double") > -1 || cell.db_column_type.Trim().ToLower().IndexOf("bool") > -1 || cell.db_column_type.Trim().ToLower().IndexOf("boolean") > -1)
                                    {
                                        columnPart = columnPart + "\"" + cell.db_column_name + "\",";

                                        int y = 0; double u = 0; bool b = true; dynamic f;

                                        if (int.TryParse(cell.dt_value, out y))
                                        {
                                            f = y;
                                        }
                                        else if ((cell.db_column_type.Trim().ToLower() == "bigint" && cell.dt_value == "default"))
                                        {
                                            f = cell.dt_value;
                                        }
                                        else
                                        {
                                            if (double.TryParse(cell.dt_value, out u))
                                            {
                                                f = u;
                                            }
                                            else
                                            {
                                                if (bool.TryParse(cell.dt_value, out b))
                                                {
                                                    f = b;
                                                }
                                                else
                                                {
                                                    f = "default";
                                                }
                                            }
                                        }

                                        columnValuesPart = columnValuesPart + f + " ,";
                                    }
                                    else
                                    {
                                        columnPart = columnPart + "\"" + cell.db_column_name + "\",";
                                        columnValuesPart = columnValuesPart + "'" + cell.dt_value.Replace("'", "''") + "',";
                                    }
                                }
                                if (columnPart.Trim() != string.Empty && columnValuesPart.Trim() != string.Empty)
                                {
                                    columnPart = columnPart.Substring(0, columnPart.Length - 1);
                                    columnValuesPart = columnValuesPart.TrimEnd(',');
                                    if (returning != string.Empty)
                                    {
                                        queryPart = tablePart + columnPart + valuesPart + columnValuesPart + ")" + returningPart + ";";
                                        returned = db.DbExecuteQuery(queryPart, "createrobot", dbconn);
                                    }
                                    else
                                    {
                                        queryPart = tablePart + columnPart + valuesPart + columnValuesPart + ");";
                                        returned = db.DbExecuteQuery(queryPart, "insert", dbconn);
                                    }

                                    if (returned.Trim() == string.Empty)
                                    {
                                        returned = "inserted";
                                    }

                                    returnValue.Columns.Add();
                                    returnValue.Rows.Add(returned);
                                }

                            }

                        }
                    }
                    else if (dbtype.Trim().ToLower() == StringHelper.mysql)
                    {
                        List<TableSchemaPostgreSQL> schems = getTableSchemamySQL(tablename, dbconn);
                        List<TableSchemaPostgreSQL> isPrimaryNotNull = getTablePrimaryKeyMySQL(tablename, dbconn);

                        string schemaname = string.Empty;
                        if (tablename.IndexOf(".") > -1)
                        {
                            schemaname = tablename.Split('.')[0];
                        }
                        else
                        {

                        }

                        List<List<InsertSchemaPostgreSQL>> inserter = new List<List<InsertSchemaPostgreSQL>>();

                        if (input != string.Empty && inputtype != string.Empty)
                        {
                            dt = new TypeHandler().getInput(input, inputtype, executionID);
                        }
                        else
                        {
                            if (rowvalues != string.Empty && columnvalues != string.Empty)
                            {
                                if (columnvalues.Trim() == "*")
                                {
                                    dt = new TypeHandler().getInput(rowvalues, "Value", executionID);
                                    if (dt.Columns.Count == schems.Count)
                                    {
                                        for (int i = 0; i < dt.Columns.Count; i++)
                                        {
                                            dt.Columns[i].ColumnName = schems[i].column_name;
                                            dt.AcceptChanges();
                                        }
                                        isDefaultColumn = true;
                                    }
                                    else
                                    {

                                    }
                                }
                                else
                                {
                                    dt = new TypeHandler().getInput(rowvalues, "Value", executionID);
                                    List<string> colNames = columnvalues.Split(':').ToList();
                                    if (dt.Columns.Count <= schems.Count && dt.Columns.Count == colNames.Count)
                                    {
                                        for (int i = 0; i < dt.Columns.Count; i++)
                                        {
                                            dt.Columns[i].ColumnName = colNames[i];
                                            dt.AcceptChanges();
                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }




                        foreach (DataRow fr in dt.Rows)
                        {
                            List<InsertSchemaPostgreSQL> rowinserter = new List<InsertSchemaPostgreSQL>();
                            if (fr.Table.Columns.Count <= schems.Count)
                            {
                                string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                                List<string> cr = columnNames.ToList();
                                cr.RemoveAll(g => g.Trim() == string.Empty);

                                if (isPrimaryNotNull.Count > 0)
                                {
                                    isDefaultColumn = true;
                                    //InsertSchemaPostgreSQL isp = new InsertSchemaPostgreSQL();
                                    //isp.db_column_name = isPrimaryNotNull[0].column_name;
                                    //isp.db_column_type = isPrimaryNotNull[0].data_type;
                                    //isp.dt_value = "default";
                                    //isp.dt_value_type = "string";
                                    //rowinserter.Add(isp);
                                }
                                else
                                {
                                    isDefaultColumn = false;
                                }

                                foreach (string c in cr)
                                {
                                    if (schems.Find(q => q.column_name.Trim().ToLower() == c.Trim().ToLower()) != null)
                                    {
                                        InsertSchemaPostgreSQL isp = new InsertSchemaPostgreSQL();
                                        isp.db_column_name = (schems.Find(q => q.column_name.Trim().ToLower() == c.Trim().ToLower())).column_name;
                                        isp.db_column_type = (schems.Find(q => q.column_name.Trim().ToLower() == c.Trim().ToLower())).data_type;
                                        isp.dt_value = fr[c].ToString();
                                        isp.dt_value_type = c;
                                        rowinserter.Add(isp);
                                    }
                                }

                                //if (schems.Find(q => q.column_name.Trim().ToLower() == "createby") != null)
                                //{
                                //    InsertSchemaPostgreSQL isp = new InsertSchemaPostgreSQL();
                                //    isp.db_column_name = schems.Find(q => q.column_name.Trim().ToLower() == "createby").column_name;
                                //    isp.db_column_type = schems.Find(q => q.column_name.Trim().ToLower() == "createby").data_type;
                                //    isp.dt_value = new VariableHandler().getVariables("ExecutedUser").vlvalue.ToString();
                                //    isp.dt_value_type = "string";
                                //    rowinserter.Add(isp);
                                //}

                                //if (schems.Find(q => q.column_name.Trim().ToLower() == "createdatetime") != null)
                                //{
                                //    InsertSchemaPostgreSQL isp = new InsertSchemaPostgreSQL();
                                //    isp.db_column_name = schems.Find(q => q.column_name.Trim().ToLower() == "createdatetime").column_name;
                                //    isp.db_column_type = schems.Find(q => q.column_name.Trim().ToLower() == "createdatetime").data_type;
                                //    isp.dt_value = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
                                //    isp.dt_value_type = "string";
                                //    rowinserter.Add(isp);
                                //}

                                //if (schems.Find(q => q.column_name.Trim().ToLower() == "updateby") != null)
                                //{
                                //    InsertSchemaPostgreSQL isp = new InsertSchemaPostgreSQL();
                                //    isp.db_column_name = schems.Find(q => q.column_name.Trim().ToLower() == "updateby").column_name;
                                //    isp.db_column_type = schems.Find(q => q.column_name.Trim().ToLower() == "updateby").data_type;
                                //    isp.dt_value = new VariableHandler().getVariables("ExecutedUser").vlvalue.ToString();
                                //    isp.dt_value_type = "string";
                                //    rowinserter.Add(isp);
                                //}

                                //if (schems.Find(q => q.column_name.Trim().ToLower() == "updatedatetime") != null)
                                //{
                                //    InsertSchemaPostgreSQL isp = new InsertSchemaPostgreSQL();
                                //    isp.db_column_name = schems.Find(q => q.column_name.Trim().ToLower() == "updatedatetime").column_name;
                                //    isp.db_column_type = schems.Find(q => q.column_name.Trim().ToLower() == "updatedatetime").data_type;
                                //    isp.dt_value = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
                                //    isp.dt_value_type = "string";
                                //    rowinserter.Add(isp);
                                //}

                                //if (schems.Find(q => q.column_name.Trim().ToLower() == "status") != null)
                                //{
                                //    InsertSchemaPostgreSQL isp = new InsertSchemaPostgreSQL();
                                //    isp.db_column_name = schems.Find(q => q.column_name.Trim().ToLower() == "status").column_name;
                                //    isp.db_column_type = schems.Find(q => q.column_name.Trim().ToLower() == "status").data_type;
                                //    isp.dt_value = "true";
                                //    isp.dt_value_type = "string";
                                //    rowinserter.Add(isp);
                                //}

                            }
                            inserter.Add(rowinserter);
                        }

                        List<string> returns = new List<string>();

                        foreach (List<InsertSchemaPostgreSQL> row in inserter)
                        {
                            string tablePart = "insert into " + tablename + "(";
                            string columnPart = string.Empty;
                            string valuesPart = ") values( ";
                            string columnValuesPart = string.Empty;
                            string returningPart = "Returning \"" + returning + "\"";
                            string queryPart = string.Empty;

                            foreach (InsertSchemaPostgreSQL cell in row)
                            {
                                if (cell.db_column_type.Trim().ToLower().IndexOf("int") > -1 || cell.db_column_type.Trim().ToLower().IndexOf("serial") > -1 || cell.db_column_type.Trim().ToLower().IndexOf("numeric") > -1 || cell.db_column_type.Trim().ToLower().IndexOf("decimal") > -1 || cell.db_column_type.Trim().ToLower().IndexOf("double") > -1 || cell.db_column_type.Trim().ToLower().IndexOf("bool") > -1 || cell.db_column_type.Trim().ToLower().IndexOf("boolean") > -1)
                                {
                                    columnPart = columnPart + "" + cell.db_column_name + ",";
                                    columnValuesPart = columnValuesPart + cell.dt_value + " ,";
                                }
                                else
                                {
                                    columnPart = columnPart + "" + cell.db_column_name + ",";
                                    columnValuesPart = columnValuesPart + "'" + cell.dt_value + "',";
                                }
                            }
                            if (columnPart.Trim() != string.Empty && columnValuesPart.Trim() != string.Empty)
                            {
                                columnPart = columnPart.Substring(0, columnPart.Length - 1);
                                columnValuesPart = columnValuesPart.TrimEnd(',');
                                if (returning != string.Empty)
                                {
                                    queryPart = tablePart + columnPart + valuesPart + columnValuesPart + ")" + returningPart + ";";
                                    returned = sql.DbExecuteQuerymySQL(queryPart, "createrobot", dbconn);
                                }
                                else
                                {
                                    queryPart = tablePart + columnPart + valuesPart + columnValuesPart + ");";
                                    returned = sql.DbExecuteQuerymySQL(queryPart, "insert", dbconn);
                                }

                                if (returned.Trim() == string.Empty)
                                {
                                    returned = "inserted";
                                }

                                returnValue.Columns.Add();
                                returnValue.Rows.Add(returned);
                            }
                        }
                    }
                        ret = StringHelper.success;
                    }
                }
            
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                new ExecutionLog().add2Log("**Please check the DB connection**", step.Robot_Id, step.Name, StringHelper.eventcode, false);
                new ExecutionLog().add2Log(ex.Message, step.Robot_Id, step.Name, StringHelper.eventcode, false);

            }

			MethodResult mr;

            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);

                if(returnValue.Rows.Count > 0)
                {
                    System.Data.DataView view = new System.Data.DataView(returnValue);
                    returnValueAlt = view.ToTable(false, "Column1");
                }

                string hetAlt = new TypeHandler().ConvertDataTableToString(returnValueAlt);
                //new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValueAlt), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
                new ExecutionLog().add2Log(hetAlt, step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
				new ExecutionLog().add2Log("**Invalid Database properites/Database Connection Please check it once**", step.Robot_Id, step.Name, StringHelper.eventcode, false);

			}

            return mr;

        }
        public MethodResult Update(Steps step, string executionID)
        {
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                DataTable returnValue = new DataTable();

                string connection = string.Empty; string tablename = string.Empty;
                string input = string.Empty; string inputtype = string.Empty; string output = string.Empty;
                string wherepart = string.Empty; string setpart = string.Empty;

                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
                connection = stp.getPropertiesValue(StringHelper.connection, step);
                tablename = stp.getPropertiesValue(StringHelper.tablename1S, step);
                wherepart = stp.getPropertiesValue(StringHelper.condition, step);
                setpart = stp.getPropertiesValue(StringHelper.set, step);

                DataTable dt = new DataTable();

                if (input != string.Empty && inputtype != string.Empty)
                {
                    dt = new TypeHandler().getInput(input, inputtype, executionID);
                    foreach (DataRow fr in dt.Rows)
                    {
                        tablename = fr["TableName"].ToString();
                        wherepart = fr["WherePart"].ToString().Trim ();
                        setpart = fr["SetPart"].ToString();
                    }
                }

                System.Data.DataTable dtconnect = new TypeHandler().getInput(connection, executionID);
                // System.Data.DataTable dtconnect = new TypeHandler().getInput(connection,"Variable");
                foreach (DataRow drt in dtconnect.Rows)
                {
                    string dbconn = drt["ConnectionString"].ToString();
                    string dbtype = drt["DataBaseServerType"].ToString();

                    if (dbtype.Trim().ToLower() == StringHelper.postgresql)
                    {
                        string schemaname = string.Empty;
                        if (tablename.IndexOf(".") > -1)
                        {
                            tablename = tablename.Split('.')[1];
                            schemaname = tablename.Split('.')[0];
                        }
                        else
                        {
                            schemaname = "public";
                        }

                        wherepart = wherepart.Replace(";", "||").Replace(":", "|");
                        //wherepart = wherepart.Replace(";", "||").Replace("\b:\b", "|");
                        setpart = setpart.Replace(";", "||").Replace(":", "|");

                        updateTableDataPostgreSQL(tablename, wherepart, setpart, schemaname, dbconn, executionID);
                    }
                    else if (dbtype.Trim().ToLower() == StringHelper.mysql)
                    {
                        wherepart = wherepart.Replace(";", "||").Replace(":", "|");
                        setpart = setpart.Replace(";", "||").Replace(":", "|");

                        List<TableSchemaPostgreSQL> schema = getTableSchemamySQL(tablename, dbconn);
                        try
                        {
                            List<string> wherevalues = wherepart.Split(new string[] { "||" }, StringSplitOptions.None).ToList();
                            List<string> setvalues = setpart.Split(new string[] { "||" }, StringSplitOptions.None).ToList();

                            string tablePart = tablename;
                            string openPart = "UPDATE " + tablePart + " SET ";
                            string wherePart = getSQLForFilterParamsMySQL(wherepart, schema, "and", executionID);
                            string setPart = getSQLForFilterParamsMySQL(setpart.Trim(), schema, ",", executionID);
                            string queryPart = openPart + setPart + " WHERE " + wherePart;
                            string result = sql.DbExecuteQuerymySQL(queryPart, "update", dbconn).ToString();
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                        }
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Delete(Steps step, string executionID)
        {
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                DataTable returnValue = new DataTable();

                string connection = string.Empty; string tablename = string.Empty;
                string input = string.Empty; string inputtype = string.Empty; string output = string.Empty;
                string wherepart = string.Empty; string isfulldelete = string.Empty; string deletepart = string.Empty;

                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
                connection = stp.getPropertiesValue(StringHelper.connection, step);
                tablename = stp.getPropertiesValue(StringHelper.tablename1S, step);
                wherepart = stp.getPropertiesValue(StringHelper.condition, step);
                isfulldelete = stp.getPropertiesValue(StringHelper.isfulldelete, step);
                deletepart = stp.getPropertiesValue(StringHelper.deletepart, step);

                DataTable dt = new DataTable();

                if (input != string.Empty && inputtype != string.Empty)
                {
                    dt = new TypeHandler().getInput(input, inputtype, executionID);
                    foreach (DataRow fr in dt.Rows)
                    {
                        tablename = fr["TableName"].ToString();
                        wherepart = fr["WherePart"].ToString();
                        deletepart = fr["DeletePart"].ToString();
                        isfulldelete = fr["IsFullDelete"].ToString();
                    }
                }
                System.Data.DataTable dtconnect = new TypeHandler().getInput(connection, executionID);
                // System.Data.DataTable dtconnect = new TypeHandler().getInput(connection,"Variable");
                foreach (DataRow drt in dtconnect.Rows)
                {
                    string dbconn = drt["ConnectionString"].ToString();
                    string dbtype = drt["DataBaseServerType"].ToString();

                    if (dbtype.Trim().ToLower() == StringHelper.postgresql)
                    {
                        string schemaname = string.Empty;
                        if (tablename.IndexOf(".") > -1)
                        {
                            tablename = tablename.Split('.')[1];
                            schemaname = tablename.Split('.')[0];
                        }
                        else
                        {
                            schemaname = "public";
                        }

                        wherepart = wherepart.Replace(";", "||").Replace(":", "|");

                        if (isfulldelete == "true")
                        {
                            List<TableSchemaPostgreSQL> schema = getTableSchemaPostgreSQL(tablename, dbconn);

                            string tablePart = "\"" + tablename + "\"";
                            string openPart = "DELETE FROM " + tablePart + "";
                            string wherePart = getSQLForFilterParamsPostgreSQL(wherepart, schema, "and", executionID);
                            string queryPart = openPart + " WHERE " + wherePart;
                            string result = db.DbExecuteQuery(queryPart, "delete", dbconn).ToString();
                        }
                        else
                        {
                            string setpart = deletepart.Replace(";", "||").Replace(":", "|");
                            updateTableDataPostgreSQL(tablename, wherepart, setpart, schemaname, dbconn, executionID);
                        }
                    }
                    else if (dbtype.Trim().ToLower() == StringHelper.mysql)
                    {
                        wherepart = wherepart.Replace(";", "||").Replace(":", "|");

                        if (isfulldelete == "true")
                        {
                            List<TableSchemaPostgreSQL> schema = getTableSchemamySQL(tablename, dbconn);
                            string tablePart = tablename;
                            string openPart = "DELETE FROM " + tablePart + "";
                            string wherePart = getSQLForFilterParamsMySQL(wherepart, schema, "and", executionID);
                            string queryPart = openPart + " WHERE " + wherePart;
                            string result = sql.DbExecuteQuerymySQL(queryPart, "delete", dbconn).ToString();
                        }
                        else
                        {
                            //string setpart = deletepart + "|" + "Equals" + "true";
                            string setpart = deletepart.Replace(";", "||").Replace(":", "|");
                            List<TableSchemaPostgreSQL> schema = getTableSchemamySQL(tablename, dbconn);
                            try
                            {
                                List<string> wherevalues = wherepart.Split(new string[] { "||" }, StringSplitOptions.None).ToList();
                                List<string> setvalues = setpart.Split(new string[] { "||" }, StringSplitOptions.None).ToList();

                                string tablePart = tablename;
                                string openPart = "UPDATE " + tablePart + " SET ";
                                string wherePart = getSQLForFilterParamsMySQL(wherepart, schema, "and", executionID);
                                string setPart = getSQLForFilterParamsMySQL(setpart, schema, ",", executionID);
                                string queryPart = openPart + setPart + " WHERE " + wherePart;
                                string result = sql.DbExecuteQuerymySQL(queryPart, "update", dbconn).ToString();
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                            }
                        }
                        ret = StringHelper.success;
                    }
                }

            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
				new ExecutionLog().add2Log(ex.Message, step.Robot_Id, step.Name, StringHelper.eventcode, false);
				new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
				new ExecutionLog().add2Log("**Invalid Database properites/Database Connection Please check it once**", step.Robot_Id, step.Name, StringHelper.eventcode, false);
			}
            return mr;
        }
        public string deleteQueryPostgreSQL(string tablename, string wheres, string schemaname, string connectionString,string ExecutionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string querypart = string.Empty;
            querypart = "delete from public.\"" + tablename + "\" where ";
            List<TableSchemaPostgreSQL> schema = getTableSchemaPostgreSQL(tablename, connectionString);
            try
            {
                List<string> wherevalues = wheres.Split(new string[] { "||" }, StringSplitOptions.None).ToList();
                string wherePart = getSQLForFilterParamsPostgreSQL(wheres, schema, "and", ExecutionID);

                querypart = querypart + wherePart;
                return db.DbExecuteQuery(querypart, "delete", connectionString);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return "";
            }
        }
        public string updateTableDataPostgreSQL(string tablename, string wheres, string sets, string schemaname, string connectionString,string executionID)
        {
            List<TableSchemaPostgreSQL> schema = getTableSchemaPostgreSQL(tablename, connectionString);
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (schemaname.Trim() == string.Empty)
                {
                    schemaname = "public";
                }

                List<string> wherevalues = wheres.Split(new string[] { "||" }, StringSplitOptions.None).ToList();
                List<string> setvalues = sets.Split(new string[] { "||" }, StringSplitOptions.None).ToList();

                string tablePart = schemaname + ".\"" + tablename + "\"";
                string openPart = "UPDATE " + tablePart + " SET ";
                string wherePart = getSQLForFilterParamsPostgreSQL(wheres, schema, "and", executionID);
                string setPart = getSQLForFilterParamsPostgreSQL(sets, schema, ",", executionID);
                string queryPart = string.Empty;
                if (wherePart.Trim() != string.Empty)
                {
                    queryPart = openPart + setPart + " WHERE " + wherePart;
                }
                else
                {
                    queryPart = openPart + setPart;
                }
                return db.DbExecuteQuery(queryPart, "update", connectionString);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return "";
            }
        }

        private string FormConnectionStringPostgreSQL(string dbtype, string dburl, string dbport, string dbusername, string dbpassword, string dbname)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string connectionstring = string.Empty;
            try
            {
                if (dbtype.ToLower().Contains(StringHelper.postgresql) || dbtype.Contains("3"))
                {
                    connectionstring = "Server=" + dburl + ";Port=" + dbport + ";User Id=" + dbusername + ";Password=" + dbpassword + ";Database=" + dbname + ";";
                }
                else if (dbtype.ToLower().Contains(StringHelper.mysql))
                {
                    if (dbname.ToLower() == "*")
                    {
                        dbname = StringHelper.mysql;
                    }

                    connectionstring = "Server=" + dburl + ";Port=" + dbport + ";Database=" + dbname + ";Uid=" + dbusername + ";Pwd=" + dbpassword + ";";

                    MySqlConnectionStringBuilder conn_string = new MySqlConnectionStringBuilder();
                    conn_string.Server = dburl;
                    conn_string.UserID = dbusername;
                    conn_string.Password = dbpassword;
                    conn_string.Database = dbname;
                    conn_string.Port = Convert.ToUInt32(dbport);
                }
                else
                {

                }
                return connectionstring;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return "";
            }
        }

        public static string getBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }

        private string getTableDataPostgreSQL(string table_name, string filter_params, string columns_to_display,string grpfields, int max_row_count, string schema_name, string sorting_params, string groupby, string connectionString, string executionID)
        {
            //check for columnnames is existing in DB
            List<TableSchemaPostgreSQL> schema = getTableSchemaPostgreSQL(table_name, connectionString);
            List<string> cols = columns_to_display.Split(new string[] { "," }, StringSplitOptions.None).ToList();
            List<string> cols1 = new List<string>();
            cols.RemoveAll(c => c.Trim() == string.Empty);
            foreach (string col in cols)
            {
                if(col.Contains("("))
                {
                    string cstr = getBetween(col, "(", ")");
                    cols1.Add("\"" + cstr + "\"");
                }
                else
                {
                    cols1.Add("\"" + col + "\"");
                }
            }
            try
            {

                if ((columns_to_display.Trim() != "*") && (columns_to_display.Trim() != string.Empty))
                {
                    
                    foreach (string col in cols1)
                    {
                        if (CheckColumnExistsPostgreSQL(col, schema))
                        {

                        }
                        else
                        {
                            return StringHelper.columnnotexists;
                        }
                    }
                }

                if (schema_name.Trim() == string.Empty)
                {
                    schema_name = "public";
                }


                string columnPart = getColumnsToDisplayPostgreSQL(columns_to_display);
                //if (grpfields != string.Empty)
                //    columnPart = grpfields + " , " + columnPart;
                string wherePart = getSQLForFilterParamsPostgreSQL(filter_params, schema, "and", executionID);
                string tablePart = schema_name + ".\"" + table_name + "\"";


                string orderPart = " 1 desc ";
                if (sorting_params.Trim() != string.Empty)
                {
                    orderPart = " " + sorting_params;
                }
                else
                {
                    orderPart = " 1";
                }
                //if (sorting_order.Trim() != string.Empty)
                //{
                //    orderPart = orderPart + " " + sorting_order;
                //}


                string queryPart = string.Empty;

               if(!groupby.Contains("\"")&& groupby != string.Empty)
                    groupby = "\"" + groupby + "\"";
                if (wherePart.Trim() != string.Empty)
                {
                    if(groupby!=string.Empty)
                        queryPart = columnPart + " from " + tablePart + " where " + wherePart + " Group by " + groupby + " order by" + orderPart;
                    else
                        queryPart = columnPart + " from " + tablePart + " where " + wherePart + " order by" + orderPart;
                    if (max_row_count > 0)
                        queryPart = "select " + queryPart + " LIMIT " + max_row_count;
                    else
                        queryPart = "select " + queryPart;
                }
                else
                {
                    if (groupby != string.Empty)
                        queryPart = columnPart + " from " + tablePart + " Group by " +  groupby + " order by" + orderPart;
                    else
                        queryPart = columnPart + " from " + tablePart + " order by" + orderPart;
                    if (max_row_count > 0)
                        queryPart = "select " + queryPart + " LIMIT " + max_row_count;
                    else
                        queryPart = "select " + queryPart;
                }


                string exitStatus = string.Empty;


                if (queryPart.Trim() != string.Empty)
                {
                    exitStatus = db.DbExecuteQuery(queryPart, "select", connectionString);
                }
                return exitStatus;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return null;
            }

        }

        private string getTableDataMySQL(string table_name, string filter_params, string columns_to_display, int max_row_count, string sorting_params, string sorting_order, string connectionString,string executionID)
        {
            //check for columnnames is existing in DB
            List<TableSchemaPostgreSQL> schema = getTableSchemamySQL(table_name, connectionString);
            List<string> cols = columns_to_display.Split(new string[] { "," }, StringSplitOptions.None).ToList();
            cols.RemoveAll(c => c.Trim() == string.Empty);
            try
            {
                if ((columns_to_display.Trim() != "*") && (columns_to_display.Trim() != string.Empty))
                {
                    foreach (string col in cols)
                    {
                        if (CheckColumnExistsPostgreSQL(col, schema))
                        {

                        }
                        else
                        {
                            return StringHelper.columnnotexists;
                        }
                    }
                }

                string columnPart = getColumnsToDisplayMySQLSQL(columns_to_display);
                string wherePart = getSQLForFilterParamsMySQL(filter_params, schema, "and", executionID);
                string tablePart = table_name;


                string orderPart = " 1 desc ";
                if (sorting_params.Trim() != string.Empty)
                {
                    orderPart = " " + sorting_params;
                }
                else
                {
                    orderPart = " 1";
                }
                if (sorting_order.Trim() != string.Empty)
                {
                    orderPart = orderPart + " " + sorting_order;
                }

                string queryPart = string.Empty;

                if (wherePart.Trim() != string.Empty)
                {
                    queryPart = columnPart + " from " + tablePart + " where " + wherePart + " order by" + orderPart;
                    if (max_row_count > 0)
                        queryPart = "select " + queryPart + " LIMIT " + max_row_count;
                    else
                        queryPart = "select " + queryPart;
                }
                else
                {
                    queryPart = columnPart + " from " + tablePart + " order by" + orderPart;
                    if (max_row_count > 0)
                        queryPart = "select " + queryPart + " LIMIT " + max_row_count;
                    else
                        queryPart = "select " + queryPart;
                }


                string exitStatus = string.Empty;


                if (queryPart.Trim() != string.Empty)
                {
                    exitStatus = sql.DbExecuteQuerymySQL(queryPart, "select", connectionString);
                }
                return exitStatus;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return null;
            }

        }
        public List<TableSchemaPostgreSQL> getTableSchemaPostgreSQL(string tablename, string connectionString)
        {
            string query = "select column_name,data_type from information_schema.columns where table_name = '" + tablename + "'; ";
            string ty = db.DbExecuteQuery(query, "select", connectionString);
            List<TableSchemaPostgreSQL> cols = JsonConvert.DeserializeObject<List<TableSchemaPostgreSQL>>(ty);
            return cols;
        }
        public List<TableSchemaPostgreSQL> getTableSchemamySQL(string tablename, string connectionString)
        {
            string query = "SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tablename + "'; ";
            string ty = sql.DbExecuteQuerymySQL(query, "select", connectionString);
            List<TableSchemaPostgreSQL> cols = JsonConvert.DeserializeObject<List<TableSchemaPostgreSQL>>(ty);
            return cols;
        }
        public List<TableSchemaPostgreSQL> getTablePrimaryKey(string tablename, string connectionString)
        {
            string query = "SELECT column_name,data_type FROM information_schema.columns WHERE  table_name = '" + tablename + "' and \"is_nullable\" = 'NO'";
            string ty = db.DbExecuteQuery(query, "select", connectionString);
            List<TableSchemaPostgreSQL> cols = JsonConvert.DeserializeObject<List<TableSchemaPostgreSQL>>(ty);
            return cols;
        }
        public List<TableSchemaPostgreSQL> getTablePrimaryKeyMySQL(string tablename, string connectionString)
        {
            string query = "  SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tablename + "' and is_nullable = 'NO';";
            string ty = sql.DbExecuteQuerymySQL(query, "select", connectionString);
            List<TableSchemaPostgreSQL> cols = JsonConvert.DeserializeObject<List<TableSchemaPostgreSQL>>(ty);
            return cols;
        }

        public bool CheckColumnExistsPostgreSQL(string column, List<TableSchemaPostgreSQL> schema)
        {
            try
            {
                column = column.Replace("\"", string.Empty).Trim();
                return schema.Any(c => c.column_name.Trim() == column.Trim());
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return true;
            }
        }
        public List<TableSchemaPostgreSQL> getColumnTypePostgreSQL(string column, List<TableSchemaPostgreSQL> schema)
        {
            return schema.FindAll(c => c.column_name.Trim() == column.Trim());
        }
        public string getColumnsToDisplayPostgreSQL(string columns_to_display)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (columns_to_display.Trim() == string.Empty || columns_to_display.Trim() == "*")
                {
                    return "*";
                }
                else
                {
                    if (columns_to_display.Substring(columns_to_display.Length - 1, 1) == ",")
                    {
                        columns_to_display = columns_to_display.Substring(0, columns_to_display.Length - 1);
                    }

                    List<string> colss = columns_to_display.Split(',').ToList();
                    List<string> cols2 = new List<string>();
                    foreach (string col in colss)
                    {

                        if (col.Contains('('))
                        {
                            string colstr = getBetween(col, "(", ")");
                            string strt = string.Empty; string end = string.Empty;
                            int cstart, cend;
                            cstart = col.IndexOf('(');
                            cend = col.IndexOf(')');
                            strt = col.Substring(0, cstart+1 );
                            end = col.Substring(cend);
                            cols2.Add(strt + "\"" + colstr + "\""+ end);
                        }
                        else
                            cols2.Add("\""+col+"\"");
                    }

                    return columns_to_display = string.Join(",", cols2);
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return "";
            }
        }
        public string getColumnsToDisplayMySQLSQL(string columns_to_display)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (columns_to_display.Trim() == string.Empty || columns_to_display.Trim() == "*")
                {
                    return "*";
                }
                else
                {
                    if (columns_to_display.Substring(columns_to_display.Length - 1, 1) == ",")
                    {
                        columns_to_display = columns_to_display.Substring(0, columns_to_display.Length - 1);
                    }

                    List<string> cols = columns_to_display.Split(',').ToList();
                    List<string> cols2 = new List<string>();
                    foreach (string col in cols)
                    {
                        cols2.Add("" + col + "");
                    }

                    return columns_to_display = string.Join(",", cols2);
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return "";
            }
        }
        public string getSQLForFilterParamsPostgreSQL(string filter_params, List<TableSchemaPostgreSQL> schema, string seperator, string ExecutionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<string> lineSeperators = new List<string>();
            List<string> conditionsList = new List<string>();
            try
            {
                if (filter_params.Contains("||"))
                {
                    lineSeperators = filter_params.Split(new string[] { "||" }, StringSplitOptions.None).ToList();
                }
                else
                {
                    lineSeperators.Add(filter_params);
                }

                lineSeperators.RemoveAll(ls => ls.Trim() == string.Empty);

                foreach (string lineSeperator in lineSeperators)
                {

                    List<string> items = lineSeperator.Split('|').ToList();
                    string op = getOperatorBasedonIDPostgreSQL(items[1]);
                    string operand1 = items[0]; //column
                    string operand2 = items[2];

                    //if (new TypeHandler().IsFormattedString(items[2]))
                    //{
                    //    operand2 = new TypeHandler().ConvertFormattedString(items[2], ExecutionID);//column condition
                    //    //if (new TypeHandler().IsValidJson(operand2))
                    //    //{
                    //    //    DataTable op2dt = JsonConvert.DeserializeObject<DataTable>(operand2);
                    //    //    int countCol = op2dt.Columns.Count;
                    //    //    operand2 = (op2dt.Rows[0][0]).ToString();
                    //    //}
                    //    DataTable dkey = new TypeHandler().getInput(operand2, ExecutionID);
                    //    int countCol = dkey.Columns.Count;
                    //    operand2 = (dkey.Rows[0][0]).ToString().Trim();
                    //}

                    string type = string.Empty; //column datatype

                    if (CheckColumnExistsPostgreSQL(operand1, schema))
                    {
                        List<TableSchemaPostgreSQL> gis = getColumnTypePostgreSQL(operand1, schema);
                        type = gis[0].data_type;
                        if (type.ToLower().IndexOf(StringHelper.character) > -1 || type.ToLower().IndexOf(StringHelper.timeperiod) > -1)
                        {
                            if(!operand2.Contains("Date"))
                            operand2 = "'" + operand2 + "'";
                        }                       

                    }
                    if (type.ToLower() == "text")
                    {
                        operand2 = "\'" + operand2 + "\'";
                    }
                    conditionsList.Add("\"" + operand1 + "\"" + op + operand2);
                }

                string joinedCondition = string.Join(" " + seperator + " ", conditionsList);

                joinedCondition = new StringFunction().getParameterizedString(joinedCondition, ExecutionID);

                return joinedCondition;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return "";
            }
        }
        public string getSQLForFilterParamsMySQL(string filter_params, List<TableSchemaPostgreSQL> schema, string seperator,string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<string> lineSeperators = new List<string>();
            List<string> conditionsList = new List<string>();
            try
            {
                if (filter_params.Contains("||"))
                {
                    lineSeperators = filter_params.Split(new string[] { "||" }, StringSplitOptions.None).ToList();
                }
                else
                {
                    lineSeperators.Add(filter_params);
                }

                lineSeperators.RemoveAll(ls => ls.Trim() == string.Empty);

                foreach (string lineSeperator in lineSeperators)
                {
                    List<string> items = lineSeperator.Split('|').ToList();
                    string op = getOperatorBasedonIDPostgreSQL(items[1]);
                    string operand1 = items[0]; //column
                    string operand2 = items[2];
                    if (new TypeHandler().IsFormattedString(items[2]))
                    {
                        operand2 = new TypeHandler().ConvertFormattedString(items[2], executionID);//column condition
                        if (new TypeHandler().IsValidJson(operand2))
                        {
                            DataTable op2dt = JsonConvert.DeserializeObject<DataTable>(operand2);
                            int countCol = op2dt.Columns.Count;
                            operand2 = (op2dt.Rows[0][0]).ToString();
                        }
                    }
                    string type = string.Empty; //column datatype

                    if (CheckColumnExistsPostgreSQL(operand1, schema))
                    {
                        List<TableSchemaPostgreSQL> gis = getColumnTypePostgreSQL(operand1, schema);
                        type = gis[0].data_type;
                        if (type.ToLower().IndexOf(StringHelper.varChar) > -1 || type.ToLower().IndexOf(StringHelper.timeperiod) > -1)
                        {
                            operand2 = "'" + operand2 + "'";
                        }
                    }
                    conditionsList.Add("" + operand1 + "" + op + operand2);
                }

                string joinedCondition = string.Join(" " + seperator + " ", conditionsList);


                return joinedCondition;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return "";
            }
        }
        public string getOperatorBasedonIDPostgreSQL(string operator_id)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string operatr = string.Empty;
            try
            {
                if (operator_id == "1" || operator_id.Trim().ToLower() == StringHelper.like)
                {
                    operatr = StringHelper.like;
                }
                else if ((operator_id == "2" || operator_id.Trim().ToLower() == StringHelper.equals || operator_id.Trim().ToLower() == "=="))
                {
                    operatr = "=";
                }
                else if (operator_id == "3" || operator_id.Trim().ToLower() == StringHelper.notequals || operator_id.Trim().ToLower() == "!=")
                {
                    operatr = "!=";
                }
                else if (operator_id == "4" || operator_id.Trim().ToLower() == StringHelper.lessthan || operator_id.Trim().ToLower() == "<")
                {
                    operatr = "<";
                }
                else if (operator_id == "5" || operator_id.Trim().ToLower() == StringHelper.greaterthan || operator_id.Trim().ToLower() == ">")
                {
                    operatr = ">";
                }
                else if (operator_id == "6" || operator_id.Trim().ToLower() == StringHelper.quottedin || operator_id.Trim().ToLower() == "in")
                {
                    operatr = " IN ";
                }
                else if (operator_id == "7" || operator_id.Trim().ToLower() == StringHelper.notin || operator_id.Trim().ToLower() == "not in")
                {
                    operatr = "NOT IN";
                }
                else if (operator_id == "8" || operator_id.Trim().ToLower() == StringHelper.lessthanorequalto || operator_id.Trim().ToLower() == "<=")
                {
                    operatr = "<=";
                }
                else if (operator_id == "9" || operator_id.Trim().ToLower() == StringHelper.greaterthanorequalto || operator_id.Trim().ToLower() == ">=")
                {
                    operatr = ">=";
                }
                else if (operator_id == "10" || operator_id.Trim().ToLower() == "like")
                {
                    operatr = "like";
                }
                return operatr;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return "";
            }
        }
        public bool canConvertType(string value, string type)
        {
            try
            {
                if (type.ToLower().IndexOf(StringHelper.boolean) > -1)
                {
                    bool parsedValue;
                    if (bool.TryParse(value, out parsedValue))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (type.ToLower().IndexOf(StringHelper.integer) > -1 || type.ToLower().IndexOf(StringHelper.serial) > -1 || type.ToLower().IndexOf(StringHelper.numeric) > -1)
                {
                    int parsedValue;
                    if (int.TryParse(value, out parsedValue))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (type.ToLower().IndexOf(StringHelper.decimalnumber) > -1 || type.ToLower().IndexOf(StringHelper.decimalnumber) > -1)
                {
                    double parsedValue;
                    if (double.TryParse(value, out parsedValue))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (type.ToLower().IndexOf(StringHelper.characters) > -1)
                {
                    char parsedValue;
                    if (char.TryParse(value, out parsedValue))
                    {
                        return true;
                    }
                    else
                    {
                        if (type.ToLower().IndexOf(StringHelper.character) > -1 || type.ToLower().IndexOf(StringHelper.text) > -1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else if (type.ToLower().IndexOf(StringHelper.timeperiod) > -1)
                {
                    DateTime parsedValue;
                    if (DateTime.TryParse(value, out parsedValue))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (type.ToLower().IndexOf(StringHelper.tdate) > -1)
                {
                    DateTime parsedValue;
                    if (DateTime.TryParse(value, out parsedValue))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (type.ToLower().IndexOf(StringHelper.characters) > -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return true;
            }
        }
        public MethodResult Query(Steps step, string executionID)
        {
            string ret = string.Empty;DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string input = string.Empty;
            string output = string.Empty;

            //for joins and complex subqueries

            try
            {
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                string query1 = stp.getPropertiesValue("query", step);
                string query = new StringFunction().getParameterizedString(query1, executionID);
                string connection = stp.getPropertiesValue(StringHelper.connection, step);
                string operationtype = stp.getPropertiesValue(StringHelper.operationtype, step);


                DataTable dtconnect = new TypeHandler().getInput(connection, executionID);

                foreach (DataRow drt in dtconnect.Rows)
                {
                    string dbconn = drt["ConnectionString"].ToString();
                    string dbtype = drt["DataBaseServerType"].ToString();


                    if (dbtype.Trim().ToLower() == StringHelper.postgresql)
                    {
                           query = query.Replace("`", "'").Trim();
                        string result = db.DbExecuteQuery(query, operationtype.ToLower(), dbconn);
                        returnValue = JsonConvert.DeserializeObject<DataTable>(result);
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execclient, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
    }
    public class TableSchemaPostgreSQL
    {
        public string column_name { get; set; }
        public string data_type { get; set; }
    }
    public class InsertSchemaPostgreSQL
    {
        public string db_column_name { get; set; }
        public string db_column_type { get; set; }
        public string dt_value { get; set; }
        public string dt_value_type { get; set; }
    }
}