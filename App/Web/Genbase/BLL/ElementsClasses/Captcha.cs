﻿using Genbase.classes;
using Genbase.Classes;
using System;
using System.Collections.Generic;

namespace Genbase.BLL.ElementsClasses
{
    public class Captcha
    {
        StepPropertyHandler stp = new StepPropertyHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult GetImage(Steps step, string executionID)
        {
            try
            {
                string ret = string.Empty;
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                string pageURL = string.Empty; string ElementXPath = string.Empty; string Output = string.Empty;

                pageURL = stp.getPropertiesValue(StringHelper.pageurl, step);
                ElementXPath = stp.getPropertiesValue(StringHelper.elementxpath, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);

                try
                {
                    ret = StringHelper.success;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                }
                MethodResult mr;
                if (ret == StringHelper.success)
                {
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                    mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                }
                else
                {
                    mr = new MethodResultHandler().createResultType(false, msgs, null, s);
                }
                return mr;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return null;
            }
        }
        public MethodResult GetText(Steps step, string executionID)
        {
            string ret = string.Empty;
            string Input = string.Empty; string Output = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);

                try
                {
                    ret = StringHelper.success;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                }
                MethodResult mr;
                if (ret == StringHelper.success)
                {
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                    mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                }
                else
                {
                    mr = new MethodResultHandler().createResultType(false, msgs, null, s);
                }
                return mr;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                return null;
            }
        }
    }
}