﻿using Genbase.BLL;
using Genbase.classes;
using HttpUtils;
using MimeKit;
using Newtonsoft.Json;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using Genbase.Classes;

namespace Genbase.BLL.ElementsClasses
{
    public class HTTP
    {
        StringFunction sf = new StringFunction();
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        VariableHandler vh = new VariableHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        // Deleted 
        public MethodResult ServiceAuthenticator( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string returns = string.Empty; string username = string.Empty; string headers = string.Empty;
            string password = string.Empty; string responseFormat = string.Empty; string requestFormat = string.Empty; string serviceurl = string.Empty;
            string authorizationtype = string.Empty; string authenticationtype = string.Empty; string input = string.Empty; string output = string.Empty;

            try
            {
                username = stp.getPropertiesValue(StringHelper.username, step);
                password = stp.getPropertiesValue(StringHelper.password, step);
                serviceurl = stp.getPropertiesValue(StringHelper.serviceurl, step);
                input = stp.getPropertiesValue(StringHelper.input, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                authenticationtype = stp.getPropertiesValue(StringHelper.authenticationtype, step);
                authorizationtype = stp.getPropertiesValue(StringHelper.authorizationtype, step);
                headers = stp.getPropertiesValue(StringHelper.headers, step);
                System.Data.DataTable returnValue = new System.Data.DataTable();
                returnValue.Columns.Add("UserName");
                returnValue.Columns.Add("Password");
                returnValue.Columns.Add("AuthenticationType");
                returnValue.Columns.Add("Token");
                returnValue.Columns.Add("Result");
                HttpWebRequest war = (HttpWebRequest)WebRequest.Create(new Uri(serviceurl));
                if (war != null)
                {
                    war.Method = StringHelper.get;
                    war.Timeout = 12000;
                    if (authenticationtype.ToLower().Trim() == "basic")
                    {

                        using (System.IO.Stream s = war.GetResponse().GetResponseStream())
                        {
                            using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                            {
                                string jsonResponse = sr.ReadToEnd();

                                returnValue.Rows.Add(username, password, "Basic", jsonResponse, jsonResponse);
                            }
                        }
                    }
                    else if (authenticationtype.ToLower().Trim() == "none")
                    {

                        using (System.IO.Stream s = war.GetResponse().GetResponseStream())
                        {
                            using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                            {
                                string jsonResponse = sr.ReadToEnd();

                                returnValue.Rows.Add(username, password, "None", jsonResponse, jsonResponse);
                            }
                        }
                    }
                    else if (authenticationtype.ToLower().Trim() == "digest")
                    {

                        var credentialCache = new CredentialCache();
                        credentialCache.Add(
                          new Uri(new Uri(serviceurl).GetLeftPart(UriPartial.Authority)), // request url's host
                          "Digest",  // authentication type 
                          new NetworkCredential("user", "password") // credentials 
                        );
                        war.Credentials = credentialCache;
                        war.Headers.Add(headers);

                        //DigestAuthFixer digest = new DigestAuthFixer(serviceurl, username, password);
                        //string strReturn = digest.GrabResponse(serviceurl);

                        using (System.IO.Stream s = war.GetResponse().GetResponseStream())
                        {
                            using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                            {
                                string jsonResponse = sr.ReadToEnd();

                                returnValue.Rows.Add(username, password, "Digest", jsonResponse, jsonResponse);
                            }
                        }
                    }
                    else if (authenticationtype.ToLower().Trim() == "ntlm")
                    {
                        war.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;
                        war.Credentials = new NetworkCredential(username, password, serviceurl);
                        war.Headers.Add(headers);

                        //war.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                        WebResponse resp = war.GetResponse();
                        StreamReader reader = new StreamReader(resp.GetResponseStream());
                        var token = reader.ReadToEnd().Trim();

                        returnValue.Rows.Add(username, password, "NTLM", token, token);
                    }
                    else if (authenticationtype.ToLower().Trim() == "kerberos")
                    {
                        war.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;
                        war.Credentials = new NetworkCredential(username, password, serviceurl);
                        war.Headers.Add(headers);

                        //war.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                        WebResponse resp = war.GetResponse();
                        StreamReader reader = new StreamReader(resp.GetResponseStream());
                        var token = reader.ReadToEnd().Trim();

                        returnValue.Rows.Add(username, password, "Kerberos", token, token);
                    }
                    else if (authenticationtype.ToLower().Trim() == "negotiate")
                    {
                        CredentialCache cc = new CredentialCache();
                        cc.Add(new Uri(serviceurl), "Negotiate", (
                                    (serviceurl == null || serviceurl == "") ?
                                    new System.Net.NetworkCredential(username, password) :
                                    new System.Net.NetworkCredential(username, password, serviceurl)));

                        war.Credentials = cc;
                        war.CookieContainer = new CookieContainer();
                        war.Headers.Add(headers);

                        using (System.IO.Stream s = war.GetResponse().GetResponseStream())
                        {
                            using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                            {
                                var jsonResponse = sr.ReadToEnd();

                                returnValue.Rows.Add(username, password, "Negotiate", jsonResponse, jsonResponse);
                            }
                        }
                    }
                    else if (authenticationtype.ToLower().Trim() == "windows live id")
                    {
                        war.Credentials = new NetworkCredential(username, password, serviceurl);
                        war.Headers.Add(headers);

                        //war.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                        WebResponse resp = war.GetResponse();
                        StreamReader reader = new StreamReader(resp.GetResponseStream());
                        var token = reader.ReadToEnd().Trim();

                        returnValue.Rows.Add(username, password, "Windows Live ID", token, token);
                    }
                    else
                    {
                        string usernamepassword = new EncoderDecoderBase64().Base64Encode(username + ":" + password);
                        war.Headers.Add(StringHelper.authorization, StringHelper.basic + usernamepassword);
                        war.Headers.Add(headers);

                        using (System.IO.Stream s = war.GetResponse().GetResponseStream())
                        {
                            using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                            {
                                string jsonResponse = sr.ReadToEnd();

                                returnValue.Rows.Add(username, password, "Basic", jsonResponse, jsonResponse);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        // Deleted 
        public MethodResult ServiceCall( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string returns = string.Empty; string operation = string.Empty; string operationtype = string.Empty; string serviceurl = string.Empty;
            string input = string.Empty; string output = string.Empty;
            string response = string.Empty; string request = string.Empty; string headers = string.Empty; string method = string.Empty;
            string parameters = string.Empty; string timeout = string.Empty;
            string token = string.Empty; string authenticationtype = string.Empty;
            string contenttype = string.Empty; string encodingformat = string.Empty;

            //operation = stp.getPropertiesValue(StringHelper.operation, step);
            //operationtype = stp.getPropertiesValue(StringHelper.operationtype, step);
            serviceurl = stp.getPropertiesValue(StringHelper.serviceurl, step);
            method = stp.getPropertiesValue(StringHelper.method, step);
            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            response = stp.getPropertiesValue(StringHelper.response, step);
            request = stp.getPropertiesValue(StringHelper.request, step);
            headers = stp.getPropertiesValue(StringHelper.headers, step);
            parameters = stp.getPropertiesValue(StringHelper.parameters, step);
            timeout = stp.getPropertiesValue(StringHelper.timeout, step);
            token = stp.getPropertiesValue(StringHelper.token, step);
            authenticationtype = stp.getPropertiesValue(StringHelper.authenticationtype, step);
            contenttype = stp.getPropertiesValue(StringHelper.contenttype, step);
            encodingformat = stp.getPropertiesValue(StringHelper.encodingformat, step);

            System.Data.DataTable returnValue = new System.Data.DataTable();
            returnValue.Columns.Add("ServiceURL");
            returnValue.Columns.Add("Parameters");
            returnValue.Columns.Add("AuthenticationType");
            returnValue.Columns.Add("Result");
            parameters = new TypeHandler().getInputJSON(parameters, executionID);
            try
            {
                string[] testing = Regex.Matches(parameters, @"\{(.+?)\}")
                            .Cast<Match>()
                            .Select(s => s.Groups[1].Value).ToArray();
                string tokken = string.Empty;

                if(parameters != string.Empty)
                {
                    parameters=new StringFunction().getParameterizedString(parameters, executionID);
                }


                if (token != string.Empty)
                {
                    if (vh.checkVariableExists(token, executionID))
                    {
                        System.Data.DataTable dtr = JsonConvert.DeserializeObject<System.Data.DataTable>(vh.getVariables(token, executionID).vlvalue);
                        string gtokken = dtr.Rows[0]["Result"].ToString();
                        Dictionary<string, string> dictr = JsonConvert.DeserializeObject<Dictionary<string, string>>(gtokken);
                        foreach (var kor in dictr)
                        {
                            if (kor.Key.ToString().IndexOf("token") > -1)
                            {
                                tokken = kor.Value.ToString();
                            }
                        }
                    }
                    else
                    {
                        tokken = token;
                    }
                }

                if (sf.isSubString(serviceurl, "?") && method.ToLower().Trim() == "get")
                {
                    if (parameters.Trim() != string.Empty)
                    {
                        if (parameters.Trim() == sf.splitString2List(serviceurl, "?")[1].Trim())
                        {
                            serviceurl = sf.splitString2List(serviceurl, "?")[0].Trim();
                        }
                        else
                        {
                            //todo

                            //parameters = string.Empty;
                            parameters = parameters;
                        }
                    }
                    else
                    {

                    }
                }
                else if (method.ToLower().Trim() == "get")
                {
                    if (parameters.Trim() != string.Empty)
                    {
                        if (sf.isSubString(parameters, "?"))
                        {

                        }
                        else
                        {
                            //todo
                            parameters = "?" + parameters;
                        }
                    }
                    else
                    {

                    }
                }
                else
                {

                }
                #region
                // Added By manikanta on 31st oct 2019:
                // Reason: In Post Call, from client side passing parameters are bind with URL i.e URL+ "?" +PassingParameters
                // simple Query String
                #endregion
                if (sf.isSubString(serviceurl, "?") && method.ToLower().Trim() == "post")
                {
                    string endPoint = @serviceurl + parameters;
                    var client = new RestClient(endpoint: endPoint,
                                method: HttpVerb.POST,
                                postData: null,
                                contentType: contenttype,
                                encodedFormat: encodingformat,
                                Headers: headers);
                    var json = client.MakeRequest();

                    returnValue.Rows.Add(serviceurl, parameters, authenticationtype, json);
                }
                else
                if (method.Trim().ToLower() == "post")
                {
                   
                    string endPoint = @serviceurl;
                    var client = new RestClient(endpoint: endPoint,
                                method: HttpVerb.POST,
                                postData: parameters,
                                contentType: contenttype,
                                encodedFormat: encodingformat,
                                Headers: headers);


                    var json = client.MakeRequest();
                    returnValue.Rows.Add(serviceurl, parameters, authenticationtype, json);
                }
                else if (method.Trim().ToLower() == "get")
                {
                    string endPoint = @serviceurl + parameters;
                    var client = new RestClient(endpoint: endPoint,
                                method: HttpVerb.GET,
                                postData: parameters,
                                contentType: contenttype,
                                encodedFormat: encodingformat,
                                Headers: headers);
                    var json = client.MakeRequest();

                    returnValue.Rows.Add(serviceurl, parameters, authenticationtype, json);
                }
                else if (method.Trim().ToLower() == "put")
                {
                    string endPoint = @serviceurl;
                    var client = new RestClient(endpoint: endPoint,
                                 method: HttpVerb.PUT,
                                 postData: parameters,
                                 contentType: contenttype,
                                 encodedFormat: encodingformat,
                                 Headers: headers);
                    var json = client.MakeRequest();
                    returnValue.Rows.Add(serviceurl, parameters, authenticationtype, json);
                }
                else if (method.Trim().ToLower() == "delete")
                {

                }
                
                else
                {

                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, "datatable", step.Robot_Id, executionID);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
    }
}
public enum HttpVerb
{
    GET,
    POST,
    PUT,
    DELETE,
    HEAD,
    CONNECT,
    OPTIONS,
    TRACE,
    PATCH
}

namespace HttpUtils
{
    public class RestClient
    {
        public string EndPoint { get; set; }
        public HttpVerb Method { get; set; }
        public string ContentType { get; set; }
        public string PostData { get; set; }
        public string EncodedFormat { get; set; }
        public string Header { get; set; }

        public RestClient()
        {
            EndPoint = "";
            Method = HttpVerb.GET;
            ContentType = "application/json";
            PostData = "";
        }
        public RestClient(string endpoint)
        {
            EndPoint = endpoint;
            Method = HttpVerb.GET;
            ContentType = "application/json";
            PostData = "";
        }
        public RestClient(string endpoint, HttpVerb method)
        {
            EndPoint = endpoint;
            Method = method;
            ContentType = "application/json";
            PostData = "";
        }

        public RestClient(string endpoint, HttpVerb method, string postData)
        {
            EndPoint = endpoint;
            Method = method;
            ContentType = "application/json";
            PostData = postData;
        }

        public RestClient(string endpoint, HttpVerb method, string postData, string contentType, string encodedFormat)
        {
            EndPoint = endpoint;
            Method = method;
            ContentType = contentType;
            PostData = postData;
            EncodedFormat = encodedFormat;
        }

        public RestClient(string endpoint, HttpVerb method, string postData, string contentType, string encodedFormat, string Headers)
        {
            EndPoint = endpoint;
            Method = method;
            ContentType = contentType;
            PostData = postData;
            EncodedFormat = encodedFormat;
            Header = Headers;
        }


        public string MakeRequest()
        {
            return MakeRequest("");
        }

        public string MakeRequest(string parameters)
        {

            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(EndPoint + parameters);

                request.Method = Method.ToString();
                request.ContentLength = 0;
                request.ContentType = ContentType;
                

                if (Header.Trim() != string.Empty)
                {
                    request.Headers.Add(Header);
                }

                if (!string.IsNullOrEmpty(PostData) && Method == HttpVerb.POST)
                {
                    var encoding = new UTF8Encoding();
                    //var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(PostData);
                    var bytes = Encoding.GetEncoding(EncodedFormat).GetBytes(PostData);
                    request.ContentLength = bytes.Length;

                    using (var writeStream = request.GetRequestStream())
                    {
                        writeStream.Write(bytes, 0, bytes.Length);
                    }
                }
                else
                {

                }

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseValue = string.Empty;

                    /*if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var message = String.Format("Request failed. Received HTTP {0}", response.StatusCode);
                        //throw new ApplicationException(message);
                    }*/

                    // grab the response
                    using (var responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                    }

                    return responseValue;
                }
            }
            catch (Exception ex)
             {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }

        }
    } // class
}