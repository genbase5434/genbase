﻿using System.Text;
using Genbase.classes;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System;
using Newtonsoft.Json;
using System.Drawing;
using Tesseract;
//using iTextSharp.text.pdf;
//using iTextSharp.text.pdf.parser;
using System.IO;
using Genbase.Classes;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using HttpUtils;
using System.Threading.Tasks;
using System.Collections.Specialized;

namespace Genbase.BLL.ElementsClasses
{
    public class OCR
    {
        StepPropertyHandler stp = new StepPropertyHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult Tesseract(Steps step, string executionID)

        {
            string ret = string.Empty; MethodResult mr = new MethodResult();
            DataTable resultDt = new DataTable();
            DataTable returnValue = new DataTable();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string Input = stp.getPropertiesValue(StringHelper.tesseractInput, step);
            string keyInput = stp.getPropertiesValue(StringHelper.keyinput, step);
            string output = stp.getPropertiesValue(StringHelper.output, step);
            DataTable Finalresult = new DataTable();
            string base64String = string.Empty;
            string Text = string.Empty;
            List<string> Values = new List<string>();

            try
            {
                DataTable dt = new TypeHandler().getInput(Input, StringHelper.variable, executionID);
                List<string> files = dt.AsEnumerable().Select(r => r.Field<string>(StringHelper.files)).ToList();

                foreach (string file in files)
                {
                    using (Image image = Image.FromFile(file))
                    {
                        using (MemoryStream m = new MemoryStream())
                        {
                            image.Save(m, image.RawFormat);
                            byte[] imageBytes = m.ToArray();
                            base64String = Convert.ToBase64String(imageBytes);
                        }
                    }
                    string filename = new StringFunction().getFileNameFromPath(file);
                    try
                    {
                        string endPoint = "http://99.148.66.155:5002/OCRKey?key=" + Uri.EscapeDataString(keyInput);
                        string textinimage = HttpUploadFile(endPoint, file, "file", "application/json", new NameValueCollection());
                        //bool key = string.IsNullOrEmpty(keyInput);
                        if (string.IsNullOrEmpty(keyInput))
                        {
                            returnValue.Columns.Add(StringHelper.Content);
                            var aa = JsonConvert.DeserializeObject<string>(textinimage);
                            aa = aa.Replace(Environment.NewLine, "<br />");
                            aa = aa.Replace("\r", "<br />");
                            aa = aa.Replace("\n", "<br />");
                            returnValue.Rows.Add(aa);
                        }
                        else
                        {
                            returnValue.Columns.Add("Token");
                            returnValue.Columns.Add("Response");
                            Dictionary<string, object> outputDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(textinimage);
                            string resultString = String.Empty;
                            foreach (var results in outputDict)
                            {
                                string sentence = String.Empty;
                                returnValue.Rows.Add(results.Key, results.Value.ToString());
                            }
                        }
                            bool isthere = false;
                            foreach (var l in Variables.dynamicvariables)
                            {
                                if ((l.vlname == output) && (l.vlstatus == StringHelper.truee))
                                {
                                    isthere = true;
                                    dt.Rows.Add(l.vlvalue);
                                    //count = 1;
                                    l.vlvalue = dt;
                                    l.ExecutionID = executionID;
                                    l.RobotID = step.Robot_Id;
                                }
                            }
                            if (!isthere)
                            {
                                DVariables d = new DVariables();
                                d.vlname = output;
                                d.vlstatus = StringHelper.truee;
                                d.vlvalue = dt;
                                d.vltype = StringHelper.datatable;
                                d.ExecutionID = executionID;
                                d.RobotID = step.Robot_Id;
                                Variables.dynamicvariables.Add(d);
                            }
                    }
                    catch (Exception ex)
                    {
                        string ro = ex.Message;
                    }
                    //                var filess = file;
                    //var uri = string.Format("http://99.148.66.155:5002/", filename);
                    //var client = new HttpClient();
                    //client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("image/pjpeg"));
                    //var content = new StreamContent(filess);
                    //var response = client.PostAsync(uri, content);


                    //    if (applyOCR(file).Trim().ToLower() == "pdf")
                    //    {
                    //        try
                    //        {
                    //            //string textinimage = PdfTextract.PdfTextExtractor.GetText(file);
                    //            //returnValue.Rows.Add(filename, textinimage);
                    //            //using (PdfReader reader = new PdfReader(file))
                    //            //{
                    //            //    StringBuilder textinimage = new StringBuilder();
                    //            //    for (int i = 1; i <= reader.NumberOfPages; i++)
                    //            //    {
                    //            //        textinimage.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                    //            //    }
                    //            //    returnValue.Rows.Add(filename, textinimage.ToString());
                    //            //    ret = StringHelper.success;
                    //            //}

                    //            string img_path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Uploads", System.IO.Path.GetFileNameWithoutExtension(filename) + ".png");

                    //            List<string> yImages = ConvertPDFTOneImage(file, img_path);


                    //            foreach(string yImage in yImages)
                    //            {
                    //                var img = new Bitmap(yImage);

                    //                string tesspath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tessdata");
                    //                using (TesseractEngine OCR = new TesseractEngine(tesspath, "eng", EngineMode.Default))
                    //                {
                    //                    var page = OCR.Process(Pix.LoadFromFile(file));
                    //                    string textinimage = page.GetText();
                    //                    returnValue.Rows.Add(filename, textinimage);
                    //                }
                    //                img.Dispose();
                    //            }
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            returnValue.Rows.Add(filename, "Exception: " + ex.Message);
                    //        }
                    //    }
                    //    else// if (applyOCR(file).Trim().ToLower() == "img")
                    //    {
                    //        try
                    //        {
                    //            var img = new Bitmap(file);


                    //            string tesspath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tessdata");

                    //            using (TesseractEngine OCR = new TesseractEngine(tesspath, "eng", EngineMode.TesseractAndCube))
                    //            {
                    //                var page = OCR.Process(Pix.LoadFromFile(file));
                    //                string textinimage = page.GetText();
                    //                returnValue.Rows.Add(filename, textinimage);

                    //            }
                    //            img.Dispose();
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            returnValue.Rows.Add(filename, "Exception: " + ex.Message);
                    //            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    //            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                    //        }
                    //    }
                    //    //else
                    //    //{

                    //    //}

                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                //returnValue.Rows.Add("");
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(Finalresult), "", true, StringHelper.datatable1, step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            if (ret == StringHelper.success)
            {

                //new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(Finalresult), "", true, StringHelper.datatable1);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, StringHelper.datatable1, step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public string HttpUploadFile(string url, string file, string paramName, string contentType,NameValueCollection nvc)
        {
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = System.Net.CredentialCache.DefaultCredentials;
            Stream rs = wr.GetRequestStream();
            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);
            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, file, contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);
            FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();
            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();
            WebResponse wresp = null;
            string result = string.Empty;
            try
            {
                wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                result = reader2.ReadToEnd();
            }
            catch (Exception ex)
            {
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                    result = ex.Message;
                }
            }
            finally
            {
                wr = null;
            }
            return result;
        }
        public static string Before(string value, string a)
        {
            int posA = value.IndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            return value.Substring(0, posA);
        }

        private string applyOCR(string fileName)
        {
            List<string> imgFormats = new string[] { "png", "jpg", "jpeg", "bmp", "tif", "tiff" }.ToList();
            List<string> pdfFormats = new string[] { "pdf" }.ToList();
            List<string> frameFormats = new string[] { "gif" }.ToList();
            List<string> complexFormats = new string[] { "svg", "ppm", "pgm", "pbm", "pnm", "psd", "wmf", "raw" }.ToList();

            string pather = System.IO.Path.GetExtension(fileName);

            if (imgFormats.FindAll(x => pather.ToLower().IndexOf(x.Trim().ToLower()) > -1).Count() > 0)
            {
                return "img";
            }
            else if (pdfFormats.FindAll(x => pather.ToLower().IndexOf(x.Trim().ToLower()) > -1).Count() > 0)
            {
                return "pdf";
            }
            else if (frameFormats.FindAll(x => pather.ToLower().IndexOf(x.Trim().ToLower()) > -1).Count() > 0)
            {
                return "frame";
            }
            else if (complexFormats.FindAll(x => pather.ToLower().IndexOf(x.Trim().ToLower()) > -1).Count() > 0)
            {
                return "complex";
            }
            else
            {
                return string.Empty;
            }
        }
        public List<string> ConvertPDFTOneImage(string pdf, string img)
        {
            SautinSoft.PdfFocus f = new SautinSoft.PdfFocus();

            List<string> imgs = new List<string>();

            f.OpenPdf(pdf);

            for (int fx = 1; fx <= f.PageCount; fx++)
            {
                try
                {
                    //Set image properties: Png, 200 dpi
                    //   f.ImageOptions.ImageFormat = System.Drawing.Imaging.ImageFormat.Png;
                    f.ImageOptions.ImageFormat = System.DrawingCore.Imaging.ImageFormat.Png;
                    f.ImageOptions.Dpi = 200;

                    img = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Uploads", System.IO.Path.GetFileNameWithoutExtension(img) + "~imgurl~" + fx + System.IO.Path.GetExtension(img));
                    //Save all PDF pages as page1.jpg, page2.jpg ... pageN.jpg

                    using (FileStream fs = System.IO.File.Create(img))
                    {

                    }

                    f.ToImage(img, fx);
                    imgs.Add(img);
                }
                catch (Exception ex)
                {
                    string exg = ex.Message;
                }
            }

            f.ClosePdf();
            return imgs;
        }

        #region File upload code
        public static class FormUpload
        {
            private static readonly Encoding encoding = Encoding.UTF8;
            public static HttpWebResponse MultipartFormDataPost(string postUrl, string userAgent, Dictionary<string, object> postParameters)
            {
                string formDataBoundary = String.Format("----------{0:N}", Guid.NewGuid());
                string contentType = "multipart/form-data; boundary=" + formDataBoundary;

                byte[] formData = GetMultipartFormData(postParameters, formDataBoundary);

                return PostForm(postUrl, userAgent, contentType, formData);
            }
            private static HttpWebResponse PostForm(string postUrl, string userAgent, string contentType, byte[] formData)
            {
                HttpWebRequest request = WebRequest.Create(postUrl) as HttpWebRequest;

                if (request == null)
                {
                    throw new NullReferenceException("request is not a http request");
                }

                // Set up the request properties.
                request.Method = "POST";
                request.ContentType = contentType;
                request.UserAgent = userAgent;
                request.CookieContainer = new CookieContainer();
                request.ContentLength = formData.Length;

                // You could add authentication here as well if needed:
                // request.PreAuthenticate = true;
                // request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;
                // request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("username" + ":" + "password")));

                // Send the form data to the request.
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(formData, 0, formData.Length);
                    requestStream.Close();
                }

                return request.GetResponse() as HttpWebResponse;
            }

            private static byte[] GetMultipartFormData(Dictionary<string, object> postParameters, string boundary)
            {
                Stream formDataStream = new System.IO.MemoryStream();
                bool needsCLRF = false;

                foreach (var param in postParameters)
                {
                    // Thanks to feedback from commenters, add a CRLF to allow multiple parameters to be added.
                    // Skip it on the first parameter, add it to subsequent parameters.
                    if (needsCLRF)
                        formDataStream.Write(encoding.GetBytes("\r\n"), 0, encoding.GetByteCount("\r\n"));

                    needsCLRF = true;

                    if (param.Value is FileParameter)
                    {
                        FileParameter fileToUpload = (FileParameter)param.Value;

                        // Add just the first part of this param, since we will write the file data directly to the Stream
                        string header = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n",
                            boundary,
                            param.Key,
                            fileToUpload.FileName ?? param.Key,
                            fileToUpload.ContentType ?? "application/json");

                        formDataStream.Write(encoding.GetBytes(header), 0, encoding.GetByteCount(header));

                        // Write the file data directly to the Stream, rather than serializing it to a string.
                        formDataStream.Write(fileToUpload.File, 0, fileToUpload.File.Length);
                    }
                    else
                    {
                        string postData = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}",
                            boundary,
                            param.Key,
                            param.Value);
                        formDataStream.Write(encoding.GetBytes(postData), 0, encoding.GetByteCount(postData));
                    }
                }

                // Add the end of the request.  Start with a newline
                string footer = "\r\n--" + boundary + "--\r\n";
                formDataStream.Write(encoding.GetBytes(footer), 0, encoding.GetByteCount(footer));

                // Dump the Stream into a byte[]
                formDataStream.Position = 0;
                byte[] formData = new byte[formDataStream.Length];
                formDataStream.Read(formData, 0, formData.Length);
                formDataStream.Close();

                return formData;
            }

            public class FileParameter
            {
                public byte[] File { get; set; }
                public string FileName { get; set; }
                public string ContentType { get; set; }
                public FileParameter(byte[] file) : this(file, null) { }
                public FileParameter(byte[] file, string filename) : this(file, filename, null) { }
                public FileParameter(byte[] file, string filename, string contenttype)
                {
                    File = file;
                    FileName = filename;
                    ContentType = contenttype;
                }
            }
        }
        #endregion
    }
}
namespace PdfTextract
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using PdfSharp.Pdf;
    using PdfSharp.Pdf.Content;
    using PdfSharp.Pdf.Content.Objects;
    using PdfSharp.Pdf.IO;

    public class PdfTextExtractor
    {
        public string GetText(string pdfFileName)
        {
            using (var _document = PdfReader.Open(pdfFileName, PdfDocumentOpenMode.ReadOnly))
            {
                var result = new StringBuilder();
                //foreach (var page in _document.Pages.OfType<PdfPage>())
                foreach (var page in _document.Pages)
                {
                    ExtractText(ContentReader.ReadContent(page), result);
                    result.AppendLine();
                }
                return result.ToString();
            }
        }

        #region CObject Visitor
        private void ExtractText(CObject obj, StringBuilder target)
        {
            if (obj is CArray)
                ExtractText((CArray)obj, target);
            else if (obj is CComment)
                ExtractText((CComment)obj, target);
            else if (obj is CInteger)
                ExtractText((CInteger)obj, target);
            else if (obj is CName)
                ExtractText((CName)obj, target);
            else if (obj is CNumber)
                ExtractText((CNumber)obj, target);
            else if (obj is COperator)
                ExtractText((COperator)obj, target);
            else if (obj is CReal)
                ExtractText((CReal)obj, target);
            else if (obj is CSequence)
                ExtractText((CSequence)obj, target);
            else if (obj is CString)
                ExtractText((CString)obj, target);
            else
                throw new NotImplementedException(obj.GetType().AssemblyQualifiedName);
        }

        private void ExtractText(CArray obj, StringBuilder target)
        {
            foreach (var element in obj)
            {
                ExtractText(element, target);
            }
        }
        private void ExtractText(CComment obj, StringBuilder target) { /* nothing */ }
        private void ExtractText(CInteger obj, StringBuilder target) { /* nothing */ }
        private void ExtractText(CName obj, StringBuilder target) { target.Append(obj.Name); }
        private void ExtractText(CNumber obj, StringBuilder target) { target.Append(obj.ToString()); }
        private void ExtractText(COperator obj, StringBuilder target)
        {
            if (obj.OpCode.OpCodeName == OpCodeName.Tj || obj.OpCode.OpCodeName == OpCodeName.TJ)
            {
                foreach (var element in obj.Operands)
                {
                    ExtractText(element, target);
                }
                target.Append(" ");
            }
        }
        private void ExtractText(CReal obj, StringBuilder target) { /* nothing */ target.Append(obj.Value); }
        private void ExtractText(CSequence obj, StringBuilder target)
        {
            foreach (var element in obj)
            {
                ExtractText(element, target);
            }
        }
        private void ExtractText(CString obj, StringBuilder target)
        {
            target.Append(obj.Value);
        }
        #endregion
    }
}