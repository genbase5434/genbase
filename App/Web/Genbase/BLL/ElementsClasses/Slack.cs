﻿using Genbase.classes;
using Newtonsoft.Json;
using Slack.Webhooks;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Genbase.Classes;
using System.Net;
using System.IO;

namespace Genbase.BLL.ElementsClasses
{
    public class SLACK
    {
        StepPropertyHandler stp = new StepPropertyHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult Login( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string hook = string.Empty; string input = string.Empty; string output = string.Empty;
            try
            {
                hook = stp.getPropertiesValue(StringHelper.hook, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                input = stp.getPropertiesValue(StringHelper.input, step);

                string tar = @hook;
                var url = tar;
                var slackClient = new SlackClient(url);
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log("Slack login is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Invite( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string inputtype = string.Empty; string input = string.Empty; string output = string.Empty;
            string invitetoken = string.Empty; string channels = string.Empty; string email = string.Empty;
            try
            {
                inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                input = stp.getPropertiesValue(StringHelper.input, step);
                invitetoken = stp.getPropertiesValue(StringHelper.invitetoken, step);
                channels = stp.getPropertiesValue(StringHelper.channels, step);
                email = stp.getPropertiesValue(StringHelper.email, step);

                //if (invitetoken != string.Empty && email != string.Empty)
                //{
                //    sende("https://slack.com/api/users.admin.invite?token=" + invitetoken + "&email=" + email + "&channels=#" + channels, "");
                //}

                string accessToken = string.Empty;
                string result = string.Empty;
                HttpClient client = null;
                HttpRequestMessage httpRequestMessage = null;
                HttpResponseMessage httpResponseMessage = null;
                string requesrUrl = "https://slack.com/api/users.admin.invite?";
                var values = new Dictionary<string, string>
                {
                    { "token", invitetoken},
                    { "email", email},
                    { "channels", "#" + channels}
                };
                var content = new FormUrlEncodedContent(values);
                client = new HttpClient();
                httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, new Uri(requesrUrl));
                client.BaseAddress = new Uri(requesrUrl);
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded");
                httpRequestMessage.Content = content;
                httpResponseMessage = client.SendAsync(httpRequestMessage).Result;
                result = httpResponseMessage.Content.ReadAsStringAsync().Result;
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log("Slack invite is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public async Task<bool> sende(string invitteurl)
        {
            try
            {
                var slackClient = new SlackClienter(new Uri(invitteurl));
                while (true)
                {
                    var message = StringHelper.hi;
                    var response = await slackClient.SendMessageAsync(message);
                    var isValid = response.IsSuccessStatusCode ? "valid" : "invalid";
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return true;
            }

        }
        public bool sende(string invitteurl, string h)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                var slackClient = new SlackClienter(new Uri(invitteurl));
                var message = StringHelper.hi;
                var response = slackClient.SendMessage(message);
                return true;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return true;
            }
        }
        public MethodResult Send( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string to = string.Empty; string channel = string.Empty;
            string hook = string.Empty; string input = string.Empty; string output = string.Empty; string message = string.Empty; string emoji = string.Empty;
            try
            {
                hook = stp.getPropertiesValue(StringHelper.hook, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                input = stp.getPropertiesValue(StringHelper.input, step);
                message = stp.getPropertiesValue(StringHelper.message, step);
                emoji = stp.getPropertiesValue(StringHelper.emoji, step);
                channel = stp.getPropertiesValue(StringHelper.channel, step);
                var url = hook;
                var slackClient = new SlackClient(url);
                var slackMessage = new SlackMessage
                {
                    Channel = "#" + channel,
                    Text = message,
                    IconEmoji = Emoji.Alien,
                    Username = StringHelper.vstrym
                };
                slackClient.Post(slackMessage);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log("Slack send is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public class SlackClienter
        {
            StatusModel s = new StatusModel();
            private readonly Uri _webhookUrl;
            private readonly HttpClient _httpClient = new HttpClient();

            public SlackClienter(Uri webhookUrl)
            {
                _webhookUrl = webhookUrl;
            }

            public async Task<HttpResponseMessage> SendMessageAsync(string message, string channel = null, string username = null)
            {
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    var payload = new
                    {
                        text = message,
                        channel,
                        username
                    };
                    var serializedPayload = JsonConvert.SerializeObject(payload);
                    var response = await _httpClient.PostAsync(_webhookUrl,
                    new StringContent(serializedPayload, Encoding.UTF8, StringHelper.applicationjson));

                    return response;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    return null;
                }
            }
            public string SendMessage(string message, string channel = null, string username = null)
            {
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    var webRequest = System.Net.WebRequest.Create(_webhookUrl); string returns = string.Empty;

                    SlackMessageSend payload = new SlackMessageSend();
                    payload.text = message;
                    payload.username = username;
                    payload.channel = channel;

                    string postData = Newtonsoft.Json.JsonConvert.SerializeObject(payload);
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                    if (webRequest != null)
                    {
                        webRequest.Method = StringHelper.post;
                        webRequest.Timeout = 12000;
                        webRequest.ContentType = StringHelper.applicationjson;
                        webRequest.Headers.Add(StringHelper.authorization, "Basic " + Variables.usernamepassword);
                        webRequest.ContentLength = byteArray.Length;

                        System.IO.Stream dataStream = webRequest.GetRequestStream();
                        // Write the data to the request stream.
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        // Close the Stream object.
                        dataStream.Close();

                        using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                        {
                            using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                            {
                                var jsonResponse = sr.ReadToEnd();
                                returns = jsonResponse;
                            }
                        }
                    }
                    return returns;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    return "";
                }
            }
        }
        public MethodResult Delete( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string inputtype = string.Empty; string input = string.Empty; string output = string.Empty;
            string invitetoken = string.Empty; string channels = string.Empty; string email = string.Empty;
            try
            {
                inputtype = stp.getPropertiesValue(StringHelper.inputtype, step);
                output = stp.getPropertiesValue(StringHelper.output, step);
                input = stp.getPropertiesValue(StringHelper.input, step);
                invitetoken = stp.getPropertiesValue(StringHelper.invitetoken, step);
                channels = stp.getPropertiesValue(StringHelper.channels, step);
                email = stp.getPropertiesValue(StringHelper.email, step);

                if (invitetoken != string.Empty && email != string.Empty)
                {
                    sende("https://slack.com/api/users.admin.invite?token=" + invitetoken + "&email=" + email + "&channels=#" + channels, "");
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log("Slack delete is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
    }
    public class SlackMessageSend
    {
        public string text { get; set; }
        public string channel { get; set; }
        public string username { get; set; }
    }
}