﻿using Genbase.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Data;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml;
using System.IO;
using System.Net;
using Genbase.Classes;

namespace Genbase.BLL.ElementsClasses
{
    public class File
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        VariableHandler vh = new VariableHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult Upload( Steps step, string executionID)
        {
            string ret = string.Empty;
            string Output = string.Empty; string isFolder = string.Empty; string input = string.Empty;
            DataTable tablu = new DataTable(); tablu.Columns.Add("Files");
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Output = stp.getPropertiesValue(StringHelper.output, step);
                input = stp.getPropertiesValue(StringHelper.input, step);
                isFolder = stp.getPropertiesValue(StringHelper.IsFolderUpload, step);

                log.add2Log(Messages.stepExecutionStarted, step.Robot_Id, step.Name, StringHelper.eventcode,false);
                if (Variables.uploads != null && Variables.uploads.Count > 0)
                {
                    if (isFolder.Trim().ToLower() == StringHelper.truee)
                    {
                        //foreach (string dir in Variables.uploads)
                        //{
                        //    DirectoryInfo d = new DirectoryInfo(dir);
                        //    FileInfo[] Files = d.GetFiles();
                        //    foreach (FileInfo file in Files)
                        //    {
                        //        tablu.Rows.Add(file.Name);
                          //}

                        foreach (string dir in Variables.uploads)
                        {
                            List<string> ser = DirSearch(dir);
                            ser.ForEach(se => tablu.Rows.Add(se));
                        }

                    }
                    else
                    {
                        foreach (string file in Variables.uploads)
                        {
                            log.add2Log(file + StringHelper.hasbeenuploaded, step.Robot_Id, step.Name, StringHelper.eventcode,false);
                            tablu.Rows.Add(file);
                        }
                    }
                }
                else if (input.Trim() != string.Empty)
                {
                    if (vh.checkVariableExists(input, executionID))
                    {
                        DataTable inpt = new DataTable();
                        inpt = new TypeHandler().getInput(input, executionID);

                        foreach (DataRow dr in inpt.Rows)
                        {
                            string file = dr["Files"].ToString();
                            tablu.Rows.Add(file);
                        }
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                //string het = new TypeHandler().ConvertDataTableToString(tablu);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(tablu), "", true, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                //new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }


        public MethodResult Download( Steps step, string executionID)
        {
            string ret = string.Empty;
            string Output = string.Empty; string Input = string.Empty; string SaveTo = string.Empty; string DownloadIn = String.Empty;
            DataTable returnValue = new DataTable();

            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                SaveTo = stp.getPropertiesValue(StringHelper.SaveTo, step);
                DownloadIn = stp.getPropertiesValue(StringHelper.DownloadIn, step);
                DataTable inpt = new DataTable();
                List<string> DWNLDS = new List<string>();
                DVariables dvs = new DVariables();
                if (DownloadIn == "Local")
                {
                    if (vh.checkVariableExists(Input.Trim(), executionID))
                    {
                        dvs = vh.getVariables(Input, executionID);
                        inpt = new TypeHandler().getInput(Input, StringHelper.variable, executionID);
                        DWNLDS.AddRange(inpt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList());
                    }
                    else
                    {
                        DWNLDS.Add(Input);
                    }
                }
                else if (DownloadIn == "Server")
                {
                    List<string> SDURLS = new List<string>();
                    try
                    {
                        string ItemfromArray = new TypeHandler().getItemfromArray(Input, executionID);
                        try
                        {
                            SDURLS.AddRange(JsonConvert.DeserializeObject<List<string>>(ItemfromArray));
                        }
                        catch (Exception e)
                        {
                            string ex = e.Message;
                            SDURLS.Add(ItemfromArray);
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                        }
                        //string filepath = @System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "GenFiles\\";
                        string filepath = Path.Combine(Directory.GetCurrentDirectory(), "GenFiles\\");
                        SDURLS.ForEach(url =>
                        {
                            string filename = "";
                            try
                            {

                                filename = DateTime.Now.ToString("ddMMyyyyhhmmss") + url.Split('/')[url.Split('/').Length - 1];
                                using (WebClient client = new WebClient())
                                {
                                    client.DownloadFile(url, @filepath + filename);
                                }
                                DWNLDS.Add(filepath + filename);
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                try
                                {
                                    filename = "\\" + url.Split('/')[url.Split('/').Length - 1];
                                    using (WebClient client = new WebClient())
                                    {
                                        client.DownloadFileAsync(new System.Uri(url), @filepath + filename);
                                    }
                                    DWNLDS.Add(filepath + filename);
                                }

                                catch (Exception e)
                                {
                                    new Logger().LogException(e);
                                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                    s = new StatusModel(StringHelper.exceptioncode, e.Message, Method, StringHelper.falsee);
                                }

                            }
                        });
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                        s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                    }
                }
                returnValue.Columns.Add("Files");
                DWNLDS.ForEach(dwn => returnValue.Rows.Add(dwn.ToString()));
                Variables.downloads.Clear();
                Variables.downloads.AddRange(DWNLDS);
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", false, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Excel( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string Input = string.Empty; string Output = string.Empty;
            string InputType = string.Empty; string Cells = string.Empty;
            string filename = string.Empty; string sheetname = string.Empty;
            string Task = string.Empty;

            DataTable returnValue = new DataTable();

            try
            {
                filename = stp.getPropertiesValue(StringHelper.filename, step);
                sheetname = stp.getPropertiesValue(StringHelper.sheetname, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                InputType = stp.getPropertiesValue(StringHelper.inputtype, step);
                Cells = stp.getPropertiesValue(StringHelper.cells, step);
                Task = stp.getPropertiesValue(StringHelper.Task, step);

                System.Data.DataTable dt = new TypeHandler().getInput(Input, InputType, executionID);

                if (Task.ToLower() == StringHelper.read)
                {
                    List<string> files = dt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList();
                    if (Cells.Trim() == StringHelper.WildCardStar)
                    {
                        foreach (string file in files)
                        {
                            returnValue.Merge(ReadAsDataTable(file, false, sheetname));
                        }
                    }
                    else if (Cells.Trim() != string.Empty)
                    {
                        foreach (string file in files)
                        {
                            if (Cells.IndexOf(":") > -1)
                            {
                                string Cells1 = Cells.Split(':')[0];
                                string Cells2 = Cells.Split(':')[1];

                                string fromrow = string.Empty;
                                string torow = string.Empty;
                                string fromcolumn = string.Empty;
                                string tocolumn = string.Empty;

                                if (Cells1.Length == 2 && Cells2.Length == 2)
                                {
                                    fromrow = Cells1.Select(c => c.ToString()).ToArray()[1];
                                    fromcolumn = Cells1.Select(c => c.ToString()).ToArray()[0];
                                    torow = Cells2.Select(c => c.ToString()).ToArray()[1];
                                    tocolumn = Cells2.Select(c => c.ToString()).ToArray()[0];

                                    returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, sheetname));
                                }
                                else if (Cells1.Length > 2 && Cells2.Length == 2)
                                {
                                    StringBuilder rows = new StringBuilder(); StringBuilder columns = new StringBuilder();
                                    foreach (string cells1 in Cells1.Select(c => c.ToString()).ToArray())
                                    {
                                        int bog = 0;
                                        if (int.TryParse(cells1, out bog))
                                        {
                                            rows.Append(bog);
                                        }
                                        else
                                        {
                                            columns.Append(cells1);
                                        }
                                    }

                                    fromrow = rows.ToString();
                                    fromcolumn = columns.ToString();
                                    torow = Cells2.Select(c => c.ToString()).ToArray()[1];
                                    tocolumn = Cells2.Select(c => c.ToString()).ToArray()[0];

                                    returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, sheetname));
                                }
                                else if (Cells1.Length == 2 && Cells2.Length > 2)
                                {
                                    StringBuilder rows = new StringBuilder(); StringBuilder columns = new StringBuilder();
                                    foreach (string cells2 in Cells2.Select(c => c.ToString()).ToArray())
                                    {
                                        int bog = 0;
                                        if (int.TryParse(cells2, out bog))
                                        {
                                            rows.Append(bog);
                                        }
                                        else
                                        {
                                            columns.Append(cells2);
                                        }
                                    }

                                    fromrow = Cells1.Select(c => c.ToString()).ToArray()[1];
                                    fromcolumn = Cells1.Select(c => c.ToString()).ToArray()[0];
                                    torow = rows.ToString();
                                    tocolumn = columns.ToString();

                                    returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, sheetname));
                                }
                                else if (Cells1.Length > 2 && Cells2.Length > 2)
                                {
                                    StringBuilder rows1 = new StringBuilder(); StringBuilder columns1 = new StringBuilder();
                                    foreach (string cells1 in Cells1.Select(c => c.ToString()).ToArray())
                                    {
                                        int bog = 0;
                                        if (int.TryParse(cells1, out bog))
                                        {
                                            rows1.Append(bog);
                                        }
                                        else
                                        {
                                            columns1.Append(cells1);
                                        }
                                    }


                                    StringBuilder rows2 = new StringBuilder(); StringBuilder columns2 = new StringBuilder();
                                    foreach (string cells2 in Cells2.Select(c => c.ToString()).ToArray())
                                    {
                                        int bog = 0;
                                        if (int.TryParse(cells2, out bog))
                                        {
                                            rows2.Append(bog);
                                        }
                                        else
                                        {
                                            columns2.Append(cells2);
                                        }
                                    }

                                    fromrow = rows1.ToString();
                                    fromcolumn = columns1.ToString();
                                    torow = rows2.ToString();
                                    tocolumn = columns2.ToString();

                                    returnValue.Merge(RangeData(fromrow, fromcolumn, torow, tocolumn, file, sheetname));
                                }
                                else
                                {

                                }
                            }
                            else
                            {

                            }
                        }
                    }
                    ret = StringHelper.success;
                }
                else if (Task.ToLower() == StringHelper.write)
                {
                    DataSet ds = new DataSet();
                    DataTable tablu = new DataTable();
                    tablu.Columns.Add("Files");

                    //string homePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                    string homePath = Directory.GetCurrentDirectory();
                    string filesFolder = homePath + StringHelper.files;

                    ds.Tables.Add(dt);
                    string filesPath = filesFolder + "\\" + filename + DateTime.Now.ToString(StringHelper.mdyhms) + ".xls";
                    if (ExportDataSet(ds, filesPath))
                    {
                        tablu.Rows.Add(filesPath);
                    }
                    returnValue = tablu;
                    ret = StringHelper.success;
                }
                else if (Task.ToLower() == StringHelper.update)
                {
                    DataTable udt = new DataTable();

                    DataTable tablu = new DataTable();
                    tablu.Columns.Add("Files");

                    if (Cells.IndexOf(":") > -1)
                    {
                        udt = new TypeHandler().getInput(Cells, "Value", executionID);
                    }
                    else
                    {
                        udt = new TypeHandler().getInput(Cells, "Variable", executionID);
                    }

                    if (udt.Columns.Count > 0)
                    {
                        // string homePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                        string homePath = Directory.GetCurrentDirectory();
                        string filesFolder = homePath + StringHelper.files;

                        foreach (DataRow dtr in dt.Rows)
                        {
                            filename = dtr["Files"].ToString();
                            string filesPath = filename;
                            if (udt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in udt.Rows)
                                {
                                    if (dr[0] != null && dr[1] != null)
                                    {
                                        var udtaddress = dr[0].ToString();
                                        var udtdata = dr[1].ToString();

                                        StringBuilder rows = new StringBuilder(); StringBuilder columns = new StringBuilder();
                                        foreach (string cells1 in udtaddress.Select(c => c.ToString()).ToArray())
                                        {
                                            int bog = 0;
                                            if (int.TryParse(cells1, out bog))
                                            {
                                                rows.Append(bog);
                                            }
                                            else
                                            {
                                                columns.Append(cells1);
                                            }
                                        }

                                        UpdateCell(filesPath, udtdata, Convert.ToUInt32(int.Parse(rows.ToString())), columns.ToString());
                                    }
                                }
                            }
                            tablu.Rows.Add(filesPath);
                        }
                    }

                    returnValue = tablu;
                    ret = StringHelper.success;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }

            MethodResult mr;
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(returnValue);
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(returnValue), "", false, "datatable", step.Robot_Id, executionID);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }


        public DataTable ReadAsDataTable(string fileName, bool allowHeader, string SHEETNAME)
        {
            DataTable dataTable = new DataTable();
            try
            {
                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(fileName, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    List<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().ToList();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        dataTable.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                    }

                    foreach (Row row in rows)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                        {
                            try
                            {
                                if (i < dataTable.Columns.Count)
                                {
                                    dataRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                                }
                            }
                            catch (Exception ex)
                            {
                                string ec = ex.Message;
                                dataRow[i] = string.Empty;

                            }
                        }
                        dataTable.Rows.Add(dataRow);
                    }
                }

                dataTable.Rows.RemoveAt(0);
                return dataTable;
            }
            catch (Exception ex)
            {
                string ec = ex.Message;
                return dataTable;
            }
        }
        private string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            if (cell != null && cell.CellValue != null)
            {
                try
                {
                    string value = cell.CellValue.InnerXml;
                    if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                    {
                        return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                    }
                    else
                    {
                        return value;
                    }
                }
                catch (Exception ex)
                {
                    string ec = ex.Message;
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }
        public DataTable RangeData(string RowStart, string ColStart, string RowEnd, string ColEnd, string filename, string sheetname)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            DataTable dataTable = new DataTable();
            try
            {
                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filename, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    List<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().ToList();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    List<Row> rows = sheetData.Descendants<Row>().ToList();

                    uint firstRowNum = GetRowIndex(ColStart + RowStart);
                    uint lastRowNum = GetRowIndex(ColEnd + RowEnd);
                    string firstColumn = GetColumnName(ColStart + RowStart);
                    string lastColumn = GetColumnName(ColEnd + RowEnd);

                    int u = 0;

                    foreach (Row row in sheetData.Elements<Row>().Where(r => r.RowIndex.Value >= firstRowNum && r.RowIndex.Value <= lastRowNum))
                    {
                        List<string> rowData = new List<string>();
                        foreach (Cell cell in row)
                        {
                            string columnName = GetColumnName(cell.CellReference.Value);
                            if (CompareColumn(columnName, firstColumn) >= 0 && CompareColumn(columnName, lastColumn) <= 0)
                            {
                                if (rowsAndColumnscellval(workbookPart, cell) != null)
                                {
                                    rowData.Add(rowsAndColumnscellval(workbookPart, cell));
                                }
                                else
                                {
                                    rowData.Add(string.Empty);
                                }
                                if (u == 0)
                                {
                                    var colList = cell.CellReference.Value.ToCharArray().ToList();

                                    string numPart = string.Empty; string letPart = string.Empty;

                                    foreach (char r in colList)
                                    {
                                        int tre = 0;
                                        if (int.TryParse(r.ToString(), out tre))
                                        {
                                            numPart = numPart + r.ToString();
                                        }
                                        else
                                        {
                                            letPart = letPart + r.ToString();
                                        }
                                    }
                                    var headerCell = GetCell(workSheet, letPart, 1);
                                    var headerCellAlt = GetCell(workSheet, letPart, 2);
                                    var headerCellAltr = GetCell(workSheet, letPart, 3);

                                    string headerCellVal = GetCellValue(spreadSheetDocument, headerCell);
                                    string headerCellValAlt = GetCellValue(spreadSheetDocument, headerCell);

                                    if (headerCellVal.Trim() != string.Empty)
                                    {
                                        dataTable.Columns.Add(headerCellVal);
                                    }
                                    else
                                    {
                                        dataTable.Columns.Add(headerCellValAlt);
                                    }
                                }
                            }
                        }

                        dataTable.Rows.Add(rowData.ToArray());
                        u++;
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
            }
            return dataTable;
        }


        /// <summary>
        /// Given a cell name, parses the specified cell to get the row index.
        /// </summary>
        /// <param name="cellName"></param>
        /// <returns></returns>
        private static uint GetRowIndex(string cellName)
        {
            // Create a regular expression to match the row index portion the cell name.
            Regex regex = new Regex(@"\d+");
            Match match = regex.Match(cellName);

            return uint.Parse(match.Value);
        }
        /// <summary>
        ///  Given a cell name, parses the specified cell to get the column name.
        /// </summary>
        /// <param name="cellName"></param>
        /// <returns></returns>
        private static string GetColumnName(string cellName)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellName);

            return match.Value;
        }
        /// <summary>
        /// Given two columns, compares the columns.
        /// </summary>
        /// <param name="column1"></param>
        /// <param name="column2"></param>
        /// <returns></returns>
        private static int CompareColumn(string column1, string column2)
        {
            if (column1.Length > column2.Length)
            {
                return 1;
            }
            else if (column1.Length < column2.Length)
            {
                return -1;
            }
            else
            {
                return string.Compare(column1, column2, true);
            }
        }

        public static string rowsAndColumnscellval(WorkbookPart wb, Cell c)
        {
            StatusModel s = new StatusModel();
            string value = c.InnerText;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (c.DataType != null)
                {
                    switch (c.DataType.Value)
                    {
                        case CellValues.SharedString:
                            var stringTable = wb.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                            if (stringTable != null)
                            {
                                value =
                                    stringTable.SharedStringTable
                                    .ElementAt(int.Parse(value)).InnerText;
                            }
                            break;

                        case CellValues.Boolean:
                            switch (value)
                            {
                                case "0":
                                    value = "FALSE";
                                    break;
                                default:
                                    value = "TRUE";
                                    break;
                            }
                            break;
                        default:
                            value = "";
                            break;
                    }
                }
                return value;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return ex.Message;
            }
        }
        public bool ExportDataSet(DataSet ds, string destination)
        {
            try
            {
                using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
                {
                    var workbookPart = workbook.AddWorkbookPart();
                    workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();
                    workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

                    uint sheetId = 1;

                    foreach (System.Data.DataTable table in ds.Tables)
                    {
                        var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                        var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                        sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                        DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                        string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                        if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                        {
                            sheetId =
                                sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                        }

                        DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                        sheets.Append(sheet);

                        DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                        List<String> columns = new List<string>();
                        foreach (DataColumn column in table.Columns)
                        {
                            columns.Add(column.ColumnName);

                            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                            headerRow.AppendChild(cell);
                        }

                        sheetData.AppendChild(headerRow);
                        int rowCount = 0;
                        foreach (DataRow dsrow in table.Rows)
                        {
                            if (rowCount >= 1)
                            {
                                DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                                foreach (String col in columns)
                                {
                                    DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString()); //
                                    newRow.AppendChild(cell);
                                }
                                sheetData.AppendChild(newRow);
                            }
                            rowCount++;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return false;
            }
        }


        public static void UpdateCell(string docName, string text, uint rowIndex, string columnName)
        {
            // Open the document for editing.
            StatusModel s = new StatusModel();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Open(docName, true))
                {
                    WorksheetPart worksheetPart = GetWorksheetPartByName(spreadSheet, "Sheet1");

                    if (worksheetPart != null)
                    {
                        Cell cell = GetCell(worksheetPart.Worksheet, columnName, rowIndex);

                        cell.CellValue = new CellValue(text);
                        cell.DataType = new EnumValue<CellValues>(CellValues.Number);

                        // Save the worksheet.
                        worksheetPart.Worksheet.Save();
                    }
                }
            }

            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
            }
        }

        private static WorksheetPart GetWorksheetPartByName(SpreadsheetDocument document, string sheetName)
        {
            IEnumerable<Sheet> sheets = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().Where(s => s.Name == sheetName);

            if (sheets.Count() == 0)
            {
                // The specified worksheet does not exist.

                return null;
            }

            string relationshipId = sheets.First().Id.Value;
            WorksheetPart worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(relationshipId);
            return worksheetPart;

        }

        // Given a worksheet, a column name, and a row index, 
        // gets the cell at the specified column and 
        private static Cell GetCell(Worksheet worksheet, string columnName, uint rowIndex)
        {
            Row row = GetRow(worksheet, rowIndex);

            if (row == null)
                return null;

            return row.Elements<Cell>().Where(c => string.Compare(c.CellReference.Value, columnName + rowIndex, true) == 0).First();
        }


        // Given a worksheet and a row index, return the row.
        private static Row GetRow(Worksheet worksheet, uint rowIndex)
        {
            return worksheet.GetFirstChild<SheetData>().
              Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
        }
        public MethodResult Read( Steps step, string executionID)
        {
            MethodResult mr = new MethodResult(); DataTable output = new DataTable();
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string filename = string.Empty; string Input = string.Empty; string Output = string.Empty;

                string folderPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, StringHelper.uploads);

                output.Columns.Add("Files");
                output.Columns.Add("Text");

                filename = stp.getPropertiesValue(StringHelper.filename, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);

                DataTable inpt = new DataTable(); List<string> uplds = new List<string>();
                DVariables dvs = new DVariables();
                if (vh.checkVariableExists(Input, executionID))
                {
                    dvs = vh.getVariables(Input, executionID);
                    inpt = new TypeHandler().getInput(Input, StringHelper.variable, executionID);
                    uplds.AddRange(inpt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList());
                }
                else
                {
                    uplds.Add(Input);
                }

                try
                {
                    Variables.readOutput.Clear();
                    foreach (string ups in uplds)
                    {
                        string totaltext = string.Empty;
                        using (StreamReader sr = new StreamReader(ups))
                        {
                            totaltext += sr.ReadToEnd();
                            output.Rows.Add(ups, totaltext);
                            sr.Close();
                            sr.Dispose();
                        }
                        Variables.readOutput.Add(totaltext);
                        totaltext = string.Empty;
                    }

                    foreach (string file in Variables.uploads)
                    {
                        if (System.IO.File.Exists(file))
                        {
                            System.IO.File.Delete(file);
                        }
                    }

                    if (Output != string.Empty && Variables.readOutput.Count > 0)
                    {
                        new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(output), "", false, "datatable", step.Robot_Id, executionID);
                    }
                    ret = StringHelper.success;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(output);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Rename( Steps step, string executionID)
        {
            MethodResult mr = new MethodResult(); DataTable output = new DataTable();
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string Input = string.Empty; string Output = string.Empty; string Filename = string.Empty;
            try
            {

                output.Columns.Add("Files");

                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);
                Filename = stp.getPropertiesValue(StringHelper.filename, step);

                if (vh.checkVariableExists(Input, executionID))
                {
                    DataTable inpt = new DataTable();
                    inpt = new TypeHandler().getInput(Input, executionID);

                    if (inpt.Columns.Count > 0 && inpt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in inpt.Rows)
                        {
                            string oldfilename = dr[0].ToString();
                            if (Directory.Exists(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, StringHelper.uploads)))
                            {
                                string newfilename = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, StringHelper.uploads, Filename);
                                System.IO.File.Move(oldfilename, newfilename);
                                output.Rows.Add(newfilename);
                            }
                            else
                            {
                                string newfilename = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), StringHelper.uploads, Filename);
                                System.IO.File.Move(oldfilename, newfilename);
                                output.Rows.Add(newfilename);
                            }
                        }
                    }
                }
                else
                {
                    string oldfilename = Input.Trim();
                    string newfilename = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, StringHelper.uploads, Filename);
                    System.IO.File.Move(oldfilename, newfilename);
                    output.Rows.Add(newfilename);
                }

                ret = StringHelper.success;


            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            if (ret == StringHelper.success)
            {
                new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(output), "", true, "datatable", step.Robot_Id, executionID);
                string het = new TypeHandler().ConvertDataTableToString(output);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode,false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        private List<string> DirSearch(string sDir)
        {
            List<string> files = new List<string>();
            try
            {
                foreach (string f in Directory.GetFiles(sDir))
                {
                    files.Add(f);
                }
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    files.AddRange(DirSearch(d));
                }
            }
            catch (Exception excpt)
            {
                string ex = excpt.Message;
            }

            return files;
        }
    }
}