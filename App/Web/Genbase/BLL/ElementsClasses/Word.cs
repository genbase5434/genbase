﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Genbase.classes;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using Genbase.Classes;
using System.IO;
using OfficeOpenXml;

namespace Genbase.BLL.ElementsClasses
{
    public class Word
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        VariableHandler varh = new VariableHandler();
        StatusModel s = new StatusModel();
        VariableHandler vh = new VariableHandler();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();

        public MethodResult CreateFile(Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string ret = string.Empty;
            string URL = string.Empty; string UserName = string.Empty; string Password = string.Empty; string Operation = string.Empty;
            string Input = string.Empty; string Output = string.Empty; string pdfPath = string.Empty;
            DataTable otpt = new DataTable();
            try
            {
                URL = stp.getPropertiesValue(StringHelper.url, step);
                UserName = stp.getPropertiesValue(StringHelper.username, step);
                Password = stp.getPropertiesValue(StringHelper.password, step);

                Operation = stp.getPropertiesValue(StringHelper.operation, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);

                System.Data.DataTable inpt = new System.Data.DataTable();

                otpt.Columns.Add("desc");
                otpt.Columns.Add("slackusername");
                otpt.Columns.Add("slackdpassword");
                otpt.Columns.Add("hrmsusername");
                otpt.Columns.Add("hrmsdpassword");
                otpt.Columns.Add("mailid");
                otpt.Columns.Add("mailpassword");
                otpt.Columns.Add("name");

                List<Dictionary<string, dynamic>> inputArray1 = new List<Dictionary<string, dynamic>>();
                List<string> orcas = new List<string>();

                if (vh.checkVariableExists(Input.Trim(), executionID))
                {
                    inpt = new TypeHandler().getInput(Input, StringHelper.variable, executionID);

                    inputArray1 = inpt.AsEnumerable().Select(row => inpt.Columns.Cast<DataColumn>().ToDictionary(column => column.ColumnName, column => row[column] as dynamic)).ToList();
                }

                //foreach (DataRow dr in inpt.Rows)
                {
                    string firstname = string.Empty;
                    string name = string.Empty; string lastname = string.Empty; string emailid = string.Empty;
                    string emailpswd = string.Empty; string slackid = string.Empty; string slackpswd = string.Empty;
                    string adname = string.Empty; string username = string.Empty; string orca = string.Empty;
                    xUser xu = new xUser(); string cellPhone = string.Empty; string empAddress = string.Empty;
                    string companyTitle = string.Empty; string afterFirstComma = string.Empty;

                    foreach (var inputData in inputArray1)
                    {
                        if (inputData.Values.Contains("Preferred Professional Name"))
                        {
                            //var empName = inputData["Information"];
                            var empName = inputData["Column3"];
                            string[] names = empName.Split(' ');

                            firstname = names[0];
                            lastname = names[1];
                            xu.EmployeeName = empName;
                        }
                        else if (inputData.Values.Contains("Title"))
                        {
                            companyTitle = inputData["Column3"];
                        }
                        else if (inputData.Values.Contains("Cell Phone"))
                        {
                            cellPhone = inputData["Column3"];
                        }
                        else if (inputData.Values.Contains("Employee Home Address"))
                        {
                            empAddress = inputData["Column3"];
                        }
                    }

                    name = firstname.Trim() + " " + lastname.Trim();
                    var regexItem = new Regex("^[a-zA-Z0-9 ].*$");

                    if (name.Trim() != string.Empty && regexItem.IsMatch(name))
                    {
                        if (name.Trim().IndexOf(" ") > -1)
                        {
                            string fmail;
                            string lmail;
                            char[] seperator = { ' ' };
                            String[] fnames = firstname.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                            String[] lnames = lastname.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
                            if (fnames.Length >= 2)
                            {
                                fmail = fnames[0] + fnames[1];
                            }
                            else
                            {
                                fmail = firstname;
                            }
                            if (lnames.Length >= 2)
                            {
                                lmail = lnames[0] + lnames[1];
                            }
                            else
                            {
                                lmail = lastname;
                            }

                            username = fmail.ToLower() + '.' + lmail.ToLower();
                            emailid = username + "@genbase.onmicrosoft.com";
                            emailpswd = username + "@genbase.onmicrosoft.com";
                            Variables.xusername = username;
                            //xUser xu = new xUser();
                            xu.mailid = emailid;
                            xu.sapuserid = fmail.ToUpper() + lmail.ToUpper().ToString().Substring(0, 1);
                            if (Variables.xuser.Count != 0)
                            {
                                Variables.xuser.Add(xu);
                            }
                            else
                            {
                                Variables.xuser.Add(xu);
                            }
                        }
                        else
                        {
                            username = name;
                            Variables.xusername = username;
                            //xUser xu = new xUser();
                            xu.adusername = username;
                            Variables.xuser.Add(xu);
                        }

                        try
                        {
                            string folderPath = Path.Combine(Directory.GetCurrentDirectory(), "TempDocuments" + "\\");

                            if (!Directory.Exists(folderPath))
                            {
                                Directory.CreateDirectory(folderPath);
                            }

                            //name = "John Wesley";
                            //empAddress = "Software Engineer";
                            //emailid = "john.wesley@onmicrosoft.com";
                            //cellPhone = "(654)657-8932";
                            string[] strAddress = empAddress.Split(", ");

                            if (empAddress.IndexOf(',') > 0)
                            {
                                afterFirstComma = empAddress.Split(new char[] { ',' }, 2)[1].Trim();
                            }

                            string Mainpath = folderPath + name + " Signature.docx";
                            using (FileStream stream = System.IO.File.Create(Mainpath))
                            {
                                // Add some text to file    
                                Byte[] title = new UTF8Encoding(true).GetBytes( name + " | Consultant");
                                stream.Write(title, 0, title.Length);

                                byte[] author = new UTF8Encoding(true).GetBytes("\n" + "Ph:" + cellPhone);
                                stream.Write(author, 0, author.Length);
                                byte[] author1 = new UTF8Encoding(true).GetBytes("\n\n" + companyTitle);
                                stream.Write(author1, 0, author1.Length);
                                byte[] author2 = new UTF8Encoding(true).GetBytes("\n" + strAddress[0]);
                                stream.Write(author2, 0, author2.Length);
                                byte[] author3 = new UTF8Encoding(true).GetBytes("\n" + afterFirstComma);
                                stream.Write(author3, 0, author3.Length);
                            }

                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                    }
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                vh.CreateVariable(Output, JsonConvert.SerializeObject(otpt), "", true, "datatable", step.Robot_Id, executionID);
                new ExecutionLog().add2Log("Create File is being proccessed", step.Robot_Id, step.Name, StringHelper.eventcode, false);

            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult ReadFile(Steps step, string executionID)
        {
            MethodResult mr = new MethodResult(); DataTable output = new DataTable();
            string ret = string.Empty; List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string filename = string.Empty; string Input = string.Empty; string Output = string.Empty;

                string folderPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, StringHelper.uploads);

                output.Columns.Add("Files");
                output.Columns.Add("Text");

                filename = stp.getPropertiesValue(StringHelper.filename, step);
                Input = stp.getPropertiesValue(StringHelper.input, step);
                Output = stp.getPropertiesValue(StringHelper.output, step);

                DataTable inpt = new DataTable(); List<string> uplds = new List<string>();
                DVariables dvs = new DVariables();
                if (vh.checkVariableExists(Input, executionID))
                {
                    dvs = vh.getVariables(Input, executionID);
                    inpt = new TypeHandler().getInput(Input, StringHelper.variable, executionID);
                    uplds.AddRange(inpt.AsEnumerable().Select(r => r.Field<string>("Files")).ToList());
                }
                else
                {
                    uplds.Add(Input);
                }

                try
                {
                    Variables.readOutput.Clear();
                    foreach (string ups in uplds)
                    {
                        string totaltext = string.Empty;
                        using (StreamReader sr = new StreamReader(ups))
                        {
                            totaltext += sr.ReadToEnd();
                            output.Rows.Add(ups, totaltext);
                            sr.Close();
                            sr.Dispose();
                        }
                        Variables.readOutput.Add(totaltext);
                        totaltext = string.Empty;
                    }

                    foreach (string file in Variables.uploads)
                    {
                        if (System.IO.File.Exists(file))
                        {
                            System.IO.File.Delete(file);
                        }
                    }

                    if (Output != string.Empty && Variables.readOutput.Count > 0)
                    {
                        new VariableHandler().CreateVariable(Output, JsonConvert.SerializeObject(output), "", false, "datatable", step.Robot_Id, executionID);
                    }
                    ret = StringHelper.success;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            if (ret == StringHelper.success)
            {
                string het = new TypeHandler().ConvertDataTableToString(output);
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new ExecutionLog().add2Log(het, step.Robot_Id, step.Name, StringHelper.eventcode, false);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public bool ExportDataSet(DataSet ds, string destination)
        {
            try
            {
                using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
                {
                    var workbookPart = workbook.AddWorkbookPart();
                    workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();
                    workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

                    uint sheetId = 1;

                    foreach (System.Data.DataTable table in ds.Tables)
                    {
                        var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                        var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                        sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                        DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                        string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                        if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                        {
                            sheetId =
                                sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                        }

                        DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                        sheets.Append(sheet);

                        DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                        List<String> columns = new List<string>();
                        foreach (DataColumn column in table.Columns)
                        {
                            columns.Add(column.ColumnName);

                            DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                            headerRow.AppendChild(cell);
                        }

                        sheetData.AppendChild(headerRow);
                        //int rowCount = 0;
                        foreach (DataRow dsrow in table.Rows)
                        {
                            //if (rowCount >= 1)
                            //{
                            DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                            foreach (String col in columns)
                            {
                                DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString()); //
                                newRow.AppendChild(cell);
                            }
                            sheetData.AppendChild(newRow);
                            //}
                            //rowCount++;
                        }
                    }

                    workbook.Close();
                    workbook.Dispose();
                }
                return true;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                return false;
            }
        }
    }    
}
