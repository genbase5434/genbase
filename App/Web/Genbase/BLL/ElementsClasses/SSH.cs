﻿using Genbase.classes;
using MySql.Data.MySqlClient;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json;
using Genbase.Classes;

namespace Genbase.BLL.ElementsClasses
{
    public class SSH
    {
        ExecutionLog log = new ExecutionLog();
        StepPropertyHandler stp = new StepPropertyHandler();
        StatusModel s = new StatusModel();
        List<KeyValuePair<string, string>> Method = new List<KeyValuePair<string, string>>();
        public MethodResult VMLogin( Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>(); string ret = string.Empty;

            string ipname = string.Empty; string port = string.Empty; string username = string.Empty; string password = string.Empty;
            string input = string.Empty; string output = string.Empty;

            DataTable returnValue = new DataTable();

            returnValue.Columns.Add(StringHelper.IP);
            returnValue.Columns.Add(StringHelper.Port);
            returnValue.Columns.Add(StringHelper.UserName);
            returnValue.Columns.Add(StringHelper.Password);

            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);

            if (input.Trim() == string.Empty)
            {
                ipname = stp.getPropertiesValue(StringHelper.ipaddress, step);
                port = stp.getPropertiesValue(StringHelper.portnumber, step);
                username = stp.getPropertiesValue(StringHelper.username, step);
                password = stp.getPropertiesValue(StringHelper.password, step);
            }
            else
            {
                DataTable dtr = new DataTable();
                dtr = new TypeHandler().getInput(input, StringHelper.variable, executionID);

                ipname = dtr.Rows[0][StringHelper.IP].ToString();
                port = dtr.Rows[0][StringHelper.Port].ToString();
                username = dtr.Rows[0][StringHelper.UserName].ToString();
                password = dtr.Rows[0][StringHelper.Password].ToString();
            }

            try
            {
                // using (var ssh = new SshClient(new MethodResultHandler().CreateConnection(ipname, int.Parse(port), username, password)))
                using (var ssh = new SshClient(ipname, int.Parse(port), username, password))
                {
                    ssh.Connect();
                    returnValue.Rows.Add(ipname, port, username, password);
                    ssh.Disconnect();
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                ret = StringHelper.ffalse;
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, StringHelper.datatablesmall, step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult Create( Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>(); string ret = string.Empty;

            string input = string.Empty; string output = string.Empty; DataTable returnValue = new DataTable();
            string createtype = string.Empty; string createname = string.Empty; string notes = string.Empty;

            createtype = stp.getPropertiesValue(StringHelper.createtype, step);
            createname = stp.getPropertiesValue(StringHelper.createname, step);
            notes = stp.getPropertiesValue(StringHelper.notes, step);
            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);

            returnValue.Columns.Add(StringHelper.server);
            returnValue.Columns.Add(StringHelper.Create);
            returnValue.Columns.Add(StringHelper.namee);

            try
            {

                DataTable dtr = new DataTable();
                dtr = new TypeHandler().getInput(input, StringHelper.variable, executionID);

                string ipname = dtr.Rows[0][StringHelper.IP].ToString();
                string port = dtr.Rows[0][StringHelper.Port].ToString();
                string username = dtr.Rows[0][StringHelper.UserName].ToString();
                string password = dtr.Rows[0][StringHelper.Password].ToString();

                // using (var client = new SshClient(new MethodResultHandler().CreateConnection(ipname, int.Parse(port), username, password)))
                using (var client = new SshClient(ipname, int.Parse(port),username,password))
                {
                    if (createtype.ToLower() == StringHelper.users)
                    {
                        client.Connect();
                        if (client.IsConnected)
                        {
                            try
                            {
                                /*ShellStream shellStream = client.CreateShellStream(string.Empty, 0, 0, 0, 0, 0);
                                shellStream.WriteLine("useradd " + createname);
                                shellStream.WriteLine("passwd " + createname);
                                //shellStream.Write("\\u19");
                                string outer = string.Empty;

                                while (true)
                                {
                                    shellStream.WriteLine(createname);
                                    shellStream.WriteLine(createname);
                                    outer += shellStream.Read();
                                    break;
                                }*/

                                var command = client.CreateCommand("useradd " + createname + " -s /bin/false");
                                command.Execute();
                                command = client.CreateCommand("echo '" + createname + ":" + createname + "' >> create.txt");
                                command.Execute();
                                command = client.CreateCommand("chpasswd < create.txt");
                                command.Execute();
                                command = client.CreateCommand("rm create.txt");
                                command.Execute();
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                            }
                        }
                        returnValue.Rows.Add(ipname, createtype, createname);
                        client.Disconnect();
                    }
                    else if (createtype.ToLower() == StringHelper.group)
                    {
                        client.Connect();
                        if (client.IsConnected)
                        {
                            try
                            {
                                SshCommand ssc = client.CreateCommand("groupadd " + createname);
                                ssc.Execute();
                                string answer = ssc.Result;
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                            }
                            returnValue.Rows.Add(ipname, createtype, createname);
                            client.Disconnect();
                        }
                        
                    }
                    else if (createtype.ToLower() == StringHelper.file)
                    {
                        client.Connect();
                        if (client.IsConnected)
                        {
                            try
                            {
                                SshCommand ssc = client.CreateCommand("cat > " + createname + " \\n This Text File is Created by User: " + username + " using SSH and C# \\n" + notes + "\x003");
                                ssc.Execute();
                                string answer = ssc.Result;
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                            }
                        }
                        returnValue.Rows.Add(ipname, createtype, createname);
                        client.Disconnect();
                    }
                    else if (createtype.ToLower() == StringHelper.folder)
                    {
                        client.Connect();
                        if (client.IsConnected)
                        {
                            try
                            {
                                SshCommand ssc = client.CreateCommand("mkdir " + createname);
                                ssc.Execute();
                                string answer = ssc.Result;
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                            }
                        }
                        returnValue.Rows.Add(ipname, createtype, createname);
                        client.Disconnect();
                    }
                    else if (createtype.ToLower() == StringHelper.database)
                    {
                        try
                        {
                            ForwardedPortLocal portFwld = null;
                            SshClient client1 = null;
                            try
                            {
                                string dbcon = stp.getPropertiesValue(StringHelper.dbconnectionDB, step);

                                DataTable dot = new TypeHandler().getInput(dbcon, StringHelper.variable, executionID);
                                string constring = dot.Rows[0]["ConnectionString"].ToString();
                                string dbtype = dot.Rows[0]["DataBaseServerType"].ToString();

                                if (dbtype.Trim().ToLower() == StringHelper.MySQL)
                                {
                                    /*PasswordConnectionInfo connectionInfo = new PasswordConnectionInfo(ipname, int.Parse(port), username, password);
                                    connectionInfo.Timeout = TimeSpan.FromSeconds(30);
                                    var client1 = new SshClient(connectionInfo);
                                    client1.Connect();
                                    ForwardedPortLocal portFwld = new ForwardedPortLocal("127.0.0.1", Convert.ToUInt32(3306), "127.0.0.1", Convert.ToUInt32(3306));
                                    client1.AddForwardedPort(portFwld);
                                    portFwld.Start();

                                    using (MySqlConnection con = new MySqlConnection((JsonConvert.DeserializeObject<MySqlConnectionStringBuilder>(constring)).ToString()))
                                    {
                                        con.Open();
                                        string query = "create database " + createname;
                                        using (MySqlCommand com = new MySqlCommand(query, con))
                                        {
                                            int reult = com.ExecuteNonQuery();
                                        }
                                        con.Close();
                                    }
                                    client1.Disconnect();*/


                                    PasswordConnectionInfo connectionInfo = new PasswordConnectionInfo(ipname, int.Parse(port), username, password);
                                    connectionInfo.Timeout = TimeSpan.FromSeconds(30);
                                    client1 = new SshClient(connectionInfo);
                                    client1.Connect();
                                    portFwld = new ForwardedPortLocal("127.0.0.1", Convert.ToUInt32(3306), "127.0.0.1", Convert.ToUInt32(3306));
                                    client1.AddForwardedPort(portFwld);
                                    portFwld.Start();

                                    MySqlConnectionStringBuilder msbor = JsonConvert.DeserializeObject<MySqlConnectionStringBuilder>(constring);

                                    MySqlConnectionStringBuilder msb = new MySqlConnectionStringBuilder();
                                    msb.UserID = msbor.UserID;
                                    msb.Password = msbor.Password;
                                    msb.Server = "127.0.0.1";
                                    msb.Port = 3306;
                                    msb.Database = msbor.Database;
                                    msb.ConnectionTimeout = 1000000;


                                    using (MySqlConnection con = new MySqlConnection(msb.ToString()))
                                    {
                                        try
                                        {
                                            con.Open();
                                            string query = "create database " + createname;
                                            using (MySqlCommand com = new MySqlCommand(query, con))
                                            {
                                                com.ExecuteNonQuery();
                                            }
                                            con.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            new Logger().LogException(ex);
                                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                            con.Close();
                                        }
                                    }

                                    portFwld.Stop();
                                    client1.Disconnect();
                                }
                                else if (dbtype.Trim().ToLower() == StringHelper.PostgreSQL)
                                {

                                }

                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                portFwld.Stop();
                                client1.Disconnect();
                            }

                            returnValue.Rows.Add(ipname, createtype, createname);
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }

                    }
                    else
                    {

                    }
                    ret = StringHelper.success;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                ret = StringHelper.ffalse;
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, StringHelper.datatablesmall, step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult Delete( Steps step, string executionID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>(); string ret = string.Empty;

            string input = string.Empty; string output = string.Empty; DataTable returnValue = new DataTable();
            string deletetype = string.Empty; string deletename = string.Empty; string notes = string.Empty;

            deletetype = stp.getPropertiesValue(StringHelper.deletetype, step);
            deletename = stp.getPropertiesValue(StringHelper.deletename, step);
            notes = stp.getPropertiesValue(StringHelper.notes, step);
            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);

            returnValue.Columns.Add(StringHelper.server);
            returnValue.Columns.Add(StringHelper.Delete);
            returnValue.Columns.Add(StringHelper.namee);

            try
            {

                DataTable dtr = new DataTable();
                dtr = new TypeHandler().getInput(input, StringHelper.variable, executionID);

                string ipname = dtr.Rows[0][StringHelper.IP].ToString();
                string port = dtr.Rows[0][StringHelper.Port].ToString();
                string username = dtr.Rows[0][StringHelper.UserName].ToString();
                string password = dtr.Rows[0][StringHelper.Password].ToString();

                // using (var client = new SshClient(new MethodResultHandler().CreateConnection(ipname, int.Parse(port), username, password)))
                using (var client = new SshClient(ipname, int.Parse(port), username, password))
                {
                    if (deletetype.ToLower() == StringHelper.users)
                    {
                        client.Connect();
                        if (client.IsConnected)
                        {
                            try
                            {
                                SshCommand ssc = client.CreateCommand("userdel " + deletename);
                                ssc.Execute();
                                string answer = ssc.Result;
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                            }
                        }
                        returnValue.Rows.Add(ipname, deletetype, deletename);
                        client.Disconnect();
                    }
                    else if (deletetype.ToLower() == StringHelper.group)
                    {
                        client.Connect();
                        if (client.IsConnected)
                        {
                            try
                            {
                                SshCommand ssc = client.CreateCommand("groupdel " + deletename);
                                ssc.Execute();
                                string answer = ssc.Result;
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                            }
                        }
                        returnValue.Rows.Add(ipname, deletetype, deletename);
                        client.Disconnect();
                    }
                    else if (deletetype.ToLower() == StringHelper.file)
                    {
                        client.Connect();
                        if (client.IsConnected)
                        {
                            try
                            {
                                SshCommand ssc = client.CreateCommand("rm " + deletename);
                                ssc.Execute();
                                string answer = ssc.Result;
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                            }
                        }
                        returnValue.Rows.Add(ipname, deletetype, deletename);
                        client.Disconnect();
                    }
                    else if (deletetype.ToLower() == StringHelper.folder)
                    {
                        client.Connect();
                        if (client.IsConnected)
                        {
                            try
                            {
                                SshCommand ssc = client.CreateCommand("rm -rf " + deletename);
                                ssc.Execute();
                                string answer = ssc.Result;
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                            }
                        }
                        returnValue.Rows.Add(ipname, deletetype, deletename);
                        client.Disconnect();
                    }
                    else if (deletetype.ToLower() == StringHelper.database)
                    {
                        try
                        {

                            try
                            {
                                ForwardedPortLocal portFwld = null;
                                SshClient client1 = null;

                                try
                                {
                                    string dbcon = stp.getPropertiesValue(StringHelper.dbconnectionDB, step);

                                    DataTable dot = new TypeHandler().getInput(dbcon, StringHelper.variable, executionID);
                                    string constring = dot.Rows[0]["ConnectionString"].ToString();
                                    string dbtype = dot.Rows[0]["DataBaseServerType"].ToString();

                                    if (dbtype.Trim().ToLower() == StringHelper.MySQL)
                                    {
                                        PasswordConnectionInfo connectionInfo = new PasswordConnectionInfo(ipname, int.Parse(port), username, password);
                                        connectionInfo.Timeout = TimeSpan.FromSeconds(30);
                                        client1 = new SshClient(connectionInfo);
                                        client1.Connect();
                                        portFwld = new ForwardedPortLocal("127.0.0.1", Convert.ToUInt32(3306), "127.0.0.1", Convert.ToUInt32(3306));
                                        client1.AddForwardedPort(portFwld);
                                        portFwld.Start();

                                        MySqlConnectionStringBuilder msbor = JsonConvert.DeserializeObject<MySqlConnectionStringBuilder>(constring);

                                        MySqlConnectionStringBuilder msb = new MySqlConnectionStringBuilder();
                                        msb.UserID = msbor.UserID;
                                        msb.Password = msbor.Password;
                                        msb.Server = "127.0.0.1";
                                        msb.Port = 3306;
                                        msb.Database = msbor.Database;
                                        msb.ConnectionTimeout = 1000000;

                                        using (MySqlConnection con = new MySqlConnection(msb.ToString()))
                                        {
                                            try
                                            {
                                                con.Open();
                                                string query = StringHelper.dropdb + deletename;
                                                using (MySqlCommand com = new MySqlCommand(query, con))
                                                {
                                                    com.ExecuteNonQuery();
                                                }
                                                con.Close();
                                            }
                                            catch (Exception ex)
                                            {
                                                new Logger().LogException(ex);
                                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                                con.Close();
                                            }
                                        }

                                        portFwld.Stop();
                                        client1.Disconnect();
                                    }
                                    else if (dbtype.Trim().ToLower() == "postgresql")
                                    {

                                    }
                                }
                                catch (Exception ex)
                                {
                                    new Logger().LogException(ex);
                                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                    portFwld.Stop();
                                    client1.Disconnect();
                                }

                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                            }

                            returnValue.Rows.Add(ipname, deletetype, deletename);
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }

                    }
                    else
                    {

                    }
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                ret = StringHelper.ffalse;
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, StringHelper.datatablesmall, step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult Copy( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            string input = string.Empty; string output = string.Empty; DataTable returnValue = new DataTable();
            string source = string.Empty; string destination = string.Empty;
            string notes = string.Empty; string filename = string.Empty; string move = string.Empty;


            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            source = stp.getPropertiesValue(StringHelper.source, step);
            destination = stp.getPropertiesValue(StringHelper.destination, step);
            notes = stp.getPropertiesValue(StringHelper.notes, step);
            filename = stp.getPropertiesValue(StringHelper.filename, step);
            move = stp.getPropertiesValue(StringHelper.move, step);

            returnValue.Columns.Add(StringHelper.FileName);
            returnValue.Columns.Add(StringHelper.Source);
            returnValue.Columns.Add(StringHelper.Destination);

            try
            {
                DataTable dtr = new DataTable();
                dtr = new TypeHandler().getInput(input, StringHelper.variable, executionID);

                string ipname = dtr.Rows[0][StringHelper.IP].ToString();
                string port = dtr.Rows[0][StringHelper.Port].ToString();
                string username = dtr.Rows[0][StringHelper.UserName].ToString();
                string password = dtr.Rows[0][StringHelper.Password].ToString();

              //  using (var client = new SshClient(new MethodResultHandler().CreateConnection(ipname, int.Parse(port), username, password)))
                using (var client = new SshClient(ipname, int.Parse(port), username, password))
                {
                    client.Connect();
                    if (client.IsConnected)
                    {
                        try
                        {
                            if (move.Trim().ToLower() == "true")
                            {
                                SshCommand ssc = client.CreateCommand("mv " + source + filename + " " + destination);
                                ssc.Execute();
                                string answer = ssc.Result;
                            }
                            else
                            {
                                SshCommand ssc = client.CreateCommand("cp " + source + filename + " " + destination);
                                ssc.Execute();
                                string answer = ssc.Result;
                            }
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                            Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                            s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                        }
                    }

                    client.Disconnect();
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
                new VariableHandler().CreateVariable(output, JsonConvert.SerializeObject(returnValue), "", true, StringHelper.datatablesmall, step.Robot_Id, executionID);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult DBRestore( Steps step, string executionID)
        {
            string dbnames = string.Empty; string backupfilename = string.Empty; string backupfolderpath = string.Empty; string allinone = string.Empty;
            string ret = string.Empty; string input = string.Empty; string output = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>(); DataTable returnValue = new DataTable();

            dbnames = stp.getPropertiesValue(StringHelper.dbn, step);
            backupfilename = stp.getPropertiesValue(StringHelper.backupfilename, step);
            backupfolderpath = stp.getPropertiesValue(StringHelper.backupfolderpath, step);
            allinone = stp.getPropertiesValue(StringHelper.allinone, step);
            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);



            try
            {

                DataTable dtr = new DataTable();
                dtr = new TypeHandler().getInput(input, StringHelper.variable, executionID);

                string ipname = dtr.Rows[0][StringHelper.IP].ToString();
                string port = dtr.Rows[0][StringHelper.Port].ToString();
                string username = dtr.Rows[0][StringHelper.UserName].ToString();
                string password = dtr.Rows[0][StringHelper.Password].ToString();
                string dbcon = stp.getPropertiesValue(StringHelper.dbconnectionDB, step);

                DataTable dot = new TypeHandler().getInput(dbcon, StringHelper.variable, executionID);
                string constring = dot.Rows[0]["ConnectionString"].ToString();
                string dbtype = dot.Rows[0]["DataBaseServerType"].ToString();

               // using (var client = new SshClient(new MethodResultHandler().CreateConnection(ipname, int.Parse(port), username, password)))
                using (var client = new SshClient(ipname, int.Parse(port), username, password))
                {
                    client.Connect();
                    if (client.IsConnected)
                    {
                        if (dbtype.ToLower().Trim() == StringHelper.MySQL)
                        {
                            if (backupfolderpath != string.Empty)
                            {
                                MySqlConnectionStringBuilder con = JsonConvert.DeserializeObject<MySqlConnectionStringBuilder>(constring);
                                //mysqldump database_name > database_name.sql
                                //SshCommand ssc = client.CreateCommand("groupadd " + createname);
                                //ssc.Execute();
                                //var cmd = client.CreateCommand("mysql --user=" + Variables.unamedb + " --password=" + Variables.pwddb + " --default_character_set utf8 " + dbnames + " < " + backupfolderpath + "/" + backupfilename + ";");
                                var cmd = client.CreateCommand("mysql -u" + con.UserID + " -p" + con.Password + " " + dbnames + " < " + backupfolderpath + "" + backupfilename);
                                var result = cmd.BeginExecute();
                                new MethodResultHandler().outputDisplay(cmd, result, step);
                            }
                            else
                            {
                                var cmd = client.CreateCommand(StringHelper.sql + dbnames + " <" + backupfilename);
                                var result = cmd.BeginExecute();
                                new MethodResultHandler().outputDisplay(cmd, result, step);
                            }
                        }
                        else if (dbtype.Trim() == "postgresql")
                        {

                        }
                    }
                    client.Disconnect();
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }

        public MethodResult DBBackup( Steps step, string executionID)
        {
            string dbnames = string.Empty; string backupfilename = string.Empty; string backupfolderpath = string.Empty; string allinone = string.Empty;
            string ret = string.Empty; string input = string.Empty; string output = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>(); DataTable returnValue = new DataTable();

            dbnames = stp.getPropertiesValue(StringHelper.dbn, step);
            backupfilename = stp.getPropertiesValue(StringHelper.backupfilename, step);
            backupfolderpath = stp.getPropertiesValue(StringHelper.backupfolderpath, step);
            allinone = stp.getPropertiesValue(StringHelper.allinone, step);

            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);


            try
            {
                DataTable dtr = new DataTable();
                dtr = new TypeHandler().getInput(input, StringHelper.variable, executionID);

                string ipname = dtr.Rows[0][StringHelper.IP].ToString();
                string port = dtr.Rows[0][StringHelper.Port].ToString();
                string username = dtr.Rows[0][StringHelper.UserName].ToString();
                string password = dtr.Rows[0][StringHelper.Password].ToString();
                string dbcon = stp.getPropertiesValue(StringHelper.dbconnectionDB, step);

                DataTable dot = new TypeHandler().getInput(dbcon, StringHelper.variable, executionID);
                string constring = dot.Rows[0]["ConnectionString"].ToString();
                string dbtype = dot.Rows[0]["DataBaseServerType"].ToString();

               // using (var client = new SshClient(new MethodResultHandler().CreateConnection(ipname, int.Parse(port), username, password)))
                using (var client = new SshClient(ipname, int.Parse(port), username, password))
                {
                    client.Connect();
                    if (client.IsConnected)
                    {
                        if (dbtype.ToLower().Trim() == StringHelper.MySQL)
                        {
                            MySqlConnectionStringBuilder con = JsonConvert.DeserializeObject<MySqlConnectionStringBuilder>(constring);

                            if (backupfolderpath != string.Empty)
                            {
                                //mysqldump database_name > database_name.sql
                                //SshCommand ssc = client.CreateCommand("mysqldump " + dbnames + " > " + backupfolderpath + "" + backupfilename + ".sql" + ";");
                                //ssc.Execute();
                                var cmd = client.CreateCommand("mysqldump -u" + con.UserID + " -p" + con.Password + " " + dbnames + " > " + backupfolderpath + "" + backupfilename + ".sql");
                                var result = cmd.BeginExecute();
                                new MethodResultHandler().outputDisplay(cmd, result, step);
                            }
                            else
                            {
                                var cmd = client.CreateCommand(StringHelper.sql + dbnames + " >" + backupfilename + ".sql");
                                var result = cmd.BeginExecute();
                                new MethodResultHandler().outputDisplay(cmd, result, step);
                            }
                        }
                        else if (dbtype.Trim() == "postgresql")
                        {

                        }
                    }
                    client.Disconnect();
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }


        public MethodResult CreateDBUser( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string uname = string.Empty; string pwd = string.Empty; string input = string.Empty; string output = string.Empty;

            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            uname = stp.getPropertiesValue(StringHelper.username, step);
            pwd = stp.getPropertiesValue(StringHelper.password, step);

            DataTable returnValue = new DataTable();

            try
            {

                DataTable dtr = new DataTable();
                dtr = new TypeHandler().getInput(input, StringHelper.variable, executionID);

                string ipname = dtr.Rows[0][StringHelper.IP].ToString();
                string port = dtr.Rows[0][StringHelper.Port].ToString();
                string username = dtr.Rows[0][StringHelper.UserName].ToString();
                string password = dtr.Rows[0][StringHelper.Password].ToString();

                ForwardedPortLocal portFwld = null;
                SshClient client1 = null;

                try
                {
                    string dbcon = stp.getPropertiesValue(StringHelper.dbconnectionDB, step);

                    DataTable dot = new TypeHandler().getInput(dbcon, StringHelper.variable, executionID);
                    string constring = dot.Rows[0]["ConnectionString"].ToString();
                    string dbtype = dot.Rows[0]["DataBaseServerType"].ToString();

                    if (dbtype.Trim().ToLower() == StringHelper.MySQL)
                    {
                        PasswordConnectionInfo connectionInfo = new PasswordConnectionInfo(ipname, int.Parse(port), username, password);
                        connectionInfo.Timeout = TimeSpan.FromSeconds(30);
                        client1 = new SshClient(connectionInfo);
                        client1.Connect();
                        portFwld = new ForwardedPortLocal("127.0.0.1", Convert.ToUInt32(3306), "127.0.0.1", Convert.ToUInt32(3306));
                        client1.AddForwardedPort(portFwld);
                        portFwld.Start();

                        MySqlConnectionStringBuilder msbor = JsonConvert.DeserializeObject<MySqlConnectionStringBuilder>(constring);

                        MySqlConnectionStringBuilder msb = new MySqlConnectionStringBuilder();
                        msb.UserID = msbor.UserID;
                        msb.Password = msbor.Password;
                        msb.Server = "127.0.0.1";
                        msb.Port = 3306;
                        msb.Database = msbor.Database;
                        msb.ConnectionTimeout = 1000000;

                        using (MySqlConnection con = new MySqlConnection(msb.ToString()))
                        {
                            try
                            {
                                con.Open();
                                string query = "CREATE USER '" + uname + "'@'%' IDENTIFIED BY '" + pwd + "';";
                                using (MySqlCommand com = new MySqlCommand(query, con))
                                {
                                    com.ExecuteNonQuery();
                                }
                                con.Close();
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                con.Close();
                            }
                        }
                        portFwld.Stop();
                        client1.Disconnect();
                    }
                    else if (dbtype.Trim().ToLower() == "postgres")
                    {

                    }
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                    portFwld.Stop();
                    client1.Disconnect();
                }
                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
        public MethodResult DeleteDBUser( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string uname = string.Empty; string input = string.Empty; string output = string.Empty;

            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            uname = stp.getPropertiesValue(StringHelper.username, step);

            DataTable returnValue = new DataTable();


            try
            {

                DataTable dtr = new DataTable();
                dtr = new TypeHandler().getInput(input, StringHelper.variable, executionID);

                string ipname = dtr.Rows[0][StringHelper.IP].ToString();
                string port = dtr.Rows[0][StringHelper.Port].ToString();
                string username = dtr.Rows[0][StringHelper.UserName].ToString();
                string password = dtr.Rows[0][StringHelper.Password].ToString();

                ForwardedPortLocal portFwld = null;
                SshClient client1 = null;

                try
                {
                    string dbcon = stp.getPropertiesValue(StringHelper.dbconnectionDB, step);

                    DataTable dot = new TypeHandler().getInput(dbcon, StringHelper.variable, executionID);
                    string constring = dot.Rows[0]["ConnectionString"].ToString();
                    string dbtype = dot.Rows[0]["DataBaseServerType"].ToString();

                    if (dbtype.Trim().ToLower() == StringHelper.MySQL)
                    {
                        PasswordConnectionInfo connectionInfo = new PasswordConnectionInfo(ipname, int.Parse(port), username, password);
                        connectionInfo.Timeout = TimeSpan.FromSeconds(30);
                        client1 = new SshClient(connectionInfo);
                        client1.Connect();
                        portFwld = new ForwardedPortLocal("127.0.0.1", Convert.ToUInt32(3306), "127.0.0.1", Convert.ToUInt32(3306));
                        client1.AddForwardedPort(portFwld);
                        portFwld.Start();

                        MySqlConnectionStringBuilder msbor = JsonConvert.DeserializeObject<MySqlConnectionStringBuilder>(constring);

                        MySqlConnectionStringBuilder msb = new MySqlConnectionStringBuilder();
                        msb.UserID = msbor.UserID;
                        msb.Password = msbor.Password;
                        msb.Server = "127.0.0.1";
                        msb.Port = 3306;
                        msb.Database = msbor.Database;
                        msb.ConnectionTimeout = 1000000;

                        using (MySqlConnection con = new MySqlConnection(msb.ToString()))
                        {
                            try
                            {
                                con.Open();
                                string query = "DROP USER '" + uname + "'@'%';";
                                using (MySqlCommand com = new MySqlCommand(query, con))
                                {
                                    com.ExecuteNonQuery();
                                }
                                con.Close();
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                con.Close();
                            }
                        }
                        portFwld.Stop();
                        client1.Disconnect();
                    }
                    else if (dbtype.Trim().ToLower() == "postgres")
                    {

                    }

                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                    portFwld.Stop();
                    client1.Disconnect();
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }




        public MethodResult GrantDBPermission( Steps step, string executionID)
        {
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string uname = string.Empty; string pwd = string.Empty; string permissiontype = string.Empty; string dbname = string.Empty; string rdbname = string.Empty;
            string input = string.Empty; string output = string.Empty;

            input = stp.getPropertiesValue(StringHelper.input, step);
            output = stp.getPropertiesValue(StringHelper.output, step);
            uname = stp.getPropertiesValue(StringHelper.username, step);
            pwd = stp.getPropertiesValue(StringHelper.password, step);
            permissiontype = stp.getPropertiesValue(StringHelper.permissiontype, step);
            dbname = stp.getPropertiesValue(StringHelper.databasename, step);

            DataTable returnValue = new DataTable();



            try
            {

                DataTable dtr = new DataTable();
                dtr = new TypeHandler().getInput(input, StringHelper.variable, executionID);

                string ipname = dtr.Rows[0][StringHelper.IP].ToString();
                string port = dtr.Rows[0][StringHelper.Port].ToString();
                string username = dtr.Rows[0][StringHelper.UserName].ToString();
                string password = dtr.Rows[0][StringHelper.Password].ToString();

                ForwardedPortLocal portFwld = null;
                SshClient client1 = null;

                try
                {


                    string dbcon = stp.getPropertiesValue(StringHelper.ConnectionString, step);

                    DataTable dot = new TypeHandler().getInput(dbcon, StringHelper.variable, executionID);
                    string constring = dot.Rows[0]["ConnectionString"].ToString();
                    string dbtype = dot.Rows[0]["DataBaseServerType"].ToString();

                    if (dbtype.Trim().ToLower() == StringHelper.MySQL)
                    {
                        PasswordConnectionInfo connectionInfo = new PasswordConnectionInfo(ipname, int.Parse(port), username, password);
                        connectionInfo.Timeout = TimeSpan.FromSeconds(30);
                        client1 = new SshClient(connectionInfo);
                        client1.Connect();
                        portFwld = new ForwardedPortLocal("127.0.0.1", Convert.ToUInt32(3306), "127.0.0.1", Convert.ToUInt32(3306));
                        client1.AddForwardedPort(portFwld);
                        portFwld.Start();

                        MySqlConnectionStringBuilder msbor = JsonConvert.DeserializeObject<MySqlConnectionStringBuilder>(constring);

                        MySqlConnectionStringBuilder msb = new MySqlConnectionStringBuilder();
                        msb.UserID = msbor.UserID;
                        msb.Password = msbor.Password;
                        msb.Server = "127.0.0.1";
                        msb.Port = 3306;
                        msb.Database = msbor.Database;
                        msb.ConnectionTimeout = 1000000;

                        using (MySqlConnection con = new MySqlConnection(msb.ToString()))
                        {
                            try
                            {
                                con.Open();
                                string query = StringHelper.grantprivileges + dbname + ".* to '" + uname + "'@'%' identified by \"" + pwd + "\"; flush privileges;";
                                using (MySqlCommand com = new MySqlCommand(query, con))
                                {
                                    com.ExecuteNonQuery();
                                }
                                con.Close();
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.errorcode);
                                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                                con.Close();
                            }
                        }
                        portFwld.Stop();
                        client1.Disconnect();
                    }
                    else if (dbtype.Trim().ToLower() == "postgres")
                    {

                    }
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                    s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
                    portFwld.Stop();
                    client1.Disconnect();
                }

                ret = StringHelper.success;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex, step, StringHelper.server, StringHelper.exceptioncode);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.exceptioncode, ex.Message, Method, StringHelper.falsee);
            }
            MethodResult mr;
            if (ret == StringHelper.success)
            {
                Method.Add(new MethodResultHandler().createMessage(this.GetType().Name, step.Name));
                s = new StatusModel(StringHelper.successcode, step.Name + StringHelper.execsuccess, Method, StringHelper.truee);
                mr = new MethodResultHandler().createResultType(true, msgs, Variables.dynamicvariables, s);
            }
            else
            {
                mr = new MethodResultHandler().createResultType(false, msgs, null, s);
            }
            return mr;
        }
    }
}