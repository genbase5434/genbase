﻿using Genbase.BLL;
using Genbase.classes;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Genbase.Classes;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.DAL
{
    public class PostgresConnect
    {
		public NpgsqlConnection con;
		public static string strConnString;
		public static bool canconnect = false;
		private static PostgresConnect instance = null;
		public static PostgresConnect GetInstance
		{
			get
			{
				if (instance == null)
				{
					instance = new PostgresConnect();
				}
				return instance;
			}
		}
		public PostgresConnect()
		{
			strConnString = Config.getconfig();
			List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
			try
			{
				if (con == null)
				{
					con = new NpgsqlConnection(strConnString);
				}
			}
			catch (Exception ex)
			{
				new Logger().LogException(ex);
				if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
				{
					msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
				}
				else
				{
					msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

				}
			}
		}

	//	public static string connectionString = Config.getconfig();
      //  NpgsqlConnection con = new NpgsqlConnection(connectionString);
        public bool Dbopen()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            bool ret = true;
            try
            {
				if (!canconnect)
				{
					con.Open();
					canconnect = true;
				}

            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //new ExecutionLog().add2Log(ex.Message, "", "", StringHelper.exceptioncode);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ret = false;
            }
            return ret;
        }
        public bool Dbclose()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            bool ret = true;
            try
            {
                con.Close();
				canconnect = false;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                ret = false;
            }
            return ret;
        }

        public string DbExecuteQuery(string query, string ret)
        {
            {
                string result = string.Empty;
                string sql = string.Empty;
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    Dbopen();
                    NpgsqlCommand command = new NpgsqlCommand(query, con);
                    command.CommandTimeout = 30;
                    NpgsqlDataAdapter sda = new NpgsqlDataAdapter(command);
                    DataSet ds = new DataSet();

                    int x = 0;
                    switch (ret)
                    {
                        case StringHelper.selection:
                            sda.Fill(ds);
                            DataTable dt = ds.Tables[0];
                            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                            Dictionary<string, object> row;
                            foreach (DataRow dr in dt.Rows)
                            {
                                row = new Dictionary<string, object>();
                                foreach (DataColumn col in dt.Columns)
                                {
                                    row.Add(col.ColumnName, dr[col]);
                                }
                                rows.Add(row);
                            }
                            result = JsonConvert.SerializeObject(rows);
                            break;
                        case StringHelper.insert:
                            x = command.ExecuteNonQuery();
                            result = StringHelper.success;
                            break;
                        case StringHelper.delete:
                            x = command.ExecuteNonQuery();
                            result = StringHelper.success;
                            break;
                        case StringHelper.update:
                            x = command.ExecuteNonQuery();
                            result = StringHelper.success;
                            break;
                        case StringHelper.insertwithreturn:
                            x = command.ExecuteNonQuery();
                            string query1 = ("select MAX(\"Id\") from public.\"STEPS\"");
                            NpgsqlCommand command1 = new NpgsqlCommand(query1, con);
                            result = command1.ExecuteScalar().ToString();
                            break;
                        case StringHelper.insertwithreturnprops:
                            x = command.ExecuteNonQuery();
                            string query2 = ("select CONCAT(\"Id\", '%' ,\"StepProperty\") from public.\"STEPPROPERTIES\" where \"Id\" = (select max(\"Id\") from public.\"STEPPROPERTIES\")");
                            NpgsqlCommand command2 = new NpgsqlCommand(query2, con);
                            result = command2.ExecuteScalar().ToString();
                            break;

                        case StringHelper.createtheproject:
                            NpgsqlCommand command3 = new NpgsqlCommand(query, con);
                            result = command3.ExecuteScalar().ToString();
                            break;
                        case StringHelper.createtherobot:
                            NpgsqlCommand command4 = new NpgsqlCommand(query, con);
                            result = command4.ExecuteScalar().ToString();
                            break;
                        case StringHelper.updatelinks:
                            x = command.ExecuteNonQuery();
                            result = x.ToString();
                            break;
                        case StringHelper.selectwithnodes:
                            NpgsqlCommand command5 = new NpgsqlCommand(query, con);
                            result = command5.ExecuteScalar().ToString();
                            break;
                        case StringHelper.selectwithnode:
                            NpgsqlCommand command7 = new NpgsqlCommand(query, con);
                            result = command7.ExecuteScalar().ToString();
                            break;
                        case StringHelper.selectrobotslogs:
                            x = command.ExecuteNonQuery();
                            NpgsqlCommand command6 = new NpgsqlCommand(query, con);
                            try
                            {
                                result = command6.ExecuteScalar().ToString();
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                return StringHelper.error;
                            }
                            break;
                    }
                    Dbclose();
                    return result;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                    if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                    }
                    else
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    return ex.Message;
                }
            }
        }
        public string DbExecuteQuery(string query, string ret, string connectionString)
        {
            string result = string.Empty;
            string sql = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                NpgsqlConnection vcon = new NpgsqlConnection(connectionString);
                vcon.Open();
                NpgsqlCommand command = new NpgsqlCommand(query, vcon);
                command.CommandTimeout = 30;
                NpgsqlDataAdapter sda = new NpgsqlDataAdapter(command);
                DataSet ds = new DataSet();
                int x = 0;
                switch (ret)
                {
                    case StringHelper.selection:
                        sda.Fill(ds);
                        DataTable dt = ds.Tables[0];
                        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                        Dictionary<string, object> row;
                        foreach (DataRow dr in dt.Rows)
                        {
                            row = new Dictionary<string, object>();
                            foreach (DataColumn col in dt.Columns)
                            {
                                row.Add(col.ColumnName, dr[col]);
                            }
                            rows.Add(row);
                        }
                        result = JsonConvert.SerializeObject(rows);
                        break;
                    case StringHelper.insert:
                        x = command.ExecuteNonQuery();
                        result = StringHelper.success;
                        break;
                    case StringHelper.delete:
                        x = command.ExecuteNonQuery();
                        result = StringHelper.success;
                        break;
                    case StringHelper.update:
                        x = command.ExecuteNonQuery();
                        result = StringHelper.success;
                        break;
                    case StringHelper.insertwithreturn:
                        x = command.ExecuteNonQuery();
                        string query1 = ("select MAX(\"Id\") from public.\"STEPS\"");
                        NpgsqlCommand command1 = new NpgsqlCommand(query1, vcon);
                        result = command1.ExecuteScalar().ToString();
                        break;
                    case StringHelper.insertwithreturnprops:
                        x = command.ExecuteNonQuery();
                        string query2 = ("select CONCAT(\"Id\", '%' ,\"StepProperty\") from public.\"STEPPROPERTIES\" where \"Id\" = (select max(\"Id\") from public.\"STEPPROPERTIES\")");
                        NpgsqlCommand command2 = new NpgsqlCommand(query2, vcon);
                        result = command2.ExecuteScalar().ToString();
                        break;
                    case StringHelper.createtheproject:
                        NpgsqlCommand command3 = new NpgsqlCommand(query, vcon);
                        result = command3.ExecuteScalar().ToString();
                        break;
                    case StringHelper.createtherobot:
                        NpgsqlCommand command4 = new NpgsqlCommand(query, vcon);
                        result = command4.ExecuteScalar().ToString();
                        break;
                    case StringHelper.updatelinks:
                        x = command.ExecuteNonQuery();
                        result = x.ToString();
                        break;
                    case StringHelper.selectwithnodes:
                        NpgsqlCommand command5 = new NpgsqlCommand(query, vcon);
                        result = command5.ExecuteScalar().ToString();
                        break;
                    case StringHelper.selectwithnode:
                        NpgsqlCommand command7 = new NpgsqlCommand(query, vcon);
                        result = command7.ExecuteScalar().ToString();
                        break;
                    case StringHelper.selectrobotslogs:
                        x = command.ExecuteNonQuery();
                        NpgsqlCommand command6 = new NpgsqlCommand(query, vcon);
                        try
                        {
                            result = command6.ExecuteScalar().ToString();
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            return StringHelper.error;
                        }
                        break;
                }

                vcon.Close();
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        // Getting Tables List
        public Dictionary<string, List<string>> getTables()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<string> objList = new List<string>();
            Dictionary<string, List<string>> objDict = new Dictionary<string, List<string>>();
            try
            {
                Dbopen();

                NpgsqlCommand cmd = new NpgsqlCommand("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='public'", con);
                NpgsqlDataReader dr = cmd.ExecuteReader();
                List<string> objL = new List<string>();
                while (dr.Read())
                {
                    objL.Add(dr["TABLE_NAME"].ToString()); ;
                }
                Dbclose();

                if (objL.Count > 0)
                {
                    foreach (string tbName in objL)
                    {
                        objList = getTableColumns(tbName);
                        objDict.Add(tbName, objList);
                    }
                }
                return objDict;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return null;
            }
        }

        public string CreateTable(string table_id, string table_name, string category, string mode, string createby, DataTable table_columns)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Dbopen();
                NpgsqlCommand command = new NpgsqlCommand("tablemanager", con);
                command.CommandTimeout = 30;
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("table_id", "table_id");
                command.Parameters.AddWithValue("tbl_name", "table_name");
                command.Parameters.AddWithValue("category", "category");
                command.Parameters.AddWithValue("mode", "mode");
                command.Parameters.AddWithValue("createby", "createby");
                command.Parameters.AddWithValue("table_columns", "table_columns");

                int i = command.ExecuteNonQuery();
                Dbclose();
                return i.ToString();
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        // Getting Table Columns List List
        public List<string> getTableColumns(string tableName)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<string> objList = new List<string>();
            try
            {
                Dbopen();
            }
            catch (Exception ex)
            {
                string ec = ex.Message;
            }
            NpgsqlCommand cmd2 = new NpgsqlCommand("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "' ORDER BY ORDINAL_POSITION", con);
            NpgsqlDataReader dr1 = cmd2.ExecuteReader();

            while (dr1.Read())
            {
                objList.Add(dr1["COLUMN_NAME"].ToString());
            }
            Dbclose();

            return objList;
        }

        public List<TableInfoModel> getTableInfo(string tableName)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<TableInfoModel> objList = new List<TableInfoModel>();
            try
            {
                Dbopen();
            }
            catch (Exception ex)
            {
                string ec = ex.Message;
            }
            NpgsqlCommand cmd2 = new NpgsqlCommand("SELECT COLUMN_NAME,data_type FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "' ORDER BY ORDINAL_POSITION", con);
            cmd2.CommandTimeout = 30;
            NpgsqlDataReader dr1 = cmd2.ExecuteReader();

            while (dr1.Read())
            {
                TableInfoModel objtb = new TableInfoModel();
                objtb.table_column = dr1["COLUMN_NAME"].ToString();
                objtb.column_datatype = dr1["data_type"].ToString();
                objList.Add(objtb);
            }
            Dbclose();

            return objList;
        }

        public string Insert_GetID(string Query)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string id = string.Empty;
            try
            {
                Dbopen();
            }
            catch (Exception ex)
            {
                string ec = ex.Message;
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
            }
            NpgsqlCommand cmd2 = new NpgsqlCommand(Query, con);
            cmd2.CommandTimeout = 30;
            NpgsqlDataReader dr1 = cmd2.ExecuteReader();
            while (dr1.Read())
            {
                id = dr1[0].ToString();
            }
            Dbclose();
            return id;
        }

        public string GetDataType(string tableColumn)
        {
            string col_datatype = string.Empty;
            string[] arr_tbl = tableColumn.Split('.');
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (arr_tbl.Length > 1)
                {
                    string str_Sql = "SELECT data_type FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + Convert.ToString(arr_tbl[0]) + "' and COLUMN_NAME='" + Convert.ToString(arr_tbl[1]) + "' ORDER BY ORDINAL_POSITION";
                    Dbopen();
                    NpgsqlCommand cmd1 = new NpgsqlCommand(str_Sql, con);
                    cmd1.CommandTimeout = 30;
                    NpgsqlDataReader dr1 = cmd1.ExecuteReader();

                    while (dr1.Read())
                    {
                        col_datatype = dr1["data_type"].ToString();
                    }
                }
                string strDT = "string";
                if (col_datatype.Length > 0)
                {
                    strDT = CheckDataType(col_datatype);
                }
                Dbclose();
                return strDT;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public DataTable GetRuleDetails(string query)
        {
            List<Dictionary<string, string>> objDict = new List<Dictionary<string, string>>();
            Dbopen();
            NpgsqlCommand cmd1 = new NpgsqlCommand(query, con);
            cmd1.CommandTimeout = 30;

            NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd1);
            DataSet ds = new DataSet();
            da.Fill(ds);
            DataTable dt = ds.Tables[0];
            Dbclose();
            return dt;
        }

        public string CheckDataType(string strDataType)
        {
            switch (strDataType)
            {
                case "smallint":
                case "integer":
                case "bigint":
                case "decimal":
                case "numeric":
                case "double precision":
                case "smallserial":
                case "serial":
                case "bigserial":
                    return "int";
                default:
                    return "string";
            }
        }
    }

    public class MySQL
    {
        public string DbExecuteQuerymySQL(string query, string ret, string connectionString)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string result = string.Empty;
            MySqlConnection conn = new MySqlConnection((JsonConvert.DeserializeObject<MySqlConnectionStringBuilder>(connectionString)).ToString());
            conn.Open();
            MySqlCommand cmd = new MySqlCommand(query, conn);
            MySqlDataAdapter rdr = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                int x = 0;
                switch (ret)
                {
                    case StringHelper.selection:
                        rdr.Fill(ds);
                        DataTable dt = ds.Tables[0];
                        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                        Dictionary<string, object> row;
                        foreach (DataRow dr in dt.Rows)
                        {
                            row = new Dictionary<string, object>();
                            foreach (DataColumn col in dt.Columns)
                            {
                                row.Add(col.ColumnName, dr[col]);
                            }
                            rows.Add(row);
                        }
                        result = JsonConvert.SerializeObject(rows);
                        break;
                    case StringHelper.insert:
                        x = cmd.ExecuteNonQuery();
                        result = StringHelper.success;
                        break;
                    case StringHelper.delete:
                        x = cmd.ExecuteNonQuery();
                        result = StringHelper.success;
                        break;
                    case StringHelper.update:
                        x = cmd.ExecuteNonQuery();
                        result = StringHelper.success;
                        break;
                    case StringHelper.createtherobot:
                        MySqlCommand command4 = new MySqlCommand(query, conn);
                        result = command4.ExecuteScalar().ToString();
                        break;
                }
                conn.Close();
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
    }

}