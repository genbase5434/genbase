﻿using Genbase.BLL;
using Genbase.classes;
using Genbase.Classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.DAL
{
    public class QueryUtil
    {
        BLL.EncoderDecoderBase64 baser = new BLL.EncoderDecoderBase64();
        public string getTableData(string table_name, string filter_params, string columns_to_display, int max_row_count, string schema_name, string sorting_params, string sorting_order)
        {
            //check for columnnames is existing in DB
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<TableSchema> schema = getTableSchema(table_name);
            List<string> cols = columns_to_display.Split(new string[] { "," }, StringSplitOptions.None).ToList();
            try
            {
                if ((columns_to_display.Trim() != "*") && (columns_to_display.Trim() != string.Empty))
                {
                    foreach (string col in cols)
                    {
                        if (CheckColumnExists(col, schema))
                        {

                        }
                        else
                        {
                            return StringHelper.columnnotexists;
                        }
                    }
                }

                if (schema_name.Trim() == string.Empty)
                {
                    schema_name = "public";
                }

                string columnPart = getColumnsToDisplay(columns_to_display);
                string wherePart = getSQLForFilterParams(filter_params, schema, "and", false);
                string tablePart = schema_name + ".\"" + table_name + "\"";

                string orderPart = " 1 desc ";
                if (sorting_params.Trim() != string.Empty)
                {
                    orderPart = " " + sorting_params;
                }
                else
                {
                    orderPart = " 1";
                }
                if (sorting_order.Trim() != string.Empty)
                {
                    orderPart = orderPart + " " + sorting_order;
                }

                string queryPart = string.Empty;

                if (wherePart.Trim() != string.Empty)
                {
                    queryPart = columnPart + " from " + tablePart + " where " + wherePart + " order by" + orderPart;
                    if (max_row_count > 0)
                        queryPart = "select " + queryPart + " LIMIT " + max_row_count;
                    else
                        queryPart = "select " + queryPart;
                }
                else
                {
                    queryPart = columnPart + " from " + tablePart + " order by" + orderPart;
                    if (max_row_count > 0)
                        queryPart = "select " + queryPart + " LIMIT " + max_row_count;
                    else
                        queryPart = "select " + queryPart;
                }

                string exitStatus = string.Empty;



                if (queryPart.Trim() != string.Empty)
                {
                    exitStatus = new PostgresConnect().DbExecuteQuery(queryPart, "select");
                }
                return exitStatus;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return null;
            }
        }

        public string insertTableData(string schemaname, string tablename, string rowvalues, string ReturningColumn, bool isEncoded)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<TableSchema> schema = getTableSchema(tablename);
            List<string> row = rowvalues.Split(',').ToList();
            List<string> newrow = rowvalues.Split(',').ToList();

            if (isEncoded)
            {
                row.Clear();
                foreach (var ro in newrow)
                {
                    row.Add(baser.Base64Decode(ro));
                }
            }
            List<InsertSchema> insete = new List<InsertSchema>();
            try
            {
                if (schemaname.Trim() == string.Empty)
                {
                    schemaname = "public";
                }

                string tablePart = schemaname + ".\"" + tablename + "\"";
                string queryPart1 = "INSERT INTO " + tablePart + " VALUES( ";
                string queryPart2 = " ) ";
                string returningPart = "Returning \"" + ReturningColumn + "\" ;";
                string queryPart = string.Empty;
                string insertPart = string.Empty;

                int i = 0;
                //bool isDefault = true;

                if (schema[0].data_type.ToLower().IndexOf("int") > -1 || schema[0].data_type.ToLower().IndexOf("serial") > -1)
                {
                    row.Insert(0, "default");
                }
                else
                {
                    //isDefault = false;
                }
                if (schema.Count >= row.Count)
                {
                    foreach (TableSchema sch in schema)
                    {

                        if (i == 0 && row[0] == StringHelper.bydefault)
                        {
                            InsertSchema inset = new InsertSchema();
                            inset.column_name = sch.column_name;
                            inset.data_type = sch.data_type;
                            inset.value = row[i];
                            insete.Add(inset);
                        }
                        else if (i < row.Count)
                        {
                            if (canConvertType(row[i], sch.data_type))
                            {
                                InsertSchema inset = new InsertSchema();
                                inset.column_name = sch.column_name;
                                inset.data_type = sch.data_type;
                                inset.value = row[i];
                                insete.Add(inset);
                            }
                            else
                            {
                                InsertSchema inset = new InsertSchema();
                                inset.column_name = sch.column_name;
                                inset.data_type = sch.data_type;
                                inset.value = "1";
                                insete.Add(inset);
                            }
                        }
                        else
                        {
                            InsertSchema inset = new InsertSchema();
                            inset.column_name = sch.column_name;
                            inset.data_type = sch.data_type;
                            inset.value = "0";
                            insete.Add(inset);
                        }

                        i++;
                    }

                    foreach (InsertSchema ys in insete)
                    {
                        if (ys.data_type.IndexOf(StringHelper.character) > -1)
                        {
                            insertPart = insertPart + "'" + ys.value + "'";
                        }
                        else if (ys.data_type.IndexOf(StringHelper.tdate) > -1)
                        {
                            if (canConvertType(ys.value, ys.data_type))
                            {
                                insertPart = insertPart + "'" + ys.value + "'";
                            }
                            else
                            {
                                insertPart = insertPart + "null";
                            }
                        }
                        else if (ys.data_type.IndexOf(StringHelper.timeperiod) > -1)
                        {
                            if (canConvertType(ys.value, ys.data_type))
                            {
                                insertPart = insertPart + "'" + ys.value + "' " + "::TIMESTAMP";
                            }
                            else
                            {
                                insertPart = insertPart + "null";
                            }
                        }
                        else
                        {
                            insertPart = insertPart + ys.value + "";
                        }
                        insertPart = insertPart + ",";
                    }

                    insertPart = insertPart.Remove(insertPart.Length - 1);

                    if (ReturningColumn.Trim() != string.Empty)
                    {
                        queryPart = queryPart1 + insertPart + queryPart2 + returningPart;
                        return new PostgresConnect().DbExecuteQuery(queryPart, "createrobot");
                    }
                    else
                    {
                        queryPart = queryPart1 + insertPart + queryPart2;
                        return new PostgresConnect().DbExecuteQuery(queryPart, "insert");
                    }
                }
                else
                {
                    return StringHelper.unevenfields;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string insertdetails(string schemaname, string tablename, string rowvalues, string ReturningColumn)
        {
            List<TableSchema> schema = getTableSchema(tablename);
            List<string> row = rowvalues.Split(',').ToList();
            List<InsertSchema> insete = new List<InsertSchema>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (schemaname.Trim() == string.Empty)
                {
                    schemaname = "public";
                }

                string tablePart = schemaname + ".\"" + tablename + "\"";
                string queryPart1 = "INSERT INTO " + tablePart + " VALUES( ";
                string queryPart2 = " ) ";
                string returningPart = "Returning \"" + ReturningColumn + "\" ;";
                string queryPart = string.Empty;
                string insertPart = string.Empty;

                int i = 0;
                //bool isDefault = true;

                if (schema[0].data_type.ToLower().IndexOf("int") > -1 || schema[0].data_type.ToLower().IndexOf("serial") > -1)
                {
                    row.Insert(0, "default");
                }
                else
                {
                    //isDefault = false;
                }
                if (schema.Count >= row.Count)
                {

                    foreach (TableSchema sch in schema)
                    {

                        if (i == 0 && row[0] == StringHelper.bydefault)
                        {
                            InsertSchema inset = new InsertSchema();
                            inset.column_name = sch.column_name;
                            inset.data_type = sch.data_type;
                            inset.value = row[i];
                            insete.Add(inset);
                        }
                        else if (i < row.Count)
                        {
                            if (canConvertType(row[i], sch.data_type))
                            {
                                InsertSchema inset = new InsertSchema();
                                inset.column_name = sch.column_name;
                                inset.data_type = sch.data_type;
                                inset.value = row[i];
                                insete.Add(inset);
                            }
                            else
                            {
                                InsertSchema inset = new InsertSchema();
                                inset.column_name = sch.column_name;
                                inset.data_type = sch.data_type;
                                inset.value = "";
                                insete.Add(inset);
                            }
                        }
                        else
                        {
                            InsertSchema inset = new InsertSchema();
                            inset.column_name = sch.column_name;
                            inset.data_type = sch.data_type;
                            inset.value = "";
                            insete.Add(inset);
                        }

                        i++;
                    }

                    foreach (InsertSchema ys in insete)
                    {
                        if (ys.data_type.IndexOf(StringHelper.character) > -1)
                        {
                            insertPart = insertPart + "'" + ys.value + "'";
                        }
                        else if (ys.data_type.IndexOf(StringHelper.tdate) > -1)
                        {
                            if (canConvertType(ys.value, ys.data_type))
                            {
                                insertPart = insertPart + "'" + ys.value + "'";
                            }
                            else
                            {
                                insertPart = insertPart + "null, ";
                            }
                        }
                        else if (ys.data_type.IndexOf(StringHelper.timeperiod) > -1)
                        {
                            if (canConvertType(ys.value, ys.data_type))
                            {
                                insertPart = insertPart + "TO_CHAR(current_timestamp, '" + ys.value + "')";
                            }
                            else
                            {
                                insertPart = insertPart + "null, ";
                            }
                        }
                        else
                        {
                            insertPart = insertPart + ys.value + "";
                        }
                        insertPart = insertPart + ",";
                    }

                    insertPart = insertPart.Remove(insertPart.Length - 1);

                    if (ReturningColumn.Trim() != string.Empty)
                    {
                        queryPart = queryPart1 + insertPart + queryPart2 + returningPart;
                        return new PostgresConnect().DbExecuteQuery(queryPart, "createrobot");
                    }
                    else
                    {
                        queryPart = queryPart1 + insertPart + queryPart2;
                        return new PostgresConnect().DbExecuteQuery(queryPart, "insert");
                    }
                }
                else
                {
                    return StringHelper.unevenfields;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string updateTableData(string tablename, string wheres, string sets, string schemaname, bool isEncoded)
        {
            List<TableSchema> schema = getTableSchema(tablename);
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (schemaname.Trim() == string.Empty)
                {
                    schemaname = "public";
                }

                List<string> wherevalues = wheres.Split(new string[] { "||" }, StringSplitOptions.None).ToList();
                List<string> setvalues = sets.Split(new string[] { "||" }, StringSplitOptions.None).ToList();

                string tablePart = schemaname + ".\"" + tablename + "\"";
                string openPart = "UPDATE " + tablePart + " SET ";
                string wherePart = getSQLForFilterParams(wheres, schema, "and", isEncoded);
                string setPart = getSQLForFilterParams(sets, schema, ",", isEncoded);
                string queryPart = openPart + setPart + " WHERE " + wherePart;
                return new PostgresConnect().DbExecuteQuery(queryPart, "update");
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string TableManipulate(string queryoperation, string tablename, string colnames, string schemaname, string newname)
        {
            string tablePart = schemaname + ".\"" + tablename + "\"";
            string openPart = string.Empty;
            string seperator = ",";
            List<string> lineSeperators = new List<string>();
            List<string> conditionsList = new List<string>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (colnames.Contains("||"))
                {
                    lineSeperators = colnames.Split(new string[] { "||" }, StringSplitOptions.None).ToList();
                }
                else
                {
                    lineSeperators.Add(colnames);
                }

                foreach (string lineSeperator in lineSeperators)
                {
                    List<string> items = lineSeperator.Split('|').ToList();
                    string columnname = items[0]; //column
                    string datatype = items[1]; //column condition
                    string type = string.Empty; //column datatype

                    conditionsList.Add(columnname + " " + datatype);
                }

                string joinedCondition = string.Join(" " + seperator + " ", conditionsList);

                switch (queryoperation)
                {
                    case "create":
                        openPart = "CREATE " + tablePart + " ( ";
                        openPart = openPart + joinedCondition + ")";
                        break;
                    case "rename":
                        openPart = "ALTER TABLE IF EXISTS " + tablePart + " RENAME TO " + newname;
                        break;
                    case "drop":
                        openPart = "DROP TABLE " + tablePart;
                        break;
                }

                return new PostgresConnect().DbExecuteQuery(openPart, "update");
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string DeleteTableData(string tablename, string wheres, string schemaname)
        {
            List<TableSchema> schema = getTableSchema(tablename);
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (schemaname.Trim() == string.Empty)
                {
                    schemaname = "public";
                }

                List<string> wherevalues = wheres.Split(new string[] { "||" }, StringSplitOptions.None).ToList();
                string tablePart = schemaname + ".\"" + tablename + "\"";
                string openPart = string.Empty;
                string wherePart = getSQLForFilterParams(wheres, schema, "and", false);
                string queryPart = string.Empty;
                if (wheres == string.Empty)
                {
                    openPart = "TRUNCATE " + tablePart + " RESTART IDENTITY";
                }
                else
                {
                    openPart = "DELETE " + tablePart;
                    queryPart = openPart + " WHERE " + wherePart;
                }

                return new PostgresConnect().DbExecuteQuery(queryPart, "delete");
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string getColumnsToDisplay(string columns_to_display)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (columns_to_display.Trim() == string.Empty || columns_to_display.Trim() == "*")
                {
                    return "*";
                }
                else
                {
                    return columns_to_display;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getSQLForFilterParams(string filter_params, List<TableSchema> schema, string seperator, bool isEncoded)
        {
            List<string> lineSeperators = new List<string>();
            List<string> conditionsList = new List<string>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (filter_params.Contains("||"))
                {
                    lineSeperators = filter_params.Split(new string[] { "||" }, StringSplitOptions.None).ToList();
                }
                else
                {
                    lineSeperators.Add(filter_params);
                }

                foreach (string lineSeperator in lineSeperators)
                {
                    List<string> items = lineSeperator.Split('|').ToList();

                    string op = getOperatorBasedonID(items[1]);
                    string operand1 = items[0]; //column
                    string operand2 = "";
                    if (isEncoded)
                        operand2 = baser.Base64Decode(items[2]); //column condition
                    else
                        operand2 = items[2];
                    string type = string.Empty; //column datatype

                    if (CheckColumnExists(operand1, schema))
                    {
                        List<TableSchema> gis = getColumnType(operand1, schema);
                        type = gis[0].data_type;
                        if (type.ToLower().IndexOf(StringHelper.character) > -1 || type.ToLower().IndexOf(StringHelper.timeperiod) > -1)
                        {
                            operand2 = "'" + operand2 + "'";
                        }
                    }
                    conditionsList.Add("\"" + operand1 + "\"" + op + operand2);
                }

                string joinedCondition = string.Join(" " + seperator + " ", conditionsList);


                return joinedCondition;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public List<TableSchema> getTableSchema(string tablename)
        {
            string query = "select column_name,data_type from information_schema.columns where table_name = '" + tablename + "'; ";
            string ty = new PostgresConnect().DbExecuteQuery(query, "select");
            List<TableSchema> cols = JsonConvert.DeserializeObject<List<TableSchema>>(ty);
            return cols;
        }

        public string getOperatorBasedonID(string operator_id)
        {
            string operatr = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (operator_id == "1" || operator_id.Trim().ToLower() == StringHelper.like)
                {
                    operatr = StringHelper.like;
                }
                else if ((operator_id == "2" || operator_id.Trim().ToLower() == StringHelper.equals))
                {
                    operatr = "=";
                }
                else if (operator_id == "3" || operator_id.Trim().ToLower() == StringHelper.notequals)
                {
                    operatr = "!=";
                }
                else if (operator_id == "4" || operator_id.Trim().ToLower() == StringHelper.lessthan)
                {
                    operatr = "<";
                }
                else if (operator_id == "5" || operator_id.Trim().ToLower() == StringHelper.greaterthan)
                {
                    operatr = ">";
                }
                else if (operator_id == "6" || operator_id.Trim().ToLower() == StringHelper.quottedin)
                {
                    operatr = "IN";
                }
                else if (operator_id == "7" || operator_id.Trim().ToLower() == StringHelper.notin)
                {
                    operatr = "NOT IN";
                }
                else if (operator_id == "8" || operator_id.Trim().ToLower() == StringHelper.lessthanorequalto)
                {
                    operatr = "<=";
                }
                else if (operator_id == "9" || operator_id.Trim().ToLower() == StringHelper.greaterthanorequalto)
                {
                    operatr = ">=";
                }
                return operatr;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public bool CheckColumnExists(string column, List<TableSchema> schema)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                column = column.Replace("\"", string.Empty).Trim();
                return schema.Any(c => c.column_name.Trim() == column.Trim());
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return true;
            }
        }
        public List<TableSchema> getColumnType(string column, List<TableSchema> schema)
        {
            return schema.FindAll(c => c.column_name.Trim() == column.Trim());
        }
        public bool canConvertType(string value, string type)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (type.ToLower().IndexOf(StringHelper.boolean) > -1)
                {
                    bool parsedValue;
                    if (bool.TryParse(value, out parsedValue))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (type.ToLower().IndexOf(StringHelper.integer) > -1 || type.ToLower().IndexOf(StringHelper.serial) > -1 || type.ToLower().IndexOf(StringHelper.numeric) > -1)
                {
                    int parsedValue;
                    if (int.TryParse(value, out parsedValue))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (type.ToLower().IndexOf(StringHelper.decimalnumber) > -1 || type.ToLower().IndexOf(StringHelper.decimalnumber) > -1)
                {
                    double parsedValue;
                    if (double.TryParse(value, out parsedValue))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (type.ToLower().IndexOf(StringHelper.characters) > -1)
                {
                    char parsedValue;
                    if (char.TryParse(value, out parsedValue))
                    {
                        return true;
                    }
                    else
                    {
                        if (type.ToLower().IndexOf(StringHelper.character) > -1 || type.ToLower().IndexOf(StringHelper.text) > -1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else if (type.ToLower().IndexOf(StringHelper.timeperiod) > -1)
                {
                    DateTime parsedValue;
                    if (DateTime.TryParse(value, out parsedValue))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (type.ToLower().IndexOf(StringHelper.tdate) > -1)
                {
                    DateTime parsedValue;
                    if (DateTime.TryParse(value, out parsedValue))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (type.ToLower().IndexOf(StringHelper.characters) > -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                //msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return true;
            }
        }
    }
    public class TableSchema
    {
        public string column_name { get; set; }
        public string data_type { get; set; }
    }
    public class InsertSchema
    {
        public string column_name { get; set; }
        public string data_type { get; set; }
        public string value { get; set; }
    }
}