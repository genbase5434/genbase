﻿using Genbase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genbase.ServiceInterfaces
{
   public interface DesignStudioInterfaces
    {
         string GenerateJwtToken(string mail, string MobilePhone);
         string ValidateToken(string JWToken);
         string Create(string input);
         string Get(string input);
         string Delete(string input);
         string SaveWorkflow(Entities input);
         string Update(string input);
         string ExecuteRobot(string input);
         void GetAllNextStep(Steps step);
         string debug_Order(string input);
         string debug_Execution(string input);
         string Set(Entities input);
         string getStudio(string roleId, string userId);
         string StepExecution(string input);
         string Validate(string LocalStepPairs);
         string Upload(string fPath, string data1, bool isFolder, string previousFolderPath);
         string sendMail(string input);
         string NewSaveWorkflow(Dictionary<string, string> input);
    }
}
