﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genbase.ServiceInterfaces
{
    public interface ScreenGenerationInterfaces
    {
        string SetPage(object screenData);
        string UpdatePage(object screenData);
        string DropDownTables();
        string EditView(string name);
        string getScreenDetails(int id);
        string updateMenuDetails(string Source, int destID);
        string LoadRoles();
        string getMenu(string tablename);
        string deleteScreen(string tableName, string deleteId);
        string getWidgetRow(int elementId);
        string widget(string columnDetails);
    }
}
