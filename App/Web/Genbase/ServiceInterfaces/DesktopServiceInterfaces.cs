﻿using Genbase.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genbase.ServiceInterfaces
{
   public interface DesktopServiceInterfaces
    {
        string CreateProject(Entities input);
        string Delete(Entities input);
        string Get(string input);
        string Update(Entities input);
        string Execute(Entities input);
        string Set(Entities input);
        string Rename(Entities inputString);
        string Debug(Entities inputString);
        string Validate(Entities inputString);
        string StepExecution(Entities inputString);
        string UploadDocument(byte[] file, string file_name);
        string ClearRobotGlobalVariables(string Robot_Id);
        bool TestConnection();
    }
}
