﻿using Genbase.Classes;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genbase.ServiceInterfaces
{
    public interface CrudServiceInterfaces
    {
         string getTableData(string tablename);
         DataTables getGridData(string tablename, PagingParams data);        
         string getTableDataByUserID(string tablename, string CreateBy);
         string loadUsernames();
         string GetRobotsByLOB();
         string GetVersionsofRobot(string id);
         string createTableRow(string tablename, string Values);
         string createTableForLob(string tablename, string Values);
         string getStepProperties(string stepId);
         string UserRegistration(string UserDetails);
         string GetRobotLog(string tablename, string roleid);
         string UpdateTableRow(string tablename, string Values, string ID);
         string UpdateTableRowforLob(string tablename, string Values, string ID);
         string GetHistory(string robotid);
         string PreDefinedRobot(string projectid, string robotname, string predefinedbotid);
         Renci.SshNet.ConnectionInfo CreateConnection(string ipname, int port, string username, string password);
         string Monitor(string ScreenName, string Role, string CreateBy, string UpdateBy);
         string GetScheduleDetails(string RobotId, string SystemId);
         string GetRobotTimelineLive(string lasttimestamp);
         string deleteTableRow(string tablename, string ID);
         DataTables getGridDataByUser(string tablename, string CreateBy, PagingParams data);
        DataTables getGridDataByUserCategory(string tablename, string CreateBy, PagingParams data, string Category);
        DataTables getConfigurationGridData(string tablename, string type, PagingParams data);
        DataTables getConnectionGridData(string tablename, string type, PagingParams data);
         string Connective_info();
         string LoginAuthenticationEX(string user);
         string GetSystemsByRobotsCount();
         string getMenuRoleId(int RoleId, string LOB);
         string ForgetPassword(string email);
         string robotSchedule(string ScheduleDetails);
         string genericScreenDropDown(int elementId);
         string getWidgetRow(int elementId);
         string stopRobotSchedule(string ScheduleDetails);
         string gettablebyId(string tablename, string id);
         string getRobotsByLOB(string timestamp);
         string generateSQLScripts(string Robots, string url);
         string uploadSQLScripts(object importRobotData);
         string UpdateRobotStatus(string BotID);
        string getDataByRTable(string tableDetails);
        string createMenuTableRow(string tablename, string Values);
        string hardDeleteTableRow(string tablename, string roleid);
        string getRoleMenu(int roleID);
        DataTables GetDataByUser(string tablename, string CreateBy);
        string getapp_tablesData();
        string deleteTables(string table_id);
        //string uploadSQLScripts(Entities input);
        string UpdateNKPIPath(string winlocation, string path, int id);
        //string CreateVariables(string rid, DVariables variable);
        string CreateVariables(string rid, string variable);
        string UpdateVariables(string rid, string variable);
        string getVariables(string rid);
        string Deletevariables(string rid, string variable);
        string getFilterTableData(string tablename, string Category);
        string connectionHealthCheck(string tablename);
        string getConnectivity_Info(string tablename, string Name, string Server, int Conn_ID);
    }
}
