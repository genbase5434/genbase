﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genbase.Interface_DashBoard
{
   public interface DashBoardServiceInterfaces
    {
        Task<string> getKPI(int roleid, string monitor, string filters, string KPIid);
		Task<string> getCharts(int roleid, string monitor, string filters, string KPIid);
		Task<string> getTimeCharts(int roleid, string monitor, string filters, string chartId);
		Task<string> queryValidate(string DBInstance, string Query);
		Task<string> dashboardIcons(int roleid);
		Task<string> updateDashboard(string Type, string roleid,string AddnewdashboardId,string dashboardId);
		Task<string> getMenu(string tablename);
		Task<string> filterChartsSeries();
		Task<string> filterCharts(string category);
    }
}
