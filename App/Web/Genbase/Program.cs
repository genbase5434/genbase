﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Genbase.BLL;
using Genbase.classes;
using Newtonsoft.Json;
using Genbase.Classes;
using NHibernate;

namespace Genbase
{
    public class Program
    {
        public static void Main(string[] args)
        {
            APIScheduler _objAPIS = new APIScheduler();
            _objAPIS.StartScheduler();

            CreateWebHostBuilder(args).Build().Run();
            Console.ReadLine();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .UseKestrel()
            .UseContentRoot(Directory.GetCurrentDirectory())
            .UseIISIntegration()
            .UseStartup<Startup>();

        public static ISessionFactory NHibernateSessionFactory;

    }

    public class APIScheduler
    {
        Timer timer1;
        public void StartScheduler()
        {
            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(ThreadFunc));
            thread.IsBackground = true;
            thread.Name = "ThreadFunc";
            thread.Start();
        }

        protected void ThreadFunc()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                timer1 = new Timer();
                // classes.ProcessManager pm = new classes.ProcessManager();
                timer1.Elapsed += new ElapsedEventHandler(serverscheduler);
                timer1.Interval = 1000 * 60; // in miliseconds
                timer1.Stop();
                timer1.Start();
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
            }
        }

        protected void serverscheduler(object source, ElapsedEventArgs e)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Classes.Robot cr = new Classes.Robot();
                Classes.cSystems_Robots sr = new Classes.cSystems_Robots();
                string result = sr.getAllSchedulesforthisServer();
                var ts = JsonConvert.DeserializeObject<List<Classes.Scheduler>>(result);
                Entities inputEntity = new Entities();
                timer1.Stop();

                new Logger().LogException(" timer1 stopped");

                foreach (Classes.Scheduler ScheduleRobot in ts)
                {
                    string jobid = cr.createProcessManager("0", ScheduleRobot.ID, "Server", null);
                    Variables.jobid = jobid;
                    inputEntity.ID = ScheduleRobot.ID;
                    inputEntity.Name = ScheduleRobot.Name;
                    inputEntity.Project_Id = ScheduleRobot.Project_Id;
                    inputEntity.systemId_pm = jobid;
                    inputEntity.Name = "Server";
                    inputEntity.Type = "Execution";

                    new Logger().LogException(ScheduleRobot.Name + " - " + ScheduleRobot.ID + " bot execution Started");
                    string executerobot = cr.Executerobo(inputEntity);
                    new Logger().LogException(ScheduleRobot.Name + " - " + ScheduleRobot.ID + " bot execution stopped");
                }

                timer1.Start();
                new Logger().LogException(" timer1 started");
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
            }
        }
    }

}