﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web.Http;
using Genbase.Controllers.Services;
using Genbase.Implementation_Dashboard;
using Genbase.Interface_DashBoard;
using Genbase.ServiceImplementation;
using Genbase.ServiceInterfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace Genbase
{
    public class Startup
    {
        //public static string ConnectionString
        //{
        //    get;
        //    private set;
        //}
       
        public Startup(IHostingEnvironment configuration)
        {
            //Configuration = configuration;
            //Configuration = new ConfigurationBuilder().SetBasePath(configuration.ContentRootPath).AddJsonFile("appsettings.json").Build();
            var builder = new ConfigurationBuilder()
            .SetBasePath(configuration.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appsettings.{configuration.EnvironmentName}.json", optional: true)
            .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddHttpContextAccessor();
            services.AddTransient<DashBoardServiceInterfaces, DashBoardServiceImplementations>();
            services.AddTransient<CrudServiceInterfaces, CrudServiceImplementations>();
            services.AddTransient<DesktopServiceInterfaces, DesktopServiceImplementations>();
            services.AddTransient<DesignStudioInterfaces, DesignStudioImplementations>();
            services.AddTransient<ScreenGenerationInterfaces, ScreenGenerationImplementation>();
            services.AddTransient<CrudServiceController>();
            services.AddTransient<DashboardController>();
            services.AddTransient<DesktopServiceController>();
            services.AddTransient<DesignStudioController>();
            services.AddTransient<ScreenGenerationController>();
            services.AddDirectoryBrowser();
            //Swagger for testing the API
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info {
                    Title = "Genbase",
                    Version = "v1",
                    Description= "Genbase .Net Core Web API",
                });
            });
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration["JwtIssuer"],
                        ValidAudience = Configuration["JwtAudience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"])),
                        ClockSkew = TimeSpan.Zero, // remove delay of token when expire
                        RequireExpirationTime = true,
                        ValidateLifetime = true,
                        ValidateAudience = true,
                        ValidateIssuer = true,
                    };
                });

            services.AddCors(options =>
            {
                options.AddPolicy("GenbasePolicy",
                    builder =>
                    {
                        builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                    });

            });
          //  string root = Directory.GetCurrentDirectory() + "\\Uploads";
          //  IFileProvider physicalProvider = new PhysicalFileProvider(root);
           // IFileProvider embeddedProvider = new EmbeddedFileProvider(Assembly.GetEntryAssembly());
            //IFileProvider compositeProvider = new CompositeFileProvider(physicalProvider, embeddedProvider);
         //   services.AddSingleton<IFileProvider>(physicalProvider);
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IHttpContextAccessor accessor)
        {
            if (env.IsDevelopment())
            {
                //string webRoot = env.WebRootPath;
                //File.Delete(webRoot + "\\webroot\\root.json");

                //JObject webRootString = new JObject(
                //    new JProperty("path", webRoot)
                //    );
                //File.WriteAllText(webRoot + "\\webroot\\root.json", webRootString.ToString());

                //using (StreamWriter sw = File.CreateText(webRoot + "\\webroot\\root.json"))
                //using (JsonTextWriter writer = new JsonTextWriter(sw))
                //{
                //    webRootString.WriteTo(writer);
                //}
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            // Use Authentication
            app.UseAuthentication();
            app.UseHttpsRedirection();
            // app.UseDefaultFiles();
            //app.UseStaticFiles();
            //app.UseStaticFiles(new StaticFileOptions
            //{
            //    FileProvider = new PhysicalFileProvider(
            //    Path.Combine(Directory.GetCurrentDirectory(), "Uploads")),
            //    RequestPath = "/Uploads"
            //});
            ////Enable directory browsing
            ///

            //-----> The following code enables static files, default files, and directory browsing of Uploads folder Documents/images etc..

            //app.UseFileServer(new FileServerOptions
            //{
            //    FileProvider = new PhysicalFileProvider(
            //    Path.Combine(Directory.GetCurrentDirectory(), "Uploads")),
            //    RequestPath = "/Uploads",
            //    EnableDirectoryBrowsing = true,
            //    EnableDefaultFiles = true,
            //});
            //app.UseDirectoryBrowser(new DirectoryBrowserOptions
            //{
            //    FileProvider = new PhysicalFileProvider(
            //                Path.Combine(Directory.GetCurrentDirectory(), "generatedSQLScripts")),
            //    RequestPath = "/generatedSQLScripts"
            //});

            app.UseMvc();
            //Use Swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

           // ConnectionString = Configuration["ConnectionStrings:Default"];
        }
       
        //public static string GetConnectionString()
        //{
        //    var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
        //    return Startup.ConnectionString = config.GetConnectionString("Default");
        //}

    }
}
