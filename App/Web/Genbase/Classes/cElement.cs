﻿using Genbase.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Genbase.BLL;
using Genbase.Classes;

namespace Genbase.classes
{
    public class cElement
    {
        DAL.PostgresConnect dbd = new DAL.PostgresConnect();
        public string getElements()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("ELEMENT", "Status|Equals|true", "\"Id\", \"Name\",\"Icon\", \"Description\",\"Category\"", 0, "public", "\"Id\"", "ASC");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }

        }
        public string getElementById(string elemId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("ELEMENT", "Id|Equals|" + elemId + "||Status|Equals|true", "\"Id\", \"Name\",\"Icon\", \"Description\"", 0, "public", "", "");
                List<Elements> elementsret = JsonConvert.DeserializeObject<List<Elements>>(ret);

                string retr = string.Empty;
                foreach (Elements elementret in elementsret)
                {
                    retr = elementret.Name;
                }
                return retr;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getElementDetails(string element)
        {
            return StringHelper.notimplemented;
        }
        public string createLabel(String data)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                return "<label for= \"queue\" class=\"col-sm-3 control-label\">" + data + "</label>";
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string createTextBox(Dictionary<string, string> data)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                return "<div class=\"col-sm-9\"><input type=\"text\" class=\"form-control\" name=\"{Edit/none}Columns\" id=\"" + data["columnName"] + "|" + data["elementId"] + "\" value=\"" + data["defaultValue"] + "\"{Mandatory} {Length}/></div>";
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string createID(Dictionary<string, string> data)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                return "<div class=\"col-sm-9\"><label class=\" control-label\" name=\"{Edit/none}Columns\" id=\"" + data["columnName"] + "|" + data["elementId"] + "\"></label></div>";
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string createCheckBox(Dictionary<string, string> data)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                if (data[StringHelper.defaultvalue].ToString().ToUpper() == StringHelper.ttrue || data[StringHelper.defaultvalue].ToString().ToUpper() == StringHelper.yes || data[StringHelper.defaultvalue].ToString().ToUpper() == StringHelper.check)
                    return "<div class=\"col-sm-9\"><input type=\"checkbox\" name=\"{Edit/none}Columns\" id=\"" + data["columnName"] + "|" + data["elementId"] + "\" checked{Mandatory}/></div>";
                else
                    return "<div class=\"col-sm-9\"><input type=\"checkbox\" name=\"{Edit/none}Columns\" id=\"" + data["columnName"] + "|" + data["elementId"] + "\"{Mandatory}/></div>";
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string createPassword(Dictionary<string, string> data)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                return "<div class=\"col-sm-9\"><input type=\"password\" class=\"form-control\" name=\"{Edit/none}Columns\" id=\"" + data["columnName"] + "|" + data["elementId"] + "\" value=\"" + data["defaultValue"] + "\"{Mandatory}/></div>";
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string createDropDown(Dictionary<string, string> data)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                return "<div class=\"col-sm-9\"><select size=\"1\" name =\"{Edit/none}Columns\" class=\"form-control select2 select" + data["columnName"] + "\" id=\"" + data["columnName"] + "|" + data["elementId"] + "\" value=\"" + data["defaultValue"] + "\"{Mandatory}></select></div>";
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string createTextarea(Dictionary<string, string> data)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                return "<div class=\"col-sm-9\"><textarea rows=\"5\" class=\"form-control\" name=\"{Edit/none}Columns\" id=\"" + data["columnName"] + "|" + data["elementId"] + "\" {Mandatory}>" + data["defaultValue"] + "</textarea></div>";
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string createTextfile(Dictionary<string, string> data)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                return "<div class=\"col-sm-9\"><input type=\"file\" class=\"form-control\" name=\"{Edit/none}Columns\" id=\"" + data["columnName"] + "|" + data["elementId"] + "\" {Mandatory}>" + data["defaultValue"] + " /></div>";
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
    }
}