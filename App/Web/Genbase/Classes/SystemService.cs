﻿using Genbase.BLL;
using Genbase.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genbase.Classes
{
    public class SystemService
    {
        DAL.PostgresConnect dbd = new DAL.PostgresConnect();
        string time = DateTime.Now.ToString();
        public string addSystem(string mac, string ip, string systemName, string UserName, DateTime? prevTime, DateTime? curTime, string systemtype)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (systemtype != string.Empty)
                {
                    string values = systemName + ", " + ip + ", " + mac + ", " + curTime + ", " + UserName + ", Admin, Active, Admin, " + time + ", Admin, " + time + ", true," + systemtype;
                    string ret = new QueryUtil().insertTableData("public", "Systems", values, "", false);
                    return ret;
                }
                else
                {
                    string values = systemName + ", " + ip + ", " + mac + ", " + curTime + ", " + UserName + ", Admin, Active, Admin, " + time + ", Admin, " + time + ", true,User";
                    string ret = new QueryUtil().insertTableData("public", "Systems", values, "", false);
                    return ret;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }

        }
        public string updateSystemTime(string mac, string ip, string systemName, DateTime? prevTime, DateTime? curTime)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().updateTableData("Systems", "MAC_Address|Equals|" + mac, "Binding_Time|Equals|" + curTime + "||UpdateDatetime|Equals|" + time + "||System_Status|Equals|Active", "public", false);
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string addSystemStatus(string mac, string ip, string systemName, DateTime? prevTime, DateTime? curTime)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().updateTableData("Systems", "MAC_Address|Equals|" + mac, "System_Status|Equals|Active", "public", false);
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string deleteSystem(string mac, string ip, string systemName, DateTime? prevTime, DateTime? curTime)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().updateTableData("Systems", "MAC_Address|Equals|" + mac, "System_Status|Equals|Not Active", "public", false);
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getSystemByMAC(string mac, string ip, string systemname)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("Systems", "MAC_Address|Equals|" + mac + "||Name|Equals|" + systemname + "||IP_Address|Equals|" + ip + "", " * ", 0, "public", "", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }

        }
        public string getAllSystems()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("Systems", "", "*", 0, "public", "\"ID\"", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getAllSystemsRobotsList()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string sysRobots = "select \"id\",\"systemname\",\"status\",\"type\",\"IP_Address\", \"MAC_Address\", \"Binding_Time\", \"Username\", \"Password\", \"System_Status\",array_agg(robotid)as \"robots\" from(select a.\"ID\" as \"id\", a.\"Name\" as \"systemname\",a.\"System_Status\" as \"status\", a.\"systemtype\" as \"type\"," +
                                    " a.\"IP_Address\" as \"IP_Address\", a.\"MAC_Address\" as \"MAC_Address\",a.\"Binding_Time\" as \"Binding_Time\",a.\"Username\" as \"Username\",a.\"Password\" as \"Password\",a.\"System_Status\" as \"System_Status\", " +
                                    " b.\"System_Id\" as \"systemid\",b.\"ID\" as \"sno\",b.\"Robot_Id\" as \"robotid\" " +
                                    " FROM public.\"Systems\" as a LEFT JOIN public.\"SystemsRobots\" b ON a.\"ID\"=b.\"System_Id\" " +
                                    " order by b.\"ID\") as controls group by \"id\",\"systemname\",\"status\",\"type\",\"IP_Address\",\"MAC_Address\", \"Binding_Time\", \"Username\", \"Password\", \"System_Status\" order by \"id\" ";
                string srs = dbd.DbExecuteQuery(sysRobots, "select");

                List<systemS> systems = JsonConvert.DeserializeObject<List<systemS>>(srs);

                List<SystemsRobotsList> op = new List<SystemsRobotsList>();

                foreach (systemS system in systems)
                {
                    SystemsRobotsList srl = new SystemsRobotsList();
                    srl.ID = system.id;
                    srl.IP_Address = system.IP_Address;
                    srl.MAC_Address = system.MAC_Address;
                    srl.Username = system.Username;
                    srl.Binding_Time = system.Binding_Time;
                    srl.Name = system.systemname;
                    srl.System_Status = system.status;
                    srl.type = system.type;

                    List<int> robotids = system.robots.ToList();
                    srl.robots = new List<Robots>();
                    foreach (int rbt in robotids)
                    {
                        if (rbt != 0)
                        {
                            string robtret = new QueryUtil().getTableData("ROBOT", "Id|Equals|" + rbt.ToString() + "", "*", 0, "public", "", "");
                            List<Robots> robots = JsonConvert.DeserializeObject<List<Robots>>(robtret);
                            srl.robots.AddRange(robots);
                        }
                        else
                        {

                        }
                    }
                    op.Add(srl);
                }
                string ret = JsonConvert.SerializeObject(op);

                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getAllRobotSystemList()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string robquery = "select * FROM public.\"ROBOT\" where \"Status\" = true";
                List<RobotsSytemsList> robots = JsonConvert.DeserializeObject<List<RobotsSytemsList>>(dbd.DbExecuteQuery(robquery, "select"));

                foreach (RobotsSytemsList robot in robots)
                {
                    string sysrquery = "select * from  public.\"SystemsRobots\" where \"Robot_Id\" = " + robot.Id + "";
                    List<Systems_Robots> sysrs = JsonConvert.DeserializeObject<List<Systems_Robots>>(dbd.DbExecuteQuery(sysrquery, "select"));
                    foreach (Systems_Robots sysr in sysrs)
                    {
                        string sysquery = "select * FROM public.\"Systems\" where \"ID\" = " + sysr.System_Id + " and \"Status\" = true";
                        List<SystemsR> sysers = JsonConvert.DeserializeObject<List<SystemsR>>(dbd.DbExecuteQuery(sysquery, "select"));
                        if (robot.Systems != null && robot.Systems.Count > 0)
                        {
                            robot.Systems.AddRange(sysers);
                        }
                        else
                        {
                            robot.Systems = sysers;
                        }
                    }
                }

                return JsonConvert.SerializeObject(robots);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }

        }
        public class systemS
        {
            public string id { get; set; }
            public string systemname { get; set; }
            public string status { get; set; }
            public string type { get; set; }
            public int[] robots { get; set; }
            public string IP_Address { get; set; }
            public string MAC_Address { get; set; }
            public string Binding_Time { get; set; }
            public string Username { get; set; }
            public string System_Status { get; set; }
        }
    }
}
