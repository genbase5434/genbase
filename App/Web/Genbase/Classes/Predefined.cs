﻿using Genbase.BLL;
using Genbase.Classes;
using Genbase.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Genbase.classes
{
    public class Predefined
    {
        DAL.PostgresConnect dbd = new DAL.PostgresConnect();
        string time = DateTime.Now.ToString();
        public string getPredefindedBotsByCategory()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("Category", "Status|Equals|true", "*", 0, "public", "\"ID\"", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getCategoryProjects(string catid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("PreDefinedRobot", "Category_Id|Equals|" + catid + "||Status |Equals|true", "*", 0, "public", "\"ID\"", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getCategoryRobotSteps(string botid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("PredefinedActions", "PredefinedBot_ID|Equals|" + botid + "||Status |Equals|true", "*", 0, "public", "\"ID\"", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string setPredefindedBots(PredefinedBots bot)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (bot.ID != null && bot.ID != string.Empty)
                {
                    string values = bot.Name + ",robot.png,predefinedRobot, " + bot.RobotType + ",postgres," + time + ",postgres," + time + ",true";
                    string pdBotID = new QueryUtil().insertTableData("public", "PreDefinedRobot", values, "ID", false);
                    foreach (var pdBot in bot.Actions)
                    {
                        values = pdBotID + "," + pdBot.Action_ID + "," + pdBot.ChildActions + "," + pdBot.Icon + ", " + pdBot.Location + ",postgres," + time + ",postgres," + time + ",true";
                        string ret1 = new QueryUtil().insertTableData("public", "PredefinedActions", values, "", false);
                    }

                }
                return "";
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string loadPredefinedBotDetails()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Dictionary<string, string> PredefinedBotDetails = new Dictionary<string, string>();
                PredefinedBotDetails["Categroy"] = new QueryUtil().getTableData("LOB", "Status|Equals|true", "*", 0, "public", "\"ID\"", "");
                PredefinedBotDetails["PredefinedBot"] = new QueryUtil().getTableData("PreDefinedRobot", "Status|Equals|true", "*", 0, "public", "\"ID\"", "");
                PredefinedBotDetails["PredefinedActions"] = new QueryUtil().getTableData("PredefinedActions", "Status|Equals|true", "*", 0, "public", "\"ID\"", "");
                return JsonConvert.SerializeObject(PredefinedBotDetails);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }


    }
}