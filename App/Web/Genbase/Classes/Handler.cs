﻿using ezBot.classes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Policy;
using System.Security;
using System.Security.Cryptography;
using ezBot.BLL;
using Renci.SshNet;

namespace ezBot.Classes
{
    public class Handler
    {
        public class MethodResult
        {
            public bool result { get; set; }
            public List<KeyValuePair<string, string>> message { get; set; }
            public List<DVariables> dynamicVariables { get; set; }
            public List<ELogs> elog { get; set; }
            public StatusModel Status { get; set; }
        }
        public class MethodResultHandler
        {
            ExecutionLog log = new ExecutionLog();
            /// <summary>
            /// 
            /// </summary>
            /// <param name="result"></param>
            /// <param name="messages"></param>
            /// <param name="dynamicVariables"></param>
            /// <returns></returns>
            public MethodResult createResultType(bool result, List<KeyValuePair<string, string>> messages, List<DVariables> dynamicVariables, StatusModel s)
            {
                MethodResult mr = new MethodResult();
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {

                    mr.dynamicVariables = dynamicVariables;
                    mr.message = messages;
                    mr.result = result;
                    mr.Status = s;
                    return mr;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                    }
                    else
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    return mr;
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="ipname"></param>
            /// <param name="port"></param>
            /// <param name="username"></param>
            /// <param name="password"></param>
            /// <returns></returns>
            //public ConnectionInfo CreateConnection(string ipname, int port, string username, string password)
            //{
            //    ConnectionInfo connInfo = new ConnectionInfo(ipname, port, username,
            //        new AuthenticationMethod[] {
            //new PasswordAuthenticationMethod(username, password),
            //    new PrivateKeyAuthenticationMethod(username, new PrivateKeyFile[]
            //    {
            //        //new PrivateKeyFile(PrivateKeyFilePath, "passphrase")
            //    }),
            //    });
            //    try
            //    {
            //        return connInfo;
            //    }
            //    catch (Exception ex)
            //    {
            //        new Logger().LogException(ex);
            //        return connInfo;
            //    }

            //}
            /// <summary>
            /// 
            /// </summary>
            /// <param name="cmd"></param>
            /// <param name="result"></param>
            /// <param name="step"></param>
            public void outputDisplay(SshCommand cmd, IAsyncResult result, Steps step)
            {
                try
                {
                    using (var reader = new StreamReader(cmd.OutputStream, Encoding.UTF8, true, 1024, true))
                    {
                        while (!result.IsCompleted || !reader.EndOfStream)
                        {
                            string line = reader.ReadLine();
                            if (line != null)
                            {
                                log.add2Log(line, step.Robot_Id, step.Name, StringHelper.eventcode);
                            }
                        }
                    }
                    cmd.EndExecute(result);
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="message"></param>
            /// <param name="type"></param>
            /// <returns></returns>
            public KeyValuePair<string, string> createMessage(string message, string type)
            {
                return new KeyValuePair<string, string>(message, type);
            }

            public ConnectionInfo CreateConnection(string ipname, int port, string username, string password)
            {
                ConnectionInfo connInfo = new ConnectionInfo(ipname, port, username,
                new AuthenticationMethod[] {
            new PasswordAuthenticationMethod(username, password),
                new PrivateKeyAuthenticationMethod(username, new PrivateKeyFile[]
                {
                    //new PrivateKeyFile(PrivateKeyFilePath, "passphrase")
                }),
            });
                try
                {
                    return connInfo;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    return connInfo;
                }
                //throw new NotImplementedException();
            }
        }
        public class EncoderDecoderBase64
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="plainText"></param>
            /// <returns></returns>
            public string Base64Encode(string plainText)
            {
                if (plainText == null || plainText.Trim() == string.Empty)
                {
                    plainText = string.Empty;
                }
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
                return System.Convert.ToBase64String(plainTextBytes);
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="base64EncodedData"></param>
            /// <returns></returns>
            public string Base64Decode(string base64EncodedData)
            {
                if (base64EncodedData == null || base64EncodedData.Trim() == string.Empty)
                {
                    base64EncodedData = string.Empty;
                }
                var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
                return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="value"></param>
            /// <returns></returns>
            public bool IsBase64String(string value)
            {
                if (value == null || value.Length == 0 || value.Length % 4 != 0 || value.Contains(' ') || value.Contains('\t') || value.Contains('\r') || value.Contains('\n'))
                    return false;
                var index = value.Length - 1;
                if (value[index] == '=')
                    index--;
                if (value[index] == '=')
                    index--;
                for (var i = 0; i <= index; i++)
                    if (IsInvalid(value[i]))
                        return false;
                return true;
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="value"></param>
            /// <returns></returns>
            private bool IsInvalid(char value)
            {
                var intValue = (Int32)value;
                if (intValue >= 48 && intValue <= 57)
                    return false;
                if (intValue >= 65 && intValue <= 90)
                    return false;
                if (intValue >= 97 && intValue <= 122)
                    return false;
                return intValue != 43 && intValue != 47;
            }
        }
        public class VariableHandler
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="name"></param>
            /// <param name="value"></param>
            /// <param name="status"></param>
            /// <param name="isUpdate"></param>
            /// <returns></returns>
            public bool CreateVariable(string name, dynamic value, string status, bool isUpdate)
            {
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    DVariables d = new DVariables();
                    d.vlstatus = status;
                    d.vlname = name;
                    d.vlvalue = value;

                    if (isUpdate == true && checkVariableExists(name) == true)
                    {
                        foreach (var item in Variables.dynamicvariables.Where(w => w.vlname == name))
                        {
                            item.vlvalue = value;
                        }
                    }
                    else if (isUpdate == false && checkVariableExists(name) == true)
                    {
                        //deletion goes here
                        //creation goes here
                        Variables.dynamicvariables.Remove(d);
                        Variables.dynamicvariables.Add(d);
                    }
                    else
                    {
                        //creation goes here
                        Variables.dynamicvariables.Add(d);
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                    }
                    else
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    return true;
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="name"></param>
            /// <param name="value"></param>
            /// <param name="status"></param>
            /// <param name="isUpdate"></param>
            /// <param name="TYPE"></param>
            /// <returns></returns>
            public bool CreateVariable(string name, dynamic value, string status, bool isUpdate, string TYPE)
            {
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    DVariables d = new DVariables();
                    d.vlname = name;
                    d.vlvalue = value;
                    d.vltype = TYPE;
                    d.vlstatus = StringHelper.truee;
                    if (isUpdate == true && checkVariableExists(name) == true)
                    {
                        foreach (var item in Variables.dynamicvariables.Where(w => w.vlname == name))
                        {
                            item.vlvalue = value;
                            item.vltype = TYPE;
                            item.vlstatus = StringHelper.truee;
                        }
                    }
                    else if (isUpdate == false && checkVariableExists(name) == true)
                    {
                        //deletion goes here
                        //creation goes here
                        Variables.dynamicvariables.Remove(d);
                        Variables.dynamicvariables.Add(d);
                    }
                    else
                    {
                        //creation goes here
                        Variables.dynamicvariables.Add(d);
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                    }
                    else
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    return true;
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="name"></param>
            /// <returns></returns>
            public bool checkVariableExists(string name)
            {
                return Variables.dynamicvariables.Any(w => w.vlname == name);
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="name"></param>
            /// <returns></returns>
            public bool DeleteVariable(string name)
            {
                try
                {
                    foreach (var item in Variables.dynamicvariables.Where(w => w.vlname == name))
                    {
                        Variables.dynamicvariables.Remove(item);
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    return true;
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="name"></param>
            /// <returns></returns>
            public DVariables getVariables(string name)
            {
                
                return Variables.dynamicvariables.Find(w => w.vlname == name);
            }
        }
        public class StepPropertyHandler
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propName"></param>
            /// <param name="step"></param>
            /// <returns></returns>
            public string getPropertiesValue(string propName, Steps step)
            {
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    string propValue = string.Empty;
                    if (step.StepProperties != null)
                    {
                        List<StepProperties> props = step.StepProperties.FindAll(s => s.StepProperty.Trim().ToLower() == propName.ToLower());
                        if (props.Count > 0)
                        {
                            return props[0].StepPropertyValue;
                        }
                        else
                        {
                            return string.Empty;
                        }
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                    }
                    else
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    return ex.Message;
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="propName"></param>
            /// <param name="step"></param>
            /// <param name="propAttr"></param>
            /// <returns></returns>
            public StepProperties getPropertiesValue(string propName, Steps step, string propAttr)
            {
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    string propValue = string.Empty;
                    if (step.StepProperties != null)
                    {
                        List<StepProperties> props = step.StepProperties.FindAll(s => s.StepProperty.ToLower() == propName.ToLower());
                        if (props.Count > 0)
                        {
                            return props[0];
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                    }
                    else
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    return null;
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="properties"></param>
            /// <returns></returns>
            public List<KeyValuePair<string, string>> getPropertiesValue(List<StepProperties> properties)
            {
                string propValue = string.Empty;
                List<KeyValuePair<string, string>> lkvp = new List<KeyValuePair<string, string>>();
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    if (properties != null)
                    {
                        properties.ForEach(prop => lkvp.Add(new KeyValuePair<string, string>(prop.StepProperty, prop.StepPropertyValue)));
                    }
                    else
                    {

                    }
                    return lkvp;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                    }
                    else
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    return lkvp;
                }
            }

            public class StepPropertyValidationHandler
            {
                /// <summary>
                /// 
                /// </summary>
                /// <param name="step"></param>
                /// <returns></returns>
                public bool ValidateProperties(Steps step)
                {
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        if (step.StepProperties != null)
                        {
                            List<StepProperties> props = step.StepProperties.FindAll(s => s.StepPropertyValue.ToLower().Trim() == string.Empty);
                            props.ForEach(d => new Logger().LogException(Messages.EmptyStepProperty.Replace("---", d.StepProperty).Replace(StringHelper.star, step.Name)));
                            props.ForEach(d => new ExecutionLog().add2Log(Messages.EmptyStepProperty.Replace("---", d.StepProperty).Replace(StringHelper.star, step.Name), step.Robot_Id, step.Name, StringHelper.errorcode));
                            if (props.Count > 0)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else
                        {
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return true;
                    }
                }
                public enum PasswordScore
                {
                    Blank = 0,
                    VeryWeak = 1,
                    Weak = 2,
                    Medium = 3,
                    Strong = 4,
                    VeryStrong = 5
                }
                public static PasswordScore CheckStrength(string password)
                {
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    int score = 0;

                    if (password.Length < 1)
                        return PasswordScore.Blank;
                    if (password.Length < 4)
                        return PasswordScore.VeryWeak;

                    if ((password.Length >= 4) && (password.Length < 8))
                        score++;
                    if (password.Length >= 8)
                        score++;

                    Regex rg = new Regex(@"^[a-zA-Z0-9\s,]*$");
                    if (Regex.Match(password, @"^[0-9]*$", RegexOptions.ECMAScript).Success)
                        score++;
                    if (Regex.Match(password, @"^[a-z]*$", RegexOptions.ECMAScript).Success ||
                      Regex.Match(password, @"^[A-Z]*$", RegexOptions.ECMAScript).Success)
                        score++;
                    if (Regex.Match(password, @"/.[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]/", RegexOptions.ECMAScript).Success)
                        score++;
                    if (Regex.Match(password, @"^[a-zA-Z09!@#$%^&*(),]*$", RegexOptions.ECMAScript).Success)
                        return PasswordScore.VeryStrong;
                    return (PasswordScore)score;
                }
                private bool ValidateEmail(string mailid)
                {
                    Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                    Match match = regex.Match(mailid);
                    if (match.Success)
                        return true;
                    else
                        return false;
                }
                public List<StepProperties> ValidateProperties(Steps step, bool returner)
                {
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        if (step.StepProperties != null)
                        {
                            List<StepProperties> stepProps = step.StepProperties;
                            List<StepProperties> props = new List<StepProperties>();
                            string actionid = step.Action_Id;

                            List<ElementProperties> actProps = JsonConvert.DeserializeObject<List<ElementProperties>>(new Action().getActionProperty(actionid));
                            List<ValidationTypes> validation = JsonConvert.DeserializeObject<List<ValidationTypes>>(new Action().getValidations());

                            foreach (StepProperties sp in stepProps)
                            {
                                foreach (ElementProperties actProp in actProps)
                                {
                                    if (sp.Order == actProp.Order)
                                    {
                                        if (actProp.CustomValidation != null)
                                        {
                                            string[] values = actProp.CustomValidation.Split(',');
                                            foreach (var i in values)
                                            {
                                                List<ValidationTypes> validate = validation.FindAll(s => s.ID == i);
                                                if (validate.Count > 0)
                                                {

                                                    switch (validate[0].ValidationName.ToLower())
                                                    {
                                                        case "requiredvalidator":
                                                            if (sp.StepPropertyValue.Trim() == string.Empty)
                                                            {
                                                                props.Add(sp);
                                                            }
                                                            break;
                                                        case "emailvalidator":
                                                            if (!ValidateEmail(sp.StepPropertyValue.Trim()))
                                                            {
                                                                props.Add(sp);
                                                            }
                                                            break;
                                                        case "passwordvalidator":

                                                            var ps = CheckStrength(sp.StepPropertyValue.Trim());
                                                            string scor = ps.ToString().ToLower();
                                                            if ((scor == "blank") || (scor == "veryweak") || (scor == "weak"))
                                                            {
                                                                props.Add(sp);
                                                            }
                                                            break;
                                                    }

                                                }

                                            }
                                        }
                                    }
                                }
                            }

                            props.ForEach(d => new Logger().LogException(Messages.EmptyStepProperty.Replace(StringHelper.dashedlines, d.StepProperty).Replace(StringHelper.star, step.Name)));
                            props.ForEach(d => new ExecutionLog().add2Log(Messages.EmptyStepProperty.Replace(StringHelper.dashedlines, d.StepProperty).Replace(StringHelper.star, step.Name), step.Robot_Id, step.Name, StringHelper.errorcode));

                            return props;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return null;
                    }
                }
            }
            public class TypeHandler
            {
                /// <summary>
                /// 
                /// </summary>
                /// <typeparam name="T"></typeparam>
                /// <param name="items"></param>
                /// <returns></returns>
                public DataTable ToDataTable<T>(List<T> items)
                {
                    DataTable dataTable = new DataTable(typeof(T).Name);
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                        foreach (PropertyInfo prop in Props)
                        {
                            var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                            dataTable.Columns.Add(prop.Name, type);
                        }
                        foreach (T item in items)
                        {
                            var values = new object[Props.Length];
                            for (int i = 0; i < Props.Length; i++)
                            {
                                values[i] = Props[i].GetValue(item, null);
                            }
                            dataTable.Rows.Add(values);
                        }
                        return dataTable;
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return dataTable;
                    }
                }
                /// <summary>
                /// 
                /// </summary>
                /// <typeparam name="T"></typeparam>
                /// <param name="list"></param>
                /// <returns></returns>
                public DataSet ToDataSet<T>(IList<T> list)
                {
                    DataSet ds = new DataSet();
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        Type elementType = typeof(T);
                        DataTable t = new DataTable();
                        ds.Tables.Add(t);

                        //add a column to table for each public property on T
                        foreach (var propInfo in elementType.GetProperties())
                        {
                            Type ColType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

                            t.Columns.Add(propInfo.Name, ColType);
                        }

                        //go through each property on T and add each value to the table
                        foreach (T item in list)
                        {
                            DataRow row = t.NewRow();

                            foreach (var propInfo in elementType.GetProperties())
                            {
                                row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                            }

                            t.Rows.Add(row);
                        }

                        return ds;
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return ds;
                    }
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="text"></param>
                /// <returns></returns>
                public string StringtoJsonD(string text)
                {
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        List<string> rows = new List<string>();
                        List<string> HeaderRow = new List<string>();

                        //text = text.Replace("?", "q"); text = text.Replace('?', 'q');

                        text = text.TrimStart();
                        //rows
                        if (text.IndexOf("\n") > -1)
                        {
                            rows = text.Split(new string[] { "\n" }, StringSplitOptions.None).ToList();
                        }
                        DataTable dt = new DataTable();


                        dt.Columns.Add(" ");
                        dt.Columns.Add(rows[0]);
                        foreach (string row in rows)
                        {
                            DataRow workRow = dt.NewRow();

                            string val = row.Trim();
                            if (!string.IsNullOrEmpty(val))
                            {
                                workRow[0] = "";
                                workRow[1] = row;
                                dt.Rows.Add(workRow);
                            }
                        }
                        return JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return ex.Message;
                    }
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="text"></param>
                /// <returns></returns>
                public string StringtoJson(string text)
                {
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        List<string> rows = new List<string>();
                        List<string> HeaderRow = new List<string>();
                        text = text.TrimStart();
                        //rows
                        if (text.IndexOf("\n") > -1)
                        {
                            rows = text.Split(new string[] { "\n" }, StringSplitOptions.None).ToList();
                        }
                        //HeaderRow
                        if (rows[0].IndexOf(" ") > -1)
                        {
                            HeaderRow = rows[0].Split(new string[] { " ", "  ", "   ", "  " }, StringSplitOptions.None).ToList();
                        }
                        HeaderRow.ForEach(x => x.Trim()); HeaderRow.RemoveAll(x => x == string.Empty);


                        DataTable dt = new DataTable(); dt.Columns.Add(" ");
                        foreach (string head in HeaderRow)
                        {
                            dt.Columns.Add(head);
                        }

                        foreach (string row in rows)
                        {
                            List<string> rower = new List<string>();
                            rower = row.Split(new string[] { " ", "  ", "   ", "  " }, StringSplitOptions.None).ToList();
                            rower.ForEach(x => x.Trim()); rower.RemoveAll(x => x == string.Empty);
                            rower.Insert(0, "");
                            dt.Rows.Add(rower.ToArray());
                        }
                        return JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return ex.Message;
                    }
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="strInput"></param>
                /// <returns></returns>
                public bool IsValidJson(string strInput)
                {
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        strInput = strInput.Trim();
                        if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                            (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
                        {
                            try
                            {
                                var obj = JToken.Parse(strInput);
                                return true;
                            }
                            catch (JsonReaderException jex)
                            {
                                //Exception in parsing json
                                new Logger().LogException(jex);
                                return false;
                            }
                            catch (Exception ex) //some other exception
                            {
                                new Logger().LogException(ex);
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return false;
                    }
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="strInput"></param>
                /// <returns></returns>
                public bool IsFormattedString(string strInput)
                {
                    try
                    {
                        strInput = strInput.Trim();
                        if (strInput.StartsWith("{") && strInput.EndsWith("}")) //For object
                        {
                            try
                            {
                                return true;
                            }
                            catch (Exception ex) //some other exception
                            {
                                new Logger().LogException(ex);
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        return false;
                    }
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="formattedstring"></param>
                /// <returns></returns>
                public string ConvertFormattedString(string formattedstring)
                {
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        string returns = string.Empty;

                        List<string> varslist1 = formattedstring.Split('}').ToList();
                        varslist1.RemoveAll(x => x.Trim() == string.Empty);
                        string newString = formattedstring;
                        List<string> varslist2 = new List<string>();

                        foreach (string varsin in varslist1)
                        {
                            varslist2.Add(varsin.Split('{')[1].Trim());
                        }

                        StringBuilder sr = new StringBuilder(newString);

                        foreach (string varsin in varslist2)
                        {
                            DVariables value = new VariableHandler().getVariables(varsin);
                            if (value != null && value.vlvalue.GetType() == typeof(string))
                            {
                                sr.Replace("{" + varsin + "}", value.vlvalue);
                            }
                        }

                        returns = sr.ToString();
                        return returns;
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return ex.Message;
                    }
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="input"></param>
                /// <param name="inputtype"></param>
                /// <returns></returns>
                public DataTable getInput(string input, string inputtype)
                {
                    DataTable returnValue = new DataTable();

                    if (inputtype.Trim().ToLower() == StringHelper.value)
                    {
                        returnValue.Columns.Add("Output");
                        returnValue.Rows.Add(input);
                        foreach (var l in Variables.dynamicvariables)
                        {
                            if ((l.vlname == input) && (l.vlstatus == StringHelper.truee))
                            {
                                l.vlvalue = JsonConvert.SerializeObject(returnValue);
                            }
                        }
                    }
                    else if (inputtype.Trim().ToLower() == "reference")
                    {
                        returnValue.Columns.Add("Output");
                        returnValue.Rows.Add(input);
                    }
                    else if (inputtype.Trim().ToLower() == StringHelper.variable)
                    {
                        VariableHandler vh = new VariableHandler();
                        if (vh.checkVariableExists(input))
                        {
                            DVariables dv = vh.getVariables(input);
                            if (dv.vltype != null && dv.vltype.ToLower() == "datatable")
                            {
                                returnValue = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                            }
                            else
                            {
                                if ((dv.vlvalue.StartsWith("{") && dv.vlvalue.EndsWith("}")) || (dv.vlvalue.StartsWith("[") && dv.vlvalue.EndsWith("]")))
                                {
                                    returnValue = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                                }
                                else
                                {
                                    string val = dv.vlvalue;
                                    returnValue.Columns.Add("Output");
                                    returnValue.Rows.Add(val);
                                    foreach (var l in Variables.dynamicvariables)
                                    {
                                        if ((l.vlname == input) && (l.vlstatus == StringHelper.truee))
                                        {
                                            l.vlvalue = JsonConvert.SerializeObject(returnValue);
                                        }
                                    }
                                }
                            }
                        }
                        else if (input.IndexOf("[") > -1 && input.IndexOf("]") > -1)
                        {
                            string returns = string.Empty; DataTable inpt = new DataTable();
                            string itemName = input.Split('[')[0].Trim();
                            string itemNumber = input.Split('[')[1].Split(']')[0].Trim().Replace("\"", "").Replace("'", "");
                            returnValue.Columns.Add("Output");

                            if (new VariableHandler().checkVariableExists(itemName))
                            {
                                DVariables dvs = new DVariables();
                                inpt = getInput(itemName, "variable");

                                int y = 0;
                                if (int.TryParse(itemNumber, out y))
                                {
                                    returns = inpt.Rows[y][0].ToString();
                                }
                                else
                                {
                                    returns = getColumnAsListFromDataTable(inpt, itemNumber);
                                    dynamic res = JsonConvert.DeserializeObject<dynamic>(JsonConvert.DeserializeObject<List<dynamic>>(returns)[0]);
                                    for (int h = 2; h < input.Split('[').Length; h++)
                                    {
                                        itemNumber = input.Split('[')[h].Split(']')[0].Trim().Replace("\"", "").Replace("'", "");
                                        res = ReturnDynamicKeyValue(res, itemNumber);
                                    }
                                    int r = 0;
                                    try
                                    {
                                        while (r < res.Count)
                                        {
                                            returnValue.Rows.Add(res[r]);
                                            r++;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        if (res != null)
                                            returnValue.Rows.Add(res.Value);
                                    }
                                }
                            }
                            else
                            {
                                returns = input;
                            }
                        }
                        else
                        {
                            returnValue.Columns.Add("Output");
                            returnValue.Rows.Add(input);
                        }
                    }
                    return returnValue;
                }
                public string getInputJSON(string input)
                {
                    string inp = input;
                    try
                    {
                        if (input.Contains("+{") && input.Contains("}+"))
                        {
                            string[] inputList = input.Split(new[] { "+{", "}+" }, StringSplitOptions.None);
                            input = "";
                            foreach (var l in inputList)
                            {
                                DataTable dt = getInput(l, "variable");
                                foreach (DataRow dr in dt.Rows)
                                {
                                    input += dr[0].ToString();
                                }
                            }
                            return input;
                        }
                        else
                            return inp;
                    }
                    catch (Exception e)
                    {
                        return inp;
                    }
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="input"></param>
                /// <returns></returns>
                /// 
                public DataTable getInput(string input)
                {
                    DataTable returnValue = new DataTable();
                    VariableHandler vh = new VariableHandler();
                    if ((input.IndexOf("{") > -1 && input.IndexOf("}") > -1))
                    {
                        if (vh.checkVariableExists(input))
                        {
                            DVariables dv = vh.getVariables(input);
                            if (dv.vltype != null && dv.vltype.ToLower() == "datatable")
                            {
                                returnValue = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                            }
                            else
                            {
                                if ((dv.vlvalue.StartsWith("{") && dv.vlvalue.EndsWith("}")) || (dv.vlvalue.StartsWith("[") && dv.vlvalue.EndsWith("]")))
                                {
                                    returnValue = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                                }
                                else
                                {
                                    string val = dv.vlvalue;
                                    returnValue.Columns.Add("Output");
                                    returnValue.Rows.Add(val);
                                    foreach (var l in Variables.dynamicvariables)
                                    {
                                        if ((l.vlname == input) && (l.vlstatus == StringHelper.truee))
                                        {
                                            l.vlvalue = JsonConvert.SerializeObject(returnValue);
                                        }
                                    }
                                }
                            }
                        }
                        else if ((input.IndexOf("[") > -1 && input.IndexOf("]") > -1))
                        {
                            List<string> SplitInputs = input.Split(']').ToList();
                            List<string> SelectColumn = new List<string>();
                            SelectColumn = SplitInputs[0].Split('[').ToList();
                            string inputv = SelectColumn[0].ToString() + "}";
                            if (vh.checkVariableExists(inputv))
                            {
                                if (vh.getVariables(inputv).vltype.ToLower() == "datatable")
                                {
                                    DVariables dv = vh.getVariables(inputv);
                                    if (dv.vltype != null && dv.vltype.ToLower() == "datatable")
                                    {
                                        returnValue = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                                    }
                                }
                                else
                                {
                                    returnValue.Columns.Add("Output");
                                    returnValue.Rows.Add(input);
                                    foreach (var l in Variables.dynamicvariables)
                                    {
                                        if ((l.vlname == input) && (l.vlstatus == StringHelper.truee))
                                        {
                                            l.vlvalue = JsonConvert.SerializeObject(returnValue);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        returnValue.Columns.Add("Output");
                        returnValue.Rows.Add(input);
                        foreach (var l in Variables.dynamicvariables)
                        {
                            if ((l.vlname == input) && (l.vlstatus == StringHelper.truee))
                            {
                                l.vlvalue = JsonConvert.SerializeObject(returnValue);
                            }
                        }
                    }
                    return returnValue;
                }
                public dynamic ReturnDynamicKeyValue(dynamic res, dynamic key)
                {
                    try
                    {
                        res = res[Convert.ToInt32(key)];
                        return res;
                    }
                    catch (Exception e)
                    {
                        string ex = e.Message;
                        return res[key];
                    }
                }
                /// <summary>
                /// Get a Value for a given Variable 
                /// </summary>
                /// <param name="input">Variable</param>
                /// <param name="isColumn">Whether to Extract a 1D Datatable from 2D Datatable based on Column when true or Row when False</param>
                /// <returns></returns>
                public DataTable getInputValue(string input, bool isColumn)
                {
                    DataTable returnValue = new DataTable();
                    VariableHandler vh = new VariableHandler();
                    StringFunction sh = new StringFunction();

                    input = input.Trim();
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        if (input.IndexOf("{") > -1 && input.IndexOf("}") > -1) //variable 
                        {
                            if (input.IndexOf("[") > -1 && input.IndexOf("]") > -1) //indexed variable {a[p]} / {a[p][q]}
                            {
                                int squaresopencount = Regex.Matches(input, "[[]").Count;
                                int squaresclosecount = Regex.Matches(input, "[]]").Count;
                                int flowersopencount = Regex.Matches(input, "[{]").Count;
                                int flowersclosecount = Regex.Matches(input, "[}]").Count;

                                if (squaresclosecount == squaresopencount && flowersclosecount == flowersopencount)
                                {
                                    int squarestart = input.IndexOf("["); int squarestop = input.IndexOf("]");

                                    string variableName = input.Substring(0, squarestart) + "}";
                                    DVariables dv = vh.getVariables(variableName);

                                    DataTable dt = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);

                                    if (squaresopencount == 1 && squaresclosecount == 1) //indexed variable 1D {a[p]}
                                    {
                                        string indexName = input.Substring(squarestart); //column index
                                        indexName = sh.removeString(indexName, "["); indexName = sh.removeString(indexName, "]}");

                                        int index = 0;
                                        if (int.TryParse(indexName, out index))
                                        {

                                        }

                                        if (isColumn == true)
                                        {
                                            returnValue.Columns.Add(dt.Columns[index].ColumnName, dt.Columns[index].DataType);

                                            foreach (DataRow dr in dt.Rows)
                                            {
                                                returnValue.Rows.Add(dr[index]);
                                            }
                                        }
                                        else
                                        {
                                            foreach (DataColumn dc in dt.Columns)
                                            {
                                                returnValue.Columns.Add(dc.ColumnName, dc.DataType);
                                            }

                                            returnValue.Rows.Add(dt.Rows[index]);
                                        }
                                    }
                                    else //indexed variable 2D {a[p][q]}
                                    {
                                        string IndCollString = input.Substring(squarestart);

                                        string rowString = IndCollString.Split(new string[] { "][" }, StringSplitOptions.None).ToList()[0];
                                        string columnString = IndCollString.Split(new string[] { "][" }, StringSplitOptions.None).ToList()[1];

                                        rowString = sh.removeString(rowString, "[");
                                        columnString = sh.removeString(columnString, "]"); columnString = sh.removeString(columnString, "}");

                                        int row = 0;
                                        int column = 0;

                                        if (int.TryParse(rowString, out row))
                                        {
                                            if (int.TryParse(columnString, out column))
                                            {

                                            }
                                        }

                                        returnValue.Columns.Add(dt.Columns[column].ColumnName, dt.Columns[column].DataType);

                                        returnValue.Rows.Add(dt.Rows[row][column]);
                                    }
                                }
                                else //improper variable
                                {

                                }
                            }
                            else //unindexed variable {a}
                            {
                                string variableName = input;
                                DVariables dv = vh.getVariables(variableName);
                                returnValue = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);
                            }
                        }
                        else //value a
                        {
                            returnValue.Columns.Add("Content");
                            returnValue.Rows.Add(input);
                        }

                        return returnValue;
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return null;
                    }


                }
                /// <summary>
                /// Convert a DataTable with single Column(1D) to DataTable with Multiple Columns(2D) Based on Seperator
                /// </summary>
                /// <param name="input">Variable</param>
                /// <param name="seperator">Seperator</param>
                /// <param name="inputColumnName">ColumnName in input to be extracted as 1D List Datatable</param>
                /// <param name="ResultColumnNames">Resultant Datatable ColumnNames</param>
                /// <returns></returns>
                public DataTable getInputValue(string input, string seperator, string inputColumnName, params string[] ResultColumnNames)
                {
                    DataTable returnValue = new DataTable();
                    VariableHandler vh = new VariableHandler();
                    StringFunction sh = new StringFunction();

                    input = input.Trim();

                    if (input.IndexOf("{") > -1 && input.IndexOf("}") > -1) //variable 
                    {
                        if (input.IndexOf("[") > -1 && input.IndexOf("]") > -1) //indexed variable {a[p]}
                        {
                            int squaresopencount = Regex.Matches(input, "[[]").Count;
                            int squaresclosecount = Regex.Matches(input, "[]]").Count;
                            int flowersopencount = Regex.Matches(input, "[{]").Count;
                            int flowersclosecount = Regex.Matches(input, "[}]").Count;

                            if (squaresclosecount == squaresopencount && flowersclosecount == flowersopencount)
                            {
                                int squarestart = input.IndexOf("["); int squarestop = input.IndexOf("]");

                                string variableName = input.Substring(0, squarestart) + "}";

                                DVariables dv = vh.getVariables(variableName);
                                DataTable dt = JsonConvert.DeserializeObject<DataTable>(dv.vlvalue);

                                if (squaresopencount == 1 && squaresclosecount == 1) //indexed variable 1D {a[p]}
                                {
                                    string indexName = input.Substring(squarestart); //column index
                                    indexName = sh.removeString(indexName, "["); indexName = sh.removeString(indexName, "]}");

                                    int index = 0;
                                    if (int.TryParse(indexName, out index))
                                    {

                                    }


                                    List<string> dtls = new List<string>(); //get datatable column as List
                                    if (inputColumnName.Trim() != string.Empty)
                                    {
                                        dtls = dt.AsEnumerable().Select(x => x[inputColumnName].ToString()).ToList();
                                    }
                                    else
                                    {
                                        dtls = dt.AsEnumerable().Select(x => x[0].ToString()).ToList();
                                    }

                                    List<string> row = new List<string>();
                                    foreach (string dtl in dtls) //build a 2D DataTable with a List based on Seperator
                                    {
                                        if (dtl.Trim() != seperator)
                                        {
                                            row.Add(dtl);
                                        }
                                        else
                                        {
                                            if (ResultColumnNames != null && ResultColumnNames.Length > 0)
                                            {
                                                ResultColumnNames.ToList().ForEach(cn => returnValue.Columns.Add(cn));
                                                if (ResultColumnNames.Length == row.Count)
                                                {
                                                    returnValue.Rows.Add(row.ToArray());
                                                }
                                            }
                                            else
                                            {
                                                row.ForEach(cn => returnValue.Columns.Add());
                                                if (returnValue.Columns.Count == row.Count)
                                                {
                                                    returnValue.Rows.Add(row.ToArray());
                                                }
                                            }
                                            row.Clear();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return returnValue;
                }

                /// <summary>
                /// 
                /// </summary>
                /// <typeparam name="T"></typeparam>
                /// <param name="list"></param>
                /// <returns></returns>
                public DataTable CreateDataTable<T>(IEnumerable<T> list)
                {
                    Type type = typeof(T);
                    var properties = type.GetProperties();

                    DataTable dataTable = new DataTable();
                    foreach (PropertyInfo info in properties)
                    {
                        dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
                    }
                    foreach (T entity in list)
                    {
                        object[] values = new object[properties.Length];
                        for (int i = 0; i < properties.Length; i++)
                        {
                            values[i] = properties[i].GetValue(entity);
                        }

                        dataTable.Rows.Add(values);
                    }

                    return dataTable;
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="type"></param>
                /// <returns></returns>
                public IEnumerable<PropertyInfo> FlattenType(Type type)
                {
                    var properties = type.GetProperties();
                    foreach (PropertyInfo info in properties)
                    {
                        if (info.PropertyType.Assembly.GetName().Name == "mscorlib")
                        {
                            yield return info;
                        }
                        else
                        {
                            foreach (var childInfo in FlattenType(info.PropertyType))
                            {
                                yield return childInfo;
                            }
                        }
                    }
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="dataTable"></param>
                /// <returns></returns>
                public string cDataTableToString(DataTable dataTable)
                {
                    if (dataTable != null)
                    {
                        var output = new StringBuilder();
                        foreach (DataRow row in dataTable.Rows)
                        {
                            for (int i = 0; i < dataTable.Columns.Count; i++)
                            {
                                output.AppendLine(dataTable.Columns[i].ColumnName + ":" + row[i]);
                            }
                            output.AppendLine();
                        }
                        return output.ToString();
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="dataTable"></param>
                /// <returns></returns>
                public string ConvertDataTableToString(DataTable dataTable)
                {
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        if (dataTable != null)
                        {
                            var output = new StringBuilder();

                            var columnsWidths = new int[dataTable.Columns.Count];

                            // Get column widths
                            foreach (DataRow row in dataTable.Rows)
                            {
                                for (int i = 0; i < dataTable.Columns.Count; i++)
                                {
                                    var length = row[i].ToString().Length;
                                    if (columnsWidths[i] < length)
                                        columnsWidths[i] = length;
                                }
                            }

                            // Get Column Titles
                            for (int i = 0; i < dataTable.Columns.Count; i++)
                            {
                                var length = dataTable.Columns[i].ColumnName.Length;
                                if (columnsWidths[i] < length)
                                    columnsWidths[i] = length;
                            }

                            // Write Column titles
                            for (int i = 0; i < dataTable.Columns.Count; i++)
                            {
                                var text = dataTable.Columns[i].ColumnName;
                                output.Append("|" + PadCenter(text, columnsWidths[i] + 2));
                            }
                            output.Append("|\n" + new string('=', output.Length) + "\n");

                            // Write Rows
                            foreach (DataRow row in dataTable.Rows)
                            {
                                for (int i = 0; i < dataTable.Columns.Count; i++)
                                {
                                    var text = row[i].ToString();
                                    output.Append("|" + PadCenter(text, columnsWidths[i] + 2));
                                }
                                output.Append("|\n");
                            }
                            return output.ToString();
                        }
                        else
                        {
                            return string.Empty;
                        }
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return null;
                    }

                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="text"></param>
                /// <param name="maxLength"></param>
                /// <returns></returns>
                private string PadCenter(string text, int maxLength)
                {
                    int diff = maxLength - text.Length;
                    return new string(' ', diff / 2) + text + new string(' ', (int)(diff / 2.0 + 0.5));

                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="itemArray"></param>
                /// <returns></returns>
                public string getItemfromArray(string itemArray)
                {
                    string returns = string.Empty; DataTable inpt = new DataTable();
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        if (itemArray.IndexOf("[") > -1 && itemArray.IndexOf("]") > -1)
                        {
                            string itemName = itemArray.Split('[')[0].Trim();
                            string itemNumber = itemArray.Split('[')[1].Split(']')[0].Trim().Replace("\"", "").Replace("'", "");

                            if (new VariableHandler().checkVariableExists(itemName))
                            {
                                DVariables dvs = new DVariables();
                                inpt = new TypeHandler().getInput(itemName, StringHelper.variable);

                                int y = 0;
                                if (int.TryParse(itemNumber, out y))
                                {
                                    returns = inpt.Rows[y][0].ToString();
                                }
                                else
                                {
                                    returns = getColumnAsListFromDataTable(inpt, itemNumber);
                                }
                            }
                            else
                            {
                                returns = itemArray;
                            }
                        }
                        else
                        {
                            if (new VariableHandler().checkVariableExists(itemArray))
                            {
                                returns = JsonConvert.SerializeObject(new TypeHandler().getInput(itemArray, StringHelper.variable));
                            }
                            else
                            {
                                returns = itemArray;
                            }
                        }
                        return returns;
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return null;
                    }
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="list"></param>
                /// <returns></returns>
                public DataTable ListtoDatatable(List<string> list)
                {
                    DataTable dataTable = new DataTable();
                    dataTable.Columns.Add("0");
                    foreach (var l in list)
                    {
                        dataTable.Rows.Add(l);
                    }

                    return dataTable;

                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="input"></param>
                /// <param name="InputColumn"></param>
                /// <returns></returns>
                public DataTable OneDConversionsVariable(string input, string InputColumn)
                {
                    DataTable returnValue = new DataTable();
                    DataTable dtr = new DataTable();
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        dtr = getInput(input);
                        List<string> final = new List<string>();
                        if (InputColumn != null)
                        {
                            foreach (DataRow dr in dtr.Rows)
                            {
                                try
                                {
                                    int n;
                                    if (int.TryParse(InputColumn, out n))
                                        final.Add(dr[Convert.ToInt32(InputColumn)].ToString());
                                    else
                                        final.Add(dr[InputColumn].ToString());

                                }
                                catch (Exception e)
                                {
                                    string ex = e.Message;
                                    final.Add(dr[InputColumn].ToString());
                                }


                            }

                            returnValue = ListtoDatatable(final);
                        }
                        else
                        {
                            foreach (DataRow dr in dtr.Rows)
                            {
                                final.Add(dr[0].ToString());
                            }

                            returnValue = ListtoDatatable(final);
                        }
                    }
                    catch (Exception ex)
                    {
                        string y = ex.Message;
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                    }
                    return returnValue;
                }
                public class CurrentWindowsSystemFacts
                {
                    /// <summary>
                    /// 
                    /// </summary>
                    /// <returns></returns>
                    public DetailsList getSystemDetails()
                    {
                        List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                        try
                        {
                            DetailsList dl = new DetailsList();
                            dl.Username = Environment.UserName;
                            dl.MacAddress = NetworkInterface.GetAllNetworkInterfaces().Where(nic => nic.OperationalStatus == OperationalStatus.Up && nic.NetworkInterfaceType != NetworkInterfaceType.Loopback).Select(nic => nic.GetPhysicalAddress().ToString()).FirstOrDefault();

                            dl.IPAddress = string.Empty;

                            foreach (var ip in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
                            {
                                if (ip.AddressFamily == AddressFamily.InterNetwork)
                                {
                                    dl.IPAddress = ip.ToString();
                                }
                            }
                            dl.SystemName = System.Environment.MachineName;


                            return dl;
                        }
                        catch (Exception ex)
                        {
                            string ht = ex.Message;
                            if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                            }
                            else
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                            }
                            return null;
                        }
                    }
                    /// <summary>
                    /// 
                    /// </summary>
                    /// <param name="SERVICENAME"></param>
                    /// <returns></returns>
                    public string thisSystemServices(string SERVICENAME)
                    {
                        List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                        try
                        {
                            ServiceController sc = new ServiceController(SERVICENAME);

                            switch (sc.Status)
                            {
                                case ServiceControllerStatus.Running:
                                    return StringHelper.Running;
                                case ServiceControllerStatus.Stopped:
                                    return StringHelper.Stopped;
                                case ServiceControllerStatus.Paused:
                                    return StringHelper.Paused;
                                case ServiceControllerStatus.StopPending:
                                    return StringHelper.Stopping;
                                case ServiceControllerStatus.StartPending:
                                    return StringHelper.Starting;
                                default:
                                    return StringHelper.StatusChanging;
                            }
                        }
                        catch (Exception ex)
                        {
                            string htr = ex.Message;
                            if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                            }
                            else
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                            }
                            return "No Such Service Exists";
                        }
                    }
                    /// <summary>
                    /// 
                    /// </summary>
                    /// <param name="SERVICENAME"></param>
                    /// <param name="MACHINENAME"></param>
                    /// <returns></returns>
                    public string remoteSystemServices(string SERVICENAME, string MACHINENAME)
                    {
                        try
                        {
                            ServiceController sc = new ServiceController(SERVICENAME, MACHINENAME);
                            switch (sc.Status)
                            {
                                case ServiceControllerStatus.Running:
                                    return StringHelper.Running;
                                case ServiceControllerStatus.Stopped:
                                    return StringHelper.Stopped;
                                case ServiceControllerStatus.Paused:
                                    return StringHelper.Paused;
                                case ServiceControllerStatus.StopPending:
                                    return StringHelper.Stopping;
                                case ServiceControllerStatus.StartPending:
                                    return StringHelper.Starting;
                                default:
                                    return StringHelper.StatusChanging;
                            }
                        }
                        catch (Exception ex)
                        {
                            string htr = ex.Message;
                            return "No Such Service Exists";
                        }
                    }
                    public class DetailsList
                    {
                        public string Username { get; set; }
                        public string MacAddress { get; set; }
                        public string IPAddress { get; set; }
                        public string SystemName { get; set; }
                    }
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="input"></param>
                /// <param name="inputRow"></param>
                /// <param name="inputColumn"></param>
                /// <returns></returns>
                public DataTable TwoDScalarConversionVariable(string input, string inputRow, string inputColumn)
                {
                    DataTable returnValue = new DataTable();
                    DataTable dtr = new DataTable();
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        dtr = getInput(input);
                        List<string> final = new List<string>();
                        if (inputColumn != null && inputRow != null)
                        {
                            final.Add(dtr.Rows[Int32.Parse(inputRow)][Int32.Parse(inputColumn)].ToString());
                        }
                        else
                        {
                            final.Add(dtr.Rows[0][0].ToString());
                        }
                        returnValue = ListtoDatatable(final);
                    }
                    catch (Exception ex)
                    {
                        string e = ex.Message;
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                    }
                    return returnValue;
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="input"></param>
                /// <param name="inputRow"></param>
                /// <returns></returns>
                public DataTable OneDScalarConversionVariable(string input, string inputRow)
                {
                    DataTable returnValue = new DataTable();
                    DataTable dtr = new DataTable();
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        dtr = getInput(input);
                        List<string> final = new List<string>();
                        if (inputRow != null)
                        {
                            final.Add(dtr.Rows[Int32.Parse(inputRow)][0].ToString());
                        }
                        else
                        {
                            final.Add(dtr.Rows[0][0].ToString());
                        }
                        returnValue = ListtoDatatable(final);
                    }
                    catch (Exception ex)
                    {
                        string y = ex.Message;
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                    }
                    return returnValue;
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="input"></param>
                /// <param name="TwoDValue"></param>
                /// <returns></returns>
                public DataTable OneD2DConversionVariable(string input, string TwoDValue)
                {
                    DataTable returnValue = new DataTable();
                    DataTable returnValue1 = new DataTable();
                    DataTable dtr = new DataTable();
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        dtr = getInput(input);
                        List<string> final = new List<string>();
                        foreach (DataRow dr in dtr.Rows)
                        {
                            final.Add(dr[0].ToString());
                        }
                        int count = 0;
                        foreach (var k in final)
                        {
                            count++;

                            if (count > Int32.Parse(TwoDValue))
                            {
                                DataColumnCollection columns = returnValue.Columns;
                                if (columns.Contains("0"))
                                {
                                    returnValue.Rows.Add(k);
                                }
                                else
                                {
                                    returnValue.Columns.Add("0");
                                    returnValue.Rows.Add(k);
                                }
                            }
                            else
                            {
                                DataColumnCollection columns = returnValue1.Columns;
                                if (columns.Contains("0"))
                                {
                                    returnValue1.Rows.Add(k);
                                }
                                else
                                {
                                    returnValue1.Columns.Add("0");
                                    returnValue1.Rows.Add(k);
                                }
                            }
                        }
                        returnValue1 = returnValue.Clone();
                    }
                    catch (Exception ex)
                    {
                        string y = ex.Message;
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                    }
                    return returnValue1;
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="dt"></param>
                /// <param name="columnName"></param>
                /// <returns></returns>
                public string getColumnAsListFromDataTable(DataTable dt, string columnName)
                {
                    List<string> returns = new List<string>();
                    returns.AddRange(dt.AsEnumerable().Select(r => r.Field<string>(columnName)).ToList());

                    return JsonConvert.SerializeObject(returns);
                }
            }
            public class StringFunction
            {
                /// <summary>
                /// 
                /// </summary>
                /// <param name="baseString"></param>
                /// <param name="subString"></param>
                /// <returns></returns>
                public int countSubstring(string baseString, string subString)
                {
                    return Regex.Matches(baseString, subString).Count;
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="baseString"></param>
                /// <param name="splitString"></param>
                /// <returns></returns>
                public List<string> splitString2List(string baseString, string splitString)
                {
                    return baseString.Split(new string[] { splitString }, StringSplitOptions.None).ToList();
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="baseString"></param>
                /// <param name="subString"></param>
                /// <returns></returns>
                public bool isSubString(string baseString, string subString)
                {
                    return baseString.IndexOf(subString) > -1;
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="baseString"></param>
                /// <param name="subString"></param>
                /// <returns></returns>
                public string removeString(string baseString, string subString)
                {
                    int index = baseString.IndexOf(subString, StringComparison.Ordinal);
                    return (index < 0)
                        ? baseString
                        : baseString.Remove(index, subString.Length);
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="json"></param>
                public void printKeysAndValues(string json)
                {
                    var jobject = (JObject)((JArray)JsonConvert.DeserializeObject(json))[0];

                    List<KeyValuePair<object, object>> kvp = new List<KeyValuePair<object, object>>();

                    foreach (var jproperty in jobject.Properties())
                    {
                        //Console.WriteLine("{0} - {1}", jproperty.Name, jproperty.Value);
                        kvp.Add(new KeyValuePair<object, object>(jproperty.Name, jproperty.Value));
                    }

                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="path"></param>
                /// <returns></returns>
                public string getFileNameFromPath(string path)
                {
                    List<string> filenameparts = new StringFunction().splitString2List(path, "\\");
                    string filename = filenameparts[filenameparts.Count - 1];
                    return filename;
                }
                /// <summary>
                /// 
                /// </summary>
                /// <returns></returns>
                public string getApplicationFolderPath()
                {
                    string path = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)));
                    path = path.Substring(6);
                    return path;
                }
                /// <summary>
                /// 
                /// </summary>
                /// <returns></returns>
                public string getApplicationRootFolderPath()
                {
                    return AppDomain.CurrentDomain.BaseDirectory;
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="datetime"></param>
                /// <param name="format"></param>
                /// <returns></returns>
                public string getDateInFormat(DateTime datetime, string format)
                {
                    string result = string.Empty;
                    try
                    {
                        result = datetime.ToString(format);
                    }
                    catch (Exception ex)
                    {
                        string exceptionresult = ex.Message;
                    }
                    return result;
                }
                /// <summary>
                /// 
                /// </summary>
                /// <param name="Text"></param>
                /// <param name="seperator1"></param>
                /// <param name="seperator2"></param>
                /// <returns></returns>
                public List<string> getStringPart(string Text, string seperator1, string seperator2)
                {
                    List<string> finds = new List<string>();
                    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                    try
                    {
                        int counter = 0;

                        foreach (string sampling in Text.Split(new string[] { seperator1 }, StringSplitOptions.None))
                        {
                            if (counter == 0)
                            {

                            }
                            else
                            {
                                Regex reg = new Regex("[;\\\\/:*?\"<>|&']");
                                string str = reg.Replace(sampling, string.Empty);

                                //split at spaces

                                string result = str.Split(new string[] { seperator2, "\n", "\\n" }, StringSplitOptions.None)[0].Trim();
                                result = result.Split(' ')[0];
                                finds.Add(result);
                            }
                            counter++;
                        }
                    }
                    catch (Exception ex)
                    {
                        string message = ex.Message;
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        finds.Add(message);
                    }

                    return finds;
                }
            }
        }
        public class ClassHandler
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="type"></param>
            /// <returns></returns>
            public string getMethods(Type type)
            {
                //List<MethodInfo> methodsList = Type.GetType(type).GetMethods().ToList();
                List<MethodInfo> methodsList = type.GetMethods().ToList();
                List<MethodDefinition> methods = new List<MethodDefinition>();
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    foreach (MethodInfo method in methodsList)
                    {
                        List<ParameterInfo> paramss = method.GetParameters().ToList();
                        List<string> paras = new List<string>();
                        if (paramss.Count > 0)
                            paramss.ForEach(p => paras.Add(p.ParameterType + "  " + p.Name));
                        MethodDefinition md = new MethodDefinition();
                        md.Name = method.Name;

                        if (method.ReturnType.Name.ToLower().IndexOf("list") > -1)
                        {
                            md.ReturnType = "List<" + method.ReturnType.GenericTypeArguments[0] + ">";
                        }
                        else if (method.ReturnType.Name.ToLower().IndexOf("dictionary") > -1)
                        {
                            md.ReturnType = "Dictionary<" + method.ReturnType.GenericTypeArguments[0] + "," + method.ReturnType.GenericTypeArguments[1] + " > ";
                        }
                        else
                        {
                            md.ReturnType = method.ReturnType.Name;
                        }

                        var attrs = method.GetCustomAttributesData().ToList();

                        string route = string.Empty;
                        string httptype = string.Empty;

                        //a => a.GetCustomAttributes(typeof(WebInvokeAttribute), true)

                        foreach (var ttr in attrs)
                        {
                            if (ttr.NamedArguments.Count > 0)
                            {
                                foreach (var attru in ttr.NamedArguments)
                                {
                                    if (attru.MemberName.ToLower() == "uritemplate")
                                    {
                                        route = attru.TypedValue.Value.ToString();
                                    }
                                    else if (attru.MemberName.ToLower() == "method")
                                    {
                                        httptype = attru.TypedValue.Value.ToString();
                                    }
                                }
                            }
                        }

                        if (type.IsInterface)
                        {
                            // We get the current assembly through the current class
                            var currentAssembly = this.GetType().GetTypeInfo().Assembly;

                            // we filter the defined classes according to the interfaces they implement
                            List<TypeInfo> iAssemblies = currentAssembly.DefinedTypes.Where(typer => typer.ImplementedInterfaces.Any(inter => inter == type)).ToList();

                            string Classes = string.Empty;
                            Classes = String.Join(", ", iAssemblies.Select(x => x.Name));
                            md.ClassName = Classes;
                        }
                        else
                        {
                            md.ClassName = type.Name;
                        }

                        md.Parameters = string.Join(" , ", paramss);
                        md.Route = route;
                        md.Flag = httptype;

                        methods.Add(md);
                    }
                    return JsonConvert.SerializeObject(methods);
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                    }
                    else
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    return null;
                }

            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="methodName"></param>
            /// <param name="className"></param>
            /// <param name="obj"></param>
            /// <returns></returns>
            public string InvokeMethod(string methodName, string className, string obj)
            {
                string result = string.Empty;
                //Assembly assembly = this.GetType().GetTypeInfo().Assembly;
                Type typo = Type.GetType("ezBot.services." + className);
                //Type type = assembly.GetType("services." + className);
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    if (typo != null)
                    {
                        MethodInfo methodInfo = typo.GetMethod(methodName);

                        if (methodInfo != null)
                        {

                            List<ParameterInfo> parameters = methodInfo.GetParameters().ToList();
                            object classInstance = Activator.CreateInstance(typo, null);

                            if (parameters.Count == 0)
                            {
                                // This works fine
                                result = JsonConvert.SerializeObject(methodInfo.Invoke(classInstance, null));
                            }
                            else
                            {
                                List<string> objers = new List<string>(); List<ParameterDefinitionFactor> ldf = new List<ParameterDefinitionFactor>();
                                objers = obj.Split(',').ToList();

                                object[] parametersArray = new object[] { }; List<object> objList = new List<object>();

                                foreach (var objer in objers)
                                {
                                    string name = objer.Split(':')[0].Trim();
                                    string value = objer.Split(':')[1].Trim();
                                    string type = parameters.FindAll(par => par.Name == name)[0].ParameterType.Name;

                                    if (type.Trim() == "String")
                                    {
                                        objList.Add(value);
                                    }
                                    else if (type.Trim().ToLower().IndexOf("int") > -1)
                                    {
                                        objList.Add(int.Parse(value));
                                    }
                                    else if (type.Trim().ToLower().IndexOf("bool") > -1)
                                    {
                                        objList.Add(bool.Parse(value));
                                    }
                                }


                                parametersArray = objList.ToArray();

                                // The invoke does NOT work;
                                // it throws "Object does not match target type"             
                                result = JsonConvert.SerializeObject(methodInfo.Invoke(classInstance, parametersArray));
                            }
                        }

                    }
                    return result;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                    }
                    else
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    return ex.Message;
                }

            }


            public class MethodDefinition
            {
                public string Name { get; set; }
                public string ReturnType { get; set; }
                public string Parameters { get; set; }
                public string Route { get; set; }
                public string Flag { get; set; }
                public string Description { get; set; }
                public string ClassName { get; set; }
            }
            public class ParameterDefinitionFactor
            {
                public string Name { get; set; }
                public string Type { get; set; }
                public string Value { get; set; }
            }
        }
        public class CurrentWindowsSystemFacts
        {
            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public DetailsList getSystemDetails()
            {
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    DetailsList dl = new DetailsList();
                    dl.Username = Environment.UserName;
                    dl.MacAddress = NetworkInterface.GetAllNetworkInterfaces().Where(nic => nic.OperationalStatus == OperationalStatus.Up && nic.NetworkInterfaceType != NetworkInterfaceType.Loopback).Select(nic => nic.GetPhysicalAddress().ToString()).FirstOrDefault();

                    dl.IPAddress = string.Empty;

                    foreach (var ip in Dns.GetHostEntry(Dns.GetHostName()).AddressList)
                    {
                        if (ip.AddressFamily == AddressFamily.InterNetwork)
                        {
                            dl.IPAddress = ip.ToString();
                        }
                    }
                    dl.SystemName = System.Environment.MachineName;


                    return dl;
                }
                catch (Exception ex)
                {
                    string ht = ex.Message;
                    if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                    }
                    else
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    return null;
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="SERVICENAME"></param>
            /// <returns></returns>
            public string thisSystemServices(string SERVICENAME)
            {
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    ServiceController sc = new ServiceController(SERVICENAME);

                    switch (sc.Status)
                    {
                        case ServiceControllerStatus.Running:
                            return StringHelper.Running;
                        case ServiceControllerStatus.Stopped:
                            return StringHelper.Stopped;
                        case ServiceControllerStatus.Paused:
                            return StringHelper.Paused;
                        case ServiceControllerStatus.StopPending:
                            return StringHelper.Stopping;
                        case ServiceControllerStatus.StartPending:
                            return StringHelper.Starting;
                        default:
                            return StringHelper.StatusChanging;
                    }
                }
                catch (Exception ex)
                {
                    string htr = ex.Message;
                    if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                    }
                    else
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    return "No Such Service Exists";
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="SERVICENAME"></param>
            /// <param name="MACHINENAME"></param>
            /// <returns></returns>
            public string remoteSystemServices(string SERVICENAME, string MACHINENAME)
            {
                try
                {
                    ServiceController sc = new ServiceController(SERVICENAME, MACHINENAME);
                    switch (sc.Status)
                    {
                        case ServiceControllerStatus.Running:
                            return StringHelper.Running;
                        case ServiceControllerStatus.Stopped:
                            return StringHelper.Stopped;
                        case ServiceControllerStatus.Paused:
                            return StringHelper.Paused;
                        case ServiceControllerStatus.StopPending:
                            return StringHelper.Stopping;
                        case ServiceControllerStatus.StartPending:
                            return StringHelper.Starting;
                        default:
                            return StringHelper.StatusChanging;
                    }
                }
                catch (Exception ex)
                {
                    string htr = ex.Message;
                    return "No Such Service Exists";
                }
            }
            public class DetailsList
            {
                public string Username { get; set; }
                public string MacAddress { get; set; }
                public string IPAddress { get; set; }
                public string SystemName { get; set; }
            }
        }
    }
}
