﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using Genbase.BLL;
using Genbase.DAL;
using Genbase.Classes;
using Microsoft.AspNetCore.Http;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;
using System.Net;
using System.Threading;

namespace Genbase.classes
{
    public class cSQLScripts
    {
        Dictionary<int, string> sp = new Dictionary<int, string>();
        Connection c = new Connection();
		//Connection c = Connection.GetInstance;
		public string cInsertScript(string tablename, int id, string ParentName, string ParentValue, string ReturnId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "";
                string Values = c.executeSQL("Select * from \"" + tablename + "\" where \"Id\"='" + id + "'", "Select");
                List<Dictionary<string, string>> InsertValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(Values);
                foreach (Dictionary<string, string> IValues in InsertValues)
                {
                    IList<string> CNames = IValues.Keys.ToArray();
                    string query1 = "INSERT INTO public.\"" + tablename + "\"(";
                    string query2 = "VALUES(";
                    for (var i = 0; i < (IValues.Count); i++)
                    {
                        if (CNames[i].ToString().ToUpper() != StringHelper.id)
                        {

                            if (i != (IValues.Count - 1))
                            {
                                query1 += "\"" + CNames[i].ToString() + "\",";
                                if (tablename == StringHelper.linknodes && (CNames[i].ToString() == StringHelper.stepid || CNames[i].ToString() == StringHelper.childstepids))
                                {
                                    if (CNames[i].ToString() == StringHelper.stepid)
                                    {
                                        query2 += sp[Convert.ToInt32(IValues[CNames[i]])] + ",";
                                    }
                                    else if (CNames[i].ToString() == StringHelper.childstepids)
                                    {
                                        var Childs = IValues[CNames[i]].Split('`');
                                        foreach (string child in Childs)
                                        {
                                            if (child != "" && child != "0")
                                                query2 += sp[Convert.ToInt32(child)] + "||'`'||";
                                            else
                                                query2 += "''";

                                        }
                                        query2 += ",";
                                    }

                                }
                                else if (CNames[i].ToString() == StringHelper.createdatetime || CNames[i].ToString() == StringHelper.updatedatetime)
                                {
                                    query2 += "now(),";
                                }
                                else if (CNames[i].ToString() == ParentName)
                                {

                                    query2 += "" + ParentValue + ",";
                                }
                                else
                                {
                                    if (CNames[i] == "Project_Id")
                                        query2 += "Project_Id,";
                                    else if (IValues[CNames[i]] == null || IValues[CNames[i]].ToString() == "")
                                        query2 += "null,";
                                    else
                                        query2 += "'" + IValues[CNames[i]] + "',";
                                }
                            }
                            else
                            {
                                query1 += "\"" + CNames[i].ToString() + "\")";
                                if (CNames[i].ToString() == ParentName)
                                {
                                    query2 += "" + ParentValue + ")";
                                }
                                else if (CNames[i].ToString() == StringHelper.createdatetime || CNames[i].ToString() == StringHelper.updatedatetime)
                                {
                                    query2 += "now())";
                                }
                                else
                                {
                                    if (CNames[i] == "Project_Id")
                                        query2 += "Project_Id)";
                                    else if (IValues[CNames[i]] == "null" || IValues[CNames[i]] == null || IValues[CNames[i]].ToString() == "")
                                        query2 += "null)";
                                    else
                                        query2 += "'" + IValues[CNames[i]] + "')";
                                }
                            }

                        }
                    }
                    query = query1 + query2 + "returning \"Id\" into " + ReturnId + ";";
                }
                return query;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string cRobotScripts(string Robots, string url)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                int step = 1, stepProp = 1, l = 1;
                var sb = new StringBuilder();
                Dictionary<string, List<Dictionary<string, string>>> RobotDetails = JsonConvert.DeserializeObject<Dictionary<string, List<Dictionary<string, string>>>>(Robots);
                List<string> InsertQuery = new List<string>();

                List<string> sproperties = new List<string>();
                List<string> ln = new List<string>();
                List<int> StepIds, StepPropertiesIds, LinkNodeIds = new List<int>();
                sb.AppendLine("CREATE OR REPLACE FUNCTION public.ImportRobot(Project_Id integer)");
                sb.AppendLine("RETURNS integer as $id$");
                sb.AppendLine(StringHelper.declare);
                List<string> sourcepath = new List<string>();
                foreach (Dictionary<string, string> Robot in RobotDetails[StringHelper.robots])
                {
                    sb.AppendLine("Robot_Id integer;");
                    sb.AppendLine("Checker integer;");

                    //string vquery = "select max(\"versionid\") from \"Versions\" where \"rid\" =" + Robot["id"] + "";
                    //string vs = new PostgresConnect().DbExecuteQuery(vquery, "createproject");
                    string[] rversion = Robot["Version"].Split(",");
                    InsertQuery.Add(cInsertScript(StringHelper.robot, Convert.ToInt32(Robot["id"]), StringHelper.projectid, StringHelper.projectid, StringHelper.robotid));
                    foreach (string vs in rversion)
                    {
                        StepIds = checkDependencies(StringHelper.steps + "," + vs, StringHelper.robotid, Convert.ToInt32(Robot["id"]));

                        foreach (int Id in StepIds)
                        {
                            sp[Id] = "Step_Id_" + step;
                            sb.AppendLine("Step_Id_" + step + " integer;");
                            InsertQuery.Add(cInsertScript(StringHelper.steps, Id, StringHelper.robotid, StringHelper.robotid, "Step_Id_" + step));
                            StepPropertiesIds = checkDependencies(StringHelper.stepproperties, StringHelper.stepsid, Id);
                            foreach (int propertiesIds in StepPropertiesIds)
                            {
                                sproperties.Add("StepProp_Id_" + stepProp);
                                sb.AppendLine("StepProp_Id_" + stepProp + " integer;");
                                InsertQuery.Add(cInsertScript(StringHelper.stepproperties, propertiesIds, StringHelper.stepsid, "Step_Id_" + step, "StepProp_Id_" + stepProp));
                                stepProp++;
                            }
                            step++;
                        }

                        foreach (int Id in StepIds)
                        {
                            LinkNodeIds = checkDependencies(StringHelper.linknodes, StringHelper.stepid, Id);
                            foreach (int linkIds in LinkNodeIds)
                            {
                                ln.Add("Links_" + l);
                                sb.AppendLine("Links_" + l + " integer;");
                                InsertQuery.Add(cInsertScript(StringHelper.linknodes, linkIds, StringHelper.stepid, "", "Links_" + l));
                                l++;
                            }
                        }
                    }
                    sb.AppendLine("BEGIN");
                    sb.AppendLine("select count (*) from \"ROBOT\" where \"Project_Id\" = Project_Id and \"Name\" ='" + Robot["Name"] + "' and \"Status\" = true into Checker;");
                    sb.AppendLine("if Checker<1 THEN");
                    foreach (string query in InsertQuery)
                    {
                        sb.AppendLine(query);
                    }
                    //  string vquery = "select max(\"versionid\") from \"Versions\" where \"rid\" =" + Robot["id"] + "";
                    // string vs = new PostgresConnect().DbExecuteQuery(vquery, "createproject");
                    //for (int v = 1; v <= Convert.ToInt32(vs); v++)
                    //for (int v = Convert.ToInt32(vs); v <= Convert.ToInt32(vs); v++)
                    //               {
                    //                   sb.AppendLine("INSERT INTO public.\"Versions\"(\"ID\",\"rid\",\"versionid\",\"rflag\",\"CreateDatetime\",\"Status\")VALUES(DEFAULT,Robot_Id,'" + v + "','1',now(),'true');");
                    //               }
                    foreach (string k in rversion) { 
                    sb.AppendLine("INSERT INTO public.\"Versions\"(\"ID\",\"rid\",\"versionid\",\"rflag\",\"CreateDatetime\",\"Status\")VALUES(DEFAULT,Robot_Id,'" + Convert.ToInt32(k) + "','1',now(),'true');");
                    }
                    sb.AppendLine("END IF;");
                    sb.AppendLine("return 1;");
                    sb.AppendLine("END;");
                    sb.AppendLine("$id$ LANGUAGE plpgsql;");
                    sb.Replace(",null,", ",'',");
                    //string p1 = Directory.GetCurrentDirectory().ToString();
                    //var path = Directory.GetCurrentDirectory().ToString() + @"\generatedSQLScripts\" + Robot["Name"] + "_" + Robot["id"] + ".sql";
                    string dirName = Environment.CurrentDirectory + "\\generatedSQLScripts";
                    if (!Directory.Exists(dirName))
                    {
                        Directory.CreateDirectory(dirName);
                    }
						//string id = DateTime.UtcNow.ToString().Replace(' ', '-').Replace(':', '-').Replace("-", null);
						string id = Guid.NewGuid().ToString();

					var path = Environment.CurrentDirectory + @"\generatedSQLScripts\" + Robot["Name"].Trim() + "_" + Robot["id"].Trim()+"_"+ id + ".sql";
                
                    if (File.Exists(path))
                    {
							using (FileStream fs = File.OpenWrite(path))
							{
								byte[] bytesInfo = new UTF8Encoding(true).GetBytes(sb.ToString());
								fs.Write(bytesInfo, 0, bytesInfo.Length);
								fs.Close();
							}
						}
                    else
                    {
                        using (FileStream fs = File.Create(path))
                        {
                            byte[] bytesInfo = new UTF8Encoding(true).GetBytes(sb.ToString());
                            fs.Write(bytesInfo, 0, bytesInfo.Length);
                            fs.Close();
                        }

                    }
                    //FileStream fs = new FileStream(path, FileMode.Create);
                    //fs.Close();
                    //using (StreamWriter files = new StreamWriter(path))
                    //{
                    //    files.WriteLine(sb.ToString());
                    //}
                    //bool isdone = false;
                    //while (File.Exists(path))
                    //{
                    //    isdone = true;
                    //    //Application.DoEvents();
                    //    //Application.RaiseIdle(new EventArgs());
                    //    Thread.Sleep(0);
                    //}
                    //if (isdone)
                    //{
                    //    return path;
                    //}
                    //else
                    //{

                    //    return path;
                    //}
                    //string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    //string url = "http://localhost:61048//generatedSQLScripts//";

                    //using (var client = new WebClient())
                    //{
                    //    client.DownloadFile(url + "//generatedSQLScripts//", desktopPath + "/" + Robot["Name"] + "_" + Robot["id"] + ".sql");
                    //}
                    sourcepath.Add("generatedSQLScripts");
                    sourcepath.Add(Robot["Name"] + "_" + Robot["id"] +"_" +id+".sql");

                }

                return String.Join(",", sourcepath.ToArray());
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string UploadSQLScripts(string Robots, string Fileupload)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<Dictionary<string, string>> RobotDetails = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(Robots);

                string result = c.executeSQL(Fileupload, "Insert");
                foreach (Dictionary<string, string> robot in RobotDetails)
                {
                    Connection c = new Connection();
                    string query = "Select ImportRobot('" + robot["ID"] + "')";
                    string result1 = c.executeSQL(query, "Select");
                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string Base64Decode(string base64EncodedData)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
                return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public List<int> checkDependencies(string tablename, string ColName, int ID)
        {
			string resultIds = string.Empty;

			List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<int> Ids = new List<int>();
                Connection c = new Connection();
				if (tablename.Contains(","))
				{
					string[] al = tablename.Split(",");
					 resultIds = c.executeSQL("Select distinct(\"Id\") from \"" + al[0] + "\" where \"" + ColName + "\"='" + ID + "' and \"Status\"=true and \"Version\"= '" +Convert.ToInt32(al[1])+"' "  , "Select");
					//if (resultIds != "[]")
					//{
					//	List<Dictionary<string, int>> Result = JsonConvert.DeserializeObject<List<Dictionary<string, int>>>(resultIds);
					//	foreach (Dictionary<string, int> DictId in Result)
					//	{
					//		Ids.Add(DictId["Id"]);
					//	}
					//}
					//return Ids;
				}
				else
				{
					 resultIds = c.executeSQL("Select distinct(\"Id\") from \"" + tablename + "\" where \"" + ColName + "\"='" + ID + "' and \"Status\"=true", "Select");
					//if (resultIds != "[]")
					//{
					//	List<Dictionary<string, int>> Result = JsonConvert.DeserializeObject<List<Dictionary<string, int>>>(resultIds);
					//	foreach (Dictionary<string, int> DictId in Result)
					//	{
					//		Ids.Add(DictId["Id"]);
					//	}
					//}
					//return Ids;
				}
				if (resultIds != "[]")
				{
					List<Dictionary<string, int>> Result = JsonConvert.DeserializeObject<List<Dictionary<string, int>>>(resultIds);
					foreach (Dictionary<string, int> DictId in Result)
					{
						Ids.Add(DictId["Id"]);
					}
				}
				return Ids;
			}
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return null;
            }
        }
        //public void Download(string sFilePath, string sFileName)
        //{
        //    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
        //    try
        //    {
        //        HttpContext.Current.Response.ContentType = "APPLICATION/OCTET-STREAM";
        //        String Header = "Attachment; Filename=" + sFileName;
        //        HttpContext.Current.Response.AppendHeader("Content-Disposition", Header);
        //        System.IO.FileInfo Dfile = new System.IO.FileInfo((sFilePath));//HttpContext.Current.Server.MapPath(sFilePath)
        //        HttpContext.Current.Response.WriteFile(Dfile.FullName);
        //        HttpContext.Current.Response.End();
        //    }
        //    catch (Exception ex)
        //    {
        //        new Logger().LogException(ex);
        //        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
        //        {
        //            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
        //        }
        //        else
        //        {
        //            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

        //        }
        //        //return ex.Message;
        //    }
        //}
    }
}