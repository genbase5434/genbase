﻿#region "NameSpaces"
using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using Npgsql;
using Newtonsoft.Json;
using Genbase.BLL;
using Genbase.Classes;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

#endregion

namespace Genbase.Classes
{
    public class Connection
    {
        public NpgsqlConnection con;
        public static string strConnString;
        private static Connection instance = null;
        public static Connection GetInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Connection();
                }
                return instance;
            }
        }
        public Connection()
        {
            strConnString = Config.getconfig();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                con = new NpgsqlConnection(strConnString);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
            }
        }

        public Connection(string Server, string Port, string Database, string Username, string Password)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string strConn = "Server=" + Server + ";Port=" + Port + ";" + "Database=" + Database + ";" + "User Id=" + Username + ";" + "Password=" + Password + ";";
                con = new NpgsqlConnection(strConn);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
            }
        }
        public bool openDB()
        {
            bool ret = true;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                ret = false;
            }
            return ret;
        }

        public string executeSQL(string query, string op)
        {
            //JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string sql = string.Empty;
            try
            {
                openDB();
                NpgsqlCommand command = new NpgsqlCommand(query, con);
                command.CommandTimeout = 30;

                //NpgsqlDataAdapter sda = new NpgsqlDataAdapter(command);

                NpgsqlDataReader dread = command.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(dread);
                Dictionary<string, object> row;


                //command.ExecuteNonQuery();
                closeDB();
                if (op == StringHelper.select)
                {
                    //DataSet ds = new DataSet();
                    //sda.Fill(ds);
                    //DataTable dt = ds.Tables[0];


                    //Dictionary<string, object> row;
                    foreach (DataRow dr in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        rows.Add(row);
                    }
                    return JsonConvert.SerializeObject(rows);
                }

                else
                {
                    return StringHelper.truee;
                }

            }
            catch (Exception ex)
            {
                if (ex.HResult == -2146233079 && ex.Message != StringHelper.connectionnotopened)
                {
                    List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>(rows);
                    rows.RemoveRange(rows.Count - 5000, 5000);
                    rows1.RemoveRange(0, rows1.Count - 5000);
                    return JsonConvert.SerializeObject(rows1) + JsonConvert.SerializeObject(rows);
                }
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return StringHelper.ffalse;
            }

        }

        internal string executeSQL(object p, string v)
        {
            throw new NotImplementedException();
        }

        public bool closeDB()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            bool ret = true;
            try
            {
                con.Close();
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                ret = false;
            }
            return ret;
        }

        public string VerifyDBServer(string Server, string Port, string Database, string Username, string Password)
        {
            string _result = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                bool dbExists = false;

                using (NpgsqlConnection conn = new NpgsqlConnection("Server=" + Server + ";Port=" + Port + ";Database=postgres;User Id=" + Username + ";Password=" + Password + ""))
                {
                    try
                    {
                        conn.Open();
                    }
                    catch (Exception ex)
                    {
                        string ec = ex.Message;
                        _result = "-1";
                    }
                    string cmdText = "SELECT 1 FROM pg_database WHERE datname='" + Database + "'";
                    using (NpgsqlCommand cmd = new NpgsqlCommand(cmdText, conn))
                    {
                        dbExists = cmd.ExecuteScalar() != null;
                    }
                }
                if (dbExists)
                {
                    _result = "1";
                }
                else
                {
                    _result = "0";
                }
                return _result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                string ec = ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
                _result = "-2";
                return _result;
            }
        }

        public string executeDBScriptSQL(string query)
        {

            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string _result = string.Empty;
            try
            {
                openDB();
                NpgsqlCommand command = new NpgsqlCommand(query, con);
                command.CommandTimeout = 30;
                int i = command.ExecuteNonQuery();
                closeDB();
                if (i > 0)
                {
                    _result = StringHelper.truee;
                }
                return _result;
            }
            catch (Exception ex)
            {

                if (ex.HResult == -2146233079 && ex.Message != StringHelper.connectionnotopened)
                {
                    List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>(rows);
                    rows.RemoveRange(rows.Count - 5000, 5000);
                    rows1.RemoveRange(0, rows1.Count - 5000);
                    return JsonConvert.SerializeObject(rows1) + JsonConvert.SerializeObject(rows);
                }
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return StringHelper.ffalse;
            }

        }
    }
}