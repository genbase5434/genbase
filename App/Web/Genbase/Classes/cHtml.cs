﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Genbase.BLL;
using Genbase.classes;
using System.Text.RegularExpressions;

namespace Genbase.Classes
{
    public class cHtml
    {
       // public string generateHTML(string s, string ScreenDetails/*name*/, string ElementRows/*, string tablename*/,string pathUI)
        public string generateHTML(string s, string ScreenDetails/*name*/, string ElementRows/*, string tablename*/)
        {
            string line;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            JObject screenData = JObject.Parse(ScreenDetails);

             var UIRootPath = SetEnvironmentPath();
            //var UIRootPath = pathUI;
            //var path = UIRootPath + @"\Views\generatedHTML\" + s/*screenData["SName"].ToString()*/ + ".html";
            var path = UIRootPath + @"Views\generatedHTML\" + s + ".html";
            new Logger().LogException("GenerateHTML: "+path);
            try
            {
                var sb = new StringBuilder();
                var g = new StringBuilder();
                string htmlPath = "";
                FileStream fs = new FileStream(path, FileMode.Create);
                fs.Close();
                if (screenData[StringHelper.formtype].ToString() == StringHelper.inline)
                    htmlPath = "Views\\Temp\\InlineTemplate.html";
                else
                    htmlPath = "Views\\Temp\\Template1.html";
                StreamReader file = new StreamReader(UIRootPath + htmlPath);
                while ((line = file.ReadLine()) != null)
                    sb.AppendLine(line.Substring(0, Math.Min(1000, line.Length)));

                file.Close();

                sb.Replace("{Screen Title}", screenData[StringHelper.sname].ToString());
                sb.Replace("{tablename}", screenData[StringHelper.tablen].ToString());
                sb.Replace("{JSFileName}", s + ".js");
                if (Convert.ToBoolean(screenData[StringHelper.ainsert]) == true)
                {
                    sb.Replace("{AddNewRecordButton}", "<button type=\"button\" class=\"btn plusBtn plusBtn-add\" onclick=\"addFunction(this)\"><i class=\"fa fa-plus icon-bg\"></i> Add New</button>");
                    sb.Replace("{AddNewTab}", "<li><a data-toggle=\"tab\" href=\"#addNew\" onclick=\"removeEditTab();addNewDropDowns()\"><i class=\"fa fa-plus\"></i> Add " + screenData[StringHelper.sname].ToString() + "</a></li>");
                }
                else
                {
                    sb.Replace("{AddNewRecordButton}", "");
                    sb.Replace("{AddNewTab}", "");
                }
                if (screenData[StringHelper.insights].ToString() == StringHelper.None)
                {
                    sb.Replace("{Insights}", "<button type = \"button\" id=\"Insights\" onclick = \"Insightclick()\" insights=\"\" class=\"btn plusBtn plusBtn-insights btn-insight\" style=\"color:#fff\" disabled><i class=\"fa fa-bar-chart icon-bg-insights\" aria-hidden=\"true\"></i> Insights</button>");
                    sb.Replace("{InsightsTab}", "");
                }
                else
                {
                    sb.Replace("{Insights}", "<button type = \"button\" id=\"Insights\" onclick = \"Insightclick();\" insights=\"" + screenData["Insights"].ToString() + "\" class=\"btn plusBtn plusBtn-insights btn-insight\" style=\"color:#fff\"><i class=\"fa fa-bar-chart icon-bg-insights\" aria-hidden=\"true\"></i> Insights</button>");
                    sb.Replace("{InsightsTab}", "<li><a data-toggle=\"tab\" class=\"btn-insight\" id=\"removeEditTab();Insights\" onclick = \"Insightclick()\" insights=\"" + screenData["Insights"].ToString() + "\" href=\"#insights_view\">Insights</a></li>");
                }
                JArray ElementRow = JArray.Parse(ElementRows);
                List<Dictionary<string, string>> list = new List<Dictionary<string, string>>();
                int ListCount = ElementRow.Count;
                foreach (JObject o in ElementRow.Children<JObject>())
                {
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    foreach (JProperty p in o.Properties())
                    {
                        string name = p.Name;
                        string value = (string)p.Value;
                        if (name == StringHelper.columnorder && value == "")
                            value = "" + ListCount++ + "";
                        dict.Add(name, value);
                    }
                    list.Add(dict);
                }

                var list1 = list.OrderBy(x => Convert.ToInt64(x[StringHelper.columnorder])).ToList<Dictionary<string, string>>();
                var sb1 = cAddScreen(list1);

                sb.Replace("{AddModel}", sb1.ToString());
                var sb2 = cEditScreen(list1);
                sb.Replace("{EditModel}", sb2.ToString());

                using (StreamWriter files = new StreamWriter(UIRootPath + @"Views\generatedHTML\" + s/*screenData["SName"].ToString()*/ + ".html"))
                {
                    files.WriteLine(sb.ToString());
                }
                return StringHelper.truee;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];

            }
        }

        public StringBuilder cAddScreen(List<Dictionary<string, string>> ElementRows)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                var sb = new StringBuilder();
                Widget w = new Widget();
                cElement E = new cElement();
                for (int i = 0; i < ElementRows.Count; i++)
                {
                    var x = Convert.ToBoolean(ElementRows[i][StringHelper.quottedactive]);

                    sb.Append("<div class=\"form-group col-sm-12\" style=\"display:{display}\">{ElementLabel} {Elements}</div>");

                    sb.Replace("{ElementLabel}", E.createLabel(ElementRows[i][StringHelper.displayname].ToString()));
                    if ((ElementRows[i][StringHelper.elementid]).ToString() != "")
                    {
                        int validilty = w.checkWidget(Convert.ToInt32(ElementRows[i][StringHelper.elementid]));

                        if (Convert.ToBoolean(validilty))
                        {
                            string widgetDetails = w.getWidget(Convert.ToInt32(ElementRows[i][StringHelper.elementid]));
                            JArray widget = JArray.Parse(widgetDetails);

                            if (widget[0][StringHelper.widgettype].ToString() == StringHelper.tbox)
                            {
                                sb.Replace("{Elements}", E.createTextBox(ElementRows[i]));
                                sb.Replace("{Edit/none}", "");
                            }
                            else if (widget[0][StringHelper.widgettype].ToString() == StringHelper.cbox)
                            {
                                sb.Replace("{Elements}", E.createCheckBox(ElementRows[i]));
                                sb.Replace("{Edit/none}", "");
                            }
                            else if (widget[0][StringHelper.widgettype].ToString() == StringHelper.pword)
                            {
                                sb.Replace("{Elements}", E.createPassword(ElementRows[i]));
                                sb.Replace("{Edit/none}", "");
                            }
                            else if (widget[0][StringHelper.widgettype].ToString() == StringHelper.dropdown)
                            {
                                sb.Replace("{Elements}", E.createDropDown(ElementRows[i]));
                                sb.Replace("{Edit/none}", "");
                            }
                            else if (widget[0][StringHelper.widgettype].ToString() == StringHelper.textarea)
                            {
                                sb.Replace("{Elements}", E.createTextarea(ElementRows[i]));
                                sb.Replace("{Edit/none}", "");
                            }
                            else if (widget[0][StringHelper.widgettype].ToString() == StringHelper.textfile)
                            {
                                sb.Replace("{Elements}", E.createTextfile(ElementRows[i]));
                                sb.Replace("{Edit/none}", "");
                            }
                            if (Convert.ToBoolean(widget[0][StringHelper.mandatory]))
                            {
                                sb.Replace("{Mandatory}", StringHelper.required);

                            }
                            else
                                sb.Replace("{Mandatory}", "");
                            if (widget[0][StringHelper.Length] != null)
                            {
                                sb.Replace("{Length}", StringHelper.lenval + "=" + widget[0][StringHelper.Length]);
                            }
                            else
                            {
                                sb.Replace("{Length}", "");
                            }
                        }
                        else
                        {
                            sb.Replace("{Elements}", E.createTextBox(ElementRows[i]));
                            sb.Replace("{Edit/none}", "");
                        }
                    }
                    else
                    {
                        sb.Replace("{Elements}", E.createTextBox(ElementRows[i]));
                        sb.Replace("{Edit/none}", "");

                    }
                    sb.Replace("{Mandatory}", "");
                    sb.Replace("{Length}", "");

                    if ((ElementRows[i]["columnName"].ToLower() == "id") || x == false || Convert.ToBoolean(ElementRows[i][StringHelper.showform]) == false)
                    {
                        sb.Replace("{display}", StringHelper.quottednone);
                    }
                    else
                    {
                        sb.Replace("{display}", StringHelper.initial);
                    }
                }
                return sb;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return null;
            }
        }
        public StringBuilder cEditScreen(List<Dictionary<string, string>> ElementRows)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                var sb = new StringBuilder();
                Widget w = new Widget();
                cElement E = new cElement();

                for (int i = 0; i < ElementRows.Count; i++)
                {

                    if (Convert.ToBoolean(ElementRows[i][StringHelper.aactive]) == true || ElementRows[i][StringHelper.cname].ToUpper() == StringHelper.id)
                    {


                        sb.Append("<div class=\"form-group col-sm-12\">{ElementLabel} {Elements}</div>");

                        sb.Replace("{ElementLabel}", E.createLabel(ElementRows[i]["displayName"].ToString()));
                        if ((ElementRows[i][StringHelper.elementid]).ToString() != "")
                        {
                            int validilty = w.checkWidget(Convert.ToInt32(ElementRows[i][StringHelper.elementid]));

                            if (Convert.ToBoolean(validilty))
                            {
                                string widgetDetails = w.getWidget(Convert.ToInt32(ElementRows[i][StringHelper.elementid]));
                                JArray widget = JArray.Parse(widgetDetails);
                                if (ElementRows[i][StringHelper.cname].ToLower() == StringHelper.identitydocument)
                                {
                                    sb.Replace("{Elements}", E.createID(ElementRows[i]));
                                    sb.Replace("{Edit/none}", StringHelper.edit);
                                }
                                if (widget[0][StringHelper.widgettype].ToString() == StringHelper.tbox)
                                {
                                    sb.Replace("{Elements}", E.createTextBox(ElementRows[i]));
                                    sb.Replace("{Edit/none}", StringHelper.edit);
                                }
                                else if (widget[0]["WidgetType"].ToString() == StringHelper.cbox)
                                {
                                    sb.Replace("{Elements}", E.createCheckBox(ElementRows[i]));
                                    sb.Replace("{Edit/none}", StringHelper.edit);
                                }
                                else if (widget[0]["WidgetType"].ToString() == StringHelper.pword)
                                {
                                    sb.Replace("{Elements}", E.createPassword(ElementRows[i]));
                                    sb.Replace("{Edit/none}", StringHelper.edit);
                                }
                                else if (widget[0]["WidgetType"].ToString() == StringHelper.dropdown)
                                {
                                    sb.Replace("{Elements}", E.createDropDown(ElementRows[i]));
                                    sb.Replace("{Edit/none}", StringHelper.edit);
                                }
                                else if (widget[0]["WidgetType"].ToString() == StringHelper.textarea)
                                {
                                    sb.Replace("{Elements}", E.createTextarea(ElementRows[i]));
                                    sb.Replace("{Edit/none}", StringHelper.edit);
                                }
                                if (Convert.ToBoolean(widget[0][StringHelper.mandatory]))
                                {
                                    sb.Replace("{Mandatory}", StringHelper.required);
                                }
                                else
                                    sb.Replace("{Mandatory}", "");
                            }
                            else
                            {
                                if (ElementRows[i][StringHelper.cname].ToLower() == StringHelper.identitydocument)
                                {
                                    sb.Replace("{Elements}", E.createID(ElementRows[i]));
                                    sb.Replace("{Edit/none}", StringHelper.edit);
                                }
                                else
                                {
                                    sb.Replace("{Elements}", E.createTextBox(ElementRows[i]));
                                    sb.Replace("{Edit/none}", StringHelper.edit);
                                }
                            }
                        }
                        else
                        {
                            if (ElementRows[i][StringHelper.cname].ToLower() == StringHelper.identitydocument)
                            {
                                sb.Replace("{Elements}", E.createID(ElementRows[i]));
                                sb.Replace("{Edit/none}", StringHelper.edit);
                            }
                            else
                            {
                                sb.Replace("{Elements}", E.createTextBox(ElementRows[i]));
                                sb.Replace("{Edit/none}", StringHelper.edit);
                            }
                        }

                    }
                }
                return sb;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return null;
            }
        }

        public string SetEnvironmentPath()
        {
            string UIRootPath = "";
            string workingDirectory = Environment.CurrentDirectory;
            new ExecutionLog().add2Log(workingDirectory, "SetEnvironmentPath", "HTML", StringHelper.eventcode, false);
            string currentDirectory = "";
            if (workingDirectory.Contains("trunk"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"/UI/wwwroot/";
            }
            else if (workingDirectory.Contains("Genbase_v2_apiservice-DEV"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"\\Genbase_v2_uiservice-DEV\\wwwroot\\";
            }
            else if (workingDirectory.Contains("Genbase_v2_apiservice-QA"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"\\Genbase_v2_uiservice-QA\\wwwroot\\";
            }
            else if (workingDirectory.Contains("Genbase_v2_apiservice-Alpha"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"\\Genbase_v2_uiservice-Alpha\\wwwroot\\";
            }
            else if (workingDirectory.Contains("Genbase_v2_apiservice-Beta"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"\\Genbase_v2_uiservice-Beta\\wwwroot\\";
            }
            else if (workingDirectory.Contains("Genbase_v2_apiservice-Prod"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"\\Genbase_v2_uiservice-Prod\\wwwroot\\";
            }
            else if (workingDirectory.Contains("Genbase_v2_apiservice-TVG"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"\\Genbase_v2_uiservice-TVG\\wwwroot\\";
            }
            else if (workingDirectory.Contains("Genbase_v2_apiservice-NADG"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"\\Genbase_v2_uiservice-NADG\\wwwroot\\";
            }
            else
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"/Genbase_UI/wwwroot/";
            }

            return UIRootPath;
        }
    }
}
