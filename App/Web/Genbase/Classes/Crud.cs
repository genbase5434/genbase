﻿using Genbase.BLL;
using Genbase.classes;
using Genbase.DAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Text.RegularExpressions;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;
using System.Net.NetworkInformation;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Net;

namespace Genbase.Classes
{
    public class Crud
    {
        Connection c = Connection.GetInstance;
       // PostgresConnect dbd = new PostgresConnect();
		PostgresConnect dbd = PostgresConnect.GetInstance;

        public string getTableData(string tablename)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (tablename == StringHelper.screentable)
                {

                    string result = new QueryUtil().getTableData(tablename, "Status|Equals|true", "*", 0, "public", "", "");
                    return result;
                }

                else if (tablename == StringHelper.notificationsTable)
                {
                    string result = new QueryUtil().getTableData(tablename, "Status|Equals|true", "*", 0, "public", "\"ID\"", "desc");
                    return result;
                }

                else if (tablename == StringHelper.dbconnection)
                {
                    List<List<string>> finalResult = new List<List<string>>();
                    List<Dictionary<string, string>> success = new List<Dictionary<string, string>>();
                    List<Dictionary<string, string>> failed = new List<Dictionary<string, string>>();
                    List<int> temp = new List<int>();
                    bool endResult;


                    string query = "Select d.*,t.\"Name\" as \"DBName\" from \"DBConnection\" d inner join \"DBType\" t on t.\"ID\" = d.\"DBType\" and t.\"Status\"=true and d.\"Status\"=true";
                    string result = c.executeSQL(query, "Select");
                    List<DBConnection> ListValues = JsonConvert.DeserializeObject<List<DBConnection>>(result);
                    List<string> tempS = new List<string>();
                    temp.Add(0);
                    foreach (DBConnection d in ListValues)
                    {
                        //Dictionary<string, string> tempS = new Dictionary<string, string>();
                        string tempF = "";
                        if (d.DBName == StringHelper.mysqldb)
                        {
                            MysqlConnect s = new MysqlConnect(d.Server, d.Port, d.Database, d.Username, d.Password);
                            endResult = s.OpenConnection();
                            if (endResult == true)
                            {
                                s.CloseConnection();
                            }

                            int r = Convert.ToInt32(endResult);
                            tempF = d.ID.ToString() + "," + d.Server.ToString() + "," + d.Database.ToString() + "," + r.ToString();
                            tempS.Add(tempF);

                        }
                        else if (d.DBName == StringHelper.postgressql)
                        {
                            Connection c1 = new Connection(d.Server, d.Port, d.Database, d.Username, d.Password);
                            endResult = c1.openDB();

                            if (endResult == true)
                            {
                                c1.closeDB();
                            }
                            int r = Convert.ToInt32(endResult);
                            tempF = d.ID.ToString() + "," + d.Server.ToString() + "," + d.Database.ToString() + "," + r.ToString();
                            tempS.Add(tempF);

                        }
                    }
                    finalResult.Add(tempS);
                    return JsonConvert.SerializeObject(finalResult);
                }
                else if (tablename == StringHelper.action || tablename == StringHelper.actionproperties || tablename == StringHelper.actionproperty || tablename == StringHelper.element || tablename == StringHelper.robot || tablename == StringHelper.timesheets || tablename == StringHelper.workflow || tablename == StringHelper.cases)
                {

                    string result = new QueryUtil().getTableData(tablename, "Status|Equals|true", "*", 0, "public", "\"Id\"", "");
                    return result;
                }
                else if (tablename == StringHelper.stepproperties)
                {

                    string result = new QueryUtil().getTableData(tablename, "Status|Equals|true", "*", 1000, "public", "\"Id\"", "");
                    return result;
                }
                else if (tablename == StringHelper.steps)
                {

                    string result = new QueryUtil().getTableData(tablename, "Status|Equals|true", "*", 1000, "public", "\"Id\"", "");
                    return result;
                }
                else if (tablename == StringHelper.ActionControl)
                {

                    string query = " Select * from \"ActionProperties\" where \"Action_Id\"=777 order By \"Order\"";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.ConnectivitySystems)
                {

                    string query = "Select s.*,d.\"Server\" as serverName from \"Systems\" s inner join \"DBConnection\" d on s.\"Server\"::integer = d.\"ID\"  and s.\"Status\"=true and d.\"Status\"=true";

                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.ConnectivityRobots)
                {

                    string query = "Select r.*,sr.\"System_Id\" as sytemID,s.\"Name\" as systemName,d.\"Server\" as ServerName from \"Systems\" s inner join \"DBConnection\" d on s.\"Server\"::integer = d.\"ID\" inner join \"SystemsRobots\" sr on sr.\"System_Id\" = s.\"ID\" inner join \"ROBOT\" r on sr.\"Robot_Id\" = r.\"Id\" and r.\"Status\" = true and sr.\"Status\"=true and s.\"Status\"=true and d.\"Status\"=true";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.ConnectivityServerDetails)
                {

                    string query = "Select * from \"ServerStatus\" ";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else
                {
                    string result = "";
                    List<TableSchema> schema = new QueryUtil().getTableSchema(tablename);
                    bool contains = schema.Any(str => str.column_name == "Status");
                    if (contains)
                    {
                        result = new QueryUtil().getTableData(tablename, "Status|Equals|true", "*", 0, "public", "\"ID\"", "");
                    }
                    else
                    {
                        string query = "SELECT * from public.\"" + tablename + "\"";
                        result = c.executeSQL(query, "Select");
                    }
                    
                    return result;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getTableData(string tablename, string CreateById)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string table = string.Empty;
                if (tablename == StringHelper.iinbox)
                {

                    string query = "select i.*,r.\"Name\",s.\"Name\" as \"StepName\" from public.\"Inbox\" i inner join \"ROBOT\" r on i.\"RobotId\" = r.\"Id\" inner join \"STEPS\" s on s.\"Id\" = i.\"StepId\" and i.\"Status\"='true' and r.\"Status\" and i.\"AssignedTo\"='" + CreateById + "'";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                if (tablename == StringHelper.CustomActionProperties)
                {

                    string query = "select * from \"ActionProperties\" where \"Action_Id\" =" + CreateById + " order By \"Order\"";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                if (tablename == StringHelper.CommandCenter_Inbox)
                {

                    string query = "select i.*,r.\"Name\",s.\"Name\" as \"StepName\" from public.\"Inbox\" i inner join \"ROBOT\" r on i.\"RobotId\" = r.\"Id\" inner join \"STEPS\" s on s.\"Id\" = i.\"StepId\" and i.\"Status\"='true' and r.\"Status\"='true' and i.\"AssignedTo\"='" + CreateById + "' and i.\"MsgStatus\" = 'New' order by i.\"ID\" desc";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.processmangerbotdetails)
                {

                    string query = "select \"JobStatus\" as category , count(\"JobStatus\") as value from \"ProcessManager\" where \"RobotId\"::Integer = '" + CreateById + "'  and \"Status\" = 'true' group by \"JobStatus\"";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.schedulercommandcenter)
                {

                    table = StringHelper.scheduler;
                    string result = new QueryUtil().getTableData(table, "RobotId|Equals|" + CreateById + "||Status|Equals|true", "*", 0, "public", "", "");
                    return result;
                }
                else if (tablename == StringHelper.history)
                {

                    string query = "select * from \"ProcessManager\" where \"RobotId\"::Integer = '" + CreateById + "' and \"Status\" = 'true' order by \"StartingTime\"::Timestamp DESC";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.Menu)
                {

                    string query = "select * from \"MENU\" where \"ID\" = '" + CreateById + "' and \"Status\" = 'true'";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.removesystem)
                {

                    string result = new QueryUtil().updateTableData("Systems", "ID|Equals|" + CreateById, "Status|Equals|false", "public", false);
                    return result;
                }

                else if (tablename == StringHelper.processmanager)
                {

                    string chartType = CreateById;
                    string query = "";
                    if (chartType == "Live")
                    {
                        query = "SELECT \"CreateDateTime\"::TimeStamp,sum(case when \"JobStatus\" = 'New' then 1 else 0 end) as \"New\",sum(case when  \"JobStatus\" = 'In Progress' then 1 else 0 end) as \"In Progress\",sum(case when  \"JobStatus\" = 'Success' then 1 else 0 end) as \"Success\",sum(case when  \"JobStatus\" = 'Failure' then 1 else 0 end) as \"Failure\"from \"ProcessManager\"  where \"CreateDateTime\"::Timestamp>date_trunc('week',current_date) - interval '1 week' group by \"CreateDateTime\"::TimeStamp order by \"CreateDateTime\"";
                    }

                    else
                    {
                        query = "SELECT date_trunc('" + chartType + "', \"CreateDateTime\":: TIMESTAMP) as \"CreateDateTime\",sum(case when \"JobStatus\" = 'New' then 1 else 0 end) as \"New\",sum(case when  \"JobStatus\" = 'In Progress' then 1 else 0 end) as \"In Progress\",sum(case when  \"JobStatus\" = 'Success' then 1 else 0 end) as \"Success\",sum(case when  \"JobStatus\" = 'Failure' then 1 else 0 end) as \"Failure\" from \"ProcessManager\" group by date_trunc('" + chartType + "', \"CreateDateTime\"::TIMESTAMP) order by date_trunc('" + chartType + "',\"CreateDateTime\"::TIMESTAMP)";
                    }
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.systems)
                {

                    string chartType = CreateById;
                    string query = "";
                    if (chartType == "Live")
                    {
                        query = "SELECT \"CreateDatetime\"::TimeStamp,sum(case when \"System_Status\" = 'Not Active' then 1 else 0 end) as \"Not Active\",sum(case when  \"System_Status\" = 'In Progress' then 1 else 0 end) as \"In Progress\",sum(case when  \"System_Status\" = 'Warning' then 1 else 0 end) as \"Warning\",sum(case when  \"System_Status\" = 'Active' then 1 else 0 end) as \"Active\"from \"Systems\" group by \"CreateDatetime\"::TimeStamp order by \"CreateDatetime\"";
                    }
                    else
                    {
                        query = "SELECT date_trunc('" + chartType + "', \"CreateDatetime\"::TIMESTAMP) as \"CreateDatetime\",sum(case when \"System_Status\" = 'Not Active' then 1 else 0 end) as \"Not Active\",sum(case when  \"System_Status\" = 'In Progress' then 1 else 0 end) as \"In Progress\",sum(case when  \"System_Status\" = 'Warning' then 1 else 0 end) as \"Warning\",sum(case when  \"System_Status\" = 'Active' then 1 else 0 end) as \"Active\" from \"Systems\" where \"Status\" = true group by date_trunc('" + chartType + "', \"CreateDatetime\"::TIMESTAMP)  order by date_trunc('" + chartType + "', \"CreateDatetime\"::TIMESTAMP)";
                    }
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.profile)
                {

                    string result = new QueryUtil().getTableData("USER", "Status|Equals|true||ID|Equals|" + CreateById + "", "*", 0, "public", "", "");
                    return result;
                }
                else if (tablename == StringHelper.connectionsetup)
                {
                    string result = StringHelper.truee;
                    try
                    {
                        string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "Wizard\\");

                        using (StreamReader r = new StreamReader(path + "json.json"))
                        {
                            var json = r.ReadToEnd();
                            if (json.Contains(StringHelper.New))
                            {
                                result = StringHelper.truee;
                            }
                            else if (json.Contains(StringHelper.existed))
                            {
                                result = StringHelper.ffalse;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        string ect = ex.Message;
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
                    }
                    return result;
                }

                else if (tablename == StringHelper.dbsetup)
                {

                    string dbresult = "";
                    Dictionary<string, string> details = JsonConvert.DeserializeObject<Dictionary<string, string>>(CreateById);
                    try
                    {
                        Connection c1 = new Connection();
                        string result = c1.VerifyDBServer(details[StringHelper.server], details[StringHelper.portt], details[StringHelper.db], details[StringHelper.nameoftheuser], details[StringHelper.pword]);
                        return result;
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        dbresult = ex.Message.ToString();
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }

                    }
                    return dbresult;
                }
                else if (tablename == StringHelper.dbinstall)

                {
                    string result = "";
                    bool endResult;
                    Dictionary<string, string> details = JsonConvert.DeserializeObject<Dictionary<string, string>>(CreateById);

                    Connection c1 = new Connection(details[StringHelper.server], details[StringHelper.portt], details[StringHelper.db], details[StringHelper.nameoftheuser], details[StringHelper.pword]);
                    endResult = c1.openDB();
                    if (endResult == true)
                    {
                        bool isNew = false;
                        string path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"Web.Config";
                        XmlDocument doc = new XmlDocument();
                        doc.Load(path);
                        XmlNodeList list = doc.DocumentElement.SelectNodes(string.Format("connectionStrings/add[@name='{0}']", "PgSqlConn"));
                        XmlNode node;
                        isNew = list.Count == 0;
                        if (isNew)
                        {
                            node = doc.CreateNode(XmlNodeType.Element, StringHelper.append, null);
                            XmlAttribute attribute = doc.CreateAttribute(StringHelper.name);
                            attribute.Value = StringHelper.pgsqlconn;
                            node.Attributes.Append(attribute);

                            attribute = doc.CreateAttribute(StringHelper.connectionstring);
                            attribute.Value = "";
                            node.Attributes.Append(attribute);

                            attribute = doc.CreateAttribute(StringHelper.providername);
                            attribute.Value = StringHelper.npgsql;
                            node.Attributes.Append(attribute);

                        }
                        else
                        {
                            node = list[0];
                        }
                        string conString = node.Attributes[StringHelper.connectionstring].Value;
                        NpgsqlConnectionStringBuilder conStringBuilder = new NpgsqlConnectionStringBuilder(conString);
                        conStringBuilder.Host = details[StringHelper.server];
                        conStringBuilder.Database = details[StringHelper.db];
                        conStringBuilder.Port = Int32.Parse(details[StringHelper.portt]);
                        conStringBuilder.Username = details[StringHelper.nameoftheuser];
                        conStringBuilder.Password = details[StringHelper.pword];

                        node.Attributes[StringHelper.connectionstring].Value = conStringBuilder.ConnectionString;
                        if (isNew)
                        {
                            doc.DocumentElement.SelectNodes(StringHelper.connectionstring)[0].AppendChild(node);
                        }
                        try
                        {
                            doc.Save(path);
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                            }
                            else
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                            }
                        }
                        result = StringHelper.truee;
                    }
                    else if (endResult == false)
                    {
                        bool isNew = false;
                        string path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"Web.Config";
                        XmlDocument doc = new XmlDocument();
                        doc.Load(path);
                        XmlNodeList list = doc.DocumentElement.SelectNodes(string.Format("connectionStrings/add[@name='{0}']", StringHelper.pgsqlconn));
                        XmlNode node;
                        isNew = list.Count == 0;
                        if (isNew)
                        {
                            node = doc.CreateNode(XmlNodeType.Element, StringHelper.append, null);
                            XmlAttribute attribute = doc.CreateAttribute(StringHelper.name);
                            attribute.Value = StringHelper.pgsqlconn;
                            node.Attributes.Append(attribute);

                            attribute = doc.CreateAttribute(StringHelper.connectionstring);
                            attribute.Value = "";
                            node.Attributes.Append(attribute);

                            attribute = doc.CreateAttribute(StringHelper.providername);
                            attribute.Value = StringHelper.npgsql;
                            node.Attributes.Append(attribute);

                        }
                        else
                        {
                            node = list[0];
                        }
                        string conString = node.Attributes[StringHelper.connectionstring].Value;
                        NpgsqlConnectionStringBuilder conStringBuilder = new NpgsqlConnectionStringBuilder(conString);
                        conStringBuilder.Host = details[StringHelper.server];
                        conStringBuilder.Database = "postgres";
                        conStringBuilder.Port = Int32.Parse(details[StringHelper.portt]);
                        conStringBuilder.Username = details[StringHelper.nameoftheuser];
                        conStringBuilder.Password = details[StringHelper.pword];

                        node.Attributes[StringHelper.connectionstring].Value = conStringBuilder.ConnectionString;

                        if (isNew)
                        {
                            doc.DocumentElement.SelectNodes(StringHelper.connectionstring)[0].AppendChild(node);
                        }

                        if (conString.Length == 0)
                        {
                            conString = conStringBuilder.ConnectionString;
                        }

                        var m_conn = new NpgsqlConnection(conString);
                        var m_createdb_cmd = new NpgsqlCommand("CREATE DATABASE " + details["Database"] + " " +
                                                                //"WITH OWNER = \"postgres\" " +
                                                                "WITH OWNER = '" + details[StringHelper.nameoftheuser] + "'" +
                                                               "ENCODING = 'UTF8' " +
                                                               "CONNECTION LIMIT = -1;", m_conn);
                        try
                        {
                            m_conn.Open();
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                            }
                            else
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                            }
                            result = StringHelper.error;
                        }

                        m_createdb_cmd.ExecuteNonQuery();
                        conStringBuilder.Database = details["Database"];
                        node.Attributes[StringHelper.connectionstring].Value = conStringBuilder.ConnectionString;

                        string[] strFile = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory.ToString() + "Wizard\\Database");

                        foreach (string sqlFl in strFile)
                        {
                            FileInfo file = new FileInfo(Convert.ToString(sqlFl));
                            string txt = file.OpenText().ReadToEnd();
                            string result1 = c1.executeDBScriptSQL(txt);
                        }
                        try
                        {
                            doc.Save(path);
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                            }
                            else
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                            }
                        }
                    }
                    string result2 = string.Empty;

                    using (StreamReader r = new StreamReader(AppDomain.CurrentDomain.BaseDirectory.ToString() + "Wizard\\Json.json"))
                    {
                        var json = r.ReadToEnd();
                        if (json.Contains(StringHelper.New))
                        {
                            result2 = json.Replace(StringHelper.New, StringHelper.existed);

                        }
                    }
                    using (StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory.ToString() + "Wizard\\Json.json"))
                    {

                        sw.WriteLine(result2);
                    }
                    result = StringHelper.ffalse;

                    return result;

                }

                else if (tablename == StringHelper.setuppage)
                {
                    string result = "";

                    Dictionary<string, string> details = JsonConvert.DeserializeObject<Dictionary<string, string>>(CreateById);
                    try
                    {
                        string values = details["FirstName"] + "," + details["LastName"] + ", " + details["Email"] + "," + details["Contact"] + "," + details["Username"] + "," + details["Password"] + ",1,null,null,null,null,null,true,1";
                        result = new QueryUtil().insertTableData("public", "USER", values, "", false);
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        result = StringHelper.ffalse;
                    }
                    return result;
                }
                else if (tablename == StringHelper.notification)
                {
                    Dictionary<string, string> InsertValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(CreateById);

                    string values = InsertValues["Message"] + "," + InsertValues["UserName"] + ", " + InsertValues["CreateBy"] + "," + InsertValues["CreateDateTime"] + "," + InsertValues["UpdateBy"] + "," + InsertValues["UpdateDateTime"] + ",true";
                    string result = new QueryUtil().insertTableData("public", "Notification", values, "", false);
                    return result;

                }
                else if (tablename == "ProcessManager")
                {
                                       
                        string query = "select * from public.\"ProcessManager\" where \"Status\"=true and \"UpdateBy\"='" + CreateById + "' order by \"ID\" desc";
                        string result = c.executeSQL(query, "Select");
                        return result;
                }
                else if (tablename == StringHelper.getscreenID)
                {

                    string result = new QueryUtil().getTableData("MENU", "Status|Equals|true||ID|Equals|" + CreateById + "", "*", 0, "public", "", "");
                    return result;
                }

                else
                {

                    string result = new QueryUtil().getTableData(tablename, "Status|Equals|true||CreateBy|Equals|" + CreateById + "", "*", 0, "public", "", "");
                    return result;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string getTableData(string tablename, string CreateById, string Category)
        {
            string query = "";
            if (Category == null)
            {
                query = "select * from public.\"ProcessManager\" where \"Status\"=true and \"UpdateBy\"='" + CreateById + "' order by \"ID\" desc";
            }
            else
            {
                //query = "select * from public.\"ProcessManager\" where \"Status\"=true and \"UpdateBy\"='" + CreateById + "' and \"JobStatus\"='" + Category + "'  order by \"ID\" desc";
                //query = "select distinct on(r.\"Id\") * from \"ProcessManager\" pm left join \"ROBOT\" r on r.\"Id\"=pm.\"JobName\"::integer inner join \"PROJECT\" p on p.\"ID\"=r.\"Project_Id\" inner join \"LOB\" l on l.\"ID\"=p.\"LOBId\" where r.\"Status\"=true and pm.\"Status\"=true and p.\"Status\"=true and l.\"Status\"=true and \"JobStatus\"='" + Category + "' order by r.\"Id\" desc";
                query = "select distinct on(r.\"Id\") * from \"ProcessManager\" pm left join \"ROBOT\" r on r.\"Id\"=pm.\"RobotId\"::integer inner join \"PROJECT\" p on p.\"ID\"=r.\"Project_Id\" inner join \"LOB\" l on l.\"ID\"=p.\"LOBId\" where r.\"Status\"=true and pm.\"Status\"=true and p.\"Status\"=true and l.\"Status\"=true and \"JobStatus\"='" + Category + "' order by r.\"Id\" desc";
            }

            string result = c.executeSQL(query, "Select");
            return result;
        }
        public string getFilterTableData(string tablename, string Category)
        {
            string query = "";
            if (Category == "" || Category == null)
                query = "select * from public.\"" + tablename + "\" where \"Status\"=true";
            else
                query = "select * from public.\"" + tablename + "\" where \"Status\"=true and \"PortalName\"='" + Category + "' order by \"ID\" desc";

            string result = c.executeSQL(query, "Select");
            return result;
        }
        public string getTableDataByUserID(string tablename, string CreateById)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string table = string.Empty;
                if (tablename.ToLower() == StringHelper.dbconnectionDB)
                {
                    string query = "SELECT * FROM public.\"DBConnection\" where \"DBType\"="+ Convert.ToInt32(CreateById)+ "and \"Status\" = true";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                if (tablename == StringHelper.iinbox)
                {

                    string query = "select i.*,r.\"Name\",s.\"Name\" as \"StepName\" from public.\"Inbox\" i inner join \"ROBOT\" r on i.\"RobotId\" = r.\"Id\" inner join \"STEPS\" s on s.\"Id\" = i.\"StepId\" and i.\"Status\"='true' and r.\"Status\" and i.\"AssignedTo\"='" + CreateById + "'";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                if (tablename == StringHelper.CustomActionProperties)
                {

                    string query = "select * from \"ActionProperties\" where \"Action_Id\" =" + CreateById + " order By \"Order\"";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                if (tablename == StringHelper.CommandCenter_Inbox)
                {

                    string query = "select i.*,r.\"Name\",s.\"Name\" as \"StepName\" from public.\"Inbox\" i inner join \"ROBOT\" r on i.\"RobotId\" = r.\"Id\" inner join \"STEPS\" s on s.\"Id\" = i.\"StepId\" and i.\"Status\"='true' and r.\"Status\"='true' and i.\"AssignedTo\"='" + CreateById + "' and i.\"MsgStatus\" = 'New' order by i.\"ID\" desc";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.processmangerbotdetails)
                {

                    string query = "select \"JobStatus\" as category , count(\"JobStatus\") as value from \"ProcessManager\" where \"RobotId\"::Integer = '" + CreateById + "'  and \"Status\" = 'true' group by \"JobStatus\"";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.schedulercommandcenter)
                {

                    table = StringHelper.scheduler;
                    string result = new QueryUtil().getTableData(table, "RobotId|Equals|" + CreateById + "||Status|Equals|true", "*", 0, "public", "", "");
                    return result;
                }
                else if (tablename == StringHelper.history)
                {

                    string query = "select * from \"ProcessManager\" where \"RobotId\"::Integer = '" + CreateById + "' and \"Status\" = 'true' order by \"StartingTime\"::Timestamp DESC";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.Menu)
                {

                    string query = "select * from \"MENU\" where \"ID\" = '" + CreateById + "' and \"Status\" = 'true'";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.removesystem)
                {

                    string result = new QueryUtil().updateTableData("Systems", "ID|Equals|" + CreateById, "Status|Equals|false", "public", false);
                    return result;
                }

                else if (tablename == StringHelper.processmanager)
                {

                    string chartType = CreateById;
                    string query = "";
                    if (chartType == "Live")
                    {
                        query = "SELECT \"CreateDatetime\"::TimeStamp,sum(case when \"JobStatus\" = 'New' then 1 else 0 end) as \"New\",sum(case when  \"JobStatus\" = 'In Progress' then 1 else 0 end) as \"In Progress\",sum(case when  \"JobStatus\" = 'Success' then 1 else 0 end) as \"Success\",sum(case when  \"JobStatus\" = 'Failure' then 1 else 0 end) as \"Failure\" from \"ProcessManager\"  where \"CreateDatetime\"::Timestamp>date_trunc('week',current_date) - interval '1 week' group by \"CreateDatetime\"::TimeStamp order by \"CreateDatetime\"";
                    }

                    else
                    {
                        query = "SELECT date_trunc('" + chartType + "', \"CreateDatetime\":: TIMESTAMP) as \"CreateDateTime\",sum(case when \"JobStatus\" = 'New' then 1 else 0 end) as \"New\",sum(case when  \"JobStatus\" = 'In Progress' then 1 else 0 end) as \"In Progress\",sum(case when  \"JobStatus\" = 'Success' then 1 else 0 end) as \"Success\",sum(case when  \"JobStatus\" = 'Failure' then 1 else 0 end) as \"Failure\" from \"ProcessManager\" group by date_trunc('" + chartType + "', \"CreateDatetime\"::TIMESTAMP) order by date_trunc('" + chartType + "',\"CreateDatetime\"::TIMESTAMP)";
                    }
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.systems)
                {

                    string chartType = CreateById;
                    string query = "";
                    if (chartType == "Live")
                    {
                        query = "SELECT \"CreateDatetime\"::TimeStamp,sum(case when \"System_Status\" = 'Not Active' then 1 else 0 end) as \"Not Active\",sum(case when  \"System_Status\" = 'In Progress' then 1 else 0 end) as \"In Progress\",sum(case when  \"System_Status\" = 'Warning' then 1 else 0 end) as \"Warning\",sum(case when  \"System_Status\" = 'Active' then 1 else 0 end) as \"Active\" from \"Systems\" group by \"CreateDatetime\"::TimeStamp order by \"CreateDatetime\"";
                    }
                    else
                    {
                        query = "SELECT date_trunc('" + chartType + "', \"CreateDatetime\"::TIMESTAMP) as \"CreateDatetime\",sum(case when \"System_Status\" = 'Not Active' then 1 else 0 end) as \"Not Active\",sum(case when  \"System_Status\" = 'In Progress' then 1 else 0 end) as \"In Progress\",sum(case when  \"System_Status\" = 'Warning' then 1 else 0 end) as \"Warning\",sum(case when  \"System_Status\" = 'Active' then 1 else 0 end) as \"Active\" from \"Systems\" where \"Status\" = true group by date_trunc('" + chartType + "', \"CreateDatetime\"::TIMESTAMP)  order by date_trunc('" + chartType + "', \"CreateDatetime\"::TIMESTAMP)";
                    }
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.profile)
                {

                    string result = new QueryUtil().getTableData("USER", "Status|Equals|true||ID|Equals|" + CreateById + "", "*", 0, "public", "", "");
                    return result;
                }
                else if (tablename == StringHelper.connectionsetup)
                {
                    string result = StringHelper.truee;
                    try
                    {

                        string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(), "Wizard\\");

                        if (!(Directory.Exists(path)))
                        {
                            Directory.CreateDirectory(path);
                        }

                        if (File.Exists(path + "json.json"))
                        {
                            using (StreamReader r = new StreamReader(path + "json.json"))
                            {
                                var json = r.ReadToEnd();
                                if (json.Contains(StringHelper.New))
                                {
                                    result = StringHelper.truee;
                                }
                                else if (json.Contains(StringHelper.existed))
                                {
                                    result = StringHelper.ffalse;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        string ect = ex.Message;
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
                    }
                    return result;
                }

                else if (tablename == StringHelper.dbsetup)
                {

                    string dbresult = "";
                    Dictionary<string, string> details = JsonConvert.DeserializeObject<Dictionary<string, string>>(CreateById);
                    try
                    {
                        Connection c1 = new Connection();
                        string result = c1.VerifyDBServer(details[StringHelper.server], details[StringHelper.portt], details[StringHelper.db], details[StringHelper.nameoftheuser], details[StringHelper.pword]);
                        return result;
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        dbresult = ex.Message.ToString();
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }

                    }
                    return dbresult;
                }
                else if (tablename == StringHelper.dbinstall)

                {
                    string result = "";
                    bool endResult;
                    Dictionary<string, string> details = JsonConvert.DeserializeObject<Dictionary<string, string>>(CreateById);

                    Connection c1 = new Connection(details[StringHelper.server], details[StringHelper.portt], details[StringHelper.db], details[StringHelper.nameoftheuser], details[StringHelper.pword]);
                    endResult = c1.openDB();
                    if (endResult == true)
                    {
                        bool isNew = false;
                        string path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"Web.Config";
                        XmlDocument doc = new XmlDocument();
                        doc.Load(path);
                        XmlNodeList list = doc.DocumentElement.SelectNodes(string.Format("connectionStrings/add[@name='{0}']", "PgSqlConn"));
                        XmlNode node;
                        isNew = list.Count == 0;
                        if (isNew)
                        {
                            node = doc.CreateNode(XmlNodeType.Element, StringHelper.append, null);
                            XmlAttribute attribute = doc.CreateAttribute(StringHelper.name);
                            attribute.Value = StringHelper.pgsqlconn;
                            node.Attributes.Append(attribute);

                            attribute = doc.CreateAttribute(StringHelper.connectionstring);
                            attribute.Value = "";
                            node.Attributes.Append(attribute);

                            attribute = doc.CreateAttribute(StringHelper.providername);
                            attribute.Value = StringHelper.npgsql;
                            node.Attributes.Append(attribute);

                        }
                        else
                        {
                            node = list[0];
                        }
                        string conString = node.Attributes[StringHelper.connectionstring].Value;
                        NpgsqlConnectionStringBuilder conStringBuilder = new NpgsqlConnectionStringBuilder(conString);
                        conStringBuilder.Host = details[StringHelper.server];
                        conStringBuilder.Database = details[StringHelper.db];
                        conStringBuilder.Port = Int32.Parse(details[StringHelper.portt]);
                        conStringBuilder.Username = details[StringHelper.nameoftheuser];
                        conStringBuilder.Password = details[StringHelper.pword];

                        node.Attributes[StringHelper.connectionstring].Value = conStringBuilder.ConnectionString;
                        if (isNew)
                        {
                            doc.DocumentElement.SelectNodes(StringHelper.connectionstring)[0].AppendChild(node);
                        }
                        try
                        {
                            doc.Save(path);
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                            }
                            else
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                            }
                        }
                        result = StringHelper.truee;
                    }
                    else if (endResult == false)
                    {
                        bool isNew = false;
                        string path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"Web.Config";
                        XmlDocument doc = new XmlDocument();
                        doc.Load(path);
                        XmlNodeList list = doc.DocumentElement.SelectNodes(string.Format("connectionStrings/add[@name='{0}']", StringHelper.pgsqlconn));
                        XmlNode node;
                        isNew = list.Count == 0;
                        if (isNew)
                        {
                            node = doc.CreateNode(XmlNodeType.Element, StringHelper.append, null);
                            XmlAttribute attribute = doc.CreateAttribute(StringHelper.name);
                            attribute.Value = StringHelper.pgsqlconn;
                            node.Attributes.Append(attribute);

                            attribute = doc.CreateAttribute(StringHelper.connectionstring);
                            attribute.Value = "";
                            node.Attributes.Append(attribute);

                            attribute = doc.CreateAttribute(StringHelper.providername);
                            attribute.Value = StringHelper.npgsql;
                            node.Attributes.Append(attribute);

                        }
                        else
                        {
                            node = list[0];
                        }
                        string conString = node.Attributes[StringHelper.connectionstring].Value;
                        NpgsqlConnectionStringBuilder conStringBuilder = new NpgsqlConnectionStringBuilder(conString);
                        conStringBuilder.Host = details[StringHelper.server];
                        conStringBuilder.Database = "postgres";
                        conStringBuilder.Port = Int32.Parse(details[StringHelper.portt]);
                        conStringBuilder.Username = details[StringHelper.nameoftheuser];
                        conStringBuilder.Password = details[StringHelper.pword];

                        node.Attributes[StringHelper.connectionstring].Value = conStringBuilder.ConnectionString;

                        if (isNew)
                        {
                            doc.DocumentElement.SelectNodes(StringHelper.connectionstring)[0].AppendChild(node);
                        }

                        if (conString.Length == 0)
                        {
                            conString = conStringBuilder.ConnectionString;
                        }

                        var m_conn = new NpgsqlConnection(conString);
                        var m_createdb_cmd = new NpgsqlCommand("CREATE DATABASE " + details["Database"] + " " +
                                                                //"WITH OWNER = \"postgres\" " +
                                                                "WITH OWNER = '" + details[StringHelper.nameoftheuser] + "'" +
                                                               "ENCODING = 'UTF8' " +
                                                               "CONNECTION LIMIT = -1;", m_conn);
                        try
                        {
                            m_conn.Open();
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                            }
                            else
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                            }
                            result = StringHelper.error;
                        }

                        m_createdb_cmd.ExecuteNonQuery();
                        conStringBuilder.Database = details["Database"];
                        node.Attributes[StringHelper.connectionstring].Value = conStringBuilder.ConnectionString;

                        string[] strFile = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory.ToString() + "Wizard\\Database");

                        foreach (string sqlFl in strFile)
                        {
                            FileInfo file = new FileInfo(Convert.ToString(sqlFl));
                            string txt = file.OpenText().ReadToEnd();
                            string result1 = c1.executeDBScriptSQL(txt);
                        }
                        try
                        {
                            doc.Save(path);
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                            }
                            else
                            {
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                            }
                        }
                    }
                    string result2 = string.Empty;

                    using (StreamReader r = new StreamReader(AppDomain.CurrentDomain.BaseDirectory.ToString() + "Wizard\\Json.json"))
                    {
                        var json = r.ReadToEnd();
                        if (json.Contains(StringHelper.New))
                        {
                            result2 = json.Replace(StringHelper.New, StringHelper.existed);

                        }
                    }
                    using (StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory.ToString() + "Wizard\\Json.json"))
                    {

                        sw.WriteLine(result2);
                    }
                    result = StringHelper.ffalse;

                    return result;

                }

                else if (tablename == StringHelper.setuppage)
                {
                    string result = "";

                    Dictionary<string, string> details = JsonConvert.DeserializeObject<Dictionary<string, string>>(CreateById);
                    try
                    {
                        string values = details["FirstName"] + "," + details["LastName"] + ", " + details["Email"] + "," + details["Contact"] + "," + details["Username"] + "," + details["Password"] + ",1,null,null,null,null,null,true,1";
                        result = new QueryUtil().insertTableData("public", "USER", values, "", false);
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        result = StringHelper.ffalse;
                    }
                    return result;
                }
                else if (tablename == StringHelper.notification)
                {
                    Dictionary<string, string> InsertValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(CreateById);

                    string values = InsertValues["Message"] + "," + InsertValues["UserName"] + ", " + InsertValues["CreateBy"] + "," + InsertValues["CreateDateTime"] + "," + InsertValues["UpdateBy"] + "," + InsertValues["UpdateDateTime"] + ",true";
                    string result = new QueryUtil().insertTableData("public", "Notification", values, "", false);
                    return result;

                }
                else if (tablename == "ProcessManager")
                {

                    string query = "select * from public.\"ProcessManager\" where \"Status\"=true and \"UpdateBy\"='" + CreateById + "' order by \"ID\" desc";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.getscreenID)
                {

                    string result = new QueryUtil().getTableData("MENU", "Status|Equals|true||ID|Equals|" + CreateById + "", "*", 0, "public", "", "");
                    return result;
                }

                else
                {

                    string result = new QueryUtil().getTableData(tablename, "Status|Equals|true||CreateBy|Equals|" + CreateById + "", "*", 0, "public", "", "");
                    return result;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string GetVersionsofRobot(string id)
        {

            string query1 = "SELECT v.rid, v.versionid, r.\"Name\" as robotname,v.\"CreateDatetime\"  FROM public.\"Versions\" as v inner join public.\"ROBOT\" as r ON v.\"rid\"=r.\"Id\"  where r.\"Id\"='" + id + "' and v.\"Status\"=true";
            string result = c.executeSQL(query1, "Select");

            return result;
        }

        public string addMenuTableData(string tablename, string values)
        {
            string result = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (tablename == StringHelper.RoleMenu)
                {
                    Connection c = new Connection();
                    Dictionary<string, string> Values = JsonConvert.DeserializeObject<Dictionary<string, string>>(values);
                    if (Values.Count != 0)
                    {
                        string query = "INSERT INTO public.\"Role_Menu_Relation\" (\"ID\", \"Role_ID\", \"Menu_ID\",\"Status\") VALUES(DEFAULT,'" + Values["Role_ID"] + "','" + Values["Menu_ID"] + "','true')";
                        result = c.executeSQL(query, "Insert");
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }

        public string addTableData(string tablename, string values)
        {
            string result = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (tablename == StringHelper.Menu)
                {                    
                    Dictionary<string, string> Values = JsonConvert.DeserializeObject<Dictionary<string, string>>(values);
                    string Query1 = "Select count(*) FROM public.\"MENU\" where \"Status\"='true' and \"Name\" = '" + Values["Name"] + "'";
                    string rResult1 = c.executeSQL(Query1, "Select");
                    List<Dictionary<string, string>> resultMenu = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(rResult1);
                    if (resultMenu[0]["count"] == "0")
                    {
                        string Query = "INSERT INTO public.\"MENU\"(\"ID\", \"Name\", \"Parent_ID\", \"Path\", \"Class\", \"Role_ID\",\"Status\",\"Type\",\"LOB\") VALUES(DEFAULT,'" + Values["Name"] + "','" + Values["Parent_ID"] + "', '" + Values["Path"] + "','" + Values["Class"] + "','" + Values["Role_ID"] + "','" + Values["Status"] + "','" + Values["Type"] + "','" + Values["LOB"] + "')";
                        result = c.executeSQL(Query, "Insert");
                    }
                    else
                    {
                        result = StringHelper.ffalse;

                    }
                    //string Query = "INSERT INTO public.\"MENU\"(\"ID\", \"Name\", \"Parent_ID\", \"Path\", \"Class\", \"Role_ID\",\"Status\",\"Type\",\"LOB\") VALUES(DEFAULT,'" + Values["Name"] + "','" + Values["Parent_ID"] + "', '" + Values["Path"] + "','" + Values["Class"] + "','" + Values["Role_ID"] + "','" + Values["Status"] + "','" + Values["Type"] + "','" + Values["LOB"] + "')";
                    //result = c.executeSQL(Query, "Insert");

                }
                else if (tablename == StringHelper.RoleMenu)
                {

                    Dictionary<string, string> Values = JsonConvert.DeserializeObject<Dictionary<string, string>>(values);
                    string Query = "Select * FROM public.\"MENU\" where \"Name\" = '" + Values["Name"] + "' and \"Parent_ID\"= '" + Values["Parent_ID"] + "' and \"Path\"='" + Values["Path"] + "' and \"Class\"='" + Values["Class"] + "' and \"Role_ID\"='" + Values["Role_ID"] + "'and \"Status\"='" + Values["Status"] + "'";
                    string result1 = c.executeSQL(Query, "Select");
                    List<Dictionary<string, string>> resultMENU = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(result1);
                    if (resultMENU[0].Count != 0)
                    {
                        string query = "INSERT INTO public.\"Role_Menu_Relation\" (\"ID\", \"Role_ID\", \"Menu_ID\",\"Status\") VALUES(DEFAULT,'" + Values["Role_ID"] + "','" + resultMENU[0]["ID"] + "','" + Values["Status"] + "')";
                        result = c.executeSQL(query, "Insert");
                    }
                }
                else if (tablename == StringHelper.CustomActionProperties)
                {

                    cSQLScripts cs = new cSQLScripts();
                    Dictionary<string, string> Values = JsonConvert.DeserializeObject<Dictionary<string, string>>(values);
                    string Query = "INSERT INTO public.\"ActionProperties\"(\"Id\", \"Name\", \"Description\", \"Action_Id\", \"PropertyType\", \"CreateBy\", \"CreateDatetime\", \"UpdateBy\", \"UpdateDatetime\", \"Status\", \"Order\", \"CustomValidation\")VALUES(DEFAULT, '" + cs.Base64Decode(Values["Name"]) + "', '" + cs.Base64Decode(Values["Description"]) + "', '" + Values["Action_Id"] + "', '" + cs.Base64Decode(Values["PropertyType"]) + "', '" + cs.Base64Decode(Values["CreateBy"]) + "', '" + cs.Base64Decode(Values["CreateDatetime"]) + "', null, null, true, '" + cs.Base64Decode(Values["Order"]) + "', null);";
                    result = c.executeSQL(Query, "Insert");

                }
                //else if (tablename == StringHelper.Service)
                //{
                //    cSQLScripts cs = new cSQLScripts();
                //    Dictionary<string, string> Values = JsonConvert.DeserializeObject<Dictionary<string, string>>(values);
                //    string Query = "INSERT INTO public.\"Service\"(\"ServiceId\", \"Servicename\", \"ExternalId\", \"Meta_Txt\", \"User_Id\", \"subscriptionId\")VALUES(DEFAULT, '" + Values["Servicename"] + "', " + Values["ExternalId"] + ", '" + Values["Meta_Txt"] + "', " + Values["User_Id"] + ", " + Values["subscriptionId"] + ");";
                //    result = c.executeSQL(Query, "Insert");
                //}

                //else if (tablename == StringHelper.constraints)
                //{
                //    cSQLScripts cs = new cSQLScripts();
                //    Dictionary<string, string> Values = JsonConvert.DeserializeObject<Dictionary<string, string>>(values);
                //    string Query = "INSERT INTO public.\"constraints\"(\"constraintsid\", \"diskspace\", \"disk_name\", \"user_id\")VALUES(DEFAULT, " + Values["diskspace"] + ", '" + Values["disk_name"] + "', " + Values["user_id"] + ");";
                //    result = c.executeSQL(Query, "Insert");
                //}

                //else if (tablename == StringHelper.Subscription)
                //{
                //    cSQLScripts cs = new cSQLScripts();
                //    Dictionary<string, string> Values = JsonConvert.DeserializeObject<Dictionary<string, string>>(values);
                //    string Query = "INSERT INTO public.\"subscription\"(\"subscriptionid\", \"startdate\", \"enddate\", \"time_period\", \"is_trailed\", \"user_id\", \"status\")VALUES(DEFAULT, '" + Values["startdate"] + "', '" + Values["enddate"] + "', " + Values["time_period"] + ", " + Values["is_trailed"] + ", " + Values["user_id"] + ", '" + Values["status"] + "');";
                //    result = c.executeSQL(Query, "Insert");
                //}
                else
                {
                    try
                    {

                        cSQLScripts cs = new cSQLScripts();
                        JObject Values = JObject.Parse(values);
                        Dictionary<string, string> dictValues = Values.ToObject<Dictionary<string, string>>();
                        IList<string> CNames = dictValues.Keys.ToArray();
                        if (tablename == StringHelper.systemsrobots)
                        {
                            string temp = "Select count(*) from \"SystemsRobots\" where \"Robot_Id\"= '" + cs.Base64Decode(dictValues["Robot_Id"].Replace(" ", "+")) + "' and \"System_Id\" = '" + cs.Base64Decode(dictValues["System_Id"].Replace(" ", "+")) + "' and \"Status\" = true ";

                            string tResult = c.executeSQL(temp, "Select");
                            if (tResult != StringHelper.ffalse)
                            {
                                List<Dictionary<string, string>> tValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(tResult);
                                if (tValues[0]["count"] != "0")
                                {
                                    return StringHelper.truee;
                                }
                            }

                        }

                        string query1 = "INSERT INTO public.\"" + tablename + "\"(";
                        string query2 = "VALUES(";
                        if (tablename == "ConnectionConfig")
                        {
                            query1 += "\"ID\",";
                            query2 += "default,";
                        }
                        for (var i = 0; i < (dictValues.Count); i++)
                        {
                            string Base64Result = cs.Base64Decode(dictValues[CNames[i]].Replace(" ", "+")).Replace("'", "''").Replace("&amp;", "&").Replace("&nbsp;", " ").Replace("&lt;", "<").Replace("&gt;", ">");
                            if (i != (dictValues.Count - 1))
                            {
                                
                                query1 += "\"" + CNames[i].ToString() + "\",";
                                if (Base64Result == "")
                                    query2 += "'',";
                                else
                                    query2 += "'" + Base64Result + "',";
                            }
                            else
                            {
                                query1 += "\"" + CNames[i].ToString() + "\")";
                                if (Base64Result == "")
                                    query2 += "null)";
                                else
                                    query2 += "'" + Base64Result + "')";
                            }

                        }

                        string query = query1 + query2;
                        //result = c.executeSQL(query, "Insert");
                        if (tablename == "EmailTemplate")
                        {
                            string Query1 = "Select count(*) FROM public.\"EmailTemplate\" where \"Status\"='true' and \"Subject\" = '" + cs.Base64Decode(dictValues["Subject"].Replace(" ", "+")) + "'";
                            string rResult1 = c.executeSQL(Query1, "Select");                         
                                List<Dictionary<string, string>> resultROLE = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(rResult1);
                                if (resultROLE[0]["count"] == "0")
                                {
                                    result = c.executeSQL(query, "Insert");
                                }
                                else
                                {                                   
                                    result = StringHelper.ffalse;

                                }
                        }
                        else if (tablename == "Systems")
                        {
                            string Query1 = "Select count(*) FROM public.\"Systems\" where \"Status\"='true' and \"Name\" = '" + cs.Base64Decode(dictValues["Name"].Replace(" ", "+")) + "'";
                            string rResult1 = c.executeSQL(Query1, "Select");
                            // if (rResult1 != StringHelper.ffalse)
                            //{
                            List<Dictionary<string, string>> resultROLE = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(rResult1);
                            if (resultROLE[0]["count"] == "0")
                            {
                                //result = StringHelper.ffalse;
                                result = c.executeSQL(query, "Insert");
                            }
                            else
                            {
                                result = StringHelper.ffalse;
                            }
                        }
                        else if(tablename== "EmailTemplateParameters")
                        {
                            string Query1 = "Select count(*) FROM public.\"EmailTemplateParameters\" where \"Status\"='true' and \"Name\" = '" + cs.Base64Decode(dictValues["Name"].Replace(" ", "+")) + "'";
                            string rResult1 = c.executeSQL(Query1, "Select");                    
                            List<Dictionary<string, string>> resultROLE = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(rResult1);
                            if (resultROLE[0]["count"] == "0")
                            {
                                result = c.executeSQL(query, "Insert");
                            }
                            else
                            {
                                result = StringHelper.ffalse;
                            }
                        }
                        else if (tablename == "DBType")
                        {
                            string Query1 = "Select count(*) FROM public.\"DBType\" where \"Status\"='true' and  \"Name\" = '" + cs.Base64Decode(dictValues["Name"].Replace(" ", "+")) + "'";
                            string rResult1 = c.executeSQL(Query1, "Select");
                            List<Dictionary<string, string>> resultROLE = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(rResult1);
                            if (resultROLE[0]["count"] == "0")
                            {
                                result = c.executeSQL(query, "Insert");
                            }
                            else
                            {
                                result = StringHelper.ffalse;
                            }
                        }
                        else if (tablename == "ConnectionConfig")
                        {
                            string Query1 = "Select count(*) FROM public.\"ConnectionConfig\" where \"Status\"='true' and  \"connectionname\" = '" + cs.Base64Decode(dictValues["connectionname"].Replace(" ", "+")) + "'";
                            string rResult1 = c.executeSQL(Query1, "Select");
                            List<Dictionary<string, string>> resultROLE = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(rResult1);
                            if (resultROLE[0]["count"] == "0")
                            {
                                result = c.executeSQL(query, "Insert");
                            }
                            else
                            {
                                result = StringHelper.ffalse;
                            }
                        }
                        else if (tablename == "DBConnection")
                        {
                            string Query1 = "Select count(*) FROM public.\"DBConnection\" where \"Status\"='true' and  \"Name\" = '" + cs.Base64Decode(dictValues["Name"].Replace(" ", "+")) + "'";
                            string rResult1 = c.executeSQL(Query1, "Select");
                            List<Dictionary<string, string>> resultROLE = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(rResult1);
                            if (resultROLE[0]["count"] == "0")
                            {
                                result = c.executeSQL(query, "Insert");
                            }
                            else
                            {
                                result = StringHelper.ffalse;
                            }
                        }
                        else
                        {
                            result = c.executeSQL(query, "Insert");
                        }   
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);

                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string addTableDataForLob(string tablename, string values)
        {


            cSQLScripts cs = new cSQLScripts();
            JObject Values = JObject.Parse(values);
            Dictionary<string, string> dictValues = Values.ToObject<Dictionary<string, string>>();
            IList<string> CNames = dictValues.Keys.ToArray();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (tablename == "LOB")
                {
                    string lob = "Select count(*) from \"LOB\" where \"Name\"= '" + cs.Base64Decode(dictValues["Name"].Replace(" ", "+")) + "'  and \"Status\" = true ";

                    string LResult = c.executeSQL(lob, "Select");

                    List<Dictionary<string, string>> LValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(LResult);
                    if (LValues[0]["count"] == "0")
                    {
                        return "true";
                    }
                    else
                    {
                        return "false";
                    }
                }
                else if (tablename == StringHelper.kpi)
                {
                    string Kpi = "Select count(*) from \"KPI\" where \"Title\"= '" + cs.Base64Decode(dictValues["Title"].Replace(" ", "+")) + "'  and \"Status\" = true ";

                    string KResult = c.executeSQL(Kpi, "Select");

                    List<Dictionary<string, string>> KValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(KResult);
                    if (KValues[0]["count"] == "0")
                    {
                        return StringHelper.truee;
                    }
                    else
                    {
                        return StringHelper.ffalse;
                    }
                }

                else if (tablename == StringHelper.nkpi)
                {
                    string nKpi = "Select count(*) from \"NKPI\" where \"Title\"= '" + cs.Base64Decode(dictValues["Title"].Replace(" ", "+")) + "'  and \"Status\" = true and \"Role_Id\"= '" + cs.Base64Decode(dictValues["Role_Id"].Replace(" ", "+")) + "'";

                    string NResult = c.executeSQL(nKpi, "Select");

                    List<Dictionary<string, string>> NValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(NResult);
                    if (NValues[0]["count"] == "0")
                    {
                        return StringHelper.truee;
                    }
                    else
                    {
                        return StringHelper.ffalse;
                    }
                }
                else if (tablename == StringHelper.srole)
                {
                    string srole = "Select count(*) from \"ROLE\" where \"RoleName\"= '" + cs.Base64Decode(dictValues["RoleName"].Replace(" ", "+")) + "'  and \"Status\" = true ";

                    string RResult = c.executeSQL(srole, "Select");

                    List<Dictionary<string, string>> RValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(RResult);
                    if (RValues[0]["count"] == "0")
                    {
                        return StringHelper.truee;
                    }
                    else
                    {
                        return StringHelper.ffalse;
                    }
                }
                else if (tablename == StringHelper.scharttype)
                {
                    string scharttype = "Select count(*) from \"Chart_Type\" where \"Type\"= '" + cs.Base64Decode(dictValues["Type"].Replace(" ", "+")) + "'  and \"Status\" = true ";

                    string CResult = c.executeSQL(scharttype, "Select");

                    List<Dictionary<string, string>> CValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(CResult);
                    if (CValues[0]["count"] == "0")
                    {
                        return StringHelper.truee;
                    }
                    else
                    {
                        return StringHelper.ffalse;
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getStepPropertiesDetails(string stepId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string result = new QueryUtil().getTableData("STEPPROPERTIES", "Steps_Id|Equals|" + stepId + "||Status|Equals|true", "*", 0, "public", "", "");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string GetRobotlog(string tablename, string roleid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string query = "SELECT * FROM \"ProcessManager\" where \"Status\"=true and \"RobotId\"= '" + roleid + "' ORDER BY \"CreateDatetime\" DESC LIMIT 1";
                string result1 = c.executeSQL(query, "Select");
                List<Dictionary<string, string>> details = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(result1);
                string Starttime = details[0][StringHelper.startingtime];
                string result = string.Empty;
                if (details[0][StringHelper.endingtime] != "")
                {
                    query = "Select * from \"EXECUTIONLOG\" where \"CreateDateTime\"::TIMESTAMP >= '" + details[0]["StartingTime"] + "'  and \"CreateDateTime\"::TIMESTAMP <= '" + details[0]["EndingTime"] + "' and \"Robot_ID\" = '" + roleid + "'  and \"Status\"= 'true'  ";
                    result = c.executeSQL(query, "Select");
                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getapp_tablesData()
        {

            string query = "select table_id,table_name,category,status,createby,updateby,createdatetime,updatedatetime,comments from app_tables where status='true'";
            string result = c.executeSQL(query, "Select");
            return result;
        }
        public string UpdateTableData(string tablename, string values, string ID)
        {
            string result = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (tablename == StringHelper.Menu)
                {

                    Dictionary<string, string> Values = JsonConvert.DeserializeObject<Dictionary<string, string>>(values);
                    string Query = "UPDATE public.\"MENU\" SET \"Name\" = '" + Values["Name"] + "', \"Path\" = '" + Values["Path"] + "', \"Class\" = '" + Values["Class"] + "', \"Role_ID\" = '" + Values["Role_ID"] + "',\"Type\" = '" + Values["Type"] + "',\"LOB\"='" + Values["LOB"] + "',\"Status\"='" + Values["Status"] + "' where \"ID\" = '" + ID + "' ";
                    result = c.executeSQL(Query, "Update");
                }
                else
                {

                    try
                    {
                        cSQLScripts cs = new cSQLScripts();

                        JObject Values = JObject.Parse(values);
                        Dictionary<string, string> dictValues = Values.ToObject<Dictionary<string, string>>();
                        IList<string> CNames = dictValues.Keys.ToArray();
                        JObject Id = JObject.Parse(ID);
                        Dictionary<string, string> dictID = Id.ToObject<Dictionary<string, string>>();
                        IList<string> IDName = dictID.Keys.ToArray();
                        string query = "Update public.\"" + tablename + "\" SET ";

                        for (var i = 0; i < (dictValues.Count); i++)
                        {
                            string Base64Result = "";
                            if (CNames[i] == "CurrentOutput")
                            {
                                Base64Result = System.Uri.UnescapeDataString(cs.Base64Decode(dictValues[CNames[i]].Replace(" ", "+"))).Replace("'", "''").Replace("&amp;", "&").Replace("&nbsp;", " ").Replace("&lt;", "<").Replace("&gt;", ">");
                            }
                            else
                            {
                                Base64Result = cs.Base64Decode(dictValues[CNames[i]].Replace(" ", "+")).Replace("'", "''").Replace("&amp;", "&").Replace("&nbsp;", " ").Replace("&lt;", "<").Replace("&gt;", ">");
                            }
                            Base64Result = Base64Result.Trim();
                            if (i != (dictValues.Count - 1))
                            {
                                if (Base64Result != "")
                                {
                                    query += "\"" + CNames[i].ToString() + "\"='" + Base64Result + "',";
                                }
                                else

                                    query += "\"" + CNames[i].ToString() + "\"=null, ";
                            }
                            else
                            {
                                if (Base64Result != "")
                                    query += "\"" + CNames[i].ToString() + "\"='" + Base64Result + "'";
                                else
                                    query += "\"" + CNames[i].ToString() + "\"=null ";

                            }

                        }
                        query += "where \"" + IDName[0] + "\"='" + dictID[IDName[0]] + "'";
                        result = c.executeSQL(query, "update");
                    }


                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                        }
                        else
                        {
                            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                        }
                        return ex.Message;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string TableDataupdateforLob(string tablename, string values, string ID)
        {

            cSQLScripts cs = new cSQLScripts();
            JObject Values = JObject.Parse(values);
            Dictionary<string, string> dictValues = Values.ToObject<Dictionary<string, string>>();
            IList<string> CNames = dictValues.Keys.ToArray();
            //Dictionary<string, string> dictID = ID.ToObject<Dictionary<string, string>>();
            Dictionary<string, string> dictID = JsonConvert.DeserializeObject<Dictionary<string, string>>(ID);
            //IList<string> ProjID = dictID.Keys.ToArray();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (tablename == "LOB")
                {
                    string lob = "Select count(*) from \"LOB\" where \"Name\"= '" + cs.Base64Decode(dictValues["Name"].Replace(" ", "+")) + "'  and \"Status\" = true ";

                    string LResult = c.executeSQL(lob, "Select");

                    List<Dictionary<string, string>> LValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(LResult);
                    if (LValues[0]["count"] == "0")
                    {
                        return "true";
                    }
                    else
                    {
                        return "false";
                    }
                }
                else if (tablename == "PROJECT")
                {
                    //string projectQ = "Select count(*) from \"PROJECT\" where \"NAME\"= '" + cs.Base64Decode(dictValues["NAME"].Replace(" ", "+")) + "' and \"WorkFlowProjectType\"='" + cs.Base64Decode(dictValues["WorkFlowProjectType"].Replace(" ", "+")) + "' and \"CreateBy\"='" + cs.Base64Decode(dictValues["UpdateBy"].Replace(" ", "+")) + "' and \"Status\" = true ";
                    string projectQ = "UPDATE \"PROJECT\" SET \"NAME\"= '" + cs.Base64Decode(dictValues["NAME"].Replace(" ", "+")) + "' , \"WorkFlowProjectType\"='" + cs.Base64Decode(dictValues["WorkFlowProjectType"].Replace(" ", "+")) + "' , \"UpdateBy\"='" + cs.Base64Decode(dictValues["UpdateBy"].Replace(" ", "+")) + "' , \"Status\" = true WHERE \"ID\"=" + (dictID["ID"].Replace(" ", ""));

                    string LResult = c.executeSQL(projectQ, "update");

                    //List<Dictionary<string, string>> LValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(LResult);
                    //if (LValues[0]["count"] == "0")
                    if (LResult == "true")
                    {
                        return "true";
                    }
                    else
                    {
                        return "false";
                    }
                }


                else if (tablename == "ROBOT")
                {
                    string RobotQ = "Select count(*) from \"ROBOT\" where \"Name\"= '" + cs.Base64Decode(dictValues["Name"].Replace(" ", "+")) + "'";

                    string LResult = c.executeSQL(RobotQ, "Select");

                    List<Dictionary<string, string>> LValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(LResult);
                    if (LValues[0]["count"] == "0")
                    {
                        return "true";
                    }
                    else
                    {
                        return "false";
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }

        }
        public string SystemsByRobotAssigned(string RobotId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string query = " select r.\"Name\" as RobotName ,r.\"Description\" , s.* from \"SystemsRobots\" sr inner join \"Systems\" s on sr.\"System_Id\" = s.\"ID\" inner join \"ROBOT\" r on r.\"Id\" = sr.\"Robot_Id\" inner join \"PROJECT\" p on p.\"ID\"=r.\"Project_Id\" inner join \"LOB\" l on l.\"ID\"=p.\"LOBId\" where r.\"Id\" = ' " + RobotId + " ' and r.\"Status\" = true and s.\"Status\" = true and sr.\"Status\" = true ";
                string result = c.executeSQL(query, "Select");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getGridData(string tablename)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (tablename == StringHelper.screentable)
                {

                    string result = new QueryUtil().getTableData(tablename, "Status|Equals|true", "*", 0, "public", "", "");
                    return result;
                }

                else if (tablename == StringHelper.action || tablename == StringHelper.actionproperties || tablename == StringHelper.actionproperty || tablename == StringHelper.element || tablename == StringHelper.robot || tablename == StringHelper.timesheets || tablename == StringHelper.steps || tablename == StringHelper.workflow || tablename == StringHelper.cases)
                {

                    string result = new QueryUtil().getTableData(tablename, "Status|Equals|true", "*", 0, "public", "\"Id\"", "");
                    return result;
                }
                else if (tablename == StringHelper.stepproperties)
                {

                    string result = new QueryUtil().getTableData(tablename, "Status|Equals|true", "*", 1000, "public", "\"Id\"", "");
                    return result;
                }
                else if (tablename == StringHelper.ConnectivitySystems)
                {

                    string query = "Select s.*,d.\"Server\" as serverName from \"Systems\" s inner join \"DBConnection\" d on s.\"Server\"::integer = d.\"ID\"  and s.\"Status\"=true and d.\"Status\"=true";

                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.ConnectivityRobots)
                {

                    string query = "Select r.*,sr.\"System_Id\" as sytemID,s.\"Name\" as systemName,d.\"Server\" as ServerName from \"Systems\" s inner join \"DBConnection\" d on s.\"Server\"::integer = d.\"ID\" inner join \"SystemsRobots\" sr on sr.\"System_Id\" = s.\"ID\" inner join \"ROBOT\" r on sr.\"Robot_Id\" = r.\"Id\" and r.\"Status\" = true and sr.\"Status\" and s.\"Status\"=true and d.\"Status\"=true";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.ConnectivityServerDetails)
                {

                    string query = "Select * from \"ServerStatus\" ";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.ConnectivityInfo)
                {

                    string query = "Select * from \"ConnectivityInfo\" ";
                    string result = c.executeSQL(query, "Select");
                    return result;
                }
                else if (tablename == StringHelper.PendingSubscriptions)
                {

                    string result = new QueryUtil().getTableData(tablename, "Status|Equals|true", "*", 0, "public", "\"ID\"", "");
                    return result;
                }
                else if (tablename == StringHelper.PendingSubscriptionsapproval)
                {

                    string result = new QueryUtil().getTableData("PendingSubscriptions", "Status|Equals|true||SubscriptionStatus|Equals|Approved", " *", 0, "public", "\"ID\"", "");
                    return result;
                }
                else
                {

                    string result = new QueryUtil().getTableData(tablename, "Status|Equals|true", "*", 0, "public", "\"ID\"", "desc");
                    return result;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string Connective_info()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<List<string>> finalResult = new List<List<string>>();
                List<Dictionary<string, string>> success = new List<Dictionary<string, string>>();
                List<Dictionary<string, string>> failed = new List<Dictionary<string, string>>();
                List<int> temp = new List<int>();
                bool endResult;


                string query = "Select d.*,t.\"Name\" as \"DBName\" from \"DBConnection\" d inner join \"DBType\" t on t.\"ID\" = d.\"DBType\" and t.\"Status\"=true and d.\"Status\"=true";
                string result = c.executeSQL(query, "Select");
                List<DBConnection> ListValues = JsonConvert.DeserializeObject<List<DBConnection>>(result);
                List<ConnectivityInfo> _objList = new List<ConnectivityInfo>();

                foreach (DBConnection d in ListValues)
                {

                    if (d.DBName == StringHelper.mysqldb)
                    {
                        MysqlConnect s = new MysqlConnect(d.Server, d.Port, d.Database, d.Username, d.Password);
                        endResult = s.OpenConnection();
                        if (endResult == true)
                        {
                            s.CloseConnection();
                        }

                        int r = Convert.ToInt32(endResult);
                        ConnectivityInfo _objCont = new ConnectivityInfo();
                        _objCont.Id = d.ID.ToString();
                        _objCont.Server = d.Server.ToString();
                        _objCont.Name = d.Name.ToString();
                        _objCont.Status = r.ToString();
                        _objList.Add(_objCont);
                    }
                    else if (d.DBName == StringHelper.postgressql)
                    {
                        Connection c1 = new Connection(d.Server, d.Port, d.Database, d.Username, d.Password);
                        endResult = c1.openDB();

                        if (endResult == true)
                        {
                            c1.closeDB();
                        }
                        int r = Convert.ToInt32(endResult);
                        ConnectivityInfo _objCont = new ConnectivityInfo();
                        _objCont.Id = d.ID.ToString();
                        _objCont.Server = d.Server.ToString();
                        _objCont.Name = d.Name.ToString();
                        _objCont.Status = r.ToString();
                        _objList.Add(_objCont);
                    }
                }

                string _sqlStr_del = "Delete from public.\"ConnectivityInfo\"";
                string _delresult = c.executeSQL(_sqlStr_del, "delete");

                foreach (ConnectivityInfo ci in _objList)
                {

                    string _sqlStr_ins = "INSERT INTO public.\"ConnectivityInfo\"(\"ID\", \"Server\", \"Name\", \"Status\")	VALUES('" + ci.Id + "', '" + ci.Server + "', '" + ci.Name + "', '" + ci.Status + "'); ";
                    string _insrtresult = c.executeSQL(_sqlStr_ins, "insert");
                }
                return JsonConvert.SerializeObject(_objList);
            }
            catch (Exception ex)
            {
                string exception = ex.Message;
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return exception;
            }

        }
        public string Connectivity_info(string conn_string,string conn_name, string conn_type, string conn_ID) {
            string status = string.Empty;
            string servername = string.Empty;

            if (conn_type == "Database")
            {
                string[] tmp = conn_string.Split(';');
                string[] server = tmp[0].Split('=');
                if ((server[0].ToLower() == "server") && server[1] != null)
                {
                    servername = server[1];
                
                    if (tmp[4].ToLower() == "database=postgres")
                    {
                        try
                        {
                            NpgsqlConnection pg_con = new NpgsqlConnection(conn_string);
                            pg_con.Open();
                            status = "Success";
                            pg_con.Close();
                        }
                        catch (Exception ex)
                        {
                            new Logger().LogException(ex);
                            status = "Failed";
                        }
                    }
                    else if (tmp[1].ToLower() == "database=mysql")
                    {
                        try
                        {
                            MySqlConnection mysql_conn = new MySqlConnection((JsonConvert.DeserializeObject<MySqlConnectionStringBuilder>(conn_string)).ToString());
                            mysql_conn.Open();
                            status = "Success";
                            mysql_conn.Close();
                        }
                        catch
                        {
                            status = "Failure";
                        }
                    }
                    else { status = "Failure"; }
                    }
                else
                {
                    servername = conn_string;
                }
            }
            else if (conn_type == "Server")
            {
                servername = conn_string;
                try
                {
                    Ping myPing = new Ping();
                    PingReply reply = myPing.Send(conn_string, 1000);
                    if (reply != null)
                    {
                        //Console.WriteLine(reply.ToString());
                        status = reply.Status.ToString();
                    }
                    else {
                        status = "Failure";
                    }
                }
                catch
                {
                    status = "Failure";
                    Console.WriteLine("ERROR: You have Some TIMEOUT issue");
                }
            }
            else if (conn_type == "API") {
                string[] tmp = conn_string.Split(';');
                string[] URL = tmp[0].Split('=');
                string[] API_Type = tmp[1].Split('=');
                if (URL[0] == "URL" && URL[1] != null)
                {
                    servername = URL[1];
                }
                if (API_Type[0] == "API_Type" && API_Type[1] != null)
                {
                    try
                    {
                        if (API_Type[1] == "PythonServer")
                        {
                            System.Net.WebClient client = new System.Net.WebClient();
                            string result = client.DownloadString(URL[1]);
                            Regex reg = new Regex("[;\\\\/*?\"<>{}|&']");
                            result = reg.Replace(result.Trim(), string.Empty);
                            string[] API_Name = result.Split(",");
                            for (var i = 0; i < API_Name.Length; i++)
                            {
                                if (reg.Replace(API_Name[i].Split(":")[0], string.Empty) == conn_name)
                                {
                                    if (reg.Replace(API_Name[i].Split(":")[1], string.Empty) == "200")
                                    {
                                        status = "Success";
                                    }
                                    else
                                    {
                                        status = "Failure";
                                    }
                                }
                            }
                        }
                        else {
                            try
                            {
                                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(URL[1]);
                                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                                if (myHttpWebResponse.StatusCode == HttpStatusCode.OK)
                                    status = "Success";
                                myHttpWebResponse.Close();
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                status = "Failure";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        new Logger().LogException(ex);
                        status = "Failure";
                    }
                }
                else
                {
                    try
                    {
                        System.Net.WebClient client = new System.Net.WebClient();
                        string result = client.DownloadString("https://alphaapi.ez-bot.ai/swagger");
                    }
                    catch
                    {
                        status = "Failure";
                    }
                }
            }
            bool state; string connection = string.Empty;
            if (status == "Success") { state = true; connection = "Running"; } else { state = false; connection = "Stopped"; }
            string ConnInfo_ins = "INSERT INTO public.\"ConnectivityInfo\"(\"ID\", \"Server\", \"Name\", \"Status\",\"CreateBy\",\"CreateDatetime\",\"Connection_ID\",\"Connection_Status\")	VALUES(default, '" + servername + "', '" + conn_name + "', true ,'postgres',now()," + conn_ID + ",'"+ connection + "'); ";
            string ConnInfo_ins_res = c.executeSQL(ConnInfo_ins, "insert");
            string ConnConf_upd = "UPDATE public.\"ConnectionConfig\" SET \"Connection_Status\"=" + state + " WHERE \"ID\"=" + conn_ID;
            string ConnConf_upd_res = c.executeSQL(ConnConf_upd, "Update");
            string ConnConf_del = "DELETE FROM public.\"ConnectivityInfo\" WHERE \"CreateDatetime\"::TimeStamp::date < CURRENT_DATE";
            string ConnConf_del_old = c.executeSQL(ConnConf_del, "delete");
            return status; 
        }
        public string getConnectivity_Info(string tablename, string Name, string Server, int Conn_ID) {
            string result = string.Empty;
            if (tablename == "ConnectivityInfo")
            {
                string query = "select * from \"ConnectivityInfo\" WHERE \"Status\"=true and \"Connection_ID\" = " + Conn_ID + " order by \"ID\" desc";
                result = c.executeSQL(query, "Select");
            }
            return result;
        }
        public string CommandCenterNKPIclick(string clickedNKPI)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {


                string query1 = "SELECT * FROM ( select distinct on (pm.\"RobotId\") pm.\"RobotId\",s.\"Name\" as \"SystemName\",r.*,p.\"CreateBy\" as \"CreatedBy\",pm.\"CreateBy\" as \"SubmittedBy\" from \"ProcessManager\" pm left join \"ROBOT\" r on r.\"Id\" = pm.\"RobotId\"::integer inner join \"PROJECT\" p on p.\"ID\" = r.\"Project_Id\" inner join \"LOB\" l on l.\"ID\" = p.\"LOBId\" left join \"Systems\" s on s.\"Name\"=pm.\"SubmittedBy\" where pm.\"JobStatus\" = '" + clickedNKPI + "' and r.\"Status\" = true and pm.\"Status\" = true and p.\"Status\" = true and l.\"Status\" = true  ) t ORDER BY \"Id\" Desc";
                string result = c.executeSQL(query1, "Select");

                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return "";
            }
        }
        public string CommandCenterSystemNKPIclick(string clickedNKPI)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (clickedNKPI== "Available") {
                    string query1 = "SELECT * FROM \"Systems\" where \"Status\"=true and \"System_Status\" NOT in ('Active','Not Active','Warning')";
                    string result = c.executeSQL(query1, "Select");

                    return result;
                }
                else
                {
                    string query1 = "SELECT * FROM \"Systems\" where \"Status\"=true and \"System_Status\"='" + clickedNKPI + "'";
                    string result = c.executeSQL(query1, "Select");

                    return result;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string ConnectionGridData(string tablename, string type, PagingParams data)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string data_columns = "";
                for (var i = 0; i < data.columns.Count; i++)
                {
                    if (i != data.columns.Count - 1) {
                        data_columns += "\""+ data.columns[i].data + "\",";
                            }
                    else {
                        data_columns += "\"" + data.columns[i].data + "\"";
                    }
                }
                string query1 = "SELECT "+ data_columns + " FROM \""+ tablename + "\" WHERE \"connectiontype\"='" + type + "'and \"Status\"=true ORDER BY \"ID\" Desc";
                string result = c.executeSQL(query1, "Select");

                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return "";
            }
        }
        public string deleteTableData(string tablename, string ID)
        {
            string result = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                JObject Id = JObject.Parse(ID);
                Dictionary<string, string> dictID = Id.ToObject<Dictionary<string, string>>();
                IList<string> CID = dictID.Keys.ToArray();
                if (tablename == "USER")
                {
                    //var JSONObj = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(ID);
                    string query = "SELECT \"UserName\" FROM \"USER\" WHERE \"ID\"=" + dictID[CID[0]];
                    string uname = dbd.DbExecuteQuery(query, "selectwithnodes");
                    string query1 = "SELECT \"Email_ID\" FROM \"USER\" WHERE \"ID\"=" + dictID[CID[0]];
                    string uemail = dbd.DbExecuteQuery(query1, "selectwithnodes");
                    string now = DateTime.Now.ToString("yyyyMMddHHmmss");
                    uname = String.Concat(uname, now);
                    uemail = String.Concat(uemail, now);
                    string query2 = new QueryUtil().updateTableData(tablename, CID[0].ToString() + "|Equals|" + dictID[CID[0]], "UserName|Equals|" + uname, "public", false);
                    string query3 = new QueryUtil().updateTableData(tablename, CID[0].ToString() + "|Equals|" + dictID[CID[0]], "Email_ID|Equals|" + uemail, "public", false);
                }
               else if(tablename=="LOB")
                {
                    string query1 = "SELECT count(*) FROM \"PROJECT\" where \"Status\"='true' and \"LOBId\"=" + dictID[CID[0]];
                    string lobresult1 = c.executeSQL(query1, "Select");
                    if (lobresult1 != StringHelper.ffalse)
                    {
                        List<Dictionary<string, string>> lValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lobresult1);
                        if (lValues[0]["count"] != "0")
                        {
                            result = StringHelper.ffalse;
                        }
                        else
                        {
                            result = new QueryUtil().updateTableData(tablename, CID[0].ToString() + "|Equals|" + dictID[CID[0]], "Status|Equals|false", "public", false);
                        }
                    }
                 }
                else if (tablename == "ROLE")
                {
                    string query1 = "SELECT count(*) FROM \"MENU\" where \"Status\"='true' and \"Role_ID\"=" + dictID[CID[0]];
                    string lobresult1 = c.executeSQL(query1, "Select");                    
                    List<Dictionary<string, string>> lValues1 = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lobresult1);
                    string query2 = "SELECT count(*) FROM \"Role_Menu_Relation\" where \"Status\"='true' and \"Role_ID\"=" + dictID[CID[0]];
                    string lobresult2 = c.executeSQL(query2, "Select");
                    List<Dictionary<string, string>> lValues2 = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lobresult2);                   
                    string query3 = "SELECT count(*) FROM \"USER\" where \"Status\"='true' and \"Role_ID\"=" + dictID[CID[0]];
                    string lobresult3 = c.executeSQL(query3, "Select");
                    List<Dictionary<string, string>> lValues3 = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lobresult3);
                    if (lValues1[0]["count"] != "0" || lValues2[0]["count"] != "0" || lValues3[0]["count"] != "0")
                        {
                            result = StringHelper.ffalse;
                        }
                        else
                        {
                            result = new QueryUtil().updateTableData(tablename, CID[0].ToString() + "|Equals|" + dictID[CID[0]], "Status|Equals|false", "public", false);
                        }                   
                }
                else if(tablename== "EmailTemplate")
                {
                    string query1 = "SELECT count(*) FROM \"STEPPROPERTIES\" where \"Status\"='true' and \"StepProperty\"='Subject' and \"StepPropertyValue\"=(SELECT \"Subject\" from \"EmailTemplate\" where \"ID\"= " + dictID[CID[0]] +")";
                    string lobresult1 = c.executeSQL(query1, "Select");
                    List<Dictionary<string, string>> lValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lobresult1);
                    if (lValues[0]["count"] != "0")
                    {
                        result = StringHelper.ffalse;
                    }
                    else
                    {
                        result = new QueryUtil().updateTableData(tablename, CID[0].ToString() + "|Equals|" + dictID[CID[0]], "Status|Equals|false", "public", false);
                    }
                }
                else if (tablename == "DBType")
                {
                    string query1 = "SELECT count(*) FROM \"STEPPROPERTIES\" where \"Status\"='true' and \"StepProperty\"='DB Type' and \"StepPropertyValue\"=(SELECT \"Name\" from \"DBType\" where \"ID\"= " + dictID[CID[0]] + ")";
                    string lobresult1 = c.executeSQL(query1, "Select");
                    List<Dictionary<string, string>> lValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lobresult1);
                    if (lValues[0]["count"] != "0")
                    {
                        result = StringHelper.ffalse;
                    }
                    else
                    {
                        result = new QueryUtil().updateTableData(tablename, CID[0].ToString() + "|Equals|" + dictID[CID[0]], "Status|Equals|false", "public", false);
                    }
                }
                else
                {
                   result = new QueryUtil().updateTableData(tablename, CID[0].ToString() + "|Equals|" + dictID[CID[0]], "Status|Equals|false", "public", false);

                }
                //string result = new QueryUtil().updateTableData(tablename, CID[0].ToString() + "|Equals|" + dictID[CID[0]], "Status|Equals|false", "public", false);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        //MenuPermissionsScreen
        public string hardDeleteTableData(string tablename, string roleid)
        {
            string result = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (tablename == StringHelper.RoleMenu)
                {
                    Connection c = new Connection();
                    string Query = "DELETE FROM \"Role_Menu_Relation\" WHERE \"Role_ID\" = '" + roleid + "';";
                    result = c.executeSQL(Query, "delete");
                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }

        public string tableDataByVersionId(string tablename, string id)
        {
            if (tablename == "ConnectivityGraph_Systems")
            {
                string query = "Select s.*,d.\"Server\" as serverName from \"Systems\" s inner join \"DBConnection\" d on s.\"Server\"::integer = d.\"ID\" and d.\"ID\"= " + id + " and s.\"Status\"=true and d.\"Status\"=true";
                string result = c.executeSQL(query, "Select");
                return result;
            }
            else if (tablename == "ConnectivityGraph_Robots") {
                string query = "Select r.*,sr.\"System_Id\" as sytemID,s.\"Name\" as systemName,d.\"Server\" as ServerName from \"Systems\" s inner join \"DBConnection\" d on s.\"Server\"::integer = d.\"ID\" inner join \"SystemsRobots\" sr on sr.\"System_Id\" = s.\"ID\" inner join \"ROBOT\" r on sr.\"Robot_Id\" = r.\"Id\" and d.\"ID\"= " + id + " and r.\"Status\" = true and sr.\"Status\"=true and s.\"Status\"=true and d.\"Status\"=true";
                string result = c.executeSQL(query, "Select");
                return result;
            }
            else
            {
                string query1 = "SELECT v.rid, v.versionid, r.\"Name\" as robotname,v.\"CreateDatetime\"  FROM public.\"" + tablename + "\" as v inner join public.\"ROBOT\" as r ON v.\"rid\"=r.\"Id\"  where r.\"Id\"='" + id + "' and v.\"Status\"=true";
                string result = c.executeSQL(query1, "Select");
                return result;
            }
            
        }
        public string updateRobots(string BotID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string ret = new QueryUtil().updateTableData("Inbox", "ID|Equals|" + BotID, "MsgStatus|Equals|Accepted", "public", false);
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public int generateFiles(string SName, string TName, bool AInsert, bool AEdit, bool ADelete, bool SChild, string DSort, string FilterClause, int RCount, string Preferences, string Fields, string Insights, string FormType)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                //Connection con = new Connection();
                string time = DateTime.Now.ToShortTimeString();
                string date = DateTime.Now.ToShortDateString();
                string values = SName + "," + TName + ", " + AInsert + "," + AEdit + "," + ADelete + "," + SChild + "," + DSort + "," + FilterClause + "," + RCount + "," + Preferences + "," + Fields + ",true," + Insights + "," + FormType + "";

                string valueS = string.Empty;
                foreach (string gval in values.Split(',').ToList())
                {
                    if (gval == string.Empty)
                    {
                        valueS = valueS + ", null";
                    }
                    else
                    {
                        valueS = valueS + ", " + gval;
                    }
                }

                values = valueS.Trim();
                values = values.Remove(0, 1);
                string query1 = "select count(*) from public.\"MENU\" where \"Name\"='" + SName + "' and \"Status\"=true";
                string result1 = c.executeSQL(query1, "Select");
                List<Dictionary<string, string>> lValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(result1);
                if (lValues[0]["count"] != "0")
                {
                    return 0;
                }
                else
                {
                    string ret = new QueryUtil().insertTableData("public", "ScreenTable", values, "", false);
                    string query = "select max(\"Screen_ID\") from public.\"ScreenTable\"";
                    string result = c.executeSQL(query, "Select");
                    JArray obj = JArray.Parse(result);
                    int val = Convert.ToInt32(obj[0]["max"]);
                    return val;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return 0;
            }
        }
        public string updateFiles(int Sid, string SName, string TName, bool AInsert, bool AEdit, bool ADelete, bool SChild, string DSort, string FilterClause, int RCount, string Preferences, string Fields, string Insights, string formtype)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Connection con = new Connection();
                //string result = new QueryUtil().updateTableData("ScreenTable", "Screen_ID|Equals|" + Sid, "Screen_ID|Equals|" + Sid + "||Screen_Name|Equals|" + SName + "||Table_Name|Equals|" + TName + "||Allow_Insert|Equals|" + AInsert + "||Allow_Edit|Equals|" + AEdit + "||Allow_Delete|Equals|" + ADelete + "||Show_As_Child|Equals|" + SChild + "||Default_Sort|Equals|" + DSort + "||Default_Filter_Clause|Equals|null||Row_Count|Equals|" + RCount + "||Preferences|Equals|" + Preferences + "||Fields|Equals|null||Insights|Equals|" + Insights + "||FormType|Equals|" + formtype, "public", false);
                string result = new QueryUtil().updateTableData("ScreenTable", "Screen_ID|Equals|" + Sid, "Screen_Name|Equals|" + SName + "||Table_Name|Equals|" + TName + "||Allow_Insert|Equals|" + AInsert + "||Allow_Edit|Equals|" + AEdit + "||Allow_Delete|Equals|" + ADelete + "||Show_As_Child|Equals|" + SChild + "||Default_Sort|Equals|" + DSort + "||Default_Filter_Clause|Equals|" + FilterClause + "||Row_Count|Equals|" + RCount + "||Preferences|Equals|" + Preferences + "||Fields|Equals|" + Fields + "||Insights|Equals|" + Insights + "||FormType|Equals|" + formtype, "public", false);

                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getScreen(int id)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Connection c = new Connection();
                string result = new QueryUtil().getTableData("ScreenTable", "Screen_ID|Equals|" + id + "", "\"Screen_Name\", \"Table_Name\", \"Allow_Insert\", \"Allow_Edit\", \"Allow_Delete\", \"Show_As_Child\", \"Default_Sort\", \"Default_Filter_Clause\", \"Row_Count\",\"Preferences\",\"Insights\",\"FormType\",\"Fields\"", 0, "public", "", "");

                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getDataByRelationTable(string tableDetails)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Dictionary<string, string> tableContent = JsonConvert.DeserializeObject<Dictionary<string, string>>(tableDetails);
                Connection c = new Connection();
                string query = "select distinct on (a.\"" + tableContent["ConditionValue"] + "\") * from public.\"" + tableContent["tablename"] + "\" a inner join \"" + tableContent["RTablename"] + "\" b on b.\"" + tableContent["ConditionName"] + "\"=a.\"" + tableContent["ConditionValue"] + "\" where a.\"Status\"=true and b.\"Status\"=true and ";
                string[] Values = tableContent["MainConditionIdValue"].Split(',');
                query += "(";
                foreach (var i in Values)
                {

                    query += "\"" + tableContent["MainConditionIdName"] + "\"='" + i + "'";

                    query += " or ";
                }
                query = query.Remove(query.Length - 4);
                query += ")";
                string result = c.executeSQL(query, "Select");

                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string UpdateNKPIPath(string path, int id)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = string.Empty;

                string Query = "UPDATE public.\"NKPI\" SET \"Group\" = '" + path + "' where \"ID\" = '" + id + "' ";
                result = c.executeSQL(Query, "Update");

                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

    }
}
