﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Genbase.BLL;
using Genbase.DAL;
using Newtonsoft.Json;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.Classes
{
    public class ExecutionLog
    {
        string time = DateTime.Now.ToShortTimeString();
        DAL.PostgresConnect dbd = new DAL.PostgresConnect();
        string date = DateTime.Now.ToShortDateString();
        BLL.EncoderDecoderBase64 baser = new BLL.EncoderDecoderBase64();

        public void add2Log(string msg, string robotid, string stepname, string type)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (msg.Contains("'"))
                {
                    msg = msg.Replace("'", "''");
                }
                string values = baser.Base64Encode(stepname) + "," + baser.Base64Encode(msg) + "," + baser.Base64Encode(robotid) + "," + baser.Base64Encode("Admin") + "," + baser.Base64Encode(date + " " + time) + "," + baser.Base64Encode("Admin") + "," + baser.Base64Encode(date + " " + time) + "," + baser.Base64Encode("true") + "," + baser.Base64Encode(type);
                string ret = new QueryUtil().insertTableData("public", "EXECUTIONLOG", values, "", true);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
            }
        }
        public string getFromLogs(string robotid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "select * from public.\"EXECUTIONLOG\" s where s.\"Robot_ID\" = " + robotid + "  and s.\"Type\"!='720' and  s.\"Type\"!='721'";
                string ret = dbd.DbExecuteQuery(query, "select");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getFromLogs(string robotid, bool trus)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = string.Empty;
                string query = "select * from public.\"EXECUTIONLOG\" where \"Robot_ID\"=" + robotid + " and \"Status\"=true order by \"ID\" desc limit 1";
                string ret = dbd.DbExecuteQuery(query, "select");
                List<Dictionary<string, string>> details = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(ret);
                if (details.Count > 0)
                {
                    string createTime = details[0]["CreateDatetime"];
                    query = "select * from public.\"EXECUTIONLOG\" where \"CreateDatetime\" = '" + createTime + "' and \"Status\"=true and \"Robot_ID\"=" + robotid + "";
                    result = dbd.DbExecuteQuery(query, "select");
                    return result;
                }
                else
                {
                    return ret;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public void deleteFromLogs(string robotid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().updateTableData("EXECUTIONLOG", "Robot_ID|Equals|" + robotid, "Status|Equals|false", "public", false);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
            }
        }
        public string getFromLogsbyTime(string robotid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "SELECT distinct(\"CreateDatetime\") FROM public.\"EXECUTIONLOG\" where \"Robot_ID\" = 12;";
                string ret = dbd.DbExecuteQuery(query, "select");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        //public string getFromRobotsLog(string robotid)
        //{
        //    List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
        //    try
        //    {
        //        List<robotsLogs> rler = new List<robotsLogs>();

        //        string lastTime = "SELECT distinct(\"CreateDatetime\") FROM public.\"EXECUTIONLOG\" where \"Robot_ID\" = " + robotid + " order by \"CreateDatetime\" desc limit 1 ";
        //        string lasttime = dbd.DbExecuteQuery(lastTime, "selectRobotsLogs");
        //        if (lasttime == "error")
        //        {
        //            lasttime = "0";
        //        }
        //        string createtime = new QueryUtil().getTableData("ROBOT", "Id|Equals|" + robotid + "", "\"CreateDatetime\"", 0, "public", "", "");
        //        string systemid = new QueryUtil().getTableData("SystemsRobots", "Robot_Id|Equals|" + robotid + "", "\"System_Id\"", 0, "public", "", "");
        //        string systemname = new QueryUtil().getTableData("Systems", "ID|Equals|" + systemid + "", "\"Name\"", 0, "public", "", "");
        //        string robotname = new QueryUtil().getTableData("ROBOT", "ID|Equals|" + robotid + "", "\"Name\"", 0, "public", "", "");

        //        robotsLogs rl = new robotsLogs();
        //        rl.ID = robotid;
        //        rl.Name = robotname;
        //        rl.SystemID = systemid;
        //        rl.SystemName = systemname;
        //        rl.CreateTime = createtime;
        //        rl.LastExecutedTime = lasttime;
        //        rl.Status = StringHelper.active;
        //        rler.Add(rl);

        //        string ret = Newtonsoft.Json.JsonConvert.SerializeObject(rler);

        //        return ret;
        //    }
        //    catch (Exception ex)
        //    {
        //        new Logger().LogException(ex);
        //        if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
        //        {
        //            msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
        //        }
        //        else
        //        {
        //            msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

        //        }
        //        return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
        //    }
        //}
        public string getFromRobotsLog(string robotid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<RobotsLogs> rler = new List<RobotsLogs>();

                string lastTime = "SELECT distinct(\"CreateDatetime\") FROM public.\"EXECUTIONLOG\" where \"Robot_ID\" = " + robotid + " order by \"CreateDatetime\" desc limit 1 ";
                string lasttime = dbd.DbExecuteQuery(lastTime, "selectRobotsLogs");
                if (lasttime == "error")
                {
                    lasttime = "0";
                }
                string createtime = new QueryUtil().getTableData("ROBOT", "Id|Equals|" + robotid + "", "\"CreateDatetime\"", 0, "public", "", "");
                string systemid = new QueryUtil().getTableData("SystemsRobots", "Robot_Id|Equals|" + robotid + "", "\"System_Id\"", 0, "public", "", "");
                string systemname = new QueryUtil().getTableData("Systems", "ID|Equals|" + systemid + "", "\"Name\"", 0, "public", "", "");
                string robotname = new QueryUtil().getTableData("ROBOT", "Id|Equals|" + robotid + "", "\"Name\"", 0, "public", "", "");

                RobotsLogs rl = new RobotsLogs();
                rl.ID = robotid;
                rl.Name = robotname;
                rl.SystemID = systemid;
                rl.SystemName = systemname;
                rl.CreateTime = createtime;
                rl.LastExecutedTime = lasttime;
                rl.Status = StringHelper.active;
                rler.Add(rl);

                string ret = Newtonsoft.Json.JsonConvert.SerializeObject(rler);

                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public void add2Log(string msg, string robotid, string stepname, string type, bool isContains)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (msg.Contains("'"))
                {
                    msg = msg.Replace("'", "''");
                }
                string values = baser.Base64Encode(stepname) + "," + baser.Base64Encode(msg) + "," + baser.Base64Encode(robotid) + "," + baser.Base64Encode("Admin") + "," + baser.Base64Encode(date + " " + time) + "," + baser.Base64Encode("Admin") + "," + baser.Base64Encode(date + " " + time) + "," + baser.Base64Encode("true") + "," + baser.Base64Encode(type);
                string ret = new QueryUtil().insertTableData("public", "EXECUTIONLOG", values, "", true);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
            }
        }

        public void add2Log(Exception exception, Steps step, string where, string type)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();

            try
            {
                string msg = CreateErrorLogs(exception, step, where);
                string stepname = step.Name;
                string robotid = step.Robot_Id;

                if (msg.Contains("'"))
                {
                    msg = msg.Replace("'", "''");
                }
                string values = baser.Base64Encode(stepname) + "," + baser.Base64Encode(msg) + "," + baser.Base64Encode(robotid) + "," + baser.Base64Encode("Admin") + "," + baser.Base64Encode(date + " " + time) + "," + baser.Base64Encode("Admin") + "," + baser.Base64Encode(date + " " + time) + "," + baser.Base64Encode("true") + "," + baser.Base64Encode(type);
                string ret = new QueryUtil().insertTableData("public", "EXECUTIONLOG", values, "", true);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
            }
        }

        /// <summary>
        /// Generate Alphanumeric code => with 1 Digit top, 2 Digit Element Id, 2 Digit Action Id, Exception Id and Exception Message
        /// </summary>
        /// <param name="msg">Exception => ex</param>
        /// <param name="step">Steps => step</param>
        /// <param name="where">S/C/O => server/client/other</param>
        /// <returns></returns>
        public string CreateErrorLogs(Exception msg, Steps step, string where)
        {
            string excode = string.Empty;
            try
            {
                string top = string.Empty;
                if (where.Trim().ToLower() == "server")
                {
                    top = "S";
                }
                else if (where.Trim().ToLower() == "client")
                {
                    top = "C";
                }
                else
                {
                    top = "O";
                }

                if (step != null)
                {
                    excode = "\n" + top + " : " + step.Element_Id + " : " + step.Action_Id + " : " + msg.HResult.ToString() + " => " + msg.Message;
                }
                else
                {
                    excode = "\n" + top + " : " + "00" + " : " + "00" + " : " + msg.HResult.ToString() + " => " + msg.Message;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
            }
            return excode;
        }
    }
}
