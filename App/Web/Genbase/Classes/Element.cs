﻿using Genbase.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Genbase.BLL;
using Genbase.Classes;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.classes
{
    public class Element
    {
        DAL.PostgresConnect dbd = new DAL.PostgresConnect();
        public string getElements()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("ELEMENT", "Status|Equals|true", "\"Id\", \"Name\",\"Icon\", \"Description\",\"Category\"", 0, "public", "\"Id\"", "ASC");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }

        }
        public string getElementById(string elemId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("ELEMENT", "Id|Equals|" + elemId + "||Status|Equals|true", "\"Id\", \"Name\",\"Icon\", \"Description\"", 0, "public", "", "");
                List<Elements> elementsret = JsonConvert.DeserializeObject<List<Elements>>(ret);

                string retr = string.Empty;
                foreach (Elements elementret in elementsret)
                {
                    retr = elementret.Name;
                }
                return retr;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getElementDetails(string element)
        {
            return StringHelper.notimplemented;
        }
    }
}