﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Genbase.BLL;

namespace Genbase.Classes
{
    public class Logger
    {
        ExecutionLog log = new ExecutionLog();
        public void LogException(Exception ex, [CallerMemberName] string memberName = "")
        {
            string fileName1 = @"\ErrorLog" + new DateTime().Date + ".txt";
            string fileName = "ErrorLog" + DateTime.Now.ToString(StringHelper.ymd) + ".txt";

            //string path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)));
            string path = Directory.GetCurrentDirectory() + "\\Uploads";
            //path = path.Substring(6);

            Variables.errorsList.Add(memberName + "~~" + ex.Message);
            string dirName = path + "\\Logs";
            if (!Directory.Exists(dirName))
            {
                Directory.CreateDirectory(dirName);
            }
            fileName = dirName + "\\" + fileName;

            try
            {
                // Check if file already exists. If yes, delete i
                if (File.Exists(fileName))
                {
                    using (var tw = new StreamWriter(fileName, true))
                    {
                        tw.WriteLine("=====================================================================================================================");
                        tw.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace + "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                        tw.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                        tw.Close();
                    }
                }
                else
                {
                    // Create a new file 
                    using (StreamWriter sw = File.CreateText(fileName))
                    {
                        sw.WriteLine("New file created: {0}", DateTime.Now.ToString());
                        sw.WriteLine(Environment.MachineName);
                        sw.WriteLine("****//\\\\*****");
                        sw.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace + "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                        sw.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                        sw.WriteLine("=====================================================================================================================");
                        sw.Close();
                    }
                }
            }
            catch (Exception Ex)
            {
                string ec = Ex.Message;
                new ExecutionLog().add2Log(Ex.Message, "", "", "701");
                new ExecutionLog().add2Log(ex.Message, "", "", "701");
            }
        }
        public void LogException(string message)
        {
            string fileName1 = @"\ErrorLog" + DateTime.Now.ToString(StringHelper.ymd) + ".txt";
            string fileName = @"\StepsLog" + DateTime.Now.ToString(StringHelper.ymd) + ".txt";

            //string path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)));
            string path = Directory.GetCurrentDirectory() + "\\Uploads";
            //path = path.Substring(6);
            string dirName = path + "\\Logs";
            if (!Directory.Exists(dirName))
            {
                Directory.CreateDirectory(dirName);
            }
            fileName = dirName + "\\" + fileName;

            try
            {
                // Check if file already exists. If yes, delete it. 
                if (File.Exists(fileName))
                {
                    using (var tw = new StreamWriter(fileName, true))
                    {
                        tw.WriteLine("=====================================================================================================================");
                        tw.WriteLine("Message :" + message);
                        tw.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                        tw.Close();
                    }
                }
                else
                {
                    // Create a new file 
                    using (StreamWriter sw = File.CreateText(fileName))
                    {
                        sw.WriteLine("New file created: {0}", DateTime.Now.ToString());
                        sw.WriteLine(Environment.MachineName);
                        sw.WriteLine("****//\\\\*****");
                        sw.WriteLine("Message :" + message);
                        sw.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                        sw.WriteLine("=====================================================================================================================");
                        sw.Close();
                    }
                }
            }
            catch (Exception Ex)
            {
                string ec = Ex.Message;
                new ExecutionLog().add2Log(Ex.Message, "", "", "701");
                new ExecutionLog().add2Log(message, "", "", "701");
            }
        }
        public void LogException(Exception ex, string RobotDetails, string StepDetails, [CallerMemberName] string memberName = "")
        {
            string fileName1 = @"\ErrorLog" + new DateTime().Date + ".txt";
            string fileName = "ErrorLog" + DateTime.Now.ToString(StringHelper.ymd) + ".txt";

            //string path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)));
            string path = Directory.GetCurrentDirectory() + "\\Uploads";
            //path = path.Substring(6);

            Variables.errorsList.Add(memberName + "~~" + ex.Message + StepDetails + RobotDetails);
            string dirName = path + "\\Logs";
            if (!Directory.Exists(dirName))
            {
                Directory.CreateDirectory(dirName);
            }
            fileName = dirName + "\\" + fileName;

            try
            {
                // Check if file already exists. If yes, delete i
                if (File.Exists(fileName))
                {
                    using (var tw = new StreamWriter(fileName, true))
                    {
                        tw.WriteLine("=====================================================================================================================");
                        tw.WriteLine("Message :" + ex.Message + "<br/> Robot Details:" + RobotDetails + "<br/> Step Details:" + StepDetails + Environment.NewLine + "StackTrace :" + ex.StackTrace + "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                        tw.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                        tw.Close();
                    }

                }
                else
                {
                    // Create a new file 
                    using (StreamWriter sw = File.CreateText(fileName))
                    {
                        sw.WriteLine("New file created: {0}", DateTime.Now.ToString());
                        sw.WriteLine(Environment.MachineName);
                        sw.WriteLine("****//\\\\*****");
                        sw.WriteLine("Message :" + ex.Message + "<br/> Robot Details:" + RobotDetails + "<br/> Step Details:" + StepDetails + Environment.NewLine + "StackTrace :" + ex.StackTrace + "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                        sw.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                        sw.WriteLine("=====================================================================================================================");
                        sw.Close();
                    }
                }
            }
            catch (Exception Ex)
            {
                string ec = Ex.Message;
                new ExecutionLog().add2Log(Ex.Message, "", "", "701");
                new ExecutionLog().add2Log(ex.Message, "", "", "701");
            }
        }
    }
}
