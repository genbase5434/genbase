﻿using Genbase.BLL;
using Genbase.classes;
using Genbase.Classes;
using Genbase.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Genbase.Classes
{
    public class Robot
    {
        Connection c = Connection.GetInstance;
		PostgresConnect dbd =  PostgresConnect.GetInstance;
		//PostgresConnect dbd = new PostgresConnect();
        Step sc = new Step();
        LinkNode cl = new LinkNode();
        VariableClass cv = new VariableClass();
        AppExecution ape = new AppExecution();
        string time = DateTime.Now.ToShortTimeString();
        string date = DateTime.Now.ToShortDateString();
        Step cs = new Step();
        ResponseResult responseResult = new ResponseResult();
        public string updateProcessManager(string jobid, string status)
        {
            string query = "";
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (status == "In Progress")
                    query = new QueryUtil().updateTableData("ProcessManager", "ID|Equals|" + jobid + "", "JobStatus|Equals|" + status + "||EndingTime|Equals|" + date + " " + time + "", "", false);
                else
                    query = new QueryUtil().updateTableData("ProcessManager", "ID|Equals|" + jobid + "", "JobStatus|Equals|" + status + "||EndingTime|Equals|" + date + " " + time + "", "", false);
                return query;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string createProcessManager(string SystemId, string RobotId, string SystemName, string MAC)
        {
            //string query = "";
            if (SystemId == "Win Adhoc" && MAC != null)
            {
                string sysQuery = "Select \"ID\" from \"Systems\" where \"MAC_Address\" like '%" + MAC + "%' and \"Status\"=true";
                string sysResult = new Connection().executeSQL(sysQuery, "Select");
                if (sysResult != "false")
                {
                    List<Systems> sResult = JsonConvert.DeserializeObject<List<Systems>>(sysResult);
                    if (sResult.Count > 0)
                    {
                        SystemId = sResult[0].ID;
                    }
                }
            }
            string getrname = "Select \"Name\" from \"ROBOT\" where \"Id\"="+RobotId;
            string getRobot = new Connection().executeSQL(getrname, "Select");
			List<RobotDetails> rName = JsonConvert.DeserializeObject<List<RobotDetails>>(getRobot);
            string query = "INSERT INTO public.\"ProcessManager\" VALUES(default," + SystemId + ",'" + RobotId + "','" + rName[0].Name + "','" + SystemName + "','In Progress','',now()::character varying,'','" + SystemName + "',now()::character varying,'" + SystemName + "',now()::character varying,true) Returning \"ID\";";
            //string values = SystemId + "," + RobotId + "," + SystemName + ",In Progress,," + date + " " + time + ",," + SystemName + "," + date + " " + time + "," + SystemName + "," + date + " " + time + ",true";
            //string ret = new QueryUtil().insertTableData("public", "ProcessManager", values, "ID", false);
            string ret = new PostgresConnect().DbExecuteQuery(query, "createrobot");
            return ret;
        }
        public string createRobot(string robot, string robotname, string projectid, string Description, string CreateBy, string UpdateBy)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "SELECT count(*)	FROM public.\"ROBOT\" where lower(\"Name\")='" + robotname.ToLower() + "' and \"Project_Id\" = " + projectid + " and \"RobotType\"='" + robot + "' and \"Status\"=true";
                string ret = dbd.DbExecuteQuery(query, "select");
                if (ret.Contains("1"))
                {
                    ret = "false";
                }
                else
                {
                    query = "insert into public.\"ROBOT\" values (default,'" + robotname + "','" + Description + "','" + robot + "','" + projectid + "','" + CreateBy + "','" + date + " " + time + "','" + UpdateBy + "','" + date + " " + time + "',true) RETURNING \"Id\" ";
                    ret = dbd.DbExecuteQuery(query, "createrobot");
                    string values = ret + "," + robotname + ",false,," + date + " " + time + ",Admin," + date + " " + time + ",Admim," + "true" + "";
                    values = new QueryUtil().insertTableData("public", "RobotProperties", values, "", false);
                }
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string updateRobot(string robot)
        {
            string query = "update public.\"ROBOT\" SET \"Name\" ='AR77' where \"Name\" = 'AR3'";
            string ret = dbd.DbExecuteQuery(query, "update");
            return ret;
        }
        public string getRobot(string Robot)
        {
            string ret = new QueryUtil().getTableData("ROBOT", "Status|Equals|" + true + "", "*", 0, "public", "", "");
            return ret;
        }
        public string getRobotI(string rid)
        {
            string ret = new QueryUtil().getTableData("ROBOT", "Id|Equals|" + rid + "||Status|Equals|" + true + "", "*", 0, "public", "", "");
            return ret;
        }
        public string getRobot(string RobotId, string ProjectId)
        {
            string ret = new QueryUtil().getTableData("ROBOT", "Id|Equals|" + RobotId + "||Status|Equals|" + true + "", "*", 0, "public", "", "");
            return ret;
        }
        public string getAllRobots(string Robot)
        {
            string ret = new QueryUtil().getTableData("ROBOT", "Name|Equals|" + Robot + "||Status|Equals|" + true + "", "*", 0, "public", "", "");
            return ret;
        }
        public string getallRobots()
        {
            string ret = new QueryUtil().getTableData("ROBOT", "Status|Equals|" + true + "", "*", 0, "public", "", "");
            return ret;
        }
        public string FilterChartsOfRobots()
        {
            string query = "select r.*,l.\"Name\" as \"LOB\" from \"ROBOT\" r inner join \"PROJECT\" pr on r.\"Project_Id\" = pr.\"ID\" inner join \"LOB\" l on pr.\"LOBId\" = l.\"ID\" and l.\"Status\"=true and pr.\"Status\" = true and r.\"Status\"=true";
            string result = c.executeSQL(query, "Select");
            return result;
        }
        public string FilterChartsOfRobots(string category)
        {
            string query = "";
            if (category == null)
                query = "select r.*,l.\"Name\" as \"LOB\" from \"ROBOT\" r inner join \"PROJECT\" pr on r.\"Project_Id\" = pr.\"ID\" inner join \"LOB\" l on pr.\"LOBId\" = l.\"ID\" and l.\"Status\"=true and pr.\"Status\" = true and r.\"Status\"=true";
            else
                query = "select r.*,l.\"Name\" as \"LOB\" from \"ROBOT\" r inner join \"PROJECT\" pr on r.\"Project_Id\" = pr.\"ID\" inner join \"LOB\" l on pr.\"LOBId\" = l.\"ID\" and l.\"Status\"=true and pr.\"Status\" = true and r.\"Status\"=true and l.\"Name\"='" + category + "'";
            string result = c.executeSQL(query, "Select");
            return result;
        }
        public string deleteRobot(string robotid, string parentid)
        {
            string ret = new QueryUtil().updateTableData("ROBOT", "Id|Equals|" + robotid + "||Project_Id|Equals|" + parentid, "Status|Equals|false", "", false);
            return ret;
        }
        public string updateRobotProperties(RobotProperties rProperties)
        {
            string ret = new QueryUtil().updateTableData("RobotProperties", "Robot_ID|Equals|" + rProperties.Robot_ID + "||Status|Equals|" + true, "Client_Dependency|Equals|" + rProperties.Client_Dependency + "||Name|Equals|" + rProperties.Name + "||Default_Execution|Equals|" + rProperties.Default_Execution, "", false);
            return ret;
        }
        public string getRobots(string projectid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("ROBOT", "Project_Id|Equals|" + projectid + "||Status|Equals|" + true + "", "*", 0, "public", "", "");
                List<Robots> rbts = JsonConvert.DeserializeObject<List<Robots>>(ret);
                foreach (Robots rbt in rbts)
                {
                    string PResult = new QueryUtil().getTableData("RobotProperties", "Robot_ID|Equals|" + rbt.Id + "", "*", 0, "public", "", "");
                    RobotProperties rp = new RobotProperties();
                    if (PResult != "[]")
                        rp = JsonConvert.DeserializeObject<List<RobotProperties>>(PResult)[0];
                    List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(sc.getSteps(rbt.Id));
                    string vquery = "select max(\"versionid\") from \"Versions\" where \"rid\" =" + rbt.Id + "";
                    string vs = dbd.DbExecuteQuery(vquery, "createproject");
                    steps = steps.FindAll(stp => stp.Version == vs);
                    foreach (Steps step in steps)
                    {
                        List<StepProperties> props = JsonConvert.DeserializeObject<List<StepProperties>>(sc.getStepProperties(step.Id));
                        step.StepProperties = props;
                    }
                    List<LinkNodes> nodes = JsonConvert.DeserializeObject<List<LinkNodes>>(cl.getAllNodes(rbt.Id));
                    rbt.Steps = steps;
                    rbt.LinkNodes = nodes;
                    rbt.RProperties = rp;
                    List<FVariables> fvs = JsonConvert.DeserializeObject<List<FVariables>>(cv.getVariables(rbt.Id));
                    List<DVariables> dvs = new List<DVariables>();
                    dvs.AddRange(fvs.ConvertAll(x => new DVariables() { vlname = x.Name, vlvalue = x.Value, vlstatus = x.VStatus, vlscope = x.Scope, vltype = x.Type, RobotID = x.Robot_Id }));
                    if (dvs.Count > 0)
                    {
                        if (rbt.dynamicvariables != null)
                        {
                            rbt.dynamicvariables.AddRange(dvs);
                        }
                        else
                        {
                            rbt.dynamicvariables = new List<DVariables>();
                            rbt.dynamicvariables = dvs;
                        }
                    }
                }

                return JsonConvert.SerializeObject(rbts);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getVersionRobots(string rbtid, string versionid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("ROBOT", "Id|Equals|" + rbtid + "||Status|Equals|" + true + "", "*", 0, "public", "", "");
                List<Robots> rbts = JsonConvert.DeserializeObject<List<Robots>>(ret);
                string vs = versionid;
                foreach (Robots rbt in rbts)
                {
                    string stpsList = new Step().getSteps(rbt.Id, vs);

                    List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(stpsList);

                    List<LinkNodes> nodes = JsonConvert.DeserializeObject<List<LinkNodes>>(cl.getAllNodes(rbt.Id));
                    rbt.Steps = steps;
                    rbt.LinkNodes = nodes;
                    List<FVariables> fvs = JsonConvert.DeserializeObject<List<FVariables>>(cv.getVariables(rbt.Id));
                    List<DVariables> dvs = new List<DVariables>();
                    dvs.AddRange(fvs.ConvertAll(x => new DVariables() { vlname = x.Name, vlvalue = x.Value, vlstatus = x.VStatus, vlscope = x.Scope, vltype = x.Type, RobotID = x.Robot_Id }));
                    if (dvs.Count > 0)
                    {
                        if (rbt.dynamicvariables != null)
                        {
                            rbt.dynamicvariables.AddRange(dvs);
                        }
                        else
                        {
                            rbt.dynamicvariables = new List<DVariables>();
                            rbt.dynamicvariables = dvs;
                        }
                    }
                }

                return JsonConvert.SerializeObject(rbts);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getRobots(string projectid, string robotid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("ROBOT", "Project_Id|Equals|" + projectid + "||Status|Equals|" + true + "||Id|Equals|" + robotid + "", "*", 0, "public", "", "");
                List<Robots> rbts = JsonConvert.DeserializeObject<List<Robots>>(ret);
                foreach (Robots rbt in rbts)
                {
                    List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(sc.getSteps(rbt.Id));
                    foreach (Steps step in steps)
                    {
                        List<StepProperties> props = JsonConvert.DeserializeObject<List<StepProperties>>(sc.getStepProperties(step.Id));
                        step.StepProperties = props;
                    }
                    List<LinkNodes> nodes = JsonConvert.DeserializeObject<List<LinkNodes>>(cl.getAllNodes(rbt.Id));
                    rbt.Steps = steps;
                    rbt.LinkNodes = nodes;
                    List<FVariables> fvs = JsonConvert.DeserializeObject<List<FVariables>>(cv.getVariables(rbt.Id));
                    List<DVariables> dvs = new List<DVariables>();
                    dvs.AddRange(fvs.ConvertAll(x => new DVariables() { vlname = x.Name, vlvalue = x.Value, vlstatus = x.VStatus, vlscope = x.Scope, vltype = x.Type, RobotID = x.Robot_Id }));
                    if (dvs.Count > 0)
                    {
                        if (rbt.dynamicvariables != null)
                        {
                            rbt.dynamicvariables.AddRange(dvs);
                        }
                        else
                        {
                            rbt.dynamicvariables = new List<DVariables>();
                            rbt.dynamicvariables = dvs;
                        }
                    }
                }

                return JsonConvert.SerializeObject(rbts);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getRobotProperties(string rbtid, string versionid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("ROBOT", "Id|Equals|" + rbtid + "||Status|Equals|" + true + "", "*", 0, "public", "", "");
                List<Robots> rbts = JsonConvert.DeserializeObject<List<Robots>>(ret);
                string vquery = "select max(\"versionid\") from \"Versions\" where \"rid\" =" + rbtid + "";
                string vs = dbd.DbExecuteQuery(vquery, "createrobot");
                foreach (Robots rbt in rbts)
                {
                    string stpsList = new Step().getSteps(rbt.Id, vs);

                    List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(stpsList);

                    List<LinkNodes> lnodes = new List<LinkNodes>();

                    foreach (Steps stp in steps)
                    {
                        lnodes.AddRange(JsonConvert.DeserializeObject<List<LinkNodes>>(new LinkNode().getNodes(stp.Id)));
                    }

                    rbt.LinkNodes = lnodes;
                    rbt.Steps = steps;

                    List<FVariables> fvs = JsonConvert.DeserializeObject<List<FVariables>>(cv.getVariables(rbt.Id));
                    List<DVariables> dvs = new List<DVariables>();
                    dvs.AddRange(fvs.ConvertAll(x => new DVariables() { vlname = x.Name, vlvalue = x.Value, vlstatus = x.VStatus, vlscope = x.Scope, vltype = x.Type, RobotID = x.Robot_Id }));
                    if (dvs.Count > 0)
                    {
                        if (rbt.dynamicvariables != null)
                        {
                            rbt.dynamicvariables.AddRange(dvs);
                        }
                        else
                        {
                            rbt.dynamicvariables = new List<DVariables>();
                            rbt.dynamicvariables = dvs;
                        }
                    }
                }

                return JsonConvert.SerializeObject(rbts);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string RenameRobot(string oldname, string newname, string robotid)
        {
            string query = "update public.\"ROBOT\" SET \"Name\" =  '" + newname + "' where \"Name\" = '" + oldname + "' and \"Id\" = " + robotid + "";
            string ret = dbd.DbExecuteQuery(query, "update");
            return ret;
        }
        public string getRobotsByProjectId(string id)
        {
            string query = "select  p.\"NAME\" as \"Project_Name\",p.\"ID\" as \"Project_Id\", r. * from public.\"ROBOT\" r right join  public.\"PROJECT\" p on  r.\"Project_Id\"= p.\"ID\"  and r.\"Status\"=true and p.\"Status\"=true where p.\"ID\" ='" + id + "'";
            string ret = dbd.DbExecuteQuery(query, "select");
            return ret;
        }
        public string getRobotsStepsStepProps(string id)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Step cs = new Step();
                string stpsList = cs.getSteps(id);
                List<Steps> stps = JsonConvert.DeserializeObject<List<Steps>>(stpsList);
                foreach (Steps stp in stps)
                {
                    string prpList = cs.getStepProperties(stp.Id);
                    List<StepProperties> prps = JsonConvert.DeserializeObject<List<StepProperties>>(prpList);
                    stp.StepProperties = prps;
                }
                return JsonConvert.SerializeObject(stps);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getRobotsStepsStepProps(string rid, string vid)
        {
            List<Robots> rbts = JsonConvert.DeserializeObject<List<Robots>>(getRobot(rid, ""));
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                foreach (Robots rbt in rbts)
                {
                    Step cs = new Step();
                    string stpsList = cs.getSteps(rbt.Id, vid);
                    List<Steps> stps = JsonConvert.DeserializeObject<List<Steps>>(stpsList);
                    rbt.Steps = stps;
                    List<LinkNodes> lnodes = new List<LinkNodes>();
                    foreach (Steps stp in stps)
                    {
                        lnodes.AddRange(JsonConvert.DeserializeObject<List<LinkNodes>>(new LinkNode().getNodes(stp.Id)));
                    }
                    rbt.LinkNodes = lnodes;
                    List<FVariables> fvs = JsonConvert.DeserializeObject<List<FVariables>>(cv.getVariables(rbt.Id));
                    List<DVariables> dvs = new List<DVariables>();
                    dvs.AddRange(fvs.ConvertAll(x => new DVariables() { vlname = x.Name, vlvalue = x.Value, vlstatus = x.VStatus, vlscope = x.Scope, vltype = x.Type, RobotID = x.Robot_Id }));
                    if (dvs.Count > 0)
                    {
                        if (rbt.dynamicvariables != null)
                        {
                            rbt.dynamicvariables.AddRange(dvs);
                        }
                        else
                        {
                            rbt.dynamicvariables = new List<DVariables>();
                            rbt.dynamicvariables = dvs;
                        }
                    }
                }

                return JsonConvert.SerializeObject(rbts);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getRobotsStepsStepProps()
        {
            string robots = getallRobots();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<Robots> robtsList = JsonConvert.DeserializeObject<List<Robots>>(robots);
            try
            {
                foreach (Robots robt in robtsList)
                {
                    Step cs = new Step();
                    string stpsList = cs.getSteps(robt.Id);
                    List<Steps> stps = JsonConvert.DeserializeObject<List<Steps>>(stpsList);
                    foreach (Steps stp in stps)
                    {
                        string prpList = cs.getStepProperties(stp.Id);
                        List<StepProperties> prps = JsonConvert.DeserializeObject<List<StepProperties>>(prpList);
                        stp.StepProperties = prps;
                    }
                    robt.Steps = stps;
                }
                return JsonConvert.SerializeObject(robtsList);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string cPredefinedBot(string projectid, string robotname, string predefinedbotid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<Dictionary<string, string>> Steps = new List<Dictionary<string, string>>();
                List<List<Dictionary<string, string>>> Actions = new List<List<Dictionary<string, string>>>();

                List<Dictionary<string, string>> pActions = new List<Dictionary<string, string>>();
                string query = "select * from public.\"PreDefinedRobot\" where \"ID\" = '" + predefinedbotid + "' and \"Status\" = true";
                string ret = dbd.DbExecuteQuery(query, "select");
                List<Dictionary<string, string>> PredefinedBots = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(ret);
                int order = 0;
                string versionId = "";
                foreach (Dictionary<string, string> pbot in PredefinedBots)
                {
                    pbot["Id2"] = pbot["ID"];
                    pbot["Name1"] = robotname;
                    pbot["Project_Id"] = projectid;
                    ROBOT bot = new ROBOT(pbot);

                    string query1 = "insert into public.\"ROBOT\" values (default,'" + robotname + "','" + pbot["Description"] + "','" + pbot["RobotType"] + "','" + projectid + "','WEB','" + date + " " + time + "','WEB','" + date + " " + time + "',true)";
                    string ret1 = dbd.DbExecuteQuery(query1, "insert");
                    string query2 = "select max(\"Id\") from public.\"ROBOT\" where \"Status\" = true";
                    string ret2 = dbd.DbExecuteQuery(query2, "select");
                    List<Dictionary<string, string>> botId = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(ret2);
                    string query3 = "select * from public.\"PredefinedActions\" where \"PredefinedBot_ID\" = '" + predefinedbotid + "' and \"Status\" = true";
                    string ret3 = dbd.DbExecuteQuery(query3, "select");
                    string versionquery = "select  max(\"versionid\") from public.\"Versions\" where \"rid\"=" + botId[0]["max"] + "";
                    string versionret = dbd.DbExecuteQuery(versionquery, "select");
                    List<Dictionary<string, string>> verId = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(versionret);
                    if (verId[0]["max"] == null)
                    {
                        versionId = "1";
                        string versioninsertion = "INSERT INTO public.\"Versions\" (rid, versionid, rflag,\"Status\" ) VALUES('" + botId[0]["max"] + "','1','1','true');";
                        string versioninsert = dbd.DbExecuteQuery(versioninsertion, "select");
                    }
                    else
                    {
                        versionId = verId[0]["max"];
                    }
                    pActions = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(ret3);
                    foreach (Dictionary<string, string> pAction in pActions)
                    {
                        order++;
                        Dictionary<string, string> Step = new Dictionary<string, string>();
                        string query4 = "SELECT * FROM public.\"ACTION\" where \"Id\"='" + pAction["Action_ID"] + "' and \"Status\"=true order by \"Id\"";
                        string ret4 = dbd.DbExecuteQuery(query4, "select");
                        List<Dictionary<string, string>> Action = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(ret4);
                        Actions.Add(Action);
                        string query5 = "insert into public.\"STEPS\" values (default,'" + Action[0]["Name"] + "','" + botId[0]["max"] + "','" + Action[0]["Element_Id"] + "','" + Action[0]["Id"] + "',5,'kk','" + date + " " + time + "','mm','" + date + " " + time + "',true,'" + order + "', '" + Action[0]["RuntimeUserInput"] + "','" + versionId + "')";
                        string ret5 = dbd.DbExecuteQuery(query5, "insert");
                        string query6 = "select max(\"Id\") from public.\"STEPS\" where \"Status\" = true";
                        string ret6 = dbd.DbExecuteQuery(query6, "select");
                        List<Dictionary<string, string>> maxStepId = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(ret6);
                        Step["ActionId"] = Action[0]["Id"];
                        Step["StepId"] = maxStepId[0]["max"];
                        Steps.Add(Step);
                        string query7 = "SELECT * FROM public.\"ActionProperties\" where (\"Action_Id\"='" + pAction["Action_ID"] + "' or \"Action_Id\"='777') and \"Status\"=true order by \"Id\"";
                        string ret7 = dbd.DbExecuteQuery(query7, "select");
                        List<Dictionary<string, string>> ActionProperties = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(ret7);
                        foreach (Dictionary<string, string> ActionProperty in ActionProperties)
                        {
                            string StepPropertyValue = "";
                            if (ActionProperty["Name"] == "Name")
                            {
                                StepPropertyValue = Action[0]["Name"];
                            }
                            else if (ActionProperty["Name"] == "Position-X")
                            {
                                StepPropertyValue = pAction["Location"].Split(' ')[0];
                            }
                            else if (ActionProperty["Name"] == "Position-Y")
                            {
                                StepPropertyValue = pAction["Location"].Split(' ')[1];
                            }
                            string query8 = "insert into public.\"STEPPROPERTIES\" values (default,'" + maxStepId[0]["max"] + "','" + ActionProperty["Name"] + "','" + StepPropertyValue + "','" + ActionProperty["PropertyType"] + "','" + projectid + "','k','" + date + " " + time + "','mm','" + date + " " + time + "',true," + ActionProperty["Order"] + ")";
                            string ret8 = dbd.DbExecuteQuery(query8, "insert");
                        }

                    }
                    for (int i = 0; i < Steps.Count; i++)
                    {
                        string ChildlStepId = "";
                        foreach (string actionId in pActions[i]["ChildActions"].Split(','))
                        {
                            if (actionId != "")
                            {
                                int index = pActions.FindIndex(p => p["ID"] == actionId);
                                if (index != -1)
                                    ChildlStepId += Steps[index]["StepId"] + "`";
                                else
                                    ChildlStepId += actionId;
                            }
                        }
                        string query9 = "insert into public.\"LinkNodes\" values (default,'" + Steps[i]["StepId"] + "','" + pActions[i]["Location"] + "','" + ChildlStepId + "','k','" + date + " " + time + "','mm','" + date + " " + time + "',true)";
                        string ret9 = dbd.DbExecuteQuery(query9, "insert");
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string cProcessManager(string username, string robotid, string userid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret1 = "false";
                string getrname = "Select \"Name\" from \"ROBOT\" where \"Id\"=" + robotid;
                string getRobot = new Connection().executeSQL(getrname, "Select");
                List<RobotDetails> rName = JsonConvert.DeserializeObject<List<RobotDetails>>(getRobot);
                string query = "insert into public.\"ProcessManager\" values (default,null,'" + robotid + "','"+ rName[0].Name + "','" + username + "','New','','" + date + " " + time + "','','" + username + "','" + date + " " + time + "','" + username + "','" + date + " " + time + "',true)";
                string ret = dbd.DbExecuteQuery(query, "insert");
                if (ret != "false")
                {
                    string query1 = "select max(\"ID\") from public.\"ProcessManager\" where \"Status\" = true";
                    ret1 = dbd.DbExecuteQuery(query1, "select");
                }
                return ret1;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getWFRobots(string projectid)
        {
            string query = "select * from public.\"ROBOT\" r where r.\"Project_Id\" = " + projectid + "and r.\"Status\" = true";
            return dbd.DbExecuteQuery(query, "select");
        }
        public string createNotification(string robotname, string user, string status)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string values, ret;
                if (status == StringHelper.success)
                {
                    values = robotname + " is Executed Successfully" + ",Normal," + user + "," + date + " " + time + "," + user + "," + date + " " + time + "," + "true" + "";
                    ret = new QueryUtil().insertTableData("public", "Notification", values, "", false);
                }
                else
                {
                    values = robotname + " Execution Failed" + ",Critical," + user + "," + date + " " + time + "," + user + "," + date + " " + time + "," + "true" + "";
                    ret = new QueryUtil().insertTableData("public", "Notification", values, "", false);
                }
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getRobotSchedule(string robotid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "SELECT count(*) FROM public.\"Scheduler\" where \"RobotId\"='" + robotid + "' ";
                string ret = dbd.DbExecuteQuery(query, "select");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string setInputInterval(string robotid, string interal, string result)
        {
            string query = string.Empty;
            string ret = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (result.Contains("1"))
                {
                    query = "update public.\"Scheduler\" SET \"interval\" =" + interal + ",\"LastExecutionTime\" ='" + date + time + "' where \"RobotId\" = " + robotid + " ";
                    ret = dbd.DbExecuteQuery(query, "update");
                }
                else
                {
                    query = "insert into public.\"Scheduler\" values (default," + robotid + "," + interal + ",'" + date + time + "','postgres','" + date + time + "','postgres','" + date + time + "',true)";
                    ret = dbd.DbExecuteQuery(query, "insert");
                }
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getSchedulerInerval()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "select \"interval\" from public.\"Scheduler\" order by \"Id\" desc limit 1 ";
                string interval = dbd.DbExecuteQuery(query, "selectwithnodes");
                return interval;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string Executerobo(Entities inputString)
        {

            Result rs = new Result();

            Variables.robotid = inputString.ID;
            if (inputString.dynamicvariables != null)
            {
                Variables.dynamicvariables.Clear();
                Variables.dynamicvariables.AddRange(inputString.dynamicvariables.ConvertAll(x => new DVariables() { vlname = x.vlname, vlvalue = x.vlvalue, vlstatus = x.vlstatus }));

            }
            if (inputString.uploadFiles != null && inputString.uploadFiles.Count > 0)
            {
                Variables.uploads = inputString.uploadFiles;
            }

            if (inputString.Type == StringHelper.execution)
            {
                string projectid = inputString.Project_Id;
                string robotid = inputString.ID;

                new ExecutionLog().deleteFromLogs(robotid);

                Variables.vDebugger.ID = inputString.Step_Id;

                string exeid = new StringFunction().getRandomString("32");

                int ExecResult = ape.startRobot(int.Parse(robotid), 0, exeid);

                string state = Variables.state.ToString();

                rs.SuccessState = state;
                if ((state.ToLower() != StringHelper.sucess) && (state.ToLower() != StringHelper.operationfailed) && (state.ToLower() != StringHelper.nosteps))
                {
                    if (rs.SuccessState.ToLower().IndexOf("inprogress") > -1)
                    {
                        RuntimeUserInputRequest ruir = new RuntimeUserInputRequest();
                        ruir.dvariables = Variables.dynamicvariables.FindAll(v => v.ExecutionID == exeid);
                        ruir.nextStepID = new AppExecution().getNextStep(int.Parse(robotid), int.Parse(Variables.thisStep.Id), string.Empty).ToString();
                        ruir.step = Variables.thisStep;
                        ruir.thisVariableName = Variables.thisVariable;

                        rs.runtimerequest = ruir;
                        rs.elog = new List<ELogs>();
                    }
                    else
                    {
                        rs.elog = JsonConvert.DeserializeObject<List<ELogs>>(new ExecutionLog().getFromLogs(robotid, true));
                        rs.Downloads = DownloadFiles(projectid, robotid);
                    }
                }
                else
                {
                    //rs.elog = JsonConvert.DeserializeObject<List<ELogs>>(new cExecutionLog().getFromLogs(robotid, true));
                    rs.SuccessState = state;
                    rs.Downloads = DownloadFiles(projectid, robotid);
                }

                rs.RobotId = Convert.ToInt32(robotid);
                rs.vdebugger = Variables.vDebugger;
                string res = JsonConvert.SerializeObject(rs);

                responseResult.ResultString = res;
                return JsonConvert.SerializeObject(responseResult);
            }
            return JsonConvert.SerializeObject(responseResult);
        }
        private List<DownloadFilesObject> DownloadFiles(string projectid, string robotid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<DownloadFilesObject> returner = new List<DownloadFilesObject>();
            try
            {
                string vs = ape.getVersion(robotid);
                List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(cs.getSteps(robotid, vs));

                List<Steps> blackone = steps.FindAll(stp => stp.Action_Id == "215");

                if (blackone.Count > 0)
                {
                    if (Variables.downloads.Count > 0)
                    {
                        foreach (var download in Variables.downloads)
                        {
                            DownloadFilesObject dfo = new DownloadFilesObject();
                            dfo.FileName = download;
                            try
                            {
                                byte[] ress = System.IO.File.ReadAllBytes(download);
                                dfo.FileData = ress;
                            }
                            catch (Exception ex)
                            {
                                string ec = ex.Message;
                            }
                            returner.Add(dfo);
                        }
                    }
                }
                else
                {

                }

                return returner;
            }
            catch (Exception ex)

            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return null;
            }
        }
        public string getProjectSystem(string ipaddress)
        {
            string query = "select sh.\"Id\",s.\"ID\" as SystemId,s.\"Name\" as SystemName,\"RobotId\",\"interval\",\"LastExecutionTime\",\"Project_Id\",r.\"Name\" as \"RobotName\",p.\"NAME\" as \"ProjectName\" from \"Scheduler\" sh inner join \"Systems\" s on s.\"ID\"=sh.\"SystemId\"::int inner join \"ROBOT\" r on r.\"Id\"=sh.\"RobotId\" inner join \"PROJECT\" p on p.\"ID\"=r.\"Project_Id\" where s.\"IP_Address\"='" + ipaddress + "' and r.\"Status\"=true and s.\"Status\"=true and sh.\"Status\"=true and p.\"Status\"=true";
            string schedule = dbd.DbExecuteQuery(query, "select");
            return schedule;
        }
        public string getCurrentTimestamp(string interval, string RobotId, string SystemId)
        {
            string selectquery = "select CURRENT_TIMESTAMP-max(\"StartingTime\"::TimeStamp)>(" + interval + " ||' minutes')::interval as timestatus  from \"ProcessManager\" where \"RobotId\"='" + RobotId + "' and \"SystemId\"='" + SystemId + "' and \"Status\"=true";
            string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
            return lastExecutionResult;
        }
        public string getSchedule(string ipaddress)
        {
            string query = "select sh.\"Id\",s.\"ID\" as SystemId,s.\"Name\" as SystemName,\"RobotId\",\"interval\",\"LastExecutionTime\" from \"Scheduler\" sh inner join \"Systems\" s on s.\"ID\"=sh.\"SystemId\"::int where s.\"IP_Address\"='" + ipaddress + "'";
            string schedule = dbd.DbExecuteQuery(query, "select");
            return schedule;
        }

    }
    public class RobotDetails
    {
        public string Name { get; set; }
    }
}