﻿using Genbase.BLL;
using Genbase.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
//using static Genbase.Classes.Handler;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.Classes
{
    public class Action
    {

        DAL.PostgresConnect dbd = new DAL.PostgresConnect();
        public string getActions(string type)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = string.Empty;
                if (type == "('Robot','Workflow')")
                {
                    string query = "select* from \"ACTION\" where \"Type\" in ('Robot','Workflow') and \"Status\" = true ORDER BY \"Order\"";

                    ret = dbd.DbExecuteQuery(query, "select");
                }
                else
                {
                    ret = new QueryUtil().getTableData("ACTION", "Status|Equals|true||Type|Equals|" + type + "", "\"Id\", \"Name\", \"Element_Id\", \"Icon\", \"Description\", \"Type\",\"RuntimeUserInput\",\"Dependency\"", 0, "public", "\"Id\"", "");
                }
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }

        }
        public string getActions()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("ACTION", "Status|Equals|true", "\"Id\", \"Name\", \"Element_Id\", \"Icon\", \"Description\", \"Type\", \"RuntimeUserInput\"", 0, "public", "\"Id\"", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getProperties()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret1 = new QueryUtil().getTableData("ActionProperties", "Status|Equals|true", "\"Id\", \"Name\", \"Description\", \"Action_Id\", \"PropertyType\", \"Order\",\"CustomValidation\",\"DefaultValue\"", 0, "public", "\"Order\"", "");
                return ret1;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getActionProperties()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret1 = new QueryUtil().getTableData("ELEMENT", "Status|Equals|true", "*", 0, "public", "", "");
                List<Elements> elements = JsonConvert.DeserializeObject<List<Elements>>(ret1);
                foreach (Elements element in elements)
                {
                    string ret2 = new QueryUtil().getTableData("ACTION", "Status|Equals|true||Element_Id|Equals|" + element.Id + "", "*", 0, "public", "", "");
                    List<Actions> actions = JsonConvert.DeserializeObject<List<Actions>>(ret2);
                    string ret4 = new QueryUtil().getTableData("ActionProperties", "Status|Equals|true||Action_Id|Equals|777", "*", 0, "public", "", "");
                    List<ElementProperties> commonproperties = JsonConvert.DeserializeObject<List<ElementProperties>>(ret4);

                    foreach (Actions action in actions)
                    {
                        string ret3 = new QueryUtil().getTableData("ActionProperties", "Status|Equals|true||Action_Id|Equals|" + action.Id + "", "*", 0, "public", "", "");
                        List<ElementProperties> properties = JsonConvert.DeserializeObject<List<ElementProperties>>(ret3);

                        action.elementProperties = commonproperties.Union(properties).OrderBy(x => x.Id).ToList();
                    }
                    element.action = actions;
                }
                Entities e = new Entities();
                e.elements = elements;

                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(e);
                return jsonString;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string getAction(string actionid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("ACTION", "Id|Equals|" + actionid + "", "\"Id\", \"Name\", \"Element_Id\", \"Icon\", \"Description\", \"Type\", \"RuntimeUserInput\"", 0, "public", "", "");
                List<Actions> actions = JsonConvert.DeserializeObject<List<Actions>>(ret);

                string result = string.Empty;
                result = actions.SingleOrDefault()?.Name;
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getActionRow(string actionid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("ACTION", "Id|Equals|" + actionid + "", "\"Id\", \"Name\", \"Element_Id\", \"Icon\", \"Description\", \"Type\", \"RuntimeUserInput\"", 0, "public", "", "");
                string result = string.Empty;
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string getActionProperty(string actionid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("ActionProperties", "Status|Equals|true||Action_Id|Equals|" + actionid + "", "\"Id\", \"Name\", \"Description\", \"Action_Id\", \"PropertyType\", \"Order\"", 0, "public", "\"Order\"", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getValidations()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("ActionProperties", "Status|Equals|true", "\"Id\", \"Name\", \"Description\", \"Action_Id\", \"PropertyType\", \"Order\"", 0, "public", "\"Order\"", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getWAActionsActionProperties()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("ACTION", "Status|Equals|true||Element_Id|Equals|-1", "*", 0, "public", "", "");
                List<Actions> actions = JsonConvert.DeserializeObject<List<Actions>>(ret);

                string ret1 = new QueryUtil().getTableData("ACTION", "Status|Equals|true||Element_Id|Equals|1", "*", 0, "public", "", "");
                actions.AddRange(JsonConvert.DeserializeObject<List<Actions>>(ret1));

                actions.RemoveAll(act => act.Type.Trim().ToLower() == "valuestreammap");

                string ret4 = new QueryUtil().getTableData("ActionProperties", "Action_Id|Equals|777", "*", 0, "public", "", "");
                List<ElementProperties> commonproperties = JsonConvert.DeserializeObject<List<ElementProperties>>(ret4);

                foreach (Actions action in actions)
                {
                    string ret3 = new QueryUtil().getTableData("ActionProperties", "Status|Equals|true||Action_Id|Equals|" + action.Id + "", "*", 0, "public", "", "");
                    List<ElementProperties> properties = JsonConvert.DeserializeObject<List<ElementProperties>>(ret3);

                    action.elementProperties = commonproperties.Union(properties).OrderBy(x => x.Id).ToList();
                }
                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(actions);
                return jsonString;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }

        }

    }
}