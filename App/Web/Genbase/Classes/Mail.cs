﻿using Genbase.BLL;
using Genbase.Classes;
using Genbase.DAL;
using System;
using System.Collections.Generic;

namespace Genbase.Classes
{
    public class Mail
    {
        DAL.PostgresConnect dbd = new DAL.PostgresConnect();

        public string getMailItems(string subjekt)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("EmailTemplate", "Subject|Equals|" + subjekt + "", "\"ID\", \"Subject\", \"Body\", \"Type\", \"Active\"", 0, "public", "", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string GetAllMailItems()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("EmailTemplate", "Status|Equals|true", "\"ID\", \"Subject\", \"Body\", \"Type\", \"Active\"", 0, "public", "", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string GetMailTemplate(string ID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                //
                Connection _objCon = new Connection();
                string query1 = "select \"ID\", \"Subject\", \"Body\", \"Type\", \"Active\" from public.\"EmailTemplate\" where \"Status\"=true and \"ID\"=" + ID + " order by 1";
                string result1 = _objCon.executeSQL(query1, "Select");

                return result1;
                //string ret = new QueryUtil().getTableData("EmailTemplate", "Status|Equals|true", "\"ID\", \"Subject\", \"Body\", \"Type\", \"Active\"", 0, "public", "", "");
                //return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string GetGlobalConfiguration()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("Configuartion", "Status|Equals|true", "\"ID\", \"Name\", \"Value\"", 0, "public", "", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

    }
}