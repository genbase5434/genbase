﻿using System;
using Genbase.DAL;
using Genbase.BLL;
using System.Collections.Generic;
using Genbase.Classes;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.classes
{
    public class User
    {
        BLL.EncoderDecoderBase64 baser = new BLL.EncoderDecoderBase64();
        Connection c = Connection.GetInstance;
        public string getUser()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = new QueryUtil().getTableData("USER", "Status|Equals|true", "\"ID\",\"FirstName\",\"LastName\",\"Email_ID\",\"ContactNo\",\"CreateDatetime\"", 0, "public", "", "");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string updateUser(int UserID, string FirstName, string LastName, string EmailID, long ContactNo, string CreationDate, string LastLoginTime)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string query = "UPDATE \"User\" SET \"FirstName\" ='" + FirstName + "',\"LastName\" = '" + LastName + "',\"EmailID\" = '" + EmailID + "',\"ContactNo\" = " + ContactNo + ",\"CreationDate\" = '" + CreationDate + "',\"LastLoginTime\" = '" + LastLoginTime + "' WHERE \"UserID\" ='" + UserID + "'";
                string result = new QueryUtil().updateTableData("User", "UserID|Equals|" + UserID, "FirstName|Equals|" + FirstName + "||LastName|Equals|" + LastName + "||EmailID|Equals|" + EmailID + "||ContactNo|Equals|" + ContactNo + "||CreationDate|Equals|" + CreationDate + "||LastLoginTime|Equals|" + LastLoginTime, "public", false);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string addUser(string FirstName, string LastName, string EmailID, long ContactNo, string CreationDate, string LastLoginTime)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string values = FirstName + "," + LastName + "," + EmailID + "," + ContactNo.ToString() + "," + CreationDate + "," + LastLoginTime;
                string result = new QueryUtil().insertTableData("public", "User", values, "", false);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
    }
}