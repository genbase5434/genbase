﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ezBot.Classes
{
    public class StatusModel
    {
        //private string type;

        public StatusModel(string Code, string Message, List<KeyValuePair<string, string>> Method, string Status)
        {
            this.Code = Code;
            this.Message = Message;
            this.Method = Method;
            this.Status = Convert.ToBoolean(Status);
        }
        public StatusModel()
        {

        }
        public string Code { get; set; }
        public string Message { get; set; }
        public List<KeyValuePair<string, string>> Method { get; set; }
        public bool Status { get; set; }

    }
}
