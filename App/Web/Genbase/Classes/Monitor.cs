﻿using Genbase.BLL;
using Genbase.classes;
using Genbase.DAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Genbase.Classes
{
    public class Monitor
    {
        Connection c = Connection.GetInstance;
        public string AddRobot()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query, result;

                query = "SELECT \"Robot_Status\",COUNT(\"Robot_Status\")  as \"Value\" FROM \"ROBOT_LOG\" where (\"Robot_Status\" = 'Available' or \"Robot_Status\" = 'Unresponsive' or \"Robot_Status\" = 'Disconnected' or \"Robot_Status\" = 'BUSY' )GROUP BY \"Robot_Status\"";
                result = c.executeSQL(query, "Select");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string addRobotBarChart()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string query, result;
                query = "SELECT \"Robot_ID\",\"Name\" FROM \"ROBOT_LOG\" where \"Name\"='jan' OR \"Name\"='feb' OR \"Name\"='mar'  ";
                result = c.executeSQL(query, "Select");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getKpi(string tablename, int roleid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string query = "SELECT A.\"ID\",A.\"Name\", A.\"Class\", A.\"IconClass\", A.\"Value\",A.\"Status\",regexp_matches(A.\"Role\", '" + roleid + "')from public.\"" + tablename + "\" AS A  INNER JOIN public.\"KPI\" AS C ON A.\"Name\"=C.\"PortalName\";";
                string result = c.executeSQL(query, "Select");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string KPIScreen(int roleid, string monitor, string tablename, string KPIid)
        {
            string query = "";
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                // var c = Connection.GetInstance;
                if (KPIid == null || KPIid == "null")
                {
                    query = "SELECT distinct *,regexp_matches(\"Role_Id\", '" + roleid + "') FROM public.\"" + tablename + "\" where \"PortalName\" = '" + monitor + "' and \"Status\"=true order by \"ID\"";

                }
                else if (KPIid != null)
                {
                    query = "SELECT distinct *,regexp_matches(\"Role_Id\", '" + roleid + "') FROM public.\"" + tablename + "\" where \"PortalName\" = '" + monitor + "' and \"ID\"='" + KPIid + "' and \"Status\"=true order by \"ID\"";

                }
                string result = c.executeSQL(query, "Select");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string KPIScreen1(int roleid, string monitor, string tablename, string chartId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string query = "SELECT distinct *,regexp_matches(\"Role_Id\", '" + roleid + "') FROM public.\"" + tablename + "\" where \"PortalName\" = '" + monitor + "' and \"Chart_Type\" = 'timeseries' and \"Status\"=true and \"ID\"='" + chartId + "' order by \"ID\"";

                string result = c.executeSQL(query, "Select");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }


        public string KPIdbConnection(int roleid, string tablename)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string query = "select distinct dt.\"Name\",d.*,k.\"ID\" from \"DBConnection\" d join  \"" + tablename + "\" k on d.\"ID\"=k.\"DbConnection_Id\" join \"DBType\" dt on d.\"DBType\"=dt.\"ID\"  where k.\"Role_Id\"='" + roleid + "' and d.\"Status\"=true and dt.\"Status\"=true and k.\"Status\"=true order by k.\"ID\"";

                string result = c.executeSQL(query, "Select");

                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string monitorsettings(int roleid, int userid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string query = "SELECT \"ID\", \"PortalName\", \"Chart_Type\", \"SQL\", \"Row_Number\", \"Column_Number\", \"Link\", \"Height\", \"Width\", \"Role_Id\", \"Preferences\", \"CreateBy\", \"CreateDateTime\", \"UpdateBy\", \"UpdateDateTime\", \"Status\" FROM public.\"KPI\" WHERE  (\"PortalName\"=\'ASSETS\' or \"PortalName\"=\'QUEUES\'  or \"PortalName\"=\'PROCESSES\'  or \"PortalName\"=\'SCHEDULES\'  )and \"CreateBy\"='" + userid + "' and \"Role_Id\"='" + roleid + "';";

                string result = c.executeSQL(query, "Select");

                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }

        }

        public string dashboard(int roleid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                char[] outarray = { ',', ' ' };
                //string query = "SELECT \"ID\", href,\"Name\",\"Description\",\"Imgsrc\",\"color\", regexp_matches(\"Role\", '" + roleid + "') FROM public.\"Dashboard\" where \"Status\"='t' order by \"ID\";";
                string query1 = "Select \"ID\",\"Role\" from \"Dashboard\" WHERE \"Status\"=true order by \"ID\"";
                string all_data = c.executeSQL(query1, "Select");
                //List<DBConnection> ListValues = JsonConvert.DeserializeObject<List<DBConnection>>(all_data);
                List<Dictionary<string, string>> ListValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(all_data);
                string role_list = string.Empty;
                List<string> outputidlist = new List<string>();
                for (int i = 0; i < ListValues.Count; i++)
                {
                    if (ListValues[i].Keys.Last() == "Role")
                    {
                        String[] outputlist = ListValues[i].Values.Last().Split(outarray);

                        for (int k = 0; k < outputlist.Length; k++)
                        {
                            if (outputlist[k] == roleid.ToString())
                            {
                                var list = new List<string>(outputlist);
                                list.Remove(outputlist[k]);
                                outputidlist.Add(ListValues[i].Values.First());

                            }
                        }
                    }
                }
                role_list = String.Join(", ", outputidlist);
                string query = "SELECT \"ID\", href,\"Name\",\"Description\",\"Imgsrc\",\"color\", regexp_matches(\"Role\", '" + roleid + "') FROM public.\"Dashboard\" where \"ID\"in(" + role_list + ") and \"Status\"='t' order by \"ID\";";
                string result = c.executeSQL(query, "Select");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string dashboardUpdate(string Type, string roleid,string AddnewdashboardId, string dashboardId)
        {
            string result = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if(Type == "Add")
                {
                    AddnewdashboardId= Regex.Replace(AddnewdashboardId, @"[^0-9,]+", "");
                    dashboardId= Regex.Replace(dashboardId, @"[^0-9,]+", "");
                    char[] outarray = { ',',  ' '};
                    String[] AddValues = AddnewdashboardId.Split(outarray);
                    String[] DelValues = dashboardId.Split(outarray);

                    for(int i = 0; i < AddValues.Length; i++)
                    {
                        if (AddValues[i] != "")
                        {
                            string query1 = "Select \"Role\" from \"Dashboard\" WHERE \"ID\"=" + AddValues[i] + " and \"Status\"=true order by \"ID\"";
                            string data = c.executeSQL(query1, "Select");
                            List<Dictionary<string, string>> datavalue = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(data);
                            if (datavalue[0].Values.First() == "")
                            {
                                string query = "UPDATE \"Dashboard\" SET \"Role\"='" + roleid + "' WHERE \"ID\"='" + AddValues[i] + "'";
                                result = c.executeSQL(query, "Insert");
                            }
                            else
                            {
                                string query = "UPDATE \"Dashboard\" SET \"Role\"=\"Role\"||'," + roleid + "' WHERE \"ID\"='" + AddValues[i] + "'";
                                result = c.executeSQL(query, "Insert");
                            }
                        }
                    }

                    for(int j = 0; j < DelValues.Length; j++)
                    {
                        
                        string query1 = "Select \"Role\" from \"Dashboard\" WHERE \"ID\"='" + DelValues[j] + "' ";
                        string output = c.executeSQL(query1, "Select");
                        output = Regex.Replace(output, @"[^0-9,]+", "");
                        String[] outputlist = output.Split(outarray);
                        for (int k = 0; k < outputlist.Length; k++)
                        {
                            if(outputlist[k]== roleid)
                            {
                                var list = new List<string>(outputlist);
                                list.Remove(outputlist[k]);
                                outputlist = list.ToArray();
                            }
                        }
                        output = String.Join(",", outputlist);
                        string query2 = "UPDATE \"Dashboard\" SET \"Role\"='" + output + "' WHERE \"ID\"='" + DelValues[j] + "'";
                        result=c.executeSQL(query2, "Insert");

                    }
                }
                    return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string updatechartsdata(string values, string ID)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                JObject Values = JObject.Parse(values);
                Dictionary<string, string> dictValues = Values.ToObject<Dictionary<string, string>>();
                IList<string> CNames = dictValues.Keys.ToArray();
                JObject Id = JObject.Parse(ID);
                Dictionary<string, string> dictID = Id.ToObject<Dictionary<string, string>>();
                IList<string> IDName = dictID.Keys.ToArray();


                string query = "Update public.\"KPI\"  SET ";

                for (var i = 0; i < (dictValues.Count); i++)
                {
                    if (i != (dictValues.Count - 1))
                    {
                        if (dictValues[CNames[i]].ToString() != "")
                            query += "\"" + CNames[i].ToString() + "\"='" + dictValues[CNames[i]].ToString() + "',";
                        else
                            query += "\"" + CNames[i].ToString() + "\"=null, ";
                    }
                    else
                    {
                        if (dictValues[CNames[i]].ToString() != "")
                            query += "\"" + CNames[i].ToString() + "\"='" + dictValues[CNames[i]].ToString() + "'";
                        else
                            query += "\"" + CNames[i].ToString() + "\"=null ";
                    }

                }
                query += "where \"" + IDName[0] + "\"='" + dictID[IDName[0]] + "'";

                string result = c.executeSQL(query, "Insert");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        public string cMonitor(string screenName, string role, string CreateBy, string UpdateBy)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string line;

                var sb = new StringBuilder();

                StreamReader file = new StreamReader(AppDomain.CurrentDomain.BaseDirectory.ToString() + "Views\\Temp\\MTemplate.html");
                while ((line = file.ReadLine()) != null)
                    sb.AppendLine(line.Substring(0, Math.Min(1000, line.Length)));
                file.Close();
                sb.Replace("{MonitorTitle}", screenName);
                sb.Replace("{JsFilename}", screenName.Replace(" ", "_") + "_" + role + ".js");
                var path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"Views\generatedMonitors\html\" + screenName.Replace(" ", "_") + "_" + role + ".html";
                FileStream fs = new FileStream(path, FileMode.Create);
                fs.Close();
                using (StreamWriter files = new StreamWriter(path))
                {
                    files.WriteLine(sb.ToString());
                }
                var jsSb = new StringBuilder();
                StreamReader jsfile = new StreamReader(AppDomain.CurrentDomain.BaseDirectory.ToString() + "Views\\Temp\\MTemplate.js");
                while ((line = jsfile.ReadLine()) != null)
                    jsSb.AppendLine(line.Substring(0, Math.Min(1000, line.Length)));
                jsfile.Close();
                var jspath = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"Views\generatedMonitors\js\" + screenName.Replace(" ", "_") + "_" + role + ".js";
                FileStream fsJs = new FileStream(jspath, FileMode.Create);
                fsJs.Close();
                using (StreamWriter files = new StreamWriter(jspath))
                {
                    files.WriteLine(jsSb.ToString());
                }
                Menu cm = new Menu();
                int m_id = cm.addMenu(screenName, 0, "/Views/Layout.html?generatedMonitors/html/" + screenName.Replace(" ", "_") + "_" + role, "img-icon-monitor|/Views/dist/img/icons/dboard/monitor.png|fas fa-chart-area", 0, CreateBy, UpdateBy);
                Roles cr = new Roles();
                List<string> Roles = new List<string>();
                Roles.Add(role);

                string rresult = cr.addRoles(m_id, role, CreateBy, UpdateBy);
                return StringHelper.truee;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string QueryValidate(string DBInstance, string Query)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            bool ConnectionResult;
            string FinalResult = string.Empty;
            try
            {
                cSQLScripts cs = new cSQLScripts();
                string result = new QueryUtil().getTableData("DBConnection", "ID|Equals|" + DBInstance + "||Status|Equals|true", "*", 0, "public", "", "");
                if (result != null)
                {
                    //List<Dictionary<string, string>> DBValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(result);
                    List<KPIConnection> KPIdbConnectionList = JsonConvert.DeserializeObject<List<KPIConnection>>(result);
                    foreach (KPIConnection scd in KPIdbConnectionList)
                    {
                        Connection v = new Connection(scd.Server, scd.Port, scd.Database, scd.Username, scd.Password);
                        ConnectionResult = v.openDB();
                        if (ConnectionResult == true)
                        {
                            string finalQuery = cs.Base64Decode(Query);
                            var Charrr = finalQuery.ToCharArray().ToList();
                            if (Charrr.FindAll(c => c.ToString() == "#").Count() > 0)
                            {
                                string minDate = DateTime.MinValue.ToString("yyyy-MM-dd");
                                string maxDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                                finalQuery = finalQuery.Replace("#minDate#", "'" + minDate + "'");
                                finalQuery = finalQuery.Replace("#maxDate#", "'" + maxDate + "'");
                                finalQuery = finalQuery.Replace("#timeseriestype#", "'week'");
                                FinalResult = v.executeSQL(finalQuery, "Select");
                            }
                            else
                            {
                                FinalResult = v.executeSQL(finalQuery, "Select");
                            }
                        }
                        else
                        {
                            FinalResult = "Connection Failed";
                        }
                    }
                }
                return FinalResult;
            }
            catch (Exception ex)

            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
    }
}
