﻿using Genbase.BLL;
using Genbase.DAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.Classes
{
    public class Menu
    {
        Connection c = Connection.GetInstance;
        BLL.EncoderDecoderBase64 baser = new BLL.EncoderDecoderBase64();
        public string getMenu(string tablename)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string query = "Select \"ID\" as \"id\",\"Name\",nullif(\"Parent_ID\",'0') as \"parentId\",\"Path\",\"Class\",\"Type\",\"LOB\" from \"" + tablename + "\" where \"Status\"=true order by \"ID\" ASC";
                string result = c.executeSQL(query, "Select");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getMenuRole(int RoleId, string LOB)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = string.Empty;
                //if (RoleId != 1 && RoleId != 4)
                //{

                //    string query = "Select DISTINCT A.\"ID\" as \"id\",A.\"Name\",nullif(A.\"Parent_ID\",'0') as \"parentId\",A.\"Path\",A.\"Class\",A.\"Order\",A.\"ScreenId\" FROM public.\"MENU\" as A  INNER JOIN public.\"Role_Menu_Relation\" as B ON A.\"ID\" =B.\"Menu_ID\" AND B.\"Role_ID\"=" + RoleId + " where A.\"LOB\" in( " + LOB + ") and B.\"Status\"='1' and A.\"Status\"='1' order by A.\"Order\",A.\"ID\" ASC";
                //    result = c.executeSQL(query, "Select");
                //}
                //else
                //{

                    string query = "Select DISTINCT A.\"ID\" as \"id\",A.\"Name\",nullif(A.\"Parent_ID\",'0') as \"parentId\",A.\"Path\",A.\"Class\",A.\"Order\",A.\"ScreenId\" FROM public.\"MENU\" as A  INNER JOIN public.\"Role_Menu_Relation\" as B ON A.\"ID\" =B.\"Menu_ID\" AND B.\"Role_ID\"='" + RoleId + "' and B.\"Status\"='1' and A.\"Status\"='1' order by A.\"Order\",A.\"ID\" ASC";
                    result = c.executeSQL(query, "Select");
                //}
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }

        //Role_Menu_Relation
        public string getMenuRoleId(int RoleId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = string.Empty;

                Connection c = new Connection();
                string query = "SELECT distinct \"Menu_ID\" from \"Role_Menu_Relation\" Where \"Role_ID\" = '" + RoleId + "'  and \"Status\"=true Order by \"Menu_ID\"";
                result = c.executeSQL(query, "Select");

                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }

        public int addMenu(string Name, int Parent_id, string Path, string class1, int screenId, string CreateBy, string UpdateBy)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string time = DateTime.Now.ToShortTimeString();
                string date = DateTime.Now.ToShortDateString();
                Boolean status = true;
                string values = Name + "," + Parent_id + "," + Path + "," + class1 + ",null," + screenId + ",null," + CreateBy + "," + date + "" + time + "," + UpdateBy + "," + date + "" + time + "," + status + ",1,1";
                string result = new QueryUtil().insertTableData("public", "MENU", values, "", false);
                string query1 = "select max(\"ID\") from public.\"MENU\"";
                string result1 = c.executeSQL(query1, "Select");
                JArray obj = JArray.Parse(result1);
                string x = obj[0]["max"].ToString();
                return Convert.ToInt16(x);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return 1;
            }

        }
        public string updateMenu(int screenId, string Name, string Path, string class1)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string query = "UPDATE public.\"MENU\" SET \"Name\"='" + Name + "',\"Path\"='" + Path + "',\"Class\"='" + class1 + "',\"Status\"='1' where \"ScreenId\"='" + screenId + "' returning \"ID\"";
                string result = c.executeSQL(query, "Select");
                if (result != "[]")
                {
                    List<Dictionary<string, string>> id = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(result);
                    return id[0][StringHelper.id];
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string updateMenuParentID(int id, string Name, int Parent_id, string Path, string class1)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string result = string.Empty;
                if (id != Parent_id)
                {
                    result = new QueryUtil().updateTableData("MENU", "ID|Equals|" + baser.Base64Encode(id.ToString()), "Name|Equals|" + baser.Base64Encode(Name) + "||Parent_ID|Equals|" + baser.Base64Encode(Parent_id.ToString()) + "||Path|Equals|" + baser.Base64Encode(Path) + "||Class|Equals|" + baser.Base64Encode(class1) + "||Status|Equals|" + baser.Base64Encode("true"), "public", true);
                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string deleteMenuScreenId(string tableName, string deleteId)
        {
            string result = "";
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (tableName == StringHelper.Menu)
                    result = new QueryUtil().updateTableData("MENU", "ID|Equals|" + deleteId, "Status|Equals|false", "public", false);
                else if (tableName == StringHelper.screentable)
                {
                    result = new QueryUtil().updateTableData("ScreenTable", "Screen_ID|Equals|" + deleteId, "Status|Equals|false", "public", false);
                    string result1 = new QueryUtil().updateTableData("MENU", "ScreenId|Equals|" + deleteId, "Status|Equals|false", "public", false);
                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
    }
}
