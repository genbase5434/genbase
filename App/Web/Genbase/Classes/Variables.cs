﻿//using ezBot.BLL.ElementsClasses;
using ezBot.classes;
using ezBot.Classes;
using MimeKit;
using MySql.Data.MySqlClient;
//using MimeKit;
//using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using Tweetinvi.Logic.DTO;
//using Tweetinvi.Logic.DTO;
using static ezBot.Classes.Handler;

namespace ezBot.BLL
{
    public class Variables
    {
        public class Posts
        {
            public string PostId { get; set; }
            public string PostStory { get; set; }
            public string PostMessage { get; set; }
            public string PostPicture { get; set; }
            public string UserId { get; set; }
            public string UserName { get; set; }
        }
        //to maintain db connection type
        public static string dbConnectionType = string.Empty;
        public static string state = string.Empty;
        //program output
        public static string output = string.Empty;
        //status to continue step
        public static bool exeStatus = false;
        public static bool InStatus = false;

        public static List<DVariables> dynamicvariables = new List<DVariables>();
        //input
        public static string Variable = string.Empty;
        public static string Title = string.Empty;
        public static string Displaymessage = string.Empty;
        public static string Buttons = string.Empty;
        public static int rtype = 0;
        //output
        public static string Messageformat = string.Empty;

        //mail login
        public static string username = string.Empty;
        public static string password = string.Empty;
        public static string port = string.Empty;
        public static string mailserver = string.Empty;

        //Project_ID
        public static string Project_Id = string.Empty;

        //readmail output
        public static List<MimeMessage> mails = new List<MimeMessage>();

        //sendmail Template Global Config
        public static string global_config = string.Empty;
        //match(readmail) output
        public static List<MimeMessage> downloadattachments = new List<MimeMessage>();

        //download attachments output
        public static List<string> attaches = new List<string>();

        //db connection output
        public static string connString = string.Empty;
        public static string unamedb = string.Empty;
        public static string pwddb = string.Empty;
        public static string urldb = string.Empty;
        public static string portdb = string.Empty;
        public static string dbname = string.Empty;
        public static string datatbname = string.Empty;
        public static MySqlConnectionStringBuilder conn_string = new MySqlConnectionStringBuilder();

        //db input 
        public static string dbtype = string.Empty;

        //copyfrom excel output
        public static DataTable exceloutput = new DataTable();
        //excel opps
        public static Microsoft.Office.Interop.Excel.Workbook xlBook = null;
        public static Microsoft.Office.Interop.Excel.Application xlApp = null;
        public static Microsoft.Office.Interop.Excel.Sheets xlsheet = null;
        public static Microsoft.Office.Interop.Excel.Chart xlchart = null;
        public static Microsoft.Office.Interop.Excel.Range chartRange = null;
        public static Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet = null;

        //read file opps
        //public static Microsoft.Office.Interop.Word.Application wapp = null;
        //public static Microsoft.Office.Interop.Word.Document wdoc = null;


        public static List<string> lstwf = new List<string>();
        public static List<string> filesList = new List<string>();
        public static string rowsoutput = string.Empty;
        public static DataTable dboutput = new DataTable();
        public static string namexl = string.Empty;

        //read files output
        public static List<string> readOutput = new List<string>();

        //match(files) output
        public static List<string> readOutputupdate = new List<string>();

        //errors log temp @ runtime
        public static List<string> errorsList = new List<string>();
        public static List<ELogs> elogs = new List<ELogs>();


        //logging events
        public static string robotid = string.Empty;
        public static string jobid = string.Empty;


        //events log temp @ runtime
        public static List<string> eventsLog = new List<string>();


        //espocrm/Service Authenticator output
        public static string usernamepassword = string.Empty;

        //Twitter output
        public static List<TweetDTO> isSocial = new List<TweetDTO>();

        ////Facebook output
        //--- public static List<Posts> fbOutput = new List<Posts>();

        ////match(twitter) ouput
        public static List<TweetDTO> isSocialUpate = new List<TweetDTO>();

        ////match(facebook) ouput
       public static List<Posts> fbOutputUpdate = new List<Posts>();

        //ticketdata input espoCRM
        //--- public static List<TicketData> ticketdata = new List<TicketData>();
        public static List<Posts> fbOutput = new List<Posts>();

        //Classified Data //group,message,sender
        public static List<KeyValuePair<string, KeyValuePair<string, string>>> classifiedData = new List<KeyValuePair<string, KeyValuePair<string, string>>>();

        public static List<dynamic> tweetData = new List<dynamic>();

        //SSH 1
        public static string ipname1 = string.Empty;
        public static string port1 = string.Empty;
        public static string username1 = string.Empty;
        public static string password1 = string.Empty;

        //SSH 2
        public static string ipname2 = string.Empty;
        public static string port2 = string.Empty;
        public static string username2 = string.Empty;
        public static string password2 = string.Empty;


        //excel open       
        public static List<string> files = new List<string>();

        //excel rows
        public static DataTable rows = new DataTable();

        //excel columns
        public static DataTable columns = new DataTable();

        //files upload
        public static List<string> uploads = new List<string>();

        //files download
        public static List<string> downloads = new List<string>();

        //users
        public static string xusername = string.Empty;
        public static string xempid = string.Empty;
        public static List<xUser> xuser = new List<xUser>();

        //public static List<string> iusername = new List<string>();
        //public static List<string> ipassword = new List<string>();
        //public static List<string> iurl = new List<string>();

        public static List<KeyValuePair<string, string>> ipuser = new List<KeyValuePair<string, string>>();
        public static List<KeyValuePair<string, string>> ippass = new List<KeyValuePair<string, string>>();
        public static List<KeyValuePair<string, string>> ipurl = new List<KeyValuePair<string, string>>();

        public static KeyValuePair<string, string> ilinuxconnection = new KeyValuePair<string, string>();

        public static StepDebug vDebugger = new StepDebug();

        public static Steps thisStep = new Steps();
        public static string thisVariable = string.Empty;

        public static MethodResult MethodReturns = new MethodResult();
    }

    public class TicketData
    {
        public string tickettitle { get; set; }
        public string tickeDesc { get; set; }
        public string ticketDate { get; set; }
        public string status { get; set; }
        public string priority { get; set; }
        public string type { get; set; }
        public string assignedUserId { get; set; }
        public string[] attachmentsIds { get; set; }
        public string assignedUserName { get; set; }
        public string attachmentsNames { get; set; }
        public string attachmentsTypes { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string accountName { get; set; }
        public string accountId { get; set; }
        public string[] contactsIds { get; set; }
        public string contactsNames { get; set; }
        public string[] teamsIds { get; set; }
        public string teamsNames { get; set; }
        public string leadName { get; set; }
        public string leadId { get; set; }
    }

    public class TicketGetData
    {
        public string id { get; set; }
        public string name { get; set; }
        public string deleted { get; set; }
        public string number { get; set; }
        public string status { get; set; }
        public string priority { get; set; }
        public string type { get; set; }
        public string description { get; set; }
        public string createdAt { get; set; }
        public string modifiedAt { get; set; }
        public string accountId { get; set; }
        public string accountName { get; set; }
        public string leadId { get; set; }
        public string leadName { get; set; }
        public string contactId { get; set; }
        public string contactName { get; set; }
        public string inboundEmailId { get; set; }
        public string inboundEmailName { get; set; }
        public string createdById { get; set; }
        public string createdByName { get; set; }
        public string modifiedById { get; set; }
        public string modifiedByName { get; set; }
        public string assignedUserId { get; set; }
        public string assignedUserName { get; set; }
    }

    public class TicketResponse
    {
        public string total { get; set; }
        public List<TicketGetData> list { get; set; }
    }
    public class xUser
    {
        public string adusername { get; set; }
        public string adpassword { get; set; }
        public string slackusername { get; set; }
        public string slackdpassword { get; set; }
        public string hrmsusername { get; set; }
        public string hrmsdpassword { get; set; }
        public string mailid { get; set; }
        public string mailpassword { get; set; }
        public string phonenumber { get; set; }
    }


}