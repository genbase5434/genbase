﻿using Genbase.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genbase.Classes
{
    public class ModelClass
    {
    }
    public class PagingParams
    {
        public int draw { get; set; }
        public int length { get; set; }
        public int start { get; set; }
        public List<PagingColumns> columns { get; set; }
        public List<PagingOrder> order { get; set; }
        public PagingSearch search { get; set; }
    }
    public class PagingColumns
    {
        public string data { get; set; }
        public string name { get; set; }
        public bool orderable { get; set; }
        public bool searchable { get; set; }
        public PagingSearch search { get; set; }
    }
    public class PagingSearch
    {
        public string value { get; set; }
        public bool regex { get; set; }
    }
    public class PagingOrder
    {
        public int column { get; set; }
        public string dir { get; set; }
    }
    public class UserInstance
    {
        public string ID { get; set; }
        public string UserID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string ActivityID { get; set; }
        public string Type { get; set; }
        public string RobotID { get; set; }
        public string StepID { get; set; }
    }
    public class PostDetails
    {
        public string postid { get; set; }
        public string message { get; set; }
        public string likes { get; set; }
        public string comments { get; set; }
    }
    public class Entities
    {
        public string Type { get; set; }
        public string Name { get; set; }
       // public string Robots { get; set; }
       // public string Fileupload { get; set; }
        public string Parent { get; set; }
        public string Class { get; set; }
        public List<Entities> Children { get; set; }
        public string oldname { set; get; }
        public string newname { set; get; }
        public string count { set; get; }
        public int Idm { set; get; }
        public string Namer { set; get; }
        public string Step_Id { get; set; }
        public string Project_Id { get; set; }
        public string oldstepvalue { get; set; }
        public string newstepvalue { get; set; }
        public string aid { get; set; }
        public string eid { get; set; }
        public string wid { get; set; }
        public string rid { get; set; }
        public string rname { get; set; }
        public string stepvalue { get; set; }
        public string ID { get; set; }
        public string fname { get; set; }
        public string mname { get; set; }
        public string DOB { get; set; }
        public string gender { get; set; }
        public string mstatus { get; set; }
        public string czship { get; set; }
        public string ID1 { get; set; }
        public string ID2 { get; set; }
        public string addTemp { get; set; }
        public string addPerm { get; set; }
        public string cnum { get; set; }
        public string cemail { get; set; }
        public string pphoto { get; set; }
        public string ctable { get; set; }
        public string StepPropertyType { get; set; }
        public string projectType { get; set; }
        public string subject { get; set; }
        public string ChildStepId { get; set; }
        public string Location { get; set; }
        public string LOBId { get; set; }
        public string StepPropertym { get; set; }
        public string StepPropertyValue { get; set; }
        public string RobotType { get; set; }
        public string userid { get; set; }
        public string DBType { get; set; }
        public string constring { get; set; }
        public string vvalues { get; set; }
        public string tablename { get; set; }
        public string stepProperty { get; set; }
        public string serviceActionType { get; set; }
        public string state { get; set; }
        public string Order { get; set; }
        public string jsonString1 { get; set; }
        public string jsonString2 { get; set; }
        public string jsonString3 { get; set; }
        public string logid { get; set; }
        public string logpwd { get; set; }
        public Robots robot { get; set; }
        public string RuntimeUserInput { get; set; }
        public string StepName { get; set; }
        public string Description { get; set; }
        public string Robot_ID { get; set; }

        public string WorkFlowProjectType { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }

        public List<Elements> elements { get; set; }
        public string Connection { get; set; }
        public DateTime? CurrentDateTime { get; set; }
        public DateTime? PreviousDateTime { get; set; }
        public string SystemId { get; set; }
        public string SystemName { get; set; }
        public string IP { get; set; }
        public string MAC { get; set; }
        public string interval { get; set; }
        public string LastExecutionTime { get; set; }
        public List<DVariables> dynamicvariables { get; set; }
        public string systemtype { get; set; }
        public string username { get; set; }
        public string userrole { get; set; }
        public string Version_Id { get; set; }
        public List<string> uploadFiles { get; set; }

        public PredefinedBots predefinedbot { get; set; }

        public string systemId_pm { get; set; }

        public Steps cstep = new Steps();
        public string user = string.Empty;
    }
    public class Scheduler
    {
        public string Id { get; set; }
        public string RobotId { get; set; }
        public string Interval { get; set; }
        public string LastExecutionTime { get; set; }
        public string SystemId { get; set; }
        public string SystemName { get; set; }
        public string Project_Id { get; set; }
        public string Name { get; set; }
        public string ProjectName { get; set; }
        public string MAC_Address { get; set; }
        public string RobotStatus { get; set; }
        public string Priority { get; set; }
        public string ScheduleStatus { get; set; }
        public string ID { get; set; }
        public string CurrentOutput { get; set; }
        public string Timeperiod { get; set; }
        public string Startdate { get; set; }
        public string Starttime { get; set; }
        public string EndDate { get; set; }
        public string End { get; set; }
        public string RepeatEvery { get; set; }
        public string RepeatOn { get; set; }
        public string Instance { get; set; }
    }
    public class Robots
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Project_Id { get; set; }
        public string Description { get; set; }
        public string RobotType { get; set; }
        public RobotProperties RProperties { get; set; }
        public List<StepDebug> Debugger { get; set; }
        public List<Steps> Steps { get; set; }
        public List<LinkNodes> LinkNodes { get; set; }
        public List<DVariables> dynamicvariables { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
    }
    public class RobotProperties
    {
        public string ID { get; set; }
        public string Robot_ID { get; set; }
        public string Name { get; set; }
        public bool Client_Dependency { get; set; }
        public string Default_Execution { get; set; }
    }
    public class WARobot
    {
        public string ID { get; set; }
        public string projectID { get; set; }
        public string RID { get; set; }
        public string Name { get; set; }
        public string StepsXML { get; set; }
        public string Description { get; set; }
    }
    public class ResponseResult
    {
        public int StatusCode { get; set; } = 200;
        public string StatusMessage { get; set; }
        public bool ExceptionStatus { get; set; } = false;
        public string ResultString { get; set; }
    }
    public class PredefinedBots
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Description { get; set; }
        public string Category_ID { get; set; }
        public string RobotType { get; set; }
        public List<PredefinedActions> Actions { get; set; }
    }
    public class PredefinedActions
    {
        public string ID { get; set; }
        public string Action_ID { get; set; }
        public string ChildActions { get; set; }
        public string Icon { get; set; }
        public string Location { get; set; }
        public string PredefinedBot_ID { get; set; }
    }
    public class LinkNodes
    {
        public string Id { get; set; }
        public string StepId { get; set; }
        public string Location { get; set; }
        public string ChildStepIds { get; set; }
        public string Status { get; set; }
    }
    public class DVariables
    {
        public string vlname { get; set; }
        public dynamic vlvalue { get; set; }
        public string vlstatus { get; set; }
        public string vlscope { get; set; }
        public string vltype { get; set; }
        public string RobotID { get; set; }
        public string ExecutionID { get; set; }
        public string sheetname { get; set; }
    }
    public class ELogs
    {
        public string ID { get; set; }
        public string StepName { get; set; }
        public string Description { get; set; }
        public string Robot_ID { get; set; }
        public int Type { get; set; }
    }
    public class ValidationTypes
    {
        public string ID { get; set; }
        public string ValidationName { get; set; }
        public string ValidationDescription { get; set; }
        public string ValidationType { get; set; }
        public bool Status { get; set; }
    }
    public class ElementProperties
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Action_Id { get; set; }
        public string PropertyType { get; set; }
        public string Order { get; set; }
        public string CustomValidation { get; set; }
    }
    public class StepDebug
    {
        public string ID { get; set; }
        public string projectID { get; set; }
        public string RID { get; set; }
        public string ActionName { get; set; }
        public string Description { get; set; }
        public string getNextstepid { get; set; }
    }
    public class Steps
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Workflow_Id { get; set; }
        public string Element_Id { get; set; }
        public string Action_Id { get; set; }
        public string Robot_Id { get; set; }
        public string Order { get; set; }
        public string Status { get; set; }
        public string RuntimeUserInput { get; set; }
        public string Dependency { get; set; }
        public string Version { get; set; }
        public string Type { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }

        public List<StepProperties> StepProperties { get; set; }
    }
    public class StepProperties
    {
        public string Id { get; set; }
        public string Steps_Id { get; set; }
        public string Project_Id { get; set; }
        public string StepPropertyType { get; set; }
        public string StepProperty { get; set; }
        public string StepPropertyValue { get; set; }
        public string Order { get; set; }
        public string CustomValidation { get; set; }
        public bool Status { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
    }
    public class DBConnection
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Server { get; set; }
        public string Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }
        public string DBType { get; set; }
        public string DBName { get; set; }

    }
    public class ArithmeticInput
    {
        public string type { get; set; }
        public string value { get; set; }
    }
    public class Actions
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Element_Id { get; set; }
        public string Icon { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string RuntimeUserInput { get; set; }
        public string Dependency { get; set; }
        public List<ElementProperties> elementProperties { get; set; }
    }
    public class Elements
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Description { get; set; }
        public string Type_Id { get; set; }
        public List<Actions> action { get; set; }
    }
    public class ResultStepDebug
    {
        public string StepName { get; set; }
        public List<string> Input { get; set; }
        public List<string> Output { get; set; }
    }
    public class EmailTemplateModel
    {
        public string ID { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Type { get; set; }
        public string Active { get; set; }
    }
    public class EmailDefineModel
    {
        public string Snumber { get; set; }
        public string Type { get; set; }

    }
    public class EmailTemplate
    {
        public string Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Type { get; set; }
        public string Active { get; set; }
    }
    public class Configuration
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
    public class FVariables
    {
        public string Name { get; set; }
        public dynamic Value { get; set; }
        public string Type { get; set; }
        public string VStatus { get; set; }
        public string Scope { get; set; }
        public string Robot_Id { get; set; }
    }
    public class ROBOT
    {
        public ROBOT(Dictionary<string, string> r)
        {
            this.Id = r["Id2"];
            this.Name = r["Name1"];
            this.Project_Id = r["Project_Id"];
            this.Description = r["Description"];
            this.RobotType = r["RobotType"];

        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string Project_Id { get; set; }
        public string Description { get; set; }
        public string RobotType { get; set; }

    }
    public class Projects
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DesktopProjectType { get; set; }
        public string WorkFlowProjectType { get; set; }
        public string LOBId { get; set; }
        public List<Robots> Robots { get; set; }
    }
    public class LineofBusiness
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Path { get; set; }
        public List<Projects> Projects { get; set; }
    }
    public class SystemsRobots
    {
        public string System_Id { get; set; }
        public List<ROBOT> robots { get; set; }
        public Systems system { get; set; }
    }
    public class ConnectivityInfo
    {
        public string Id { get; set; }
        public string Server { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
    }
    public class RobotsLogs
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string SystemName { get; set; }
        public string SystemID { get; set; }
        public string CreateTime { get; set; }
        public string LastExecutedTime { get; set; }
        public string Status { get; set; }
    }
    public class KPIConnection
    {
        public string ID { get; set; }
        public string ID1 { get; set; }
        public string Name { get; set; }
        public string Server { get; set; }
        public string Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }
    }
    public class DataTables
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public string data { get; set; }

    }
   
    public class Systems
    {
        public Systems(Dictionary<string, string> r)
        {
            try
            {
                this.ID = r["ID1"];
                this.Name = r["Name"];
                this.IP_Address = r["IP_Address"];
                this.MAC_Address = r["MAC_Address"];
                this.Binding_Time = Convert.ToDateTime(r["Binding_Time"]);
                this.Username = r["Username"];
                this.System_Status = r["System_Status"];
            }
            catch
            {

            }
        }

        public string ID { get; set; }
        public string Name { get; set; }
        public string IP_Address { get; set; }
        public string MAC_Address { get; set; }
        public DateTime? Binding_Time { get; set; }
        public string Username { get; set; }
        public string System_Status { get; set; }

    }
    public class RuntimeUserInputRequest
    {
        public Steps step { get; set; }
        public List<DVariables> dvariables { get; set; }
        public string thisVariableName { get; set; }
        public string nextStepID { get; set; }
    }
    public class Inprogress
    {
        public string Status { get; set; }
        public string inDex { get; set; }
        public Steps step { get; set; }
    }
    public class Systems_Robots
    {
        public string ID { get; set; }
        public string System_Id { get; set; }
        public string Robot_Id { get; set; }

    }
    public class Result
    {
        public int RobotId { get; set; }
        public int StepId { get; set; }
        public int SystemId { get; set; }
        public string SuccessState { get; set; }
        public string executionID { get; set; }
        public Step step { get; set; }
        public List<ELogs> elog { get; set; }
        public List<DVariables> dvs { get; set; }
        public Runtime runtime { get; set; }
        public string requestFrom { get; set; }
        public RuntimeUserInputRequest runtimerequest { get; set; }
        public StepDebug vdebugger { get; set; }
        public List<DownloadFilesObject> Downloads { get; set; }

    }
    public class Runtime
    {
        public string ID { get; set; }
        public string StepName { get; set; }
        public string Description { get; set; }
        public string Robot_ID { get; set; }
        public int Type { get; set; }
    }
    public class DownloadFilesObject
    {
        public string FolderAddress { get; set; }
        public string FileName { get; set; }
        public byte[] FileData { get; set; }
    }
    public class NKPI
    {
        public string ID { get; set; }
        public string MonitorType { get; set; }
        public string Title { get; set; }
        public string Group { get; set; }
        public string DbConnection_Id { get; set; }
        public string SQL { get; set; }
        public string data { get; set; }
    }
    public class KPI
    {
        public string ID { get; set; }
        public string PortalName { get; set; }
        public string Title { get; set; }
        public string Chart_Type { get; set; }
        public string SQL { get; set; }
        public string Row_Number { get; set; }
        public string Column_Number { get; set; }
        public string Link { get; set; }
        public string Height { get; set; }
        public string Width { get; set; }
        public string DbConnection_Id { get; set; }
        public string data { get; set; }
        public string data1 { get; set; }
        public string SQL1 { get; set; }
        public string KPI_ID { get; set; }
    }
    public class SystemsRobotsList
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string IP_Address { get; set; }
        public string MAC_Address { get; set; }
        public string Binding_Time { get; set; }
        public string Username { get; set; }
        public string System_Status { get; set; }
        public string type { get; set; }
        public List<Robots> robots { get; set; }
    }
    public class RobotsSytemsList
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Project_Id { get; set; }
        public string Description { get; set; }
        public string RobotType { get; set; }
        public List<SystemsR> Systems { get; set; }
    }
    public class SystemsR
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string IP_Address { get; set; }
        public string MAC_Address { get; set; }
        public string Binding_Time { get; set; }
        public string Username { get; set; }
        public string System_Status { get; set; }
        public string systemtype { get; set; }
    }
    public class ListDatabase
    {
        public string ID { get; set; }
        public string connectionname { get; set; }
        public string connectiontype { get; set; }
        public string hostname { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string port { get; set; }
        public string authenticationtype { get; set; }

    }
    public class Polling
    {
        public string Connection { get; set; }
        public DateTime? CurrentDateTime { get; set; }
        public DateTime? PreviousDateTime { get; set; }
        public string SystemName { get; set; }
        public string IP { get; set; }
        public string MAC { get; set; }
        public string Name { get; set; }
    }
    public class DocumentModel
    {
        //public string filePath { get; set; }
       // public bool status { get; set; }
        public int StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public bool ExceptionStatus { get; set; }
        public string ResultString { get; set; }
    }
}
