﻿using Genbase.BLL;
using Genbase.Classes;
using Genbase.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Genbase.classes
{
    public class LOB
    {
		// PostgresConnect dbd = new PostgresConnect();
		PostgresConnect dbd = PostgresConnect.GetInstance;

		string time = DateTime.Now.ToShortTimeString();
        string date = DateTime.Now.ToShortDateString();
        public string getLOB()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("LOB", "Status|Equals|true", "*", 0, "public", "", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getWFLOBProjectRobots()
        {
            string returns = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string lobquery = "select * from PUBLIC.\"LOB\" where \"Status\" = true order by \"ID\"";
                List<LineofBusiness> lobs = JsonConvert.DeserializeObject<List<LineofBusiness>>(dbd.DbExecuteQuery(lobquery, "select"));
                foreach (LineofBusiness lob in lobs)
                {
                    string projquery = "select * from public.\"PROJECT\" where \"LOBId\" = " + lob.ID + " and \"WorkFlowProjectType\"='project' and \"DesktopProjectType\"='API' and \"Status\" = true order by \"ID\" ";
                    List<Projects> projects = JsonConvert.DeserializeObject<List<Projects>>(dbd.DbExecuteQuery(projquery, "select"));
                    foreach (Projects project in projects)
                    {
                        string robquery = "select * FROM public.\"ROBOT\" where \"Project_Id\" = " + project.Id + " and \"RobotType\"='Robot' and \"Status\" = true order by \"Id\" ";
                        List<Robots> robots = JsonConvert.DeserializeObject<List<Robots>>(dbd.DbExecuteQuery(robquery, "select"));
                        project.Robots = robots;
                    }
                    lob.Projects = projects;
                }
                returns = JsonConvert.SerializeObject(lobs);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
            return returns;
        }
        public string getWFConnection()
        {
            string returns = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string conquery = "select * from PUBLIC.\"ConnectionConfig\" where \"Status\" = true order by \"ID\"";
                List<ListDatabase> listdbs = JsonConvert.DeserializeObject<List<ListDatabase>>(dbd.DbExecuteQuery(conquery, "select"));
                returns = JsonConvert.SerializeObject(listdbs);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
            return returns;
        }
    }
}