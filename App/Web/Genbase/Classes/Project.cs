﻿using Genbase.BLL;
using Genbase.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genbase.Classes
{
    public class Project
    {
        DAL.PostgresConnect dbd = new DAL.PostgresConnect();
        string time = DateTime.Now.ToShortTimeString();
        string date = DateTime.Now.ToShortDateString();

        public string createProject(string project, string projectname, string projecttype, string lobid, string username, string Description, string CreateBy)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "SELECT count(*) FROM public.\"PROJECT\" where \"NAME\"='" + projectname + "' and \"CreateBy\"='" + CreateBy + "' ";
                string ret = dbd.DbExecuteQuery(query, "select");
                List<Dictionary<string, string>> LValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(ret);
                if (LValues[0]["count"] != "0")
                {
                    ret = "false";
                }
                else
                {
                    string values = projectname + "," + Description + "," + projecttype + ",project," + username + "," + date + " " + time + "," + username + "," + date + " " + time + ",true," + lobid;

                    ret = new QueryUtil().insertTableData("public", "PROJECT", values, "ID", false);
                }
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }
        public string createProjectWorkflow(string project, string projectname, string projectDescription, string projecttype, string lobid, string username, string workflowtype, string CreateBy)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string query = "SELECT count(*) FROM public.\"PROJECT\" where \"NAME\"='" + projectname + "' and \"WorkFlowProjectType\"='" + workflowtype + "'and \"CreateBy\"='" + CreateBy + "' and \"Status\"=true";
                string ret = dbd.DbExecuteQuery(query, "select");
                List<Dictionary<string, string>> LValues = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(ret);
                if (LValues[0]["count"] != "0")
                {
                    ret = "false";
                }
                else
                {
                    if (lobid == null)
                        lobid = "null";
                    query = "insert into public.\"PROJECT\" values (default,'" + projectname + "','" + projectDescription + "','" + projecttype + "','" + workflowtype + "','" + CreateBy + "','" + date + " " + time + "','" + CreateBy + "','" + date + " " + time + "',true," + lobid + ") RETURNING \"ID\" ";
                    ret = dbd.DbExecuteQuery(query, "createproject");

                }
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }
        public string updateProject(string Project)
        {
            return "Not Implemented";
        }
        public string getProject(string Project)
        {

            string query = "select * from public.\"PROJECT\" Where \"NAME\"='" + Project + "' and \"Status\" = true";
            string ret = dbd.DbExecuteQuery(query, "select");
            return ret;
        }

        public string getProjectId(string ProjectId)
        {

            string query = "select * from public.\"PROJECT\" Where \"ID\"='" + ProjectId + "' and \"Status\" = true";
            string ret = dbd.DbExecuteQuery(query, "select");
            return ret;
        }

        public string RenameProject(string oldname, string newname, string projectid)
        {

            string query = "update public.\"PROJECT\" SET \"NAME\" =  '" + newname + "' where \"NAME\" = '" + oldname + "' and \"ID\" = '" + projectid + "'";
            string ret = dbd.DbExecuteQuery(query, "rename");
            return ret;
        }

        public string getProjects()
        {
            string query = "select * from public.\"PROJECT\" where \"Status\" = true order by \"ID\" ";
            string ret = dbd.DbExecuteQuery(query, "select");
            return ret;
        }
        public string getProjects(string username)
        {
            string query = "select * from public.\"PROJECT\" where \"Status\" = true and \"CreateBy\"='" + username + "' order by \"ID\" ";
            string ret = dbd.DbExecuteQuery(query, "select");
            return ret;
        }

        public string deleteProject(string Project)
        {
            string query = "update public.\"PROJECT\" SET \"Status\" = false where \"NAME\" = '" + Project + "'";
            string ret = dbd.DbExecuteQuery(query, "update");
            return ret;
        }

        public string getAllLOBs()
        {
            string query = "select \"ID\", \"Name\", \"Type\", \"Path\" from PUBLIC.\"LOB\" where \"Status\" = true order by \"ID\" ";
            string ret = dbd.DbExecuteQuery(query, "select");
            return ret;
        }
        public string getProjectsRobots()
        {
            string query = "SELECT public.\"PROJECT\".\"ID\", public.\"PROJECT\".\"NAME\",'0' as \"Parent_Id\"	FROM public.\"PROJECT\" UNION   SELECT public.\"ROBOT\".\"Id\", public.\"ROBOT\".\"Name\",\"Project_Id\" as \"Parent_Id\" FROM public.\"ROBOT\"  order by \"Parent_Id\" ";
            string ret = dbd.DbExecuteQuery(query, "select");
            return ret;
        }
        public string getLOBProjects(string lobid)
        {
            string query = "select * from public.\"PROJECT\" where \"LOBId\" = " + lobid + " and \"Status\" = true order by \"ID\"";
            string ret = dbd.DbExecuteQuery(query, "select");
            return ret;
        }
        public string getLOBProjectRobots()
        {
            string returns = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string lobquery = "select * from PUBLIC.\"LOB\" where \"Status\" = true order by \"ID\"";
                List<LineofBusiness> lobs = JsonConvert.DeserializeObject<List<LineofBusiness>>(dbd.DbExecuteQuery(lobquery, "select"));
                foreach (LineofBusiness lob in lobs)
                {
                    string projquery = "select * from public.\"PROJECT\" where \"LOBId\" = " + lob.ID + " and \"Status\" = true";
                    List<Projects> projects = JsonConvert.DeserializeObject<List<Projects>>(dbd.DbExecuteQuery(projquery, "select"));
                    foreach (Projects project in projects)
                    {
                        string robquery = "select * FROM public.\"ROBOT\" where \"Project_Id\" = " + project.Id + " and \"Status\" = true";
                        List<Robots> robots = JsonConvert.DeserializeObject<List<Robots>>(dbd.DbExecuteQuery(robquery, "select"));
                        project.Robots = robots;
                    }
                    lob.Projects = projects;
                }
                returns = JsonConvert.SerializeObject(lobs);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
            return returns;
        }
    }
}
