﻿using Genbase.BLL;
using Genbase.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.Classes
{
    public class cSystems_Robots
    {
        //PostgresConnect dbd = new PostgresConnect();
		PostgresConnect dbd =  PostgresConnect.GetInstance;
        string time = DateTime.Now.ToShortTimeString();
        string date = DateTime.Now.ToShortDateString();
        public string updateSysRobots(string systemid, string robotId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().updateTableData("SystemsRobots", "System_Id|Equals|" + systemid + "||Robot_Id|Equals|" + robotId, "Status|Equals|false", "public", false);
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getAllSysRobots(string systemid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("SystemsRobots", "Status|Equals|true||System_Id|Equals|" + systemid + "", "*", 0, "public", "", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getAllRobSystems(string robotid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("SystemsRobots", "Status|Equals|true||Robot_Id|Equals|" + robotid + "", "*", 0, "public", "", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getSystemRobotsCollection()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "SELECT * FROM public.\"SystemsRobots\" as sr inner join \"Systems\" as s on s.\"ID\"=sr.\"System_Id\" inner join \"ROBOT\" as r on r.\"Id\"=sr.\"Robot_Id\" where sr.\"Status\"=true";
                string ret = dbd.DbExecuteQuery(query, "select");
                List<int> sysIds = new List<int>();
                List<ROBOT> robot = new List<ROBOT>();
                List<Dictionary<string, string>> Result = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(ret);
                List<SystemsRobots> SystemsRobotList = new List<SystemsRobots>();

                foreach (Dictionary<string, string> r in Result)
                {
                    SystemsRobots systemRobo = new SystemsRobots();
                    {
                        systemRobo.System_Id = r[StringHelper.systemid];
                        sysIds.Add(Convert.ToInt32(r[StringHelper.systemid]));
                        Systems S = new Systems(r);
                        S.ID = r["ID1"];
                        ROBOT robo = new ROBOT(r);
                        robo.Id = r["Id2"];
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return string.Empty;
            }
        }
        public string setRobotSystem(string robotid, string systemid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string collection = getAllSysRobots(systemid);
                List<Systems_Robots> systemsRobots = JsonConvert.DeserializeObject<List<Systems_Robots>>(collection);
                if (systemsRobots.Any(x => x.Robot_Id == robotid))
                {
                    return StringHelper.recordexists;
                }
                else
                {
                    string values = systemid + ", " + robotid + ", Admin, " + date + " " + time + ", Admin, " + date + " " + time + ", true";
                    string ret = new QueryUtil().insertTableData("public", "SystemsRobots", values, "", false);
                    return ret;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getAllSchedulesforthisSystem(string systemmac)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<string> systemMacList = systemmac.Trim().Split(';').Where(x => !string.IsNullOrEmpty(x)).ToList<string>();
                string macCheck = "(";
                foreach (var sys in systemMacList)
                {
                    macCheck += "s.\"MAC_Address\" like '%" + sys.Trim() + "%' or ";
                }
                macCheck = macCheck.Remove(macCheck.Length - 4);
                macCheck += ")";
                //string query = "select distinct s.\"ID\" as \"SystemId\",s.\"Name\" as \"SystemName\",r.\"Name\", r.\"Id\" as \"ID\",r.\"Project_Id\",p.\"NAME\" as \"ProjectName\",sh.\"Priority\",sh.\"interval\" as \"Interval\",sh.\"LastExecutionTime\", s.\"MAC_Address\", sh.\"ScheduleStatus\",sh.\"Timeperiod\",sh.\"Starttime\",sh.\"Startdate\" from \"Scheduler\" sh inner join \"Systems\" s on s.\"ID\"=sh.\"SystemId\"::int inner join \"ROBOT\" r on r.\"Id\"=sh.\"RobotId\" inner join \"PROJECT\" p on p.\"ID\"=r.\"Project_Id\" where " + macCheck + " and r.\"Status\"=true and s.\"Status\"=true and sh.\"Status\"=true and p.\"Status\"=true";
                string query = "select distinct s.\"ID\" as \"SystemId\",s.\"Name\" as \"SystemName\",r.\"Name\", r.\"Id\" as \"ID\",r.\"Project_Id\",p.\"NAME\" as \"ProjectName\",sh.\"interval\" as \"Interval\",sh.\"LastExecutionTime\", s.\"MAC_Address\", sh.\"ScheduleStatus\",sh.\"Timeperiod\",sh.\"Starttime\",sh.\"Startdate\", sh.\"Instance\" from \"Scheduler\" sh inner join \"Systems\" s on s.\"ID\"=sh.\"SystemId\"::int inner join \"ROBOT\" r on r.\"Id\"=sh.\"RobotId\" inner join \"PROJECT\" p on p.\"ID\"=r.\"Project_Id\" where " + macCheck + " and r.\"Status\"=true and s.\"Status\"=true and sh.\"Status\"=true and p.\"Status\"=true";
                string schedule = dbd.DbExecuteQuery(query, "select");
                List<Scheduler> Schedulers = JsonConvert.DeserializeObject<List<Scheduler>>(schedule);
                List<Scheduler> FinalResult = new List<Scheduler>();

                foreach (Scheduler scd in Schedulers)
                {
                    if (scd.Timeperiod.ToLower() == "repeat by interval")
                    {
                        string selectquery = "select (CURRENT_TIMESTAMP-\"StartingTime\"::TimeStamp>(" + scd.Interval + " ||' minutes')::interval) and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') as timestatus  from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"='" + scd.SystemId + "' and \"Status\"=true order by \"ID\" DESC limit 1";
                        string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                        List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                        if (lastExecutionTime.Count == 0 || lastExecutionTime[0]["timestatus"] != "false")
                        {
                            FinalResult.Add(scd);
                        }
                    }
                    else if (scd.Timeperiod.ToLower() == "repeat once per day")
                    {
                        string selectquery = "select \"ID\"  from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"='" + scd.SystemId + "' and ((concat(concat(Current_Date,' '),'00:27')::Timestamp>=now() and \"StartingTime\"::Timestamp >= concat(concat(Current_Date, ' '), '00:27')::Timestamp - INTERVAL '1 DAY' and \"StartingTime\"::Timestamp <= concat(concat(Current_Date, ' '), '00:27')::Timestamp) or(concat(concat(Current_Date, ' '), '00:27')::Timestamp <= now() and \"StartingTime\"::Timestamp <= concat(concat(Current_Date, ' '), '00:27')::Timestamp + INTERVAL '1 DAY' and \"StartingTime\"::Timestamp >= concat(concat(Current_Date, ' '), '00:27')::Timestamp)) and \"Status\"=true and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') order by \"ID\" DESC limit 1";
                        //string selectquery = "select "+scd.Starttime+"(CURRENT_TIMESTAMP-\"StartingTime\"::TimeStamp>(" + scd.Interval + " ||' minutes')::interval) and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') as timestatus  from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"='" + scd.SystemId + "' and \"Status\"=true order by \"ID\" DESC limit 1";
                        string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                        List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                        if (lastExecutionTime.Count == 0)
                        {
                            FinalResult.Add(scd);
                        }
                    }
                    else if (scd.Timeperiod.ToLower() == "do not repeat")
                    {
                        string selectquery = "Select \"ID\" from \"ProcessManager where \"RobotId\"='" + scd.ID + "' and \"SystemId\"='" + scd.SystemId + "' and \"Startingtime\"::Timestamp>concat(concat('" + scd.Startdate + "',' '),'" + scd.Starttime + "')::Timestamp and \"Status\"=true and and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') ";
                        string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                        List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                        if (lastExecutionTime.Count == 0)
                        {
                            FinalResult.Add(scd);
                        }
                    }
                }
                //string supervisedQuery = "select sy.\"ID\" as \"SystemId\",sy.\"Name\" as \"SystemName\",r.\"Name\",r.\"Id\" as \"ID\",r.\"Project_Id\",p.\"NAME\" as \"ProjectName\",sy.\"MAC_Address\",'5' as \"Priority\" from \"ProcessManager\" pm inner join \"Systems\" sy on sy.\"ID\"=pm.\"SystemId\" inner join \"ROBOT\" r on r.\"Id\"::character varying=pm.\"RobotId\" inner join \"PROJECT\" p on p.\"ID\"=r.\"Project_Id\" where pm.\"JobStatus\"='Supervised Completed' and sy.\"MAC_Address\" like '%" + systemmac + "%' and pm.\"Status\"=true and r.\"Status\"=true and p.\"Status\"=true and sy.\"Status\"=true";
                string supervisedQuery = "select sy.\"ID\" as \"SystemId\",sy.\"Name\" as \"SystemName\",r.\"Name\",r.\"Id\" as \"ID\",r.\"Project_Id\",p.\"NAME\" as \"ProjectName\",sy.\"MAC_Address\",'5' as \"Priority\" from \"ProcessManager\" pm inner join \"Systems\" sy on sy.\"ID\"=pm.\"SystemId\" inner join \"ROBOT\" r on r.\"Id\"= CAST( pm.\"RobotId\" AS bigint) inner join \"PROJECT\" p on p.\"ID\"=r.\"Project_Id\" where pm.\"JobStatus\"='Supervised Completed' and sy.\"MAC_Address\" like '%" + systemmac + "%' and pm.\"Status\"=true and r.\"Status\"=true and p.\"Status\"=true and sy.\"Status\"=true";
                string supervisedResult = dbd.DbExecuteQuery(supervisedQuery, "select");
                List<Scheduler> supervisedBots = JsonConvert.DeserializeObject<List<Scheduler>>(supervisedResult);
                if (supervisedBots.Count > 0)
                {
                    FinalResult.AddRange(supervisedBots);
                    FinalResult = FinalResult.OrderByDescending(s => Convert.ToInt32(s.Priority)).ToList();
                }
                return JsonConvert.SerializeObject(FinalResult);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return "false";
            }
            //string query = "select r.\"Name\",r.\"Id\",r.\"Project_Id\", s.\"interval\" as \"Interval\",s.\"LastExecutionTime\", sy.\"MAC_Address\", s.\"Priority\", s.\"ScheduleStatus\" from public.\"Scheduler\" s, public.\"ROBOT\" r, public.\"Systems\" sy where s.\"RobotId\" = r.\"Id\" and s.\"SystemId\"::bigint = sy.\"ID\"; ";
            //string ret = new DAL.Database().DbExecuteQuery(query, "select");

            //List<ScheduleDetails> scs = JsonConvert.DeserializeObject<List<ScheduleDetails>>(ret);
            //scs.RemoveAll(sc=>sc.MAC_Address.Trim() != systemmac);

            //return ret;
        }
        public string getAllSchedulesforthisServer()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                //string query = "select distinct r.\"Id\" as \"ID\",r.\"Project_Id\",sh.\"SystemId\",p.\"NAME\" as \"ProjectName\",sh.\"interval\" as \"Interval\",sh.\"LastExecutionTime\", sh.\"ScheduleStatus\",sh.\"Timeperiod\",sh.\"Starttime\",sh.\"Startdate\",sh.\"EndDate\",sh.\"End\",sh.\"RepeatEvery\",sh.\"RepeatOn\"   from \"Scheduler\"sh inner join \"ROBOT\" r on r.\"Id\"=sh.\"RobotId\" inner join \"PROJECT\" p on p.\"ID\"=r.\"Project_Id\" where now() >= concat(concat(sh.\"Startdate\",' '),sh.\"Starttime\")::timestamp and r.\"Status\"=true and sh.\"Status\"=true and p.\"Status\"=true and sh.\"Runatserver\"=true";
                string query = "select distinct r.\"Id\" as \"ID\",r.\"Project_Id\",sh.\"SystemId\",p.\"NAME\" as \"ProjectName\",sh.\"interval\" as \"Interval\",sh.\"LastExecutionTime\", sh.\"ScheduleStatus\", sh.\"Timeperiod\",sh.\"Starttime\",sh.\"Startdate\",sh.\"EndDate\", sh.\"Instance\" ,sh.\"End\",sh.\"RepeatEvery\", sh.\"RepeatOn\"   from \"Scheduler\"sh inner join \"ROBOT\" r on r.\"Id\"=sh.\"RobotId\" inner join \"PROJECT\" p on p.\"ID\"=r.\"Project_Id\" where r.\"Status\"=true and sh.\"Status\"=true and p.\"Status\"=true and sh.\"Runatserver\"=true and sh.\"Startdate\" IS NOT NULL and sh.\"Starttime\" IS NOT NULL and now() >= concat(concat(sh.\"Startdate\",' '), sh.\"Starttime\")::timestamp";
                string schedule = dbd.DbExecuteQuery(query, "select");
                List<Scheduler> Schedulers = new List<Scheduler>();
                if (schedule != null)
                    Schedulers = JsonConvert.DeserializeObject<List<Scheduler>>(schedule);
                List<Scheduler> FinalResult = new List<Scheduler>();

                foreach (Scheduler scd in Schedulers)
                {
                    if (scd.Timeperiod.ToLower() == "Never")
                    {
                        string selectquery = "Select \"ID\" from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"=0 and \"StartingTime\"::Timestamp>concat(concat('" + scd.Startdate + "',' '),'" + scd.Starttime + "')::Timestamp and \"Status\"=true and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') ";
                        string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                        List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                        if (lastExecutionTime.Count == 0)
                        {
                            FinalResult.Add(scd);
                        }
                    }
                    else if(scd.Timeperiod.ToLower() == "repeat by interval")
                    {
                        string selectquery = "select (CURRENT_TIMESTAMP-\"StartingTime\"::TimeStamp>(" + scd.Interval + " ||' minutes')::interval) and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') as timestatus  from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"=0 and \"Status\"=true order by \"ID\" DESC limit 1";
                        string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                        List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                        if (lastExecutionTime.Count == 0 || lastExecutionTime[0]["timestatus"] != "false")
                        {
                            FinalResult.Add(scd);
                        }
                    }
                    //else if (scd.Timeperiod.ToLower() == "repeat once per day")
                    //{
                    //    string selectquery = "select \"ID\"  from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"='" + scd.SystemId + "' and ((concat(concat(Current_Date,' '),'00:27')::Timestamp>=now() and \"StartingTime\"::Timestamp >= concat(concat(Current_Date, ' '), '00:27')::Timestamp - INTERVAL '1 DAY' and \"StartingTime\"::Timestamp <= concat(concat(Current_Date, ' '), '00:27')::Timestamp) or(concat(concat(Current_Date, ' '), '00:27')::Timestamp <= now() and \"StartingTime\"::Timestamp <= concat(concat(Current_Date, ' '), '00:27')::Timestamp + INTERVAL '1 DAY' and \"StartingTime\"::Timestamp >= concat(concat(Current_Date, ' '), '00:27')::Timestamp)) and \"Status\"=true and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') order by \"ID\" DESC limit 1";
                    //    //string selectquery = "select "+scd.Starttime+"(CURRENT_TIMESTAMP-\"StartingTime\"::TimeStamp>(" + scd.Interval + " ||' minutes')::interval) and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') as timestatus  from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"='" + scd.SystemId + "' and \"Status\"=true order by \"ID\" DESC limit 1";
                    //    string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                    //    List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                    //    if (lastExecutionTime.Count == 0)
                    //    {
                    //        FinalResult.Add(scd);
                    //    }
                    //}
                    //else if (scd.Timeperiod.ToLower() == "do not repeat")
                    //{
                    //    string selectquery = "Select \"ID\" from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"=0 and \"StartingTime\"::Timestamp>concat(concat('" + scd.Startdate + "',' '),'" + scd.Starttime + "')::Timestamp and \"Status\"=true and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') ";
                    //    string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                    //    List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                    //    if (lastExecutionTime.Count == 0)
                    //    {
                    //        FinalResult.Add(scd);
                    //    }
                    //}
                    else if(scd.Timeperiod.ToLower() == "daily")
                    {
                        if (scd.End.ToLower() == "never")
                        {
                            //string selectquery = "Select \"ID\" from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\" IS NULL and \"StartingTime\"::Timestamp>concat(concat('" + scd.Startdate + "',' '),'" + scd.Starttime + "')::Timestamp and \"Status\"=true and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') ";
                            string selectquery = "select \"ID\" from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"=0 and ((concat(concat(Current_Date,' '),'" + scd.Starttime + " + 00:05')::Timestamp>=now() and \"StartingTime\"::Timestamp >= concat(concat(Current_Date, ' '), '" + scd.Starttime + " + 00:05')::Timestamp - INTERVAL '1 DAY' and \"StartingTime\"::Timestamp <= concat(concat(Current_Date, ' '), '" + scd.Starttime + " + 00:05')::Timestamp) or(concat(concat(Current_Date, ' '), '" + scd.Starttime + " + 00:05')::Timestamp <= now() and \"StartingTime\"::Timestamp <= concat(concat(Current_Date, ' '), '" + scd.Starttime + " + 00:05')::Timestamp + INTERVAL '1 DAY' and \"StartingTime\"::Timestamp >= concat(concat(Current_Date, ' '), '" + scd.Starttime + " + 00:05')::Timestamp)) and \"Status\"=true and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') order by \"ID\" DESC limit 1";
                            string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                            List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                            if (lastExecutionTime.Count == 0)
                            {
                                FinalResult.Add(scd);
                            }
                        }
                     else if (scd.End.ToLower().Contains("After"))
                        {
                            string selectquery = "Select \"ID\" from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"=0 and \"StartingTime\"::Timestamp>concat(concat('" + scd.Startdate + "',' '),'" + scd.Starttime + "')::Timestamp and \"Status\"=true and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') ";
                            string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                            List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                            if (lastExecutionTime.Count == 0)
                            {
                                FinalResult.Add(scd);
                            }

                        }
                        else if (scd.End.ToLower().Contains("On"))
                        {
                            string selectquery = "Select \"ID\" from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"=0 and \"StartingTime\"::Timestamp>concat(concat('" + scd.Startdate + "',' '),'" + scd.Starttime + "')::Timestamp and \"Status\"=true and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') ";
                            string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                            List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                            if (lastExecutionTime.Count == 0)
                            {
                                FinalResult.Add(scd);
                            }

                        }
                    }
                    else if(scd.Timeperiod.ToLower() == "weekly")
                    {
                            if (scd.End.ToLower() == "never")
                            {
                                string selectquery = "Select \"ID\" from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"=0 and \"StartingTime\"::Timestamp>concat(concat('" + scd.Startdate + "',' '),'" + scd.Starttime + "')::Timestamp and \"Status\"=true and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') ";
                                string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                                List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                                if (lastExecutionTime.Count == 0)
                                {
                                    FinalResult.Add(scd);
                                }

                            }
                        
                        else if (scd.End.ToLower().Contains("After"))
                        {
                            string selectquery = "Select \"ID\" from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"=0 and \"StartingTime\"::Timestamp>concat(concat('" + scd.Startdate + "',' '),'" + scd.Starttime + "')::Timestamp and \"Status\"=true and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') ";
                            string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                            List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                            if (lastExecutionTime.Count == 0)
                            {
                                FinalResult.Add(scd);
                            }

                        }
                        else if (scd.End.ToLower().Contains("On"))
                        {
                            string selectquery = "Select \"ID\" from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"=0 and \"StartingTime\"::Timestamp>concat(concat('" + scd.Startdate + "',' '),'" + scd.Starttime + "')::Timestamp and \"Status\"=true and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') ";
                            string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                            List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                            if (lastExecutionTime.Count == 0)
                            {
                                FinalResult.Add(scd);
                            }

                        }
                    }
                    else if (scd.Timeperiod.ToLower() == "monthly")
                    {
                        if (scd.End.ToLower() == "never")
                        {
                            string selectquery = "Select \"ID\" from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"=0 and \"StartingTime\"::Timestamp>concat(concat('" + scd.Startdate + "',' '),'" + scd.Starttime + "')::Timestamp and \"Status\"=true and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') ";
                            string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                            List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                            if (lastExecutionTime.Count == 0)
                            {
                                FinalResult.Add(scd);
                            }

                        }
                        else if (scd.End.ToLower().Contains("After"))
                        {
                            string selectquery = "Select \"ID\" from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"=0 and \"StartingTime\"::Timestamp>concat(concat('" + scd.Startdate + "',' '),'" + scd.Starttime + "')::Timestamp and \"Status\"=true and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') ";
                            string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                            List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                            if (lastExecutionTime.Count == 0)
                            {
                                FinalResult.Add(scd);
                            }

                        }
                        else if (scd.End.ToLower().Contains("On"))
                        {
                            string selectquery = "Select \"ID\" from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"=0 and \"StartingTime\"::Timestamp>concat(concat('" + scd.Startdate + "',' '),'" + scd.Starttime + "')::Timestamp and \"Status\"=true and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') ";
                            string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                            List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                            if (lastExecutionTime.Count == 0)
                            {
                                FinalResult.Add(scd);
                            }

                        }
                    }
                  else if (scd.Timeperiod.ToLower() == "yearly")
                    {
                        string selectquery = "select (CURRENT_TIMESTAMP-\"StartingTime\"::TimeStamp>(" + scd.Interval + " ||' minutes')::interval) and (\"JobStatus\" != 'Supervised' and \"JobStatus\" != 'Supervised Completed') as timestatus  from \"ProcessManager\" where \"RobotId\"='" + scd.ID + "' and \"SystemId\"=0 and \"Status\"=true order by \"ID\" DESC limit 1";
                        string lastExecutionResult = dbd.DbExecuteQuery(selectquery, "select");
                        List<Dictionary<string, string>> lastExecutionTime = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(lastExecutionResult);
                        if (lastExecutionTime.Count == 0 || lastExecutionTime[0]["timestatus"] != "false")
                        {
                            FinalResult.Add(scd);
                        }
                    }
                }
                //string supervisedQuery = "select sy.\"ID\" as \"SystemId\",sy.\"Name\" as \"SystemName\",r.\"Name\",r.\"Id\" as \"ID\",r.\"Project_Id\",p.\"NAME\" as \"ProjectName\",sy.\"MAC_Address\",i.\"CurrentOutput\",'5' as \"Priority\" from \"ProcessManager\" pm inner join \"Systems\" sy on sy.\"ID\"=pm.\"SystemId\" inner join \"ROBOT\" r on r.\"Id\"::character varying=pm.\"RobotId\" inner join \"PROJECT\" p on p.\"ID\"=r.\"Project_Id\" inner join \"Inbox\" i on i.\"JobId\"=pm.\"ID\" where pm.\"JobStatus\"='Supervised Completed' and sy.\"ID\"=0 and pm.\"Status\"=true and r.\"Status\"=true and p.\"Status\"=true and sy.\"Status\"=true and i.\"Status\"=true";
                string supervisedQuery = "select sy.\"ID\" as \"SystemId\",sy.\"Name\" as \"SystemName\",r.\"Name\",r.\"Id\" as \"ID\",r.\"Project_Id\",p.\"NAME\" as \"ProjectName\",sy.\"MAC_Address\",i.\"CurrentOutput\",'5' as \"Priority\" from \"ProcessManager\" pm inner join \"Systems\" sy on sy.\"ID\"=pm.\"SystemId\" inner join \"ROBOT\" r on r.\"Id\"=CAST( pm.\"RobotId\" AS bigint ) inner join \"PROJECT\" p on p.\"ID\"=r.\"Project_Id\" inner join \"Inbox\" i on i.\"JobId\"=pm.\"ID\" where pm.\"JobStatus\"='Supervised Completed' and sy.\"ID\"=0 and pm.\"Status\"=true and r.\"Status\"=true and p.\"Status\"=true and sy.\"Status\"=true and i.\"Status\"=true";
                string supervisedResult = dbd.DbExecuteQuery(supervisedQuery, "select");
                List<Scheduler> supervisedBots = JsonConvert.DeserializeObject<List<Scheduler>>(supervisedResult);
                if (supervisedBots.Count > 0)
                {
                    FinalResult.AddRange(supervisedBots);
                    FinalResult = FinalResult.OrderByDescending(s => Convert.ToInt32(s.Priority)).ToList();
                }
                return JsonConvert.SerializeObject(FinalResult);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return "false";
            }
        }

        static DateTime GetDateFromWeekNumberAndDayOfWeek(int year,int month,int weekNumber, int dayOfWeek)
        {
            int i, prev_months_days=0;
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Tuesday - jan1.DayOfWeek;

            DateTime firstMonday = jan1.AddDays(daysOffset);

            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(jan1, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            for (i=1; i< month; i++) {
                int days = DateTime.DaysInMonth(2020, 7);
                prev_months_days += days;
            }
            var prev_months_weeks = prev_months_days / 7;
            var weekNum = prev_months_weeks + weekNumber;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }

            var result = firstMonday.AddDays(weekNum * 7 + dayOfWeek - 1);
            return result;
        }

        public static DateTime FirstDateOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            // Use first Thursday in January to get first week of the year as
            // it will never be in Week 52/53
            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            // As we're adding days to a date in Week 1,
            // we need to subtract 1 in order to get the right date for week #1
            if (firstWeek == 1)
            {
                weekNum -= 1;
            }

            // Using the first Thursday as starting week ensures that we are starting in the right year
            // then we add number of weeks multiplied with days
            var result = firstThursday.AddDays(weekNum * 7);

            // Subtract 3 days from Thursday to get Monday, which is the first weekday in ISO8601
            return result.AddDays(-3);
        }

        public class ScheduleDetails
        {
            public string Name { get; set; }
            public string Interval { get; set; }
            public string MAC_Address { get; set; }
            public string RobotStatus { get; set; }
            public string Proirity { get; set; }
            public string ScheduleStatus { get; set; }
        }
    }
}
