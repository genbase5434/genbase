﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Genbase.BLL;
using System.Text.RegularExpressions;

namespace Genbase.Classes
{
    public class cScript
    {
        public string newLine = "\n";
       // public string generateJS(string fileName, string ScreenDetails, string ElementRows, string pathUI)
        public string generateJS(string fileName, string ScreenDetails, string ElementRows)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string line;
                JObject screenData = JObject.Parse(ScreenDetails);
                JArray data = JArray.Parse(ElementRows);

                 var UIRootPath = SetEnvironmentPath();
                //var UIRootPath = pathUI;
                var path = UIRootPath + @"Views\generatedJS\" + fileName.Trim()/*screenData["SName"].ToString()*/ + ".js";

                try
                {
                    var sb = new StringBuilder();
                    FileStream fs = new FileStream(path, FileMode.Create);
                    fs.Close();
                    string jsPath = "";
                    if (screenData[StringHelper.formtype].ToString() == StringHelper.inline)
                        jsPath = "Views\\Temp\\InlineTemplate.js";
                    else
                        jsPath = "Views\\Temp\\Template1.js";
                    StreamReader file = new StreamReader(UIRootPath + jsPath);
                    while ((line = file.ReadLine()) != null)
                        sb.AppendLine(line.Substring(0, Math.Min(1000, line.Length)));
                    file.Close();
                    if (screenData[StringHelper.rcount].ToString() == "")
                        screenData[StringHelper.rcount] = "10";
                    sb.Replace("{Screen Title}", screenData[StringHelper.sname].ToString().Trim());
                    sb.Replace("{pageSize}", screenData[StringHelper.rcount].ToString().Trim());
                    if (screenData[StringHelper.preferences].ToString() == StringHelper.uuser)
                    {
                        sb.Replace("{getDataFunction}'", "getGridDataByUser?tablename=' + document.getElementsByTagName(\"tablename\")[0].innerHTML+'&CreateBy='+JSON.parse(localStorage.Result)[0][\"ID\"]");
                    }
                    else
                    {
                        sb.Replace("{getDataFunction}'", "getGridData?tablename=' + document.getElementsByTagName(\"tablename\")[0].innerHTML");
                    }
                    JArray ElementRow = JArray.Parse(ElementRows);
                    List<Dictionary<string, string>> list = new List<Dictionary<string, string>>();
                    int ListCount = ElementRow.Count;
                    foreach (JObject o in ElementRow.Children<JObject>())
                    {
                        Dictionary<string, string> dict = new Dictionary<string, string>();
                        foreach (JProperty p in o.Properties())
                        {

                            string name = p.Name;
                            string value = (string)p.Value;
                            if (name == StringHelper.columnorder && value == "")
                                value = "" + ListCount++ + "";
                            dict.Add(name, value);
                        }
                        list.Add(dict);
                    }

                    var list1 = list.OrderBy(x => Convert.ToInt64(x[StringHelper.columnorder])).ToList<Dictionary<string, string>>();
                    sb.Replace("{ DefaultValuesDropDown }", defaultValuesDropDown(list1));
                    sb.Replace("[hidecolumns]", hideColumns(list1));
                    sb.Replace("\"[FillGlobalVar]\":", fillGlobalVar(list1));
                    sb.Replace("\"[FillGlobalVar]\":", "");
                    if (Convert.ToBoolean(screenData[StringHelper.adel]) == true && Convert.ToBoolean(screenData[StringHelper.aedit]) == true)
                    {

                        sb.Replace("\"[Action]\",", "{\"title\": \"Action\",\"defaultContent\":\"<button class='btn btn-warning btn-flat' title='Edit' onclick='editFunction(this)'><i class='fa fa-edit' aria-hidden='true' style='font-size:16px'></i></button>&nbsp;<button class='deleteBtnClass btn btn-flat' title='Delete' onclick='deleteFunction(this)'><i class='fa fa-trash' aria-hidden='true' style='font-size:16px'></i> </button>\"}],");
                    }
                    else if (Convert.ToBoolean(screenData[StringHelper.adel]) == true)
                    {

                        sb.Replace("\"[Action]\",", "{\"title\": \"Action\",\"defaultContent\":\"<button class='deleteBtnClass btn btn-flat' title='Delete' onclick='deleteFunction(this)'><i class='fa fa-trash' aria-hidden='true' style='font-size:16px'></i> </button>\"}],");
                    }
                    else if (Convert.ToBoolean(screenData[StringHelper.aedit]) == true)
                    {
                        sb.Replace("\"[Action]\",", "{\"title\": \"Action\",\"defaultContent\":\"<button class='btn btn-warning btn-flat' title='Edit' onclick='editFunction(this)'><i class='fa fa-edit' aria-hidden='true' style='font-size:16px'></i></button>\"}],");
                    }
                    else
                    {
                        sb.Replace("\"[Action]\",", "");
                    }

                    sb.Replace("{SortColumn}", "\"" + data[0][StringHelper.cname] + "\"");
                    if (screenData[StringHelper.dsort].ToString() == StringHelper.asc)
                    {
                        sb.Replace("{DefaultSort}", "\"asc\"");
                    }
                    else if (screenData["DSort"].ToString() == StringHelper.desc)
                    {
                        sb.Replace("{DefaultSort}", "\"desc\"");
                    }
                    else
                    {
                        sb.Replace("{DefaultSort}", "\"\"");
                    }
                    using (StreamWriter files = new StreamWriter(UIRootPath + @"Views\generatedJS\" + fileName/*screenData["SName"].ToString()*/ + ".js"))
                    {
                        files.WriteLine(sb.ToString());
                    }
                    return StringHelper.truee;
                }
                catch (Exception e)
                {
                    return e.ToString();
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }

        }
        public string defaultValuesDropDown(List<Dictionary<String, String>> ElementRows)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                var sb = new StringBuilder();
                for (int i = 0; i < ElementRows.Count; i++)
                {
                    sb.AppendLine("DropDownDefaultValues[\"select" + ElementRows[i][StringHelper.cname].Trim() + "\"]=\"" + ElementRows[i][StringHelper.defaultvalue].Trim() + "\"");
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string fillGlobalVar(List<Dictionary<String, String>> ElementRows)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                var sb = new StringBuilder();
                sb.AppendLine("\"columns\": [" + newLine);
                for (int i = 0; i < ElementRows.Count; i++)
                {
                    if (Convert.ToBoolean(ElementRows[i]["active"]) == true)
                    {
                        sb.AppendLine("{\"data\":\"" + ElementRows[i][StringHelper.cname].Trim() + "\"," + newLine);
                        sb.AppendLine("\"title\":\"" + ElementRows[i][StringHelper.displayname].Trim() + "\"}," + newLine);
                    }
                }

                return sb.ToString();
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string hideColumns(List<Dictionary<String, String>> ElementRows)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                var sb = new StringBuilder();
                string s = "";
                for (int i = 0; i < ElementRows.Count; i++)
                {
                    var x = Convert.ToBoolean(ElementRows[i][StringHelper.quottedactive]);
                    if (x == true)
                    {
                        if (Convert.ToBoolean(ElementRows[i][StringHelper.showgrid]) != true)
                        {
                            if (s == "")
                                s += i;
                            else
                                s += "," + i;
                        }
                    }
                }
                sb.AppendLine("{\"aTargets\": [0," + s.Trim() + "]," + newLine);
                sb.AppendLine("\"sClass\": \"hidden\" }" + newLine);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string SetEnvironmentPath()
        {
            string UIRootPath = "";
            string workingDirectory = Environment.CurrentDirectory;
            new ExecutionLog().add2Log(workingDirectory, "SetEnvironmentPath", "Script", StringHelper.eventcode, false);
            string currentDirectory = "";
            if (workingDirectory.Contains("trunk"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"/Genbase_UI/wwwroot/";
            }
            else if (workingDirectory.Contains("Genbase_v2_apiservice-DEV"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"\\Genbase_v2_uiservice-DEV\\wwwroot\\";
            }
            else if (workingDirectory.Contains("Genbase_v2_apiservice-QA"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"\\Genbase_v2_uiservice-QA\\wwwroot\\";
            }
            else if (workingDirectory.Contains("Genbase_v2_apiservice-Alpha"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"\\Genbase_v2_uiservice-Alpha\\wwwroot\\";
            }
            else if (workingDirectory.Contains("Genbase_v2_apiservice-Beta"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"\\Genbase_v2_uiservice-Beta\\wwwroot\\";
            }
            else if (workingDirectory.Contains("Genbase_v2_apiservice-Prod"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"\\Genbase_v2_uiservice-Prod\\wwwroot\\";
            }
            else if (workingDirectory.Contains("Genbase_v2_apiservice-TVG"))
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"\\Genbase_v2_uiservice-TVG\\wwwroot\\";
            }
            else
            {
                currentDirectory = Path.GetDirectoryName(workingDirectory);
                UIRootPath = currentDirectory + @"/Genbase_UI/wwwroot/";
            }

            return UIRootPath;
        }
    }
}
