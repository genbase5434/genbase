﻿using Genbase.BLL;
using Genbase.DAL;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genbase.Classes
{
    public class Roles
    {
		PostgresConnect dbd =  PostgresConnect.GetInstance;
        //PostgresConnect dbd = new PostgresConnect();
		Connection c = Connection.GetInstance;
        public string getRole(string username)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {


                string QgetRole = "select \"RoleName\" from public.\"ROLE\" where \"ID\" in (select \"Role_ID\" from public.\"USER\" where \"UserName\"='" + username + "')";
                string getRole = dbd.DbExecuteQuery(QgetRole, "selectwithnodes");
                return getRole;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string getRoles(int screenId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                // Connection c = new Connection();
                string query = "select rmr.\"Role_ID\" from \"Role_Menu_Relation\" rmr inner join \"MENU\" m on m.\"ID\"=rmr.\"Menu_ID\" where m.\"ScreenId\"='" + screenId + "' and m.\"Status\"=true and rmr.\"Status\"=true";
                string result = c.executeSQL(query, "Select");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string loadRoles()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                // Connection c = new Connection();
                string result = new QueryUtil().getTableData("ROLE", "Status|Equals|true", "\"ID\",\"RoleName\"", 0, "public", "\"ID\"", "");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }

        }
        public string addRoles(int menuId, string Roles, string CreateBy, string UpdateBy)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = "";
                // Connection c = new Connection();
                string time = DateTime.Now.ToShortTimeString();
                string date = DateTime.Now.ToShortDateString();
                try
                {
                    JArray roleId = JArray.Parse(Roles);
                    for (int i = 0; i < roleId.Count; i++)
                    {
                        string values = roleId[i] + ", " + menuId + "," + true + "," + CreateBy + "," + date + "" + time + "," + UpdateBy + "," + date + "" + time;
                        result = new QueryUtil().insertTableData("public", "Role_Menu_Relation", values, "", false);
                    }
                    return result;
                }
                catch (Exception ex)
                {

                    string ec = ex.Message;
                    string values = Roles + ", " + menuId + ", 1";
                    result = new QueryUtil().insertTableData("public", "Role_Menu_Relation", values, "", false);
                    return result;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string updateRoles(int menuId, string Roles)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                JArray roleId = JArray.Parse(Roles);
                string result = "";
                // Connection c = new Connection();
                result = new QueryUtil().updateTableData("Role_Menu_Relation", "Menu_ID|Equals|" + menuId, "Status|Equals|0", "public", false);
                for (int i = 0; i < roleId.Count; i++)
                {
                    string query2 = "UPDATE public.\"Role_Menu_Relation\" SET \"Status\"='1' WHERE \"Role_ID\"='" + roleId[i] + "' and \"Menu_ID\"='" + menuId + "' returning \"ID\";";
                    result = c.executeSQL(query2, "Select");
                    JArray r = JArray.Parse(result);
                    string x = r.ToString();
                    if (r.ToString() == "[]")
                    {
                        string values = roleId[i] + ", " + menuId + ", true";
                        result = new QueryUtil().insertTableData("public", "Role_Menu_Relation", values, "", false);
                    }
                    else
                    {
                        result = StringHelper.truee;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
    }
}
