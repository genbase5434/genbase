﻿using Genbase.BLL;
using Genbase.Classes;
using Genbase.DAL;
using System;
using System.Collections.Generic;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.Classes
{
    public class LinkNode
    {
        //PostgresConnect dbd = new PostgresConnect();
		PostgresConnect dbd =  PostgresConnect.GetInstance;
        BLL.EncoderDecoderBase64 baser = new BLL.EncoderDecoderBase64();
        public string getStepsLinks(string rid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "SELECT * FROM public.\"LinkNodes\" l inner join \"STEPS\" s on  l.\"StepId\"=(select distinct s.\"Id\"  where s.\"Robot_Id\" ='" + rid + "' and s.\"Status\"='1') and l.\"Status\"='1'";
                string ret = dbd.DbExecuteQuery(query, "select");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string InsertLinkNodes(List<LinkNodes> list)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = "";
                string datetime = DateTime.Now.ToShortDateString() + DateTime.Now.ToShortTimeString();
                foreach (LinkNodes l in list)
                {
                    if (l.Id != null)
                        ret = new QueryUtil().updateTableData("LinkNodes", "StepId|Equals|" + l.StepId, "Location|Equals|" + l.Location + "||ChildStepIds|Equals|" + l.ChildStepIds + "||Status|Equals|true", "public", false);
                    else
                    {
                        string values = baser.Base64Encode(l.StepId) + "," + baser.Base64Encode(l.Location) + "," + baser.Base64Encode(l.ChildStepIds) + "," + baser.Base64Encode("Admin") + "," + baser.Base64Encode(datetime) + "," + baser.Base64Encode("Admin") + "," + baser.Base64Encode(datetime) + ", " + baser.Base64Encode("true") + "";
                        ret = new QueryUtil().insertTableData("public", "LinkNodes", values, "", true);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        string time = DateTime.Now.ToShortTimeString();
        string date = DateTime.Now.ToShortDateString();
        public string getNodes(string stepId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "select \"Id\", \"StepId\", \"Location\", \"ChildStepIds\" FROM public.\"LinkNodes\" s where s.\"StepId\"= " + stepId + " and s.\"Status\" =true ";
                string ret = dbd.DbExecuteQuery(query, "select");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string getAllNodes(string robotId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "SELECT a.\"Id\", a.\"StepId\",a.\"Location\",a.\"ChildStepIds\",a.\"Status\" FROM public.\"LinkNodes\" as a  INNER JOIN public.\"STEPS\" as stp ON a.\"StepId\"=stp.\"Id\"  INNER JOIN public.\"ROBOT\" as rbt ON stp.\"Robot_Id\"=rbt.\"Id\" where rbt.\"Id\"=" + robotId + " ";
                string ret = dbd.DbExecuteQuery(query, "select");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string createNodes(string StepId, string Location)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string values = baser.Base64Encode(StepId) + "," + baser.Base64Encode(Location) + "," + baser.Base64Encode("0") + "," + baser.Base64Encode("Admin") + "," + baser.Base64Encode(date + time) + "," + baser.Base64Encode("Admin") + "," + baser.Base64Encode(date + time) + "," + baser.Base64Encode("true") + "";
                string ret = new QueryUtil().insertTableData("public", "LinkNodes", values, "", true);
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string createNodes(string StepId, string Location, string childstepids)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string values = baser.Base64Encode(StepId) + "," + baser.Base64Encode(Location) + "," + baser.Base64Encode(childstepids) + "," + baser.Base64Encode("Admin") + "," + baser.Base64Encode(date + time) + "," + baser.Base64Encode("Admin") + "," + baser.Base64Encode(date + time) + "," + baser.Base64Encode("true") + "";
                string ret = new QueryUtil().insertTableData("public", "LinkNodes", values, "", true);
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string updateNodes(string StepId, string ChildStepId, string Location)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string ret = new QueryUtil().updateTableData("LinkNodes", "StepId|Equals|" + baser.Base64Encode(StepId), "Location|Equals|" + baser.Base64Encode(Location) + "||ChildStepIds|Equals|" + baser.Base64Encode(ChildStepId), "public", true);
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string initNodes(string StepId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().updateTableData("LinkNodes", "ChildStepId|Equals|" + StepId, "ChildStepIds|Equals|0", "public", false);
                return ret;

            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
    }
}