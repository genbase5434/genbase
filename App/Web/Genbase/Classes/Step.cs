﻿using Genbase.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Genbase.BLL;
using Genbase.Classes;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.Classes
{
    public class Step
    {
		PostgresConnect dbd =  PostgresConnect.GetInstance;
		//PostgresConnect dbd = new PostgresConnect();
        string time = DateTime.Now.ToShortTimeString();
        string date = DateTime.Now.ToShortDateString();
        LinkNode ln = new LinkNode();
        BLL.EncoderDecoderBase64 baser = new BLL.EncoderDecoderBase64();

        public string savePropertiesforSteps(string stepsactionsandproperties)
        {
            return StringHelper.success;
        }
        public string getSteps(string rid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "select * from public.\"STEPS\" s where s.\"Robot_Id\" = " + rid + " and s.\"Status\"=true ORDER BY \"Order\" asc";
                string ret = dbd.DbExecuteQuery(query, "select");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getStepType(string actionid)
        {
            string query = "select \"Type\" from public.\"ACTION\" where \"Id\"=" + actionid + "";
            return dbd.DbExecuteQuery(query, StringHelper.createtherobot);
        }
        public string getSteps(string rid, string versionid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "select * from public.\"STEPS\" s where s.\"Robot_Id\" = " + rid + " and s.\"Status\"=true and s.\"Version\"=" + versionid + " ORDER BY \"Order\" asc";
                string ret = dbd.DbExecuteQuery(query, "select");
                List<Steps> stps = JsonConvert.DeserializeObject<List<Steps>>(ret);
                foreach (Steps stp in stps)
                {
                    stp.Type = getStepType(stp.Action_Id);
                    stp.StepProperties = JsonConvert.DeserializeObject<List<StepProperties>>(getStepProperties(stp.Id));
                }

                ret = JsonConvert.SerializeObject(stps);
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getStepProperties(string sid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "select * from public.\"STEPPROPERTIES\" s where s.\"Steps_Id\" = " + sid + " order by \"Order\"";
                string ret = dbd.DbExecuteQuery(query, "select");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getStepbyId(string sid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "select * from public.\"STEPS\" s where s.\"Id\" = " + sid + " order by \"Id\"";
                string ret = dbd.DbExecuteQuery(query, "select");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string setSteps(string stepname, string rid, string wid, string eid, string aid, string order, string runtimeinput, string version, string Project_Id, string CreateBy, string UpdateBy)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = "";

                string values = baser.Base64Encode(stepname) + "," + baser.Base64Encode(rid) + "," + baser.Base64Encode(eid) + "," + baser.Base64Encode(aid) + "," + baser.Base64Encode("5") + "," + baser.Base64Encode(CreateBy) + "," + baser.Base64Encode(date + " " + time) + "," + baser.Base64Encode(UpdateBy) + "," + baser.Base64Encode(date + " " + time) + "," + baser.Base64Encode("true") + "," + baser.Base64Encode(order) + "," + baser.Base64Encode(runtimeinput) + "," + baser.Base64Encode(version) + "," + baser.Base64Encode(Project_Id);
                ret = new QueryUtil().insertTableData("public", "STEPS", values, "Id", true);

                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }

        public string setStepProperties(string stepvalue, int stepid, string projectid, string stepproperty, string steppropertyvalue, string spropertytype, string order, string customvalidation, string CreateBy, string UpdateBy)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = string.Empty;
                string values = baser.Base64Encode(stepid.ToString()) + "," + baser.Base64Encode(stepproperty) + "," + baser.Base64Encode(steppropertyvalue) + "," + baser.Base64Encode(spropertytype) + ", " + baser.Base64Encode(projectid) + "," + baser.Base64Encode(CreateBy) + "," + baser.Base64Encode(date + " " + time) + "," + baser.Base64Encode(UpdateBy) + "," + baser.Base64Encode(date + " " + time) + "," + baser.Base64Encode("true") + "," + baser.Base64Encode(order) + "," + baser.Base64Encode(customvalidation);
                ret += new QueryUtil().insertTableData("public", "STEPPROPERTIES", values, "", true);
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string setStepProperties(string stepProperty, string stepvalue, int stepid, string projectid, string oldstepvalue, string newstepvalue, string spropertytype, string order)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = string.Empty;
                string values = baser.Base64Encode(stepid + ToString()) + "," + baser.Base64Encode(stepProperty) + "," + baser.Base64Encode(stepvalue) + "," + baser.Base64Encode(spropertytype) + ", " + baser.Base64Encode(projectid) + "," + baser.Base64Encode("Admin") + "," + baser.Base64Encode(date + " " + time) + "," + baser.Base64Encode("Admin") + "," + baser.Base64Encode(date + " " + time) + "," + baser.Base64Encode("true") + "," + baser.Base64Encode(order);
                ret += new QueryUtil().insertTableData("public", "STEPPROPERTIES", values, "", true);
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getAllSteps()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("STEPS", "", "*", 0, "public", "\"Id\"", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getAllStepProperties()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = new QueryUtil().getTableData("STEPPROPERTIES", "", "*", 0, "public", "\"Id\"", "");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string updateStepProperties(string stepid, string ID, string steppropertyvalue)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = "";
                ret = new QueryUtil().updateTableData("STEPPROPERTIES", "Steps_Id|Equals|" + baser.Base64Encode(stepid) + "||Id|Equals|" + baser.Base64Encode(ID), "StepPropertyValue|Equals|" + baser.Base64Encode(steppropertyvalue), "public", true);
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string updateSteps(string isDelete, string ID, string stepname, string rid, string eid, string aid, string wid, string order)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = string.Empty;
                if (isDelete.ToLower() == StringHelper.truee)
                {
                    ret = new QueryUtil().updateTableData("STEPS", "Id|Equals|" + ID, "Robot_Id|Equals|0||Element_Id|Equals|0||Action_Id|Equals|0||Workflow_Id|Equals|0", "public", false);
                }
                else
                {
                    ret = new QueryUtil().updateTableData("STEPS", "Id|Equals|" + ID, "Name|Equals|" + stepname + "||Robot_Id|Equals|" + rid + "||Element_Id|Equals|" + eid + "||Action_Id|Equals|" + aid + "||Order|Equals|" + order, "public", false);
                }
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string getStepsStepProperties(string rid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string tempResult = ln.getStepsLinks(rid);
                List<Dictionary<String, String>> Steps = new List<Dictionary<string, string>>();
                Steps = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(tempResult);
               

                for (var i = 0; i < Steps.Count; i++)
                {

                    Steps[i][StringHelper.sproperties] = getStepProperties(Steps[i][StringHelper.stepid]);
                }
                string result = JsonConvert.SerializeObject(Steps);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string deleteSteps(string rid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = string.Empty;
                string query = "DELETE from public.\"STEPPROPERTIES\" WHERE \"Steps_Id\" IN (SELECT \"Id\" FROM public.\"STEPS\" WHERE  \"Robot_Id\"=" + rid + "); \n DELETE from public.\"LinkNodes\" WHERE \"StepId\" IN (SELECT \"Id\" FROM public.\"STEPS\" WHERE  \"Robot_Id\"=" + rid + "); \n Delete from public.\"STEPS\" where \"Robot_Id\" = " + rid + "";
                ret = dbd.DbExecuteQuery(query, "update");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string deleteSteps_New(string rid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = string.Empty;
                string query = "Delete from public.\"STEPS\" where \"Robot_Id\" = " + rid + "";
                ret = dbd.DbExecuteQuery(query, "update");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string deleteStepProperties_New(string sid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = string.Empty;
                string query = "DELETE FROM public.\"STEPPROPERTIES\" WHERE \"Steps_Id\" =" + sid + "";
                ret = dbd.DbExecuteQuery(query, "update");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string deleteLinkNodes_New(string sid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = string.Empty;
                string query = "DELETE FROM public.\"LinkNodes\" WHERE \"StepId\" =" + sid + "";
                ret = dbd.DbExecuteQuery(query, "update");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string UpdateSteps(string name, string robotid, string elementid, string actionid, string workflowid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = string.Empty;
                ret = new QueryUtil().updateTableData("STEPPROPERTIES", "Id|Equals|?||Steps_Id|Equals|?||Project_Id|Equals|?", "StepPropertyValue | Equals|", "public", false);
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string UpdateStepProperties(string id, string stepid, string steppropertyvalue, string projectid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = string.Empty;
                ret = new QueryUtil().updateTableData("STEPPROPERTIES", "Id|Equals|" + baser.Base64Encode(id) + "||Steps_Id|Equals|" + baser.Base64Encode(stepid), "StepPropertyValue|Equals|" + baser.Base64Encode(steppropertyvalue), "public", true);
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string UpdateLinkNodes(string stepid, string location, string childstepids)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string ret = string.Empty;
                string query = "";
                ret = dbd.DbExecuteQuery(query, "update");
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
    }
}