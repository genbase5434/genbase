﻿using Genbase.BLL;
using Genbase.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genbase.Classes
{
    public class TableManager
    {
        Connection c = Connection.GetInstance;
        public string deleteTables(string table_id)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string status = string.Empty;
                string query = "update app_tables set status='false' where table_id=" + table_id;
                string result = new PostgresConnect().DbExecuteQuery(query, "update");
                if (result.Length > 0)
                {
                    if (result.ToLower() == "success")
                    {
                        status = "1";
                    }
                }
                return status;
            }
            catch (Exception ex)
            {
                string ec = ex.Message;
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ex.Message;
            }
        }
        public string CreateTable(string table_id, string table_name, string category, string comments, string mode, string createby, string table_columns)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            string status = string.Empty;
            List<Dictionary<string, string>> tabcontent = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(table_columns);

            List<string> columns = new List<string>();
            try
            {
                string result = "";
                if (mode == "Add")
                {
                    string query = "CREATE TABLE public.\"" + table_name + "\"(";
                    for (int i = 0; i < tabcontent.Count; i++)
                    {
                        if (tabcontent[i]["data_type_length"] == "")
                        {
                            if (i != tabcontent.Count - 1)
                            {
                                query += "\"" + tabcontent[i]["column_name"] + "\"" + " " + tabcontent[i]["data_type"] + "," + " ";
                            }
                            else
                            {
                                query += "\"" + tabcontent[i]["column_name"] + "\"" + " " + tabcontent[i]["data_type"] + " ";
                            }

                        }
                        else
                        {
                            if (i != tabcontent.Count - 1)
                            {
                                query += "\"" + tabcontent[i]["column_name"] + "\"" + " " + tabcontent[i]["data_type"] + "(" + tabcontent[i]["data_type_length"] + ")," + " ";
                            }
                            else
                            {
                                query += "\"" + tabcontent[i]["column_name"] + "\"" + " " + tabcontent[i]["data_type"] + "(" + tabcontent[i]["data_type_length"] + ")" + " ";
                            }

                        }

                        columns.Add(tabcontent[i]["column_name"]);
                    }
                    query += ");";
                    result = c.executeSQL(query, "Create");
                    if (result == "true")
                    {
                        DateTime dt = Convert.ToDateTime(DateTime.Now.ToString());
                        string query1 = "INSERT INTO public.app_tables(table_name, category, status, createby, updateby, createdatetime, updatedatetime, comments) VALUES('" + table_name + "', '" + category + "',";
                        query1 += "true,'" + createby + "', '" + createby + "', '" + dt + "', '" + dt + "', '" + comments + "') Returning table_id";

                        string t_id = new PostgresConnect().Insert_GetID(query1);
                        if (t_id != "")
                        {
                            foreach (var content in tabcontent)
                            {

                                string leng = "null";
                                if (content["data_type_length"] != "")
                                {
                                    leng = content["data_type_length"];
                                }
                                string precision = "null";
                                if (content["data_type_precision"] != "")
                                {
                                    precision = content["data_type_precision"];
                                }
                                string query2 = "INSERT INTO public.app_columns(table_id, column_name, column_description, data_type, data_type_length, data_type_precision, primary_key, mandatory, unique_key, effective_date, comments, status, createby, createdatetime, updateby, updatedatetime) ";
                                query2 += "VALUES(" + t_id + ",'" + content["column_name"] + "','" + content["column_description"] + "','" + content["data_type"] + "'," + leng + "," + precision + " ," + content["primary_key"] + "," + content["mandatory"] + "," + content["unique_key"] + ",'" + dt + "','',true,'" + createby + "','" + dt + "','" + createby + "','" + dt + "')";
                                new PostgresConnect().DbExecuteQuery(query2, "insert");
                            }
                            status = "true";
                        }
                        return status;
                    }
                }

                else if (mode == "Edit")
                {
                    foreach (var content in tabcontent)
                    {
                        string query3 = "";
                        if (content["data_type_length"] == "")
                            query3 = "ALTER TABLE \"" + table_name + "\" ADD COLUMN \"" + content["column_name"] + "\" " + content["data_type"];
                        else
                            query3 = "ALTER TABLE \"" + table_name + "\" ADD COLUMN \"" + content["column_name"] + "\"" + content["data_type"] + "(" + content["data_type_length"] + ")";

                        string AlterResult = new PostgresConnect().DbExecuteQuery(query3, "insert");
                        string FinalResult = "false";
                        if (AlterResult == "Success")
                        {
                            string leng = "null";
                            if (content["data_type_length"] != "")
                            {
                                leng = content["data_type_length"];
                            }
                            string precision = "null";
                            if (content["data_type_precision"] != "")
                            {
                                precision = content["data_type_precision"];
                            }
                            DateTime dt = Convert.ToDateTime(DateTime.Now.ToString());
                            string query2 = "INSERT INTO public.app_columns(table_id, column_name, column_description, data_type, data_type_length, data_type_precision, primary_key, mandatory, unique_key, effective_date, comments, status, createby, createdatetime, updateby, updatedatetime) ";
                            query2 += "VALUES(" + table_id + ",'" + content["column_name"] + "','" + content["column_description"] + "','" + content["data_type"] + "'," + leng + "," + precision + " ," + content["primary_key"] + "," + content["mandatory"] + "," + content["unique_key"] + ",'" + dt + "','',true,'" + createby + "','" + dt + "','" + createby + "','" + dt + "')";
                            FinalResult = new PostgresConnect().DbExecuteQuery(query2, "insert");
                        }
                        result = FinalResult;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                string ec = ex.Message;
                return "false";
            }
        }
        public List<string> viewtable(string tablename)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            List<string> objList = new List<string>();
            try
            {
                string query_col = " SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS where table_name   = '" + tablename + "'";
                string result_col = new PostgresConnect().DbExecuteQuery(query_col, "select");

                string str_col = "";
                List<TableInfoModel> objListTb = new PostgresConnect().getTableInfo(tablename);
                foreach (TableInfoModel tb in objListTb)
                {
                    if (tb.column_datatype.ToLower() == "timestamp without time zone")
                    {
                        if (str_col.Trim().Length == 0)
                        {
                            str_col = "to_char(\"" + tb.table_column + "\",'YYYY/MM/DD HH:MI:SS') as \"" + tb.table_column + "\"";
                        }
                        else
                        {
                            str_col = str_col + ",to_char(\"" + tb.table_column + "\",'YYYY/MM/DD HH:MI:SS') as \"" + tb.table_column + "\"";
                        }
                    }
                    else
                    {
                        if (str_col.Trim().Length == 0)
                        {
                            str_col = "\"" + tb.table_column + "\"";
                        }
                        else
                        {
                            str_col = str_col + ",\"" + tb.table_column + "\"";
                        }
                    }

                }

                string query = string.Empty;
                if (str_col.Trim().Length == 0)
                {
                    query = "SELECT * from public.\"" + tablename + "\"";
                }
                else
                {
                    query = "SELECT " + str_col + " from public.\"" + tablename + "\"";
                }
                //string query = "SELECT * from public.\"" + tablename + "\"";
                string result = new PostgresConnect().DbExecuteQuery(query, "select");

                objList.Add(result_col);
                objList.Add(result);
                return objList;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                string ec = ex.Message;
                return null;
            }
        }
        public string CheckTableName(string tablename)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string status = string.Empty;
                string query = "select EXISTS(SELECT 1 FROM information_schema.tables where table_name='" + tablename + "') ;";
                string result = new PostgresConnect().DbExecuteQuery(query, "select");
                List<Dictionary<string, string>> SchemaResult = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(result);
                if (SchemaResult[0]["exists"] != "true")
                {
                    string query1 = "update app_tables SET status=false where table_name='" + tablename + "';";
                    string result1 = new PostgresConnect().DbExecuteQuery(query1, "update");
                    return "true";
                }
                else
                {
                    string query2 = "select count(*) from app_tables where table_name='" + tablename + "' and status=true";
                    string result1 = new PostgresConnect().DbExecuteQuery(query2, "select");
                    List<Dictionary<string, int>> MetaDataResult = JsonConvert.DeserializeObject<List<Dictionary<string, int>>>(result1);
                    if (MetaDataResult[0]["count"] > 0)
                    {
                        return "false";
                    }
                    else
                    {
                        return "handle";
                    }
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                string ec = ex.Message;
                return "false";
            }
        }
    }
    public class TableInfoModel
    {
        public string table_column { get; set; }
        public string column_datatype { get; set; }
    }
}
