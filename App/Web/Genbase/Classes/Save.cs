﻿using Genbase.BLL;
using Genbase.Classes;
using Genbase.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using static Genbase.BLL.StepPropertyHandler;

namespace Genbase.classes
{
    public class Save
    {
        Robot cr = new Robot();
        Project cp = new Project();
        Workflow cw = new Workflow();
        Step cs = new Step();
        Entities entities = new Entities();
        DAL.PostgresConnect dbd = new DAL.PostgresConnect();
        LinkNode aln = new LinkNode();
        ExecutionLog cel = new ExecutionLog();
        Variables cv = new Variables();
        Predefined cpd = new Predefined();
        string time = DateTime.Now.ToShortTimeString();
        string date = DateTime.Now.ToShortDateString();

        public string SaveRobot(Entities inputString)
        {

            Robots robot = inputString.robot;
            //string rst=new cRobot().updateRobotProperties(robot.RProperties);

            List<Steps> stps = JsonConvert.DeserializeObject<List<Steps>>(cr.getRobotsStepsStepProps(robot.Id));
            List<string> vstep = new List<string>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                bool validate = true;
                List<string> ValidationList = new List<string>();

                List<List<StepProperties>> stoik = inputString.robot.Steps.Select(stp => new StepPropertyValidationHandler().ValidateProperties(stp, true)).ToList();
                List<Steps> stepsValidation = robot.Steps.FindAll(rts => rts.Action_Id != "1" && rts.Action_Id != "2");
                if (stepsValidation.Count <= 0)
                {
                    return StringHelper.robotnotsavedaddanothersteps;
                }

                foreach (List<StepProperties> stk in stoik)
                {
                    if (stk.Count > 0 && robot.RobotType != "WinDows")
                    {
                        validate = false;
                        foreach (StepProperties stepproperty in stk)
                        {
                            var fe = robot.Steps.Find(stp => stp.Id == stepproperty.Steps_Id);
                            ValidationList.Add("please check " + fe.Name + " " + stepproperty.StepProperty + " \n");
                        }
                    }
                    else if (robot.RobotType == "WinDows")
                    {
                        robot.LinkNodes = new List<LinkNodes>();
                    }
                }
                if (validate != true)
                {
                    return StringHelper.robotnotsavedfilltheproperties + string.Join(" , ", ValidationList.ToArray());
                }

                if (validate != false)
                {

                    List<Versions> versions = JsonConvert.DeserializeObject<List<Versions>>(getVersion(robot.Id));
                    int version = 0;


                    if (versions.Count > 0)
                    {
                        //update version of robot

                        string latestvid = versions[versions.Count - 1].versionid;
                        version = int.Parse(latestvid.Trim());

                        List<Robots> tobors = JsonConvert.DeserializeObject<List<Robots>>(new Robot().getRobots(robot.Project_Id, robot.Id));

                        if (checkVersionSteps(robot, version))
                        {
                            UpdateRobot(inputString);
                            return StringHelper.robotupdated;
                        }
                        else
                        {
                            //createds the version
                            saveSteps(robot, version);
                            return StringHelper.stepssaved;
                        }

                    }
                    else
                    {
                        //create version / new robot
                        saveSteps(robot, version);
                        return StringHelper.versioncreated;
                    }
                    //return "Steps Saved";
                }
                else
                {
                    return StringHelper.robotnotsavedfilltheproperties;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string UpdateRobot(Entities entity)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                foreach (Steps step in entity.robot.Steps)
                {
                    string stepId = string.Empty;
                    if (step.Id != null && int.Parse(step.Id) > 0)
                    {
                        foreach (StepProperties stepproperty in step.StepProperties)
                        {
                            string steppropId = cs.UpdateStepProperties(stepproperty.Id, stepproperty.Steps_Id, stepproperty.StepPropertyValue, stepproperty.Project_Id);
                        }
                    }
                }
                foreach (LinkNodes linknode in entity.robot.LinkNodes)
                {

                    if (linknode.StepId != null && int.Parse(linknode.StepId) > 0)
                    {

                        if (linknode.Id != null && linknode.ChildStepIds.Contains(',')/*&&result!=null*/)
                        {

                            string childc = string.Empty;

                            if (linknode.StepId != string.Empty)
                            {
                                string htr = aln.updateNodes(linknode.StepId, "", linknode.Location);
                            }
                        }
                        else
                        {
                            if (linknode.StepId != string.Empty)
                            {
                                string htr = aln.updateNodes(linknode.StepId, linknode.ChildStepIds, linknode.Location);
                            }
                        }
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string SaveProject(Entities entity)
        {

            return string.Empty;
        }
        public string setVersion(string rid, string rflag)
        {
            List<Versions> versions = JsonConvert.DeserializeObject<List<Versions>>(getVersion(rid));
            string latestvid = versions[versions.Count - 1].versionid;

            string values = rid + "," + latestvid + "," + rflag + ",kk," + date + " " + time + ",mm," + date + " " + time + ",true";
            string ret = new QueryUtil().insertTableData("public", "Versions", values, "ID", false);
            return ret;
        }

        public string setVersion(string rid, string rflag, string version)
        {
            string values = rid + "," + version + "," + rflag + ",kk," + date + " " + time + ",mm," + date + " " + time + ",true";
            string ret = new QueryUtil().insertTableData("public", "Versions", values, "", false);
            return ret;
        }

        public string getVersion(string robotid)
        {
            string ret = new QueryUtil().getTableData("Versions", "rid|Equals|" + robotid + "||rflag|Equals|1||Status|Equals|true", "*", 0, "", "", "");
            return ret;
        }
        public bool saveSteps(Robots robot, int version)
        {
            version = version + 1;
            Variables.Project_Id = robot.Project_Id;
            List<string> lstep = new List<string>();
            Dictionary<string, string> orderid = new Dictionary<string, string>();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                //steps and stepproperties
                foreach (Steps step in robot.Steps)
                {
                    string stepId = string.Empty;
                    stepId = (-(int.Parse(step.Id))).ToString();
                    //maintain positive order
                    stepId = cs.setSteps(step.Name, step.Robot_Id, step.Workflow_Id, step.Element_Id, step.Action_Id, stepId, step.RuntimeUserInput, version.ToString(), Variables.Project_Id, step.CreateBy, step.UpdateBy);
                    orderid.Add(step.Id, stepId);
                    foreach (StepProperties stepproperty in step.StepProperties)
                    {
                        string steppropId = cs.setStepProperties(step.Name, int.Parse(stepId), stepproperty.Project_Id, stepproperty.StepProperty, stepproperty.StepPropertyValue, stepproperty.StepPropertyType, stepproperty.Order, stepproperty.CustomValidation, stepproperty.CreateBy, stepproperty.UpdateBy);
                    }
                    lstep.Add(stepId);
                }
                //links
                foreach (LinkNodes linknode in robot.LinkNodes)
                {
                    //new linknodes
                    if (linknode.ChildStepIds != null && linknode.ChildStepIds.Contains('`'))
                    {
                        string[] childs = linknode.ChildStepIds.Split('`'); string childc = string.Empty;
                        foreach (string child in childs)
                        {
                            if (orderid.ContainsKey(child))
                                childc = childc + orderid[child] + "`";
                        }
                        if (orderid.ContainsKey(linknode.StepId))
                        {
                            aln.createNodes(orderid[linknode.StepId], linknode.Location, childc);
                        }
                    }
                    else
                    {
                        if (orderid.ContainsKey(linknode.StepId))
                        {
                            aln.createNodes(orderid[linknode.StepId], linknode.Location);
                        }
                    }
                }

                setVersion(robot.Id, "1", version.ToString());

                return true;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return false;
            }
        }

        public bool checkVersionSteps(Robots robot, int version)
        {
            bool StepComparisionStatus = false;
            List<Steps> prevStps = JsonConvert.DeserializeObject<List<Steps>>(new Step().getSteps(robot.Id, version.ToString()));
            List<Steps> currSteps = robot.Steps;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                if (currSteps.Count != prevStps.Count)
                {
                    return false;
                }
                else if (currSteps.Count == prevStps.Count)
                {
                    foreach (Steps r in currSteps)
                    {
                        StepComparisionStatus = false;
                        foreach (Steps f in prevStps)
                        {
                            if (r.Id == f.Id)
                            {
                                StepComparisionStatus = true;
                            }
                        }
                        if (!StepComparisionStatus)
                        {
                            return false;
                        }
                    }
                    return StepComparisionStatus;
                }
                else
                {
                    return false;
                }
            }

            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return false;
            }
        }

        public class DBWARobot
        {
            string time = DateTime.Now.ToShortTimeString();
            string date = DateTime.Now.ToShortDateString();
            public string saveWARobot(string rid, string wasteps)
            {
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    string result = string.Empty;

                    List<WARobot> war = JsonConvert.DeserializeObject<List<WARobot>>(new QueryUtil().getTableData("WASteps", "RID|Equals|" + rid, "*", 0, "public", "", ""));

                    if (war.Count > 0)
                    {
                        string values = rid + "," + wasteps + ",Admin," + date + " " + time + ",Admin," + date + " " + time + ",true";
                        result = new QueryUtil().updateTableData("WASteps", "RID|Equals|" + rid, "StepsXML" + values, "public", false);
                    }
                    else
                    {
                        string values = rid + "," + wasteps + ",Admin," + date + " " + time + ",Admin," + date + " " + time + ",true";
                        result = new QueryUtil().insertTableData("public", "WASteps", values, "ID", false);
                    }

                    return result;
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                    }
                    else
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
                }
            }
            public string getWARobot(string pid)
            {
                List<WARobot> wars = new List<WARobot>();
                List<Robots> robots = JsonConvert.DeserializeObject<List<Robots>>(new Robot().getRobots(pid));
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    foreach (Robots robot in robots)
                    {
                        List<WARobot> war = JsonConvert.DeserializeObject<List<WARobot>>(new QueryUtil().getTableData("WASteps", "RID|Equals|" + robot.Id, "*", 0, "public", "", ""));
                        war[0].Name = robot.Name;
                        war[0].projectID = robot.Project_Id;
                        war[0].Description = robot.Description;
                        wars.Add(war[0]);
                    }

                    return JsonConvert.SerializeObject(wars);
                }
                catch (Exception ex)
                {
                    new Logger().LogException(ex);
                    if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                    }
                    else
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
                }
            }
            public string getWARobotI(string rid)
            {
                List<WARobot> wars = new List<WARobot>();

                List<Robots> robots = JsonConvert.DeserializeObject<List<Robots>>(new Robot().getRobotI(rid));
                List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
                try
                {
                    foreach (Robots robot in robots)
                    {
                        List<WARobot> war = JsonConvert.DeserializeObject<List<WARobot>>(new QueryUtil().getTableData("WASteps", "RID|Equals|" + robot.Id, "*", 0, "public", "", ""));
                        if (war != null && war.Count > 0)
                        {
                            war[0].Name = robot.Name;
                            war[0].projectID = robot.Project_Id;
                            war[0].Description = robot.Description;
                            wars.Add(war[0]);
                        }
                    }

                    return JsonConvert.SerializeObject(wars);
                }
                catch (Exception ex)

                {

                    new Logger().LogException(ex);
                    if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                    }
                    else
                    {
                        msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                    }
                    return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
                }
            }
        }

        public class Versions
        {
            public string ID { get; set; }
            public string rid { get; set; }
            public string versionid { get; set; }
            public string rflag { get; set; }
        }
    }
}