﻿using Genbase.BLL;
using Genbase.DAL;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using Genbase.Classes;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.classes
{
    public class Widget
    {
        BLL.EncoderDecoderBase64 baser = new BLL.EncoderDecoderBase64();
        Connection c = Connection.GetInstance;
        public int checkWidget(int ElementId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string query = "select count(\"Id\") from \"WidgetTable\" where \"ElementId\"='" + ElementId + "'";
                string result = c.executeSQL(query, "Select");
                JArray obj = JArray.Parse(result);
                return Convert.ToInt32(obj[0]["count"]);
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return 1;
            }
        }
        public string getWidget(int ElementId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string result = new QueryUtil().getTableData("WidgetTable", "ElementId|Equals|" + ElementId + "", "\"WidgetType\",\"query\",\"Mandatory\",\"Length\"", 0, "public", "", "");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string addWidget(string ColumnDetails)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                JObject Columns = JObject.Parse(ColumnDetails);

                if (Columns["query"].ToString() != "")
                    Columns["query"] = Columns["query"].ToString().Replace("'", "''");
                string values = baser.Base64Encode(Columns["ElementId"].ToString()) + "," + baser.Base64Encode(Columns["WidgetType"].ToString()) + ", " + baser.Base64Encode(Columns["query"].ToString()) + "," + baser.Base64Encode(Columns["Mandatory"].ToString()) + "," + baser.Base64Encode(Columns["Length"].ToString()) + "," + Columns["CreateBy"] + "," + Columns["CreateDatetime"] + "," + Columns["UpdateBy"] + "," + Columns["UpdateDatetime"] + "," + Columns["Status"];
                string result = new QueryUtil().insertTableData("public", "WidgetTable", values, "", true);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string updateWidget(string ColumnDetails)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                JObject Columns = JObject.Parse(ColumnDetails);

                string result = new QueryUtil().updateTableData("WidgetTable", "ElementId|Equals|" + Columns["ElementId"], "WidgetType|Equals|" + Columns["WidgetType"] + "||query|Equals|" + Columns["query"] + "||Mandatory|Equals|" + Columns["Mandatory"].ToString() + "||Length|Equals|" + Columns["Length"], "public", false);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
    }
}