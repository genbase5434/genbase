﻿using ezBot.classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using ezBot.DAL;
using static ezBot.Classes.Handler;
using ezBot.Classes;

namespace ezBot.BLL
{
    public class APILayer
    {
        PostgresConnect dbd = new PostgresConnect(); ExecutionLog logs = new ExecutionLog();
        public string apiLogic(string projectid, string robotid, bool isInProgress, string inDex)
        {
            string result = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Step cs = new Step();
                Variables.robotid = robotid;
                string stepstring = cs.getSteps(robotid);
                logs.deleteFromLogs(robotid);
                List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(stepstring);

                if (steps.Count > 0)
                {
                    foreach (Steps step in steps)
                    {
                        step.StepProperties = JsonConvert.DeserializeObject<List<StepProperties>>(cs.getStepProperties(step.Id));
                    }

                    if (isInProgress == false)
                        result = stepsExecution(steps, false, "", int.Parse(inDex));
                    else if (isInProgress == true)
                        result = stepsExecution(steps, false, "", int.Parse(inDex));
                }
                else
                {
                    result = StringHelper.nosteps;
                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return result;
            }
        }

        public string stepsExecution(List<Steps> steps, bool isgotoMode, string InputValue, int stepState)
        {
            string js = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string robotnum = Variables.robotid;
                LinkNode c = new LinkNode();
                List<LinkNodes> LinkNode = JsonConvert.DeserializeObject<List<LinkNodes>>(c.getStepsLinks(robotnum));

                bool checkStart = steps.Any(x => x.Name.ToLower() == StringHelper.start);
                bool checkStop = steps.Any(x => x.Name.ToLower() == StringHelper.stop);

                if (checkStart == true && checkStop == true)
                {
                    if (LinkNode != null && LinkNode.Count > 0)
                    {

                        LinkNode.OrderBy(o => int.Parse(o.Id));
                        var firstElement = LinkNode.First();
                        var il = (from j in steps where j.Name.ToLower() == StringHelper.start select j).ToList();
                        if (stepState != 0)
                        {
                            il = (from j in steps where j.Id == stepState.ToString() select j).ToList();
                        }

                        while (il != null && il.Count > 0)
                        {
                            var result = LinkNode.Find(item => item.StepId == il[0].Id);
                            string xchild = string.Empty;
                            if (result != null) xchild = result.ChildStepIds;

                            try
                            {
                                cElement cde = new cElement(); Classes.Action ca = new Classes.Action();
                                string elemName = cde.getElementById(il[0].Element_Id);
                                string actName = ca.getAction(il[0].Action_Id);

                                Type type = Type.GetType(StringHelper.ExecutionClassTypes + elemName);
                                object instance = Activator.CreateInstance(type);
                                MethodInfo method = type.GetMethod(actName);
                                if (il[0].RuntimeUserInput.ToLower() == StringHelper.truee)
                                {
                                    object[] paramss = new object[3] { il[0].StepProperties, il[0].Name, il[0] };
                                    MethodResult mr = new MethodResult();
                                    mr = (MethodResult)method.Invoke(instance, paramss);

                                    if (xchild.IndexOf(',') > -1)
                                    {
                                        xchild = xchild.Split(',')[0];
                                    }
                                    Inprogress progress = new Inprogress();
                                    progress.inDex = xchild;
                                    progress.Status = StringHelper.inprogress;
                                    progress.step = il[0];

                                    return js = JsonConvert.SerializeObject(progress);
                                }
                                else if (Variables.exeStatus)
                                {
                                    object[] paramss = new object[3] { il[0].StepProperties, il[0].Name, il[0] };
                                    MethodResult mr = new MethodResult();
                                    mr = (MethodResult)method.Invoke(instance, paramss);
                                    Variables.exeStatus = false;

                                    Inprogress progress = new Inprogress();
                                    progress.Status = StringHelper.inprogress;
                                    progress.step = il[0];

                                }
                                else
                                {
                                    object[] paramss = new object[3] { il[0].StepProperties, il[0].Name, il[0] };
                                    MethodResult mr = new MethodResult();
                                    mr = (MethodResult)method.Invoke(instance, paramss);
                                    if (mr.result == false)
                                    {
                                        return js = StringHelper.failure;
                                    }
                                    else
                                    {
                                        js = StringHelper.success;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                new Logger().LogException(ex);
                                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                            }
                            if (xchild.IndexOf(',') > -1)
                            {
                                xchild = xchild.Split(',')[0];
                                il = (from j in steps where j.Id == xchild select j).ToList();
                            }
                            else
                            {
                                il = null;
                            }
                        }
                    }
                }
                else
                {
                    new ExecutionLog().add2Log(StringHelper.nostartorstop, robotnum, StringHelper.zero, StringHelper.exceptioncode);
                    return js = StringHelper.failure;
                }
                //here added to make it clear the previous details
                Variables.downloadattachments.Clear(); Variables.isSocial.Clear(); Variables.readOutputupdate.Clear(); Variables.classifiedData.Clear();
                Variables.fbOutput.Clear(); Variables.fbOutputUpdate.Clear();
                return js;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return js;
            }
        }
    }

}