﻿using Genbase.BLL;
using Genbase.Classes;
using Genbase.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Genbase.classes
{
    public class VariableClass
    {
        DAL.PostgresConnect dbd = new DAL.PostgresConnect();
        BLL.EncoderDecoderBase64 baser = new BLL.EncoderDecoderBase64();
        public string setVariables(DVariables variable, string rid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                bool chck = checkVariables(variable, rid);
                if (chck == true)
                {
                    string values = variable.vlname + "," + variable.vlvalue + "," + variable.vltype + "," + variable.vlstatus + "," + variable.vlscope + ",postgres," + DateTime.Now.ToString() + ",postgres," + DateTime.Now.ToString() + ",true," + rid + "," + variable.sheetname ;
                    return new QueryUtil().insertTableData("public", "Variables", values, " ", false);
                }
                else
                {
                    string feedback = "false";
                    return feedback;
                    
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new BLL.MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new BLL.MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public bool checkVariables(DVariables variable, string rid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string xc = new QueryUtil().getTableData("Variables", "Name|Equals|" + variable.vlname + "||Robot_Id|Equals|" + rid + "||Status|Equals|true", "*", 0, "public", "", "");
                List<FVariables> lf = JsonConvert.DeserializeObject<List<FVariables>>(xc);
                if (lf.Count > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new BLL.MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new BLL.MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return false;
            }
        }
        public string getVariables(string rid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "select * FROM public.\"Variables\" where \"Robot_Id\" = " + rid + " and \"Status\"=true ";
                return dbd.DbExecuteQuery(query, "select");
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new BLL.MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new BLL.MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string updateVariables(DVariables variable, string rid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
             //string values = variable.vlname + ", " + variable.vlvalue + ", " + variable.vltype + ", " + variable.vlstatus + ", " + variable.vlscope + ",postgres," + DateTime.Now.ToString() + ",postgres," + DateTime.Now.ToString() + ", true," + rid , vid;
             return new QueryUtil().updateTableData("Variables", "Name|Equals|" + baser.Base64Encode(variable.vlname) +  "||Robot_Id|Equals|" + baser.Base64Encode(rid), "Name|Equals|" + baser.Base64Encode(variable.vlname) + "||Value|Equals|" + baser.Base64Encode(variable.vlvalue) + "||Type|Equals|" + baser.Base64Encode(variable.vltype) + "||SheetName|Equals|" + baser.Base64Encode(variable.sheetname) + "||Status|Equals|" + baser.Base64Encode(variable.vlstatus) + "||Scope|Equals|" + baser.Base64Encode(variable.vlscope), "public", true);
                
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new BLL.MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new BLL.MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string DeleteVariables(DVariables variable, string rid )
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
          {
                string query = "UPDATE public.\"STEPPROPERTIES\" SET \"StepPropertyValue\" =' ' where \"StepPropertyValue\" = '"+ variable.vlname + "'  and \"Status\"=true ";
                string result = dbd.DbExecuteQuery(query, "update");
                return new QueryUtil().updateTableData("Variables", "Name|Equals|" + baser.Base64Encode(variable.vlname) + "||Robot_Id|Equals|" + baser.Base64Encode(rid), "Status|Equals|"+ baser.Base64Encode("false"), "public", true);                
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new BLL.MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new BLL.MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
    }
}