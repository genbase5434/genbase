﻿using ezBot.classes;
using ezBot.Classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Reflection;
using static ezBot.Classes.Handler;

namespace ezBot.BLL
{
    public class Validator
    {
        AppExecution ape = new BLL.AppExecution();
        Step cs = new Step();
        public string ValidateStep(Steps stp)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                cElement cde = new cElement();
                Classes.Action ca = new Classes.Action();
                string elemName = cde.getElementById(stp.Element_Id);
                string actName = ca.getAction(stp.Action_Id);
                Type type = Type.GetType("ezBot.BLL.ElementsClasses." + elemName);
                object instance = Activator.CreateInstance(type);
                MethodInfo method = type.GetMethod(actName);

                object[] paramss = new object[3] { stp.StepProperties, stp.Name, stp };
                MethodResult mr = new MethodResult();
                mr = (MethodResult)method.Invoke(instance, paramss);
                if (mr.result == false)
                {
                    return StringHelper.failure;
                }
                else
                {
                    return StringHelper.success;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                new ExecutionLog().add2Log(ex.Message, stp.Robot_Id, stp.Name, StringHelper.exceptioncode);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return StringHelper.failed;
            }
        }
        public string DebugStep(int robotId, int stepid)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                int Stepid = ape.getNextStep(robotId, stepid, "");
                if (Stepid <= 0)
                {
                    return string.Empty;
                }
                Variables.vDebugger.ID = Stepid.ToString();
                string vs = ape.getVersion(robotId.ToString());
                Steps step = new Steps();
                List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(cs.getSteps(robotId.ToString(), vs));
                if (steps.Count > 0)
                {
                    if (Stepid > 0)
                    {
                        step = steps.Find(c => c.Id == Stepid.ToString());
                    }
                }
                ValidateStep(step);
                return Stepid.ToString();
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return StringHelper.failed;
            }
        }

        // SAP
        public MethodResult StepExecution(int robotId, int stepid)
        {
            MethodResult mr = new MethodResult();
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                int Stepid = stepid;
                Variables.vDebugger.ID = Stepid.ToString();
                Variables.robotid = robotId.ToString();
                string vs = ape.getVersion(robotId.ToString());

                Steps step = new Steps();
                List<Steps> steps = JsonConvert.DeserializeObject<List<Steps>>(cs.getSteps(robotId.ToString(), vs));
                if (steps.Count > 0)
                {
                    if (Stepid > 0)
                    {
                        step = steps.Find(c => c.Id == Stepid.ToString());
                    }
                }

                cElement cde = new cElement();
                Classes.Action ca = new Classes.Action();
                string elemName = cde.getElementById(step.Element_Id);
                string actName = ca.getAction(step.Action_Id);
                Type type = Type.GetType("ezBot.BLL.ElementsClasses." + elemName);
                object instance = Activator.CreateInstance(type);
                MethodInfo method = type.GetMethod(actName);

                object[] paramss = new object[3] { step.StepProperties, step.Name, step };

                mr = (MethodResult)method.Invoke(instance, paramss);
                return mr;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                mr.result = false;
                return mr;
            }
        }
    }
}