﻿using Genbase.BLL;
using Genbase.classes;
using Genbase.DAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Genbase.Classes
{
    public class Login
    {
        Connection c = Connection.GetInstance;
        public string Authentication(string username, string pwd)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = new QueryUtil().getTableData("USER", "UserName|Equals|" + username + "||Password|Equals|" + pwd + "", "*", 0, "public", "", "");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                }
                return ex.Message;
            }
        }
        public string Authentication(string user)
        {
            List<UserResult> userresult = new List<UserResult>(); string returnValue = string.Empty;
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string usere = Base64Decode(user).Split(new string[] { "||" }, StringSplitOptions.None)[0];
                string passer = Base64Decode(user).Split(new string[] { "||" }, StringSplitOptions.None)[1];
                string query = "select \"ID\" as \"User_ID\",\"FirstName\",\"LastName\",\"Email_ID\",\"ContactNo\",\"UserName\",\"Password\",\"Role_ID\",\"CreateBy\",\"CreateDatetime\",\"UpdateBy\",\"UpdateDatetime\",\"Imagesrc\",\"Status\",\"LOB\" from public.\"USER\" where LOWER(\"UserName\")='" + usere.ToLower() + "' and \"Password\"='" + passer + "' order by 1;";
                string result = c.executeSQL(query, "Select");
                List<UserLogon> users = JsonConvert.DeserializeObject<List<UserLogon>>(result);

                UserResult ur = new UserResult();
                if (users.Count > 0)
                {
                    if (users.Count > 1)
                    {
                        ur.Message = "Duplicate Users";
                        ur.User = new List<UserLogon>();
                        ur.User.AddRange(users);
                    }
                    else
                    {
                        ur.Message = "Success";
                        ur.User = users;
                        Crud _obCrud = new Crud();
                    }
                }
                else
                {
                    string result1 = new QueryUtil().getTableData("USER", "UserName|Equals|" + usere, "*", 0, "public", "", "");
                    List<UserLogon> users1 = JsonConvert.DeserializeObject<List<UserLogon>>(result1);
                    if (users1.Count > 0)
                    {
                        ur.Message = "User Exists But Password Wrong";
                        ur.User = users;
                    }
                    else
                    {
                        ur.Message = "User Doesn't Exists";
                        ur.User = users;
                    }
                }
                List<UserResult> urs = new List<UserResult>();
                urs.Add(ur);

                returnValue = JsonConvert.SerializeObject(ur);

                return returnValue;
            }
            catch (Exception ex)
            {
                new Logger().LogException("Login Exception" + ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                }
                return ex.Message;
            }
        }

        public static void Connective_information()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                List<List<string>> finalResult = new List<List<string>>();
                List<Dictionary<string, string>> success = new List<Dictionary<string, string>>();
                List<Dictionary<string, string>> failed = new List<Dictionary<string, string>>();
                List<int> temp = new List<int>();
                bool endResult;

                Connection c = new Connection();
                string query = "Select d.*,t.\"Name\" as \"DBName\" from \"DBConnection\" d inner join \"DBType\" t on t.\"ID\" = d.\"DBType\" and t.\"Status\"=true and d.\"Status\"=true";
                string result = c.executeSQL(query, "Select");
                List<DBConnection> ListValues = JsonConvert.DeserializeObject<List<DBConnection>>(result);
                List<ConnectivityInfo> _objList = new List<ConnectivityInfo>();

                foreach (DBConnection d in ListValues)
                {
                    if (d.DBName == StringHelper.mysqldb)
                    {
                        MysqlConnect s = new MysqlConnect(d.Server, d.Port, d.Database, d.Username, d.Password);
                        endResult = s.OpenConnection();
                        if (endResult == true)
                        {
                            s.CloseConnection();
                        }
                        int r = Convert.ToInt32(endResult);
                        ConnectivityInfo _objCont = new ConnectivityInfo();
                        _objCont.Id = d.ID.ToString();
                        _objCont.Server = d.Server.ToString();
                        _objCont.Name = d.Name.ToString();
                        _objCont.Status = r.ToString();
                        _objList.Add(_objCont);
                    }
                    else if (d.DBName == StringHelper.postgressql)
                    {
                        Connection c1 = new Connection(d.Server, d.Port, d.Database, d.Username, d.Password);
                        endResult = c1.openDB();

                        if (endResult == true)
                        {
                            c1.closeDB();
                        }
                        int r = Convert.ToInt32(endResult);
                        ConnectivityInfo _objCont = new ConnectivityInfo();
                        _objCont.Id = d.ID.ToString();
                        _objCont.Server = d.Server.ToString();
                        _objCont.Name = d.Name.ToString();
                        _objCont.Status = r.ToString();
                        _objList.Add(_objCont);
                    }
                }

                string _sqlStr_del = "Delete from public.\"ConnectivityInfo\"";
                string _delresult = c.executeSQL(_sqlStr_del, "delete");

                foreach (ConnectivityInfo ci in _objList)
                {

                    string _sqlStr_ins = "INSERT INTO public.\"ConnectivityInfo\"(\"ID\", \"Server\", \"Name\", \"Status\")	VALUES('" + ci.Id + "', '" + ci.Server + "', '" + ci.Name + "', '" + ci.Status + "'); ";
                    string _insrtresult = c.executeSQL(_sqlStr_ins, "insert");
                }
            }
            catch (Exception ex)
            {
                string exception = ex.Message;
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                }
            }
        }

        public string userRegister(string UserDetails)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                DateTime now = DateTime.Now;
                JObject UserCredentials = JObject.Parse(UserDetails);

                string values = UserCredentials["FirstName"].ToString() + "," + UserCredentials["LastName"].ToString() + "," + UserCredentials["Email"].ToString() + "," + UserCredentials["ContactNo"].ToString() + "," + UserCredentials["Username"].ToString() + "," + UserCredentials["Password"].ToString() + "," + now.ToString("G") + ",1";
                string result = new QueryUtil().insertTableData("public", "USER", values, "", false);
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                }
                return ex.Message;
            }
        }
        public string forgetPassword(string email)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = new QueryUtil().getTableData("USER", "Email_ID|Equals|" + email + "", "\"UserName\", \"Password\"", 0, "public", "", "");
                JArray UserDetails = JArray.Parse(result);
                if (UserDetails.ToString() != "[]")
                {
                    string username = UserDetails[0][StringHelper.uname].ToString();
                    string password = UserDetails[0][StringHelper.pword].ToString();
                    MailMessage testMailMessage = new MailMessage();
                    testMailMessage.From = new MailAddress("Genbaseservices@gmail.com");
                    testMailMessage.To.Add(new MailAddress(email));
                    testMailMessage.Subject = StringHelper.passwordrecovery;
                    testMailMessage.Body = string.Format("Hi {0},<br /><br />Your password is {1}.<br /><br />Thank You.", username, password);
                    testMailMessage.IsBodyHtml = true;
                    testMailMessage.Priority = MailPriority.Normal;
                    testMailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
                    testMailMessage.BodyEncoding = System.Text.Encoding.UTF8;
                    //SmtpClient testSmtp = new SmtpClient("smtp.gmail.com", 587);
                    SmtpClient testSmtp = new SmtpClient("smtp.office365.com", 587);
                    testSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    testSmtp.UseDefaultCredentials = false;
                    testSmtp.EnableSsl = true;
                    //testSmtp.Credentials = new System.Net.NetworkCredential("Genbaseservices@gmail.com", "Genbase!@#");
                    testSmtp.Credentials = new System.Net.NetworkCredential("admin@genbase.onmicrosoft.com", "Genbase!@#");
                    testSmtp.Send(testMailMessage);
                    return StringHelper.truee;
                }
                else
                {
                    return StringHelper.ffalse;
                }
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                }
                return ex.Message;
            }
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public class UserLogon
        {
            public string User_ID { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email_ID { get; set; }
            public string ContactNO { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
            public string Role_ID { get; set; }
            public string Imagesrc { get; set; }
            public string LOB { get; set; }
        }
        public class UserResult
        {
            public string Message { get; set; }
            public List<UserLogon> User { get; set; }
        }
    }
}
