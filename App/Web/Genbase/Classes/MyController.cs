﻿using Genbase.BLL;
using Genbase.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genbase.Classes
{
    public class MyController
    {
        public string scheduleDetails(string RobotId, string SystemId)
        {
            string query = "select * from \"Scheduler\" where \"RobotId\"='" + RobotId + "' and \"SystemId\"='" + SystemId + "'";
            string result = new Connection().executeSQL(query, "Select");
            return result;
        }
        public string robotTimelineLive(string lasttimestamp)
        {
            //string query = "SELECT \"CreateDateTime\"::TimeStamp,sum(case when \"JobStatus\" = 'New' then 1 else 0 end) as \"New\",sum(case when  \"JobStatus\" = 'In Progress' then 1 else 0 end) as \"In Progress\",sum(case when  \"JobStatus\" = 'Success' then 1 else 0 end) as \"Success\",sum(case when  \"JobStatus\" = 'Failure' then 1 else 0 end) as \"Failure\"from \"ProcessManager\"  where \"CreateDateTime\"::Timestamp>'" + lasttimestamp + "' group by \"CreateDateTime\"::TimeStamp order by \"CreateDateTime\"";
            string query = "SELECT \"CreateDatetime\"::TimeStamp,sum(case when \"JobStatus\" = 'New' then 1 else 0 end) as \"New\",sum(case when  \"JobStatus\" = 'InProgress' then 1 else 0 end) as \"InProgress\",sum(case when  \"JobStatus\" = 'Success' then 1 else 0 end) as \"Success\",sum(case when  \"JobStatus\" = 'Failure' then 1 else 0 end) as \"Failure\"from \"ProcessManager\"  where \"CreateDatetime\"::Timestamp>date_trunc('week',current_date) - interval '1 week' group by \"CreateDatetime\"::TimeStamp order by \"CreateDatetime\"";
            string result = new Connection().executeSQL(query, "Select");
            return result;
        }
        public string Schedule(string details)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Dictionary<string, string> schedule = JsonConvert.DeserializeObject<Dictionary<string, string>>(details);
                Connection dbd = new Connection();
                string time = DateTime.Now.ToShortTimeString();
                string date = DateTime.Now.ToShortDateString();
                string query = "SELECT count(*) FROM public.\"Scheduler\" where \"RobotId\"='" + schedule["RobotId"] + "' and \"SystemId\"='" + schedule["SystemId"] + "' and \"Status\" = 'true' ";
                string ret = dbd.executeSQL(query, "Select");
                List<Dictionary<string, int>> c = JsonConvert.DeserializeObject<List<Dictionary<string, int>>>(ret);
                if (c[0]["count"] != 0)
                {
                    if (schedule["RepeatOn"] == "" && schedule["RepeatEvery"] == "" && schedule["End"] == "")
                    {
                        ret = new QueryUtil().updateTableData("Scheduler", "RobotId|Equals|" + schedule["RobotId"] + "||SystemId|Equals|" + schedule["SystemId"], "interval|Equals|" + schedule["interval"] + "||LastExecutionTime|Equals|" + date + " " + time + "||Startdate|Equals|" + schedule["StartDate"] + "||Starttime|Equals|" + schedule["StartTime"] + "||Tag|Equals|" + schedule["Description"] + "||Timeperiod|Equals|" + schedule["TimePeriod"] + "||Runatserver|Equals|" + schedule["Runatserver"] + "||ScheduleStatus|Equals|" + schedule["Status"] + "||Instance|Equals|" + schedule["Instance"].Replace(",", "-"), "public", false);
                    }
                    else if (schedule["RepeatOn"] == "" && schedule["RepeatEvery"] == "")
                    {
                        ret = new QueryUtil().updateTableData("Scheduler", "RobotId|Equals|" + schedule["RobotId"] + "||SystemId|Equals|" + schedule["SystemId"], "interval|Equals|" + schedule["interval"] + "||LastExecutionTime|Equals|" + date + " " + time + "||Startdate|Equals|" + schedule["StartDate"] + "||Starttime|Equals|" + schedule["StartTime"] + "||Tag|Equals|" + schedule["Description"] + "||Timeperiod|Equals|" + schedule["TimePeriod"] + "||Runatserver|Equals|" + schedule["Runatserver"] + "||End|Equals|" + schedule["End"] + "||ScheduleStatus|Equals|" + schedule["Status"], "public", false);
                    }
                    else if (schedule["RepeatOn"] == "")
                    {
                        ret = new QueryUtil().updateTableData("Scheduler", "RobotId|Equals|" + schedule["RobotId"] + "||SystemId|Equals|" + schedule["SystemId"], "interval|Equals|" + schedule["interval"] + "||LastExecutionTime|Equals|" + date + " " + time + "||Startdate|Equals|" + schedule["StartDate"] + "||Starttime|Equals|" + schedule["StartTime"] + "||Tag|Equals|" + schedule["Description"] + "||Timeperiod|Equals|" + schedule["TimePeriod"] + "||Runatserver|Equals|" + schedule["Runatserver"] + "||End|Equals|" + schedule["End"] + "||RepeatEvery|Equals|" + schedule["RepeatEvery"] + "||ScheduleStatus|Equals|" + schedule["Status"], "public", false);
                    }
                    else
                    {
                        //ret = new QueryUtil().updateTableData("Scheduler", "RobotId|Equals|" + schedule["RobotId"] + "||SystemId|Equals|" + schedule["SystemId"], "interval|Equals|" + schedule["interval"] + "||LastExecutionTime|Equals|" + date + " " + time + "||Startdate|Equals|" + schedule["StartDate"] + "||Starttime|Equals|" + schedule["StartTime"] + "||Priority|Equals|" + schedule["Priority"] + "||Timeperiod|Equals|" + schedule["TimePeriod"] + "||Status|Equals|" + schedule["Status"] + "||Runatserver|Equals|" + schedule["Runatserver"], "public", false);
                        ret = new QueryUtil().updateTableData("Scheduler", "RobotId|Equals|" + schedule["RobotId"] + "||SystemId|Equals|" + schedule["SystemId"], "interval|Equals|" + schedule["interval"] + "||LastExecutionTime|Equals|" + date + " " + time + "||Startdate|Equals|" + schedule["StartDate"] + "||Starttime|Equals|" + schedule["StartTime"] + "||Tag|Equals|" + schedule["Description"] + "||Timeperiod|Equals|" + schedule["TimePeriod"] + "||Runatserver|Equals|" + schedule["Runatserver"] + "||End|Equals|" + schedule["End"] + "||RepeatEvery|Equals|" + schedule["RepeatEvery"] + "||RepeatOn|Equals|" + schedule["RepeatOn"] + "||ScheduleStatus|Equals|" + schedule["Status"], "public", false);
                    }
                }
                else
                {
                    //string values = schedule["RobotId"] + "," + schedule["interval"] + "," + date + " " + time + "," + schedule["SystemId"] + ", " + schedule["CreateBy"] + "," + date + " " + time + ", " + schedule["UpdateBy"] + "," + date + " " + time + "," + schedule["Status"] + "," + schedule["StartDate"] + "," + schedule["StartTime"] + ",null," + schedule["TimePeriod"] + "," + schedule["Priority"] + "," + null + "," + schedule["Runatserver"];
                    string values = schedule["RobotId"] + "," + schedule["interval"] + "," + date + " " + time + "," + schedule["SystemId"] + ", " + schedule["CreateBy"] + "," + date + " " + time + ", " + schedule["UpdateBy"] + "," + date + " " + time + ",true," + schedule["StartDate"] + "," + schedule["StartTime"] + "," + schedule["Description"] + "," + schedule["TimePeriod"] + "," + schedule["Runatserver"] + "," + schedule["EndDate"] + "," + schedule["End"] + "," + schedule["RepeatEvery"] + "," + schedule["RepeatOn"] + "," + schedule["Status"] + "," + schedule["Instance"].Replace(",", "-");
                    ret = new QueryUtil().insertTableData("public", "Scheduler", values, "", false);
                }
                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
        public string stopSchedule(string details)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                Dictionary<string, string> schedule = JsonConvert.DeserializeObject<Dictionary<string, string>>(details);
                Connection dbd = new Connection();
                string query = "SELECT count(*) FROM public.\"Scheduler\" where \"RobotId\"='" + schedule["RobotId"] + "' and \"Status\" = 'true' ";
                string ret = dbd.executeSQL(query, "Select");
                List<Dictionary<string, int>> c = JsonConvert.DeserializeObject<List<Dictionary<string, int>>>(ret);
                if (c[0]["count"] != 0)
                {
                    ret = new QueryUtil().updateTableData("Scheduler", "RobotId|Equals|" + schedule["RobotId"], "Status|Equals|false", "public", false);
                }

                return ret;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                if (ExceptionList.Exceptionlist.ContainsKey(ex.GetType().Name.ToString()))
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, ExceptionList.Exceptionlist[ex.GetType().Name.ToString()]));
                }
                else
                {
                    msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));

                }
                return ExceptionList.Exceptionlist[ex.GetType().Name.ToString()];
            }
        }
    }
}
