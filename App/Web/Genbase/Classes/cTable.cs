﻿using Genbase.BLL;
using Genbase.DAL;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.Classes
{
    public class cTable
    {
        Connection c = Connection.GetInstance;
        public string getTables()
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                var query = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' AND table_type = 'BASE TABLE' AND table_name!= 'ElementTable' AND table_name!= 'ScreenTable' AND table_name!='WidgetTable' order by table_name";

                String result = c.executeSQL(query, "Select");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }

        public string getElementRows(string tablename)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string query = "SELECT column_name FROM information_schema.columns WHERE table_name = '" + tablename + "'";

                string result = c.executeSQL(query, "Select");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }

        public string insertElementTable(string data2, int screenId)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = "";
                Boolean hidden = false;
                Connection con = new Connection();
                string time = DateTime.Now.ToShortTimeString();
                string date = DateTime.Now.ToShortDateString();
                JArray data = JArray.Parse(data2);
                for (int i = 0; i < data.Count; i++)
                {

                    string values = screenId + "," + Convert.ToBoolean(data[i]["active"]) + "," + data[i]["columnName"].ToString() + "," + data[i]["displayName"].ToString() + "," + data[i]["columnOrder"].ToString() + "," + Convert.ToBoolean(data[i]["showGrid"]) + "," + Convert.ToBoolean(data[i]["showSearch"]) + "," + Convert.ToBoolean(data[i]["showForm"]) + "," + hidden + "," + data[i]["defaultValue"].ToString() + "," + data[i]["CreateBy"] + ", " + date + "" + time + "," + data[i]["UpdateBy"] + "," + date + "" + time + ",true";
                    result = new QueryUtil().insertTableData("public", "ElementTable", values, "", false);

                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }
        }

        public string updateElementTable(string data2)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {
                string result = "";
                Connection con = new Connection();
                JArray data = JArray.Parse(data2);
                for (int i = 0; i < data.Count; i++)
                {
                    result = new QueryUtil().updateTableData("ElementTable", "ElementId|Equals|" + data[i]["elementId"], "Active|Equals|" + data[i]["active"] + "||ColumnName|Equals|" + data[i]["columnName"] + "||DisplayName|Equals|" + data[i]["displayName"] + "||ColumnOrder|Equals|null||ShowGrid|Equals|" + data[i]["showGrid"] + "||ShowSearch|Equals|" + data[i]["showSearch"] + "||ShowForm|Equals|" + data[i]["showForm"] + "||Hidden|Equals|false||DefaultValue|Equals|null", "public", false);
                }
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }

        }
        public string getScreenElements(int id)
        {
            List<KeyValuePair<string, string>> msgs = new List<KeyValuePair<string, string>>();
            try
            {

                string result = new QueryUtil().getTableData("ElementTable", "ScreenId|Equals|" + id + "", "\"ElementId\",\"Active\", \"ColumnName\", \"DisplayName\",\"ColumnOrder\", \"ShowGrid\", \"ShowSearch\", \"ShowForm\", \"DefaultValue\"", 0, "public", "\"ElementId\"", "");
                return result;
            }
            catch (Exception ex)
            {
                new Logger().LogException(ex);
                msgs.Add(new MethodResultHandler().createMessage(ex.Message, StringHelper.exceptioncode));
                return ex.Message;
            }

        }
    }
}
