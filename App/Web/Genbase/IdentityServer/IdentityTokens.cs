﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Genbase.IdentityServer
{
    public class IdentityTokens
    {
        public IConfiguration _configuration = Config.getconfig1();
        public string GenerateJwtToken(string mail, string MobilePhone)
        {
            try
            {
                var claims = new List<Claim>
            {
                //Email
                new Claim(JwtRegisteredClaimNames.Sub, mail),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                // UserId
                //new Claim(ClaimTypes.NameIdentifier, userid),
                new Claim(ClaimTypes.NameIdentifier, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Actor, "#######"),
                new Claim(ClaimTypes.MobilePhone, MobilePhone),
                new Claim(ClaimTypes.Role, "admin")
            };
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["JwtExpireDays"]));

                var token = new JwtSecurityToken(
                    _configuration["JwtIssuer"],
                    _configuration["JwtAudience"],
                    claims,
                    expires: expires,
                    signingCredentials: creds
                );

                string maintoken = new JwtSecurityTokenHandler().WriteToken(token);
                return maintoken;

            }
            catch (Exception ex)
            {
                return ex.Message.ToString(); ;
            }
        }

        public string ValidateJwtToken(string JWToken)
        {
            var validationParameters = new TokenValidationParameters
            {
                // Clock skew compensates for server time drift. I recommend 5 minutes or less:
                ClockSkew = TimeSpan.FromMinutes(5),
                // Specify the key used to sign the token:
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"])),
                RequireSignedTokens = true,
                // Ensure the token hasn't expired:
                RequireExpirationTime = true,
                ValidateLifetime = true,
                // Ensure the token audience matches our audience value (default true):
                ValidateAudience = true,
                ValidAudience = _configuration["JwtAudience"],
                // Ensure the token was issued by a trusted authorization server (default true):
                ValidateIssuer = true,
                ValidIssuer = _configuration["JwtIssuer"],
            };

            try
            {
                var claimsPrincipal = new JwtSecurityTokenHandler()
                    .ValidateToken(JWToken, validationParameters, out var rawValidatedToken);

                //  return (JwtSecurityToken)rawValidatedToken;
                return "Success";
                // Or, you can return the ClaimsPrincipal
                // (which has the JWT properties automatically mapped to .NET claims)
            }
            catch (SecurityTokenValidationException stvex)
            {
                // The token failed validation!
                // TODO: Log it or display an error.
                // new Exception($"Token failed validation: {stvex.Message}");
                return $"Token failed validation: {stvex.Message.ToString()}";
            }
            catch (ArgumentException argex)
            {
                // The token was not well-formed or was invalid for some other reason.
                // TODO: Log it or display an error.
                //throw new Exception($"Token was invalid: {argex.Message}");
                return $"Token was invalid: {argex.Message.ToString()}";
            }
        }

    }
}
