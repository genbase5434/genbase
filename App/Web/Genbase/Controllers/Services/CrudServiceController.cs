﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Genbase.BLL;
using Genbase.BLL.ElementsClasses;
using Genbase.classes;
using Genbase.Classes;
using Genbase.ServiceInterfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Renci.SshNet;
//using MethodResultHandler = Genbase.BLL.MethodResultHandler;

namespace Genbase.Controllers.Services
{
    [Route("api/CrudService")]
    [EnableCors("GenbasePolicy")]
    [ApiController]
    public class CrudServiceController : ControllerBase
    {
        private readonly CrudServiceInterfaces _crudService;
        public CrudServiceController(CrudServiceInterfaces crudService)
        {
            _crudService = crudService;
            ServicePointManager.Expect100Continue = false;
        }
        // [Authorize]
        [HttpGet("getTableData")]
        public async Task<string> getTableData(string tablename)
        {
            try
            {
                return _crudService.getTableData(tablename);
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        //[Authorize]
        [HttpPost("getGridData")]
        public string getGridData(string tablename, [FromBody] PagingParams data)
        {
            try
            {
                return JsonConvert.SerializeObject(_crudService.getGridData(tablename, data));

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        // [Authorize]
        [HttpGet("getTableDataByUserID")]
        public async Task<string> getTableDataByUserID(string tablename, string CreateBy)
        {
            try
            {
                return _crudService.getTableDataByUserID(tablename, CreateBy);
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("loadUsernames")]
        public async Task<string> loadUsernames()
        {
            try
            {
                return _crudService.loadUsernames();
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("GetRobotsByLOB")]
        public async Task<string> GetRobotsByLOB()
        {
            try
            {
                return _crudService.GetRobotsByLOB();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        // [Authorize]
        [HttpGet("GetVersionsofRobot")]
        public async Task<string> GetVersionsofRobot(string id)
        {
            try
            {
                return _crudService.GetVersionsofRobot(id);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpPost("createTableRow")]
        public string createTableRow(string tablename, [FromBody]string Values)
        {
            try
            {
                //string values = JsonConvert.SerializeObject(Values);
                return _crudService.createTableRow(tablename, Values);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost("createMenuTableRow")]
        public string createMenuTableRow(string tablename, [FromBody]string Values)
        {
            try
            {
                return _crudService.createMenuTableRow(tablename, Values);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        // [Authorize]
        [HttpPost("createTableForLob")]
        public string createTableForLob(string tablename, [FromBody]string Values)
        {
            try
            {
                return _crudService.createTableForLob(tablename, Values);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("GetStepProperties/stepId")]
        public async Task<string> getStepProperties(string stepId)
        {
            try
            {
                return _crudService.getStepProperties(stepId);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpPost("UserRegistration")]
        public string UserRegistration(string UserDetails)
        {
            try
            {
                return _crudService.UserRegistration(UserDetails);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("GetRobotLog")]
        public async Task<string> GetRobotLog(string tablename, string roleid)
        {
            try
            {
                return _crudService.GetRobotLog(tablename, roleid);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpPost("UpdateTableRow")]
        public string UpdateTableRow(string tablename, [FromBody]string Values, string ID)
        {
            try
            {
                return _crudService.UpdateTableRow(tablename, Values, ID);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpPost("updateTableRowforLob")]
        public string UpdateTableRowforLob(string tablename, string Values, string ID)
        {
            try
            {
                return _crudService.UpdateTableRowforLob(tablename, Values, ID);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("GetHistory")]
        public async Task<string> GetHistory(string robotid)
        {
            try
            {
                return _crudService.GetHistory(robotid);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("PreDefinedRobot")]
        public async Task<string> PreDefinedRobot(string projectid, string robotname, string predefinedbotid)
        {
            try
            {
                return _crudService.PreDefinedRobot(projectid, robotname, predefinedbotid);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpPost("CreateConnection")]
        public Renci.SshNet.ConnectionInfo CreateConnection(string ipname, int port, string username, string password)
        {
            return _crudService.CreateConnection(ipname, port, username, password);
        }
        // [Authorize]
        [HttpGet("Monitor")]
        public async Task<string> Monitor(string ScreenName, string Role, string CreateBy, string UpdateBy)
        {
            try
            {
                return _crudService.Monitor(ScreenName, Role, CreateBy, UpdateBy);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("getScheduleDetails")]
        public async Task<string> GetScheduleDetails(string RobotId, string SystemId)
        {
            try
            {
                return _crudService.GetScheduleDetails(RobotId, SystemId);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("GetRobotTimelineLive")]
        public async Task<string> GetRobotTimelineLive(string lasttimestamp)
        {
            try
            {
                return _crudService.GetRobotTimelineLive(lasttimestamp);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpPost("deleteTableRow")]
        public string deleteTableRow(string tablename, string ID)
        {
            try
            {
                return _crudService.deleteTableRow(tablename, ID);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost("hardDeleteTableRow")]
        public string hardDeleteTableRow(string tablename, string roleid)
        {
            try
            {
                return _crudService.hardDeleteTableRow(tablename, roleid);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        // [Authorize]
        [HttpPost("getGridDataByUser")]
        public string getGridDataByUser(string tablename, string CreateBy, [FromBody] PagingParams data)
        {
            return JsonConvert.SerializeObject(_crudService.getGridDataByUser(tablename, CreateBy, data));
        }

        // [Authorize]
        [HttpPost("getGridDataByUserCategory")]
        public string getGridDataByUserCategory(string tablename, string CreateBy, [FromBody] PagingParams data, string Category)
        {
            return JsonConvert.SerializeObject(_crudService.getGridDataByUserCategory(tablename, CreateBy, data, Category));
        }

        // [Authorize]
        [HttpPost("getConfigurationGridData")]
        public DataTables getConfigurationGridData(string tablename, string type, [FromBody] PagingParams data)
        {
            return _crudService.getConfigurationGridData(tablename, type, data);
        }

         [HttpPost("getConnectionGridData")]
        public DataTables getConnectionGridData(string tablename, string type, [FromBody] PagingParams data)
        {
            return _crudService.getConnectionGridData(tablename, type, data);
        }

        //// [Authorize]
        [HttpGet("ConnectiveInfo")]
        public async Task<string> Connective_info()
        {
            try
            {
                return _crudService.Connective_info();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpPost("LoginAuthenticationEX")]
        public string LoginAuthenticationEX(string user)
        {
            try
            {
                return _crudService.LoginAuthenticationEX(user);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("GetSystemsByRobotsCount")]
        public async Task<string> GetSystemsByRobotsCount()
        {
            try
            {
                return _crudService.GetSystemsByRobotsCount();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("getMenuRoleId")]
        public async Task<string> getMenuRoleId(int RoleId, string LOB)
        {
            try
            {
                return _crudService.getMenuRoleId(RoleId, LOB);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpGet("getRoleMenu")]
        public string getRoleMenu(int roleID)
        {
            try
            {
                return _crudService.getRoleMenu(roleID);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        
        [HttpPost("ForgetPassword")]
        public string ForgetPassword(string email)
        {
            try
            {
                return _crudService.ForgetPassword(email);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        // [Authorize]
        [HttpGet("robotSchedule")]
        public string robotSchedule(string ScheduleDetails)
        {
            try
            {
                return _crudService.robotSchedule(ScheduleDetails);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        // [Authorize]
        [HttpGet("genericScreenDropDown")]
        public async Task<string> genericScreenDropDown(int elementId)
        {
            try
            {
                return _crudService.genericScreenDropDown(elementId);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("getWidgetRow")]
        public async Task<string> getWidgetRow(int elementId)
        {
            try
            {
                return _crudService.getWidgetRow(elementId);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("StopRobotSchedule")]
        public string stopRobotSchedule(string ScheduleDetails)
        {
            try
            {
                return _crudService.stopRobotSchedule(ScheduleDetails);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        // [Authorize]
        [HttpGet("gettablebyId")]
        public async Task<string> gettablebyId(string tablename, string id)
        {
            try
            {
                return _crudService.gettablebyId(tablename, id);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("GetRobotsByLOBTimeStamp")]
        public async Task<string> getRobotsByLOB(string timestamp)
        {
            try
            {
                return _crudService.getRobotsByLOB(timestamp);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("generateSQLScripts")]
        public string generateSQLScripts(string Robots)
        {
            try
            {
                string ul = url();
                return _crudService.generateSQLScripts(Robots, ul);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        // [Authorize]
        [HttpPost("UploadSQLScripts")]
        public string uploadSQLScripts(object importRobotData)
        {
            try
            {
                return _crudService.uploadSQLScripts(importRobotData);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        // [Authorize]
        [HttpPost("UpdateRobotStatus")]
        public string UpdateRobotStatus(string BotID)
        {
            try
            {
                return _crudService.UpdateRobotStatus(BotID);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("getDataByRTable")]
        public string getDataByRTable(string tableDetails)
        {
            try
            {
                return _crudService.getDataByRTable(tableDetails);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("GetDataByUser")]
        public string GetDataByUser(string tablename, string CreateBy)
        {
            return JsonConvert.SerializeObject(_crudService.GetDataByUser(tablename, CreateBy));
        }

        // [Authorize]
        [HttpGet("getapp_tablesData")]
        public string getapp_tablesData()
        {
            return JsonConvert.SerializeObject(_crudService.getapp_tablesData());
        }

        // [Authorize]
        [HttpGet("deleteTables")]
        public string deleteTables(string table_id)
        {
            return JsonConvert.SerializeObject(_crudService.deleteTables(table_id));
        }
        [HttpGet("url")]
        public string url()
        {
            var s = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            return s;
        }
       
        [HttpGet("Download")]
        public FileContentResult TestDownload(string fileName, string foldername)
        {
            if (!string.IsNullOrEmpty(fileName))
            {
                KeyValuePair<string, byte[]> content = new FileDownloadHandler().getFileNameandContent(fileName, foldername);

                if (System.IO.File.Exists(content.Key))
                {
                    FileContentResult result = new FileContentResult(content.Value, "application/octet-stream")
                    {
                        FileDownloadName = fileName
                    };

                    return result;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        // [Authorize]
        [HttpPost("UpdateNKPIPath")]
        public string UpdateNKPIPath(string winlocation, string path, int id)
        {
            try
            {
                return _crudService.UpdateNKPIPath(winlocation, path, id);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }



        [HttpPost("CreateVariables")]
        public string CreateVariables(string rid, string variable)
        {
            try
            {
                return _crudService.CreateVariables(rid, variable);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpPost("UpdateVariables")]
        public string UpdateVariables(string rid, string variable)
        {
            try
            {
                return _crudService.UpdateVariables(rid, variable);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpPost("DeleteVariables")]
        public string DeleteVariables(string rid, string variable)
        {
            try
            {
                return _crudService.Deletevariables(rid, variable);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpGet("GetVariables")]
        public string getVariables(string rid)
        {
            return _crudService.getVariables(rid);
        }

        [HttpGet("getFilterTableData")]
        public async Task<string> getFilterTableData(string tablename, string Category)
        {
            try
            {
                return _crudService.getFilterTableData(tablename, Category);
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        [HttpGet("connectionHealthCheck")]
        public string connectionHealthCheck(string tablename)
        {
            try
            {
                return _crudService.connectionHealthCheck(tablename);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpGet("getConnectivity_Info")]
        public string getConnectivity_Info(string tablename, string Name, string Server, int Conn_ID)
        {
            try
            {
                return _crudService.getConnectivity_Info(tablename,Name,Server,Conn_ID);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}