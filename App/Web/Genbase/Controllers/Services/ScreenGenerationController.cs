﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Genbase.ServiceInterfaces;
using Microsoft.AspNetCore.Cors;
using System.Net;

namespace Genbase.Controllers.Services
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("GenbasePolicy")]
    public class ScreenGenerationController : ControllerBase
    {
        private readonly ScreenGenerationInterfaces _screensService;
        public ScreenGenerationController(ScreenGenerationInterfaces screensService)
        {
            _screensService = screensService;
            ServicePointManager.Expect100Continue = false;
        }

        // [Authorize]
        [HttpPost("SetPage")]
        public async Task<string> SetPage(object screenData)
        {
            try
            {
                return _screensService.SetPage(screenData);
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        [HttpPost("UpdatePage")]
        public async Task<string> UpdatePage(object screenData)
        {
            try
            {
                return _screensService.UpdatePage(screenData);
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        [HttpGet("DropDownTables")]
        public async Task<string> DropDownTables()
        {
            try
            {
                return _screensService.DropDownTables();
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        [HttpGet("EditView")]
        public async Task<string> EditView(string name)
        {
            try
            {
                return _screensService.EditView(name);
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        [HttpGet("getScreenDetails")]
        public async Task<string> getScreenDetails(int id)
        {
            try
            {
                return _screensService.getScreenDetails(id);
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        [HttpPost("updateMenuDetails")]
        public async Task<string> updateMenuDetails(string Source, int destID)
        {
            try
            {
                return _screensService.updateMenuDetails(Source, destID);
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        [HttpGet("LoadRoles")]
        public async Task<string> LoadRoles()
        {
            try
            {
                return _screensService.LoadRoles();
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        [HttpGet("getMenu")]
        public async Task<string> getMenu(string tablename)
        {
            try
            {
                return _screensService.getMenu(tablename);
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        [HttpPost("deleteScreen")]
        public async Task<string> deleteScreen(string tableName, string deleteId)
        {
            try
            {
                return _screensService.deleteScreen(tableName, deleteId);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpGet("getWidgetRow")]
        public string getWidgetRow(int elementId)
        {
            try
            {
                return _screensService.getWidgetRow(elementId);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost("widget")]
        public string widget(string columnDetails)
        {
            try
            {
                return _screensService.widget(columnDetails);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

    }
}