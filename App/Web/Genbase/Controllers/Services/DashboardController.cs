﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Genbase.BLL;
using Genbase.classes;
using Genbase.Classes;
using Genbase.Interface_DashBoard;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Genbase.Controllers.Services
{
    [Route("api/Dashboard")]
    [EnableCors("GenbasePolicy")]
    [ApiController]
    //[Authorize]
    public class DashboardController : ControllerBase
    {
        private readonly DashBoardServiceInterfaces _dashBoardService;
        public DashboardController(DashBoardServiceInterfaces dashBoardService)
        {
            _dashBoardService = dashBoardService;
            ServicePointManager.Expect100Continue = false;
        }
        [HttpGet("getKPI")]
		public async Task<string> getKPI(int roleid, string monitor, string filters, string KPIid)
        {
            try
            {
                return await _dashBoardService.getKPI(roleid, monitor, filters, KPIid);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpGet("getCharts")]
		 public async Task<string> getCharts(int roleid, string monitor, string filters, string KPIid)
        {
            try
            {
                return await _dashBoardService.getCharts(roleid, monitor, filters, KPIid);
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        [HttpGet("getTimeCharts")]
		public async Task<string> getTimeCharts(int roleid, string monitor, string filters, string chartId)
        {
            try
            {
                return await _dashBoardService.getTimeCharts(roleid, monitor, filters, chartId);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpGet("queryValidate")]
		public async Task<string> queryValidate(string DBInstance, string Query)
        {
            try
            {
                return await _dashBoardService.queryValidate(DBInstance, Query);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpGet("dashboardIcons")]
		public async Task<string> dashboardIcons(int roleid)
        {
            try
            {
                return await _dashBoardService.dashboardIcons(roleid);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpPost("updateDashboard")]
        public async Task<string> updateDashboard(string Type, string roleid, string AddnewdashboardId, [FromBody]string dashboardId)
        {
            try
            {
                return await _dashBoardService.updateDashboard(Type, roleid, AddnewdashboardId, dashboardId);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpGet("getMenu")]
		public async Task<string> getMenu(string tablename)
        {
            try
            {
                return await _dashBoardService.getMenu(tablename);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpGet("filterChartsSeries")]
        public async Task<string> filterChartsSeries()
        {
            try
            {
                return await _dashBoardService.filterChartsSeries();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpGet("filterCharts")]
        public async Task<string> filterCharts(string category)
        {
            try
            {
                return await _dashBoardService.filterCharts(category);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

    }
}