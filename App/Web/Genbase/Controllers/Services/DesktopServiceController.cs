﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Genbase.BLL;
using Genbase.classes;
using Genbase.Classes;
using Genbase.ServiceInterfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static Genbase.classes.Save;
using static Genbase.Classes.Login;

namespace Genbase.Controllers.Services
{
    [Route("api/DesktopService")]
    [EnableCors("GenbasePolicy")]
    [ApiController]
    //[Authorize]
    public class DesktopServiceController : ControllerBase
    {
        public DesktopServiceController(DesktopServiceInterfaces desktopService)
        {
            _desktopService = desktopService;
            ServicePointManager.Expect100Continue = false;
        }
        private readonly DesktopServiceInterfaces _desktopService;

        [HttpPost("Create")]
        public string CreateProject(Entities inputString)
        {
            try
            {
                return _desktopService.CreateProject(inputString);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost("Delete")]
        public string Delete(Entities inputString)
        {
            try
            {
                return _desktopService.Delete(inputString);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpGet("Get")]
        public async Task<string> Get(string input)
        {
            try
            {
                return _desktopService.Get(input);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost("Update")]
        public string Update(Entities inputString)
        {
            try
            {
                return _desktopService.Update(inputString);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost("Execute")]
        public string Execute(Entities inputString)
        {
            try
            {
                return _desktopService.Execute(inputString);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost("Set")]
        public string Set(Entities inputString)
        {
            try
            {
                return _desktopService.Set(inputString);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost("Rename")]
        public string Rename(Entities inputString)
        {
            try
            {
                return _desktopService.Rename(inputString);
            }
            catch (Exception ex)
            {

                return ex.Message; ;
            }
        }

        [HttpPost("Validate")]
        public string Validate(Entities inputString)
        {
            try
            {
              return _desktopService.Validate(inputString);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost("Debug")]
        public string Debug(Entities inputString)
        {
            try
            {
                return _desktopService.Debug(inputString);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost("StepExecution")]
        public string StepExecution(Entities inputString)
        {
            try
            {
                return _desktopService.StepExecution(inputString);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost("Upload")]
        public string UploadDocument(string file_name, [FromBody]byte[] file)
        {
            try
            {
                return _desktopService.UploadDocument(file, file_name);

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpGet("ClearRobotGlobalVariables")]
        public string ClearRobotGlobalVariables(string Robot_Id)
        {
            try
            {
                return _desktopService.ClearRobotGlobalVariables(Robot_Id);

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpGet("TestConnection")]
        public bool TestConnection()
        {
            try
            {
                return _desktopService.TestConnection();
            }
            catch (Exception ex)
            {
                string exc = ex.Message;
                return false;
            }
        }
    }
}