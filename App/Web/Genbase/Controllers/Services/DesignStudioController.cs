﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Genbase.BLL;
using Genbase.classes;
using Genbase.Classes;
using Genbase.IdentityServer;
using Genbase.ServiceInterfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;


namespace Genbase.Controllers.Services
{

    [Route("api/DesignStudio")]
    [EnableCors("GenbasePolicy")]
    [ApiController]
    public class DesignStudioController : ControllerBase
    {
        public DesignStudioController(DesignStudioInterfaces genbaseStudioInterfaces)
        {
            _studio = genbaseStudioInterfaces;
            ServicePointManager.Expect100Continue = false;
        }
        private readonly DesignStudioInterfaces _studio;
        
        [HttpGet("CreateToken")]
        public async Task<string> GenerateJwtToken(string mail, string MobilePhone)
        {
            try
            {
                return _studio.GenerateJwtToken(mail, MobilePhone);
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        [HttpGet("ValidateToken")]
        public async Task<string> ValidateToken(string JWToken)
        {
            try
            {
                return _studio.ValidateToken(JWToken);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpPost("Create")]
        public string Create(string input)
        {
            try
            {
                return _studio.Create(input);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        // [Authorize]
        [HttpGet("Get")]
        public async Task<string> Get(string input)
        {
            try
            {
                return _studio.Get(input);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("Delete")]
        public string Delete(string input)
        {
            try
            {
                return _studio.Delete(input);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpPost("SaveWorkflow")]
        public string SaveWorkflow(Entities input)
        {
            try
            {
                return _studio.SaveWorkflow(input);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpPost("Update")]
        public string Update(string input)
        {
            try
            {
                return _studio.Update(input);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpGet("ExecuteRobot")]
        public string ExecuteRobot(string input)
        {
            try
            {
                return _studio.ExecuteRobot(input);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("GetAllNextStep")]
        public void GetAllNextStep(Steps step)
        {
            _studio.GetAllNextStep(step);
        }
        // [Authorize]
        [HttpGet("debug_Order")]
        public string debug_Order(string input)
        {
            try
            {
                return _studio.debug_Order(input);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        [HttpGet("debug_Execution")]
        public string debug_Execution(string input)
        {
            try
            {
                return _studio.debug_Execution(input);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpPost("set")]
        public string Set(Entities input)
        {
            try
            {
                return _studio.Set(input);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpGet("getStudio")]
        public string getStudio(string roleId, string userId)
        {
            try
            {
                return _studio.getStudio(roleId, userId);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpPost("StepExecution")]
        public string StepExecution(string input)
        {
            try
            {
                return _studio.StepExecution(input);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpPost("Validate")]
        public string Validate(string LocalStepPairs)
        {
            try
            {
                return _studio.Validate(LocalStepPairs);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpPost("Upload")]
        public string Upload(string fPath, [FromBody]string data1, bool isFolder, string previousFolderPath)
        {
            try
            {
                return _studio.Upload(fPath, data1, isFolder, previousFolderPath);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpPost("SendMail")]
        public string sendMail(string input)
        {
            try
            {
                return _studio.sendMail(input);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        // [Authorize]
        [HttpPost("NewSaveWorkflow")]
        public string NewSaveWorkflow(Dictionary<string, string> input)
        {
            try
            {
                return _studio.NewSaveWorkflow(input);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        
    }
}