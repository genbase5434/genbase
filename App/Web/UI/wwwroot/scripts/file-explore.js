(function ($) {
    $.fn.filetree = function (method) {

        var settings = { // settings to expose
            animationSpeed: 'fast',
            collapsed: true,
            console: false

        }

        var methods = {
            init: function (options) {
                // Get standard settings and merge with passed in values
                var options = $.extend(settings, options);
                // Do this for every file tree found in the document
                return this.each(function () {

                    var $fileList = $(this);
                    var rtype = '';
                    var projecttype = '';
                    $fileList
                        .addClass('file-list')
                        .find('li')
                        .has('ul') // Any li that has a list inside is a folder root
                            .addClass('folder-root closed')
                            .on('click', 'a[href="javascript:;"]', function (e) { // Add a click override for the folder root links  //e.preventDefault();

                                if ($(this).parent().first().hasClass('Prjbtn')) {
                                    $(this).parent().toggleClass('closed');
                                    if ($(".Rbtbtn.active").length < 1) {


                                        if (!$(this).parent().find('.Rbtbtn').parent().hasClass('Workflow_Robot'))
                                            $(this).parent().find('.Rbtbtn').first().addClass('active').append('<button id="edit_Robot" onclick="editrobot(this)" class="button edit_prj fa fa-edit" ><span class="tooltiptext">Edit</span></button>','<button id="delete_Robot"  class="button delete_prj fa fa-trash" ><span class="tooltiptext">Delete</span></button>');
                                        else
                                            $(this).parent().find('.Rbtbtn').first().addClass('active');
                                    }
                                }
                                else {
                                    $('.active').find('#delete_Robot').remove()
                                    $('.active').find('#edit_Robot').remove()
                                    $('.active').removeClass('active');
                                    $("#PropertiesBody").html("");
                                    rtype = "";
                                    if ($(this).parent().first().hasClass('Prjbtn') == false) {
                                        $(this).addClass('active').append('<button id="edit_Robot" onclick="editrobot(this)" class="button edit_prj fa fa-edit" ><span class="tooltiptext">Edit</span></button>', '<button id="delete_Robot" class="button delete_prj fa fa-trash" ><span class="tooltiptext">Delete</span></button>');
                                        if ($(this).parent().first().hasClass('ValueStreamMap')) rtype = 'ValueStreamMap';
                                        else if ($(this).parent().first().hasClass('FlowChart')) rtype = 'FlowChart';
                                        else rtype = 'Robot';

                                        if (typeof ($(this).attr('id')) !== 'undefined') actiononClickRobot(); loadRobots($(this).attr('id'), rtype); loadPalette(rtype); versions($(this).attr('id')); loadRobotProperties();
                                    }
                                    else if (jQuery('.active').parent().hasClass('Workflow_Robot')) {
                                        $("#delete_Robot").hide();
                                    }
                                }


                                //  return false;
                            });
                    $fileList
                      .addClass('file-list')
                      .find('li')
                      .has('ul') // Any li that has a list inside is a folder root
                          .addClass('folder-root closed')
                          .on('click', '#delete_Robot', function (e) { // Add a click override for the folder root links
                                //e.preventDefault();
                              //deleteRobot($($('#delete_Robot').parent().first())[0].id, $(".Prjbtn")[0].id);
                              deleteFunction(e);
                              //loadProjectProcess();
                              //loadProcess("", "Robot");
                              //  return false;
                          });
                    //alert(options.animationSpeed); Are the settings coming in

                });


            }
        }

        if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.on("error", function () {
                console.log(method + " does not exist in the file exploerer plugin");
            });
        }
    }

}(jQuery));

var UpdateRobotId;
function editrobot(e) {
    $("#RobotNameModal").modal('show');
    var botName = e.parentElement.children[0].innerText;
    UpdateRobotId = e.parentElement.id;
    $("#robotnamelabel").val(botName);

}
$(document).ready(function () {
    $("#UpdateForm1").on("submit", function (e) {
        e.preventDefault();
        var Values = {
            "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
            //  "UpdateDatetime": window.btoa(GetNow())
        };
        var ID = {};
        ID["Id"] = UpdateRobotId;
        Values["Name"] = window.btoa(e.currentTarget.children[0].children[1].children[0].value);
        var values = JSON.stringify(Values);
        var Id = JSON.stringify(ID);
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/updateTableRowforLob?tablename=ROBOT&Values=' + values + '&ID=' + Id,
            contentType: 'application/json; charset=utf-8',
            success: function (val) {
                var UpdRobotID = JSON.parse(Id).Id;
                if (val == "true") {
                    swal("", "Robot Updated Successfully", "success")
                }
                else{
                    swal("", "Robot already exists, please try again!", "error");
                }
                $("#RobotNameModal").modal('hide');
                loadProjectProcess();
                loadProcess(UpdRobotID, "Robot");
                
            },
            error: function (e) {
            }
        });
    });
});