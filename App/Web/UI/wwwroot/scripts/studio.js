var x = localStorage.getItem("Result");
var UserDetails = JSON.parse(x);
var MK;
var StepCustomID;
var RobotID;
var RobotType;
var CreateProperties;
var ActionJSON;
var datasource;
var data = {};
var data1;
var RoboGeneralArr = [];
var ValGeneralArr = [];
var FlowGeneralArr = [];
var MailAutomationArr = [];
var ExcelAutomationArr = [];
var TallyAutomationArr = [];
var TallyApiArr = [];
var ExcelApiArr = [];
var WindowArr = [];
var OCRArr = [];
var NLPArr = [];
var DatabaseArr = [];
var DeveloperArr = [];
var WebArr = [];
var HTTPArr = [];
var SocialArr = [];
var FileArr = [];
var SSHArr = [];
var Steps = {};
var versionSteps = {};
var StepIndex;
var StepPropertiesLength;
var StepPropertiesIndex;
var ActionPropertiesJSON = {};
var DVariables = [];
var Elements;
var ElementArr = {};
var developer;
var myPaletteRobot = {};
var RobotPalette = {};
var myPaletteRobotAdv = {};
var RobotAdvPalette = {};
var Sname;
var cur_sp;
var uploadDatatTable;
var version_id;
var DropDownTableValues = {};
function loadElments() {
    var data = { "Type": "AllElements" };
    data1 = JSON.stringify(data);

    datasource = GlobalURL + '/api/GenbaseStudio/Get?input=' + data1;
    $.ajax({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        type: 'GET',
        url: datasource,
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (data2) {
            var accordinHeight = $("#left-accordin-Panel").height() / 7;
            Elements = JSON.parse(data2);
            $("#accordion").html("");
            var AccordinDiv = "";
            var ElementsDiv = "";
            for (element1 in Elements) {
                if (Elements[element1]["Category"] != "Advanced") {

                    AccordinDiv += "<h4>" + Elements[element1]["Name"] + "</h4>" + "<div><div id='myPalette" + Elements[element1]["Name"].trim() + "' style='width: 100%; height: 250px'></div></div>";
                }
                else {
                    ElementsDiv += "<h4>" + Elements[element1]["Name"] + "</h4>" + "<div><div id='myPalette" + Elements[element1]["Name"].trim() + "' style='width: 100%; height:250px'></div></div>";
                }
            }
            $('#accordion').html(AccordinDiv);
            $('#elementaccordion').html(ElementsDiv);
        },
        error: function (e) {
            loadElments();
        }
    });
}
var isfolder = false;
function changefilefolder(e) {
  
    if (e.type != "file") {

        var checkedfolder = 0;
        var Tr = $("#PropertiesBody").find("tr");
        for (var t = 0; t < Tr.length; t++) {
            if (typeof ($(Tr[t]).find('td')[0]) != typeof (undefined)) {
                if (($(Tr[t]).find('td')[0].innerText.toLowerCase().trim() == "isfolderupload*") || ($(Tr[t]).find('td')[0].innerText.toLowerCase().trim() == "isfolderupload")) {
                    if ($(Tr[t]).find('td input')[0].checked == true) {
                        checkedfolder++;
                    }
                }
            }
        }

        for (var t = 0; t < Tr.length; t++) {

            if (typeof ($(Tr[t]).find('td')[0]) != typeof (undefined)) {
                if (($(Tr[t]).find('td')[0].innerText.toLowerCase().trim() == "path*") || ($(Tr[t]).find('td')[0].innerText.toLowerCase().trim() == "path") || ($(Tr[t]).find('td')[0].innerText.toLowerCase().trim() == "attachments") || ($(Tr[t]).find('td')[0].innerText.toLowerCase().trim() == "attachment") || ($(Tr[t]).find('td')[0].innerText.toLowerCase().trim() == "file selection local") || ($(Tr[t]).find('td')[0].innerText.toLowerCase().trim() == "source") || ($(Tr[t]).find('td')[0].innerText.toLowerCase().trim() == "Match Input File1") || ($(Tr[t]).find('td')[0].innerText.toLowerCase().trim() == "Match Input File2") || ($(Tr[t]).find('td')[0].innerText.toLowerCase().trim() == "Classify Input File") || ($(Tr[t]).find('td')[0].innerText.toLowerCase().trim() == "Entity Input File") || ($(Tr[t]).find('td')[0].innerText.toLowerCase().trim() == "Sentiment Input File") || ($(Tr[t]).find('td')[0].innerText.toLowerCase().trim() == "SentenceFormation Input File")) {
                    if (checkedfolder > 0) {
                        isfolder = true;
                        e.type = "file";
                        e.webkitdirectory = true
                        e.mozdirectory = true;
                        e.directory = true;
                        e.accept = "image/*";
                        e.multiple = true;
                        // $(e).click();
                        $(e).addClass("choose_file2");
                    }
                    else {
                        isfolder = false;
                        e.type = "file";
                        e.webkitdirectory = false;
                        e.mozdirectory = false;
                        e.directory = false;
                        e.multiple = true;
                        // $(e).click();
                        $(e).addClass("choose_file2");
                    }
                    break;
                }
            }
        }
    }

}
var LOB;
var Project;
var DropDownRobots;
$.ajax({
    type: "GET",
    url: GlobalURL + '/api/CrudService/getTableData?tablename=LOB',
    async: true,
    contentType: 'application/json; charset=utf-8',
    success: function (result) {
        LOB = $.parseJSON(result);
    },
    error: function (e) {
    }
});
$.ajax({
    type: "GET",
    url: GlobalURL + '/api/CrudService/getTableData?tablename=PROJECT',
    async: true,
    contentType: 'application/json; charset=utf-8',
    success: function (result) {
        Project = $.parseJSON(result);
    },
    error: function (e) {
    }
});
$.ajax({
    type: "GET",
    url: GlobalURL + '/api/CrudService/GetRobotsByLOB',
    async: true,
    contentType: 'application/json; charset=utf-8',
    success: function (result) {
        DropDownRobots = $.parseJSON(result);
    },
    error: function (e) {
    }
});
// getUploadFolderFiles method no found in Api also
// ExcelActionFiles();
var ExcelActionUploadFiles;
function ExcelActionFiles() {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getUploadFolderFiles',
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            ExcelActionUploadFiles = result;
        },
        error: function (e) {
        }
    });
}
var CnctDataSource;
load_DBConnections();
function load_DBConnections() {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableData?tablename=ConnectionConfig',
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            CnctDataSource = JSON.parse(val);
        },
        error: function (e) {
        }
    });
}
var emailTemplateDataSource;
load_EmailTemplateDataSource();
function load_EmailTemplateDataSource() {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableData?tablename=EmailTemplate',
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            emailTemplateDataSource = JSON.parse(val);
        },
        error: function (e) {
        }
    });
}
var dbTypeDataSource;
load_DBTypeDataSource();
function load_DBTypeDataSource() {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableData?tablename=DBType',
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            dbTypeDataSource = JSON.parse(val);
        },
        error: function (e) {
        }
    });
}
var LocalRobotProperties = {};
function loadRobotProperties() {
    var RobotHTML = "";
    //uploadList = [];
    $("#PropertiesBody").html("");
    RobotHTML += "<tr><td class='col-sm-2'>Name</td><td><input type='text' col_id=\"Name\" name='RobotPropertyInput'  class='form-control col-sm-3'  onblur='updateRobotProperties(this)' value=\"" + LocalRobotProperties.Name + "\"/></td></tr>";
    if (LocalRobotProperties.Client_Dependency)
        RobotHTML += "<tr><td class='col-sm-2'>Client Dependency</td><td><input type='checkbox' class='checker'  col_id=\"Client_Dependency\" name='RobotPropertyInput'  class='col-sm-3'  onblur='updateRobotProperties(this)' checked/></td></tr>";
    else
        RobotHTML += "<tr><td class='col-sm-2'>Client Dependency</td><td><input type='checkbox' class='checker' col_id=\"Client_Dependency\"name='RobotPropertyInput'  class='col-sm-3'  onblur='updateRobotProperties(this)'/></td></tr>";
    RobotHTML += "<tr><td class='col-sm-2'>Default Execution</td><td><select  col_id=\"Default_Execution\" name='RobotPropertyInput'  class='form-control col-sm-3'  onblur='updateRobotProperties(this)' value=\"" + LocalRobotProperties.Default_Execution + "\"/><option>Select System</option><option value='On Server'>On Server</option>"
    for (var system in Systems_List) {
        RobotHTML += '<option value=' + Systems_List[system]["ID"] + '>On ' + Systems_List[system]["Name"] + '</option>';
    }
    RobotHTML += '</select></td></tr>';
    $("#PropertiesBody")[0].innerHTML += RobotHTML;
}
var Usernames;
loadUsers();
function loadUsers() {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/loadUsernames',
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            Usernames = JSON.parse(val);
        },
        error: function (e) {
        }
    });
}
function updateRobotProperties(e) {


    if (e.type != "checkbox")
        LocalRobotProperties[$(e)[0].attributes["col_id"].nodeValue] = $(e).val();
    else
        LocalRobotProperties[$(e)[0].attributes["col_id"].nodeValue] = e.checked;

}
var uploadfolderpath = [];
function loadStepProperties(step_name, s, sp_1) {
    debugger;
    $("#PropertiesBody").html("");
    var sp = sp_1.split(" ");
    var GeneralHtml = "<tr class='prop_tr'><th class='propertiescaption' colspan='2'>General</th></tr>";
    //var ActionHtml = "<tr><th>" + step_name + "-" + s + "</th><th></th></tr>";
    var ActionHtml = "<tr class='prop_tr'><th class='propertiescaption' colspan='2'>" + step_name + "</th></tr>";
    var reqStr = "";
    var requiredValue = "";
    var arr_ddl = [];
    for (i in sp) {
        if (sp[i] != " " && sp[i] != "") {
            if ((localStepProperties[s][sp[i]].CustomValidation == "1") || (localStepProperties[s][sp[i]].CustomValidation == "2") || (localStepProperties[s][sp[i]].CustomValidation == "3")) {
                reqStr = "<span style=\"color:red\">*</span>"
                requiredValue = "required";
                if ((localStepProperties[s][sp[i]].StepProperty != "Name") && (localStepProperties[s][sp[i]].StepProperty != "Description") && (localStepProperties[s][sp[i]].StepProperty != "Type") && (localStepProperties[s][sp[i]].StepProperty != "Goto") && localStepProperties[s][sp[i]].StepProperty != "Left" && localStepProperties[s][sp[i]].StepProperty != "Top" && localStepProperties[s][sp[i]].StepProperty != "DisplayName") {
                    switch (localStepProperties[s][sp[i]]["StepPropertyType"].trim().toLowerCase()) {
                        case "textbox":
                            if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().indexOf("password") > -1)
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input type='password' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "excel input") {
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><div class='exopen'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3 excelInput' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + "/></div></td></tr>";
                                $("#sp" + sp[i]).val(localStepProperties[s][sp[i]].StepPropertyValue);
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "tesseract input") {
                                if (localStepProperties[s][sp[i - 1]].StepPropertyValue == "Input") {
                                    ActionHtml += "<tr id='" + s + "'style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><div='tessaract'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3 tesseractInput' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + "onchange='selectedTesseractInput(this)'/></div></td></tr>";
                                    $("#sp" + sp[i]).val(localStepProperties[s][sp[i]].StepPropertyValue);
                                }
                                else {
                                    ActionHtml += "<tr id='" + s + "'style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><div='tessaract'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3 tesseractInput' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + "/></div></td></tr>";
                                    $("#sp" + sp[i]).val(localStepProperties[s][sp[i]].StepPropertyValue);
                                }
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3 tesseractInput' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + " readonly/></td></tr>";
                                $("#sp" + sp[i]).val(localStepProperties[s][sp[i]].StepPropertyValue);

                            }

                            else {
                                //ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + " /></td></tr>";
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><div class='iconfield'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + " /><span class='sampleicon' onclick='loadbVariables(this)'><img id='" + sp[i] + "' src='../Views/dist/img/variable_icon.svg' title='Variable'></span></div></td></tr>";
                                $("#sp" + sp[i]).val(localStepProperties[s][sp[i]].StepPropertyValue);
                            }
                            break;
                        case "checkbox":
                            if (typeof (localStepProperties[s][sp[i]].StepPropertyValue) != typeof (undefined)) {
                                if (localStepProperties[s][sp[i]].StepPropertyValue == true || localStepProperties[s][sp[i]].StepPropertyValue == true)
                                    ActionHtml += "<tr  id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input name='PropertyInput' autocomplete='off' type='checkbox' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' checked " + requiredValue + "/></td></tr>";
                                else
                                    ActionHtml += "<tr  id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input name='PropertyInput' autocomplete='off' type='checkbox' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' " + requiredValue + "/></td></tr>";
                            }
                            else {
                                ActionHtml += "<tr  id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input name='PropertyInput' autocomplete='off' type='checkbox' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' " + requiredValue + "/></td></tr>";
                            }
                            break;
                        case "dropdownlist":
                            if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "comparisontype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sourcetype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option  " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Static") + ">Static</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "datatype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "lefttype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "righttype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "starttype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "buttons") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "OK") + ">OK</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "OK-Cancel") + ">OK-Cancel</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "readtype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Unread") + ">Unread</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Read") + ">Read</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Star") + ">Star</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Important") + ">Important</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "All") + ">All</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "read type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Unread") + ">Unread</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "All") + ">All</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "category") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Home Timeline") + ">Home Timeline</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Mentions Timeline") + ">Mentions Timeline</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "User Timeline") + ">User Timeline</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "db type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "postgreSQL") + ">postgreSQL</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "MySQL") + ">MySQL</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "MSSQLServer") + ">MSSQLServer</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "download in") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Local") + ">Local</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Server") + ">Server</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "exportin") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Local") + ">Local</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Server") + ">Server</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "connectiontype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='connectiontypechange()'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Static") + " value='static'>Static</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Dynamic") + " value='dynamic'>Dynamic</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "RuntimeInput") + " value='runtimeinput'>RuntimeInput</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "connections") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 connectionscls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedConnection(this)'></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "lob") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 lobcls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedLOB(this)'></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "projectname") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 projectcls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedProject(this)'></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "robotname") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 robotcls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedRobot(this)'></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "template") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 templatecls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "tesseract input type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedTesseractInput(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Input") + ">Input</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "input type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedInput(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Select Option") + ">Select Option</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Input") + ">Input</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File Selection Control") + ">File Selection Control</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "file selection type") {
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 excelFileSelect' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + " 'title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedFileType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Select Option") + ">Select Option</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Local") + ">Local</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Network") + ">Network</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "file selection network") {
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 fileNetwork' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "output type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "classify input type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedClassifyType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "entity input type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedEntityType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sentiment input type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedSentimentType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sentenceformation input type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedSentenceFormationType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "match input type1") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedMatchType1(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Input") + ">Input</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "match input type2") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedMatchType2(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Input") + ">Input</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "condition") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " ><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "!=") + ">!=</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "==") + ">==</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "<") + "><</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, ">") + ">></option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "<=") + "><=</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, ">=") + ">>=</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "isbodyhtml") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "text") + ">text</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "html") + ">html</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "attribute") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Id") + ">Id</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Class") + ">Class</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Name") + ">Name</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Type") + ">Type</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Custom") + ">Custom</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sourcetype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Static") + ">Static</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "groupby") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "true") + ">true</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "false") + ">false</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "createtype") {

                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "deletetype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "User") + ">User</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Group") + ">Group</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Folder") + ">Folder</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Database") + ">Database</option></select></td></tr>";
                            } else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "permissiontype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "All") + ">All</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "None") + ">None</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "ostype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Windows") + ">Windows</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Linux") + ">Linux</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "protocol") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "IMAP") + ">IMAP</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "POP3") + ">POP3</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Others") + ">Others</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "charttype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "pieChart") + ">PieChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AreaChart") + ">AreaChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Lines") + ">Lines</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarOfPie") + ">BarOfPie</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "ColumnGraph") + ">ColumnGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarGraph") + ">BarGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Doughnut") + ">Doughnut</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Bubble") + ">Bubble</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Scatterplot") + ">Scatterplot</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Radar") + ">Radar</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "mail server") {
                                //ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' onblur='updateStepProperties(this)' onchange='updateMails(this) 'value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Gmail") + ">Gmail</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Outlook") + ">Outlook</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AOL") + ">AOL</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Yandex") + ">Yandex</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Yahoo") + ">Yahoo</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "GMX") + ">GMX</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Mail") + ">Mail</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Others") + ">Others</option></select></td></tr>";
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' onchange='updateMails(this) ' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Gmail") + ">Gmail</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Others") + ">Others</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "webmailserver") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'   title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' onchange='updateMails(this) 'value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Gmail") + ">Gmail</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Outlook") + ">Outlook</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AOL") + ">AOL</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Yandex") + ">Yandex</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Yahoo") + ">Yahoo</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "GMX") + ">GMX</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Mail") + ">Mail</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Others") + ">Others</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "folder") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'   title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Inbox") + ">Inbox</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "All") + ">All</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Drafts") + ">Drafts</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Flagged") + ">Flagged</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Junk") + ">Junk</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Trash") + ">Trash</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Sent") + ">Sent</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Archive") + ">Archive</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "inputtype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'   title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "input type 1") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'    title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "input type 2") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "propertyname") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AutomationID") + ">AutomationID</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Name") + ">Name</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "outputtype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "labeltype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "widgettype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Textbox") + ">Textbox</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Checkbox") + ">Checkbox</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Dropdown") + ">Dropdown</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Textarea") + ">Textarea</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Multiple Textbox") + ">Multiple Textbox</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Image + Textbox") + ">Image + Textbox</option></select></td></tr>";
                                //ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Textbox") + ">Textbox</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Checkbox") + ">Checkbox</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Dropdown") + ">Dropdown</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Textarea") + ">Textarea</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "contenttype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Datatable") + ">Datatable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Plaintext") + ">Plaintext</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "success") {
                                var sucessProp = "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 hoverBoard' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + ">";
                                for (var ls in LocalSteps) {
                                    if ((LocalSteps[ls]["Name"] + " , " + LocalSteps[ls]["Order"]).indexOf(localStepProperties[s][sp[i]].StepPropertyValue) == -1)
                                        sucessProp += "<option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "\"" + LocalSteps[ls]["Name"] + "," + ls + "\"") + ">" + LocalSteps[ls]["Name"] + " , " + ls + "</option>"
                                    else
                                        sucessProp += "<option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "\"" + LocalSteps[ls]["Name"] + "," + ls + "\"") + " selected>" + LocalSteps[ls]["Name"] + " , " + ls + "</option>"
                                }
                                sucessProp += "</select></td></tr>"
                                ActionHtml += sucessProp;
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "loopstartid") {

                                var sucessProp = "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + ">";
                                for (var ls in LocalSteps) {
                                    if (LocalSteps[ls]["Action_Id"] == "162") {
                                        if ((LocalSteps[ls]["Name"] + " , " + LocalSteps[ls]["Order"]).indexOf(localStepProperties[s][sp[i]].StepPropertyValue) == -1)
                                            sucessProp += "<option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "\"" + LocalSteps[ls]["Name"] + "," + LocalSteps[ls]["Order"] + "\"") + ">" + LocalSteps[ls]["Name"] + " , " + LocalSteps[ls]["Order"] + "</option>"
                                        else
                                            sucessProp += "<option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "\"" + LocalSteps[ls]["Name"] + "," + LocalSteps[ls]["Order"] + "\"") + " selected>" + LocalSteps[ls]["Name"] + " , " + LocalSteps[ls]["Order"] + "</option>"
                                    }
                                }
                                sucessProp += "</select></td></tr>"
                                ActionHtml += sucessProp;
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "failure") {
                                var failureProp = "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 hoverBoard' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + ">";
                                for (var ls in LocalSteps) {
                                    if (LocalSteps[ls]["Name"] + " , " + LocalSteps[ls]["Order"] != localStepProperties[s][sp[i]].StepPropertyValue)
                                        failureProp += "<option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "\"" + LocalSteps[ls]["Name"] + " , " + ls + "\"") + ">" + LocalSteps[ls]["Name"] + "," + ls + "</option>"
                                    else
                                        failureProp += "<option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "\"" + LocalSteps[ls]["Name"] + " , " + ls + "\"") + " selected>" + LocalSteps[ls]["Name"] + "," + ls + "</option>"
                                }
                                failureProp += "</select></td></tr>"
                                ActionHtml += failureProp;
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "user") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'   title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + ">";
                                for (var user in Usernames) {
                                    if (localStepProperties[s][sp[i]].StepPropertyValue == Usernames[user].ID) {
                                        ActionHtml += "<option selected value='" + Usernames[user].ID + "'>" + Usernames[user]["FirstName"] + " " + Usernames[user]["LastName"] + "</option>"
                                    }
                                    else {
                                        ActionHtml += "<option value='" + Usernames[user].ID + "'>" + Usernames[user]["FirstName"] + " " + Usernames[user]["LastName"] + "</option>"
                                    }
                                }
                            }
                            else {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option></option></select></td></tr>";
                            }
                            break;
                        case "filedialog":
                            ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onchange=\"openFile(event)\" onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            break;
                        case "folderdialog":
                            uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                            ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);updateStepProperties(this);\"/></td></tr>";

                            break;
                        case "folderorfiledialog":
                            if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "file selection local") {
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "source") {
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "classify input file") {
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'   value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "entity input file") {
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sentiment input file") {
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sentenceformation input file") {
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'title='" + localStepProperties[s][sp[i]].DefaultValue + "'   value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "match input file1") {
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfiles(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "match input file2") {
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'title='" + localStepProperties[s][sp[i]].DefaultValue + "'   value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfiles(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></td></tr>";
                            }
                            else {
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                            }
                            break;
                        case "directory":
                            ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            break;

                    }

                }
                else {
                    switch (localStepProperties[s][sp[i]]["StepPropertyType"].trim().toLowerCase()) {

                        case "textbox":
                            if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().indexOf("password") > -1)
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input type='password' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            else if (localStepProperties[s][sp[i]].StepProperty == "Left" || localStepProperties[s][sp[i]].StepProperty == "Top") {
                                //GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='number' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  onchange='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "excel input") {
                                GeneralHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><div class='exopen'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3 excelInput' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + "/></div></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "tesseract input") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3 tesseractInput' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + "/></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3 tesseractInput' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + " readonly/></td></tr>";
                            }
                            else
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='text' name='PropertyInput'autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onkeyup='updateSP(this)' onblur='updateActionSP(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            break;
                        case "checkbox":
                            if (typeof (localStepProperties[s][sp[i]].StepPropertyValue) != typeof (undefined)) {
                                if (localStepProperties[s][sp[i]].StepPropertyValue == true || localStepProperties[s][sp[i]].StepPropertyValue.toLowerCase().trim() == "true")
                                    ActionHtml += "<tr  id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input name='PropertyInput' autocomplete='off' type='checkbox' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' checked " + requiredValue + "/></td></tr>";
                                else
                                    ActionHtml += "<tr  id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input name='PropertyInput' autocomplete='off' type='checkbox' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' " + requiredValue + "/></td></tr>";
                            }
                            else {
                                ActionHtml += "<tr  id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input name='PropertyInput' autocomplete='off' type='checkbox' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' " + requiredValue + "/></td></tr>";
                            }
                            break;
                        case "dropdownlist":
                            if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "comparisontype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sourcetype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option  " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Static") + ">Static</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "datatype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "lefttype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "righttype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "buttons") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "OK") + ">OK</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "OK-Cancel") + ">OK-Cancel</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "readtype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Unread") + ">Unread</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Read") + ">Read</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Star") + ">Star</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Important") + ">Important</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "All") + ">All</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "read type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Unread") + ">Unread</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "All") + ">All</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "category") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Home Timeline") + ">Home Timeline</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Mentions Timeline") + ">Mentions Timeline</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "User Timeline") + ">User Timeline</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "db type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "postgreSQL") + ">postgreSQL</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "MySQL") + ">MySQL</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "MSSQLServer") + ">MSSQLServer</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "download in") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Local") + ">Local</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Server") + ">Server</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "exportin") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Local") + ">Local</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Server") + ">Server</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "connectiontype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='connectiontypechange()'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Static") + " value='static'>Static</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Dynamic") + " value='dynamic'>Dynamic</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "RuntimeInput") + " value='runtimeinput'>RuntimeInput</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "connections") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 connectionscls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedConnection(this)'></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "lob") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 lobcls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedLOB(this)'></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "projectname") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 projectcls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedProject(this)'></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "robotname") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 robotcls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedRobot(this)'></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "template") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 templatecls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "tesseract input type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedTesseractInput(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Input") + ">Input</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "input type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedInput(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Select Option") + ">Select Option</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Input") + ">Input</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File Selection Control") + ">File Selection Control</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "file selection type") {
                                GeneralHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 excelFileSelect' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedFileType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Select Option") + ">Select Option</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Local") + ">Local</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Network") + ">Network</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "file selection network") {
                                GeneralHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 fileNetwork' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "output type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "classify input type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedClassifyType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "entity input type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedEntityType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sentenceformation input type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedSentenceFormationType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sentiment input type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedSentimentType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "isbodyhtml") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "text") + ">text</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "html") + ">html</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "attribute") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Id") + ">Id</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Class") + ">Class</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Name") + ">Name</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Type") + ">Type</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Custom") + ">Custom</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "groupby") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "true") + ">true</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "false") + ">false</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "createtype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "User") + ">User</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Group") + ">Group</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Folder") + ">Folder</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Database") + ">Database</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "deletetype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "User") + ">User</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Group") + ">Group</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Folder") + ">Folder</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Database") + ">Database</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "permissiontype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "All") + ">All</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "None") + ">None</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "ostype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Windows") + ">Windows</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Linux") + ">Linux</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "ostype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Windows") + ">Windows</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Linux") + ">Linux</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "charttype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "pieChart") + ">PieChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AreaChart") + ">AreaChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Lines") + ">Lines</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarOfPie") + ">BarOfPie</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "ColumnGraph") + ">ColumnGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarGraph") + ">BarGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Doughnut") + ">Doughnut</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Bubble") + ">Bubble</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Scatterplot") + ">Scatterplot</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Radar") + ">Radar</option></select></td></tr>";
                            }
                            else {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option></option></select></td></tr>";
                            }
                            break;
                        case "filedialog":
                            GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            break;
                        case "folderdialog":
                            if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "file selection local") {
                                GeneralHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "pieChart") + ">PieChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AreaChart") + ">AreaChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Lines") + ">Lines</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarOfPie") + ">BarOfPie</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "ColumnGraph") + ">ColumnGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarGraph") + ">BarGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Doughnut") + ">Doughnut</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Bubble") + ">Bubble</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Scatterplot") + ">Scatterplot</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Radar") + ">Radar</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "source") {
                                GeneralHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "pieChart") + ">PieChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AreaChart") + ">AreaChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Lines") + ">Lines</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarOfPie") + ">BarOfPie</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "ColumnGraph") + ">ColumnGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarGraph") + ">BarGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Doughnut") + ">Doughnut</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Bubble") + ">Bubble</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Scatterplot") + ">Scatterplot</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Radar") + ">Radar</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "classify input file") {
                                GeneralHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "pieChart") + ">PieChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AreaChart") + ">AreaChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Lines") + ">Lines</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarOfPie") + ">BarOfPie</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "ColumnGraph") + ">ColumnGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarGraph") + ">BarGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Doughnut") + ">Doughnut</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Bubble") + ">Bubble</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Scatterplot") + ">Scatterplot</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Radar") + ">Radar</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "entity input file") {
                                GeneralHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "pieChart") + ">PieChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AreaChart") + ">AreaChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Lines") + ">Lines</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarOfPie") + ">BarOfPie</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "ColumnGraph") + ">ColumnGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarGraph") + ">BarGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Doughnut") + ">Doughnut</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Bubble") + ">Bubble</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Scatterplot") + ">Scatterplot</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Radar") + ">Radar</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sentiment input file") {
                                GeneralHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "pieChart") + ">PieChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AreaChart") + ">AreaChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Lines") + ">Lines</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarOfPie") + ">BarOfPie</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "ColumnGraph") + ">ColumnGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarGraph") + ">BarGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Doughnut") + ">Doughnut</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Bubble") + ">Bubble</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Scatterplot") + ">Scatterplot</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Radar") + ">Radar</option></select></td></tr>";
                            }
                            else {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "pieChart") + ">PieChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AreaChart") + ">AreaChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Lines") + ">Lines</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarOfPie") + ">BarOfPie</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "ColumnGraph") + ">ColumnGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarGraph") + ">BarGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Doughnut") + ">Doughnut</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Bubble") + ">Bubble</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Scatterplot") + ">Scatterplot</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Radar") + ">Radar</option></select></td></tr>";
                            } break;
                        case "directory":
                            GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            break;
                    }
                }
            }
            else {
                reqStr = "";
                requiredValue = "";
                if ((localStepProperties[s][sp[i]].StepProperty != "Name") && (localStepProperties[s][sp[i]].StepProperty != "Description") && (localStepProperties[s][sp[i]].StepProperty != "Type") && (localStepProperties[s][sp[i]].StepProperty != "Goto") && localStepProperties[s][sp[i]].StepProperty != "Left" && localStepProperties[s][sp[i]].StepProperty != "Top" && localStepProperties[s][sp[i]].StepProperty != "DisplayName") {
                    switch (localStepProperties[s][sp[i]]["StepPropertyType"].trim().toLowerCase()) {

                        case "textbox":
                            if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().indexOf("password") > -1)
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input type='password' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "excel input") {
                                if (localStepProperties[s][sp[i - 1]].StepPropertyValue == "Input" || localStepProperties[s][sp[i - 2]].StepPropertyValue == "Input") {
                                    ActionHtml += "<tr id='" + s + "' style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><div class='excel'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3 excelInput' id='sp" + sp[i] + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' order='" + localStepProperties[s][sp[i]].Order + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + "/></div></td></tr>";
                                    $("#sp" + sp[i]).val(localStepProperties[s][sp[i]].StepPropertyValue);
                                }
                                else {
                                    ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><div class='excel'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3 excelInput' id='sp" + sp[i] + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' order='" + localStepProperties[s][sp[i]].Order + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + "/></td></tr>";
                                }

                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "tesseract input") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3 tesseractInput' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + "/></td></tr>";
                                $("#sp" + sp[i]).val(localStepProperties[s][sp[i]].StepPropertyValue);
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3 tesseractInput' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + " readonly/></td></tr>";
                                $("#sp" + sp[i]).val(localStepProperties[s][sp[i]].StepPropertyValue);
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "variables") {
                                if (localStepProperties[s][sp[i - 4]].StepPropertyValue == "Variable") {
                                    ActionHtml += "<tr id='" + s + "'style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><div class='iconfield'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + " /><span class='sampleicon' onclick='loadbVariables(this)'><img id='" + sp[i] + "' src='../Views/dist/img/variable_icon.svg' title='Variable'></span></div></td></tr>";

                                }
                                else {
                                    ActionHtml += "<tr id='" + s + "'style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><div class='iconfield'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + " /><span class='sampleicon' onclick='loadbVariables(this)'><img id='" + sp[i] + "' src='../Views/dist/img/variable_icon.svg' title='Variable'></span></div></td></tr>";

                                }
                                $("#sp" + sp[i]).val(localStepProperties[s][sp[i]].StepPropertyValue);
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "variable") {
                                if (localStepProperties[s][sp[i - 2]].StepPropertyValue == "Variable") {
                                    ActionHtml += "<tr id='" + s + "'style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><div class='iconfield'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + " /><span class='sampleicon' onclick='loadbVariables(this)'><img id='" + sp[i] + "' src='../Views/dist/img/variable_icon.svg' title='Variable'></span></div></td></tr>";

                                }
                                else {
                                    ActionHtml += "<tr id='" + s + "'style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><div class='iconfield'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + " /><span class='sampleicon' onclick='loadbVariables(this)'><img id='" + sp[i] + "' src='../Views/dist/img/variable_icon.svg' title='Variable'></span></div></td></tr>";

                                }
                                $("#sp" + sp[i]).val(localStepProperties[s][sp[i]].StepPropertyValue);
                            }
                            else {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><div class='iconfield'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + " /><span class='sampleicon' onclick='loadbVariables(this)'><img id='" + sp[i] + "' src='../Views/dist/img/variable_icon.svg' title='Variable'></span></div></td></tr>";
                                $("#sp" + sp[i]).val(localStepProperties[s][sp[i]].StepPropertyValue);
                            }
                            break;
                        case "checkbox":
                            if (typeof (localStepProperties[s][sp[i]].StepPropertyValue) != typeof (undefined)) {
                                if (localStepProperties[s][sp[i]].StepPropertyValue == true || localStepProperties[s][sp[i]].StepPropertyValue.toString().toLowerCase().trim() == "true")
                                    ActionHtml += "<tr  id='" + s + "'><td style='float:left;width:unset'><input name='PropertyInput' autocomplete='off' type='checkbox' style='height:20px;width:20px' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' checked " + requiredValue + "/></td><td class='col-sm-1' style='width:unset'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td></tr>";

                                //ActionHtml += "<tr  id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input name='PropertyInput' autocomplete='off' type='checkbox' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' onblur='updateStepProperties(this)' checked " + requiredValue + "/></td></tr>";
                                else
                                    ActionHtml += "<tr id='" + s + "'><td style='float:left;width:unset'><input name='PropertyInput' autocomplete='off' type='checkbox' style='height:20px;width:20px' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' " + requiredValue + "/></td><td class='col-sm-1' style='width:unset'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td></tr>";
                                //ActionHtml += "<tr  id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input name='PropertyInput' autocomplete='off' type='checkbox' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' onblur='updateStepProperties(this)' " + requiredValue + "/></td></tr>";
                            }
                            else {
                                ActionHtml += "<tr id='" + s + "'><td style='float:left;width:unset'><input name='PropertyInput' autocomplete='off' type='checkbox' style='height:20px;width:20px' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' " + requiredValue + "/></td><td class='col-sm-1' style='width:unset'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td></tr>";

                                //ActionHtml += "<tr  id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input name='PropertyInput' autocomplete='off' type='checkbox' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' onblur='updateStepProperties(this)' " + requiredValue + "/></td></tr>";
                            }
                            break;
                        case "dropdownlist":
                            if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "comparisontype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sourcetype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option  " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Static") + ">Static</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "datatype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "lefttype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "righttype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "starttype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "buttons") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "OK") + ">OK</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "OK-Cancel") + ">OK-Cancel</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "readtype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Unread") + ">Unread</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Read") + ">Read</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Star") + ">Star</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Important") + ">Important</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "All") + ">All</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "read type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Unread") + ">Unread</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "All") + ">All</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "category") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Home Timeline") + ">Home Timeline</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Mentions Timeline") + ">Mentions Timeline</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "User Timeline") + ">User Timeline</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "db type") {

                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + ">";
                                for (var e_template in dbTypeDataSource) {
                                    if (localStepProperties[s][sp[i]].StepPropertyValue == dbTypeDataSource[e_template].Name)
                                        ActionHtml += "<option selected value='" + dbTypeDataSource[e_template]["Name"] + "'>" + dbTypeDataSource[e_template]["Name"] + "</option>"
                                    else
                                        ActionHtml += "<option value='" + dbTypeDataSource[e_template]["Name"] + "'>" + dbTypeDataSource[e_template]["Name"] + "</option>"
                                }
                                ActionHtml += "</select></td></tr>"
                                //ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "postgreSQL") + ">postgreSQL</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "MySQL") + ">MySQL</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "MSSQLServer") + ">MSSQLServer</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "download in") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue,
                                ) + ">Local</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Server") + ">Server</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "exportin") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Local") + ">Local</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Server") + ">Server</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "connectiontype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='connectiontypechange(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "static") + " value='static'>Static</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "dynamic") + " value='dynamic'>Dynamic</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "runtimeinput") + " value='runtimeinput'>RuntimeInput</option></select></td></tr>";
                                var frmdt = { "id": "sp" + sp[i], "methodName": "onchange", "value": localStepProperties[s][sp[i]].StepPropertyValue };
                                arr_ddl.push(frmdt);
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "connections") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 connectionscls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedConnection(this)'></select></td></tr>";

                                var frmdt = { "id": "sp" + sp[i], "methodName": "", "value": localStepProperties[s][sp[i]].StepPropertyValue };
                                arr_ddl.push(frmdt);
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "lob") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 lobcls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedLOB(this)'></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "projectname") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 projectcls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedProject(this)'></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "robotname") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 robotcls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedRobot(this)'></select></td></tr>";
                            }

                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "template") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + ">";
                                for (var e_template in emailTemplateDataSource) {
                                    if (localStepProperties[s][sp[i]].StepPropertyValue == emailTemplateDataSource[e_template].ID)
                                        ActionHtml += "<option selected value='" + emailTemplateDataSource[e_template].ID + "'>" + emailTemplateDataSource[e_template]["Subject"] + "</option>"
                                    else
                                        ActionHtml += "<option value='" + emailTemplateDataSource[e_template].ID + "'>" + emailTemplateDataSource[e_template]["Subject"] + "</option>"
                                }
                                ActionHtml += "</select></td></tr>"
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "tesseract input type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedTesseractInput(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Select Option") + ">Select Option</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Input") + ">Input</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";

                                var frmdt = { "id": "sp" + sp[i], "methodName": "", "value": localStepProperties[s][sp[i]].StepPropertyValue };
                                arr_ddl.push(frmdt);
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "input type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedInput(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Select Option") + ">Select Option</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Input") + ">Input</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File Selection Control") + ">File Selection Control</option></select></td></tr>";
                                var frmdt = { "id": "sp" + sp[i], "methodName": "", "value": localStepProperties[s][sp[i]].StepPropertyValue };
                                arr_ddl.push(frmdt);
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "file selection type") {
                                if (localStepProperties[s][sp[i - 2]].StepPropertyValue == "File Selection Control" || localStepProperties[s][sp[i - 3]].StepPropertyValue == "File Selection Control") {
                                    ActionHtml += "<tr id='" + s + "' style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 excelFileSelect' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedFileType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Select Option") + ">Select Option</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Local") + ">Local</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Network") + ">Network</option></select></td></tr>";
                                    if (localStepProperties[s][sp[i - (-1)]].StepProperty.toLowerCase().trim() == "file selection local" && localStepProperties[s][sp[i]].StepPropertyValue == "Local") {
                                        uploadfolderpath.push(localStepProperties[s][sp[i - (-1)]].StepPropertyValue)
                                        if (localStepProperties[s][sp[i]].StepPropertyValue == "Local") {
                                            ActionHtml += "<tr id='" + s + "' style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i - (-1)]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i - (-1)]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i - (-1)]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                        }
                                        else {
                                            ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i - (-1)]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i - (-1)]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i - (-1)]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                        }
                                    }
                                    if (localStepProperties[s][sp[i - (-2)]].StepProperty.toLowerCase().trim() == "file selection network" && localStepProperties[s][sp[i]].StepPropertyValue == "Network") {
                                        if (localStepProperties[s][sp[i]].StepPropertyValue == "Network") {
                                            ActionHtml += "<tr id='" + s + "' style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i - (-2)]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 fileNetwork' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i - (-2)]].Order + "' title='" + localStepProperties[s][sp[i - (-2)]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i - (-2)]].StepPropertyValue + "\" " + requiredValue + "></select></td></tr>";
                                        }
                                        else {
                                            ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i - (-2)]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 fileNetwork' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i - (-2)]].Order + "' title='" + localStepProperties[s][sp[i - (-2)]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i - (-2)]].StepPropertyValue + "\" " + requiredValue + "></select></td></tr>";
                                        }
                                        var frmdt = { "id": "sp" + sp[i], "methodName": "", "value": localStepProperties[s][sp[i]].StepPropertyValue };
                                        arr_ddl.push(frmdt);
                                    }

                                }
                                else {
                                    ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 excelFileSelect' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedFileType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Select Option") + ">Select Option</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Local") + ">Local</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Network") + ">Network</option></select></td></tr>";
                                }
                                var frmdt = { "id": "sp" + sp[i], "methodName": "", "value": localStepProperties[s][sp[i]].StepPropertyValue };
                                arr_ddl.push(frmdt);
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "file selection network") {
                                if (localStepProperties[s][sp[i - 3]].StepPropertyValue == "Network") {
                                    ActionHtml += "<tr id='" + s + "' style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 fileNetwork' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "></select></td></tr>";
                                }
                                else {
                                    ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 fileNetwork' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "></select></td></tr>";
                                }
                                var frmdt = { "id": "sp" + sp[i], "methodName": "", "value": localStepProperties[s][sp[i]].StepPropertyValue };
                                arr_ddl.push(frmdt);
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "output type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option></select></td></tr>";
                                var frmdt = { "id": "sp" + sp[i], "methodName": "", "value": localStepProperties[s][sp[i]].StepPropertyValue };
                                arr_ddl.push(frmdt);
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "condition") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "!=") + ">!=</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "==") + ">==</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "<") + "><</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, ">") + ">></option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "<=") + "><=</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, ">=") + ">>=</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "isbodyhtml") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "text") + ">text</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "html") + ">html</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "attribute") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Id") + ">Id</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Class") + ">Class</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Name") + ">Name</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Type") + ">Type</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Custom") + ">Custom</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sourcetype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Static") + ">Static</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "classify input type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedClassifyType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "entity input type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedEntityType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sentiment input type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedSentimentType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sentenceformation input type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedSentenceFormationType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "match input type1") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedMatchType1(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Input") + ">Input</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "match input type2") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedMatchType2(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Input") + ">Input</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "groupby") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "true") + ">true</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "false") + ">false</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "createtype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "User") + ">User</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Group") + ">Group</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Folder") + ">Folder</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Database") + ">Database</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "deletetype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "User") + ">User</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Group") + ">Group</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Folder") + ">Folder</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Database") + ">Database</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "permissiontype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "All") + ">All</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "None") + ">None</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "ostype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Windows") + ">Windows</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Linux") + ">Linux</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "protocol") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "IMAP") + ">IMAP</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "POP3") + ">POP3</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Others") + ">Others</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "charttype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "pieChart") + ">PieChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AreaChart") + ">AreaChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Lines") + ">Lines</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarOfPie") + ">BarOfPie</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "ColumnGraph") + ">ColumnGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarGraph") + ">BarGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Doughnut") + ">Doughnut</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Bubble") + ">Bubble</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Scatterplot") + ">Scatterplot</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Radar") + ">Radar</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "mail server") {
                                //  ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' onblur='updateStepProperties(this)' onchange='updateMails(this) 'value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Gmail") + ">Gmail</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Outlook") + ">Outlook</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AOL") + ">AOL</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Yandex") + ">Yandex</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Yahoo") + ">Yahoo</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "GMX") + ">GMX</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Mail") + ">Mail</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Others") + ">Others</option></select></td></tr>";
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' onchange='updateMails(this) ' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Gmail") + ">Gmail</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Others") + ">Others</option></select></td></tr>";

                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "webmailserver") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' onchange='updateMails(this) 'value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Gmail") + ">Gmail</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Outlook") + ">Outlook</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AOL") + ">AOL</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Yandex") + ">Yandex</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Yahoo") + ">Yahoo</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "GMX") + ">GMX</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Mail") + ">Mail</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Others") + ">Others</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "folder") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Inbox") + ">Inbox</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "All") + ">All</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Drafts") + ">Drafts</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Flagged") + ">Flagged</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Junk") + ">Junk</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Trash") + ">Trash</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Sent") + ">Sent</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Archive") + ">Archive</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "inputtype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "input type 1") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "input type 2") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "propertyname") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AutomationID") + ">AutomationID</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Name") + ">Name</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "task") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Read") + ">Read</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Write") + ">Write</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Update") + ">Update</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "outputtype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "labeltype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "selectiontype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='emailSelection(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Select Option") + ">Select Option</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "FileAttachment") + ">FileAttachment</option></select></td></tr>";
                                var frmdt = { "id": "sp" + sp[i], "methodName": "", "value": localStepProperties[s][sp[i]].StepPropertyValue };
                                arr_ddl.push(frmdt);
                                //ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='emailSelection(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Select Option") + ">Select Option</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "FileAttachement") + ">FileAttachement</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "widgettype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Select Option") + ">SelectOption</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Textbox") + ">Textbox</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Checkbox") + ">Checkbox</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Dropdown") + ">Dropdown</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Textarea") + ">Textarea</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Multiple Textbox") + ">Multiple Textbox</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Image + Textbox") + ">Image + Textbox</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Image + Textbox + Checkbox") + ">Image + Textbox + Checkbox</option></select></td></tr>";
                                //ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Textbox") + ">Textbox</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Checkbox") + ">Checkbox</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Dropdown") + ">Dropdown</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Textarea") + ">Textarea</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "selecttype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='replyselection(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Select Option") + ">Select Option</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "FileAttachments") + ">FileAttachments</option></select></td></tr>";
                                var frmdt = { "id": "sp" + sp[i], "methodName": "", "value": localStepProperties[s][sp[i]].StepPropertyValue };
                                arr_ddl.push(frmdt);
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "contenttype") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Datatable") + ">Datatable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Plaintext") + ">Plaintext</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "success") {
                                var sucessProp = "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 hoverBoard' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + ">";
                                for (var ls in LocalSteps) {
                                    if ((LocalSteps[ls]["Name"] + " , " + ls).indexOf(localStepProperties[s][sp[i]].StepPropertyValue) == -1)
                                        sucessProp += "<option>" + LocalSteps[ls]["Name"] + " , " + ls + "</option>"
                                    else
                                        sucessProp += "<option selected>" + LocalSteps[ls]["Name"] + " , " + ls + "</option>"
                                }
                                sucessProp += "</select></td></tr>"
                                ActionHtml += sucessProp;
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "loopstartid") {
                                var lstartProp = "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + ">";
                                for (var ls in LocalSteps) {
                                    if (LocalSteps[ls]["Action_Id"] == "162") {
                                        if ((LocalSteps[ls]["Name"] + " , " + LocalSteps[ls]["Order"]).indexOf(localStepProperties[s][sp[i]].StepPropertyValue) == -1)
                                            lstartProp += "<option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "\"" + LocalSteps[ls]["Name"] + " , " + LocalSteps[ls]["Order"] + "\"") + ">" + LocalSteps[ls]["Name"] + " , " + LocalSteps[ls]["Order"] + "</option>"
                                        else
                                            lstartProp += "<option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "\"" + LocalSteps[ls]["Name"] + " , " + LocalSteps[ls]["Order"] + "\"") + " selected>" + LocalSteps[ls]["Name"] + " , " + LocalSteps[ls]["Order"] + "</option>"
                                    }
                                }
                                lstartProp += "</select></td></tr>"
                                ActionHtml += lstartProp;
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "failure") {
                                var failureProp = "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 hoverBoard' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + ">";
                                for (var ls in LocalSteps) {
                                    if ((LocalSteps[ls]["Name"] + " , " + ls).indexOf(localStepProperties[s][sp[i]].StepPropertyValue) == -1)
                                        failureProp += "<option>" + LocalSteps[ls]["Name"] + " , " + ls + "</option>"
                                    else
                                        failureProp += "<option selected>" + LocalSteps[ls]["Name"] + " , " + ls + "</option>"
                                }
                                failureProp += "</select></td></tr>"
                                ActionHtml += failureProp;
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "user") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + ">";
                                for (var user in Usernames) {
                                    if (localStepProperties[s][sp[i]].StepPropertyValue == Usernames[user].ID)
                                        ActionHtml += "<option selected value='" + Usernames[user].ID + "'>" + Usernames[user]["FirstName"] + " " + Usernames[user]["LastName"] + "</option>"
                                    else
                                        ActionHtml += "<option value='" + Usernames[user].ID + "'>" + Usernames[user]["FirstName"] + " " + Usernames[user]["LastName"] + "</option>"
                                }
                            }
                            else {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option></option></select></td></tr>";
                            }
                            break;
                        case "filedialog":
                            ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onchange=\"openFile(event)\" onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            break;
                        case "folderdialog":
                            uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                            ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);updateStepProperties(this);\"/></td></tr>";
                            break;
                        case "folderorfiledialog":
                            if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "file selection local") {
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                if (localStepProperties[s][sp[i - 2]].StepPropertyValue == "Local" && localStepProperties[s][sp[i - 5]].StepPropertyValue == "File Selection Control") {
                                    ActionHtml += "<tr id='" + s + "' style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                                else {
                                    ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                            }
                           else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "file selection network") {
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                if (localStepProperties[s][sp[i - 3]].StepPropertyValue == "Network" && localStepProperties[s][sp[i - 6]].StepPropertyValue == "File Selection Control") {
                                    ActionHtml += "<tr id='" + s + "' style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                                else {
                                    ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                            }

                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "source") {
                                if (localStepProperties[s][sp[i - 2]].StepPropertyValue == "File") {
                                    uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                    ActionHtml += "<tr id='" + s + "' style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "classify input file") {
                                if (localStepProperties[s][sp[i - 2]].StepPropertyValue == "File") {
                                    uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                    ActionHtml += "<tr id='" + s + "' style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'   value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                                else {
                                    uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                    ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'   value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "entity input file") {
                                if (localStepProperties[s][sp[i - 3]].StepPropertyValue == "File") {
                                    uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                    ActionHtml += "<tr id='" + s + "' style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sentiment input file") {
                                if (localStepProperties[s][sp[i - 1]].StepPropertyValue == "File") {
                                    uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                    ActionHtml += "<tr id='" + s + "' style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                                else {
                                    uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                    ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sentenceformation input file") {
                                if (localStepProperties[s][sp[i - 2]].StepPropertyValue == "File") {
                                    uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                    ActionHtml += "<tr id='" + s + "' style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                                else {
                                    uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                    ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "match input file1") {
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfiles(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "match input file2") {
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                ActionHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfiles(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "attachments") {
                                if (localStepProperties[s][sp[i - 2]].StepPropertyValue == "FileAttachment") {
                                    uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                    ActionHtml += "<tr id='" + s + "' style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                                else {
                                    uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                    ActionHtml += "<tr id='" + s + "'' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                                $("#sp" + sp[i]).val(localStepProperties[s][sp[i]].StepPropertyValue);
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "attachment") {
                                if (localStepProperties[s][sp[i - 1]].StepPropertyValue == "FileAttachments") {
                                    uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                    ActionHtml += "<tr id='" + s + "' style='display:block'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                                else {
                                    uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                    ActionHtml += "<tr id='" + s + "'' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                                }
                                $("#sp" + sp[i]).val(localStepProperties[s][sp[i]].StepPropertyValue);
                            }
                            else {
                                uploadfolderpath.push(localStepProperties[s][sp[i]].StepPropertyValue)
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2' style='display:none'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><div class='encode1'><input type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%;display:none''  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange=\"uploadfile(event);this.type='text';this.value=changedURL;updateStepProperties(this);\"onclick='changefilefolder(this)'/></div></td></tr>";
                            }
                            break;
                        case "directory":
                            ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            break;
                    }
                }
                else {
                    switch (localStepProperties[s][sp[i]]["StepPropertyType"].trim().toLowerCase()) {

                        case "textbox":
                            if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().indexOf("password") > -1)
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input type='password' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            else if (localStepProperties[s][sp[i]].StepProperty == "Left" || localStepProperties[s][sp[i]].StepProperty == "Top") {
                                // GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='number' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  onchange='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "excel input") {
                                GeneralHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><div class='exopen'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3 excelInput' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + "/></div></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "tesseract input") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3 tesseractInput' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + "/></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3 tesseractInput' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value='" + localStepProperties[s][sp[i]].StepPropertyValue + "'" + requiredValue + " readonly/></td></tr>";
                            }
                            else
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "'onkeyup='updateSP(this)' onblur='updateActionSP(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            break;
                        case "checkbox":
                            if (typeof (localStepProperties[s][sp[i]].StepPropertyValue) != typeof (undefined)) {
                                if (localStepProperties[s][sp[i]].StepPropertyValue == true || localStepProperties[s][sp[i]].StepPropertyValue.toLowerCase().trim() == "true")
                                    ActionHtml += "<tr  id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + " " + reqStr + "</td><td class='col-sm-10'><input name='PropertyInput' autocomplete='off' type='checkbox' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' checked " + requiredValue + "/></td></tr>";
                                else
                                    ActionHtml += "<tr  id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input name='PropertyInput' autocomplete='off' type='checkbox' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' " + requiredValue + "/></td></tr>";
                            }
                            else {
                                ActionHtml += "<tr  id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input name='PropertyInput' autocomplete='off' type='checkbox' class='checker' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' " + requiredValue + "/></td></tr>";
                            }
                            break;
                        case "dropdownlist":
                            if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "comparisontype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "sourcetype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option  " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Static") + ">Static</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "datatype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "lefttype" || localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "righttype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "buttons") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "OK") + ">OK</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "OK-Cancel") + ">OK-Cancel</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "readtype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Unread") + ">Unread</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Read") + ">Read</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Star") + ">Star</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Important") + ">Important</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "All") + ">All</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "read type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Unread") + ">Unread</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "All") + ">All</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "category") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Home Timeline") + ">Home Timeline</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Mentions Timeline") + ">Mentions Timeline</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "User Timeline") + ">User Timeline</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "db type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "postgreSQL") + ">postgreSQL</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "MySQL") + ">MySQL</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "MSSQLServer") + ">MSSQLServer</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "download in") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Local") + ">Local</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Server") + ">Server</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "exportin") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Local") + ">Local</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Server") + ">Server</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "connectiontype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='connectiontypechange()'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Static") + " value='dynamic'>Static</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Dynamic") + " value='dynamic'>Dynamic</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "RuntimeInput") + " value='runtimeinput'>RuntimeInput</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "connections") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 connectionscls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedConnection(this)'></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "classify input type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedClassifyType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "entity input type") {
                                ActionHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedEntityType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "lob") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 lobcls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedLOB(this)'></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "projectname") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 projectcls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedProject(this)'></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "robotname") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 robotcls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedRobot(this)'></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "template") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 templatecls' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "tesseract input type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedTesseractInput(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Input") + ">Input</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "input type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedInput(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Select Option") + ">Select Option</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Input") + ">Input</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File Selection Control") + ">File Selection Control</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "file selection type") {
                                GeneralHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 excelFileSelect' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + " onchange='selectedFileType(this)'><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Select Option") + ">Select Option</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Local") + ">Local</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Network") + ">Network</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "file selection network") {
                                GeneralHtml += "<tr id='" + s + "'style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3 fileNetwork' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "output type") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Variable") + ">Variable</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Value") + ">Value</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "isbodyhtml") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "text") + ">text</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "html") + ">html</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "attribute") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Id") + ">Id</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Class") + ">Class</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Name") + ">Name</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Type") + ">Type</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Custom") + ">Custom</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "groupby") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "true") + ">true</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "false") + ">false</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "createtype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "User") + ">User</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Group") + ">Group</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Folder") + ">Folder</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Database") + ">Database</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "deletetype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "User") + ">User</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Group") + ">Group</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "File") + ">File</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Folder") + ">Folder</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Database") + ">Database</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "permissiontype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "All") + ">All</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "None") + ">None</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "ostype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' v onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Windows") + ">Windows</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Linux") + ">Linux</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "ostype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Windows") + ">Windows</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Linux") + ">Linux</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "charttype") {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "pieChart") + ">PieChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AreaChart") + ">AreaChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Lines") + ">Lines</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarOfPie") + ">BarOfPie</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "ColumnGraph") + ">ColumnGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarGraph") + ">BarGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Doughnut") + ">Doughnut</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Bubble") + ">Bubble</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Scatterplot") + ">Scatterplot</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Radar") + ">Radar</option></select></td></tr>";
                            }
                            else {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option></option></select></td></tr>";
                            }
                            break;
                        case "filedialog":
                            GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='file' name='PropertyInput' class='filechange' style='width:100%' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            break;
                        case "folderdialog":
                            if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "file selection local") {
                                GeneralHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "pieChart") + ">PieChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AreaChart") + ">AreaChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Lines") + ">Lines</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarOfPie") + ">BarOfPie</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "ColumnGraph") + ">ColumnGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarGraph") + ">BarGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Doughnut") + ">Doughnut</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Bubble") + ">Bubble</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Scatterplot") + ">Scatterplot</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Radar") + ">Radar</option></select></td></tr>";
                            }
                            else if (localStepProperties[s][sp[i]].StepProperty.toLowerCase().trim() == "source") {
                                GeneralHtml += "<tr id='" + s + "' style='display:none'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "pieChart") + ">PieChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AreaChart") + ">AreaChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Lines") + ">Lines</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarOfPie") + ">BarOfPie</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "ColumnGraph") + ">ColumnGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarGraph") + ">BarGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Doughnut") + ">Doughnut</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Bubble") + ">Bubble</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Scatterplot") + ">Scatterplot</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Radar") + ">Radar</option></select></td></tr>";
                            } else {
                                GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><select name='PropertyInput' class='form-control col-sm-3' id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "' title='" + localStepProperties[s][sp[i]].DefaultValue + "'  onblur='updateStepProperties(this)' value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "pieChart") + ">PieChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "AreaChart") + ">AreaChart</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Lines") + ">Lines</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarOfPie") + ">BarOfPie</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "ColumnGraph") + ">ColumnGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "BarGraph") + ">BarGraph</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Doughnut") + ">Doughnut</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Bubble") + ">Bubble</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Scatterplot") + ">Scatterplot</option><option " + validateValue(localStepProperties[s][sp[i]].StepPropertyValue, "Radar") + ">Radar</option></select></td></tr>";
                            }
                            break;
                        case "directory":
                            GeneralHtml += "<tr id='" + s + "'><td class='col-sm-2'>" + localStepProperties[s][sp[i]].StepProperty + "" + reqStr + "</td><td class='col-sm-10'><input type='file' name='PropertyInput' class='' style='width:100%'  id='sp" + sp[i] + "' order='" + localStepProperties[s][sp[i]].Order + "'  title='" + localStepProperties[s][sp[i]].DefaultValue + "' onblur='updateStepProperties(this)' webkitdirectory directory multiple value=\"" + localStepProperties[s][sp[i]].StepPropertyValue + "\" " + requiredValue + "/></td></tr>";
                            break;
                    }
                }
            }
        }
    }
    $(document).on("mouseover", ".iconfield", function (e) {
        var str = $($(e.currentTarget.innerHTML)[0]).attr("id");
        var res = str.slice(2, 8);
        $("#" + res).css("visibility", "visible");
    });
    $(document).on("mouseout", ".iconfield", function (e) {
        var str = $($(e.currentTarget.innerHTML)[0]).attr("id");
        var res = str.slice(2, 8);
        $("#" + res).css("visibility", "hidden");
    });
    $(document).on("mouseover", "select.hoverBoard", function () {
        var n = $(this).children('option').length;
        $(this).attr("size", n);
    });
    $(document).on("mouseout", "select.hoverBoard", function () {
        $(this).attr("size", 1);
        $(this).children('options').css('color', 'initial');
    });
    $(document).on('mouseenter', 'option', function (e) {
        var $target = $(e.target);
        $target.css('color', '#ff5500');
        //console.log($target.text());
        var stpname = $target.text().split(',')[0].trim();
        var stpordr = $target.text().split(',')[1].trim();

        for (var q = 0; q < myDiagram.model.De.length; q++) {
            if (myDiagram.model.De[q].StepID == stpordr) {
                var key = myDiagram.model.De[q];
                myDiagram.model.setDataProperty(key, 'color', "skyblue");
                //console.log(myDiagram.model.De[q]);
            }
        }
    });
    $(document).on('mouseleave', 'option', function (e) {
        var $target = $(e.target);
        $target.css('color', 'initial');
        //console.log($target.text());

        var stpname = $target.text().split(',')[0].trim();
        var stpordr = $target.text().split(',')[1].trim();

        for (var q = 0; q < myDiagram.model.De.length; q++) {
            if (myDiagram.model.De[q].StepID == stpordr) {
                var key = myDiagram.model.De[q];
                myDiagram.model.setDataProperty(key, 'color', "initial");
                console.log(myDiagram.model.De[q]);
            }
        }
    });

    if (GeneralHtml != "<tr><th>General</th><th></th></tr>")
        $("#PropertiesBody")[0].innerHTML += GeneralHtml;

    if (ActionHtml != "<tr><th>" + step_name + "</th><th></th></tr>" && step_name != "Start" && step_name != "Stop")
        $("#PropertiesBody")[0].innerHTML += ActionHtml;
    $(".connectionscls").append('<option>select an option</option>')
    $(".lobcls").append('<option>select an option</option>');
    $(".projectcls").append('<option>select an option</option>');
    $(".robotcls").append('<option>select an option</option>');
    $(".templatecls").append('<option>select an option</option>');
    for (var i in LOB) {
        $(".lobcls").append('<option value=' + LOB[i].ID + '>' + LOB[i].Name + '</option>')
    }
    for (var i in emailTemplateDataSource) {
        $(".templatecls").append('<option value=' + emailTemplateDataSource[i].ID + '>' + emailTemplateDataSource[i].Subject + '</option>')
    }
    for (var i in ExcelActionUploadFiles) {
        $(".fileNetwork").append('<option value=' + ExcelActionUploadFiles[i] + '>' + ExcelActionUploadFiles[i] + '</option>')
    }
    if (arr_ddl.length > 0) {
        $.each(arr_ddl, function (k, p) {
            if (p.value.length > 0) {
                $('select[id="' + p.id + '"] option[value="' + p.value + '"]').attr("selected", "selected");
            }
            if (p.methodName.length > 0) {
                $("#" + p.id).trigger(p.methodName);
            }
        });

        arr_ddl = [];
    }
}
function selectedInput(e) {
    var trArray = [];
    trArray = $("#" + $(e.parentElement.parentElement)[0].id).siblings();
    if ($(e).val() == "File Selection Control") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText == "File Selection Type") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "Excel Input") {
                    $(trArray[i]).hide();
                }
                else if ($(trArray[i].children[0])[0].innerText == "File Selection Local") {
                    $(trArray[i]).hide();
                }
                else if ($(trArray[i].children[0])[0].innerText == "File Selection Network") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Input") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText == "Excel Input ") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "File Selection Type") {
                    $(trArray[i]).hide();
                }
                else if ($(trArray[i].children[0])[0].innerText == "File Selection Local") {
                    $(trArray[i]).hide();
                }
                else if ($(trArray[i].children[0])[0].innerText == "File Selection Network") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
}
function selectedFileType(e) {
    var trArray = [];
    trArray = $("#" + $(e.parentElement.parentElement)[0].id).siblings();
    if ($(e).val() == "Local") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText == "File Selection Local") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "File Selection Network") {
                    $(trArray[i]).hide();
                }
                else if ($(trArray[i].children[0])[0].innerText == "Excel Input") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Network") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText == "File Selection Network") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "File Selection Local") {
                    $(trArray[i]).hide();
                }
                else if ($(trArray[i].children[0])[0].innerText == "Excel Input") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
}
function emailSelection(e) {
    var trArray = [];
    trArray = $("#" + $(e.parentElement.parentElement)[0].id).siblings();
    if ($(e).val() == "FileAttachment") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText == "Attachments") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText.trim() == "Variables") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Variable") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText.trim() == "Variables") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "Attachments") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
}
function replyselection(e) {
    var trArray = [];
    trArray = $("#" + $(e.parentElement.parentElement)[0].id).siblings();
    if ($(e).val() == "FileAttachments") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText == "Attachment") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText.trim() == "Variable") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Variable") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText.trim() == "Variable") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "Attachment") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
}



function selectedTesseractInput(e) {
    var trArray = [];
    trArray = $("#" + $(e.parentElement.parentElement)[0].id).siblings();
    if ($(e).val() == "File") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText == "Source") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText.trim() == "Tesseract Input") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Input") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText.trim() == "Tesseract Input") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "Source") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
}
function selectedClassifyType(e) {
    var trArray = [];
    trArray = $("#" + $(e.parentElement.parentElement)[0].id).siblings();
    if ($(e).val() == "File") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText == "Classify Input File") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText.trim() == "Input") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Variable") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText.trim() == "Input") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "Classify Input File") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Value") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText.trim() == "Input") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "Classify Input File") {
                    $(trArray[i]).hide();
                }
            }
        }
    }

}
function selectedEntityType(e) {
    var trArray = [];
    trArray = $("#" + $(e.parentElement.parentElement)[0].id).siblings();
    if ($(e).val() == "File") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText == "Entity Input File") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText.trim() == "Input") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Variable") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText.trim() == "Input") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "Entity Input File") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Value") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText.trim() == "Input") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "Entity Input File") {
                    $(trArray[i]).hide();
                }
            }
        }
    }

}
function selectedSentimentType(e) {
    var trArray = [];
    trArray = $("#" + $(e.parentElement.parentElement)[0].id).siblings();
    if ($(e).val() == "File") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText == "Sentiment Input File") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText.trim() == "Input") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Variable") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText.trim() == "Input") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "Sentiment Input File") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Value") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText.trim() == "Input") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "Sentiment Input File") {
                    $(trArray[i]).hide();
                }
            }
        }
    }

}
function selectedSentenceFormationType(e) {
    var trArray = [];
    trArray = $("#" + $(e.parentElement.parentElement)[0].id).siblings();
    if ($(e).val() == "File") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText == "SentenceFormation Input File") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText.trim() == "Input") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Variable") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText.trim() == "Input") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "SentenceFormation Input File") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Value") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText.trim() == "Input") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "SentenceFormation Input File") {
                    $(trArray[i]).hide();
                }
            }
        }
    }

}
function selectedMatchType1(e) {
    var trArray = [];
    trArray = $("#" + $(e.parentElement.parentElement)[0].id).siblings();
    if ($(e).val() == "File") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText == "Match Input File1") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText.trim() == "Match Input1") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Input") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText.trim() == "Match Input1") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "Match Input File1") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
}
function selectedMatchType2(e) {
    var trArray = [];
    trArray = $("#" + $(e.parentElement.parentElement)[0].id).siblings();
    if ($(e).val() == "File") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText == "Match Input File2") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText.trim() == "Match Input2") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
    else if ($(e).val() == "Input") {
        for (var i in trArray) {
            if (parseInt(i) < trArray.length) {
                if ($(trArray[i].children[0])[0].innerText.trim() == "Match Input2") {
                    $(trArray[i]).show();
                }
                else if ($(trArray[i].children[0])[0].innerText == "Match Input File2") {
                    $(trArray[i]).hide();
                }
            }
        }
    }
}
function connectiontypechange(e) {
    if ($(e).val() == "dynamic") {

        load_DBConnections();
        $(".connectionscls").html('');
        $(".connectionscls").html('<option>select an option</option>');
        for (var i in CnctDataSource) {
            if (CnctDataSource[i]["connectiontype"] == "Database") {
                $(".connectionscls").append('<option value="' + CnctDataSource[i]["ID"] + '">' + CnctDataSource[i]["connectionname"] + '</option>');
            }
        }
    }
    if ($(e).val() == "static") {

        $(".connectionscls").html('<option>select an option</option>');
    }
}
var ConnectionDBName, ConnectionDBUsername, ConnectionDBPassword, ConnectionDBPort;

function selectedConnection(e) {
    var selectedValueOfConnection = $(e).val();


    for (var i in CnctDataSource) {
        if (CnctDataSource[i].ID == selectedValueOfConnection) {

            ConnectionDBName = CnctDataSource[i].connectionname;
            ConnectionDBPassword = CnctDataSource[i].password;
            ConnectionDBUsername = CnctDataSource[i].username;
            ConnectionDBPort = CnctDataSource[i].port;
            ConnectionDBUrl = CnctDataSource[i].hostname;

            $.each($('#PropertiesBody tr'), function (i, j) {
                if ($('#PropertiesBody tr')[i].children[0].innerHTML.trim() == "DB Name") {
                    $('#PropertiesBody tr')[i].children[1].children[0].value = ConnectionDBName;

                    $("#" + $($('#PropertiesBody tr')[i].children[1].children[0]).attr("id")).trigger("onblur");
                }
                if ($('#PropertiesBody tr')[i].children[0].innerHTML.trim() == "DB Password") {
                    $('#PropertiesBody tr')[i].children[1].children[0].value = ConnectionDBPassword;

                    $("#" + $($('#PropertiesBody tr')[i].children[1].children[0]).attr("id")).trigger("onblur");
                }
                if ($('#PropertiesBody tr')[i].children[0].innerHTML.trim() == "DB Username") {
                    $('#PropertiesBody tr')[i].children[1].children[0].value = ConnectionDBUsername;

                    $("#" + $($('#PropertiesBody tr')[i].children[1].children[0]).attr("id")).trigger("onblur");
                }
                if ($('#PropertiesBody tr')[i].children[0].innerHTML.trim() == "DB Port") {
                    $('#PropertiesBody tr')[i].children[1].children[0].value = ConnectionDBPort;

                    $("#" + $($('#PropertiesBody tr')[i].children[1].children[0]).attr("id")).trigger("onblur");
                }
                if ($('#PropertiesBody tr')[i].children[0].innerHTML.trim() == "DB URL") {
                    $('#PropertiesBody tr')[i].children[1].children[0].value = ConnectionDBUrl;

                    $("#" + $($('#PropertiesBody tr')[i].children[1].children[0]).attr("id")).trigger("onblur");
                }
            });
        }
    }
}
function selectedLOB(e) {
    var LOBIDForProject = $(e).val();
    $(".projectcls").html("");
    $(".projectcls").append('<option>select an option</option>')
    for (var i in Project) {
        if (LOBIDForProject == Project[i].LOBId) {
            $(".projectcls").append('<option value=' + Project[i].ID + '>' + Project[i].NAME + '</option>')
        }
    }

}
function selectedProject(e) {

    var ProjectIDForRobot = $(e).val();
    $(".robotcls").html("");
    $(".robotcls").append('<option>select an option</option>');
    for (var i in DropDownRobots) {
        if (ProjectIDForRobot == DropDownRobots[i].Project_Id) {
            $(".robotcls").append('<option value=' + DropDownRobots[i].Id + '>' + DropDownRobots[i].Name + '</option>')
        }
    }
}
function validateValue(value, inputValue) {
    if (value == inputValue) {
        return "selected";
    }
    else {
        return "";
    }
}
var eventE;
var uploadList = [];
var changedURL = "";
var uploadfile = function (event) {
    uploadList = [];
    var input = event.target;
    changedURL = event.target.files[0].name
    var reader = new FileReader();
    reader.onload = function () {
        var dataURL = reader.result;
        var output = event.target;
        output.src = dataURL;
    };
    for (var i = 0; i < input.files.length; i++) {
        var reader = new FileReader();
        reader.readAsDataURL(input.files[i]);
        if (isfolder)
            reader.name = input.files[i].webkitRelativePath;
        else
            reader.name = input.files[i].name;
        uploadList.push(reader);
    }
};
var uploadfiles = function (event) {
    var input = event.target;
    changedURL = event.target.files[0].name
    var reader = new FileReader();
    reader.onload = function () {
        var dataURL = reader.result;
        var output = event.target;
        output.src = dataURL;
    };
    for (var i = 0; i < input.files.length; i++) {
        var reader = new FileReader();
        reader.readAsDataURL(input.files[i]);
        if (isfolder)
            reader.name = input.files[i].webkitRelativePath;
        else
            reader.name = input.files[i].name;
        uploadList.push(reader);
    }
};

function updateSP(e) {
    if (e.type == "file") {

    }
    if ($(e).parent().closest('tr').children()[0].innerHTML == "DisplayName" || $(e).parent().closest('tr').children()[0].innerHTML == "Name") {
        var rename = myDiagram.Ei.Ea;
        while (rename.Xa != null) {

            rename = rename.Xa

            if (rename.key.StepID == $(e).parent().parent()[0].id) {
                myDiagram.model.setDataProperty(rename.key, 'DisplayName', $(e).val());
            }
        }

    }
    else if ($(e).parent().closest('tr').children()[0].innerHTML == "Left") {
        var rename = myDiagram.Ei.Ea;
        while (rename.Xa != null) {

            rename = rename.Xa
            if (rename.key.StepID == $(e).parent().parent()[0].id) {

                myDiagram.model.setDataProperty(rename.key, 'loc', $(e).val() + " " + rename.key.loc.split(" ")[1]);
            }

        }

    }
    else if ($(e).parent().closest('tr').children()[0].innerHTML == "Top") {
        var rename = myDiagram.Ei.Ea;
        while (rename.Xa != null) {

            rename = rename.Xa
            if (rename.key.StepID == $(e).parent().parent()[0].id) {

                myDiagram.model.setDataProperty(rename.key, 'loc', rename.key.loc.split(" ")[0] + " " + $(e).val());
            }

        }

    }

}
function updateMails(e) {

    if ($(e).parent().closest('tr').children()[0].innerHTML != "") {
        if ($(e).parent().closest('tr').children()[0].innerHTML.toLowerCase().trim() == "mail server") {
            if ($(e).val().toLowerCase() == "gmail") {
                mailserveraddress = "imap.gmail.com";
                port = "993";
            }
            else if ($(e).val().toLowerCase() == "outlook") {
                mailserveraddress = "imap-mail.outlook.com";
                port = "993";
            }
            else if ($(e).val().toLowerCase() == "aol") {
                mailserveraddress = "imap.aol.com";
                port = "993";
            }
            else if ($(e).val().toLowerCase() == "yandex") {
                mailserveraddress = "imap.yandex.com";
                port = "993";
            }
            else if ($(e).val().toLowerCase() == "gmx") {
                mailserveraddress = "imap.gmx.com";
                port = "993";
            }
            else if ($(e).val().toLowerCase() == "mail") {
                mailserveraddress = "imap.mail.com";
                port = "993";
            }
            else {
                mailserveraddress = "";
                port = "";
            }
            for (i in $(e).parent().parent().parent().find('td')) {
                if ($($(e).parent().parent().parent().find('td')[i]).hasClass('col-sm-2')) {
                    if ($(e).parent().parent().parent().find('td')[i].innerHTML.toLowerCase().trim() == "inbound address") {
                        $($($(e).parent().parent().parent().find('td')[parseInt(i) + 1]).children()[0]).val(mailserveraddress);
                        $($($(e).parent().parent().parent().find('td')[parseInt(i) + 1]).children()[0]).blur();
                    }
                    else if ($(e).parent().parent().parent().find('td')[i].innerHTML.toLowerCase().trim() == "inbound port") {
                        $($($(e).parent().parent().parent().find('td')[parseInt(i) + 1]).children()[0]).val(port);
                        $($($(e).parent().parent().parent().find('td')[parseInt(i) + 1]).children()[0]).blur();
                    }
                }
            }
        }
    }
    localStepProperties[$(e).parent().parent()[0].id][$(e)[0].id.split("p")[1]]["StepPropertyValue"] = $(e).val();
}
var FilesUploadurl = [];
function updateStepProperties(e) {

    var mailserveraddress;
    var port;
    if (e.type == "file" && $(e)[0].attributes['directory'] != undefined) {
        localStepProperties[$(e).parent().parent()[0].id][$(e)[0].id.split("p")[1]]["StepPropertyValue"] = $(e)[0].defaultValue;
        FilesUploadurl.push($(e)[0].defaultValue);
    }

    else if ($(e).parent().closest('tr').children()[0].innerHTML == "DisplayName") {

        var rename = myDiagram.Ei.Ea;
        while (rename.Xa != null) {

            rename = rename.Xa

            if (rename.key.StepID == $(e).parent().parent()[0].id) {
                if ($(e).val() != "") {
                    myDiagram.model.setDataProperty(rename.key, 'DisplayName', $(e).val());
                }
                else {
                    $(e).val(rename.key.DisplayName);
                }

            }
        }

    }
    else if ($(e).parent().closest('tr').children()[0].innerHTML == "Left") {
        var rename = myDiagram.Ei.Ea;
        while (rename.Xa != null) {

            rename = rename.Xa
            if (rename.key.StepID == $(e).parent().parent()[0].id) {

                myDiagram.model.setDataProperty(rename.key, 'loc', $(e).val() + " " + rename.key.loc.split(" ")[1]);
            }
        }

    }
    else if ($(e).parent().closest('tr').children()[0].innerHTML == "Top") {
        var rename = myDiagram.Ei.Ea;
        while (rename.Xa != null) {

            rename = rename.Xa
            if (rename.key.StepID == $(e).parent().parent()[0].id) {

                myDiagram.model.setDataProperty(rename.key, 'loc', rename.key.loc.split(" ")[0] + " " + $(e).val());
            }

        }

    }

    if ($(e)[0].type == "checkbox")
        localStepProperties[$(e).parent().parent()[0].id][$(e)[0].id.split("p")[1]]["StepPropertyValue"] = $(e)[0].checked
    else if ($(e)[0].type == "text") {

        localStepProperties[$(e).parent().parent().parent()[0].id][$(e)[0].id.split("p")[1]]["StepPropertyValue"] = $(e).val();
        //localStepProperties[$(e).parent().parent()[0].id][$(e)[0].id.split("p")[1]]["StepPropertyValue"] = $(e).val();
    }
    else {

        localStepProperties[$(e).parent().parent()[0].id][$(e)[0].id.split("p")[1]]["StepPropertyValue"] = $(e).val();
        // localStepProperties[$(e).parent().parent().parent()[0].id][$(e)[0].id.split("p")[1]]["StepPropertyValue"] = $(e).val();
    }
}
function updateActionSP(e) {
    var mailserveraddress;
    var port;
    if (e.type == "file" && $(e)[0].attributes['directory'] != undefined) {
        localStepProperties[$(e).parent().parent()[0].id][$(e)[0].id.split("p")[1]]["StepPropertyValue"] = $(e)[0].defaultValue;
        FilesUploadurl.push($(e)[0].defaultValue);
    }
    else if ($(e).parent().closest('tr').children()[0].innerHTML == "DisplayName") {

        var rename = myDiagram.Ei.Ea;
        while (rename.Xa != null) {

            rename = rename.Xa

            if (rename.key.StepID == $(e).parent().parent()[0].id) {
                myDiagram.model.setDataProperty(rename.key, 'DisplayName', $(e).val());
            }
        }

    }
    else if ($(e).parent().closest('tr').children()[0].innerHTML == "Left") {
        var rename = myDiagram.Ei.Ea;
        while (rename.Xa != null) {

            rename = rename.Xa
            if (rename.key.StepID == $(e).parent().parent()[0].id) {

                myDiagram.model.setDataProperty(rename.key, 'loc', $(e).val() + " " + rename.key.loc.split(" ")[1]);
            }
        }

    }
    else if ($(e).parent().closest('tr').children()[0].innerHTML == "Top") {
        var rename = myDiagram.Ei.Ea;
        while (rename.Xa != null) {

            rename = rename.Xa
            if (rename.key.StepID == $(e).parent().parent()[0].id) {

                myDiagram.model.setDataProperty(rename.key, 'loc', rename.key.loc.split(" ")[0] + " " + $(e).val());
            }

        }

    }
    if ($(e).val() == "") {
        //$(e).val(rename.key.DisplayName);
        //alert("Step Name can\'t be empty");
        Sname = "Empty";
    }
    else {
        Sname = "";
    }
    if ($(e)[0].type == "checkbox")
        localStepProperties[$(e).parent().parent()[0].id][$(e)[0].id.split("p")[1]]["StepPropertyValue"] = $(e)[0].checked
    else if ($(e)[0].type == "text") {
        localStepProperties[$(e).parent().parent().parent()[0].id][$(e)[0].id.split("p")[1]]["StepPropertyValue"] = $(e).val();
    }
    else
        localStepProperties[$(e).parent().parent()[0].id][$(e)[0].id.split("p")[1]]["StepPropertyValue"] = $(e).val();
}
var localStepProperties = {};
var localStepPropertiesIndex = -1;
var localSPIndex = -1;
function saveStepProperties() {

    var localSP = {};

    StepPropertiesLength = $("[name='PropertyInput']").length;
    SavedStepPropertiesIds = "";
    for (var m = 0; m < $("[name='PropertyInput']").length; m++) {

        e = $("[name='PropertyInput']")[m];
        var data = {};
        var columnname = $(e).closest('tr')[0].firstElementChild.innerText;
        if (columnname.indexOf("*") > -1) {
            columnname = columnname.replace("*", "");
        }
        data["StepProperty"] = $(e).closest('tr')[0].firstElementChild.innerHTML;
        data["Status"] = 1;
        data["Steps_Id"] = $(e).closest('tr')[0].id;
        data["Project_Id"] = $(".Prjbtn")[0].id;
        data["PropertyType"] = $(e).closest('tr')[0].attributes["type"].nodeValue;
        data["CustomValidation"] = $(e).attr("custom");
        data["DefaultValue"] = $(e).attr("title");
        data["CreateBy"] = (JSON.parse(localStorage.Result).User_ID);
        data["UpdateBy"] = (JSON.parse(localStorage.Result).User_ID);
        if ($(e).closest('tr')[0].lastElementChild.firstElementChild.type == "text") {
            if ($(e).closest('tr')[0].lastElementChild.firstElementChild.webkitdirectory == true) {
                data["StepPropertyType"] = "FolderorFileDialog";
            }
            else {
                data["StepPropertyType"] = "Textbox";
            }
            data["StepPropertyValue"] = $(e).val();
        }
        else if ($(e).closest('tr')[0].lastElementChild.firstElementChild.type == "checkbox") {
            data["StepPropertyType"] = "Checkbox";
            data["StepPropertyValue"] = $(e)[0].checked;
        }
        else if ($(e).closest('tr')[0].lastElementChild.firstElementChild.type == "select-one") {
            data["StepPropertyType"] = "DropDownList";
            data["StepPropertyValue"] = $(e).val();
        }
        else if ($(e).closest('tr')[0].lastElementChild.firstElementChild.type == "file") {
            if ($(e).closest('tr')[0].lastElementChild.firstElementChild.webkitdirectory == true) {
                data["StepPropertyType"] = "FolderDialog";
            }
            else {
                data["StepPropertyType"] = "FileDialog";
            }
            data["StepPropertyValue"] = $(e).val();
        }
        data["Order"] = $(e)[0].attributes.order.nodeValue
        localSP[localSPIndex] = data;
        SavedStepPropertiesIds += " " + localSPIndex;
        if (localSPIndex < 0)
            localSPIndex -= 1;
        else
            localSPIndex += 1;

    }
    localStepProperties[localStepPropertiesIndex] = localSP;
    if (localStepPropertiesIndex < 0)
        localStepPropertiesIndex -= 1;
    else
        localStepPropertiesIndex += 1;

}
var LocalStepsIndex = -1;
var LocalSteps = {};
function CreateStep(DraggedIcon) {

    StepIndex = DraggedIcon.Name;

    var data = {};

    data["Action_Id"] = DraggedIcon.Action_Id;
    data["Element_Id"] = DraggedIcon.Element_Id;


    data["Name"] = DraggedIcon.Name;
    data["DisplayName"] = DraggedIcon.DisplayName;
    data["Robot_Id"] = RobotID;
    data["Workflow_Id"] = "";
    data["Status"] = 1;
    data["Order"] = LocalStepsIndex.toString().split('-')[1]

    data["RuntimeUserInput"] = DraggedIcon.RuntimeUserInput;
    data["CreateBy"] = (JSON.parse(localStorage.Result).User_ID);
    data["UpdateBy"] = (JSON.parse(localStorage.Result).User_ID);
    LocalSteps[LocalStepsIndex] = data;
    StepCustomID = LocalStepsIndex;
    if (LocalStepsIndex < 0)
        LocalStepsIndex -= 1;
}

function deleteStep(e) {
    delete LocalSteps[e.fa.Aw.Qb.Va.key.Zd.StepID];
    if (e.fa.Aw.Qb.Va.key.Zd.StepProperties != "") {
        sp = e.fa.Aw.Qb.Va.key.Zd.StepProperties.split(" ");
        for (i in sp) {
            if (sp[i] != " " && sp[i] != "") {
                delete localStepProperties[e.fa.Aw.Qb.Va.key.Zd.StepID][sp[i]];
            }
        }
    }
}
datasource = GlobalURL + '/api/DesignStudio/Get?input={"Type":"AllActionProperties"}';

$.ajax({
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    },
    type: 'GET',
    url: datasource,

    ContentType: 'application/json; charset=utf-8',
    DataType: "json",
    async: false,
    success: function (data2) {

        ActionPropertiesJSON = JSON.parse(data2);
    },
    error: function (e) {

    }
});
function loadActionProperties(CustomID1, ActionName, globalIndex, url, StepProperties_1, properties) {
    var p = 0;
    var NameOfElement;
    var IdOfAction;
    $("#PropertiesBody").html("");

    if (StepProperties_1 != '0') {
        var sp = StepProperties_1.split(" ");
    }
    var GeneralHtml = "<tr><th class='propertiescaption'>General</th><th></th></tr>"
    var ActionHtml = "<tr><th class='propertiescaption'>" + ActionName + "</th><th></th></tr>"
    for (var x in ActionPropertiesJSON) {
        var reqStr = "";
        var requiredValue = "";
        if ((ActionPropertiesJSON[x].CustomValidation == 1) || (ActionPropertiesJSON[x].CustomValidation == 2) || (ActionPropertiesJSON[x].CustomValidation == 3)) {
            reqStr = "<span style=\"color:red\">*</span>"
            requiredValue = "required";
        }
        customValue = "<span>'" + ActionPropertiesJSON[x].CustomValidation + "'</span>";
        if (globalIndex == ActionPropertiesJSON[x].Action_Id) {

            if (StepProperties_1 != '0') {
                s = sp[p]
            }
            else
                s = 0;
            switch (ActionPropertiesJSON[x].PropertyType.trim().toLowerCase()) {
                case "textbox":
                    if (ActionPropertiesJSON[x].Name == "Name" || ActionPropertiesJSON[x].Name == "DisplayName" || ActionPropertiesJSON[x].Name == "Description" || ActionPropertiesJSON[x].Name == "Icon" || ActionPropertiesJSON[x].Name == "Type") {
                        for (action in ActionJSON) {
                            if (globalIndex == ActionJSON[action].Id) {
                                actionIndex = action;
                                break;
                            }
                        }
                        if (ActionPropertiesJSON[x].Name == "Element") {
                            for (elementName in ActionJSON) {
                                if (globalIndex == ActionJSON[elementName].Id) {
                                    IdOfAction = ActionJSON[elementName].Element_Id;

                                }
                            }
                            for (i in Elements) {
                                if (IdOfAction == Elements[i].Id) {
                                    NameOfElement = Elements[i].Name;
                                    break;
                                }
                            }
                        }
                        if (ActionPropertiesJSON[x].Name == "Type") {
                            var FAPName = "Element";
                        }
                        else
                            var FAPName = ActionPropertiesJSON[x].Name;
                        if (ActionPropertiesJSON[x].Name.toLowerCase().indexOf("password") > -1)
                            ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2'>" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input custom='" + ActionPropertiesJSON[x].CustomValidation + "'  type='password' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'  value='" + ActionJSON[action][FAPName] + "'onblur='updateStepProperties(this)' " + requiredValue + " />" + customValue + "</td></tr>";
                        else
                            ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input custom='" + ActionPropertiesJSON[x].CustomValidation + "' type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' value='" + ActionJSON[action][FAPName] + "' onblur='updateStepProperties(this)'" + requiredValue + "/></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name == "Left") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input  type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'value='" + parseInt(properties["loc"].split(",")[0]) + "'onblur='updateStepProperties(this)'" + requiredValue + "/></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name == "Top") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input  custom='" + ActionPropertiesJSON[x].CustomValidation + "' type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' value='" + parseInt(properties["loc"].split(",")[1]) + "'onblur='updateStepProperties(this)'" + requiredValue + "/></td></tr>";
                    }
                    else {
                        if (ActionPropertiesJSON[x].DefaultValue != null) {
                            ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input  custom='" + ActionPropertiesJSON[x].CustomValidation + "' type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' value='" /*+ ActionPropertiesJSON[x].DefaultValue*/ + "' onblur='updateStepProperties(this)'" + requiredValue + "/></td></tr>";
                        }
                        else {
                            ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input  custom='" + ActionPropertiesJSON[x].CustomValidation + "' type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' value='' onblur='updateStepProperties(this)'" + requiredValue + "/></td></tr>";
                        }
                    }
                    p = p + 1;
                    break;
                case "checkbox":
                    ActionHtml += "<tr type=1  id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input custom='" + ActionPropertiesJSON[x].CustomValidation + "' name='PropertyInput' autocomplete='off' type='checkbox' class='checker' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "/></td></tr>";
                    p = p + 1;
                    break;
                case "dropdownlist":
                    if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "comparisontype" || ActionPropertiesJSON[x].Name.toLowerCase().trim() == "sourcetype") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)' " + requiredValue + "><option>Value</option><option>Static</option><option>Variable</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "datatype" || ActionPropertiesJSON[x].Name.toLowerCase().trim() == "lefttype" || ActionPropertiesJSON[x].Name.toLowerCase().trim() == "righttype" || ActionPropertiesJSON[x].Name.toLowerCase().trim() == "labeltype") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)' " + requiredValue + "><option>Value</option><option>Variable</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "buttons") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>OK</option><option>OK-Cancel</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "readtype") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>Unread</option><option>Read</option><option>Star</option><option>Important</option><option>All</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "read type") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>Unread</option><option>All</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "category") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>Home Timeline</option><option>Mentions Timeline</option><option>User Timeline</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "db type") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)' " + requiredValue + "><option>postgreSQL</option><option>MySQL</option><option>MSSQLServer</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "download in") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)' " + requiredValue + "><option>Local</option><option>Server</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "exportin") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)' " + requiredValue + "><option>Local</option><option>Server</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "isbodyhtml") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput'custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>text</option><option>html</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "attribute") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>Id</option><option>Class</option><option>Name</option><option>Type</option><option>Custom</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "groupby") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>true</option><option>false</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "createtype") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>User</option><option>Role</option><option>Group</option><option>File</option><option>Folder</option><option>Database</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "permissiontype") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>All</option><option>None</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "charttype") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>PieChart</option><option>AreaChart</option><option>AreaChart</option><option>Lines</option><option>BarOfPie</option><option>ColumnGraph</option><option>BarGraph</option><option>Doughnut</option><option>Bubble</option><option>Scatterplot</option><option>Radar</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "ostype") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>Windows</option><option>Linux</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "sourcetype") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>Value</option><option>Static</option><option>Variable</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "protocol") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2'>" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>IMAP</option><option>POP3</option><option>Others</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "mail server") {
                        // ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2'>" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>Gmail</option><option>Outlook</option><option>AOL</option><option>Yandex</option><option>Yahoo</option><option>GMX</option><option>Mail</option><option>Others</option></select></td></tr>";
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2'>" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>Gmail</option><option>Others</option></select></td></tr>";

                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "folder") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>Inbox</option><option>All</option><option>Drafts</option><option>Flagged</option><option>Junk</option><option>Trash</option><option>Sent</option><option>Archive</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "inputtype") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>Value</option><option>Variable</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "input type 1") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>Value</option><option>Variable</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "input type 2") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>Value</option><option>Variable</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "Classify Input Type") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>Value</option><option>Variable</option><option>File</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "propertyname") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>AutomationID</option><option>Name</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "outputtype") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2'>" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>Value</option><option>Variable</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "widgettype") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2'>" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>Textbox</option><option>Checkbox</option><option>Dropdown</option><option>Textarea</option><option>Multiple Textbox</option><option>Image+Textbox</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "condition") {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>!=</option><option>==</option><option><</option><option>></option><option><=</option><option>>=</option></select></td></tr>";
                    }
                    else {
                        ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option></option></select></td></tr>";
                    }
                    p = p + 1;
                    break;
                case "filedialog":
                    ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input custom='" + ActionPropertiesJSON[x].CustomValidation + "' type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "/></td></tr>";
                    p = p + 1;
                    break;
                case "folderdialog":
                    ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input custom='" + ActionPropertiesJSON[x].CustomValidation + "' type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)' webkitdirectory directory multiple " + requiredValue + "/></td></tr>";
                    p = p + 1;
                    break;
                case "folderorfiledialog":
                    ActionHtml += "<tr type=1 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input custom='" + ActionPropertiesJSON[x].CustomValidation + "' type='input' name='PropertyInput' autocomplete='off' class='' style='width:100%' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onclick='changefilefolder(this)' onblur='updateStepProperties(this);this.type='text';this.value=changedURL' webkitdirectory directory multiple" + requiredValue + "/></td></tr>";
                    p = p + 1;
                    break;
            }
        }
        else if (ActionPropertiesJSON[x].Action_Id == 777) {
            if (StepProperties_1 != '0') {
                s = sp[p]
            }
            else
                s = 0;
            switch (ActionPropertiesJSON[x].PropertyType.toLowerCase().trim()) {
                case "textbox":
                    if (ActionPropertiesJSON[x].Name == "Name" || ActionPropertiesJSON[x].Name == "Description" || ActionPropertiesJSON[x].Name == "DisplayName" || ActionPropertiesJSON[x].Name == "Type") {
                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input custom='" + ActionPropertiesJSON[x].CustomValidation + "' type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' value='" + properties["DisplayName"] + "'onblur='updateStepProperties(this)'" + requiredValue + "/></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name == "Type") {
                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input custom='" + ActionPropertiesJSON[x].CustomValidation + "' type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' value='" + properties["DisplayName"] + "'onblur='updateStepProperties(this)'" + requiredValue + "/></td></tr>";
                    }

                    else if (ActionPropertiesJSON[x].Name == "Left") {
                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input custom='" + ActionPropertiesJSON[x].CustomValidation + "' type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' value='" + Math.ceil(Math.abs(properties["loc"].split(" ")[0])) + "'onblur='updateStepProperties(this)'" + requiredValue + "/></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name == "Top") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input custom='" + ActionPropertiesJSON[x].CustomValidation + "' type='text' name='PropertyInput' autocomplete='off' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' value='" + Math.ceil(Math.abs(properties["loc"].split(" ")[1])) + "' onblur='updateStepProperties(this)'" + requiredValue + "/></td></tr>";
                    }
                    p = p + 1;

                    break;
                case "checkbox":
                    GeneralHtml += "<tr type=0  id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input name='PropertyInput' autocomplete='off' type='checkbox' class='checker' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "/></td></tr>";
                    p = p + 1;
                    break;
                case "dropdownlist":
                    if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "comparisontype" || ActionPropertiesJSON[x].Name.toLowerCase().trim() == "sourcetype") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>Value</option><option>Static</option><option>Variable</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "datatype") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + " class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)' " + requiredValue + "><option>Value</option><option>Variable</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "buttons") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>OK</option><option>OK-Cancel</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "readtype") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>Unread</option><option>Read</option><option>Star</option><option>Important</option><option>All</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "read type") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>Unread</option><option>All</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "category") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>Home Timeline</option><option>Mentions Timeline</option><option>User Timeline</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "db type") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>postgreSQL</option><option>MySQL</option><option>MSSQLServer</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "download in") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + " class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>Local</option><option>Server</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "exportin") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>Local</option><option>Server</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "isbodyhtml") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>text</option><option>html</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "attribute") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>Id</option><option>Class</option><option>Name</option><option>Type</option><option>Custom</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "groupby") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>true</option><option>false</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "createtype") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)'" + requiredValue + "><option>User</option><option>Role</option><option>Group</option><option>File</option><option>Folder</option><option>Database</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "permissiontype") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>All</option><option>None</option></select></td></tr>";
                    }
                    else if (ActionPropertiesJSON[x].Name.toLowerCase().trim() == "charttype") {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option>PieChart</option><option>AreaChart</option><option>AreaChart</option><option>Lines</option><option>BarOfPie</option><option>ColumnGraph</option><option>BarGraph</option><option>Doughnut</option><option>Bubble</option><option>Scatterplot</option><option>Radar</option></select></td></tr>";
                    }
                    else {

                        GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2'>" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><select name='PropertyInput' custom='" + ActionPropertiesJSON[x].CustomValidation + "' class='form-control col-sm-3' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'" + requiredValue + "><option></option></select></td></tr>";
                    }

                    p = p + 1;
                    break;
                case "filedialog":
                    GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input custom=" + ActionPropertiesJSON[x].CustomValidation + " type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "'  title='" + ActionPropertiesJSON[x].DefaultValue + "'onblur='updateStepProperties(this)'/></td></tr>";
                    p = p + 1;
                    break;
                case "folderdialog":

                    GeneralHtml += "<tr type=0 id='" + CustomID1 + "'><td class='col-sm-2' >" + ActionPropertiesJSON[x].Name + "</td><td class='col-sm-10'><input custom='" + ActionPropertiesJSON[x].CustomValidation + "' type='file' name='PropertyInput' autocomplete='off' class='' style='width:100%' id='sp" + s + "' order='" + ActionPropertiesJSON[x].Order + "' title='" + ActionPropertiesJSON[x].DefaultValue + "' onblur='updateStepProperties(this)' webkitdirectory directory multiple/></td></tr>";
                    p = p + 1;
                    break;
            }
        }
    }
    if (GeneralHtml != "<tr><th>General</th><th></th></tr>")
        $("#PropertiesBody")[0].innerHTML += GeneralHtml;
    if (ActionHtml != "<tr><th>" + ActionName + "</th><th></th></tr>" && ActionName != "Start" && ActionName != "Stop")
        $("#PropertiesBody")[0].innerHTML += ActionHtml;

}
function loadImagesByAjax() {
    ElementArr = {};
    if (jQuery('.active').parent().hasClass('Workflow_Robot')) {
        $.ajax({
            type: "GET",
            url: GlobalURL + '/api/CrudService/getTableData?tablename=ROBOT',

            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (val) {
                var AllRobots = JSON.parse(val)
                for (var h in AllRobots) {
                    var y = {};
                    y["source"] = "dist/img/robot_48.png";
                    y["category"] = "ImageNode";
                    y["Name"] = AllRobots[h].Name;
                    y["DisplayName"] = AllRobots[h].Name.replace(/([A-Z])/g, ' $1').trim();
                    y["Element_Id"] = 999;
                    y["Action_Id"] = 999;
                    y["RuntimeUserInput"] = 0;
                    if (typeof (ElementArr["Workflow_Robots_Arr"]) == typeof (undefined))
                        ElementArr["Workflow_Robots_Arr"] = [];
                    ElementArr["Workflow_Robots_Arr"].push(y);
                }
            },
            error: function (e) {
                //alert("Something Wrong!!");
            }
        });

    }
    if (jQuery('.active').parent().hasClass('ValueStreamMap'))
        data = { "Type": "AllActionsByType", "ProjectType": "ValueStreamMap" };
    else if (jQuery('.active').parent().hasClass('FlowChart'))
        data = { "Type": "AllActionsByType", "ProjectType": "FlowChart" };
    else {
        data = { "Type": "AllActionsByType", "ProjectType": "('Robot','Workflow')" };

    }
    data1 = JSON.stringify(data);
    datasource = GlobalURL + '/api/DesignStudio/Get?input=' + data1;
    $.ajax({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        type: 'GET',
        url: datasource,
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (data2) {
            ActionJSON = JSON.parse(data2);
            for (var x = 0; x < ActionJSON.length; x++) {
                if (ActionJSON[x].Type == "Robot") {
                    var y = {};
                    var index = Elements.findIndex(p => p.Id == ActionJSON[x].Element_Id)
                    if (index > -1) {
                        y["source"] = "Images/" + Elements[index].Name.trim() + "/" + ActionJSON[x].Icon;
                        y["category"] = "ImageNode";
                        y["Name"] = ActionJSON[x].Name;
                        y["DisplayName"] = ActionJSON[x].Name.replace(/([A-Z])/g, ' $1').trim();
                        y["Element_Id"] = ActionJSON[x].Element_Id;
                        ActionJSON[x].Element = Elements[index].Name.trim();
                        y["Action_Id"] = ActionJSON[x].Id;
                        y["RuntimeUserInput"] = ActionJSON[x].RuntimeUserInput;
                        y["Dependency"] = ActionJSON[x].Dependency;
                        if (typeof (ElementArr["Robot_" + Elements[index].Name.trim() + "_Arr"]) == typeof (undefined))
                            ElementArr["Robot_" + Elements[index].Name.trim() + "_Arr"] = [];
                        ElementArr["Robot_" + Elements[index].Name.trim() + "_Arr"].push(y);
                    }
                }
                else if (ActionJSON[x].Type == "Workflow") {
                    var y = {};
                    var index = Elements.findIndex(p => p.Id == ActionJSON[x].Element_Id)
                    if (index > -1) {
                        y["source"] = "Images/" + Elements[index].Name.trim() + "/" + ActionJSON[x].Icon;
                        y["category"] = "ImageNode";
                        y["Name"] = ActionJSON[x].Name;
                        y["DisplayName"] = ActionJSON[x].Name.replace(/([A-Z])/g, ' $1').trim();
                        y["Element_Id"] = ActionJSON[x].Element_Id;
                        ActionJSON[x].Element = Elements[index].Name.trim();
                        y["Action_Id"] = ActionJSON[x].Id;
                        y["RuntimeUserInput"] = ActionJSON[x].RuntimeUserInput;
                        y["Dependency"] = ActionJSON[x].Dependency;
                        if (typeof (ElementArr["Robot_" + Elements[index].Name.trim() + "_Arr"]) == typeof (undefined))
                            ElementArr["Robot_" + Elements[index].Name.trim() + "_Arr"] = [];
                        ElementArr["Robot_" + Elements[index].Name.trim() + "_Arr"].push(y);
                    }
                }
                else if (ActionJSON[x].Type == "FlowChart") {
                    var y = {};
                    var index = Elements.findIndex(p => p.Id == ActionJSON[x].Element_Id)
                    if (index > -1) {
                        y["source"] = "Images/FlowChart/" + ActionJSON[x].Icon;
                        y["category"] = "ImageNode";
                        y["Name"] = ActionJSON[x].Name;
                        y["DisplayName"] = ActionJSON[x].Name.replace(/([A-Z])/g, ' $1').trim();
                        y["Element_Id"] = ActionJSON[x].Element_Id;
                        ActionJSON[x].Element = Elements[index].Name.trim();
                        y["Action_Id"] = ActionJSON[x].Id;
                        y["RuntimeUserInput"] = ActionJSON[x].RuntimeUserInput;
                        y["Dependency"] = ActionJSON[x].Dependency;
                        if (typeof (ElementArr["FlowChart_" + Elements[index].Name.trim() + "_Arr"]) == typeof (undefined))
                            ElementArr["FlowChart_" + Elements[index].Name.trim() + "_Arr"] = [];
                        ElementArr["FlowChart_" + Elements[index].Name.trim() + "_Arr"].push(y);
                    }
                }
                else {
                    var y = {};
                    var index = Elements.findIndex(p => p.Id == ActionJSON[x].Element_Id)
                    if (index > -1) {
                        y["source"] = "Images/ValueStreamMap/" + ActionJSON[x].Icon;
                        y["category"] = "ImageNode";
                        y["Name"] = ActionJSON[x].Name;
                        y["DisplayName"] = ActionJSON[x].Name.replace(/([A-Z])/g, ' $1').trim();;
                        y["Element_Id"] = ActionJSON[x].Element_Id;
                        ActionJSON[x].Element = Elements[index].Name.trim();
                        y["Action_Id"] = ActionJSON[x].Id;
                        y["RuntimeUserInput"] = ActionJSON[x].RuntimeUserInput;
                        y["Dependency"] = ActionJSON[x].Dependency;
                        if (typeof (ElementArr["ValueStream_" + Elements[index].Name.trim() + "_Arr"]) == typeof (undefined))
                            ElementArr["ValueStream_" + Elements[index].Name.trim() + "_Arr"] = [];
                        ElementArr["ValueStream_" + Elements[index].Name.trim() + "_Arr"].push(y);
                    }
                }

            }
        },
        error: function (e) {

        }
    });

}
//ajax call end 

// General Parameters for this app, used during initialization
var AllowTopLevel = true;
var CellSize = new go.Size(50, 50);
var myPaletteWorkflow = {};
var k;
function loadPalette(projectnamevalue) {

    var ElementId = [];
    if (jQuery('.active').parent().hasClass('ValueStreamMap') || (projectnamevalue == "ValueStreamMap")) {
        if (jQuery("#accordion2").hasClass('ui-accordion')) {
            jQuery("#accordion2").removeClass('ui-accordion')
        }
        if (!jQuery("#accordion2").hasClass('ui-accordion')) {
            jQuery("#FlowchartAccordian").hide();
            jQuery("#accordion2").show();
            jQuery("#accordion3").hide();
            jQuery("#actionsopen").show();
            jQuery('#actionsDiv').show();
            jQuery('#ActionProperties').hide();

            $('#predefinedRobotsDisplay').hide();
            $('#noCustomRobotsDisplay').show();
            $('#customRobotsDisplay').hide();
            $('#noVersionsDisplay').show();
            $('#versionsDisplay').hide();
            jQuery("#accordion2").accordion({
                activate: function (event, ui) {
                    for (var Element in ElementId) {
                        myPaletteValueStream[Element].requestUpdate();
                    }
                }
            });
            var myPaletteValueStream = {};

            $.each(ElementArr, function (key, value) {
                if (key.split("_")[0] != 'Robot') {
                    ElementId = [];
                    ElementId.push(Elements.findIndex(p => p.Name == key.split("_")[1]))
                    myPaletteValueStream[Element] =
                        MK(go.Palette, "myPaletteValueStream" + Elements[ElementId.length - 1].Name,  // must name or refer to the DIV HTML element
                            {
                                "animationManager.duration": 800, // slightly longer than default (600ms) animation
                                nodeTemplateMap: myDiagram.nodeTemplateMap, // share the templates used by myDiagram
                                model: new go.GraphLinksModel(ElementArr["ValueStream_" + Elements[ElementId.length - 1].Name + "_Arr"])
                            }
                        );
                }
            });
        }
    }
    else if (jQuery('.active').parent().hasClass('FlowChart') || (projectnamevalue == "FlowChart")) {
        if (jQuery("#FlowchartAccordian").hasClass('ui-accordion')) {
            jQuery("#FlowchartAccordian").removeClass('ui-accordion')
        }
        if (!jQuery("#FlowchartAccordian").hasClass('ui-accordion')) {

            jQuery("#FlowchartAccordian").show();
            jQuery("#accordion3").hide();
            jQuery("#actionsopen").show();
            jQuery('#actionsDiv').show();
            jQuery('#ActionProperties').hide();

            $('#predefinedRobotsDisplay').hide();
            $('#noCustomRobotsDisplay').show();
            $('#customRobotsDisplay').hide();
            $('#noVersionsDisplay').show();
            $('#versionsDisplay').hide();
            jQuery("#FlowchartAccordian").accordion({
                activate: function (event, ui) {
                    for (var Element in ElementId) {
                        myPaletteFlowChart[Element].requestUpdate();
                    }
                }
            });
            var myPaletteFlowChart = {};

            $.each(ElementArr, function (key, value) {
                if (key.split("_")[0] != 'Robot') {
                    ElementId = [];
                    ElementId.push(Elements.findIndex(p => p.Name == key.split("_")[1]))
                    myPaletteFlowChart[Element] =
                        MK(go.Palette, "myPaletteFlowChart" + Elements[ElementId.length - 1].Name,  // must name or refer to the DIV HTML element
                            {
                                "animationManager.duration": 800, // slightly longer than default (600ms) animation
                                nodeTemplateMap: myDiagram.nodeTemplateMap, // share the templates used by myDiagram
                                model: new go.GraphLinksModel(ElementArr["FlowChart_" + Elements[ElementId.length - 1].Name + "_Arr"])
                            }
                        );
                }
            });
        }
    }
    else if (jQuery('.active').parent().hasClass('Workflow_Robot') || (projectnamevalue == "Workflow_Robot")) {
        ElementId = [];
        if (!jQuery("#accordion3").hasClass('ui-accordion')) {
            jQuery("#FlowchartAccordian").hide();
            jQuery("#accordion3").show();
            jQuery("#accordion2").hide();
            jQuery('#actionsDiv').show();
            jQuery("#actionsopen").show();
            jQuery('#ActionProperties').hide();
            $('#noCustomRobotsDisplay').show();
            $('#customRobotsDisplay').hide();

            $('#predefinedRobotsDisplay').hide();
            $('#noVersionsDisplay').show();
            $('#versionsDisplay').hide();
            jQuery("#accordion3").accordion({
                activate: function (event, ui) {
                    for (var Element in ElementId) {
                        myPaletteWorkflow[Element].requestUpdate();
                    }
                }
            });

            myPaletteWorkflow = {};
            ElementId.push(0);
            myPaletteWorkflow[0] =
                MK(go.Palette, "myPaletteWorkflowGeneral",  // must name or refer to the DIV HTML element
                    {
                        "animationManager.duration": 800, // slightly longer than default (600ms) animation
                        nodeTemplateMap: myDiagram.nodeTemplateMap, // share the templates used by myDiagram
                        model: new go.GraphLinksModel(ElementArr["Robot_General_Arr"])
                    }
                );
            ElementId.push(1);
            myPaletteWorkflow[1] =
                MK(go.Palette, "myPaletteWorkflowDeveloper",  // must name or refer to the DIV HTML element
                    {
                        "animationManager.duration": 800, // slightly longer than default (600ms) animation
                        nodeTemplateMap: myDiagram.nodeTemplateMap, // share the templates used by myDiagram
                        model: new go.GraphLinksModel(ElementArr["Robot_Developer_Arr"])
                    }
                );

            ElementId.push(2);
            myPaletteWorkflow[2] =
                MK(go.Palette, "myPaletteWorkflowRobots",  // must name or refer to the DIV HTML element
                    {
                        "animationManager.duration": 800, // slightly longer than default (600ms) animation
                        nodeTemplateMap: myDiagram.nodeTemplateMap, // share the templates used by myDiagram
                        model: new go.GraphLinksModel(ElementArr["Robot_Robots_Arr"])
                    }
                );
        }
    }
    else if (jQuery('.active').parent().hasClass('Robot') || (projectnamevalue == "Robot")) {
        $('#predefinedRobotsDisplay').show();
        $('#noCustomRobotsDisplay').hide();
        $('#customRobotsDisplay').show();
        $('#noVersionsDisplay').hide();
        $('#versionsDisplay').show();

        if (projectnamevalue != "Robot") {
            if (!jQuery("#accordion").hasClass('ui-accordion') && jQuery("#accordion").hasClass('showAccordion')) {
                jQuery("#FlowchartAccordian").hide();
                jQuery("#actionsopen").show();
                jQuery("#accordion2").hide();
                jQuery("#accordion3").hide();
                jQuery('#actionsDiv').show();
                jQuery('#ActionProperties').show();
                jQuery('#accordion').show();

                if (jQuery.isEmptyObject(myPaletteRobot)) {
                    for (var Element in Elements) {
                        ElementId = [];
                        if (Elements[Element].Category != "Advanced") {
                            // $('#accordion').show();

                            myPaletteRobot[Element] =
                                MK(go.Palette, "myPalette" + Elements[Element].Name,  // must name or refer to the DIV HTML element
                                    {
                                        "animationManager.duration": 800, // slightly longer than default (600ms) animation
                                        nodeTemplateMap: myDiagram.nodeTemplateMap, // share the templates used by myDiagram
                                        model: new go.GraphLinksModel(ElementArr["Robot_" + Elements[Element].Name + "_Arr"])
                                    }
                                );

                        }
                    }
                    RobotPalette = myPaletteRobot;




                }
                jQuery("#accordion").accordion({
                    activate: function (event, ui) {
                        for (var Element in Elements) {
                            if (Elements[Element].Category != "Advanced") {
                                myPaletteRobot[Element].requestUpdate();
                            }
                        }
                    }
                });

            }
            else if (!jQuery("#elementaccordion").hasClass('ui-accordion') && jQuery("#elementaccordion").hasClass('showAccordion')) {
                jQuery("#actionsopen").show();
                jQuery("#accordion2").hide();
                jQuery("#accordion3").hide();
                jQuery('#actionsDiv').show();

                jQuery('#ActionProperties').show();
                if (jQuery.isEmptyObject(myPaletteRobotAdv)) {
                    for (var Element in Elements) {
                        ElementId = [];
                        if (Elements[Element].Category == "Advanced") {

                            myPaletteRobotAdv[Element] =
                                MK(go.Palette, "myPalette" + Elements[Element].Name,  // must name or refer to the DIV HTML element
                                    {
                                        "animationManager.duration": 800, // slightly longer than default (600ms) animation
                                        nodeTemplateMap: myDiagram.nodeTemplateMap, // share the templates used by myDiagram
                                        model: new go.GraphLinksModel(ElementArr["Robot_" + Elements[Element].Name + "_Arr"])
                                    }
                                );
                        }
                    }
                    RobotAdvPalette = myPaletteRobotAdv;


                }
                jQuery("#elementaccordion").accordion({
                    activate: function (event, ui) {
                        for (var Element in Elements) {
                            if (Elements[Element].Category == "Advanced") {
                                myPaletteRobotAdv[Element].requestUpdate();
                            }

                        }
                    }
                });

            }
        }
    }
    if (jQuery('.active').parent().hasClass('Workflow_Robot')) {
        $(".add_prj").hide();
        $("#delete_Robot").show();
    }
}
var Systems_List = [];
function loadSystems() {
    $.ajax({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        type: 'GET',
        url: GlobalURL + '/api/CrudService/getTableData?tablename=Systems',
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (SystemResult) {
            Systems_List = JSON.parse(SystemResult);


        },
        error: function (e) {

        }
    });
}
function updateClient_Dependency() {
    var checkDependancy = 0;
    for (var step in LocalSteps) {
        if (LocalSteps[step]["Element_Id"] == "57") {
            checkDependancy++;
        }
    }
    if (checkDependancy > 0)
        LocalRobotProperties["Client_Dependency"] = true;
    else
        LocalRobotProperties["Client_Dependency"] = false;
}
function init() {
    getMenuItems();
    loadProjectProcess();

    loadElments();

    loadImagesByAjax();
    loadSystems();
    getPredefinedBots();

    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    MK = go.GraphObject.make;  // for conciseness in defining templates
    myDiagram =
        MK(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
            {
                grid: MK(go.Panel, "Grid",
                    { gridCellSize: CellSize },
                    MK(go.Shape, "LineH", { stroke: "#f6f6f6" }),
                    MK(go.Shape, "LineV", { stroke: "#f6f6f6" })
                ),

                initialContentAlignment: go.Spot.Center,
                allowDrop: true,  // must be true to accept drops from the Palette
                "LinkDrawn": showLinkLabel,  // this DiagramEvent listener is defined below
                "LinkRelinked": showLinkLabel,
                "animationManager.duration": 800, // slightly longer than default (600ms) animation
                "undoManager.isEnabled": true, // enable undo & redo
                "ViewportBoundsChanged": function (e) {
                    e.diagram.toolManager.panningTool.isEnabled = !e.diagram.viewportBounds.containsRect(e.diagram.documentBounds);
                },

            });
    //myDiagram.validCycle = go.Diagram.CycleDestinationTree;
    //myDiagram.Spot = go.Diagram.AllSides;
    // when the document is modified, add a "*" to the title and enable the "Save" button
    myDiagram.addDiagramListener("Modified", function (e) {
        var button = document.getElementById("");
        if (button) button.disabled = !myDiagram.isModified;
        var idx = document.title.indexOf("*");
        if (myDiagram.isModified) {
            if (idx < 0) document.title += "*";
        } else {
            if (idx >= 0) document.title = document.title.substr(0, idx);
        }
    });

    // helper definitions for node templates
    function nodeStyle() {

        return [
            // The Node.location comes from the "loc" property of the node data,
            // converted by the Point.parse static method.
            // If the Node.location is changed, it updates the "loc" property of the node data,
            // converting back using the Point.stringify static method.
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            {
                // the Node.location is at the center of each node
                locationSpot: go.Spot.Center,
                //isShadowed: true,
                //shadowColor: "#888",
                // handle mouse enter/leave events to show/hide the ports
                mouseEnter: function (e, obj) {
                    showPorts(obj.part, true);
                },
                mouseLeave: function (e, obj) { showPorts(obj.part, false); }
            }
        ];
    }

    //function makePort(name, spot, output, input) {
    //    return MK(go.Shape, "Circle",
    //      {
    //          fill: "transparent",
    //          stroke: null,  // this is changed to "white" in the showPorts function
    //          desiredSize: new go.Size(8,8),
    //          alignment: spot, alignmentFocus: spot,  // align the port on the main Shape
    //          portId: name,  // declare this object to be a "port"
    //          fromSpot: spot, toSpot: spot,  // declare where links may connect at this port
    //          fromLinkable: output, toLinkable: input,  // declare whether the user may draw links to/from here
    //          cursor: "pointer"  // show a different cursor to indicate potential link point
    //      });
    //}
    function makePort(name, align, spot, output, input) {
        var horizontal = align.equals(go.Spot.Top) || align.equals(go.Spot.Bottom);
        // the port is basically just a transparent rectangle that stretches along the side of the node,
        // and becomes colored when the mouse passes over it
        return MK(go.Shape, "Circle",
            {
                fill: "transparent",  // changed to a color in the mouseEnter event handler
                stroke: "transparent",
                strokeWidth: 0,  // no stroke
                width: horizontal ? NaN : 16,  // if not stretching horizontally, just 8 wide
                height: !horizontal ? NaN : 16,  // if not stretching vertically, just 8 tall
                alignment: align,  // align the port on the main Shape
                stretch: (horizontal ? go.GraphObject.Horizontal : go.GraphObject.Vertical),
                portId: name,  // declare this object to be a "port"
                fromSpot: spot,  // declare where links may connect at this port
                fromLinkable: output,  // declare whether the user may draw links from here
                toSpot: spot,  // declare where links may connect at this port
                toLinkable: input,  // declare whether the user may draw links to here
                cursor: "pointer",  // show a different cursor to indicate potential link point
                mouseEnter: function (e, port) {  // the PORT argument will be this Shape
                    if (!e.diagram.isReadOnly) port.fill = "transparent";
                    port.stroke = "transparent";
                },

            });
    }

    // define the Node templates for regular nodes
    var lightText = 'whitesmoke';
    myDiagram.nodeTemplateMap.add("",  // the default category
        MK(go.Node, "Spot", nodeStyle(),
            // the main object is a Panel that surrounds a TextBlock with a rectangular Shape
            MK(go.Panel, "Auto",
                MK(go.Shape, "Rectangle",
                    { fill: "#00A9C9", stroke: null },
                    new go.Binding("figure", "figure")),
                MK(go.TextBlock,
                    {
                        font: "bold 10pt Roboto, sans-serif !important",
                        stroke: lightText,
                        margin: 8,
                        maxSize: new go.Size(160, NaN),
                        editable: true,
                        wrap: go.TextBlock.wrap,
                    },
                    new go.Binding("text").makeTwoWay())
            ),
            // four named ports, one on each side:
            makePort("T", go.Spot.Top, go.Spot.TopSide, false, true),
            makePort("L", go.Spot.Left, go.Spot.LeftSide, true, true),
            makePort("R", go.Spot.Right, go.Spot.RightSide, true, true),
            makePort("B", go.Spot.Bottom, go.Spot.BottomSide, true, false)
            //makePort("T", go.Spot.Top, true, true),
            //makePort("L", go.Spot.Left, true, true),
            //makePort("R", go.Spot.Right, true, true),
            //makePort("B", go.Spot.Bottom, true, true)
        )
    );

    //ImageNode
    myDiagram.nodeTemplateMap.add("ImageNode",

        MK(go.Node, "Spot",
            new go.Binding("background", "color"),
            { locationSpot: go.Spot.Center },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            { selectable: true, },

            { resizable: false, resizeObjectName: "PANEL" },
            {
                toolTip:  // define a tooltip for each node that displays the color as text
                    MK(go.Adornment, "Auto",
                        MK(go.Shape, { fill: "#fff" }),
                        MK(go.TextBlock, { margin: 6 },
                            new go.Binding("text", "DisplayName"))
                    )
            },

            // the main object is a Panel that surrounds a TextBlock with a Shape
            MK(go.Panel, "Vertical",

                MK(go.Picture,
                    {

                        desiredSize: new go.Size(56, 56),
                        margin: 1,
                        fromLinkable: true,
                        toLinkable: true,
                    },
                    new go.Binding("source")
                ),
                MK(go.TextBlock,
                    new go.Binding("text", "DisplayName"),
                    {
                        stroke: "black",
                        width: 60,
                        font: "9pt",
                        maxLines: 3,
                        editable: false,
                        wrap: go.TextBlock.wrap,
                        margin: 2,
                        textAlign: "center",
                    }
                )
            ),

            // four small named ports, one on each side:
            makePort("T", go.Spot.Top, go.Spot.TopSide, true, true),
            makePort("L", go.Spot.Left, go.Spot.LeftSide, true, true),
            makePort("R", go.Spot.Right, go.Spot.RightSide, true, true),
            makePort("B", go.Spot.Bottom, go.Spot.BottomSide, true, true),
            //makePort("T", go.Spot.TopSide, true, true),
            //makePort("L", go.Spot.LeftSide, true, true),
            //makePort("R", go.Spot.RightSide, true, true),
            //makePort("B", go.Spot.BottomSide, true, true),
            { // handle mouse enter/leave events to show/hide the ports
                //mouseEnter: function(e, node) { showSmallPorts(node, true); },
                //  mouseLeave: function(e, node) { showSmallPorts(node, false); }
            },
            new go.Binding("position", "pos", go.Point.parse).makeTwoWay(go.Point.stringify),
            { // what to do when a drag-over or a drag-drop occurs on a Group

                mouseDrop: function (e, grp) {
                    grp.diagram.currentTool.doCancel();

                    swal("", "Can't Drag on Step Itself", "error")
                }
            }
        )
    );

    myDiagram.nodeTemplateMap.add("ImageNode1",
        MK(go.Node, "Spot",
            { locationSpot: go.Spot.Center },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            { selectable: true, },

            { resizable: true, resizeObjectName: "PANEL" },
            {
                toolTip:  // define a tooltip for each node that displays the color as text
                    MK(go.Adornment, "Auto",
                        MK(go.Shape, { fill: "#fff" }),
                        MK(go.TextBlock, { margin: 6 },
                            new go.Binding("text", "DisplayName"))
                    )
            },
            // the main object is a Panel that surrounds a TextBlock with a Shape
            MK(go.Panel, "Horizontal",
                MK(go.TextBlock,
                    new go.Binding("text", "DisplayName"),
                    {
                        stroke: "white",
                        width: 300,
                        font: "16pt",
                        maxLines: 3,
                        wrap: go.TextBlock.wrap,
                        margin: 2,
                        //height:2,
                        background: "black",
                        textAlign: "center",
                    }
                )
            ),
            // four small named ports, one on each side:
            makePort("T", go.Spot.Top, go.Spot.TopSide, false, true),
            makePort("L", go.Spot.Left, go.Spot.LeftSide, true, true),
            makePort("R", go.Spot.Right, go.Spot.RightSide, true, true),
            makePort("B", go.Spot.Bottom, go.Spot.BottomSide, true, false),
            {
            }
        )
    );

    function diagramInfo(model) {
        return "Model:\n" + (model.nodeDataArray.length - 1) + " nodes, " +
            model.linkDataArray.length + " links";
    }

    // provide a tooltip for the background of the Diagram, when not over any Part
    myDiagram.toolTip =
        MK(go.Adornment, "Auto",
            MK(go.Shape, { fill: "#006dd5" }),
            MK(go.TextBlock, { margin: 4 },
                // use a converter to display information about the diagram model
                new go.Binding("text", "", diagramInfo))
        );
    myDiagram.nodeTemplateMap.add("Start",
        MK(go.Node, "Spot", nodeStyle(),
            MK(go.Panel, "Auto",
                MK(go.Shape, "Circle",
                    { minSize: new go.Size(40, 40), fill: "#79C900", stroke: null }),
                MK(go.TextBlock, "Start",
                    { font: "bold 11pt Roboto, sans-serif !important", stroke: lightText },
                    new go.Binding("text"))
            ),
            // three named ports, one on each side except the top, all output only:
            makePort("T", go.Spot.Top, go.Spot.TopSide, false, true),
            makePort("L", go.Spot.Left, go.Spot.LeftSide, true, true),
            makePort("R", go.Spot.Right, go.Spot.RightSide, true, true),
            makePort("B", go.Spot.Bottom, go.Spot.BottomSide, true, false)
        )
    );

    myDiagram.nodeTemplateMap.add("End",
        MK(go.Node, "Spot", nodeStyle(),
            MK(go.Panel, "Auto",
                MK(go.Shape, "Circle",
                    { minSize: new go.Size(40, 40), fill: "#DC3C00", stroke: null }),
                MK(go.TextBlock, "End",
                    { font: "bold 11pt Roboto, sans-serif !important", stroke: lightText },
                    new go.Binding("text"))
            ),
            // three named ports, one on each side except the bottom, all input only:
            makePort("T", go.Spot.Top, go.Spot.TopSide, false, true),
            makePort("L", go.Spot.Left, go.Spot.LeftSide, true, true),
            makePort("R", go.Spot.Right, go.Spot.RightSide, true, true),
            makePort("B", go.Spot.Bottom, go.Spot.BottomSide, true, false)
        )
    );

    myDiagram.nodeTemplateMap.add("Comment",
        MK(go.Node, "Auto", nodeStyle(),
            MK(go.Shape, "File",
                { fill: "#EFFAB4", stroke: null }),
            MK(go.TextBlock,
                {
                    margin: 5,
                    maxSize: new go.Size(200, NaN),
                    wrap: go.TextBlock.wrap,
                    textAlign: "center",
                    editable: true,
                    font: "bold 16pt Roboto, sans-serif !important",
                    stroke: '#454545'
                },
                new go.Binding("text").makeTwoWay())

        )
    );

    myDiagram.linkTemplate =
        MK(go.Link,  // the whole link panel
            {
                routing: go.Link.AvoidsNodes,
                curve: go.Link.JumpOver,
                corner: 5, toShortLength: 4,
                relinkableFrom: true,
                relinkableTo: true,
                reshapable: true,
                resegmentable: true,
                // mouse-overs subtly highlight links:
                mouseEnter: function (e, link) { link.findObject("HIGHLIGHT").stroke = "transparent"; },
                mouseLeave: function (e, link) { link.findObject("HIGHLIGHT").stroke = "transparent"; }
            },
            new go.Binding("points").makeTwoWay(),
            MK(go.Shape,  // the highlight shape, normally transparent
                { isPanelMain: true, strokeWidth: 2, stroke: "transparent", name: "HIGHLIGHT" }),
            MK(go.Shape,  // the link path shape
                { isPanelMain: true, stroke: "#1e2a41", strokeWidth: 2 }),
            MK(go.Shape,  // the arrowhead
                { toArrow: "standard", stroke: null, fill: "#1e2a41" }),
            MK(go.Panel, "Auto",  // the link label, normally not visible
                { visible: false, name: "LABEL", segmentIndex: 2, segmentFraction: 0.5 },
                new go.Binding("visible", "visible").makeTwoWay(),
                MK(go.Shape, "RoundedRectangle",  // the label shape
                    { fill: "#F8F8F8", stroke: null }),
                MK(go.TextBlock, "Yes",  // the label
                    {
                        textAlign: "center",
                        font: "10pt Roboto, sans-serif !important",
                        stroke: "#333333",
                        editable: true
                    },
                    new go.Binding("text").makeTwoWay())
            )
        );

    // Make link labels visible if coming out of a "conditional" node.
    // This listener is called by the "LinkDrawn" and "LinkRelinked" DiagramEvents.
    function showLinkLabel(e) {
        var label = e.subject.findObject("LABEL");
        if (label !== null) label.visible = (e.subject.fromNode.data.figure === "Diamond");
    }

    // temporary links used by LinkingTool and RelinkingTool are also orthogonal:
    myDiagram.toolManager.linkingTool.temporaryLink.routing = go.Link.Orthogonal;
    myDiagram.toolManager.relinkingTool.temporaryLink.routing = go.Link.Orthogonal;

    function highlightGroup(grp, show) {
        if (!grp) return;
        if (show) {  // check that the drop may really happen into the Group
            var tool = grp.diagram.toolManager.draggingTool;
            var map = tool.draggedParts || tool.copiedParts;  // this is a Map
            if (grp.canAddMembers(map.toKeySet())) {
                grp.isHighlighted = true;
                return;
            }
        }
        grp.isHighlighted = false;
    }

    var groupFill = "rgba(128,128,128,0)";
    var groupStroke = "rgba(128,128,128,0)";
    var dropFill = "rgba(128,255,255,0)";
    var dropStroke = "white";

    myDiagram.groupTemplate =
        MK(go.Group,
            {
                layerName: "Background",
                resizable: true, resizeObjectName: "SHAPE",
                // because the gridSnapCellSpot is Center, offset the Group's location
                locationSpot: new go.Spot(0, 0, CellSize.width / 2, CellSize.height / 2),
                deletable: false,
                selectable: false

            },
            // always save/load the point that is the top-left corner of the node, not the location
            new go.Binding("position", "pos", go.Point.parse).makeTwoWay(go.Point.stringify),
            { // what to do when a drag-over or a drag-drop occurs on a Group
                mouseDragEnter: function (e, grp, prev) {

                    highlightGroup(grp, true);
                },
                mouseDragLeave: function (e, grp, next) { highlightGroup(grp, false); },
                mouseDrop: function (e, grp) {
                    var flag = 0
                    if (grp.diagram.selection.Ea.key.Zd["StepID"] == undefined) {
                        if (Object.keys(LocalSteps).length != 0) {
                            var Action_Index = ActionJSON.findIndex(p => p.Id == grp.diagram.selection.Ea.key.Zd["Action_Id"])
                            for (var i in LocalSteps) {


                                if (ActionJSON[Action_Index]["Dependency"] != 0) {
                                    if (LocalSteps[i]["Action_Id"] == ActionJSON[Action_Index]["Dependency"]) {
                                        flag++;
                                    }

                                }
                                else {
                                    if (flag == 0)
                                        flag++;
                                }
                                if (LocalSteps[i]["Action_Id"] == 1) {
                                    flag++;
                                }

                                /*if (LocalSteps[i]["Action_Id"] == 2)
                                {
                                    flag++;
                                }*/

                            }
                        }
                        else {
                            if (grp.diagram.selection.Ea.key.Zd["Action_Id"] == 1) {
                                flag = 2;
                            }
                        }

                        var stepstop = 0;
                        var stepstart = 0;
                        DiagramModel = JSON.parse(myDiagram.model.toJson());
                        myDiagram.isModified = false;

                        for (i = 1; i < DiagramModel.nodeDataArray.length; i++) {

                            if (DiagramModel.nodeDataArray[i].Name == "Start") {

                                stepstart++;
                            }
                            else if (DiagramModel.nodeDataArray[i].Name == "Stop") {
                                stepstop++;

                            }
                        }

                        if ((stepstart > 1 && grp.diagram.selection.Ea.key.Zd.Name == "Start")) {
                            grp.diagram.currentTool.doCancel();
                            swal("", "Action was already added. Please check", "error")
                        }
                        else {

                            if ((flag >= 2) || grp.diagram.selection.Ea.key.Zd["Element_Id"].toString() == '1') {
                                $("#ValidateButton")[0].disabled = false;
                                var Robot_Step = grp.diagram.selection.Ea;

                                do {
                                    if (typeof (Robot_Step.key.Zd.from) == typeof (undefined)) {
                                        CreateStep(Robot_Step.key.Zd);

                                        Robot_Step.key.Zd["StepID"] = StepCustomID;
                                        Robot_Step.key.Zd["Left"] = parseInt(Robot_Step.key.Si.x);
                                        Robot_Step.key.Zd["Top"] = parseInt(Robot_Step.key.Si.y);
                                        loadActionProperties(StepCustomID, Robot_Step.key.Zd["Name"], Robot_Step.key.Zd["Action_Id"], Robot_Step.key.Zd.source.split("/")[2], 0, Robot_Step.key.Zd);
                                        saveStepProperties();
                                        Robot_Step.key.Zd["StepProperties"] = SavedStepPropertiesIds;
                                        loadStepProperties(Robot_Step.key.Zd["Name"], Robot_Step.key.Zd["StepID"], Robot_Step.key.Zd["StepProperties"])
                                    }
                                    Robot_Step = Robot_Step.Xa;
                                } while (Robot_Step != null);
                                var ok = grp.addMembers(grp.diagram.selection, true);
                                if (!ok) grp.diagram.currentTool.doCancel();
                                updateClient_Dependency();
                            }
                            else {

                                var Robot_Step = grp.diagram.selection.Ea;
                                var dependID;
                                var dependAction;
                                var dependeeAction;

                                do {
                                    if (typeof (Robot_Step.key.Zd.from) == typeof (undefined)) {
                                        dependID = Robot_Step.key.Zd["Dependency"];
                                        dependeeAction = Robot_Step.key.Zd["Name"];
                                        break;
                                    }
                                } while (Robot_Step != null);
                                if (dependID == 0) {
                                    dependAction = "Start";
                                } else {
                                    for (var t in ActionJSON) {
                                        if (ActionJSON[t].Id == dependID) {
                                            dependAction = ActionJSON[t].Name;
                                        }
                                    }
                                }
                                grp.diagram.currentTool.doCancel();
                                swal("", "Please add dependency. This action item '" + dependeeAction + "' has dependency on '" + dependAction + "' Please add that action first!!!", "error");
                            }
                        }
                    }
                    else {
                        loadStepProperties(grp.diagram.selection.Ea.key.Zd["Name"], grp.diagram.selection.Ea.key.Zd["StepID"], grp.diagram.selection.Ea.key.Zd["StepProperties"]);
                        for (n = 0; n < $("[name='PropertyInput']").parent().parent().find('td.col-sm-2').length; n++) {
                            if ($("[name='PropertyInput']").parent().parent().find('td.col-sm-2')[n].innerHTML == "Left") {
                                $("[name='PropertyInput']")[n].value = Math.ceil(Math.abs(grp.diagram.selection.Ea.key.Zd["loc"].split(" ")[0]))
                                $($("[name='PropertyInput']")[n]).blur()
                            }
                            else if ($("[name='PropertyInput']").parent().parent().find('td.col-sm-2')[n].innerHTML == "Top") {
                                $("[name='PropertyInput']")[n].value = Math.ceil(Math.abs(grp.diagram.selection.Ea.key.Zd["loc"].split(" ")[1]))
                                $($("[name='PropertyInput']")[n]).blur()
                            }
                        }
                    }
                }
            },
            MK(go.Shape,   // the rectangular shape around the members
                {
                    name: "SHAPE",
                    fill: groupFill,
                    stroke: groupStroke,
                    // minSize: new go.Size(CellSize.width*2, CellSize.height*2)
                },
                new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
                new go.Binding("fill", "isHighlighted", function (h) { return h ? dropFill : groupFill; }).ofObject(),
                new go.Binding("stroke", "isHighlighted", function (h) { return h ? dropStroke : groupStroke; }).ofObject())
        );

    // decide what kinds of Parts can be added to a Group
    myDiagram.commandHandler.memberValidation = function (grp, node) {
        if (grp instanceof go.Group && node instanceof go.Group) return false;  // cannot add Groups to Groups
        // but dropping a Group onto the background is always OK
        return true;
    };

    // what to do when a drag-drop occurs in the Diagram's background
    myDiagram.mouseDragOver = function (e) {
        if (!AllowTopLevel) {
            // but OK to drop a group anywhere
            if (!e.diagram.selection.all(function (p) { return p instanceof go.Group; })) {
                e.diagram.currentCursor = "not-allowed";
            }
        }
    };

    myDiagram.mouseDrop = function (e) {
        if (typeof (e.tr.target.fa) != typeof (undefined)) {
            var pattern = e.tr.target.fa.Ei.Ea;
            while (pattern.Xa != null) {
                pattern = pattern.Xa
            }
            pattern = pattern.key;
            if (pattern["StepID"] == undefined) {

                CreateStep(pattern);
                pattern["StepID"] = StepCustomID;
                //grp.diagram.selection["CustomID"]=StepCustomID;
                loadActionProperties(StepCustomID, pattern["Name"], pattern["Action_Id"], pattern.source.split("/")[2], 0, pattern);
                saveStepProperties();
                pattern["StepProperties"] = SavedStepPropertiesIds;
                // grp.diagram.selection.Ea.key.Zd["StepPropertiesLength"] = StepPropertiesLength;
                //loadActionProperties(StepCustomID, grp.diagram.selection.Ea.key.Zd.Action_Id, grp.diagram.selection.Ea.key.Zd.source.split("/")[2], grp.diagram.selection.Ea.key.Zd["StepProperties"]);
                loadStepProperties(pattern["Name"], pattern["StepID"], pattern["StepProperties"])
                updateClient_Dependency();
            }
            else {
                // loadActionProperties(grp.diagram.selection.Ea.key.Zd["StepID"], globalIndex, grp.diagram.selection.Ea.key.Zd.source.split("/")[2], grp.diagram.selection.Ea.key.Zd["StepProperties"]);
                loadStepProperties(pattern["Name"], pattern["StepID"], pattern["StepProperties"]);
                for (n = 0; n < $("[name='PropertyInput']").parent().parent().find('td.col-sm-2').length; n++) {
                    if ($("[name='PropertyInput']").parent().parent().find('td.col-sm-2')[n].innerHTML == "Left") {
                        $("[name='PropertyInput']")[n].value = Math.ceil(Math.abs(pattern["loc"].split(" ")[0]))
                        $($("[name='PropertyInput']")[n]).blur()
                    }
                    else if ($("[name='PropertyInput']").parent().parent().find('td.col-sm-2')[n].innerHTML == "Top") {
                        $("[name='PropertyInput']")[n].value = Math.ceil(Math.abs(pattern["loc"].split(" ")[1]))
                        $($("[name='PropertyInput']")[n]).blur()
                    }
                }
            }
            if (AllowTopLevel) {
                // when the selection is dropped in the diagram's background,
                // make sure the selected Parts no longer belong to any Group
                if (!e.diagram.commandHandler.addTopLevelParts(e.diagram.selection, true)) {
                    e.diagram.currentTool.doCancel();
                }
            } else {
                // disallow dropping any regular nodes onto the background, but allow dropping "racks"
                if (!e.diagram.selection.all(function (p) { return p instanceof go.Group; })) {

                    e.diagram.currentTool.doCancel();
                }
            }
        }
        else {

        }
    };

    //singleclick event for properties

    myDiagram.addDiagramListener("ObjectSingleClicked", function (e) {

        if (e.Ew.Dd == undefined) {

            $('#right-panel').show();
            $('#properties-panel').show();
            $('#projectexplorerhide').removeClass('bg-act');
            $('#propertieshide').addClass('bg-act');
            $('#versionshide').removeClass('bg-act');
            $('#PropertyTable').show();
            $('#noPropertiesDisplay').hide();
            $('#projectexplorer-panel').hide();
            $('#versions-panel').hide();
            $('#middel-panel').addClass('limitWidthP');
            $('.right-panel-header-value').html('Properties');
        }
        else {
            $('#right-panel').hide();
            $('#properties-panel').hide();
            $('#noPropertiesDisplay').show();
            $('#PropertyTable').hide();
            myDiagram.clearSelection();
            $('#middel-panel').removeClass('limitWidthP');
            $('.right-panel-header-value').html('');
        }

        if (e.fa.Aw.Ea != null && e.fa.Aw.Ea.key.Zd["StepID"] != undefined) {


            $("#ValidateButton")[0].disabled = false;
            validateStepId = e.fa.Aw.Ea.key.Zd["StepID"];
            debugStepId = e.fa.Aw.Ea.key.Zd["StepID"];
            loadStepProperties(e.fa.Aw.Ea.key.Zd["Name"], e.fa.Aw.Ea.key.Zd["StepID"], e.fa.Aw.Ea.key.Zd["StepProperties"]);
        }
        else {

            $('#right-panel').show();
            $('#properties-panel').show();
            $('#projectexplorerhide').removeClass('bg-act');
            $('#propertieshide').addClass('bg-act');
            $('#versionshide').removeClass('bg-act');
            $('#PropertyTable').show();
            $('#noPropertiesDisplay').hide();
            $('#projectexplorer-panel').hide();
            $('#versions-panel').hide();
            $('#middel-panel').addClass('limitWidthP');
            $('.right-panel-header-value').html('Properties');
            loadRobotProperties();
        }

    });
    myDiagram.addDiagramListener("SelectionDeleted", function (e) {

        if (e.fa.Aw.Qb.Va.key.Zd.StepID != undefined) {
            deleteStep(e);
            updateClient_Dependency();
        }
    });
    // start off with four "racks" that are positioned next to each other
    myDiagram.model = new go.GraphLinksModel([
        { key: "G1", isGroup: true, pos: "0 0", size: window.outerWidth / 1.54 + " " + window.outerHeight / 0.9 },
        /* { key: "G2", isGroup: true, pos: "200 0", size: "705 510" },
         { key: "G3", isGroup: true, pos: "0 200", size: "705 510" },
         { key: "G4", isGroup: true, pos: "200 200", size: "705 510" }*/
    ]);

    // this sample does not make use of any links
    //Magi  loadPalette();

    setHeightWidth();
    ProcessLoad();
    if (Robots.length != 0) {
        setTimeout(function () {
            loadRobot();
        }, 800)
    }
    else {
        $("#addModal").modal('show');
        $(".close").hide();
        $(".cancel").hide();
    }
    //myDiagram.allowHorizontalScroll = false;
    //myDiagram.allowVerticalScroll = false;
}//init close

function showPorts(node, show) {
    var diagram = node.diagram;
    if (!diagram || diagram.isReadOnly || !diagram.allowLink) return;
    node.ports.each(function (port) {
        port.stroke = (show ? "white" : null);
    });
}
function saveValueStream() {
    var data1 = [];
    localStorage.Points = myDiagram.model.toJson();
    DiagramModel = JSON.parse(myDiagram.model.toJson());
    myDiagram.isModified = false;
    for (i = 1; i < DiagramModel.nodeDataArray.length; i++) {
        var data = {};
        data["StepID"] = DiagramModel.nodeDataArray[i]["StepID"];
        data["loc"] = DiagramModel.nodeDataArray[i]["loc"];
        data["Childs"] = ""
        for (j = 0; j < DiagramModel.linkDataArray.length; j++) {

            if (DiagramModel.linkDataArray[j]["from"] == DiagramModel.nodeDataArray[i]["key"])
                data["Childs"] += DiagramModel.linkDataArray[j]["to"] + "`"
        }
        if (data["Childs"] != "") {
            var Childs = data["Childs"].split("`");
            data["Childs"] = "";
            for (k = 1; k < DiagramModel.nodeDataArray.length; k++) {

                for (child in Childs) {

                    if (Childs[child] == DiagramModel.nodeDataArray[k]["key"]) {
                        data["Childs"] += DiagramModel.nodeDataArray[k]["StepID"] + "`";
                    }
                }
            }
        }
        else
            data["Childs"] = 0;
        data1[i - 1] = data;
    }

    var dataString = JSON.stringify(data1);

    $.ajax({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        type: 'GET',
        url: datasource,
        url: GlobalURL + '/api/DesignStudio/InsertLinks?input=' + dataString,
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (data2) {

        },
        error: function (e) {

        }
    });
    return false;
}

function saveFlowChart() {
    var data1 = [];
    localStorage.Points = myDiagram.model.toJson();
    DiagramModel = JSON.parse(myDiagram.model.toJson());
    myDiagram.isModified = false;
    for (i = 1; i < DiagramModel.nodeDataArray.length; i++) {
        var data = {};
        data["StepID"] = DiagramModel.nodeDataArray[i]["StepID"];
        data["loc"] = DiagramModel.nodeDataArray[i]["loc"];
        data["Childs"] = ""
        for (j = 0; j < DiagramModel.linkDataArray.length; j++) {

            if (DiagramModel.linkDataArray[j]["from"] == DiagramModel.nodeDataArray[i]["key"])
                data["Childs"] += DiagramModel.linkDataArray[j]["to"] + "`"
        }
        if (data["Childs"] != "") {
            var Childs = data["Childs"].split("`");
            data["Childs"] = "";
            for (k = 1; k < DiagramModel.nodeDataArray.length; k++) {

                for (child in Childs) {

                    if (Childs[child] == DiagramModel.nodeDataArray[k]["key"]) {
                        data["Childs"] += DiagramModel.nodeDataArray[k]["StepID"] + "`";
                    }
                }
            }
        }
        else
            data["Childs"] = 0;
        data1[i - 1] = data;
    }

    var dataString = JSON.stringify(data1);

    $.ajax({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        type: 'GET',
        url: datasource,
        url: '/services/DesktopService.svc/InsertLinks?input=' + dataString,
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (data2) {

        },
        error: function (e) {

        }
    });
    return false;
}

function save() {

    $("#loader").css('display', 'block !important');
    $('#myDropdown').removeClass("show");
    //if (RobotType != 'Robot') saveValueStream(); saveFlowChart();
    var data1 = [];
    localStorage.Points = myDiagram.model.toJson();
    DiagramModel = JSON.parse(myDiagram.model.toJson());
    myDiagram.isModified = false;
    if (Sname != "Empty") {
        for (i = 1; i < DiagramModel.nodeDataArray.length; i++) {
            if (DiagramModel.nodeDataArray[i]["isGroup"] != true) {
                var data = {};
                data["StepId"] = DiagramModel.nodeDataArray[i]["StepID"];
                data["Location"] = DiagramModel.nodeDataArray[i]["loc"];
                data["Status"] = 1;
                data["ChildStepIds"] = ""
                data["CreateBy"] = (JSON.parse(localStorage.Result).User_ID);
                data["UpdateBy"] = (JSON.parse(localStorage.Result).User_ID);
                for (j = 0; j < DiagramModel.linkDataArray.length; j++) {
                    if (DiagramModel.linkDataArray[j]["from"] == DiagramModel.nodeDataArray[i]["key"])
                        data["ChildStepIds"] += DiagramModel.linkDataArray[j]["to"] + "`"
                }

                if (data["ChildStepIds"] != "") {
                    var Childs = data["ChildStepIds"].split("`");
                    data["ChildStepIds"] = "";
                    for (k = 1; k < DiagramModel.nodeDataArray.length; k++) {

                        for (child in Childs) {

                            if (Childs[child] == DiagramModel.nodeDataArray[k]["key"]) {
                                data["ChildStepIds"] += DiagramModel.nodeDataArray[k]["StepID"] + "`";

                            }
                        }
                    }
                }
                else
                    data["ChildStepsIds"] = 0;
                data1.push(data);
            }
        }

        var formdata = {};
        var data2 = {
        };
        data2["Type"] = "BulkSave";
        var x = 0;
        var SaveSteps = [CreateBy = JSON.parse(localStorage.Result).User_ID,
        UpdateBy = JSON.parse(localStorage.Result).User_ID];
        for (i in LocalSteps) {
            LocalSteps[i]["Id"] = i;
            SaveSteps[x++] = LocalSteps[i];
        }

        var y = 0; var z = 0;
        var SaveStepProperties = [
        ];
        for (i in localStepProperties) {
            var SSP = [];
            z = 0;
            for (j in localStepProperties[i]) {
                localStepProperties[i][j]["Id"] = j;
                SSP[z++] = localStepProperties[i][j]
            }
            if (SSP.length != 0) {
                SaveStepProperties[y++] = SSP;
            }
        }

        if (SaveSteps.length == 0 || SaveStepProperties == 0) {
            swal("", "Cannot save without steps", "error");
        }

        var StepIdArray = [];
        for (var save_step = 0; save_step < SaveSteps.length; save_step++) {
            SaveSteps[save_step]["StepProperties"] = SaveStepProperties[save_step];
            if (SaveSteps[save_step]["Name"] == "Stop") {
                StepIdArray.push(SaveSteps[save_step]["Id"]);
            }

        }
        var scount = 0;

        for (var save_step = 0; save_step < SaveSteps.length; save_step++) {
            SaveSteps[save_step]["StepProperties"] = SaveStepProperties[save_step];
        }
        data2["jsonString1"] = JSON.stringify(SaveSteps);
        data2["jsonString2"] = JSON.stringify(data1);
        data["jsonString3"] = JSON.stringify(DVariables);
        var lcount = 0;
        if (scount == 0) {
            for (var link = 1; link < data1.length; link++) {
                var a = StepIdArray.indexOf("" + data1[link]["StepId"] + "");
                if (data1[link]["ChildStepIds"] == null || data1[link]["ChildStepIds"] == "") {
                    if (a < 0) {
                        lcount++;
                        swal("", "Robot not saved. Please link the Steps to save robot", "error")
                        break;
                    }
                }
            }
        }
        if (SaveSteps.findIndex(step => step["Name"] == "Start") == -1) {
            swal("", "Please Add Start Action to Save the Robot", "error");
        }
        else if (SaveSteps.findIndex(step => step["Name"] == "Stop") == -1) {
            swal("", "Please Add Stop Action to Save the Robot", "error");
        }
        else if (lcount == 0 && scount == 0) {
            finaldata = {
                "Project_Id": $(".Prjbtn")[0].id,
                "Type": "SaveRobot",
                "Robot_ID": RobotID,
                "robot": {
                    "Id": RobotID,
                    "Name": $(".Rbtbtn.active")[0].innerText,
                    "Project_Id": $(".Prjbtn")[0].id,
                    "Steps": SaveSteps,
                    "LinkNodes": data1,
                    "dynamicvariables": DVariables,
                    "RProperties": LocalRobotProperties,
                    "CreateBy": JSON.parse(localStorage.Result).User_ID,
                    "UpdateBy": JSON.parse(localStorage.Result).User_ID
                }
            }
            //formdata = {
            //    input: [
            //        { Key: 'input', Value: JSON.stringify(finaldata) },

            //    ]
            //}

            var dataString = JSON.stringify(finaldata);

            $.ajax({

                type: "POST",
                url: GlobalURL + '/api/DesignStudio/SaveWorkflow',
                contentType: 'application/json; charset=utf-8',
                data: dataString,
                async: false,
                success: function (dat) {
                    var dataSource1 = $.parseJSON(dat);
                    $("#loader").css('display', 'none !important');
                    if (dataSource1["ResultString"] == "robot updated") {
                        $("#savedModel")[0].innerHTML += "\n>'" + $(".Rbtbtn.active")[0].innerText + "' Robot Updated";
                        swal({
                            title: "",
                            text: "Robot Updated Successfully",
                            icon: "success",
                            //  timer: 2000,
                            button: true,
                            closeOnClickOutside: false,
                        });
                        document.getElementsByClassName('Rbtbtn active')[0].children[1].style.display = "none";
                        $("#footerOutput").focus();
                        // document.getElementsByClassName('Rbtbtn active')[0].children[1].style.display = "none";
                        ProcessLoad();
                        versions(RobotID);
                        if (Robots.length != 0)
                            loadRobot();
                        else {
                            $("#addModal").modal('show');
                            $(".close").hide();
                            $(".cancel").hide();
                        }
                    }
                    else if (dataSource1["ResultString"] == "steps saved") {
                        $("#savedModel")[0].innerHTML += "\n>'" + $(".Rbtbtn.active")[0].innerText + "' Robot Saved and Version Created";
                        swal({
                            title: "",
                            text: "Robot Saved and Version Created Successfully",
                            icon: "success",
                            timer: 2000,
                            button: false,
                        });
                        document.getElementsByClassName('Rbtbtn active')[0].children[1].style.display = "none";
                        $("#footerOutput").focus();
                        // document.getElementsByClassName('Rbtbtn active')[0].children[1].style.display = "none";
                        ProcessLoad();
                        versions(RobotID);
                        if (Robots.length != 0)
                            loadRobot();
                        else {
                            $("#addModal").modal('show');
                            $(".close").hide();
                            $(".cancel").hide();
                        }
                    }
                    else if (dataSource1["ResultString"] == "robot saved and version created") {
                        $("#savedModel")[0].innerHTML += "\n>'" + $(".Rbtbtn.active")[0].innerText + "' Robot Saved ";
                        swal({
                            title: "",
                            text: "Robot Saved Successfully",
                            icon: "success",
                            // timer: 2000,
                            button: true,
                            closeOnClickOutside: false,
                        });
                        document.getElementsByClassName('Rbtbtn active')[0].children[1].style.display = "none";
                        $("#footerOutput").focus();
                        //
                        ProcessLoad();
                        versions(RobotID);
                        if (Robots.length != 0)
                            loadRobot();
                        else {
                            $("#addModal").modal('show');
                            $(".close").hide();
                            $(".cancel").hide();
                        }

                    }
                    else {
                        swal("", "Robot not saved", "error");
                        $("#footerOutput").focus();
                    }
                },
                error: function (e) {

                }
            });
        }
    }
    else {
        //alert("Step Name can\'t be empty");
    }
}
var validateStepId = "";
var debugStepId = "";
var nextStepId = "";
function stepoverclick() {
    document.getElementById("debugdropdown").classList.toggle("show");
    var input1 = {};
    if (nextStepId != "") {
        input1["Step_Id"] = nextStepId;
    }
    else {
        input1["Step_Id"] = 0;
    }
    input1["rid"] = RobotID;
    var obj1 = JSON.stringify(input1);
    var debugoutput = "";
    $.ajax({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        async: false,
        type: "GET",
        url: GlobalURL + '/api/DesignStudio/debug_Execution?input=' + JSON.stringify(input1),
        contentType: 'application/json; charset=utf-8',
        success: function (data2) {
            var debugList = "";
            var debug = "";
            var debugoutput = "";
            var debugListoutput = "";
            var value = JSON.parse(data2.debug_ExecutionResult);
            debugoutput = JSON.parse(value.ResultString);
            nextStepId = debugoutput.getNextstepid;
            if (debugoutput.Description != null) {
                var z = JSON.parse(debugoutput.Description);
                $("#dynamicModal").modal('show');
                $('#label_stepname').html('Stepname:' + z.StepName + '');

                $('#outputcount').html('Output:[' + z.Output.length + "]" + '');
                $("#DebugOutput").show();
                debug = "<ul><li><p style='display:none'>StepName:" + z.StepName + "</p><ul>";
                for (i in z.Input) {
                    debug += "<li>" + z.Input[i] + "</li>"
                }
                debug += "</ul></li></ul>";
                debugList += debug;
                $("#debuginputlist").html(debugList);
                debugoutput = "<ul><li><p style='display:none'>StepName:" + z.StepName + "</p><ul>";
                for (j in z.Output) {
                    debugoutput += "<li>" + z.Output[j] + "</li>"
                }
                debugoutput += "</ul></li></ul>";
                debugListoutput += debugoutput;

                $("#debugoutputlist").html(debugListoutput);
                $('#debugoutputlist,#debuginputlist').jstree({
                });
            }
        },
        error: function (e) {

        }
    });
}
var debugType;
function stepOnClick() {
    debugType = "StepInto";
}
function Validate() {
    var LocalStepPairs = {};
    var stepproparray = [];
    for (var i in LocalSteps) {
        if (i == validateStepId) {
            LocalStepPairs = {};
            LocalStepPairs["Action_Id"] = LocalSteps[i]["Action_Id"];
            LocalStepPairs["DisplayName"] = LocalSteps[i]["DisplayName"];
            LocalStepPairs["Element_Id"] = LocalSteps[i]["Element_Id"];
            LocalStepPairs["Name"] = LocalSteps[i]["Name"];
            LocalStepPairs["Robot_Id"] = LocalSteps[i]["Robot_Id"];
            LocalStepPairs["RuntimeUserInput"] = LocalSteps[i]["RuntimeUserInput"];
            LocalStepPairs["Status"] = LocalSteps[i]["Status"];
            LocalStepPairs["Workflow_Id"] = LocalSteps[i]["Workflow_Id"];
            $.each(localStepProperties[validateStepId], function (key, val) {
                stepproparray.push(val);
            });
            LocalStepPairs["StepProperties"] = stepproparray;
        }
    }
    var LocalStepValues1 = {};
    LocalStepValues1["LocalStepPairs"] = JSON.stringify(LocalStepPairs);
    $.ajax({
        type: "POST",
        url: GlobalURL + '/api/DesignStudio/Validate',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(LocalStepValues1),
        async: false,
        success: function (val) {
            var dataSource1 = $.parseJSON(val);
            if (dataSource1["ResultString"].toLowerCase() == "success") {
                swal("", "Step validated Successfully", "success", { closeOnClickOutside: false });
            }
            else {
                swal("", "Step not validated. Please enter valid credentials", "error", { closeOnClickOutside: false });
            }
        },
        error: function (e) {

        }
    });
}
//clear
function clear1() {
    $("#myDiagramDiv").html("");
    location.reload();
}
function loadProcess(id, type) {
    RobotID = id;
    RobotType = type;
    loadImagesByAjax();
    ProcessLoad();
    if (Robots.length != 0) {
        loadRobot();
    }
    else {
        $("#addModal").modal('show');
        $(".close").hide();
        $(".cancel").hide();
    }
}
function loadRobots(id, type) {
    RobotID = id;
    RobotType = type;
    loadImagesByAjax();
    if (Robots.length != 0) {
        loadRobot();
        //actionclick();
        //actiononclick();
    }
    else {
        $("#addModal").modal('show');
        $(".close").hide();
        $(".cancel").hide();
    }
}
//load
var Robots;
var StepProperties;

function loadRobot() {
    StepsProperties = [];
    LocalSteps = {};
    localStepProperties = {};
    LocalRobotProperties = {};
    var i = 0;
    var index = Robots.findIndex(p => p.Id == RobotID)
    if (index >= 0) {
        LocalRobotProperties = Robots[index]["RProperties"];
    }
    if (Robots[index]["dynamicvariables"] != null)
        DVariables = Robots[index]["dynamicvariables"];
    for (t in Object.keys(Robots[index]["Steps"])) {
        var StepsTemp = {}
        var DisplayName;
        var localSP = {};
        StepsTemp["StepProperties"] = "";
        StepPropertiesJSON = Robots[index]["Steps"][t].StepProperties;


        for (chk = 0; chk < StepPropertiesJSON.length; chk++) {
            if (StepPropertiesJSON[chk].CustomValidation == "1" && StepPropertiesJSON[chk].StepPropertyValue == "") {
                document.getElementsByClassName('Rbtbtn active')[0].children[1].style.display = "inline-block";
            }
        }

        for (var k in Object.keys(StepPropertiesJSON)) {
            if (StepPropertiesJSON[k].Steps_Id == Robots[index]["Steps"][t].Id) {
                StepsTemp["StepProperties"] += StepPropertiesJSON[k].Id + " "
            }
            var y = {
            };
            y["Project_Id"] = StepPropertiesJSON[k].Project_Id;
            y["StepProperty"] = StepPropertiesJSON[k].StepProperty;
            y["StepPropertyType"] = StepPropertiesJSON[k].StepPropertyType;
            if (StepPropertiesJSON[k].StepProperty == "Name")
                DisplayName = StepPropertiesJSON[k].StepPropertyValue;
            y["StepPropertyValue"] = StepPropertiesJSON[k].StepPropertyValue;
            y["Steps_Id"] = StepPropertiesJSON[k].Steps_Id;
            y["Order"] = StepPropertiesJSON[k].Order;
            y["CustomValidation"] = StepPropertiesJSON[k].CustomValidation;
            y["DefaultValue"] = StepPropertiesJSON[k].DefaultValue;
            localSP[StepPropertiesJSON[k].Id] = y;
        }
        localStepProperties[Robots[index]["Steps"][t].Id] = localSP;
        StepsTemp["StepID"] = Robots[index]["Steps"][t].Id;
        StepsTemp["Action_Id"] = Robots[index]["Steps"][t].Action_Id;
        StepsTemp["Element_Id"] = Robots[index]["Steps"][t].Element_Id;
        StepsTemp["Name"] = Robots[index]["Steps"][t].Name;
        StepsTemp["DisplayName"] = DisplayName;
        var LinkNodesId = Robots[index]["LinkNodes"].findIndex(p => p.StepId == Robots[index]["Steps"][t].Id)
        if (LinkNodesId != -1) {
            StepsTemp["loc"] = Robots[index]["LinkNodes"][LinkNodesId].Location;
            StepsTemp["childs"] = Robots[index]["LinkNodes"][LinkNodesId].ChildStepIds;
        }
        else {
            StepsTemp["loc"] = "";
            StepsTemp["childs"] = "";
        }
        StepsProperties[i++] = StepsTemp;
        var x = {};
        x["Action_Id"] = Robots[index]["Steps"][t].Action_Id;
        x["Element_Id"] = Robots[index]["Steps"][t].Element_Id;
        x["Name"] = Robots[index]["Steps"][t].Name;
        x["Robot_Id"] = Robots[index]["Steps"][t].Robot_Id;
        x["Workflow_Id"] = "";
        x["DisplayName"] = DisplayName;
        x["Order"] = Robots[index]["Steps"][t].Order;
        x["Status"] = 1;
        x["RuntimeUserInput"] = Robots[index]["Steps"][t].RuntimeUserInput;

        LocalSteps[Robots[index]["Steps"][t].Id] = x;
    }
    var pointsData = {};
    pointsData["class"] = "go.GraphLinksModel";
    var node = {};
    node["key"] = "G1";
    node["isGroup"] = true;
    node["pos"] = "0 0";
    node["size"] = window.outerWidth / 1.54 + " " + window.outerHeight / 1.18;
    var nodeArray = [];
    var linkArray = [];
    nodeArray[0] = node;
    var linkIndex = 0;

    for (var p in StepsProperties) {
        var node = {};
        if (RobotType != 'Robot') {
            if (jQuery('.active').parent().hasClass('Workflow_Robot')) {
                switch (StepsProperties[p].Element_Id) {
                    case "1":
                        node["source"] = "Images/General/" + StepsProperties[p].Name + ".png";
                        break;
                    case "11":
                        node["source"] = "Images/Developer/" + StepsProperties[p].Name + ".png";;
                        break;
                    case "999":
                        node["source"] = "dist/img/robot_48.png";
                        break;
                }
            }
            else if (RobotType == 'FlowChart') {
                switch (StepsProperties[p].Element_Id) {
                    case "1": if (StepsProperties[p].Name != "Start" || StepsProperties[p].Name != "Stop" || StepsProperties[p].Name != "Open") {
                        node["source"] = "Images/FlowChart/" + StepsProperties[p].Name + ".png";

                    }

                }

            }
            else {
                switch (StepsProperties[p].Element_Id) {
                    case "1": if (StepsProperties[p].Name == "Start" || StepsProperties[p].Name == "Stop" || StepsProperties[p].Name == "Open") {
                        node["source"] = "Images/General/" + StepsProperties[p].Name + ".png";
                    }
                    else
                        node["source"] = "Images/ValueStreamMap/" + StepsProperties[p].Name + ".png";
                        break;

                }
            }
        }

        else {

            var EIndex = Elements.findIndex(q => q.Id == StepsProperties[p].Element_Id)
            if (EIndex > -1)
                node["source"] = "Images/" + Elements[EIndex].Name.trim() + "/" + StepsProperties[p].Name.trim() + ".png";
        }
        node["Name"] = StepsProperties[p].Name;
        node["Action_Id"] = StepsProperties[p].Action_Id;
        node["Element_Id"] = StepsProperties[p].Element_Id;
        node["category"] = "ImageNode";
        node["key"] = "-" + (parseInt(p) + 1);
        node["loc"] = StepsProperties[p].loc;
        node["DisplayName"] = StepsProperties[p].DisplayName;
        node["StepID"] = StepsProperties[p].StepID;
        node["StepProperties"] = StepsProperties[p].StepProperties;
        node["group"] = "G1";
        var Childs = StepsProperties[p].childs.split("`");
        for (var q in Childs) {
            if (Childs[q] != "") {
                var r = 0;
                while (r < StepsProperties.length) {
                    if (Childs[q] == StepsProperties[r].StepID) {
                        var link = {};
                        var a = StepsProperties[p].loc.split(" ");
                        var b = StepsProperties[r].loc.split(" ");
                        link["from"] = "-" + (parseInt(p) + 1);
                        link["to"] = "-" + (parseInt(r) + 1);
                        var linkPoints = [];
                        linkPoints[0] = a[0];
                        linkPoints[1] = a[1];
                        linkPoints[2] = b[1];
                        linkPoints[3] = b[2];
                        link["points"] = linkPoints;
                        linkArray[linkIndex++] = link
                    }
                    r++;
                }
            }
        }
        nodeArray[(parseInt(p) + 1)] = node;
    }
    pointsData["nodeDataArray"] = nodeArray;
    pointsData["linkDataArray"] = linkArray;
    loadRobotProperties();
    myDiagram.model = go.Model.fromJson(pointsData);
    if ($("#ProjectTree li .active")[0].id != RobotID) {
        $("#ProjectTree li .Rbtbtn").removeClass("active");
        document.getElementById('edit_Robot').remove();
        document.getElementById('delete_Robot').remove();
        for (var j in $("#ProjectTree li .Rbtbtn")) {
            if ($("#ProjectTree li .Rbtbtn")[j].id == RobotID) {
                $("#ProjectTree li .Rbtbtn ")[j].classList.add("active");
                break;
            }
        }
    }
    $("#ProjectBreadCrumb").html(" " + $("#ProjectTree").find('li a').first().text());
    $("#ProcessBreadCrumb").html($($("#ProjectTree li .active #RbtName")[0]).text());

    if ($($($(".active")[0]).parent()[0]).hasClass('Robot')) {

        $("#Processi").removeClass('ValueStreamMap');
        $("#Processi").addClass('Robot');
        $("#Processi").parent().removeClass('ValueStreamMap');
        $("#Processi").parent().addClass('Robot');
    }
    else if ($($($(".active")[0]).parent()[0]).hasClass('ValueStreamMap')) {
        $("#Processi").removeClass('fa fa-list-alt');
        $("#Processi").addClass('fa fa-indent');
    }
    else if ($($($(".active")[0]).parent()[0]).hasClass('FlowChart')) {
        $("#Processi").removeClass('fa fa-list-alt');
        $("#Processi").addClass('fa fa-indent');
    }
    else {
        $("#Processi").removeClass('fa fa-list-alt')
        $("#Processi").removeClass('fa fa-indent')
    }
}
function ProcessLoad() {
    $('#myDropdown').removeClass("show");
    var data = {};
    LocalSteps = {};
    localStepProperties = {};
    data["Type"] = "StepsLinks";
    if (typeof (RobotID) == 'undefined') RobotID = $('.Rbtbtn').first().attr('id');
    data["rid"] = RobotID;
    data["Project_Id"] = $(".Prjbtn")[0].id;
    data["CreateBy"] = JSON.parse(localStorage.Result).User_ID;
    var dataString = JSON.stringify(data);

    $.ajax({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        type: 'GET',
        url: datasource,
        url: GlobalURL + '/api/DesignStudio/Get?input=' + dataString,
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (data2) {
            Robots = JSON.parse(data2);
        },
        error: function (e) {

        }
    });
}
//reset
function reset() {
    ProcessLoad();
    if (Robots.length != 0)
        loadRobot();
    else {
        $("#addModal").modal('show');
        $(".close").hide();
        $(".cancel").hide();
    }
}

function makeSVG() {
    var svg = myDiagram.makeSvg({
        scale: 0.5
    });
    svg.style.border = "1px solid black";
    obj = document.getElementById("SVGArea");
    obj.appendChild(svg);
    if (obj.children.length > 0) {
        obj.replaceChild(svg, obj.children[0]);
    }
}

function EditBots(resData) {
    var data = {}
    data["ProjectName"] = resData[0].Project_Name;
    data["ProjectID"] = resData[0].Project_Id;
    data["RobotName"] = resData[0].Name;
    data["RobotID"] = resData[0].Id;
    data["RobotType"] = resData[0].RobotType;
    data["Stat"] = 'Edit';
    dataString = JSON.stringify(data);
    var EncodeData = btoa(unescape(encodeURIComponent(dataString)));
    var url = "planogram.html?" + EncodeData;
    window.location.href = url;
}

function addBots() {
    var data = {}
    data["ProjectName"] = $("#ProjectName").val();
    data["ProjectID"] = ProjectID;
    data["RobotName"] = $("#RobotName").val();
    data["RobotID"] = RobotID;
    data["RobotType"] = $("input[name='projectProcessType']:checked").val();
    data["Stat"] = 'Add';
    dataString = JSON.stringify(data);
    var EncodeData = btoa(unescape(encodeURIComponent(dataString)));
    if ($("input[name='projectType']:checked").val() == "Process") {
        switch ($("input[name='projectProcessType']:checked").val()) {
            case "ValueStreamMap":
                var url = "ValueStream.html?" + EncodeData;
                break;
        }
    }
    else {
        var url = "planogram.html?" + EncodeData;
    }
    window.location.href = url;
}

$(document).ready(function () {
    $('#myPropertiesDiv').attr('style', 'display: block');
    $('#properties-panel').attr('style', 'display: none');
    $("#createVar").draggable({
        handle: ".modal-header"
    });
    //$("#ScheduleForm").on("submit", function (e) {
    //    e.preventDefault();
    //    ScheduleFunction();
    //})

    var today = new Date().toISOString().split('T')[0];
    document.getElementsByName("sdatetimepicker")[0].setAttribute('min', today);

    $(".ListTitle").click(function (e) {

        //$('.fa-angle-right').css('transform', 'rotate(0deg)');
        var txt = $($(e.currentTarget).find('.fa-angle-right')).attr('style');
        if (txt != undefined) {
            if (txt.length > 0) {
                if (txt.includes('rotate(90deg)')) {
                    $($(e.currentTarget).find('.fa-angle-right')).css('transform', 'rotate(0deg)');
                }
                else {
                    $($(e.currentTarget).find('.fa-angle-right')).css('transform', 'rotate(90deg)');
                }
            }
        }
        else {
            $($(e.currentTarget).find('.fa-angle-right')).css('transform', 'rotate(90deg)');
        }
        if ($($($(e.currentTarget)[0]).parent().children()[1])[0].innerHTML != "")
            $($($($(e.currentTarget)[0]).parent().children()[1]).children()).toggle(1000);
    })
    $("#dynamicModal").draggable({
        handle: ".modal-header"
    });
    $(".CreateList").click(function (e) {

        //$('.fa-angle-right').css('transform', 'rotate(0deg)');
        var txt = $($(e.currentTarget).find('.fa-angle-right')).attr('style');
        if (txt != undefined) {
            if (txt.length > 0) {
                if (txt.includes('rotate(90deg)')) {
                    $($(e.currentTarget).find('.fa-angle-right')).css('transform', 'rotate(0deg)');
                }
                else {
                    $($(e.currentTarget).find('.fa-angle-right')).css('transform', 'rotate(90deg)');
                }
            }
        }
        else {
            $($(e.currentTarget).find('.fa-angle-right')).css('transform', 'rotate(90deg)');
        }
        if ($($($(e.currentTarget)[0]).parent().children()[1])[0].innerHTML != "")
            $($($($(e.currentTarget)[0]).parent().children()[1]).children()).toggle(1000);

    })
    var PopupDataSource;
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableData?tablename=ConnectionConfig',
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            PopupDataSource = JSON.parse(val);
        },
        error: function (e) {
        }
    });

    $("#SubmitForm1").on("submit", function (e) {
        
        e.preventDefault();
        var Values = {
        }
        var Columns = $('[name="Columns"]');
        var len = $('[name="Columns"]').length;
        for (var i = 1; i < len; i++) {
            var ID = $('[name="Columns"]')[i].id;
            var columnName = ID.split("|");
            if (columnName[0].toUpperCase() == "CREATEBY") {
                Values[columnName[0]] = window.btoa(JSON.parse(localStorage.Result).User_ID);
            }
            // else if (columnName[0].toUpperCase() == "CREATEDATETIME") {
            //     Values[columnName[0]] = window.btoa(GetNow());
            //}
            else if (columnName[0].toUpperCase() == "STATUS")
                Values[columnName[0]] = window.btoa(true);
            //else if (columnName[0] == "Connection_Status")
            //    Values[columnName[0]] = window.btoa(false);
            else {
                if ($('[name="Columns"]')[i].type == "checkbox") {
                    Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].checked);
                }
                else {
                    Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].value);
                }
            }
        }

        var values = JSON.stringify(Values);
        $.ajax({
            type: "POST",
           // url: GlobalURL + '/api/CrudService/createTableRow?tablename=ConnectionConfig',
            url: GlobalURL + '/api/CrudService/createTableRow?tablename=ConnectionConfig',
            data: JSON.stringify(values),
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (val) {
                var Columns = $('[name="Columns"]');
                for (var i = 1; i < len; i++) {
                    if ($('[name="Columns"]')[i].type == "checkbox") {
                        $('[name="Columns"]')[i].checked = false;
                    }
                    else {
                        $('[name="Columns"]')[i].value = "";
                    }
                }
                $("#ConnectionAddModal").modal('hide');
                if (val == "true") {

                  //  $("#ConnectionAddModal").modal('hide');
                    // $('#SubmitForm')[0].reset();
                    swal("", "Connection Added Successfully", "success", { closeOnClickOutside: false });
                }
                else if (val == "false") {

                    //$("#ConnectionAddModal").modal('hide');
                    // $('#SubmitForm')[0].reset();
                    swal("", "Connection name already exists", "error", { closeOnClickOutside: false });
                }
                ConnectionTree();
            },
            error: function (e) {

            }
        });
    });
    $("#UpdateForm").on("submit", function (e) {

        e.preventDefault();

        var IDName = $('[name="EditColumn"]')[0].id.split("|");
        var ID = {};
        ID[IDName[0]] = $('[name="EditColumn"]')[0].value;
        columns = $('[name="EditColumn"]');
        var Values = {
            "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
            "UpdateDatetime": window.btoa(datetime)
        }
        for (var i = 1; i < columns.length; i++) {
            var z = $('[name="EditColumn"]')[i].id.split("|");
            if ($('[name="EditColumn"]')[i].type == "checkbox") {
                Values[z[0]] = window.btoa($('[name="EditColumn"]')[i].checked);
            }
            else {

                Values[z[0]] = window.btoa($('[name="EditColumn"]')[i].value);
            }
        }
        var values = JSON.stringify(Values);
        var Id = JSON.stringify(ID);
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/UpdateTableRow?tablename=ConnectionConfig&ID=' + Id,
            data: JSON.stringify(values),

            //data: Htmlname ,
            contentType: 'application/json; charset=utf-8',
            //dataType: 'json', 
            success: function (val) {

                columns = $('[name="EditColumn"]');
                for (var i = 1; i < columns.length; i++) {
                    if ($('[name="EditColumn"]')[i].type == "checkbox") {
                        $('[name="EditColumn"]')[i].checked = false;
                    }
                    else {
                        $('[name="EditColumn"]')[i].value = "";
                    }
                }
                //location.reload();
                $("#ConnectionEditModal").modal('hide');
                connectionexplorerclick();
                if (val == "true") {

                    swal("", " Updated Successfully", "success", { closeOnClickOutside: false });
                }
            },
            error: function (e) {

            }
        });


    });

    setInterval(function () {
        if (localStorage.isloggedin == 'N') {
            location.href = "../index.html";
        }
    }, 1000 * 1);

    if (sessionStorage.getItem("iSessionState") != 'Y' || sessionStorage.getItem("iSessionState") == null) {
        location.href = "../index.html";
    }
    $(".Rbtbtn").on("click", function () {
        $("#PropertiesBody").html("");
    });

    $('#add').on('click', function (e) {
        e.preventDefault();
        //var instanceRandom = Math.random().toString(36).slice(2)
        //.html($('<input type="textbox" id="instancekey"' + instanceRandom + '/>').addClass('someclass'))
        //.append($('<input type="textbox" id="instanceValue"' + instanceRandom + '/>').addClass('someclass'))
        $('<div/>').addClass('form-group col-sm-12')
            .html($('<input type="textbox" id="i_key" name="i_key[]" value=""/>').addClass('resizedTextbox'))
            .append($('<input type="textbox" id="i_value" name="i_value[]" value=""/>').addClass('resizedTextbox'))
            .append($('<button/>').addClass('remove btn btn-primary btn-flat').text('Remove'))
            .insertBefore(this);
    });
    $(document).on('click', 'button.remove', function (e) {
        e.preventDefault();
        $(this).closest('div.new-text-div').remove();
    });
})
$(document).on('click', '.add_prj', function () {
    $("#Process")[0].checked = true;
    $("#ProcessDiv").children().prop("disabled", false);
    $("#RobotName").val('');
    $("#left-accordin-Panel").show();
    $('#addModal').modal('show');
});

//$(document).on('click', '.checker', function (i,e) {
//    if ($(this).parent().prev().text().trim().toLowerCase() == "isfolderupload")
//    {
//        //$(this).parent().parent().parent().find('input[webkitdirectory=true]').each(function () { $(this) });
//    }
//});

function loadProjectProcess() {
    var str1 = window.location.href;
    var urldata = str1.substring(str1.indexOf("?") + 1, str1.length);
    if (urldata != "") {
        var jsndata = {};
        $("#ProjectTree").html(' ');
        var Rdata = {};
        Rdata.Type = "AllRobotsByPrjId";
        Rdata.Project_Id = urldata;
        var dataString = JSON.stringify(Rdata);
        $.ajax({
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            type: 'GET',

            url: GlobalURL + '/api/DesignStudio/Get?input=' + dataString,
            ContentType: 'application/json; charset=utf-8',
            DataType: "json",
            async: false,
            success: function (data2) {
                if (data2 != "false") {
                    resData = JSON.parse(data2);
                    jsndata["ProjectName"] = resData[0].Project_Name;
                    jsndata["ProjectID"] = resData[0].Project_Id;
                    jsndata["RobotName"] = resData[0].Name;
                    jsndata["RobotID"] = resData[0].Id;
                    jsndata["RobotType"] = resData[0].RobotType;
                    jsndata["Stat"] = 'Edit';
                    RobotID = jsndata["RobotID"];
                    RobotType = jsndata["RobotType"];
                    $("#ProjectTree").append("<li  class=\"Prjbtn\" id=\"" + jsndata["ProjectID"] + "\"><a href='javascript:;' >" + jsndata["ProjectName"] + "</a><button class=\"button add_prj fa fa-plus\"><span class='tooltiptext'>Add Robot</span></button><ul></ul></li>");
                    $("#ProjectBreadCrumb")[0].innerHTML = jsndata["ProjectName"];
                    $(resData).each(function (i, val) {
                        if (val.Id != null)
                            //$("#ProjectTree > li > ul").append("<li class=\"" + val.RobotType + "\"><a href='javascript:;' class=\"Rbtbtn\" id=\"" + val.Id + "\" title=\"" + val.Name + "\" parent-id=\"" + val.Project_Id + "\"><span id='RbtName'>" + val.Name + "</span></a></li>");
                            $("#ProjectTree > li > ul").append("<li class=\"" + val.RobotType + "\"><a href='javascript:;' class=\"Rbtbtn\" id=\"" + val.Id + "\" parent-id=\"" + val.Project_Id + "\"><span id='RbtName' title=\"" + val.Name + "\">" + val.Name + "</span><img src='../Views/dist/img/warning.png' class='rbtwarning'title='please configure properties before execution' width='15px' height='15px'></a></li>"); //removed titile to anchor tag
                    });
                    $(".file-tree").filetree();
                    $(".Prjbtn a").first().click();
                    if (resData.length >= 1) {
                        $('#right-panel').show();
                        $('#properties-panel').hide();
                        $('#versions-panel').hide();
                        $('#projectexplorer-panel').show();
                        $('.right-panel-header-value').html('Project Explorer');
                        $('#middel-panel').addClass('limitWidth');
                        if ($('#ProjectTree').find('ul').find('li')[0].classList[0] == "Robot") {
                            versions($('#ProjectTree').find('ul').find('li').find('a')[0].id);
                        }

                        actionclick();
                        $('#actionsopen').hide();
                        $('.side-text').removeClass('bg-act-left');


                    } else {
                        if ($('#actionsopen').hasClass('actionstabopen')) {
                            $('.left-panel-header-value').html('');
                            $('#actionsopen').removeClass('actionstabopen');
                            $('#actionsopen').hide();
                        }
                        else {
                            $('#actionsopen').addClass('actionstabopen');
                            $('.left-panel-header-value').html('Toolset');
                            $('#actionsopen').show();
                            $('#actionsDiv').show();
                            $('#ActionProperties').show();
                            var loadvalue = $($('.Prjbtn')[0]).find('li')[0].classList[0];

                            loadPalette(loadvalue);
                        }
                    }
                }
            },
            error: function (e) {

            }
        });
    }
    else {
        var data3 = {
            "CreateBy": JSON.parse(localStorage.Result).User_ID
        };
        data3.Project_Id = jsndata.ProjectID;
        data3.Project_Name = jsndata.ProjectName;
        data3['Id'] = jsndata.RobotID;
        data3['Name'] = jsndata.RobotName;
        RobotID = jsndata.RobotID;
        RobotType = jsndata.RobotType;
        $("#ProjectTree").append("<li id=\"" + jsndata.ProjectID + "\" class=\"Prjbtn\" ><a href='javascript:;' >" + jsndata.ProjectName + "</a><button title=\"Add\"class=\"button add_prj fa fa-plus-square\"></button><ul><li  class=\"" + jsndata.RobotType + "\"><a href='javascript:;' class=\"Rbtbtn\"  title=\"" + jsndata.RobotType + "\" id=\"" + jsndata.RobotID + "\" parent-id=\"" + jsndata.ProjectID + "\">" + jsndata.RobotName + "</a></li></ul></li>");
        $(".file-tree").filetree();
        $(".Prjbtn a").first().click();
    }
}

var data = {

};
$(document).ready(function () {
    $($("#calendar")[0].children[1].children[0].children[0].children[1]).click(function () {

        scheduleEvent();
    });
    //$($("#calendar")[0].children[1].children[0].children[0].children[1]).on("click", function () {
     
    //});
    //$("table tr").click(function () {
      

    //});
    $("#CreateForm").on("submit", function (e) {
        e.preventDefault();
        data["Type"] = "Robot";
        if ($("#RobotName").val() != "")
            data["Name"] = $("#RobotName").val();
        else {
            data["Name"] = $("#ProjectName").val() + "_Robot";
        }
        data["projectType"] = $("#ProjectType").val();
        data["Parent"] = $("#ProjectTree").find('li')[0].id;
        data["CreateBy"] = JSON.parse(localStorage.Result).User_ID;
        data["UpdateBy"] = JSON.parse(localStorage.Result).User_ID;
        document.getElementById("Robot").click();
        if ($("input[name='projectType']:checked").val() == "Process")
            data["RobotType"] = $("input[name='projectProcessType']:checked").val();
        else
            data["RobotType"] = $("input[name='projectType']:checked").val();
        var dataString = JSON.stringify(data);

        $.ajax({
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            type: 'POST',
            url: GlobalURL + '/api/DesignStudio/Create?input=' + dataString,
            ContentType: 'application/json; charset=utf-8',
            DataType: "json",
            async: false,
            success: function (data2) {
                if (data2 != "false") {
                    $("#savedModel")[0].innerHTML += "\n>'" + data["Name"] + "' Robot Created"
                    swal({
                        title: "",
                        text: "Robot Created Successfully",
                        icon: "success",
                        // timer: 2000,
                        button: true,
                        closeOnClickOutside: false,
                    });
                    $("#footerOutput").focus();
                    loadProjectProcess();
                    RobotID = data2;
                    var emptygojsData = {
                        "class": {},
                        "nodeDataArray": [],
                        "linkDataArray": []
                    }

                    myDiagram.model = go.Model.fromJson(emptygojsData)
                    var rtype;
                    if ($("input[name='projectType']:checked").val() == "Process")
                        rtype = $("input[name='projectProcessType']:checked").val();
                    else
                        rtype = $("input[name='projectType']:checked").val();
                    loadProcess(RobotID, rtype);

                    $(".close").show();
                    $(".cancel").show();
                    $('#addModal').modal('hide');
                    $(".Rbtbtn#" + data2).click();
                    //location.reload();
                }
                else {
                    $("#savedModel")[0].innerHTML += "\n>'" + data["Name"] + "' Robot Not Created";
                    swal("", "Robot already exists, please try again!", "error"), (window.setTimeout(2500));
                }
            },
            error: function (e) {

            }
        });

    });
});
function ProjectTypeSelect(e) {
    var ele = $("#ProcessDiv").children();
    if ($("input[name='projectType']:checked").val() != "Process") {
        $("#" + $("input[name='projectProcessType']:checked")[0].id).prop("checked", false);
        for (var i in ele) {
            ele[i].disabled = true;
        }
    }
    else {
        $("#FlowChart").prop("checked", true);
        for (var i in ele) {
            ele[i].disabled = false;
        }
    }
}
//Deleting Process/Robot

function deleteRobot(RobotID, RobotName) {

    var data = {};
    data["Type"] = "Robot";
    data["Name"] = RobotID;
    data["Parent"] = $("#ProjectTree").find('li')[0].id;
    var dataString = JSON.stringify(data);
    $.ajax({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        type: 'GET',
        url: GlobalURL + '/api/GenbaseStudio/Delete?input=' + dataString,
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (data2) {

            if (data2 == "Success") {
                swal("", "Robot Deleted Successfully", "success", { closeOnClickOutside: false });
            }
            else {
                swal("", "Sorry cannot perform such operation", "error", { closeOnClickOutside: false });
                //$('#deleteModal').modal('hide');
            }
            loadProjectProcess();
            $('.Rbtbtn')[0].click();
        },
        error: function (e) {

        }
    });
}
//Execute
var JSONSteps;
var DynamicVariables = {};
var JsonInProgress = {};
var StepID = "";
var uploadSeverFilename = [];
var uploadResultPath = "";
function uploadFiles_Service() {

    for (u = 0; u < uploadList.length; u++) {
        var upload = {};

        upload["fPath"] = uploadList[u].name;
        upload["data1"] = JSON.stringify(uploadList[u].name.replace(",", " ", 1) + "," + uploadList[u].result);
        upload["isFolder"] = JSON.stringify(isfolder);
        upload["previousFolderPath"] = uploadResultPath;
        var Uploads = JSON.stringify(upload);

        //console.log(Uploads);

        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/GenbaseStudio/Upload?fPath=' + upload["fPath"] + '&isFolder=' + JSON.stringify(isfolder) + '&previousFolderPath=' + uploadResultPath,
            //  url: 'http://localhost:61048/api/GenbaseStudio/Upload?fPath=AsA&isFolder=true&previousFolderPath=asa',
            // data: JSON.stringify(uploadList[u].result.replace("data:","")),
            data: JSON.stringify(JSON.stringify(uploadList[u].name.replace(",", " ", 1) + "," + uploadList[u].result)),
            async: false,
            contentType: 'application/json; charset=utf-8',
            success: function (uploadresult) {

                uploadDatatTable = uploadresult;
                if (isfolder)
                    uploadResultPath = uploadresult;
                else
                    uploadSeverFilename.push(uploadresult)
            },
            error: function (e) {

            }
        });

    }
    if (uploadResultPath != "")
        uploadSeverFilename.push(uploadResultPath);

}
var job = {};
function create_ProcessManager() {
    var ProcessManager_Input = {};
    ProcessManager_Input["Type"] = "ProcessManager";
    ProcessManager_Input["username"] = UserDetails.UserName;
    ProcessManager_Input["Robot_ID"] = $($(".Prjbtn ul").find('.active')[0])[0].id;
    ProcessManager_Input["userid"] = UserDetails.User_ID;
    $.ajax({
        type: 'POST',
        url: GlobalURL + '/api/GenbaseStudio/Create?input=' + JSON.stringify(ProcessManager_Input),
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (jobjson) {
            if (jobjson != "false") {
                job = JSON.parse(jobjson);
                $("#savedModel")[0].innerHTML += "\nJob is Created Please Check <a href='/Views/Layout.html?generatedHTML/Process_Manager_1_2_3_4?" + job[0]["max"] + "'>here</a>  for further Status";
            }
            return job;
        },
        error: function (e) {

        }
    });
}
var debugICON;
var DebuggerDvariables = DVariables;
function debug(e) {
    debugICON = $(e);
    $("#ExecuteButton")[0].disabled = true;
    $(e).addClass('activeClass');
    $(".left-icons .ezstudio-icon").addClass('disable-icons')
    $(".left-icons .ezstudio-icon").attr({ "title": "step-forward" });
    if ($("#spanToggle").hasClass("fa-window-maximize")) {
        $("#spanToggle").removeClass("fa-window-maximize");
        $("#spanToggle").addClass("fa-window-minimize");
        $("#logfooter").animate({ height: "25%" });
    }
    uploadSeverFilename = [];
    uploadFiles_Service();
    //create_ProcessManager();
    for (i in DVariables) {
        DVariables[i].vlstatus = true;
    }
    $("#content").scrollTop($("#content")[0].scrollHeight);
    var data = {};
    if (DVariables == null)
        data["dynamicvariables"] = [];
    else
        data["dynamicvariables"] = DVariables;
    data["Type"] = "Execution";
    data["uploadFiles"] = uploadSeverFilename;
    data["ID"] = $($(".Prjbtn ul").find('.active')[0])[0].id;
    //data["systemId_pm"] = job[0]["max"];
    data["Project_Id"] = $("#ProjectTree").find('li')[0].id;
    var dataString = JSON.stringify(data);

    if (DebugIndex == -1) {
        datasource = GlobalURL + '/api/GenbaseStudio/debug_Order?input=' + dataString;
        $.ajax({
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            type: 'GET',
            url: datasource,
            ContentType: 'application/json; charset=utf-8',
            DataType: "json",
            async: false,
            success: function (data2) {
                OrderSteps = JSON.parse(data2.debug_OrderResult);
                DebugStepExecution();

            },
            error: function (e) {

            }
        });
    } else {
        DebugStepExecution();
    }
}
function DebugStepExecution() {
    DebugIndex = DebugIndex + 1;
    DebugStepsCount = DebugStepsCount + 1;
    if (OrderSteps.length >= DebugStepsCount) {
        uploadSeverFilename = [];
        uploadFiles_Service();
        //create_ProcessManager();
        for (i in DVariables) {
            DVariables[i].vlstatus = true;
        }
        $("#content").scrollTop($("#content")[0].scrollHeight);
        var data = {};
        if (DVariables == null)
            data["dynamicvariables"] = [];
        else
            data["dynamicvariables"] = DVariables;
        data["Type"] = "Execution";
        data["uploadFiles"] = uploadSeverFilename;
        data["ID"] = $($(".Prjbtn ul").find('.active')[0])[0].id;
        data["systemId_pm"] = job[0]["max"];
        data["Project_Id"] = $("#ProjectTree").find('li')[0].id;
        data["Step_Id"] = OrderSteps[DebugIndex].Id;
        data["Namer"] = UserDetails.UserName;
        data["Name"] = $(".Rbtbtn.active")[0].innerText;
        var dataString = JSON.stringify(data);
        var formdata = {}
        formdata["input"] = btoa(dataString);
        $.ajax({
            type: 'POST',
            url: GlobalURL + '/api/GenbaseStudio/StepExecution',
            data: JSON.stringify(formdata),
            contentType: 'application/json; charset=utf-8',
            async: false,
            success: function (data2) {
                var DebuggerJSONLog = JSON.parse(data2.StepExecutionResult);
                var DebuggerResultStringS = JSON.parse(DebuggerJSONLog.ResultString);
                DVariables = JSON.parse(DebuggerJSONLog.ResultString).dynamicVariables;
                if (DebuggerResultStringS.elog != null && DebuggerResultStringS.elog.length > 0) {
                    DebuggerElog(DebuggerResultStringS);
                    $("#footerDebugger").click();
                    swal({
                        title: "",
                        text: "Step Executed Successfully",
                        icon: "success",
                        timer: 2000,
                        button: false,
                    });
                }
                var rename = myDiagram.Ei.Ea;
                while (rename.Xa != null) {

                    rename = rename.Xa

                    if (rename.key.StepID == OrderSteps[DebugIndex].Id) {
                        myDiagram.model.setDataProperty(rename.key, 'color', "Grey");
                    }
                    else {
                        myDiagram.model.setDataProperty(rename.key, 'color', "transparent");
                    }
                }
            },
            error: function (e) {

            }
        });
        if (OrderSteps.length == DebugStepsCount) {
            $("#ExecuteButton")[0].disabled = false;
            $(debugICON).removeClass("activeClass");
            $(".left-icons .ezstudio-icon").attr({ "title": "debug" });
            $(".left-icons .ezstudio-icon").removeClass('disable-icons')
        }
    }
    else {
        $("#ExecuteButton")[0].disabled = false;
        $(debugICON).removeClass("activeClass");
        $(".left-icons .ezstudio-icon").attr({ "title": "debug" });
        $(".left-icons .ezstudio-icon").removeClass('disable-icons')

        var rename = myDiagram.Ei.Ea;
        while (rename.Xa != null) {

            rename = rename.Xa
            myDiagram.model.setDataProperty(rename.key, 'color', "transparent");
        }
        //swal("", "There is no steps to debug", "error");
        swal("Do you want to restart the debugger?", {
            buttons: ["Cancel", "Ok"],
            className: "RestartDebugger",
        });
        $(".RestartDebugger .swal-footer .swal-button-container .swal-button--confirm").on("click", function () {
            DebugIndex = -1;
            DebugStepsCount = 0;
            DVariables = DebuggerDvariables;
            $("#DebuggerButton").click();

        });
    }
}

function openS(event, type) {

    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent2");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks2");
    for (i = 0; i < tablinks.length; i++) {
        $(tablinks[i]).removeClass("active")
    }
    document.getElementById(type).style.display = "block";
    $(event.target).addClass("active");

}
function executeDownload(item) {
    //console.log(item);
    var dpath = item.FileName.split("\\");
    //console.log(dpath);
    //window.open("/" + dpath[dpath.length - 2] + "/" + dpath[dpath.length - 1]);
    var a = document.createElement('a');
    a.href = GlobalURL + '/api/CrudService/Download?fileName=' + dpath[dpath.length - 1] + '&foldername=' + dpath[dpath.length - 2];
    a.click();
}

var DebugIndex = -1;
var OrderSteps;
var DebugStepsCount = 0;
function executeProcess() {

    $('#myDropdown').removeClass("show");
    $('#footerOutput').addClass('active');
    $("#footerOutput").click();
    if (LocalRobotProperties["Client_Dependency"] != true) {
        if ($("#spanToggle").hasClass("fa-window-maximize")) {
            $("#spanToggle").removeClass("fa-window-maximize");
            $("#spanToggle").addClass("fa-window-minimize");
            $("#logfooter").animate({ height: "25%" });
        }
        $("#savedModel")[0].innerHTML += ">'" + $("#ProcessBreadCrumb")[0].innerHTML + "' Execution Started"
        uploadSeverFilename = [];
        uploadFiles_Service();
        create_ProcessManager();
        for (i in DVariables) {
            DVariables[i].vlstatus = true;
        }
        $("#content").scrollTop($("#content")[0].scrollHeight);
        var data = {};
        if (DVariables == null)
            data["dynamicvariables"] = [];
        else
            data["dynamicvariables"] = DVariables;
        data["Type"] = "Execution";
        data["uploadFiles"] = uploadSeverFilename;
        data["ID"] = $($(".Prjbtn ul").find('.active')[0])[0].id;
        data["systemId_pm"] = job[0]["max"];
        data["Project_Id"] = $("#ProjectTree").find('li')[0].id;
        data["Namer"] = UserDetails.UserName;
        data["Name"] = $(".Rbtbtn.active")[0].innerText;
        data["CreateBy"] = (JSON.parse(localStorage.Result).User_ID);
        data["UpdateBy"] = (JSON.parse(localStorage.Result).User_ID);
        var dataString = JSON.stringify(data);
        //var formdata = {}
        //formdata["input"] = btoa(dataString);
        uploadResultPath = "";
        setTimeout(function () { $("#savedModel")[0].innerHTML += "\n>......................................."; }, 1000);
        $.ajax({
            type: 'POST',
            url: GlobalURL + '/api/GenbaseStudio/set',
            data: dataString,
            contentType: 'application/json; charset=utf-8',
            success: function (data2) {

                var JSONLog = JSON.parse(data2);
                var ResultStringS = JSON.parse(JSONLog.ResultString);
                if (ResultStringS.Downloads != null && ResultStringS.Downloads.length > 0) {
                    ResultStringS.Downloads.forEach(executeDownload);
                }
                if (ResultStringS.runtimerequest != null && ResultStringS.runtimerequest != undefined)
                    DVariables = ResultStringS.runtimerequest.dvariables;
                if (ResultStringS.SuccessState != "" && ResultStringS.SuccessState != null) {

                    if (ResultStringS.SuccessState.toLowerCase() == "success") {
                        var error;
                        if ($(".Rbtbtn.active").parent().hasClass("ValueStreamMap") == true) {
                            swal("", "ValueStreamMap Executed Successfully", "success", { closeOnClickOutside: false });
                        }
                        else if ($(".Rbtbtn.active").parent().hasClass("Workflow_Robot") == true) {
                            swal("", "Workflow Executed Successfully", "success", { closeOnClickOutside: false });
                        }
                        else if ($(".Rbtbtn.active").parent().hasClass("FlowChart") == true) {
                            swal("", "Flow Chart Executed Successfully", "success", { closeOnClickOutside: false });
                        }
                        else
                            swal({
                                title: "",
                                text: "Robot Executed Successfully",
                                icon: "success",
                                timer: 2000,
                                button: true,
                                closeOnClickOutside: false,
                            });
                        $("#footerOutput").focus();
                        if (ResultStringS.elog != null && ResultStringS.elog.length > 0) {
                            elog(ResultStringS);
                        }
                        else if (ResultStringS.elog == null || ResultStringS.elog == [] || ResultStringS.elog == "") {
                            swal("", "Execution Stopped. Please check Steps and its properties and try again", "error", 10000);
                        }
                        if ($(".Rbtbtn.active").parent().hasClass("ValueStreamMap") == true) {
                            $("#savedModel")[0].innerHTML += "\n>ValueStreamMap '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Executed Successfully";
                        }
                        else if ($(".Rbtbtn.active").parent().hasClass("Workflow_Robot") == true) {
                            $("#savedModel")[0].innerHTML += "\n>Workflow '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Executed Successfully";
                        }
                        else if ($(".Rbtbtn.active").parent().hasClass("FlowChart") == true) {
                            $("#savedModel")[0].innerHTML += "\n>Flow Chart '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Executed Successfully";
                        }
                        else
                            $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Executed Successfully";
                    }
                    else if (ResultStringS.SuccessState.toLowerCase() == "no steps") {
                        $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "'Cannot execute the robot without steps or without save. Please try again.";
                        swal("", "Cannot execute the robot without steps or without save. Please try again", "error");
                    }
                    else if (ResultStringS.SuccessState.toLowerCase() == "failure") {
                        $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Execution Stopped.Please check Steps and its properties and try again.";
                        swal("", "Execution Stopped. Please check Steps and its properties and try again", "error");
                    }
                    else if (ResultStringS.SuccessState.toLowerCase().trim() == "inprogress") {
                        StepID = parseInt(ResultStringS["runtimerequest"]["nextStepID"]);
                        if (ResultStringS["runtimerequest"]["step"].Name == "InputBox") {
                            display_Input(ResultStringS);
                        }
                        else if (ResultStringS["runtimerequest"]["step"].Name == "OutputBox") {
                            display_output(ResultStringS);
                        }
                        else if ((ResultStringS["runtimerequest"]["step"].Name == "Input")) {
                            display_inboxInput(ResultStringS);
                        }
                    }
                }
                else {

                    $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Execution Stopped.Please check Steps and its properties and try again.";
                    document.getElementsByClassName('Rbtbtn active')[0].children[1].style.display = "inline-block";
                    swal("", "Execution Stopped. Please check Steps properties and try again", "error");
                }
            },
            error: function (e) {

            }
        });
    }
    else {
        if (LocalRobotProperties["Default_Execution"] != null && LocalRobotProperties["Default_Execution"] != "" && LocalRobotProperties["Default_Execution"] != "Select System" && LocalRobotProperties["Default_Execution"] != "On Server" && LocalRobotProperties["Default_Execution"] != "") {
            var Values = {};
            Values["RobotId"] = $($(".Prjbtn ul").find('.active')[0])[0].id;
            Values["SystemId"] = LocalRobotProperties["Default_Execution"];
            Values["interval"] = 1;
            Values["Status"] = "true";
            $.ajax({
                type: "GET",
                url: GlobalURL + '/api/CrudService/robotSchedule?ScheduleDetails=' + JSON.stringify(Values),
                contentType: 'application/json; charset=utf-8',
                success: function (val) {
                    $('#ScheduleModal').modal('hide');
                    if (val == "true" || val == "Success") {
                        swal("", "Scheduled Successfully", "success", { closeOnClickOutside: false });
                    }
                    else {
                        swal("", "Please Schedule With All Details", "error", { closeOnClickOutside: false });
                    }
                },
                error: function (e) {

                }
            });
        }
        else {
            swal("", "Please Select Default Execution System in Robot Properties to Continue ", "error")
        }
    }
}
function display_inboxInput(ResultStringS) {
    elog(ResultStringS);
    $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Execution is initially paused and waiting for the Input";

}
function display_output(ResultStringS) {
    for (i in ResultStringS["runtimerequest"]["step"]["StepProperties"]) {
        if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepProperty == "Title") {
            $("#dynamicModal .modal-title").html(ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepPropertyValue)
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepProperty.trim() == "MessageFormat") {
            $("#OutputLabel").html(ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepPropertyValue)
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepProperty.trim() == "Buttons") {
            if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepPropertyValue.trim() == "OK")
                $("#dynamicModal .modal-footer").html('<button type="button" class="btn btn-primary btn-flat"  onclick="InputDone()"><span class="fa fa-check"></span> Ok</button>');
            else
                $("#dynamicModal .modal-footer").html('<button type="button" class="btn btn-primary btn-flat"  onclick="InputDone()"><span class="fa fa-check"></span> Ok</button><button type="button" class="btn btn-secondary" data-dismiss="modal"><span class="fas fa-times"></span>Cancel</button>');
        }
    }
    var OutputValuResult;
    OutputValuResult = ResultStringS["runtimerequest"]["dvariables"].find(ind => ind.vlname == ResultStringS["runtimerequest"]["thisVariableName"]);
    var columns = [];
    $("#OutputTable").show();
    if (OutputValuResult != undefined && OutputValuResult.vlvalue != undefined && OutputValuResult.vlvalue != "" && OutputValuResult.vlvalue != "[]") {
        if (OutputValuResult.vltype != 'string') {
            $('#dynamicModal .modal-dialog').addClass('output-dialogue');
            $("#OutputTable").show();
            var otpResult = JSON.parse(OutputValuResult.vlvalue)
            for (var key in Object.keys(otpResult[0])) {
                var key_column = {};
                key_column["title"] = Object.keys(otpResult[0])[key];
                key_column["data"] = Object.keys(otpResult[0])[key];
                columns.push(key_column);
            }
            $("#OutputValue").DataTable({
                destroy: true,
                "scrollY": '50vh',
                "scrollX": true,
                "data": otpResult,
                "iDisplayLength": 10,
                //"order": [[0, 'desc']],
                "bSort": false,
                "autoWidth": false,
                "columns": columns,
                //"columnDefs": [{
                //    "targets": '_all',
                //    "defaultContent": "",
                //}],
                "columnDefs": [
                    { "width": "50%", "targets": '_all', "defaultContent": "" },

                ],
            });

            $('#dynamicModal .modal-dialog.output-dialogue').css('width', '75%');
            $('#dynamicModal .modal-dialog.output-dialogue .dataTables_scrollHeadInner .control-label.dataTable.no-footer').css('width', '100%');
        }
        else {

            $("#OutputresultPre").html(OutputValuResult.vlvalue);

            $("#OutputTable").hide();
            $("#OutputValue").html('');

        }
    }
    else {

        $('#dynamicModal .modal-dialog').addClass('output-dialogue');
        $("#OutputresultPre").html("No Output!");
        $('#dynamicModal .modal-dialog').removeClass('output-dialogue');

    }
    elog(ResultStringS);
    $("#InputDiv").hide();
    $("#OutputDiv").show();

    $("#dynamicModal textarea").val("");

    $("#dynamicModal").modal('show');
}

function init_inputenter() {
    $('.txtinput').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            InputDone();
        }
    });
}

function display_Input(ResultStringS) {
    $("#Input_WidgetType")[0].innerHTML = "";
    var wt = ResultStringS["runtimerequest"]["step"]["StepProperties"].findIndex(wtsp => wtsp.StepProperty == "WidgetType")

    if (wt != -1) {
        if (ResultStringS["runtimerequest"]["step"]["StepProperties"][wt].StepPropertyValue.trim() == "Dropdown") {
            $("#Input_WidgetType")[0].innerHTML += "<select class='form-control' id='InputValue' variable=''></select>";
            var textSP = "", valueSP = "";
            var optionsTextResult = "";
            var optionsValueResult = "";
            var itemValueName = "", itemValueNumber = "", itemTextName = "", itemTextNumber = "";
            try {
                if ((textSP = ResultStringS["runtimerequest"]["step"]["StepProperties"].find(s => s.StepProperty == "Text")) != undefined && textSP["StepPropertyValue"] != null) {
                    if (textSP["StepPropertyValue"].indexOf("[") > -1 && textSP["StepPropertyValue"].indexOf("]") > -1) {
                        itemTextName = textSP["StepPropertyValue"].split('[')[0].trim();
                        itemTextNumber = textSP["StepPropertyValue"].split('[')[1].split(']')[0].trim().replace(/[\"\']/g, "");
                        optionsTextResult = JSON.parse(ResultStringS.runtimerequest.dvariables.find(optionTextDV => optionTextDV.vlname == itemTextName).vlvalue)
                    }
                    else {
                        optionsTextResult = JSON.parse(ResultStringS.runtimerequest.dvariables.find(optionTextDV => optionTextDV.vlname == textSP["StepPropertyValue"]))
                    }
                }
                if ((valueSP = ResultStringS["runtimerequest"]["step"]["StepProperties"].find(s => s.StepProperty == "Value")) != undefined && textSP["StepPropertyValue"] != null) {
                    if (valueSP["StepPropertyValue"].indexOf("[") > -1 && valueSP["StepPropertyValue"].indexOf("]") > -1) {
                        itemValueName = valueSP["StepPropertyValue"].split('[')[0].trim();
                        itemValueNumber = valueSP["StepPropertyValue"].split('[')[1].split(']')[0].trim().replace(/[\"\']/g, "");
                        optionsValueResult = JSON.parse(ResultStringS.runtimerequest.dvariables.find(optionTextDV => optionTextDV.vlname == itemValueName).vlvalue)
                    }
                    else {
                        optionsValueResult = JSON.parse(ResultStringS.runtimerequest.dvariables.find(optionValueDV => optionValueDV.vlname == valueSP["StepPropertyValue"]))
                    }
                }
                if (optionsTextResult != "" && optionsValueResult != "" && optionsTextResult.length == optionsValueResult.length) {
                    optionsTextResult.forEach(function iterOptText(TextResult, resultind) {
                        if (resultind != undefined) {
                            //console.log(TextResult);
                            if (itemValueNumber != "")
                                $("#InputValue")[0].innerHTML += "<option>" + TextResult[itemTextNumber] + "</option>"
                            else
                                $("#InputValue")[0].innerHTML += "<option>" + TextResult[resultind] + "</option>"
                        }
                    });
                    optionsValueResult.forEach(function iterOptValue(ValueResult, resultind) {
                        if (resultind != undefined) {
                            //console.log(ValueResult);
                            if (itemValueNumber != "")
                                $("#InputValue option")[resultind].val(ValueResult[itemValueNumber])
                            else
                                $("#InputValue option")[resultind].val(ValueResult[resultind])
                        }
                    });
                }

            }
            catch (ex) {

            }
            //  $("#InputValue")[0].
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][wt].StepPropertyValue.trim() == "Textbox") {
            $("#Input_WidgetType")[0].innerHTML += "<input type=text class='form-control txtinput' id='InputValue' variable=''/>";
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][wt].StepPropertyValue.trim() == "Checkbox") {
            $("#Input_WidgetType")[0].innerHTML += "<input type=checkbox class='txtinput' id='InputValue' variable=''/>";
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][wt].StepPropertyValue.trim() == "Textarea") {
            $("#Input_WidgetType")[0].innerHTML += "<textarea class='form-control txtinput' id='InputValue' variable=''></textarea>";
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][wt].StepPropertyValue.trim() == "SelectOption") {

            InputDone();
        }
        init_inputenter()

    }
    for (i in ResultStringS["runtimerequest"]["step"]["StepProperties"]) {
        if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepProperty == "Title") {
            $("#dynamicModal .modal-title").html(ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepPropertyValue)
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepProperty.trim() == "DisplayMessage") {
            $("#InputLabel").html(ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepPropertyValue)
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepProperty.trim() == "Variable") {
            $("#InputValue")[0].attributes.variable.nodeValue = ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepPropertyValue;
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepProperty.trim() == "Buttons") {
            if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepPropertyValue.trim() == "OK")
                $("#dynamicModal .modal-footer").html('<button type="button" class="btn btn-primary btn-flat" id="DynamicOk" onclick="InputDone()"><span class="fa fa-check"></span> Ok</button>');
            else
                $("#dynamicModal .modal-footer").html('<button type="button" class="btn btn-primary btn-flat" id="DynamicOk" onclick="InputDone()"><span class="fa fa-check"></span> Ok</button><button type="button" class="btn btn-secondary" data-dismiss="modal"><span class="fas fa-times"></span>Cancel</button>');
        }

    }
    $('#dynamicModal .modal-dialog').css('width', '');
    $("#InputDiv").show();
    $("#OutputDiv").hide();
    $("#OutputTable").hide();
    elog(ResultStringS);
    $("#dynamicModal textarea").val("");
    $('#dynamicModal .modal-dialog').removeClass('output-dialogue');
    $("#dynamicModal").modal('show');

}
function elog(ResultStringS) {


    if (ResultStringS.elog != null) {
        //if (ResultStringS.elog[0].Description.indexOf('Password') > -1) {
        

        //}
        for (i in ResultStringS.elog) {

            ResultStringS.elog[i]["Description"] = ResultStringS.elog[i]["Description"].replace(/Password=[a-zA-Z0-9$&+,:=?@#|'<>.^*()%!-]*;/g, "")
            if (ResultStringS.elog[i]["Description"].indexOf('====') > -1) {
                $("#savedModel")[0].innerHTML += "\n>'" + ResultStringS.elog[i]["StepName"] + "'\n" + ResultStringS.elog[i]["Description"];
            }
            else {
                $("#savedModel")[0].innerHTML += "\n>'" + ResultStringS.elog[i]["StepName"] + "' " + ResultStringS.elog[i]["Description"];
            }
        }
    }
}
function DebuggerElog(ResultStringS) {
    if (ResultStringS.elog != null) {
        for (i in ResultStringS.elog) {
            $("#DebuggerResultPre")[0].innerHTML += "\n>'" + ResultStringS.elog[i]["StepName"] + "' " + ResultStringS.elog[i]["Description"];
        }
    }
}
function datat(vlvalue, vlname) {
    //vlname = "Input" + vlname;
    var vardict = {};
    vardict[vlname] = vlvalue
    var myData = [];
    myData.push(vardict);

    return myData;
}
function InputDone() {
    $("#dynamicModal").modal('hide');
    $(".modal-backdrop.fade.in").remove();
    if ($("#OutputValue_wrapper").length > 0)
        $('#OutputValue').DataTable().destroy();
    $("#OutputValue").html('');
    $("#OutputresultPre").html('');
    var data = {};
    for (i in DVariables) {
        if ($("#InputValue")[0].attributes.variable.nodeValue == DVariables[i].vlname) {
            if ($("#InputValue")[0].type == "checkbox") {
                DVariables[i].vlvalue = JSON.stringify(datat($("#InputValue").prop('checked'), DVariables[i].vlname));
            }
            else {
                DVariables[i].vlvalue = JSON.stringify(datat($("#InputValue").val(), DVariables[i].vlname));
            }
            DVariables[i].vlstatus = true;
        }
        if ($("#OutputDiv").css("display") == "block") {
            DVariables[i].vlstatus = false;
        }
    }
    if (DVariables == null)
        data["dynamicvariables"] = [];
    else
        data["dynamicvariables"] = DVariables;
    data["Type"] = "ExecutionInProgress";
    data["ID"] = $($(".Prjbtn ul").find('.active')[0])[0].id;
    data["stepvalue"] = StepID;
    // data["systemId_pm"] = job[0]["max"];
    data["Project_Id"] = $("#ProjectTree").find('li')[0].id;
    data["Namer"] = UserDetails.UserName;
    data["Name"] = $(".Rbtbtn.active")[0].innerText;
    data["CreateBy"] = (JSON.parse(localStorage.Result).User_ID);
    data["UpdateBy"] = (JSON.parse(localStorage.Result).User_ID);
    var dataString = JSON.stringify(data);
    // var formdata = {}
    //formdata["input"] = btoa(dataString);
    $.ajax({
        type: 'POST',
        url: GlobalURL + '/api/GenbaseStudio/set',
        data: dataString,
        contentType: 'application/json; charset=utf-8',
        success: function (data2) {
            JSONLog = JSON.parse(data2);
            var ResultStringS = JSON.parse(JSONLog.ResultString);
            if (ResultStringS.Downloads != null && ResultStringS.Downloads.length > 0) {
                ResultStringS.Downloads.forEach(executeDownload);
            }
            if (ResultStringS.runtimerequest != null && ResultStringS.runtimerequest != undefined)
                DVariables = ResultStringS.runtimerequest.dvariables;
            if (JSON.parse(JSONLog.ResultString).SuccessState != "" && JSON.parse(JSONLog.ResultString).SuccessState != null) {
                var ResultStringS = JSON.parse(JSONLog.ResultString);
                if (ResultStringS.runtimerequest != null && ResultStringS.runtimerequest != undefined)
                    DVariables = ResultStringS.runtimerequest.dvariables;
                if (ResultStringS.SuccessState.toLowerCase() == "success") {
                    var error;
                    swal({
                        title: "",
                        text: "Robot Executed Successfully",
                        icon: "success",
                        timer: 2000,
                        button: false,
                    });
                    $("#footerOutput").focus();
                    elog(ResultStringS);
                    $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Executed Successfully";
                }
                else if (ResultStringS.SuccessState.toLowerCase() == "failure") {
                    $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Execution Stopped. Please check steps and its properties and try again.";
                    swal("", "Execution Stopped. Please check Steps and its properties and try again", "error");
                }
                else if (ResultStringS.SuccessState.toLowerCase().trim() == "inprogress") {
                    StepID = ResultStringS["runtimerequest"]["nextStepID"];
                    if (ResultStringS["runtimerequest"]["step"].Name == "InputBox") {
                        display_Input(ResultStringS)
                    }
                    else if (ResultStringS["runtimerequest"]["step"].Name == "OutputBox") {
                        display_output(ResultStringS);
                    }
                    else if ((ResultStringS["runtimerequest"]["step"].Name == "Input")) {
                        display_inboxInput(ResultStringS);
                    }
                }
            }
            else {
                $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Execution Stopped.Please check Steps and its properties and try again.";
                swal("", "Execution Stopped. Please check Steps and its properties and try again", "error");
            }
        },
        error: function (e) {

        }
    });
}
function CreateClose() {
    $("#Create1D").html("");
    $("#Create2D").html("");
    $("#CreateDynamic").html("");
    $("#CreateString").html("");
    $("#CreateInteger").html("");
    $("#CreateDateTime").html("");
    $("#CreateBoolean").html("");
    $("#CreateDataTable").html("");
    $("#VarbName").val('');
    $("#Sheetname").val('');
    $("#VarbDefaultValue").val('');
    $("#DefaultXValue").val('');
    $("#DefaultYValue").val('');
    $("#DefaultAValue").val('');
    $("#DefaultBValue").val('');
    $("#DefaultCValue").val('');
    $("#DefaultDValue").val('');
    $("#VarDataSelect").val('');
    $("#mySelectList").val('Select');
    $("#datetimeval").val('datetime-local');
    $("#VarbType").html(this.innerHTML);
    $("#VarbGlobalVariable")[0].checked = false;
    $("#Createvarb").modal('show');
    $('#boolSelect').hide();
    $('#Dateselect').hide();
    $('#Textselect').hide();
    $('#twoD').hide();
    $('#oneD').hide();
    $('#DataSelect').hide();
    $('#sheetname').hide();
    $('#VarbType').prop('selectedIndex', 0);
    $('#Validate_Roles').hide();
    $('#Validate_Roles1').hide();
    $("#Createvarb").modal('hide');
}
function Dynamicvariables() {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/GetVariables?rid=' + RobotID,
        contentType: 'application/json; charset=utf-8',
        async: false,
        DataType: "json",
        success: function (val) {
            if (val == "Success") {
            }
        },
        error: function (e) {
        }
    });
}
function loadbVariables(e) {

    Dynamicvariables();
    cur_sp = $(e).prev();
    $("#Create1D").html("");
    $("#Create2D").html("");
    $("#CreateDynamic").html("");
    $("#CreateString").html("");
    $("#CreateInteger").html("");
    $("#CreateDateTime").html("");
    $("#CreateBoolean").html("");
    $("#CreateDataTable").html("");
    $("#VarbName").val('');
    $("#Sheetname").val('');
    $("#VarbDefaultValue").val('');
    $("#DefaultXValue").val('');
    $("#DefaultYValue").val('');
    $("#DefaultAValue").val('');
    $("#DefaultBValue").val('');
    $("#DefaultCValue").val('');
    $("#DefaultDValue").val('');
    $("#VarDataSelect").val('');
    $("#mySelectList").val('Select');
    $("#datetimeval").val('datetime-local');
    $("#VarbType").html(this.innerHTML);
    $("#VarbGlobalVariable")[0].checked = false;
    $("#Createvarb").modal('show');
    $('#boolSelect').hide();
    $('#Dateselect').hide();
    $('#Textselect').hide();
    $('#twoD').hide();
    $('#oneD').hide();
    $('#DataSelect').hide();
    $('#sheetname').hide();
    $('#VarbType').prop('selectedIndex', 0);
    var i = 0; var q = 0; var e = 0;
    var c = 0; var d = 0; var v = 0;
    var k = 0; var b = 0;
    for (var j in DVariables) {
        try {
            if ((typeof (DVariables[j].vltype) != typeof (undefined) || DVariables[j].vltype != null || DVariables[j].vltype != "")) {
                if (DVariables[j].vltype == " Dynamic") {
                    $("#Create" + DVariables[j].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='CeditVariables(this)' class='Cvariables' vid='" + j + "'vlvalue='" + DVariables[j].vlvalue + "' vlstatus='" + DVariables[j].vlstatus + "'>" + DVariables[j].vlname + "</li>"
                    i++;
                    document.getElementById('dynamic').innerText = i;
                }
                if (DVariables[j].vltype == "String") {
                    $("#Create" + DVariables[j].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='CeditVariables(this)' class='Cvariables' vid='" + j + "'vlvalue='" + DVariables[j].vlvalue + "' vlstatus='" + DVariables[j].vlstatus + "'>" + DVariables[j].vlname + "</li>"
                    c++;
                    document.getElementById('string').innerText = c;
                }
                if (DVariables[j].vltype == "Integer") {
                    $("#Create" + DVariables[j].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='CeditVariables(this)' class='Cvariables' vid='" + j + "'vlvalue='" + DVariables[j].vlvalue + "' vlstatus='" + DVariables[j].vlstatus + "'>" + DVariables[j].vlname + "</li>"
                    k++;
                    document.getElementById('integer').innerText = k;
                }
                if (DVariables[j].vltype == "DateTime") {
                    $("#Create" + DVariables[j].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='CeditVariables(this)' class='Cvariables' vid='" + j + "'vlvalue='" + DVariables[j].vlvalue + "' vlstatus='" + DVariables[j].vlstatus + "'>" + DVariables[j].vlname + "</li>"
                    q++;
                    document.getElementById('datetime').innerText = q;
                }
                if (DVariables[j].vltype == "Boolean") {
                    $("#Create" + DVariables[j].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='CeditVariables(this)' class='Cvariables' vid='" + j + "'vlvalue='" + DVariables[j].vlvalue + "' vlstatus='" + DVariables[j].vlstatus + "'>" + DVariables[j].vlname + "</li>"
                    d++;
                    document.getElementById('bool').innerText = d;
                }
                if (DVariables[j].vltype == "1D") {
                    $("#Create" + DVariables[j].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='CeditVariables(this)' class='Cvariables' vid='" + j + "'vlvalue='" + DVariables[j].vlvalue + "' vlstatus='" + DVariables[j].vlstatus + "'>" + DVariables[j].vlname + "</li>"
                    e++;
                    document.getElementById('one').innerText = e;
                }
                if (DVariables[j].vltype == "2D") {
                    $("#Create" + DVariables[j].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='CeditVariables(this)' class='Cvariables' vid='" + j + "'vlvalue='" + DVariables[j].vlvalue + "' vlstatus='" + DVariables[j].vlstatus + "'>" + DVariables[j].vlname + "</li>"
                    v++;
                    document.getElementById('two').innerText = v;
                }
                if (DVariables[j].vltype == "DataTable") {
                    $("#Create" + DVariables[j].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='CeditVariables(this)' class='Cvariables' vid='" + j + "'vlvalue='" + DVariables[j].vlvalue + "' sheetname='" + DVariables[j].sheetname + "' vlstatus='" + DVariables[j].vlstatus + "'>" + DVariables[j].vlname + "</li>"
                    b++;
                    document.getElementById('datatable').innerText = b;
                }
            }
        }
        catch (Exception) {
        }
    }
    $('#myDropdown').removeClass("show");
    $("#Createvarb").modal('show');
}
function CeditVariables(e) {
    $(".Cvariables").removeClass("active");
    $(e).addClass('active');
    $(cur_sp).val($(e)[0].innerText);
    cur_sp[0].focus()
}
function boolfun() {
    if ($("#VarbType option:selected").text() == 'Boolean') {
        $('#boolSelect').show();
        $('#Dateselect').hide();
        $('#Textselect').hide();
        $('#twoD').hide();
        $('#oneD').hide();
        $('#DataSelect').hide();
        $('#sheetname').hide();
    }
    else if ($("#VarbType option:selected").text() == 'DateTime') {
        $('#Dateselect').show();
        $('#Textselect').hide();
        $('#boolSelect').hide();
        $('#twoD').hide();
        $('#oneD').hide();
        $('#DataSelect').hide();
        $('#sheetname').hide();
    }
    else if ($("#VarbType option:selected").text() == '2D') {
        $('#twoD').show();
        $('#Dateselect').hide();
        $('#Textselect').hide();
        $('#boolSelect').hide();
        $('#oneD').hide();
        $('#DataSelect').hide();
        $('#sheetname').hide();
    }
    else if ($("#VarbType option:selected").text() == '1D') {
        $('#oneD').show();
        $('#twoD').hide();
        $('#Textselect').hide();
        $('#Dateselect').hide();
        $('#boolSelect').hide();
        $('#DataSelect').hide();
        $('#sheetname').hide();
    }
    else if ($("#VarbType option:selected").text() == 'DataTable') {
        $('#DataSelect').show();
        $('#sheetname').show();
        $('#oneD').hide();
        $('#twoD').hide();
        $('#Textselect').hide();
        $('#Dateselect').hide();
        $('#boolSelect').hide();
    }
    else {
        $('#Textselect').show();
        $('#Dateselect').hide();
        $('#boolSelect').hide();
        $('#twoD').hide()
        $('#oneD').hide()
        $('#DataSelect').hide();
        $('#sheetname').hide();
    }
}
var selectedColor;
function showOptions(s) {

    selectedColor = s[s.selectedIndex].value;
}
function createVar() {

    var dvar = {};
    var rid = {};
    rid["Name"] = RobotID;
    dvar["vlname"] = $("#VarbName").val();
    dvar["sheetname"] = $("#Sheetname").val();
    dvar["vltype"] = $('#VarbType :selected').text();
    dvar["vlstatus"] = $("#VarbGlobalVariable")[0].checked;
    dvar["vlvalue"] = $("#VarbDefaultValue").val();
    DVariables.push(dvar);
    DVariables.push(rid);
    var test = $('#VarbType :selected').text();
    var iKeyCode = $("#VarbDefaultValue").val();
    if (test == "1D") {
        if ($("#DefaultXValue").val() != "" && $("#DefaultYValue").val() != "") {
            dvar["vlvalue"] = ($("#DefaultXValue").val() + ":" + $("#DefaultYValue").val());
        }
        else {
            swal("", "Please fill the Requried Fields", "error");
            return;
        }
    }
    if (test == "2D") {
        if ($("#DefaultAValue").val() != "" && $("#DefaultBValue").val() != "" && $("#DefaultCValue").val() != "" && $("#DefaultDValue").val() != "") {
            dvar["vlvalue"] = ($("#DefaultAValue").val() + ":" + $("#DefaultBValue").val() + ";" + $("#DefaultCValue").val() + ":" + $("#DefaultDValue").val());
        }
        else {
            swal("", "Please fill the Requried Fields", "error");
            return;
        }
    }
    if (test == "Boolean") {

        if (test == "Boolean") {
            dvar["vlvalue"] = selectedColor.replace();
            dvar["vlname"] = $("#VarbName").val();
        }
        else {
            swal("", "Please fill the Requried Fields", "error");
            return;
        }
    }
    if (test == "DateTime") {

        if (test == "DateTime") {
            dvar["vlvalue"] = $("#datetimeval").val();;
            dvar["vlname"] = $("#VarbName").val();
            dvar["vlstatus"] = $("#VarbGlobalVariable")[0].checked;
        }
        else {
            swal("", "Please fill the Requried Fields", "error");
            return;
        }
    }
    if (test == "DataTable") {

        if (test == "DataTable") {
            uploadFiles_Service();
            dvar["vlvalue"] = uploadDatatTable;
            dvar["sheetname"] = $("#Sheetname").val();
        }
        else {
            swal("", "Please fill the Requried Fields", "error");
            return;
        }
    }
    if (dvar["vlname"] != "" && dvar["vlvalue"] != "") {
        if (test == "Integer") {
            if (test.length > 0) {
                if ((iKeyCode <= 48 || iKeyCode >= 57)) {
                    dvar["vlvalue"] = $("#VarbDefaultValue").val();
                }
                else {
                    swal("", "It is not integer", "error");
                    return;
                }
            }
        }
        if (test == "String") {
            if (/^[A-Za-z]+$/.test($("#VarbDefaultValue").val())) {
                dvar["vlvalue"] = $("#VarbDefaultValue").val();
            }
            else {
                swal("", "It is not String", "error");
                return;
            }
        }
        if (test == "Dynamic") {
            dvar["vlvalue"] = $("#VarbDefaultValue").val();
        }
    }
    else {
        swal("", "Please fill the Requried Fields", "error");
        return;
    }
    var hpId = JSON.stringify(dvar)
    var div1 = $("#Createvarb").html();
    $.ajax({
        type: "POST",
        url: GlobalURL + '/api/CrudService/CreateVariables?rid=' + RobotID + '&variable=' + hpId,
        contentType: 'application/json; charset=utf-8',
        async: false,
        DataType: "json",
        success: function (val) {

            if (val == "Success") {
                swal("", "Variable Added Successfully", "success", { closeOnClickOutside: false });
                cur_sp.val($("#VarbName").val());
                // cur_sp[0].focus()
               // $("#Createvarb").html(div1);
            }
            else {
                swal("", "You have created duplicate variable", "error", { closeOnClickOutside: false });
            }
           // $("#Createvarb").modal('hide');
        },
        error: function (e) {
        }    
    });
    $("#Createvarb").modal('hide');
}
function loadDVariables() {

    Dynamicvariables();
    $("#Manage1D").html("");
    $("#Manage2D").html("");
    $("#ManageDynamic").html("");
    $("#ManageString").html("");
    $("#ManageInteger").html("");
    $("#ManageDatetime").html("");
    $("#ManageBoolean").html("");
    $("#ManageDataTable").html("");
    $("#ManageVarName").val('');
    $("#ManageVarDefaultValue").val('');
    $("#ManageDefaultType").val('Select');
    $("#ManageVarGlobalVariable")[0].checked = false;
    $('#manageboolSelect').hide();
    $('#manageDataSelect').hide();
    $('#manageDateselect').hide();
    $('#manageoneD').hide();
    $('#managetwoD').hide();
    $('#managetextselect').show();
    $('#managesheetname').show();
    var z = 0; var q = 0; var e = 0;
    var c = 0; var d = 0; var v = 0;
    var k = 0; var b = 0;
    for (var i in DVariables) {
        try {
            if ((typeof (DVariables[i].vltype) != typeof (undefined) || DVariables[i].vltype != null || DVariables[i].vltype != "")) {
                if (DVariables[i].vltype == "Dynamic") {
                    $("#Manage" + DVariables[i].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='editVariables(this)' class='variables' vid='" + i + "'vlvalue='" + DVariables[i].vlvalue + "' vlstatus='" + DVariables[i].vlstatus + "'>" + DVariables[i].vlname + "</li>"
                    z++;
                    document.getElementById('mdynamic').innerText = z;
                }
                if (DVariables[i].vltype == "String") {
                    $("#Manage" + DVariables[i].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='editVariables(this)' class='variables' vid='" + i + "'vlvalue='" + DVariables[i].vlvalue + "' vlstatus='" + DVariables[i].vlstatus + "'>" + DVariables[i].vlname + "</li>"
                    c++;
                    document.getElementById('mstring').innerText = c;
                }
                if (DVariables[i].vltype == "Integer") {
                    $("#Manage" + DVariables[i].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='editVariables(this)' class='variables' vid='" + i + "'vlvalue='" + DVariables[i].vlvalue + "' vlstatus='" + DVariables[i].vlstatus + "'>" + DVariables[i].vlname + "</li>"
                    k++;
                    document.getElementById('minteger').innerText = k;
                }
                if (DVariables[i].vltype == "DateTime") {
                    $("#Manage" + DVariables[i].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='editVariables(this)' class='variables' vid='" + i + "'vlvalue='" + DVariables[i].vlvalue + "' vlstatus='" + DVariables[i].vlstatus + "'>" + DVariables[i].vlname + "</li>"
                    q++;
                    document.getElementById('mdatetime').innerText = q;
                }
                if (DVariables[i].vltype == "Boolean") {
                    $("#Manage" + DVariables[i].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='editVariables(this)' class='variables' vid='" + i + "'vlvalue='" + DVariables[i].vlvalue + "' vlstatus='" + DVariables[i].vlstatus + "'>" + DVariables[i].vlname + "</li>"
                    d++;
                    document.getElementById('mbool').innerText = d;
                }
                if (DVariables[i].vltype == "1D") {
                    $("#Manage" + DVariables[i].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='editVariables(this)' class='variables' vid='" + i + "'vlvalue='" + DVariables[i].vlvalue + "' vlstatus='" + DVariables[i].vlstatus + "'>" + DVariables[i].vlname + "</li>"
                    e++;
                    document.getElementById('mone').innerText = e;
                }
                if (DVariables[i].vltype == "2D") {
                    $("#Manage" + DVariables[i].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='editVariables(this)' class='variables' vid='" + i + "'vlvalue='" + DVariables[i].vlvalue + "' vlstatus='" + DVariables[i].vlstatus + "'>" + DVariables[i].vlname + "</li>"
                    v++;
                    document.getElementById('mtwo').innerText = v;
                }
                if (DVariables[i].vltype == "DataTable") {
                    $("#Manage" + DVariables[i].vltype.trim())[0].innerHTML += "<li style='display:none;'onclick='editVariables(this)' class='variables' vid='" + i + "'vlvalue='" + DVariables[i].vlvalue + "' sheetname='" + DVariables[i].sheetname + "' vlstatus='" + DVariables[i].vlstatus + "'>" + DVariables[i].vlname + "</li>"
                    b++;
                    document.getElementById('mdatatable').innerText = b;
                }
            }
        }
        catch (Exception) {
        }
    }
    $('#myDropdown').removeClass("show");
    $("#manageVar").modal('show');
}
function managefun() {
    if ($("#ManageDefaultType option:selected").text() == 'Boolean') {
        $('#manageboolSelect').show();
        $('#managesheetname').hide();
        $('#manageDataSelect').hide();
        $('#manageDateselect').hide();
        $('#manageoneD').hide();
        $('#managetwoD').hide();
        $('#managetextselect').hide();
    }
    else if ($("#ManageDefaultType option:selected").text() == 'DateTime') {
        $('#manageboolSelect').hide();
        $('#managesheetname').hide();
        $('#manageDataSelect').hide();
        $('#manageDateselect').show();
        $('#manageoneD').hide();
        $('#managetwoD').hide();
        $('#managetextselect').hide();
    }
    else if ($("#ManageDefaultType option:selected").text() == '2D') {
        $('#manageboolSelect').hide();
        $('#managesheetname').hide();
        $('#manageDataSelect').hide();
        $('#manageDateselect').hide();
        $('#manageoneD').hide();
        $('#managetwoD').show();
        $('#managetextselect').hide();
    }
    else if ($("#ManageDefaultType option:selected").text() == '1D') {
        $('#manageboolSelect').hide();
        $('#managesheetname').hide();
        $('#manageDataSelect').hide();
        $('#manageDateselect').hide();
        $('#manageoneD').show();
        $('#managetwoD').hide();
        $('#managetextselect').hide();
    }
    else if ($("#ManageDefaultType option:selected").text() == 'DataTable') {
        $('#manageboolSelect').hide();
        $('#managesheetname').show();
        $('#manageDataSelect').show();
        $('#manageDateselect').hide();
        $('#manageoneD').hide();
        $('#managetwoD').hide();
        $('#managetextselect').hide();
    }
    else if ($("#ManageDefaultType option:selected").text() == 'Dynamic' || $("#ManageDefaultType option:selected").text() == 'String' || $("#ManageDefaultType option:selected").text() == 'Integer') {
        $('#manageboolSelect').hide();
        $('#managesheetname').hide();
        $('#manageDataSelect').hide();
        $('#manageDateselect').hide();
        $('#manageoneD').hide();
        $('#managetwoD').hide();
        $('#managetextselect').show();
    }
}
function editVariables(e) {

    $(".variables").removeClass("active");
    $(e).addClass('active');
    $("#ManageVarName").val($(e).text());
    $("#ManageDefaultType").val($($($(e)[0]).parent().parent().children()[0]).children()[1].innerHTML.trim())
    $("#ManageVarDefaultValue").val($(e)[0].attributes["vlvalue"].nodeValue);
    $("#manageSheetname").val($(e)[0].attributes["sheetname"].nodeValue);
    var check1 = $(e)[0].attributes["vlstatus"].nodeValue.toLowerCase().trim();
    if (check1 == "true") {
        $("#ManageVarGlobalVariable").prop('checked', true);
    }
    else {
        $("#ManageVarGlobalVariable").prop('checked', false);
    }
}
function updateVar() {
    if (typeof ($(".variables.active")[0]) != typeof (undefined)) {
        var variable_id = $(".variables.active")[0].attributes["vid"].nodeValue
        DVariables[variable_id]["vlname"] = $("#ManageVarName").val();
        DVariables[variable_id]["vlvalue"] = $("#ManageVarDefaultValue").val();
        DVariables[variable_id]["vltype"] = $("#ManageDefaultType").val();
        DVariables[variable_id]["vlstatus"] = $("#ManageVarGlobalVariable")[0].checked;
        $("#savedModel")[0].innerHTML += "\n>'" + DVariables[variable_id]["vlname"] + "' Variable Updated";
        var values = JSON.stringify(DVariables[variable_id]);
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/UpdateVariables?rid=' + RobotID + '&variable=' + values,
            contentType: 'application/json; charset=utf-8',
            success: function (val) {

                if (val == "Success") {

                    swal("", "Variable Updated Successfully", "success", { closeOnClickOutside: false });
                }
                else {
                    swal("", "Project already exists, please try again!", "error", { closeOnClickOutside: false });
                }
            },
            error: function (e) {
            }
        });

    }
    $("#manageVar").modal('hide');
}
function Deletevar() {

    swal("Are you sure you want to delete this variable?", {
        buttons: ["Cancel", "Ok"],
        className: "deleterecord",
        closeOnClickOutside: false,
        closeOnEsc: false,
    });
    var variable_id = $(".variables.active")[0].attributes["vid"].nodeValue;
    DVariables[variable_id]["vlname"] = $("#ManageVarName").val();
    DVariables[variable_id]["vlvalue"] = $("#ManageVarDefaultValue").val();
    DVariables[variable_id]["vltype"] = $("#ManageDefaultType").val();
    DVariables[variable_id]["vlstatus"] = $("#ManageVarGlobalVariable")[0].checked;
    var values = JSON.stringify(DVariables[variable_id]);
    var div2 = $("#manageVar").html();

    $(".deleterecord .swal-footer .swal-button-container .swal-button--confirm").on("click", function () {
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/DeleteVariables?rid=' + RobotID + '&variable=' + values,
            contentType: 'application/json; charset=utf-8',
            success: function (val) {
                async: false

                if (val == "Success") {
                    swal("", "Variable Deleted Successfully", "success")
                    Dynamicvariables()
                   // $("#manageVar").html(div2);
                   // $("#manageVar").load();
                }
                else {
                    swal("", "Variable Cannot be deleted ", "error", { closeOnClickOutside: false });
                }
            },
            error: function (e) {

            }

        });
        $("#manageVar").modal('hide');
        //$('#myDropdown').addClass('active');
    });
}
function deleteFunction(e) {
    swal("Are you sure you want to delete this record?", {
        buttons: ["Cancel", "Ok"],
        className: "deleterecord",
        closeOnClickOutside: false,
        closeOnEsc: false,
    });
    $("#deleterobotid")[0].innerHTML = $($('#delete_Robot').parent().first())[0].id
    $("#deleterobotname")[0].innerHTML = $($(".Prjbtn")[0]).id;
    $(".deleterecord .swal-footer .swal-button-container .swal-button--confirm").on("click", function () {
        $('#deleteModal').modal('hide');
        deleteRobot($("#deleterobotid")[0].innerHTML, $("#deleterobotname")[0].innerHTML);
        //swal("", "Robot Deleted Successfully", "success", 3000);
    });
}
function setHeightWidth() {
    $("#outputWindow").css("height", $("#OutputTitle").css("height"), "!important");
    $("#outputWindow").css("margin-bottom", $("footer").css("height"), "!important");
    var DivHeight = window.innerHeight - $("header").height() - $("#outputWindow").height() - $("footer").height() - $($("#middel-panel").children()[1]).height() - $($("#middel-panel").children()[2]).height();
    $("#myDiagramDiv").css("height", DivHeight + "px", "!important");
    $("#myDiagramDiv canvas").css("height", DivHeight + "px", "!important");
    $("#myDiagramDiv").css("width", DivHeight + "px", "!important");
}
//function WorkflowrobotLoad() {
//    

//}
//WorkflowrobotLoad();
function actionclick() {
    if ($('.left-panel-header-value').html() != "Toolset") {
        $('#actionsopen').addClass('actionstabopen');
        $('.left-panel-header-value').html('Toolset');
        $('#actionsopen').show();
        $('#actionsDiv').show();
        $('#ActionProperties').show();
        $('#predefinedrobotsdiv').hide();
        $('#customrobotsdiv').hide();
        $('.side-text').addClass('bg-act-left');
        $('#predefinedrobotspanel').removeClass('bg-act-left');
        $('#customrobotspanel').removeClass('bg-act-left');

    }
    else {
        $('#actionsopen').hide();
        $('.left-panel-header-value').html('');
        $('.side-text').removeClass('bg-act-left');
    }
}

function predefinedrobotsclick() {
    if ($('.left-panel-header-value').html() != "Predefined Robots") {
        $('#actionsopen').addClass('predefinedrobotstabopen');
        $('.left-panel-header-value').html('Predefined Robots');
        $('#actionsopen').show();
        $('#actionsDiv').hide();
        $('#predefinedrobotsdiv').show();
        $('#customrobotsdiv').hide();
        $('#predefinedRobotsDisplay').show();
        $('.side-text').removeClass('bg-act-left');
        $('#predefinedrobotspanel').addClass('bg-act-left');
        $('#customrobotspanel').removeClass('bg-act-left');
        loadPredefinedBots();
    }
    else {
        $('#actionsopen').hide();
        $('.left-panel-header-value').html('');
        $('#predefinedrobotspanel').removeClass('bg-act-left');
    }
}

function connectionexplorerclick() {

    if ($('.left-panel-header-value').html() != "Connection Explorer") {
        $('#actionsopen').addClass('customrobotstabopen');
        $('.left-panel-header-value').html('Connection Explorer');
        $('#actionsopen').show();
        $('#actionsDiv').hide();
        $('#predefinedrobotsdiv').hide();
        $('#customrobotsdiv').show();
        $('#customRobotsDisplay').show();
        $('.side-text').removeClass('bg-act-left');
        $('#predefinedrobotspanel').removeClass('bg-act-left');
        $('#customrobotspanel').addClass('bg-act-left');
        ConnectionTree();
    }
    else {
        $('#actionsopen').hide();
        $('.left-panel-header-value').html('');
    }
}

function load_Database_treeview() {

    var PopupDBDataSource;
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableData?tablename=ConnectionConfig',
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            PopupDBDataSource = JSON.parse(val);
        },
        error: function (e) {
        }
    });
    $('#Database_treeview').on('changed.jstree', function (e, data) {
        var i, j;
        var databaseID
        for (i = 0, j = data.selected.length; i < j; i++) {
            databaseID = data.instance.get_node(data.selected[i]).a_attr.id.split('_')[1];
        }
        var t = $('#ConnectionDetails').DataTable({
            destroy: true,
            "scrollX": false,
            "scrollY": 70,
            "oLanguage": { "sSearch": "" },
            "columnDefs": [{
                "targets": '_all',
                "defaultContent": "",
            },]
        });
        t.clear();
        for (var i in PopupDBDataSource) {
            if (PopupDBDataSource[i].ID == parseInt(databaseID)) {
                t.row.add([
                    PopupDBDataSource[i]["ID"],
                    PopupDBDataSource[i]["connectionname"],
                    PopupDBDataSource[i]["connectiontype"],
                    PopupDBDataSource[i]["hostname"],
                    PopupDBDataSource[i]["username"],
                    PopupDBDataSource[i]["password"],
                    PopupDBDataSource[i]["port"],
                ]);
                t.draw(false);
            }
            t.page('first').draw('page');
            $("#ConnectionDetails_wrapper .dataTables_length label").css("display", "none");
            $($('#ConnectionDetails_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
            // $("#ConnectionDetailsModal").modal('show');
        }
    })
    $('#Srvr_treeview').on('changed.jstree', function (e, data) {
        var i, j;
        var ServerID
        for (i = 0, j = data.selected.length; i < j; i++) {
            ServerID = data.instance.get_node(data.selected[i]).a_attr.id.split('_')[1];
        }
        var t = $('#ConnectionDetails').DataTable({
            destroy: true,
            "scrollX": false,
            "scrollY": 70,
            "oLanguage": { "sSearch": "" },
            "columnDefs": [{
                "targets": '_all',
                "defaultContent": "",
            },]
        });
        t.clear();
        for (var i in PopupDataSource) {
            if (PopupDataSource[i].ID == parseInt(ServerID)) {
                t.row.add([
                    PopupDataSource[i]["ID"],
                    PopupDataSource[i]["connectionname"],
                    PopupDataSource[i]["connectiontype"],
                    PopupDataSource[i]["hostname"],
                    PopupDataSource[i]["username"],
                    PopupDataSource[i]["password"],
                    PopupDataSource[i]["port"],
                ]);
                t.draw(false);
            }
            t.page('first').draw('page');
            $("#ConnectionDetails_wrapper .dataTables_length label").css("display", "none");
            $($('#ConnectionDetails_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
            //$("#ConnectionDetailsModal").modal('show');
        }
    })
}

function ConnectionTree() {

    $('#DB_treeview').empty();
    $('#DB_treeview').append('<div id="Database_treeview"><ul id="tree-view" class="db-ul db_treeview"></ul> </div>');
    $('#Server_treeview').empty();
    $('#Server_treeview').append('<div id="Srvr_treeview"><ul id="tree-view1" class="db-ul sr_treeview"></ul> </div>');
    $('#API_treeview').empty();
    $('#API_treeview').append('<div id="API_URL_treeview"><ul id="tree-view2" class="db-ul ap_treeview"></ul> </div>');
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableData?tablename=ConnectionConfig',
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {

            $(".db_treeview").html("");
            $(".sr_treeview").html("");
            $(".ap_treeview").html("");
            var ConnectionList = $.parseJSON(result);
            for (i in ConnectionList) {
                if (ConnectionList[i].connectiontype == "Database") {
                    $("#tree-view").append("<li class=''><div><a id='Database_" + ConnectionList[i].ID + "' href='#'>" + ConnectionList[i].connectionname + "</a><button id='delete_Connection'  class='button delete_prj fa fa-trash'  onclick='connection_delete(this)'></button><button id='edit_Connection' class='button edit_prj fa fa-edit' style='display: inline-block;' onclick='connection_edit(this)'></button></div></li>");
                }
                else if (ConnectionList[i].connectiontype == "Server") {
                    $("#tree-view1").append("<li class=''><div><a id='Srvr_" + ConnectionList[i].ID + "' href='#'>" + ConnectionList[i].connectionname + "</a><button id='delete_Connection'  class='button delete_prj fa fa-trash'  onclick='connection_delete(this)'></button><button id='edit_Connection' class='button edit_prj fa fa-edit' onclick='connection_edit(this)'></button></div></li>");
                }
                else if (ConnectionList[i].connectiontype == "API") {
                    $("#tree-view2").append("<li class=''><div><a id='APIURL_" + ConnectionList[i].ID + "' href='#'>" + ConnectionList[i].connectionname + "</a><button id='delete_Connection'  class='button delete_prj fa fa-trash'  onclick='connection_delete(this)'></button><button id='edit_Connection' class='button edit_prj fa fa-edit' onclick='connection_edit(this)'></button></div></li>");
                }
            }

            var t = $('#grid').DataTable({
                destroy: true,
                "scrollY": true,
                "scrollX": true,
                "data": ConnectionList,
                "Info": false,
                "pagingType": "simple_numbers",
                "LengthChange": true,
                "aLengthMenu": [[20, 40, 60, -1], [20, 40, 60, "All"]],
                "iDisplayLength": 20,
                "order": [[0, 'desc']],
                "language": { search: '', searchPlaceholder: "Search..." },
                "columns": [

                    {
                        "data": "ID",

                        "title": " ID"
                    },

                    {
                        "data": "connectionname",

                        "title": " Connection Name"
                    },

                    {
                        "data": "connectiontype",

                        "title": " Connection Type"
                    },

                    {
                        "data": "hostname",

                        "title": " Host Name"
                    },

                    {
                        "data": "username",

                        "title": " User Name"
                    },

                    {
                        "data": "password",

                        "title": " Password"
                    },

                    {
                        "data": "port",

                        "title": " Port"
                    },

                    {
                        "data": "authenticationtype",

                        "title": " Authentication Type"
                    },

                    { "title": "Actions", "defaultContent": "" }],
                "oLanguage": { "sSearch": "" },
                "columnDefs": [{
                    "targets": '_all',
                    "defaultContent": "",
                },
                {
                    "aTargets": [0],
                    "sClass": "hidden"
                },
                {
                    "targets": [0],
                    "orderable": true
                },
                {
                    "aTargets": [],

                    "sClass": "hidden"
                }

                ],
            });
            $("#loader").hide();
            t.page('first').draw('page');
            $(".dataTables_filter").addClass("pull-left");
            $($('#grid_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');

            $('#Database_treeview').jstree({
                "core": {
                    "themes": {
                        "icons": false,
                    }
                },
            });

            $('#Srvr_treeview').jstree({
                "core": {
                    "themes": {
                        "icons": false,
                    }
                },
            });

            $('#API_URL_treeview').jstree({
                "core": {
                    "themes": {
                        "icons": false,
                    }
                },
            });
            load_Database_treeview();

        },
        error: function (e) {

        }
    });
}

function editDisplayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        async: false,
        success: function (val) {

            var value = $('[name="EditColumn"]')[i].value;
            var select = $('[name="EditColumn"]')[i];
            $('[name="EditColumn"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            console.log(key);
            select = $('[name="EditColumn"]')[i];


            for (var j = 0; j < dropdownlist.length; j++) {


                if (key.length == 2) {
                    if (dropdownlist[j][key[1]] != null && dropdownlist[j][key[0]] != null) {
                        var opt = document.createElement('option');
                        opt.text = dropdownlist[j][key[1]];
                        opt.id = elementId + "_" + dropdownlist[j][key[1]].replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {

                            var res = "ASCII" + ($0).charCodeAt();
                            return res
                        });
                        opt.value = dropdownlist[j][key[0]];
                        DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[1]];
                    }
                }
                else {
                    if (dropdownlist[j][key[0]] != null) {
                        var opt = document.createElement('option');
                        opt.text = opt.value = dropdownlist[j][key[0]];
                        opt.id = elementId + "_" + dropdownlist[j][key[0]].replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {

                            var res = "ASCII" + ($0).charCodeAt();
                            return res
                        });
                        DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[0]];
                    }
                }
                select.add(opt, j);
            }
            $('[name="EditColumn"]')[i].value = value;
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}

function connection_edit(e) {

    $('#ConnectionEditModal').modal('show');
    var id_tmp = e.parentElement.getElementsByTagName('a')[0].id.split("_")[1];
    var lst_tmp = document.getElementsByClassName("hidden sorting_1");
    var tr;
    for (var i = 0; i < lst_tmp.length; i++) {
        if (id_tmp == lst_tmp[i].textContent) {
            tr = lst_tmp[i].closest('tr')
        }
    }
    var td = tr.children;
    var columns = $('[name="EditColumn"]');
    //console.log(td);
    for (var i = 0; i < columns.length; i++) {
        if (i == 0) {
            $('[name="EditColumn"]')[i].innerHTML = $('[name="EditColumn"]')[i].value = td[i].innerHTML;
        }
        //else if ($('[name="EditColumn"]')[i].localName == "select") {
        //    //$('[name="EditColumns"]')[i].value = $("#" + td[i].innerHTML.replace(" ","")).val();
        //    if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
        //        $('[name="EditColumn"]')[i].value = $("#" + $('[name="EditColumn"]')[i].id.split("|")[1] + "_" + td[i].innerHTML.replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {

        //            var res = "ASCII" + ($0).charCodeAt();
        //            return res
        //        })).val();
        //}
        else {
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
                $('[name="EditColumn"]')[i].value = td[i].innerHTML;
        }
        //console.log($('[name="EditColumns"]')[i].value);
    }

}

function connection_delete(e) {

    var deleteId = e.parentElement.children[0].id.split("_")[1];
    var data = {};
    data["ID"] = deleteId;
    var delId = JSON.stringify(data);
    $.ajax({
        type: "POST",
        url: GlobalURL + '/api/CrudService/deleteTableRow?tablename=ConnectionConfig&ID=' + delId,
        contentType: 'application/json; charset=utf-8',
        success: function (val) {

            if (val == "Success") {
                swal("", "Deleted Successfully", "success", { closeOnClickOutside: false });
                ConnectionTree();
            }
            else {
                $("#feedbackMsg").html("Sorry cannot perform such operation");
            }
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}
function elementsonclick() {
    $('#accordion').toggle();
    if ($('.middle').hasClass('screenwidth')) {
        if ($('.middle').hasClass('thumbexpandright')) {
            $('.middle').css('left', '335px');
            $('.middle').css('width', 'calc(100% - 425px)');
            $('.middle').css('right', '');
            $('.middle').removeClass('screenwidth');
        }
        else {
            $('.middle').css('left', '0px');
            $('.middle').css('width', 'calc(100% - 90px)');
            $('.middle').removeClass('screenwidth');
        }
    }
    else {
        if ($('.middle').hasClass('thumbexpandright')) {
            $('.middle').addClass('screenwidth');

            loadPalette(k);
            $('.middle').css('left', '380px');
            $('.middle').css('width', 'calc(100% - 760px)');
            $('.middle').css('right', '380px');


        } else {
            $('.middle').addClass('screenwidth');

            loadPalette(k);
            $('.middle').css('left', '335px');
            $('.middle').css('width', 'calc(100% - 425px)');
        }
    }
}

function thumbtackleftexpansion() {
    if ($('#thumbtack-left').hasClass('thumbexpandleft')) {
        $('#thumbtack-left').css('transform', 'rotate(90deg)');
        $('.middle').removeClass('screenwidth');
        $('#thumbtack-left').removeClass('thumbexpandleft')
        $('.middle').css('left', '0px');
        $('.middle').css('width', 'calc(100% - 90px)');
        $('.middle').css('right', '');
        $('.left-panel-header-value').html('');
        $('#actionsopen').removeClass('actionstabopen');
        $('#actionsopen').hide();
        //$('.side-text').removeClass('bg-act-left');
        //$('#predefinedrobotspanel').removeClass('bg-act-left');
        //$('#customrobotspanel').removeClass('bg-act-left');
    }
    else {
        $('#thumbtack-left').css('transform', 'rotate(360deg)');
        if ($('#thumbtack-right').hasClass('thumbexpandright')) {
            $('.middle').addClass('screenwidth');
            $('#thumbtack-left').addClass('thumbexpandleft');
            $('.middle').css('left', '380px');
            $('.middle').css('width', 'calc(100% - 760px)');

        }
        else {
            $('.middle').addClass('screenwidth');
            $('#thumbtack-left').addClass('thumbexpandleft');
            $('.middle').css('left', '335px');
            $('.middle').css('width', 'calc(100% - 425px)');


        }
    }
    myDiagram.requestUpdate();
}

function thumbtackrightexpansion() {
    if ($('#thumbtack-right').hasClass('thumbexpandright')) {
        $('#thumbtack-right').css('transform', 'rotate(90deg)');
        if ($('#thumbtack-left').hasClass('thumbexpandleft')) {
            $('.middle').css('width', 'calc(100% - 425px)'); //calc(100% - 425px)
            $('#thumbtack-right').removeClass('thumbexpandright');
        } else {
            $('.middle').css('width', 'calc(100% - 90px)');
            $('.middle').css('left', '0px');
            $('.middle').css('right', '');
            $('#thumbtack-right').removeClass('thumbexpandright');
            $('#right-panel').hide();
            $('#middel-panel').removeClass('limitWidth');
            $('.right-panel-header-value').html('');
            $('#projectexplorerhide').removeClass('bg-act');
        }
    }
    else {
        $('#thumbtack-right').css('transform', 'rotate(360deg)');
        if ($('.middle').hasClass('screenwidth')) {
            $('#thumbtack-right').addClass('thumbexpandright');
            $('.middle').css('width', 'calc(100% - 760px)');
            $('.middle').css('left', '380px');
            $('.middle').css('right', '380px');
        }
        else {
            $('#thumbtack-right').addClass('thumbexpandright');
            $('.middle').css('width', 'calc(100% - 425px)');
            $('.middle').css('left', 'unset');
            $('.middle').css('right', '380px');
        }
    }
    myDiagram.requestUpdate();
}
function SearchPalette() {
    var searchValue = $("#paletteSearch").val();
    var pFilter = searchValue.toUpperCase();
    myPaletteRobot = RobotPalette;
    myPaletteRobotAdv = RobotAdvPalette;
    for (paletteItem in myPaletteRobot) {
        var p = 0;
        myPaletteRobot[paletteItem].nodes.each(function (n) {
            if (n.Zd.Name.toUpperCase().indexOf(pFilter) > -1) {
                p++;
                n.visible = true;
            }
            else
                n.visible = false;
        });

        if (p == 0) {
            $(myPaletteRobot[paletteItem].xw).parent().parent().prev().hide();

        }
        else {
            $(myPaletteRobot[paletteItem].xw).parent().parent().prev().show();
        }
    }
    for (paletteItem in myPaletteRobotAdv) {
        var p = 0;
        myPaletteRobotAdv[paletteItem].nodes.each(function (n) {
            if (n.Zd.Name.toUpperCase().indexOf(pFilter) > -1) {
                p++;
                n.visible = true;
            }
            else
                n.visible = false;
        });

        if (p == 0) {
            $(myPaletteRobotAdv[paletteItem].xw).parent().parent().prev().hide();

        }
        else {
            $(myPaletteRobotAdv[paletteItem].xw).parent().parent().prev().show();
        }
    }
}
function ConnectionAdd() {
    $("#ConnectionAddModal").modal('show');
}

function actiononclick() {
    $('#elementaccordion').hide();
    $('#accordion').toggle();
    $('#accordion2').hide();
    $('#elementaccordion').removeClass('showAccordion');
    $('#accordion').addClass('showAccordion');
    $('#generaldivision').toggleClass('gen_hover');
    $('#conditiondivision').removeClass('gen_hover');
    loadPalette(k);
}

function actiononClickRobot() {

    $('#elementaccordion').hide();
    $('#accordion').show();
    $('#accordion2').hide();
    $('#elementaccordion').removeClass('showAccordion');
    $('#accordion').addClass('showAccordion');
    if ($('#actionsopen').is(':hidden')) {
        loadPalette(k);
        $('#actionsopen').hide();
    }
    else {
        loadPalette(k);
    }
}
// Version related data start

function versions(e) {
    var versionid;
    if (typeof (e) != typeof (undefined)) {
        versionid = e;
    }
    else {
        versionid = $(jQuery('.active').parent()[0]).find('a')[0].id
    }
    version_id = versionid;
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/gettablebyId?tablename=Versions' + '&id=' + versionid,
        contentType: 'application/json; charset=utf-8',
        async: false,
        success: function (val) {
            var dataSource = $.parseJSON(val);
            var t = $('#versionsDisplay').DataTable({
                destroy: true,
                "scrollY": true,
                "scrollX": true,
                "columnDefs": [
                    {
                        "aTargets": [0],
                        "sClass": "hidden"
                    },
                    {
                        "targets": '_all',
                        "defaultContent": "",
                    },
                ]
            });
            t.clear();
            for (i = 0; i < dataSource.length; i++) {
                var d = new Date(dataSource[i]["CreateDatetime"]);
                t.row.add([
                    "<div onclick='datatableclick(this)' style='cursor:pointer'>" + dataSource[i]["rid"] + "</div>",
                    "<div onclick='datatableclick(this)' style='cursor:pointer'> " + dataSource[i]["versionid"] + "</div>",
                    //"<div onclick='datatableclick(this)' style='cursor:pointer'> " + dataSource[i]["CreateDatetime"] + "</div>",
                    "<div onclick='datatableclick(this)' style='cursor:pointer'>  " + d.toLocaleDateString() + " " + d.toLocaleTimeString().replace(/:\d{2}\s/, ' ') + "</div>",
                ]);
                t.draw(false);
            }
            $("#versionsDisplay_wrapper .dataTables_length label").css("display", "none");
            $('#versionsDisplay_filter').css("display", "none");
            $('#versionsDisplay_paginate').css('display', 'none');
            $('#versionsDisplay_info').css('display', 'none');
        },
        error: function (e) {

        }
    });
}
function datatableclick(e) {
    var rtype = "Robot";
    loadVersionRobots($($(e).closest('tr')[0]).find('td')[0].innerText.trim(), rtype, $($(e).closest('tr')[0]).find('td')[1].innerText);
}

function loadVersionRobots(id, type, versionid) {
    RobotID = id;
    RobotType = type;
    RobotVersion = versionid;
    //loadRobotVersionImages();
    if (Robots.length != 0) {
        versionsrobots(RobotID, RobotVersion);
        loadVersionRobot();
        //actionclick();
        //actiononclick();
    }
    else {
        $("#addModal").modal('show');
        $(".close").hide();
        $(".cancel").hide();
    }
}

function loadRobotVersionImages() {
    data = { "Type": "AllActionsByType", "ProjectType": "('Robot','Workflow')" };
    data1 = JSON.stringify(data);
    datasource = GlobalURL + '/api/GenbaseStudio/Get?input=' + data1;
    $.ajax({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        type: 'GET',
        url: datasource,
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (data2) {
            ActionJSON = JSON.parse(data2);
            for (var x = 0; x < ActionJSON.length; x++) {
                if (ActionJSON[x].Type == "Robot") {
                    var y = {};
                    var index = Elements.findIndex(p => p.Id == ActionJSON[x].Element_Id)
                    if (index > -1) {
                        y["source"] = "Images/" + Elements[index].Name.trim() + "/" + ActionJSON[x].Icon;
                        y["category"] = "ImageNode";
                        y["Name"] = ActionJSON[x].Name;
                        y["DisplayName"] = ActionJSON[x].Name.replace(/([A-Z])/g, ' $1').trim();
                        y["Element_Id"] = ActionJSON[x].Element_Id;
                        ActionJSON[x].Element = Elements[index].Name.trim();
                        y["Action_Id"] = ActionJSON[x].Id;
                        y["RuntimeUserInput"] = ActionJSON[x].RuntimeUserInput;
                        y["Dependency"] = ActionJSON[x].Dependency;
                        if (typeof (ElementArr["Robot_" + Elements[index].Name.trim() + "_Arr"]) == typeof (undefined))
                            ElementArr["Robot_" + Elements[index].Name.trim() + "_Arr"] = [];
                        ElementArr["Robot_" + Elements[index].Name.trim() + "_Arr"].push(y);
                    }
                }
                else {
                    var y = {};
                    var index = Elements.findIndex(p => p.Id == ActionJSON[x].Element_Id)
                    if (index > -1) {
                        y["source"] = "Images/ValueStreamMap/" + ActionJSON[x].Icon;
                        y["category"] = "ImageNode";
                        y["Name"] = ActionJSON[x].Name;
                        y["DisplayName"] = ActionJSON[x].Name.replace(/([A-Z])/g, ' $1').trim();;
                        y["Element_Id"] = ActionJSON[x].Element_Id;
                        ActionJSON[x].Element = Elements[index].Name.trim();
                        y["Action_Id"] = ActionJSON[x].Id;
                        y["RuntimeUserInput"] = ActionJSON[x].RuntimeUserInput;
                        y["Dependency"] = ActionJSON[x].Dependency;
                        if (typeof (ElementArr["ValueStream_" + Elements[index].Name.trim() + "_Arr"]) == typeof (undefined))
                            ElementArr["ValueStream_" + Elements[index].Name.trim() + "_Arr"] = [];
                        ElementArr["ValueStream_" + Elements[index].Name.trim() + "_Arr"].push(y);
                    }
                }
            }
        },
        error: function (e) {

        }
    });
}

function loadVersionRobot() {
    StepsProperties = [];
    var i = 0;
    var index = versionSteps.findIndex(p => p.Id == RobotID)
    if (versionSteps[index]["dynamicvariables"] != null)
        DVariables = versionSteps[index]["dynamicvariables"];
    for (t in Object.keys(versionSteps[index]["Steps"])) {
        var StepsTemp = {}
        var DisplayName;
        var localSP = {};
        StepsTemp["StepProperties"] = "";
        StepPropertiesJSON = versionSteps[index]["Steps"][t].StepProperties;
        for (var k in Object.keys(StepPropertiesJSON)) {
            if (StepPropertiesJSON[k].Steps_Id == versionSteps[index]["Steps"][t].Id) {
                StepsTemp["StepProperties"] += StepPropertiesJSON[k].Id + " "
            }
            var y = {

            };
            y["Project_Id"] = StepPropertiesJSON[k].Project_Id;
            y["StepProperty"] = StepPropertiesJSON[k].StepProperty;
            y["StepPropertyType"] = StepPropertiesJSON[k].StepPropertyType;
            if (StepPropertiesJSON[k].StepProperty == "Name")
                DisplayName = StepPropertiesJSON[k].StepPropertyValue;
            y["StepPropertyValue"] = StepPropertiesJSON[k].StepPropertyValue;
            y["Steps_Id"] = StepPropertiesJSON[k].Steps_Id;
            y["Order"] = StepPropertiesJSON[k].Order;
            localSP[StepPropertiesJSON[k].Id] = y;
        }
        localStepProperties[versionSteps[index]["Steps"][t].Id] = localSP;
        StepsTemp["StepID"] = versionSteps[index]["Steps"][t].Id;
        StepsTemp["Action_Id"] = versionSteps[index]["Steps"][t].Action_Id;
        StepsTemp["Element_Id"] = versionSteps[index]["Steps"][t].Element_Id;
        StepsTemp["Name"] = versionSteps[index]["Steps"][t].Name;
        StepsTemp["DisplayName"] = DisplayName;
        var LinkNodesId = versionSteps[index]["LinkNodes"].findIndex(p => p.StepId == versionSteps[index]["Steps"][t].Id)
        if (LinkNodesId != -1) {
            StepsTemp["loc"] = versionSteps[index]["LinkNodes"][LinkNodesId].Location;
            StepsTemp["childs"] = versionSteps[index]["LinkNodes"][LinkNodesId].ChildStepIds;
        }
        else {
            StepsTemp["loc"] = "";
            StepsTemp["childs"] = "";
        }
        StepsProperties[i++] = StepsTemp;
        var x = {};
        x["Action_Id"] = versionSteps[index]["Steps"][t].Action_Id;
        x["Element_Id"] = versionSteps[index]["Steps"][t].Element_Id;
        x["Name"] = versionSteps[index]["Steps"][t].Name;
        x["Robot_Id"] = versionSteps[index]["Steps"][t].Robot_Id;
        x["Workflow_Id"] = "";
        x["DisplayName"] = DisplayName;
        x["Order"] = versionSteps[index]["Steps"][t].Order;
        x["Status"] = 1;
        x["RuntimeUserInput"] = versionSteps[index]["Steps"][t].RuntimeUserInput;
        LocalSteps[versionSteps[index]["Steps"][t].Id] = x;
    }
    var pointsData = {};
    pointsData["class"] = "go.GraphLinksModel";
    var node = {};
    node["key"] = "G1";
    node["isGroup"] = true;
    node["pos"] = "0 0";
    node["size"] = window.outerWidth / 1.54 + " " + window.outerHeight / 1.18;
    var nodeArray = [];
    var linkArray = [];
    nodeArray[0] = node;
    var linkIndex = 0;
    for (var p in StepsProperties) {
        var node = {};
        if (RobotType != 'Robot') {
            if (jQuery('.active').parent().hasClass('Workflow_Robot')) {
                switch (StepsProperties[p].Element_Id) {
                    case "1":
                        node["source"] = "Images/General/" + StepsProperties[p].Name + ".png";
                        break;
                    case "11":
                        node["source"] = "Images/Developer/" + StepsProperties[p].Name + ".png";
                        break;
                    case "999":
                        node["source"] = "dist/img/robot_48.png";
                        break;
                }
            }
            else {
                switch (StepsProperties[p].Element_Id) {
                    case "1": if (StepsProperties[p].Name == "Start" || StepsProperties[p].Name == "Stop" || StepsProperties[p].Name == "Open") {
                        node["source"] = "Images/General/" + StepsProperties[p].Name + ".png";
                    }
                    else
                        node["source"] = "Images/ValueStreamMap/" + StepsProperties[p].Name + ".png";
                        break;
                }
            }
        }
        else {
            var EIndex = Elements.findIndex(q => q.Id == StepsProperties[p].Element_Id);
            if (EIndex > -1)
                node["source"] = "Images/" + Elements[EIndex].Name.trim() + "/" + StepsProperties[p].Name.trim() + ".png";
        }
        node["Name"] = StepsProperties[p].Name;
        node["Action_Id"] = StepsProperties[p].Action_Id;
        node["Element_Id"] = StepsProperties[p].Element_Id;
        node["category"] = "ImageNode";
        node["key"] = "-" + (parseInt(p) + 1);
        node["loc"] = StepsProperties[p].loc;
        node["DisplayName"] = StepsProperties[p].DisplayName;
        node["StepID"] = StepsProperties[p].StepID;
        node["StepProperties"] = StepsProperties[p].StepProperties;
        node["group"] = "G1";
        var Childs = StepsProperties[p].childs.split("`");
        for (var q in Childs) {
            if (Childs[q] != "") {
                var r = 0;
                while (r < StepsProperties.length) {
                    if (Childs[q] == StepsProperties[r].StepID) {
                        var link = {};
                        var a = StepsProperties[p].loc.split(" ");
                        var b = StepsProperties[r].loc.split(" ");
                        link["from"] = "-" + (parseInt(p) + 1);
                        link["to"] = "-" + (parseInt(r) + 1);
                        var linkPoints = [];
                        linkPoints[0] = a[0];
                        linkPoints[1] = a[1];
                        linkPoints[2] = b[1];
                        linkPoints[3] = b[2];
                        link["points"] = linkPoints;
                        linkArray[linkIndex++] = link;
                    }
                    r++;
                }
            }
        }
        nodeArray[(parseInt(p) + 1)] = node;
    }
    pointsData["nodeDataArray"] = nodeArray;
    pointsData["linkDataArray"] = linkArray;
    myDiagram.model = go.Model.fromJson(pointsData);
    $("#ProjectBreadCrumb").html(" " + $("#ProjectTree").find('li a').first().text())
    $("#ProcessBreadCrumb").html($($("#ProjectTree li .active")[0]).text());
    if ($($($(".active")[0]).parent()[0]).hasClass('Robot')) {
        $("#Processi").removeClass('ValueStreamMap');
        $("#Processi").addClass('Robot');
        $("#Processi").parent().removeClass('ValueStreamMap');
        $("#Processi").parent().addClass('Robot');

    }
    else if ($($($(".active")[0]).parent()[0]).hasClass('ValueStreamMap')) {
        $("#Processi").removeClass('fa fa-list-alt');
        $("#Processi").addClass('fa fa-indent');
    }
    else {
        $("#Processi").removeClass('fa fa-list-alt')
        $("#Processi").removeClass('fa fa-indent')
    }
}

function versionsrobots(rid, versionid) {
    var data = { "Type": "AllVersionSteps", "rid": rid, "Version_Id": versionid };

    data1 = JSON.stringify(data);

    datasource = GlobalURL + '/api/GenbaseStudio/Get?input=' + data1;
    $.ajax({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        type: 'GET',
        url: datasource,
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (data2) {
            versionSteps = $.parseJSON(data2);
        },
        error: function (e) {

        }
    });
}

// Version related data end

function prjctclick() {
    if ($('.right-panel-header-value').html() != "Project Explorer") {
        $('#right-panel').show();
        $('#middel-panel').addClass('limitWidth');
        $('#projectexplorer-panel').show();
        $('#properties-panel').hide();
        $('#versionsDisplay').hide();
        $('#versions-panel').hide();
        $('.right-panel-header-value').html('Project Explorer');
        $('#projectexplorerhide').addClass('bg-act');
        $('#propertieshide').removeClass('bg-act');
        $('#versionshide').removeClass('bg-act');

    }
    else {

        $('#right-panel').hide();
        $('#middel-panel').addClass('limitWidth');
        $('.right-panel-header-value').html('');
        $('#projectexplorerhide').removeClass('bg-act');
    }
}

function propertyclick() {
    if ($('.right-panel-header-value').html() != "Properties") {
        $('#right-panel').show();
        $('#middel-panel').addClass('limitWidthP');
        $('.right-panel-header-value').html('Properties');
        $('#projectexplorer-panel').hide();
        $('#properties-panel').show();
        $('#versionsDisplay').hide();
        $('#versions-panel').hide();
        $('#projectexplorerhide').removeClass('bg-act');
        $('#propertieshide').addClass('bg-act');
        $('#versionshide').removeClass('bg-act');
        $('#noPropertiesDisplay').hide();
    }
    else {

        $('#right-panel').hide();
        $('#middel-panel').removeClass('limitWidthP');
        $('.right-panel-header-value').html('');
        $('#propertieshide').removeClass('bg-act');
    }
}

function versionclick() {
    if ($('.right-panel-header-value').html() != "Versions") {
        $('#right-panel').show();
        $('#middel-panel').addClass('limitWidthV');
        $('#projectexplorer-panel').hide();
        $('#properties-panel').hide();
        $('#versionsDisplay').show();
        $('#versions-panel').show();
        $('.right-panel-header-value').html('Versions');
        $('#propertieshide').removeClass('bg-act');
        $('#projectexplorerhide').removeClass('bg-act');
        $('#versionshide').addClass('bg-act');
    }
    else {
        $('#right-panel').hide();
        $('#middel-panel').removeClass('limitWidthV');
        $('.right-panel-header-value').html('');
        $('#versionshide').removeClass('bg-act');
    }
}

function myFunction() {
    //document.getElementById("myDropdown").classList.toggle("show");
    //$('.right-panel-header-value').html('');
    //$('#right-panel').hide();
    document.getElementById("myDropdown").classList.toggle("show");
    $('.right-panel-header-value').html('');
    $('#right-panel').hide();
    //$('#debugdropdown').removeClass("show");
    //document.getElementById("debugdropdown").classList.toggle("hide")
}
var debugvalue = 0;
var PredefinedCategories = [];
var Predefined_List = {};
var Predefined_Bots = [];
var Predefined_Actions = [];
var Predefined_Accordion_List = [];
var Predefined_LinkData = [];
var Predefined_Links = {};
function getPredefinedBots() {
    $.ajax({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        type: 'GET',
        url: GlobalURL + '/api/GenbaseStudio/Get?input=' + JSON.stringify({ "Type": "PredefinedBots" }),
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (PredefinedJSON) {
            if (PredefinedJSON != "false") {
                var PredefinedData = JSON.parse(PredefinedJSON);
                if (PredefinedData["Categroy"] != "false")
                    PredefinedCategories = JSON.parse(PredefinedData["Categroy"]);
                if (PredefinedData["PredefinedBot"] != false)
                    Predefined_Bots = JSON.parse(PredefinedData["PredefinedBot"]);
                if (PredefinedData["PredefinedActions"] != false)
                    Predefined_Actions = JSON.parse(PredefinedData["PredefinedActions"]);
            }


            $.each(Predefined_Bots, function (iterator, Bot_Dict) {
                var categoryIndex = PredefinedCategories.findIndex(p => p.ID == Bot_Dict["Category_Id"]);
                if (typeof (Predefined_List[PredefinedCategories[categoryIndex].Name]) == typeof (undefined))
                    Predefined_List[PredefinedCategories[categoryIndex].Name] = [];
                var Predefined_Dict = {};
                Predefined_Dict["key"] = "Group_" + Bot_Dict["ID"];
                Predefined_Dict["isGroup"] = true;
                Predefined_Dict["DisplayName"] = Bot_Dict["Name"];
                Predefined_Dict["Name"] = Bot_Dict["Name"];
                Predefined_Dict["source"] = "dist/img/icons/dboard/" + Bot_Dict["Icon"];
                Predefined_Dict["category"] = "ImageNode";
                Predefined_List[PredefinedCategories[categoryIndex].Name].push(Predefined_Dict);
            });
            $.each(Predefined_Actions, function (iterator, Action_Dict) {
                var locActionId = ActionJSON.findIndex(p => p.Id == Action_Dict["Action_ID"])
                if (locActionId >= 0) {
                    var Predefined_Dict = {};
                    var Predefined_Bot_Index = Predefined_Bots.findIndex(p => p.ID == Action_Dict["PredefinedBot_ID"]);
                    var categoryIndex = PredefinedCategories.findIndex(p => p.ID == Predefined_Bots[Predefined_Bot_Index]["Category_Id"]);
                    Predefined_Dict["key"] = Action_Dict["ID"];
                    Predefined_Dict["group"] = "Group_" + Action_Dict["PredefinedBot_ID"];
                    Predefined_Dict["DisplayName"] = ActionJSON[locActionId]["Name"];
                    Predefined_Dict["Name"] = ActionJSON[locActionId]["Name"];
                    Predefined_Dict["source"] = "Images/" + ActionJSON[locActionId]["Element"] + "/" + ActionJSON[locActionId]["Icon"];
                    Predefined_Dict["Element_Id"] = ActionJSON[locActionId]["Element_Id"];
                    Predefined_Dict["loc"] = Action_Dict["Location"];
                    Predefined_Dict["Action_Id"] = ActionJSON[locActionId]["Id"];
                    Predefined_Dict["RuntimeUserInput"] = ActionJSON[locActionId]["RuntimeUserInput"];
                    Predefined_Dict["category"] = "ImageNode";
                    //Predefined_Accordion_List.push(Predefined_Dict);
                    Predefined_List[PredefinedCategories[categoryIndex].Name].push(Predefined_Dict);
                    $.each(Action_Dict["ChildActions"].split(","), function (iterator, Childs) {
                        if (typeof (Predefined_Links[PredefinedCategories[categoryIndex].Name]) == typeof (undefined))
                            Predefined_Links[PredefinedCategories[categoryIndex].Name] = [];
                        var Pred_LinkDict = {};
                        Pred_LinkDict["from"] = Action_Dict["ID"];
                        Pred_LinkDict["to"] = Childs;
                        // Predefined_LinkData.push(Pred_LinkDict);
                        Predefined_Links[PredefinedCategories[categoryIndex].Name].push(Pred_LinkDict);
                    });
                }
            });
            $.each(PredefinedCategories, function (iterator, PCategory) {
                if (typeof (Predefined_List[PCategory["Name"]]) != typeof (undefined))
                    $("#predefinedrobotsdiv")[0].innerHTML += "<h4>" + PCategory["Name"] + "</h4><div><div id='predefined_" + PCategory["Name"].replace(/[\ ]/g, "") + "_RobotsDisplay' style='width: 100%; height:250px'></div></div>"
            });
        },
        error: function (e) {
            getPredefinedBots();
        }
    });
}
var myPredefinedPalette = {};
function loadPredefinedBots() {
    if (!jQuery("#predefinedrobotsdiv").hasClass('ui-accordion')) {
        $.each(PredefinedCategories, function (iterator, PCategory) {
            if (Predefined_List[PCategory.Name] != [] && typeof (Predefined_List[PCategory.Name]) != typeof (undefined)) {
                jQuery("#predefinedrobotsdiv").accordion({
                    activate: function (event, ui) {
                        myPredefinedPalette[PCategory.Name].requestUpdate();
                    }
                });

                myPredefinedPalette[PCategory.Name] =
                    MK(go.Palette, "predefined_" + PCategory["Name"].replace(/[\ ]/g, "") + "_RobotsDisplay",  // must name or refer to the DIV HTML element
                        {
                            "animationManager.duration": 800, // slightly longer than default (600ms) animation
                            nodeTemplateMap: myDiagram.nodeTemplateMap,
                        });
                myPredefinedPalette[PCategory.Name].groupTemplate =
                    // always save/load the point that is the top-left corner of the node, not the location
                    MK(go.Group,
                        {
                            isSubGraphExpanded: false,  // only show the Group itself, not any of its members
                            ungroupable: false,
                            layerName: "Background",
                            resizable: true, resizeObjectName: "SHAPE",
                            // because the gridSnapCellSpot is Center, offset the Group's location
                            locationSpot: new go.Spot(0, 0, CellSize.width / 2, CellSize.height / 2),
                            deletable: false,
                            selectable: true
                        },
                        MK(go.Panel, "Vertical",

                            MK(go.Picture,
                                {
                                    desiredSize: new go.Size(48, 48),
                                    margin: 1,
                                    fromLinkable: true,
                                    toLinkable: true,

                                },
                                new go.Binding("source")
                            ),
                            MK(go.TextBlock,
                                new go.Binding("text", "DisplayName"),
                                {
                                    stroke: "black",
                                    width: 48,
                                    font: "16pt",
                                    maxLines: 1,
                                    wrap: go.TextBlock.WrapFit,
                                    overflow: go.TextBlock.OverflowEllipsis,
                                    margin: 2,
                                    textAlign: "center",
                                })
                        ));

                //myPredefinedPalette.model = new go.GraphLinksModel(Predefined_Accordion_List, Predefined_LinkData);
                myPredefinedPalette[PCategory.Name].model = new go.GraphLinksModel(Predefined_List[PCategory.Name], Predefined_Links[PCategory.Name]);
            }
        });


    }

}
function scheduleModal() {

    $("#ScheduleModal").modal('show');
    //$("#ScheduleEventModal").modal('show');
    // $("#Interval").val('')
}
function loadScheduleDetails() {

    if ($("#ScheduleSystems").val() != null) {
        $.ajax({
            type: "GET",
            url: GlobalURL + '/api/CrudService/getScheduleDetails?RobotId=' + RobotID + '&SystemId=' + $("#ScheduleSystems").val(),
            async: false,
            contentType: 'application/json; charset=utf-8',
            success: function (val) {

                var scheduleDetails = JSON.parse(val);
                if (scheduleDetails.length > 0) {
                    $("#Interval").val(scheduleDetails[0]['interval']);
                    $("#startdatetimepicker").val(scheduleDetails[0]['Startdate']);
                    $("#appt-time").val(scheduleDetails[0]['Starttime']);
                    //$("#priority").val(scmheduleDetails[0]['Priority']);
                    $("#RepeatType").val(scheduleDetails[0]['Timeperiod']);
                    $("#description").val(scheduleDetails[0]['Tag']);
                    $("#Status_Scheduler")[0].checked = scheduleDetails[0]['ScheduleStatus']
                    $("#Server_Scheduler")[0].checked = (scheduleDetails[0]['Runatserver']);
                    if (scheduleDetails[0]['interval'] != "")
                        document.getElementsByName("scheduleInterval")[0].style.display = "block"
                    if (scheduleDetails[0]['Timeperiod'] != "Never") {
                        //document.getElementsByName("end_date")[0].style.display = "block"
                        var chk_list = document.getElementsByName("end_date")[0].getElementsByClassName("radio_main_cl")
                        for (j = 0; j < chk_list.length; j++) {
                            if (chk_list[j].getElementsByTagName("label")[0].textContent == scheduleDetails[0]["End"]) {
                                chk_list[j].getElementsByTagName("input")[0].checked = "true";
                            }
                        }
                    }

                    if (scheduleDetails[0]["Instance"]) {
                        var runtimeVar = "";
                        runtimeVar = scheduleDetails[0]["Instance"].replace(/-/g, ',');
                        runtimeVar = JSON.parse(runtimeVar);

                        for (var i = 0; i < runtimeVar.length; i++) {
                            var arrkey = runtimeVar[i].i_key;
                            var arrValue = runtimeVar[i].i_value;

                            $('<div/>').addClass('form-group col-sm-12')
                                .html($('<input type="textbox" id="i_key" name="i_key[]" value="' + arrkey + '"/>').addClass('resizedTextbox'))
                                .append($('<input type="textbox" id="i_value" name="i_value[]" value="' + arrValue + '"/>').addClass('resizedTextbox'))
                                .append($('<button/>').addClass('remove btn btn-primary btn-flat').text('Remove'))
                                .insertBefore($('#add'));
                        }
                    }
                }
                else {
                    $("#Interval").val('');
                    $("#startdatetimepicker").val('');
                    $("#appt-time").val('');
                    //$("#priority").val(scmheduleDetails[0]['Priority']);
                    $("#RepeatType").val('');
                    $("#description").val('');
                    $("#Status_Scheduler")[0].checked = false;
                    $("#Server_Scheduler")[0].checked = false;
                    document.getElementsByName("end_date")[0].style.display = "none"
                }
            },
            error: function (e) {
            }
        });
    }
}
function ScheduleClose() {
    $("#Interval").val("0");
    $("#startdatetimepicker").val('');
    $("#appt-time").val('');
    //$("#priority").val(scmheduleDetails[0]['Priority']);
    $("#RepeatType").val("Never");
    $("#description").val('');
    $("#Status_Scheduler")[0].checked = false;
    $("#Server_Scheduler")[0].checked = false;
    document.getElementsByName("end_date")[0].style.display = "none"
    $("#ScheduleModal").modal('hide');
    $("#ScheduleEventModal").modal('hide');
}
function ScheduleEventClose() {
    $("#Interval").val("0");
    $("#startdatetimepicker").val('');
    $("#appt-time").val('');
    //$("#priority").val(scmheduleDetails[0]['Priority']);
    $("#RepeatType").val("Never");
    $("#description").val('');
    $("#Status_Scheduler")[0].checked = false;
    $("#Server_Scheduler")[0].checked = false;
    document.getElementsByName("end_date")[0].style.display = "none"

    $("#ScheduleModal").modal('hide');
    $("#ScheduleEventModal").modal('hide');
}
//function StopSchedule() {
//    var Values = {};
//    var interval = $("#Interval").val();
//    Values["RobotId"] = RobotID;
//    Values["SystemId"] = $("#ScheduleSystems").val();
//    Values["interval"] = interval;
//    Values["StartTime"] = $("#appt-time").val();
//    Values["StartDate"] = $("#datetimepicker").val();
//    Values["Priority"] = $("#priority").val();
//    Values["TimePeriod"] = $("#TimePeriod").val();
//    Values["CreateBy"] = (JSON.parse(localStorage.Result).User_ID);
//    Values["UpdateBy"] = (JSON.parse(localStorage.Result).User_ID);
//    Values["Status"] = $("#Status_Scheduler")[0].checked;
//    $.ajax({
//        type: "GET",
//        url: '/services/CrudService.svc/robotSchedule?ScheduleDetails=' + JSON.stringify(Values),
//        async: false,
//        //data: Htmlname ,
//        contentType: 'application/json; charset=utf-8',
//        //dataType: 'json',
//        success: function (val) {
//            ScheduleClose();
//            $('#ScheduleModal').modal('hide');
//            if (val == "true" || val == "Success") {
//                swal("", "This Robot Scheduler Stopped Successfully", "success", { closeOnClickOutside: false });  
//            }
//            else {
//                swal("", "Please check and try again!", "error")
//            }
//        },
//        error: function (e) {

//        }
//    });
//}
function ScheduleFunction() {

    var interval = $("#Interval").val();
    var Values = {};
    Values["RobotId"] = RobotID;
    Values["SystemId"] = $("#ScheduleSystems").val();
    Values["interval"] = interval;
    Values["StartTime"] = $("#appt-time").val();
    Values["StartDate"] = $("#startdatetimepicker").val();
    //Values["Priority"] = null;
    Values["TimePeriod"] = $('#RepeatType')[0].value;
    Values["CreateBy"] = (JSON.parse(localStorage.Result).User_ID);
    Values["UpdateBy"] = (JSON.parse(localStorage.Result).User_ID);
    Values["Status"] = $("#Status_Scheduler")[0].checked;
    Values["Runatserver"] = $("#Server_Scheduler")[0].checked;
    //Values["EndDate"] = $("#enddatetimepicker").val();
    Values["EndDate"] = "";
    Values["End"] = "";
    Values["RepeatOn"] = "";
    Values["RepeatEvery"] = "";
    Values["Description"] = $('#description')[0].value;

    var arrKey = $('input[name="i_key[]"]').map(function () {
        return this.value; // $(this).val()
    }).get();
    var arrValue = $('input[name="i_value[]"]').map(function () {
        return this.value; // $(this).val()
    }).get();

    jsonObj = []; 

    for (var i = 0; i < arrKey.length; i++) {
        item = {}
        item["i_key"] = arrKey[i];
        item["i_value"] = arrValue[i];

        jsonObj.push(item);
    }
    if (Array.isArray(jsonObj) && jsonObj.length)
        Values["Instance"] = JSON.stringify(jsonObj);
    else
        Values["Instance"] = "";

    if (Values["TimePeriod"] != "Never") {
        if (Values["TimePeriod"] == "Daily") {
            Values["RepeatEvery"] = $('#RepeatEvery')[0].value + 'd';
        }
        else if (Values["TimePeriod"] == "Weekly") {
            Values["RepeatOn"] = [];
            Values["RepeatEvery"] = $('#RepeatEvery')[0].value + 'w';
            for (i = 0; i < document.getElementsByClassName('weekly_checkbox')[0].getElementsByClassName('predefinedCheckbox').length; i++) {
                if (document.getElementsByClassName('weekly_checkbox')[0].getElementsByClassName('predefinedCheckbox')[i].checked == true) {
                    Values["RepeatOn"].push(document.getElementsByClassName('weekly_checkbox')[0].children[i].children[2].textContent);
                }
            }
            Values["RepeatOn"] = Values["RepeatOn"].join('_');
        }
        else if (Values["TimePeriod"] == "Monthly") {
            Values["RepeatEvery"] = $('#RepeatEvery')[0].value + 'm';
            if (document.getElementsByName("end_monthly")[0].checked == true) {
                Values["RepeatOn"] = document.getElementsByName("end_monthly")[0].parentElement.children[2].value; // Only Day
            }
            else if (document.getElementsByName("end_monthly")[1].checked == true) {
                Values["RepeatOn"] = document.getElementsByName("end_monthly")[1].parentElement.children[2].value + " " + document.getElementsByName("end_monthly")[1].parentElement.children[3].value
            }
        }
        else if (Values["TimePeriod"] == "Yearly") {
            Values["RepeatEvery"] = $('#RepeatEvery')[0].value + 'y';
            if (document.getElementsByName("end_yearly")[0].checked == true) {
                Values["RepeatOn"] = document.getElementsByName("end_yearly")[0].parentElement.children[2].value + "_" + document.getElementsByName("end_yearly")[0].parentElement.children[1].value;; //  Day
            }
            else if (document.getElementsByName("end_yearly")[1].checked == true) {
                Values["RepeatOn"] = document.getElementsByName("end_yearly")[1].parentElement.children[2].value + " " + document.getElementsByName("end_yearly")[1].parentElement.children[3].value + " " + document.getElementsByName("end_yearly")[1].parentElement.children[5].value;
            }
        }
        else { }
        for (j = 0; j < document.getElementsByName("end_repeat").length; j++) {
            if (document.getElementsByName("end_repeat")[j].checked == true) {
                Values["End"] = document.getElementsByName("end_repeat")[j].parentElement.children[1].textContent;
                var End_Type = document.getElementsByName("end_repeat")[j].parentElement.children[1].textContent;
                if (End_Type == "Never") {
                    Values["End"] = End_Type;
                }
                else if (End_Type == "After") {
                    Values["End"] = document.getElementsByName("end_repeat")[j].parentElement.children[2].value + "O"
                }
                else if (End_Type == "On") {
                    Values["End"] = document.getElementsByName("end_repeat")[j].parentElement.children[2].value
                }
                else { }
            }
        }
    }
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/robotSchedule?ScheduleDetails=' + JSON.stringify(Values),
        async: false,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        success: function (val) {
            ScheduleClose();
            $('#ScheduleModal').modal('hide');
            if (val == "true" || val == "Success") {
                swal("", "Scheduled Successfully", "success", { closeOnClickOutside: false });
            }
            else {
                swal("", "Please Schedule With All Details", "error", { closeOnClickOutside: false });
            }
        },
        error: function (e) {

        }
    });
}


function scheduleEvent() {

    $('#ScheduleModal').modal('hide');
    $("#ScheduleSystems")[0].innerHTML = "";
    var select = document.getElementById("ScheduleSystems");

    var hash = {}; var out = [];
    for (var i = 0, l = Systems_List.length; i < l; i++) {
        //var key = A[i].join('|');
        var key = Systems_List[i].Name;
        if (!hash[key]) {
            out.push(Systems_List[i]);
            hash[key] = 'found';
        }
    }

    var outSystems_List = out;
    for (var system in outSystems_List) {
        var opt = document.createElement('option');
        opt.value = outSystems_List[system]["ID"];
        opt.text = outSystems_List[system]["Name"];
        select.add(opt, system);
    }
    //var opt = document.createElement('option');
    //opt.value = 0;
    //opt.text = "Server";
    //select.add(opt, 0);
    var opt = document.createElement('option');
    opt.value = "";
    opt.text = "Select System";
    opt.selected = true;
    opt.disabled = true;
    opt.hidden = true;
    select.add(opt, 0);

    var opt = document.createElement('option');
    opt.value = 0;
    opt.text = "Server";
    select.add(opt, 1);

    $("#ScheduleEventModal").modal('show');

    $('#RepeatType').on("change", function (e, data) {
        var list = document.getElementsByClassName("repeat_every")[0];
        document.getElementsByName("interval")[0].style.display = "none";
        document.getElementsByName("weekly")[0].style.display = "none";
        document.getElementsByName("daily")[0].style.display = "none";
        document.getElementsByName("end_date")[0].style.display = "none";
        document.getElementsByName("Monthly")[0].style.display = "none";
        document.getElementsByName("yearly")[0].style.display = "none";
        document.getElementsByName("scheduleInterval")[0].style.display = "none";

        if (e.currentTarget.value == "Repeat By Interval") {
            document.getElementsByName("scheduleInterval")[0].style.display = "block";
            //document.getElementsByName("interval")[0].style.display = "block";
            //document.getElementsByName("end_date")[0].style.display = "block";
        }
        else if (e.currentTarget.value == "Daily") {
            list.getElementsByTagName("label")[0].innerHTML = "Day(s)";
            document.getElementsByName("daily")[0].style.display = "block";
            //document.getElementsByName("end_date")[0].style.display = "block";
        }
        else if (e.currentTarget.value == "Weekly") {
            list.getElementsByTagName("label")[0].innerHTML = "Week(s)";
            document.getElementsByName("daily")[0].style.display = "block";
            //document.getElementsByName("end_date")[0].style.display = "block";
            document.getElementsByName("weekly")[0].style.display = "block";
        }
        else if (e.currentTarget.value == "Monthly") {
            list.getElementsByTagName("label")[0].innerHTML = "Month(s)";
            document.getElementsByName("daily")[0].style.display = "block";
            //document.getElementsByName("end_date")[0].style.display = "block";
            document.getElementsByName("Monthly")[0].style.display = "block";
        }
        else if (e.currentTarget.value == "Yearly") {
            list.getElementsByTagName("label")[0].innerHTML = "Year(s)";
            document.getElementsByName("daily")[0].style.display = "block";
            //document.getElementsByName("end_date")[0].style.display = "block";
            document.getElementsByName("yearly")[0].style.display = "block";
            $("#week_selection1").html($("#week_selection").html());
            $("#weekday_selection1").html($("#weekday_selection").html());
            $("#Month_selection1").html($("#Month_selection").html());
        }
    });
}