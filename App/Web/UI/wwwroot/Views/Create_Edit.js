﻿
var tid = '';
var tmode = '';

if (tableId == '' && tableMode == '') {
    back();
}
if (tableId != '' && tableMode == 'Edit') {
    
    tid = tableId;
    tmode = tableMode;

}
else {
    
    tableId = '';
    tableMode = 'Add'
}



tableLoad();
function tableLoad() {
  
    if (tid != '') {

        
        $.ajax({
            type: "GET",
            url: '/services/CrudService.svc/getapp_columnsData?table_id=' + tid,
            contentType: 'application/json; charset=utf-8',
            async: false,

            success: function (val) {

                
                var tblData = $.parseJSON(val[0]);
                tid = tblData.table_id;
                $('#table_id').val(tblData[0].table_id);
                $("#tname").val(tblData[0].table_name);
                $('#tname').attr('disabled', 'true');
                $("#comment").val(tblData[0].comments);
                $("#categoryvalue").val(tblData[0].category);
                $('#categoryvalue').attr('disabled', 'true');
                $('#comment').attr('disabled', 'true');
                $("#btn_Save").prop("disabled", false);
                columnsGrid(val[1]);
               
             
            },

            error: function (e) {
                //alert("Something Wrong!!");
                tableLoad();
            }

        });
    }
    else {
        columnsGrid('true');
    }

    function columnsGrid(tbresult) {
        
        var dataSource = $.parseJSON(tbresult);
        var columns = $('[name="EditColumns"]');
        var t = $('#grid_columns').DataTable({

            destroy: true,
            "scrollY": true,
            "scrollX": true,
            "data": dataSource,
            "bPaginate": false,
            "columns": [

                   {
                       "data": "column_name",
                       "title": "Column Name<span style='color:red'>*</span>"
                   },

                   {
                       "data": "column_description",
                       "title": "Column Description"
                   },

                  {
                      "data": "data_type",
                      "title": "Data Type"
                  },
                   {
                       "data": "data_type_length",
                       "title": "Length"
                   },
                   {
                       "data": "data_type_precision",
                       "title": "Precision"
                   },
                   {
                       "data": "primary_key",
                       "title": "Primary Key"
                   },
                   {
                       "data": "mandatory",
                       "title": "Mandatory Key"
                   },
                   {
                       "data": "unique_key",
                       "title": "Unique key"
                   },
                   {
                       "title": "Actions",
                       "defaultContent": "<i class=\"fas fa-trash\" style=\"cursor:pointer\"></i>"
                   },

            ],
            "columnDefs": [{
                "targets": '_all',
                "defaultContent": "",
            },
            //{
            //    "aTargets": [5, 6, 7, 8, 9],
            //    "sClass": "hidden"
            //}
            ],

            

        });
        for (i = 0; i < $($("#grid_columns")[0]).find("tr").length; i++) {
            $($($($("#grid_columns")[0]).find("tr")[i]).find("td")[8]).hide();
        }
        
        t.page('first').draw('page');
        $("#grid_wrapper .dataTables_length label").css("display", "none");
        $(".dataTables_filter").addClass("pull-left");
        
        $('#tbdata').click(function () {
            
            var tname = $('#tname').val();
            if (tname == "") {
                
                swal("", "Please fill table name", "error")
                return false;
            }
            else {


                var rowHtml = createNewRow();
                //console.log(rowHtml);

                for (var i = 0; i < $("#grid_columns tbody tr.new_tr").length; i++) {
                    if ($("#grid_columns tbody tr.new_tr").length == 1) {
                        var colname = $('.columnName')
                        if (colname.val() == "") {
                            swal("", "Please fill column name", "error")
                            return false;
                        }
                    }
                    else if ($('.columnName')[i].value == "") {
                        swal("", "Please fill column name", "error")
                        return false;
                    }
                    //else {
                    //}
                }

                t.row.add($(rowHtml)).draw();
                check_pk();




                //t.row.add( [1,2,3] ).draw();
                // var rowHtml = $("#newRow").find("tr")[0].outerHTML

                //var rowHtml = createNewRow();
                //if (createNewRow()) { }
                //console.log(rowHtml);

                //
                ////t.row.add( [1,2,3] ).draw();
                //// var rowHtml = $("#newRow").find("tr")[0].outerHTML
                //var rowHtml = createNewRow();
                //console.log(rowHtml);


                //t.row.add( [1,2,3] ).draw();
                // var rowHtml = $("#newRow").find("tr")[0].outerHTML

            }

        });
        




        $("#grid_columns").on("mousedown", "td .fas.fa-trash", function (e) {
            

            swal("Are you sure you want to delete this record?", {
                buttons: ["Cancel", "Ok"],
                className: "deleterecord",
                closeOnClickOutside: false,
                closeOnEsc: false,

            });

            
            var cr_row = this;
            $(".deleterecord .swal-footer .swal-button-container .swal-button--confirm").on("click", function () {
                
                t.row($(cr_row).closest("tr")).remove().draw();
            });
        });

    }


    function check_pk() {
        $(".pk").click(function () {
            
            var id_mk = $($(this).closest("tr")).find('td').find('.mk');
            var id_uk = $($(this).closest("tr")).find('td').find('.uk');
            if ($(this).prop('checked')) {
                // alert("dwdw");

                $(id_mk).prop('checked', true);
                $(id_uk).prop('checked', true);

                $(id_mk).prop('disabled', true);
                $(id_uk).prop('disabled', true);
                //$(".checkBoxClass").prop('checked', $(this).prop('checked'));
                //$(".checkBoxClass").prop('disabled', true)
            }
            else {

                $(id_mk).prop('checked', false);
                $(id_uk).prop('checked', false);

                $(id_mk).prop('disabled', false);
                $(id_uk).prop('disabled', false);
                //$(".checkBoxClass").prop('checked', false);
                //$(".checkBoxClass").prop('disabled', false)
            }
        });

        
        $(".select2").change(function () {
            

            if ($($(this).closest("tr")).find('td').find('.select2').val() == "character varying" || $($(this).closest("tr")).find('td').find('.select2').val() == "character" || $($(this).closest("tr")).find('td').find('.select2').val() == "numeric") {
                $($($(this).closest("tr")).find('td').find('.len')).prop('disabled', false);
                $($($(this).closest("tr")).find('td').find('.prec')).prop('disabled', true);
                $($($(this).closest("tr")).find('td').find('.len')).val('');
                $($($(this).closest("tr")).find('td').find('.prec')).val('');

            }
            else {
                $($($(this).closest("tr")).find('td').find('.len')).prop('disabled', true);
                $($($(this).closest("tr")).find('td').find('.prec')).prop('disabled', true);
                $($($(this).closest("tr")).find('td').find('.len')).val('');
                $($($(this).closest("tr")).find('td').find('.prec')).val('');
            }

        });

        $(".columnName").change(function () {
            
            var this_cr = this;
            var txt = $(this).val();
            var cr_id = $(this).attr('cr_id');
            $($(".columnName")).each(function () {
                

                var col_txt = $(this).val();
                var col_cr_id = $(this).attr('cr_id');

                if (txt == col_txt && cr_id != col_cr_id) {
                    swal("", "Column name already exist", "error")
                    $(this_cr).focus();
                }
            });
        });


    }


    function createNewRow() {
        var cr_id = Math.floor(Math.random() * 10000) + 1;

        var str = '<tr class="new_tr">';
        str += ' <td><input type="text" class="form-control columnName" name="columnName" cr_id="' + cr_id + '" value="" onkeypress="return blockSpecialChar(event)"> </td>';
        str += '<td><input type="text" class="form-control descp" value="" cr_id=""' + cr_id + '"" onkeypress="return blockSpecialChar(event)"> </td>';
        str += '<td>';
        str += '<select class="form-control select2" id="selectbasic" cr_id="' + cr_id + '" name="selectbasic">';
        str += '<option value="integer" >integer</option>';
        str += '<option value="bigint">bigint</option>';
        str += '<option value="character varying">character varying</option>';
        str += '<option value="character">character</option>';
        str += '<option value="boolean">boolean</option>';
        str += '<option value="serial">serial</option>';
        str += '<option value="bigserial">bigserial</option>';
        str += '<option value="real">real</option>';
        str += '<option value="numeric">numeric</option>';
        str += '<option value="double precision">double precision</option>';
        str += '<option value="bigserial">bigserial</option>';
        str += '<option value="small int">small int</option>';
        str += '</select>';
        str += '</td>';
        str += '<td><input type="text" class="form-control len" disabled value="" cr_id="' + cr_id + '" onkeypress="return blockSpecialandChar(event)"> </td>';
        str += '<td><input type="text" class="form-control prec" disabled value="" cr_id="' + cr_id + '" onkeypress="return blockSpecialandChar(event)"> </td>';
        str += '<td><input type="checkbox" class="pk" value="" cr_id="' + cr_id + '">  </td>';
        str += ' <td><input type="checkbox" value="" class="checkBoxClass mk" cr_id="' + cr_id + '"> </td>';
        str += '<td><input type="checkbox" value="" class="checkBoxClass uk" cr_id="' + cr_id + '"> </td>';
        str += ' <td><i class="fas fa-trash" style="cursor:pointer"></i></td>';

        str += ' </tr>';
        return str;
    }


}

function blockSpecialChar(e) {
    
    var k = e.keyCode;
    result = ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8);
    if (((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8)) {
        return result;
    }
    else {
        swal("", "Special characters & numbers not allowed", "error")
        return false;
    }
}



function blockSpecialandChar(e) {
    
    var k = e.keyCode;
    result = ((k >= 48 && k <= 57))
    if ((k >= 48 && k <= 57)) {
        return result;
    }
    else {
        swal("", "Only numbers", "error")
        return false;
    }
}


$("#btn_Save").click(function () {
    create();
})
function create() {
    
    var Data = [];
    for (var i = 0; i < $("#grid_columns tbody tr.new_tr").length; i++) {
        var obj = {};
        
        obj["column_id"] = "";
        obj["column_name"] = $(".columnName")[i].value;
        obj["column_description"] = $(".descp")[i].value;
        obj["data_type"] = $(".select2")[i].value;
        obj["data_type_length"] = $(".len")[i].value;
        obj["data_type_precision"] = $(".prec")[i].value;
        obj["primary_key"] = $(".pk")[i].checked;
        obj["mandatory"] = $(".mk")[i].checked;
        obj["unique_key"] = $(".uk")[i].checked;

        // obj["is_identity"] = $("#ident")["0"].checked;
        // obj["Actions"] = $($("#ex tbody tr")[i]).find('td')[3].innerText;
        Data.push(obj);
    }

    
    var ColumnDetails = JSON.stringify(Data);

    $.ajax({
        type: "POST",
        url: '/services/CrudService.svc/CreateTable?table_id=' + $('#table_id').val() + '&table_name=' + $("#tname").val() + '&table_columns=' + ColumnDetails + '&comments=' + $("#comment").val() + '&category=' + $("#categoryvalue").val() + '&mode=' + tableMode + '&createby=user',
        contentType: 'application/json; charset=utf-8',
        async: false,

        success: function (result) {
            
            if (result == "false") {
                swal("", "Please check the details and Try Again!", "error")
            }
            else if (result == "Success" || result == "true") {
                if (tableMode == "Add")
                    swal("", "Table Added Successfully", "success");
                else
                    swal("", "Table Updated Successfully", "success");
                layoutRefresh(window.location.origin + "/Views/Layout.html?TableManager", 0);
                //$('#create').prop("disabled", true);
            }

        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
            //alert('Something Wrong');
        }
    });
}

function tableName() {

    
    if ($("#tname").val() == "") {
        $("#btn_Save").prop("disabled", true)
    }
    else {
        $.ajax({
            type: "GET",
            url: '/services/CrudService.svc/CheckTableName?tablename=' + $("#tname").val(),
            contentType: 'application/json; charset=utf-8',
            async: false,

            success: function (result) {

                
                if (result != "true") {
                    swal("", "Tablename Already Exists", "error")
                    $("#btn_Save").prop("disabled", true)
                }
                else {
                    $("#btn_Save").prop("disabled", false)
                }



            },

            error: function (e) {
                //alert("Something Wrong!!");
                tableName();
            }


        });
    }
    //var fn = $("#tname").val();
    //var regex = /^[a-zA-Z\_]+$/
    //alert(regex.test(fn));
}
//$(document).ready(function () {


//});

$("#btn_back").click(function () {
    back();
});
function back() {
    tableId = '';
    tableMode = '';
    layoutRefresh(window.location.origin + "/Views/Layout.html?TableManager", 0);
}