﻿var IDArr = [];

var PortalNameArr = [];

var TitleArr = [];

var DbConnection_IdArr = [];

var Chart_TypeArr = [];

var Role_IdArr = [];

var PreferencesArr = [];
var getChartType = "";

var DropDownDefaultValues = {};
var DropDownTableValues = {};
DropDownDefaultValues["selectID"] = ""
DropDownDefaultValues["selectPortalName"] = "65"
DropDownDefaultValues["selectTitle"] = "ASSETS"
DropDownDefaultValues["selectDbConnection_Id"] = "2"
DropDownDefaultValues["selectChart_Type"] = "Donut"
DropDownDefaultValues["selectSQL"] = ""
DropDownDefaultValues["selectRow_Number"] = ""
DropDownDefaultValues["selectColumn_Number"] = ""
DropDownDefaultValues["selectLink"] = ""
DropDownDefaultValues["selectHeight"] = ""
DropDownDefaultValues["selectWidth"] = ""
DropDownDefaultValues["selectRole_Id"] = "1"
DropDownDefaultValues["selectPreferences"] = "New"
DropDownDefaultValues["selectCreateBy"] = ""
DropDownDefaultValues["selectCreateDateTime"] = ""
DropDownDefaultValues["selectUpdateBy"] = ""
DropDownDefaultValues["selectUpdateDateTime"] = ""
DropDownDefaultValues["selectStatus"] = ""

$(document).keypress(function (e) {
    if (e.which == 13) {
        $(".swal-button").click();
    }
});
function editFunction(e) {

    //

    $('#editModal').modal('show');
    $(".UpdateValidateInformation").html('');
    var tr = $(e)[0].closest("tr");
    var td = tr.children;
    var columns = $('[name="EditColumns"]');
    for (var i = 0; i < columns.length; i++) {
        if (i == 0) {
            $('[name="EditColumns"]')[i].innerHTML = $('[name="EditColumns"]')[i].value = td[i].innerHTML;
        }
        else if ($('[name="EditColumns"]')[i].localName == "select") {
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined)) {
                $($("[name='EditColumns']")[i]).children("#" + td[i].innerHTML.replace(" ", "")).prop("selected", true);
                //$('[name="EditColumns"]')[i].value = $("#" + td[i].innerHTML.replace(" ", "")).val().replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
            }
        }
        else {
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
                $('[name="EditColumns"]')[i].value = td[i].innerHTML.replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
    }

}

var x = localStorage.getItem("Result");
var role = JSON.parse(x);

if (role.Role_ID != "4" && role.Role_ID != "1") {
    $("#addnew").hide();
}

var row = 0;
var NKPILoad = 0;
var KPILoad = 0;

$("#loader").show();
loadNKPI();
$("#loader").show();
var id = null;
loadD3charts(id);

var flag = 0;

function d3Chart(dataSource) {
    //if (dataSource["data1"] != null && dataSource["data1"] != '' && dataSource["data1"] != "null") {
    //    var d3data = [{

    //        values: JSON.parse(dataSource["data"]),
    //        color: '#f9a65a',
    //        key: "Current",
    //        area: true
    //    }, {
    //        values: JSON.parse(dataSource["data1"]),
    //        color: '#f9a65a',
    //        key: "Previous",
    //        area: true
    //    }
    //    ]
    //} else {
    //    var d3data = [{

    //        values: JSON.parse(dataSource["data"]),
    //        //color: '#f9a65a',
    //        key: "Current",

    //        area: true
    //    }]
    //}
    //dataSource["d3data"] = d3data;

    if (dataSource["Chart_Type"].toLowerCase() == "bar")
        bar_chart(dataSource);
    else if (dataSource["Chart_Type"].toLowerCase() == "line" || dataSource["Chart_Type"].toLowerCase() == "area")
        line_chart(dataSource);
    else if (dataSource["Chart_Type"].toLowerCase() == "pie")
        pie_chart(dataSource);
    else if (dataSource["Chart_Type"].toLowerCase() == "donut")
        donut_chart(dataSource);
    else if (dataSource["Chart_Type"].toLowerCase() == "column")
        column_chart(dataSource)
    else if (dataSource["Chart_Type"].toLowerCase() == "timeseries")
        timeseries_chart(dataSource);
    else if (dataSource["Chart_Type"].toLowerCase() == "cummulativeline")
        cline_chart(dataSource);

}
function timeseries_chart(dataSource) {
    var timeserieschart;
    //if (dataSource["Chart_Type"].toLowerCase() == "line")
        dataSource["d3data"][0]["area"] = false
    nv.addGraph(function () {

        timeserieschart = nv.models.lineChart()
            .x(function (d) {
                return parseInt(d[dataSource["Column_Number"]].substring(6, 19))
                // to show Date propery on X axis
            })
            .y(function (d) {
                return d[dataSource["Row_Number"]]
            })

            .useInteractiveGuideline(true)
            //.tooltips(false);
            .showLegend(false)
            .showYAxis(true)
            .showXAxis(true)
        timeserieschart.xAxis

            .axisLabel(dataSource["Column_Number"])

        timeserieschart.yAxis

            .axisLabel(dataSource["Row_Number"])
        timeserieschart.xAxis
            .showMaxMin(false)
            .ticks(4)
            .tickFormat(function (d) {
                return d3.time.format('%d/%m/%y')(new Date(d))
            });

        timeserieschart.yAxis;

        timeserieschart.lines.dispatch.on("elementClick", function (e) {
            $('.nvtooltip').remove();
        });

        d3.select('#' + dataSource["ID"]).datum(dataSource["d3data"]).call(timeserieschart);
        nv.utils.windowResize(function () {
            d3.select('#' + dataSource["ID"]).call(timeserieschart)
        });
        return timeserieschart;
    });

}
function column_chart(dataSource) {

    nv.addGraph(function () {
        var columnchart = nv.models.discreteBarChart()
            .x(function (d) { return d[dataSource["Column_Number"]] })
            .y(function (d) { return d[dataSource["Row_Number"]] })
            .staggerLabels(true)
            //.tooltips(false)
            .showLegend(true)
            .showYAxis(true)
            .showXAxis(true)
        columnchart.xAxis
            .axisLabel(dataSource["Column_Number"])

        columnchart.yAxis
            .axisLabel(dataSource["Row_Number"])
            .showValues(true);
        columnchart.discretebar.dispatch.on("elementClick", function (e) {
            $('.nvtooltip').remove();
            if (dataSource["Link"] != null && dataSource["Link"] != "" && dataSource["Link"] != "null")
                layoutRefresh(window.location.origin + dataSource["Link"], 0);
        });
        d3.select('#' + dataSource["ID"])
            .datum(dataSource["d3data"])
            .call(columnchart)

        nv.utils.windowResize(function () {
            d3.select('#' + dataSource["ID"]).call(columnchart)
        });

        return columnchart;
    });
}
function pie_chart(dataSource) {

    nv.addGraph(function () {

        var piechart = nv.models.pieChart()
            .x(function (d) { return d[dataSource["Column_Number"]] })
            .y(function (d) { return d[dataSource["Row_Number"]] })
            //.margin({ top: 10, right: 10, bottom: 10, left: 10 })
            .labelThreshold(.05)
            .labelType("percent")
        //.tooltips(false);
        piechart.pie.dispatch.on("elementClick", function (e) {
            $('.nvtooltip').remove();
            if (dataSource["Link"] != null && dataSource["Link"] != "" && dataSource["Link"] != "null")
                layoutRefresh(window.location.origin + dataSource["Link"], 0);

        });
        d3.select('#' + dataSource["ID"])
            .datum(JSON.parse(dataSource["data"]))
            .transition().duration(1200)
            .call(piechart);
        return piechart;
    });
}
function donut_chart(dataSource) {

    nv.addGraph(function () {

        var donutchart = nv.models.pieChart()
            .x(function (d) { return d[dataSource["Column_Number"]] })
            .y(function (d) { return d[dataSource["Row_Number"]] })
            .showLabels(true)
            .labelThreshold(.05)
            .labelType("percent")
            .donut(true)
            .donutRatio(0.35)
        //.tooltips(false);
        d3.select('#' + dataSource["ID"])
            .datum(JSON.parse(dataSource["data"]))
            .transition().duration(1200)
            .call(donutchart);
        donutchart.pie.dispatch.on("elementClick", function (e) {
            $('.nvtooltip').remove();
            if (dataSource["Link"] != null && dataSource["Link"] != "" && dataSource["Link"] != "null")
                layoutRefresh(window.location.origin + dataSource["Link"], 0);

        });
        return donutchart;
    });
}

var dindex = -1;
var ddict = {};
function line_chart(dataSource) {
    var linechart;
    if (dataSource["Chart_Type"].toLowerCase() == "line")
        dataSource["d3data"][0]["area"] = false
    nv.addGraph(function () {

        linechart = nv.models.lineChart()

            .x(function (d) {
                
                if (dataSource["d3data"][0]["values"].length - 1 > dindex) {
                    dindex++;
                    ddict[d[dataSource["Column_Number"]]] = dindex;

                    return dindex
                }
                else {
                    dindex = 0;
                    return ddict[d[dataSource["Column_Number"]]];
                }
                // to show Date propery on X axis
            })
            .y(function (d) {
                return d[dataSource["Row_Number"]]
            })
            //.tooltips(false);
            .showLegend(true)
            .showYAxis(true)
            .showXAxis(true)
        linechart.xAxis
            .axisLabel(dataSource["Column_Number"])

        linechart.yAxis
            .axisLabel(dataSource["Row_Number"])
        linechart.xAxis
            .showMaxMin(false)

            .tickFormat(function (d) {

                return dataSource["d3data"][0]["values"][d][dataSource["Column_Number"]]
            });
        linechart.yAxis;

        linechart.lines.dispatch.on("elementClick", function (e) {
            $('.nvtooltip').remove();
            if (dataSource["Link"] != null && dataSource["Link"] != "" && dataSource["Link"] != "null")
                layoutRefresh(window.location.origin + dataSource["Link"], 0);

        });

        d3.select('#' + dataSource["ID"]).datum(dataSource["d3data"]).call(linechart);
        nv.utils.windowResize(function () {
            d3.select('#' + dataSource["ID"]).call(linechart)
        });
        return linechart;
    });

}
function bar_chart(dataSource) {

    var barchart;
    nv.addGraph(function () {

        var barchart = nv.models.multiBarChart()
            .x(function (d) {
                return d[dataSource["Column_Number"]] // to show Date propery on X axis
            })
            .y(function (d) {
                return d[dataSource["Row_Number"]]
            })
            .duration(300)
            .margin({ bottom: 100, left: 70 })
            .rotateLabels(0)
            //.tooltips(false)
            .groupSpacing(0.1)
            .showLegend(false)
            .showYAxis(true)
            .showXAxis(true)
        barchart.xAxis
            .axisLabel(dataSource["Column_Number"])

        barchart.yAxis
            .axisLabel(dataSource["Row_Number"])

        barchart.reduceXTicks(false).staggerLabels(true);
        barchart.xAxis;
        //barchart.tooltip.hidden(true)
        barchart.yAxis;

        barchart.multibar.dispatch.on("elementClick", function (e) {
            $('.nvtooltip').remove();
            barchart.tooltip(false)
            if (dataSource["Link"] != null && dataSource["Link"] != "" && dataSource["Link"] != "null")
                layoutRefresh(window.location.origin + dataSource["Link"], 0);

        });

        d3.select('#' + dataSource["ID"]).datum(dataSource["d3data"]).call(barchart);
        d3.select(".nv-controlsWrap").style("visibility", "hidden");
        nv.utils.windowResize(function () {
            d3.select('#' + dataSource["ID"]).call(barchart)
        });
        return barchart;
    });
}
function cline_chart(dataSource) {
    var clinechart;
    //dataSource["d3data"] = [{

    //    values: [{ Day: 1,Amount: 1669570.58 },
    //        { Day: 2,Amount: 2136534.16 },
    //        { Day: 3,Amount: 2867680.00 },
    //        { Day: 4,Amount: 5133904.99 },
    //        { Day: 5,Amount: 6081749.73 },
    //        { Day: 6,Amount: 8111824.15 },
    //        { Day: 7,Amount: 8652471.65 },
    //        { Day: 8,Amount: 9555881.00 },
    //        { Day: 9,Amount: 11390436.43 },
    //        { Day: 10,Amount: 13085432.45 },
    //        { Day: 11,Amount: 13141520.45 },
    //        { Day: 12,Amount: 14518550.2 },
    //        { Day: 13,Amount: 15500440.24 },
    //        { Day: 14,Amount: 16816714.86 },
    //        { Day: 15,Amount: 18242469.13 },
    //        { Day: 16,Amount: 19723192.29 },
    //        { Day: 17,Amount: 19748022.29 },
    //        { Day: 18,Amount: 20546637.60 },
    //        { Day: 19,Amount: 21650829.73 },
    //        { Day: 20,Amount: 22311486.67 },
    //        { Day: 21,Amount: 23843292.85 },
    //        { Day: 22,Amount: 25724033.48 },
    //        { Day: 23,Amount: 26692222.87 },
    //        { Day: 24,Amount: 28115288.97 },
            
    //    ],
    //    color: '#FA7704',
    //    key: "Apr",
    //    area: false
    //},
    //    {
    //        values: [{ Day: 1,Amount: 9887682.89 },
    //            { Day: 2,Amount: 11984150.20 },
    //            { Day: 3,Amount: 13724361.70 },
    //            { Day: 4,Amount: 13764832.90 },
    //            { Day: 5,Amount: 15099475.26 },
    //            { Day: 6,Amount: 16065379.58 },
    //            { Day: 7,Amount: 17707002.21 },
    //            { Day: 8,Amount: 20512604.77 },
    //            { Day: 9,Amount: 21394097.03 },
    //            { Day: 10,Amount: 23174143.28 },
    //            { Day: 11,Amount: 24046183.91 },
    //            { Day: 12,Amount: 25851241.20 },
    //            { Day: 13,Amount: 27097988.31 },
    //            { Day: 14,Amount: 28999837.03 },
    //            { Day: 15,Amount: 29048653.03 },
    //            { Day: 16,Amount: 29115962.56 },
    //            { Day: 17,Amount: 31200492.10 },
    //            { Day: 18,Amount: 33501025.91 },
    //            { Day: 19,Amount: 34789438.31 },
    //            { Day: 20,Amount: 35943971.48 },
    //            { Day: 21,Amount: 37086454.29 },
    //            { Day: 22,Amount: 37111996.29 },
    //            { Day: 23,Amount: 38284433.99 },
    //            { Day: 24,Amount: 39272459.48 },
    //            { Day: 25,Amount: 39663806.93 },
    //            { Day: 26,Amount: 40458686.76 },
               
    //    ],
    //        color: '#3FFF33',
    //    key: "May",
    //    area: false
    //    },
    //    {
    //        values: [{ Day: 1,Amount: 3910075.58 },
    //            { Day: 2,Amount: 5191560.84 },
    //            { Day: 3,Amount: 6721334.88 },
    //            { Day: 4,Amount: 8399535.96 },
    //            { Day: 5,Amount: 9858717.76 },
    //            { Day: 6,Amount: 10940896.75 },
    //            { Day: 7,Amount: 10958300.24 },
    //            { Day: 8,Amount: 11345083.37 },
    //            { Day: 9,Amount: 13206939.07 },
    //            { Day: 10,Amount: 14097203.65 },
    //            { Day: 11,Amount: 15810900.40 },
    //            { Day: 12,Amount: 18156241.53 },
    //            { Day: 13,Amount: 19734610.29 },
    //            { Day: 14,Amount: 20052269.15 },
    //            { Day: 15,Amount: 22095362.39 },
    //            { Day: 16,Amount: 23314888.46 },
    //            { Day: 17,Amount: 24346678.72 },
    //            { Day: 18,Amount: 26660572.31 },
    //            { Day: 19,Amount: 28376298.95 },
    //            { Day: 20,Amount: 31120186.96 },
    //            { Day: 21,Amount: 32666801.79 },
    //            { Day: 22,Amount: 34177287.43 },
    //            { Day: 23,Amount: 35249093.56 },
    //            { Day: 24,Amount: 36964914.83 },
    //            { Day: 25,Amount: 36988005.10 }
    //            ],

    //        color: '#047FFA',
    //        key: "Jun",
    //        area: false
    //    }, {
    //    values: [{ Day: 1,Amount: 2792190.64 },
    //    { Day: 2,Amount: 4081844.03 },
    //    { Day: 3,Amount: 6721334.88 },
    //    { Day: 4,Amount: 5776783.01 },
    //    { Day: 5,Amount: 6355333.33 },
    //    { Day: 6,Amount: 7912936.93 },
    //    { Day: 7,Amount: 7982919.73 },
    //    { Day: 8,Amount: 14234230.25 },
    //    { Day: 9,Amount: 15545549.17 },
    //    { Day: 10,Amount: 16977694.59 },
    //    { Day: 11,Amount: 18937374.25 },
    //    { Day: 12,Amount: 21428195.06 },
    //    { Day: 13,Amount: 23127465.88 },
    //    { Day: 14,Amount: 25029390.67 },
    //    { Day: 15,Amount: 29809628.00 },
    //    { Day: 16,Amount: 32894066.99 },
    //        { Day: 17,Amount: 34318267.79 },
    //        { Day: 18,Amount: 34350006.86 },
    //    { Day: 19,Amount: 36423163.37 },
    //    { Day: 20,Amount: 37809057.66 },
    //    { Day: 21,Amount: 38425755.54 },
    //    { Day: 22,Amount: 40959387.79 },
    //    { Day: 23,Amount: 42601309.29 },
    //    { Day: 24,Amount: 42886382.19 },
    //        { Day: 25,Amount: 43158288.34 },
    //        { Day: 26,Amount: 43994817.96 },
    //        { Day: 27,Amount: 45470292.12 },
    //        { Day: 28,Amount: 46590732.41 },
    //    ],
    //        color: '#FA042D',
    //    key: "Jul",
    //    area: false
    //    }, {
    //    values: [{ Day: 1,Amount: 2067225.55 },
    //    { Day: 2,Amount: 3414076.86 },
    //    { Day: 3,Amount: 7907319.80 },
    //    { Day: 4,Amount: 9327587.83 },
    //    { Day: 5,Amount: 9884112.36 },
    //    { Day: 6,Amount: 10578520.01 },
    //    { Day: 7,Amount: 11092702.49 },
    //    { Day: 8,Amount: 11389725.67 },
    //    { Day: 9,Amount: 12296734.66 },
       
    //    ],
    //        color: '#6F1A7B',
    //    key: "Aug",
    //    area: false,
    //            },
    //];
    nv.addGraph(function () {
        var clinechart = nv.models.lineChart()
            .x(function (d) {
                return d[dataSource["Column_Number"]] // to show Date propery on X axis
            })
            .y(function (d) {
                return d[dataSource["Row_Number"]]
            }) 
            .showLegend(true)
            .showYAxis(true)
            .showXAxis(true)
        clinechart.xAxis
            .axisLabel("Day of the Month")
            .ticks(15)
            .tickSize(30, 0)
        clinechart.yAxis
            .axisLabel("Revenue(s)")
            .tickFormat(d3.format("$,r"))

        clinechart.xAxis
            .showMaxMin(false)
           
        clinechart.yAxis;

        clinechart.lines.dispatch.on("elementClick", function (e) {
            $('.nvtooltip').remove();
            if (dataSource["Link"] != null && dataSource["Link"] != "" && dataSource["Link"] != "null")
                layoutRefresh(window.location.origin + dataSource["Link"], 0);

        });

        d3.select('#' + dataSource["ID"]).datum(dataSource["d3data"]).call(clinechart);
        d3.select(".nv-controlsWrap").style("visibility", "hidden");
        nv.utils.windowResize(function () {
            d3.select('#' + dataSource["ID"]).call(clinechart)
        });
        return clinechart;
    });
}

function editDisplayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        async: false,
        success: function (val) {

            var value = $('[name="EditColumns"]')[i].value;
            var select = $('[name="EditColumns"]')[i];
            $('[name="EditColumns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            select = $('[name="EditColumns"]')[i];


            for (var j = 0; j < dropdownlist.length; j++) {
                var opt = document.createElement('option');
                if (key.length == 2) {
                    opt.text = dropdownlist[j][key[1]];
                    opt.id = dropdownlist[j][key[1]].replace(" ", "");
                    opt.value = dropdownlist[j][key[0]];
                    DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[1]];
                }
                else {
                    opt.text = opt.value = dropdownlist[j][key[0]];
                    opt.id = dropdownlist[j][key[0]].replace(" ", "");
                    DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[0]];
                }
                select.add(opt, j);
            }
            $('[name="EditColumns"]')[i].value = value;
        },
        error: function (e) {
            //console.log("Something Wrong.");
        }
    });
}
var removeChartID;
var TimeseriesChartID;
function editsettingsFunction(e) {
    $("#QueryValidateUpdateButton")[0].disabled = true;
    $("#UpdateBtn")[0].disabled = false;
    removeChartID = $($(e).parent().parent().parent().children()[1]).children().children()[0].id;
    TimeseriesChartID = $($(e).parent().parent().parent().parent().parent())[0].className;
    //$("#chart_view")[0].innerHTML += '<div class="col-sm-12"><div class="col-sm-5"></div><div class="col-sm-1"><img alt="" src="dist/img/ring_loader_monitor.gif" class="" style="position:fixed;text-align:center;z-index:1000" /></div></div>';
    for (i = 0; i < $(".MonitorIds").length; i++)
        if ($(".MonitorIds")[i].innerHTML == e.id)
            $($(".MonitorIds")[i]).parent().parent().find('.btn-warning').click()


}
function displayDropDownList(i, elementId, modalId) {
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {


            var select = $('#' + modalId + ' [name="Columns"]')[i];
            $('#' + modalId + ' [name="Columns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            select = $('#' + modalId + ' [name="Columns"]')[i];

            var opt1 = '<option selected disabled hidden style=\"display: none\" value=\"1\"></option>';
            $('#' + modalId + ' [name="Columns"]')[i].append(opt1);
            var key1 = Object.keys(DropDownDefaultValues);
            for (var j = 0; j < dropdownlist.length; j++) {

                //  var key = Object.keys(DropDownTableValues[0]);
                var opt = document.createElement('option');
                if (key.length == 2) {
                    opt.text = dropdownlist[j][key[1]];
                    opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('#' + modalId + ' [name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                else {
                    opt.text = opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('#' + modalId + ' [name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                select.add(opt, j);
            }
            var opt = document.createElement('option');
            opt.text = "Select Option";
            opt.selected = true;
            opt.value = "";
            select.add(opt, 0);
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}


gridLoad();
function gridLoad() {
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/getTableData?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML,
        contentType: 'application/json; charset=utf-8',
        async: false,
        success: function (val) {
            //
            var dataSource = $.parseJSON(val);
            var columns = $('[name="EditColumns"]');
            for (i = 0; i < columns.length; i++) {
                if (columns[i].type == "select-one") {
                    var ID = columns[i].id;
                    var ElementDetails = ID.split("|");
                    var elementId = ElementDetails[1];
                    editDisplayDropDownList(i, elementId);

                    for (j = 0; j < dataSource.length; j++) {
                        dataSource[j][ElementDetails[0]] = DropDownTableValues[dataSource[j][ElementDetails[0]]];
                    }
                }
            }
            var t = $('#grid').DataTable({
                "paging": false,
                destroy: true,
                "scrollY": true,
                "scrollX": true,
                "columnDefs": [{
                    "targets": '_all',
                    "defaultContent": "",
                },
                ],

            });
            t.clear();
            for (var i in dataSource) {
                t.row.add([
                    "<span class='col-md-12 MonitorIds' style='padding:0%;width:100%;cursor:pointer;'>" + dataSource[i]["ID"] + "</span>",
                    dataSource[i]["PortalName"],
                    dataSource[i]["DbConnection_Id"],
                    dataSource[i]["SQL"],
                    dataSource[i]["Title"],
                    dataSource[i]["Chart_Type"],
                    dataSource[i]["Preferences"],
                    dataSource[i]["Column_Number"],
                    dataSource[i]["Row_Number"],
                    dataSource[i]["Height"],
                    dataSource[i]["Width"],
                    dataSource[i]["Role_Id"],
                    "<button class='btn btn-warning btn-flat' title='Edit' onclick='editFunction(this)'><i class='fa fa-edit' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Edit</span></button>&nbsp;<button class='deleteBtnClass btn btn-flat' title='Delete' onclick='deleteFunction(this)'><i class='fa fa-trash' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Delete</span></button>"
                ]);
                t.draw(false);

                t.page('first').draw('page');
            }

        },
        //},
        error: function (e) {
            //console.log("Grid not loaded!!");
            gridLoad();
        }
    });
}
$(document).ready(function () {
    $("#SubmitForm1").on("submit", function (e) {
        e.preventDefault();
        var KPI_Id = null;
        $('.selectPortalName option:selected').text('Monitor');
        var Values = {
        }
        var Columns = $('#addModal1 [name="Columns"]');
        var len = $('#addModal1 [name="Columns"]').length;
        for (var i = 1; i < len; i++) {
            var ID = $('#addModal1 [name="Columns"]')[i].id;
            var columnName = ID.split("|");
            if (columnName[0].toUpperCase() == "CREATEBY") {
                Values[columnName[0]] = window.btoa(JSON.parse(localStorage.Result).User_ID);
            }
            else if (columnName[0].toUpperCase() == "CREATEDATETIME") {
                Values[columnName[0]] = window.btoa(GetNow());
            }
            else if (columnName[0].toUpperCase() == "UPDATEBY") {
                Values[columnName[0]] = window.btoa(JSON.parse(localStorage.Result).User_ID);
            }
            else if (columnName[0].toUpperCase() == "UPDATEDATETIME") {
                Values[columnName[0]] = window.btoa(GetNow());
            }
            else if (columnName[0].toUpperCase() == "PORTALNAME") {
                Values[columnName[0]] = window.btoa("OBDN Dashboard");
            }
            else if (columnName[0].toUpperCase() == "STATUS")
                Values[columnName[0]] = window.btoa(true);
            else {
                if ($('#addModal1 [name="Columns"]')[i].type == "checkbox")
                    Values[columnName[0]] = window.btoa($('#addModal1 [name="Columns"]')[i].checked);
                else
                    Values[columnName[0]] = window.btoa($('#addModal1 [name="Columns"]')[i].value);
            }
        }

        var values = JSON.stringify(Values);
        $.ajax({
            type: "POST",
            url: '/services/CrudService.svc/createTableForLob',
            data: JSON.stringify({
                "tablename": document.getElementsByTagName("tablename")[0].innerHTML,
                "Values": values
            }),
            contentType: 'application/json; charset=utf-8',
            success: function (val) {
                
                if (val.createTableForLobResult == "true") {
                    swal("", "Record Added Successfully", "success");
                }
                else if (val.createTableForLobResult == "false") {
                    swal("", "KPI title name already exists", "error")

                }
                $("#addModal1 .close").click();
                gridLoad();
                loadD3charts(KPI_Id);
            },
            error: function (e) {

            }
        });
    });
    $("#SubmitForm2").on("submit", function (e) {
        e.preventDefault();
        var Values = {
        }
        var Columns = $('#addModal2 [name="Columns"]');
        var len = $('#addModal2 [name="Columns"]').length;
        for (var i = 1; i < len; i++) {
            var ID = $('#addModal2 [name="Columns"]')[i].id;
            var columnName = ID.split("|");
            if (columnName[0].toUpperCase() == "CREATEBY") {
                Values[columnName[0]] = window.btoa(JSON.parse(localStorage.Result).User_ID);
            }
            else if (columnName[0].toUpperCase() == "CREATEDATETIME") {
                Values[columnName[0]] = window.btoa(GetNow());
            }
            else if (columnName[0].toUpperCase() == "PORTALNAME") {
                Values[columnName[0]] = window.btoa("OBDN Dashboard");
            }
            else if (columnName[0].toUpperCase() == "STATUS")
                Values[columnName[0]] = window.btoa(true);
            else {
                if ($('#addModal2 [name="Columns"]')[i].type == "checkbox")
                    Values[columnName[0]] = window.btoa($('#addModal2 [name="Columns"]')[i].checked);
                else
                    Values[columnName[0]] = window.btoa($('#addModal2 [name="Columns"]')[i].value);
            }
        }

        var values = JSON.stringify(Values);
        $.ajax({
            type: "POST",
            url: '/services/CrudService.svc/createTableForLob',
            data: JSON.stringify({
                "tablename": 'NKPI',
                "Values": values
            }),
            contentType: 'application/json; charset=utf-8',
            success: function (val) {
                if (val.createTableForLobResult == "true") {
                    swal("", "Record Added Successfully", "success")
                }
                else if (val.createTableForLobResult == "false") {
                    swal("", "NKPI title name already exists", "error")

                }
                $("#addModal2 .close").click();
                gridLoad();
                loadNKPI();
            },
            error: function (e) {

            }
        });
    });
    $("#SubmitForm3").on("submit", function (e) {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: '/services/CrudService.svc/cMonitor?screenName=' + $("#monitorName").val() + '&role=' + $("#monitorRole").val(),
            async: true,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $("#addModal3").modal('hide');
                swal("", "Monitor Added Successfully", "success")
                getMenuItems();
            },
            error: function (e) {

            }
        });

    });
    $("#UpdateForm").on("submit", function (e) {
        e.preventDefault();
        var IDName = $('[name="EditColumns"]')[0].id.split("|");
        var ID = {};
        ID[IDName[0]] = $($('[name="EditColumns"]')[0].value)[0].innerHTML;
        chart_Id = $($('[name="EditColumns"]')[0].value)[0].innerHTML;
        columns = $('[name="EditColumns"]');
        var Values = {
            "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
            //  "UpdateDatetime": window.btoa(GetNow())
        }
        for (var i = 1; i < columns.length; i++) {
            var z = $('[name="EditColumns"]')[i].id.split("|");
            if ($('[name="EditColumns"]')[i].type == "checkbox") {
                Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].checked);
            }
            else {
                if (z[0] == "PortalName") {
                    Values[z[0]] = window.btoa('OBDN Dashboard');
                } else {
                    Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].value);
                }
            }
        }
        var values = JSON.stringify(Values);
        var Id = JSON.stringify(ID);

        $.ajax({
            type: "POST",
            url: '/services/CrudService.svc/updateTableRow',
            data: JSON.stringify({
                'tablename': document.getElementsByTagName("tablename")[0].innerHTML,
                'Values': values,
                'ID': Id
            }),
            async: false,
            contentType: 'application/json; charset=utf-8',
            success: function (val) {
                $("#editModal").modal('hide');
                gridLoad();
                $("#loader").show();
                loadUpdateCharts(chart_Id);
            },
            error: function (e) {
                gridLoad();
                $("#loader").show();
                loadUpdateCharts(chart_Id);
            }
        });
    });

    $("#CompareCheck").change(function () {

        if ($('#CompareCheck').prop('checked') == true) {
            $("#CompareFilterType").prop("disabled", false);
            $("#CompareFilterType").css("background-color", "white");
        }
        else if ($('#CompareCheck').attr('unchecked', true)) {
            $("#minFilter1").val("");
            $("#maxFilter1").val("");
            $("#minFilter1").prop("disabled", true);
            $("#maxFilter1").prop("disabled", true);
            $("#CompareFilterType").prop("disabled", true);
            $("#CompareFilterType").css("background-color", "#EBEBE4");
        }
    });

    $(function () {
        var dtToday = new Date();

        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();

        if (month < 10)
            month = '0' + month.toString();
        if (day < 10)
            day = '0' + day.toString();

        var maxDate = year + '-' + month + '-' + day;
        $('#minFilter').attr('max', maxDate);
        $('#maxFilter').attr('max', maxDate);
        $('#minFilter1').attr('max', maxDate);
        $('#maxFilter1').attr('max', maxDate);
    });

    var x = localStorage.getItem("Result");
    var UserDetails = JSON.parse(x);
    $('#addModal1').on('show.bs.modal', function () {
        $(".ValidateInformation").html('');
        columns = $('#addModal1 [name="Columns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                displayDropDownList(i, elementId, "addModal1")
            }
        }
    })
    $('#addModal2').on('show.bs.modal', function () {
        $(".ValidateInformation").html('');
        columns = $('#addModal2 [name="Columns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                displayDropDownList(i, elementId, "addModal2")
            }
        }
    })
    $('#editModal').on('show.bs.modal', function () {

        columns = $('[name="EditColumns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                editDisplayDropDownList(i, elementId);
            }
        }
    })


});
var h = 0;

function loadNKPI() {
    var x = localStorage.getItem("Result");
    var role = JSON.parse(x);
    var filters = {};
    filters["type"] = $("#FilterType").val();
    filters["minDate"] = $("#minFilter").val();
    filters["maxDate"] = $("#maxFilter").val();
    filters["compareType"] = $("#CompareFilterType").val();
    if (filters["type"] == "Custom") {

        if (filters["minDate"] == "" && filters["maxDate"] == "") {
            $("#Applybutton")[0].disabled = true;
        }
    }
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/getKPI?roleid=' + role.Role_ID + '&monitor=OBDN Dashboard&filters=' + JSON.stringify(filters) + '&KPIid=null',
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            console.log(result);
            var colorCSS = ["amber_grad", "red_grad", "green_grad", "cyan_grad"];
            var dataSource = $.parseJSON(result);
            var temp = [];
            h = 0;
            var j;
            $("#KPI").html("");
            $("#loader").hide();

            for (j = 0; j < dataSource.length; j++) {

                row++;

                var templateContent = "";

                var data = JSON.parse(dataSource[j]["data"]);

                for (k = 0; k < data.length; k++) {

                    data[k]["colorClass"] = colorCSS[h];
                    if (h < colorCSS.length - 1) {
                        h++;
                    }
                    else {
                        h = 0;
                    }
                    var kpiDiv = "<div class='col-sm-3'><div class='small-cardbox " + data[k].colorClass + "'><h4 class='some-text'>" + data[k].category + "</h4><span class='no'>" + data[k].value + "</span><button class='details btn-link show' onclick='" + dataSource[j].Group + "'><i class='fa fa-arrow-right'></i> View Details</button>";

                    templateContent += kpiDiv + "</div></div>";
                }

                $("#KPI").append(templateContent);

            }
        },


        error: function (e) {
            //alert("Something Wrong!!");
        }
    });
}
var GlobalDataSource;
function loadD3charts(KPI_Id) {
    if (KPI_Id != "" && KPI_Id != null && KPI_Id != "null") {
        loadUpdateCharts();
    }
    else {
        var filters = {};

        filters["type"] = $("#FilterType").val();
        filters["minDate"] = $("#minFilter").val();
        filters["maxDate"] = $("#maxFilter").val();
        filters["compareType"] = $("#CompareFilterType").val();
        filters["minCompare"] = $("#minFilter1").val();
        filters["maxCompare"] = $("#maxFilter1").val();
        filters["compareCheck"] = $('#CompareCheck')[0].checked;
        if (filters["type"] == "Custom") {
            if (filters["minDate"] == "" && filters["maxDate"] == "") {
                $("#Applybutton")[0].disabled = true;
            }
        }
        if (getChartType != "") {
            filters["timeSeriesType"] = getChartType;
        }
        else {
            filters["timeSeriesType"] = "hour"
        }
        $.ajax({
            type: "GET",
            url: '/services/CrudService.svc/getCharts?roleid=' + role.Role_ID + "&monitor=OBDN Dashboard&filters=" + JSON.stringify(filters) + "&KPIid=" + KPI_Id + "&type=" + $("#chartfiltertype").val(),
            async: false,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                console.log(result);
                GlobalDataSource = $.parseJSON(result);
                var temp = [];
                var j;
                $("#chart_view").html("");
                KPILoad = 1;
                //if (KPILoad != 0 && NKPILoad != 0)
                $("#loader").hide();
                for (j = 0; j < GlobalDataSource.length; j++) {
                    if (GlobalDataSource[j]["data"] != "") {
                        if (GlobalDataSource[j]["Chart_Type"] == "timeseries") {
                            var chartTemp = "<div class= '" + GlobalDataSource[j]["ID"] + "_user-card' style='padding-right:5px; display:inline-table;margin-bottom:10px !important; float:left; padding-left:5px;height:" + GlobalDataSource[j]["Height"] + "px; width:" + GlobalDataSource[j]["Width"] + "%'><div class='box box-primary'><div class='box-header with-border'><h5 class='box-title'>" + GlobalDataSource[j]["Title"] + "</h5><div class='col-sm-9' style='float:right'><div class='btn-group' role='group' aria-label='...' style='float:right'><button id='hour' class='btn btn-select ChartButton' type='button' onclick='loadTimeCharts(this)'>Hourly</button><button id='day' class='btn btn-select' type='button' onclick='loadTimeCharts(this)'>Daily</button><button id='week' class='btn btn-select' type='button' onclick='loadTimeCharts(this)'>Weekly</button><button id='month' class='btn btn-select' type='button' onclick='loadTimeCharts(this)'>Monthly</button><button id='year' class='btn btn-select' type='button' onclick='loadTimeCharts(this)'>Yearly</button></div></div><div id='dropdown' class='box-tools pull-right'></div></div><div class='box-body' style='height:90%'><center><svg id=" + GlobalDataSource[j]["ID"] + " style='width:97% !important;height:" + GlobalDataSource[j]["Height"] + "px !important'></svg></center><center><div id='wrongmsg'> <h3 id='WrongMsg" + GlobalDataSource[j]["ID"] + "' style='color:#B71C1C'></h3></div></center></div></div> </div>"

                            if (j % 2 == 0 && j != 0) GlobalDataSource[j].pr = 12;
                            else GlobalDataSource[j].pr = 6;
                            $("#chart_view").append(chartTemp);
                            if (GlobalDataSource[j]["data"] != "false" && GlobalDataSource[j]["data"] != "Connection Failure") {
                                d3Chart(GlobalDataSource[j]);
                                var x = localStorage.getItem("Result");
                                var UserDetails1 = JSON.parse(x);
                                if (UserDetails1.Role_ID != "1") {
                                    $(".ChartEditButton").hide();
                                }
                            }

                            else {
                                $("#WrongMsg" + GlobalDataSource[j]["ID"]).css("display", "block");
                                $("#WrongMsg" + GlobalDataSource[j]["ID"])[0].innerHTML = "Sorry! Something went wrong. Please check the KPI details and try again";

                            }
                        }
                        else {
                            if (j % 2 == 0 && j != 0) GlobalDataSource[j].pr = 12;
                            else GlobalDataSource[j].pr = 6;


                            var chartTemp = "<div class= '" + GlobalDataSource[j]["ID"] + "_user-card' style='padding-right:5px; display:inline-table;margin-bottom:10px !important; float:left; padding-left:5px;height:" + GlobalDataSource[j]["Height"] + "px; width:" + GlobalDataSource[j]["Width"] + "%'><div class='box box-primary'><div class='box-header with-border'><h5 class='box-title'>" + GlobalDataSource[j]["Title"] + "</h5><div id='dropdown' class='box-tools pull-right'><button type='button' id=" + GlobalDataSource[j]["KPI_ID"] + " class='ChartEditButton' onclick='editsettingsFunction(this)' style='background-color: transparent !important'><i class='fa fa-cog graph-img-size'></i></button><button type='button' id='bar' class='UserChartEnable' style='background-color: transparent !important' onclick='changeChartType(this)'><img src='dist/img/chart icons/bars-chart.png' class='graph-img-size'></button><button type='button' id='line' class='UserChartEnable' style='background-color: transparent !important' onclick='changeChartType(this)'><img src='dist/img/chart icons/profit.png' class='graph-img-size'></button><button type='button' id='area' class='UserChartEnable' style='background-color: transparent !important' onclick='changeChartType(this)'><img src='dist/img/chart icons/area-chart.png' class='graph-img-size'></button><button type='button' id='pie' class='UserChartEnable' style='background-color: transparent !important' onclick='changeChartType(this)'><img src='dist/img/chart icons/pie-chart.png' class='graph-img-size'></button><button type='button' id='donut' class='UserChartEnable' style='background-color: transparent !important' onclick='changeChartType(this)'><img src='dist/img/chart icons/dot-chart.png' class='graph-img-size'></button></div></div><div class='box-body' style='height:90%'><center><svg id=" + GlobalDataSource[j]["ID"] + " style='width:97% !important;height:" + GlobalDataSource[j]["Height"] + "px !important'></svg></center><center><div id='wrongmsg'> <h3 id='WrongMsg" + GlobalDataSource[j]["ID"] + "' style='color:#B71C1C;display:none'></h3></div></center></div></div> </div>"

                            $("#chart_view").append(chartTemp);
                            var x = localStorage.getItem("Result");
                            var UserDetails1 = JSON.parse(x);
                            if (UserDetails1.Role_ID != "1") {
                                $(".ChartEditButton").hide();
                            }
                            else if (UserDetails1.Role_ID == "1") {
                                $(".UserChartEnable").hide();
                            }

                            if (GlobalDataSource[j]["data"] != "false" && GlobalDataSource[j]["data"] != "Connection Failure") {
                                d3Chart(GlobalDataSource[j]);
                            }
                            else {
                                $("#WrongMsg" + GlobalDataSource[j]["ID"]).css("display", "block");
                                $("#WrongMsg" + GlobalDataSource[j]["ID"])[0].innerHTML = "Sorry! Something went wrong. Please check the KPI details and try again";
                            }
                        }
                    }
                    else {
                        var chartTemp = "<div class= 'error_user-card' style='padding-right:5px; display:inline-table;margin-bottom:10px !important; float:left; padding-left:5px;height: 300px; width:30%'><div class='box box-primary'><div class='box-header with-border'><h5 class='box-title'>Something Has Occured at Charts</h5><div class='col-sm-9' style='float:right'><div class='btn-group' role='group' aria-label='...' style='float:right'><button id='hour' class='btn btn-primary' type='button' onclick='loadTimeCharts(this)'>Hourly</button><button id='day' class='btn btn-success' type='button' onclick='loadTimeCharts(this)'>Daily</button><button id='week' class='btn btn-info' type='button' onclick='loadTimeCharts(this)'>Weekly</button><button id='month' class='btn btn-danger' type='button' onclick='loadTimeCharts(this)'>Monthly</button><button id='year' class='btn btn-primary' type='button' onclick='loadTimeCharts(this)'>Yearly</button><button type='button' id='kpi_id' class='ChartEditButton' onclick='editsettingsFunction(this)' style='background-color: transparent !important'><i class='fa fa-cog graph-img-size'></i></button></div></div><div id='dropdown' class='box-tools pull-right'></div></div><div class='box-body' style='height:90%'><center><svg id='svg_id_error' style='width:97% !important;height:100% !important'></svg></center><center><div id='wrongmsg'> <h3 id='WrongMsg_error' style='color:#B71C1C'></h3></div></center></div></div> </div>";
                        $("#chart_view").append(chartTemp);
                        $("#WrongMsg_error").css("display", "block");
                        $("#WrongMsg_error")[0].innerHTML = "Sorry! Something went wrong. Please check the KPI details and try again";
                    }
                }
            }
        });
    }
}
var chart_Id;
function loadUpdateCharts(chart_Id) {

    var filters = {};

    filters["type"] = $("#FilterType").val();
    filters["minDate"] = $("#minFilter").val();
    filters["maxDate"] = $("#maxFilter").val();
    filters["compareType"] = $("#CompareFilterType").val();
    filters["minCompare"] = $("#minFilter1").val();
    filters["maxCompare"] = $("#maxFilter1").val();
    filters["compareCheck"] = $('#CompareCheck')[0].checked;
    if (filters["type"] == "Custom") {
        if (filters["minDate"] == "" && filters["maxDate"] == "") {
            $("#Applybutton")[0].disabled = true;
        }
    }
    if (getChartType != "") {
        filters["timeSeriesType"] = getChartType;
    }
    else {
        filters["timeSeriesType"] = "hour"
    }
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/getCharts?roleid=' + role.Role_ID + "&monitor=OBDN Dashboard&filters=" + JSON.stringify(filters) + "&KPIid=" + chart_Id,
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {

            var dataSource = $.parseJSON(result);
            var j;
            KPILoad = 1;
            $("#loader").hide();
            for (j = 0; j < dataSource.length; j++) {
                if (dataSource[j]["data"] != "") {
                    if (dataSource[j]["Chart_Type"] == "timeseries") {
                        $("#Chart_Type|15")[0].disabled = true;
                        $("#" + TimeseriesChartID).html('');
                        dataSource[j]["ID"] = TimeseriesChartID;
                        var chartTemp = "<div class= '" + dataSource[j]["ID"] + "_user-card' style='padding-right:5px; display:inline-table;margin-bottom:10px !important; float:left; padding-left:5px;height:" + dataSource[j]["Height"] + "px; width:" + dataSource[j]["Width"] + "%'><div class='box box-primary'><div class='box-header with-border'><h5 class='box-title'>" + dataSource[j]["Title"] + "</h5><div class='col-sm-9' style='float:right'><div class='btn-group' role='group' aria-label='...' style='float:right'><button id='hour' class='btn btn-primary' type='button' onclick='loadTimeCharts(this)'>Hourly</button><button id='day' class='btn btn-success' type='button' onclick='loadTimeCharts(this)'>Daily</button><button id='week' class='btn btn-info' type='button' onclick='loadTimeCharts(this)'>Weekly</button><button id='month' class='btn btn-select' type='button' onclick='loadTimeCharts(this)'>Monthly</button><button id='year' class='btn btn-primary' type='button' onclick='loadTimeCharts(this)'>Yearly</button><button type='button' id='" + dataSource[j]["KPI_ID"] + "' class='ChartEditButton' onclick='editsettingsFunction(this)' style='background-color: transparent !important'><i class='fa fa-cog graph-img-size'></i></button></div></div><div id='dropdown' class='box-tools pull-right'></div></div><div class='box-body' style='height:90%'><center><svg id=" + dataSource[j]["ID"] + " style='width:97% !important;height:" + dataSource[j]["Height"] + "px !important'></svg></center><center><div id='wrongmsg'> <h3 id='WrongMsg" + dataSource[j]["ID"] + "' style='color:#B71C1C'></h3></div></center></div></div> </div>"
                        $("." + dataSource[j]["ID"])[0].outerHTML = chartTemp;
                        var x = localStorage.getItem("Result");
                        var UserDetails1 = JSON.parse(x);
                        if (UserDetails1.Role_ID != "1") {
                            $(".ChartEditButton").hide();
                        }
                        if (dataSource[j]["data"] != "false" && dataSource[j]["data"] != "Connection Failure") {
                            d3Chart(dataSource[j]);
                            swal("", "Updated Successfully", "success");
                        }

                        else {
                            $("#WrongMsg" + dataSource[j]["ID"]).css("display", "block");
                            $("#WrongMsg" + dataSource[j]["ID"])[0].innerHTML = "Sorry! Something went wrong. Please check the KPI details and try again";

                        }
                    } else {
                        $("#" + removeChartID).html('');
                        dataSource[j]["ID"] = removeChartID;
                        var chartTemp = "<div class= '" + dataSource[j]["ID"] + "_user-card' style='padding-right:5px; display:inline-table;margin-bottom:10px !important; float:left; padding-left:5px;height:" + dataSource[j]["Height"] + "px; width:" + dataSource[j]["Width"] + "%'><div class='box box-primary'><div class='box-header with-border'><h5 class='box-title'>" + dataSource[j]["Title"] + "</h5><div id='dropdown' class='box-tools pull-right'><button type='button' id='" + dataSource[j]["KPI_ID"] + "' class='ChartEditButton' onclick='editsettingsFunction(this)' style='background-color: transparent !important'><i class='fa fa-cog graph-img-size'></i></button><button type='button' id='bar' class='UserChartEnable' style='background-color: transparent !important' onclick='changeChartType(this)'><img src='dist/img/chart icons/bars-chart.png' class='graph-img-size'></button><button type='button' id='line' class='UserChartEnable' style='background-color: transparent !important' onclick='changeChartType(this)'><img src='dist/img/chart icons/profit.png' class='graph-img-size'></button><button type='button' id='area' class='UserChartEnable' style='background-color: transparent !important;' onclick='changeChartType(this)'><img src='dist/img/chart icons/area-chart.png' class='graph-img-size'></button><button type='button' id='pie' class='UserChartEnable' style='background-color: transparent !important' onclick='changeChartType(this)'><img src='dist/img/chart icons/pie-chart.png' class='graph-img-size'></button><button type='button' id='donut' class='UserChartEnable' style='background-color: transparent !important' onclick='changeChartType(this)'><img src='dist/img/chart icons/dot-chart.png' class='graph-img-size'></button></div></div><div class='box-body' style='height:90%'><center><svg id='" + dataSource[j]["ID"] + "' style='width:97% !important;height:" + dataSource[j]["Height"] + "px !important'></svg></center><center><div id='wrongmsg'> <h3 id='WrongMsg" + dataSource[j]["ID"] + "' style='color:#B71C1C;display:none'></h3></div></center></div></div> </div>"
                        $("." + removeChartID + "_user-card")[0].outerHTML = chartTemp;
                        var x = localStorage.getItem("Result");
                        var UserDetails1 = JSON.parse(x);
                        if (UserDetails1.Role_ID != "1") {
                            $(".ChartEditButton").hide();
                        }
                        if (UserDetails1.Role_ID == "1") {
                            $(".UserChartEnable").hide();
                        }
                        if (dataSource[j]["data"] != "false" && dataSource[j]["data"] != "Connection Failure") {
                            d3Chart(dataSource[j]);
                            swal("", "Updated Successfully", "success");
                        }
                        else {
                            $("#WrongMsg" + dataSource[j]["ID"]).css('display', 'block');
                            $("#WrongMsg" + dataSource[j]["ID"])[0].innerHTML = "Sorry! Something went wrong. Please check the KPI details and try again";
                            $("#WrongMsg" + dataSource[j]["ID"]).css("height", "300px");

                        }

                    }
                }
            }
        },
        error: function (e) {
            //console.log("update Charts not loaded");
        }
    });
}
function UpdateQueryChange() {
    $("#QueryValidateUpdateButton")[0].disabled = false;
    $("#UpdateBtn")[0].disabled = true;
}
function QueryChange() {

    $("#RegisterBtn1")[0].disabled = true;
}
function QueryValidateClick(e) {
    var DBinstance, query;
    $(".ValidateInformation").html('');
    if ($(e).closest('form')[0].id == "SubmitForm1") {
        DBinstance = $(e).closest('form').find('.validateQuery')[0].value;
        query = $(e).closest('form').find('.validateQuery')[1].value;
    }
    else if ($(e).closest('form')[0].id == "SubmitForm2") {
        DBinstance = $(e).closest('form').find('.validateQuery')[0].value;
        query = $(e).closest('form').find('.validateQuery')[1].value;
    }
    else if ($(e).closest('form')[0].id == "UpdateForm") {
        DBinstance = $(e).closest('form').find('.updateValidateQuery')[0].value;
        query = $(e).closest('form').find('.updateValidateQuery')[1].value;
    }


    if ($('.validateQuery').val() == "") {
        swal("", "Please select DB Instance", "error");
    }
    else if ($('.validateQuery').val() != "") {
        $.ajax({
            type: "GET",
            async: false,
            url: '/services/CrudService.svc/queryValidate?DBInstance=' + DBinstance + '&Query=' + window.btoa(query),
            contentType: 'application/json; charset=utf-8',
            success: function (val) {
                var dataSource = JSON.parse(val);
                if (val == "false") {
                    if ($(e).closest('form')[0].id == "SubmitForm1" || $(e).closest('form')[0].id == "SubmitForm2") {
                        $(".ValidateInformation").html('Please enter query');
                        $(".ValidateInformation").css('color', '#B71C1C');
                    }
                    else if ($(e).closest('form')[0].id == "UpdateForm") {
                        $(".UpdateValidateInformation").html('Please enter query');
                        $(".UpdateValidateInformation").css('color', '#B71C1C');
                    }
                }
                else if (val == "Connection Failed") {

                    if ($(e).closest('form')[0].id == "SubmitForm1" || $(e).closest('form')[0].id == "SubmitForm2") {
                        $(".ValidateInformation").html('Database connection failed');
                    }
                    else if ($(e).closest('form')[0].id == "UpdateForm") {
                        $(".UpdateValidateInformation").html('Database connection failed');

                    }
                }
                else {
                    if ($(e).closest('form')[0].id == "SubmitForm1" || $(e).closest('form')[0].id == "SubmitForm2") {
                        $(".ValidateInformation").html('Entered query is valid');
                        $(".ValidateInformation").css('color', 'green');
                        $("#RegisterBtn1")[0].disabled = false;
                        $("#RegisterBtn2")[0].disabled = false;
                    }
                    else if ($(e).closest('form')[0].id == "UpdateForm") {
                        $(".UpdateValidateInformation").html('Entered query is valid');
                        $(".UpdateValidateInformation").css('color', 'green');
                        $("#UpdateBtn")[0].disabled = false;
                    }
                }

            },
            error: function (e) {

            }
        });
    }
}

function changeChartType(e) {

    for (i in GlobalDataSource) {
        if (GlobalDataSource[i]["ID"] == $($(e).parent().parent().parent().children()[1]).children().children()[0].id) {
            $($($(e).parent().parent().parent().children()[1]).children().children()[0]).html('');
            if (GlobalDataSource[i]["data1"] != null && GlobalDataSource[i]["data1"] != '' && GlobalDataSource[i]["data1"] != "null") {
                var d3data = [{

                    values: JSON.parse(GlobalDataSource[i]["data"]),
                    color: '#f9a65a',
                    key: "Current",
                    area: true
                }, {
                    values: JSON.parse(GlobalDataSource[i]["data1"]),
                    color: '#f9a65a',
                    key: "Previous",
                    area: true
                }
                ]
            } else {
                var d3data = [{

                    values: JSON.parse(GlobalDataSource[i]["data"]),
                    color: '#f9a65a',
                    key: "Current",

                    area: true
                }]
            }
            GlobalDataSource[i]["d3data"] = d3data;
            if (e.id == "bar")
                bar_chart(GlobalDataSource[i]);
            else if (e.id == "line" || e.id == "area")
                line_chart(GlobalDataSource[i]);
            else if (e.id == "pie")
                pie_chart(GlobalDataSource[i]);
            else if (e.id == "donut")
                donut_chart(GlobalDataSource[i]);
        }
    }


}
function loadTimeCharts(e) {

    $(e.parentElement.children).removeClass('ChartButton');
    $(e).addClass('ChartButton')
    var filters = {};
    getChartType = e.id;
    filters["type"] = $("#FilterType").val();
    filters["minDate"] = $("#minFilter").val();
    filters["maxDate"] = $("#maxFilter").val();
    filters["compareType"] = $("#CompareFilterType").val();
    filters["minCompare"] = $("#minFilter1").val();
    filters["maxCompare"] = $("#maxFilter1").val();
    filters["timeSeriesType"] = e.id;
    filters["compareCheck"] = $('#CompareCheck')[0].checked;
    if (filters["type"] == "Custom") {
        if (filters["minDate"] == "" && filters["maxDate"] == "") {
            $("#Applybutton")[0].disabled = true;
        }
    }
    if (getChartType != "") {
        filters["timeSeriesType"] = getChartType;
    }
    else {
        filters["timeSeriesType"] = "hour"
    }
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/getTimeCharts?roleid=' + role.Role_ID + "&monitor=OBDN Dashboard&filters=" + JSON.stringify(filters) + "&chartId=" + $(e).parent().parent().parent().parent().children()[1].children[1].children[0].children[0].id.split("_")[1],
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            var dataSource = $.parseJSON(result);
            var temp = [];
            var j;
            for (j = 0; j < dataSource.length; j++) {
                if (dataSource[j]["data"] != "") {
                    var chartTemp = "<div class= '" + dataSource[j]["ID"] + "_user-card' style='padding-right:5px; display:inline-table;margin-bottom:10px !important; float:left; padding-left:5px;height:" + dataSource[j]["Height"] + "px; width:" + dataSource[j]["Width"] + "%'><div class='box box-primary'><div class='box-header with-border'><h5 class='box-title'>" + dataSource[j]["Title"] + "</h5><div class='col-sm-9' style='float:right'><div class='btn-group' role='group' aria-label='...' style='float:right'><button id='hour' class='btn btn-primary' type='button' onclick='loadTimeCharts(this)'>Hourly</button><button id='day' class='btn btn-success' type='button' onclick='loadTimeCharts(this)'>Daily</button><button id='week' class='btn btn-select' type='button' onclick='loadTimeCharts(this)'>Weekly</button><button id='month' class='btn btn-danger' type='button' onclick='loadTimeCharts(this)'>Monthly</button><button id='year' class='btn btn-primary' type='button' onclick='loadTimeCharts(this)'>Yearly</button><button type='button' id='" + dataSource[j]["KPI_ID"] + "' class='ChartEditButton' onclick='editsettingsFunction(this)' style='background-color: transparent !important'><i class='fa fa-cog graph-img-size'></i></button></div></div><div id='dropdown' class='box-tools pull-right'></div></div><div class='box-body' style='height:90%'><center><svg id=" + dataSource[j]["ID"] + " style='width:97% !important;height:" + dataSource[j]["Height"] + "px !important'></svg></center><center style='display:none'><div id='wrongmsg'> <h3 id='WrongMsg" + dataSource[j]["ID"] + "' style='color:#B71C1C;display:none'></h3></div></center></div></div> </div>"
                    if (j % 2 == 0 && j != 0) dataSource[j].pr = 12;
                    else dataSource[j].pr = 6;
                    $("#" + dataSource[j]["ID"]).html('');
                    if (dataSource[j]["data"] != "false" && dataSource[j]["data"] != "Connection Failure") {
                        d3Chart(dataSource[j]);
                        var x = localStorage.getItem("Result");
                        var UserDetails1 = JSON.parse(x);
                        if (UserDetails1.Role_ID != "1") {
                            $(".ChartEditButton").hide();
                        }
                    }
                    else {
                        $("#WrongMsg" + GlobalDataSource[j]["ID"]).css("display", "block");
                        $("#WrongMsg" + dataSource[j]["ID"])[0].innerHTML = "Sorry! Something went wrong. Please check the KPI details and try again";
                    }
                }
            }
        },
        error: function (e) {
            //console.log("Time Charts not loaded");
            $("#loader").show();
            loadTimeCharts();
        }
    });
}
function CancelBtn() {
    $('#dateFilterContent').hide();
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
$(document).mouseup(function (e) {
    if (!$("#dateFilterContent").is(e.target) && !$("#chartTimer").is(e.target) && !$("#dateFilterTitle").is(e.target)
        && $("#dateFilterContent").has(e.target).length === 0) {
        $("#dateFilterContent").hide();
    }
});
