
var DropDownDefaultValues = {};
var DropDownTableValues = {};
DropDownDefaultValues["selectID"] = ""
DropDownDefaultValues["selectSubject"] = ""
DropDownDefaultValues["selectBody"] = ""
DropDownDefaultValues["selectType"] = ""
DropDownDefaultValues["selectActive"] = ""
DropDownDefaultValues["selectCreateBy"] = ""
DropDownDefaultValues["selectCreateDatetime"] = ""
DropDownDefaultValues["selectUpdateBy"] = ""
DropDownDefaultValues["selectUpdateDatetime"] = ""
DropDownDefaultValues["selectStatus"] = ""
var addEdit;

$(document).keypress(function (e) {

    if (e.which == 13) {
        $(".swal-button").click();
    }
});
function displayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {


            var select = $('[name="Columns"]')[i];
            $('[name="Columns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            console.log(key);
            select = $('[name="Columns"]')[i];
            var opt1 = '<option selected disabled hidden style=\"display: none\" value=\"1\"></option>';
            $('[name="Columns"]')[i].append(opt1);
            var key1 = Object.keys(DropDownDefaultValues);
            for (var j = 0; j < dropdownlist.length; j++) {

                //  var key = Object.keys(DropDownTableValues[0]);
                var opt = document.createElement('option');
                if (key.length == 2) {
                    opt.text = dropdownlist[j][key[1]];
                    opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                else {
                    opt.text = opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                select.add(opt, j);
            }
            var opt = document.createElement('option');
            opt.text = "Select Option";
            opt.selected = true;
            opt.value = "";
            select.add(opt, 0);
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}

function editDisplayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        async: false,
        success: function (val) {

            var value = $('[name="EditColumns"]')[i].value;
            var select = $('[name="EditColumns"]')[i];
            $('[name="EditColumns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            console.log(key);
            select = $('[name="EditColumns"]')[i];


            for (var j = 0; j < dropdownlist.length; j++) {
                var opt = document.createElement('option');
                if (key.length == 2) {
                    opt.text = dropdownlist[j][key[1]];
                    opt.id = dropdownlist[j][key[1]].replace(" ", "");
                    opt.value = dropdownlist[j][key[0]];
                    DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[1]];
                }
                else {
                    opt.text = opt.value = dropdownlist[j][key[0]];
                    opt.id = dropdownlist[j][key[0]].replace(" ", "");
                    DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[0]];
                }
                select.add(opt, j);
            }
            $('[name="EditColumns"]')[i].value = value;
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}
//function onSearch()
//{

//     var Filter = $("#Filter").val();


//    var grid = $("#grid").data("kendoGrid");
//    grid.dataSource.query({
//        page:1,
//        pageSize:20,
//        filter:{
//            logic:"or",
//            filters:[
//              { field: "ID", operator: "eq", value: parseInt(Filter) },
//              { field: "id1", operator: "contains", value: Filter },
//{field: "Subject", operator:"contains",value:Filter} ,
//{field: "Body", operator:"contains",value:Filter} ,
//{field: "Type", operator:"contains",value:Filter} ,
//{field: "Active", operator:"contains",value:Filter} ,


//            ]
//        },
//        sort: { field: "ID", dir: "desc" },
//    });
//}
//document.addEventListener('keypress', function (e) {
//    
//    var key = e.which || e.keyCode;
//    if (key === 13) { // 13 is enter
//        $('#btnFilter').click();
//    }
//});

$(document).ready(function () {

    var x = localStorage.getItem("Result");
    var UserDetails = JSON.parse(x);
    $('#addModal').on('show.bs.modal', function () {
        addEdit = "Columns";
        $(document.getElementsByName("Columns")[4]).empty();
        columns = $('[name="Columns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                displayDropDownList(i, elementId)
            }
        }
    })
    $('#editModal').on('show.bs.modal', function () {
        addEdit = "EditColumns";
        $(document.getElementsByName("EditColumns")[4]).empty();
        columns = $('[name="EditColumns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                editDisplayDropDownList(i, elementId);
            }
        }
        //Etype = document.getElementsByName("EditColumns")[2].value;
        //getEmailParameter(Etype);
    });
    $(document.getElementsByName("Columns")[2]).on("change", function () {
        Etype = $(this).val();
        getEmailParameter(Etype);
    });
    $(document.getElementsByName("EditColumns")[2]).on("change", function () {
        Etype = $(this).val();
        getEmailParameter(Etype);
    });

});

function getEmailParameter(Etype) {
        $(document.getElementsByName(addEdit)[4]).empty();
    if (Etype == 'Application') {
            var btn = document.createElement("BUTTON");
            btn.type = "button";
            $(btn).addClass("appvar");
            btn.onclick = function () {

                $(document.getElementsByName(addEdit)[3]).insertAtCaret('% %');
            }
            btn.innerHTML = 'Add Parameter';
            
            document.getElementsByName(addEdit)[4].appendChild(btn);
        }
    else if (Etype == 'System') {
            
            $.ajax({
                type: "GET",
                url: GlobalURL + '/api/CrudService/getTableData?tablename=EmailTemplateParameters',
                async: false,
                //data: Htmlname ,
                contentType: 'application/json; charset=utf-8',
                //dataType: 'json',
                success: function (val) {
                    
                    var dataSource = $.parseJSON(val);
                    var array = [];
                    for (i = 0; i < dataSource.length; i++) {
                        array.push([dataSource[i].ID, dataSource[i].Name]);
                    }
                    for (j = 0; j < array.length; j++) {
                        var btn = document.createElement("BUTTON");
                        btn.id = array[j][1] + '|' + array[j][0];
                        btn.type = "button";
                        $(btn).addClass("sysvar");
                        btn.onclick = function () {
                            
                            var code = '%' + $(this)[0].id.split('|')[0] + '%';
                            //$('.sysvar').id.split('|')[0]
                            $(document.getElementsByName(addEdit)[3]).insertAtCaret(code);

                        };
                        btn.innerHTML = array[j][1];
                        document.getElementsByName(addEdit)[4].appendChild(btn);
                    }
                },
                error: function (e) {

                }
            });
        }
};
jQuery.fn.extend({
    insertAtCaret: function (myValue) {
        return this.each(function (i) {
            if (document.selection) {
                //For browsers like Internet Explorer
                this.focus();
                sel = document.selection.createRange();
                sel.text = myValue;
                this.focus();
            }
            else if (this.selectionStart || this.selectionStart == '0') {
                //For browsers like Firefox and Webkit based
                var startPos = this.selectionStart;
                var endPos = this.selectionEnd;
                var scrollTop = this.scrollTop;
                this.value = this.value.substring(0, startPos) + myValue + this.value.substring(endPos, this.value.length);
                this.focus();
                this.selectionStart = startPos + myValue.length;
                this.selectionEnd = startPos + myValue.length;
                this.scrollTop = scrollTop;
            } else {
                this.value += myValue;
                this.focus();
            }
        })
    }
});
gridLoad();
function gridLoad() {
    $("#loader").show();
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableData?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML,
        async: false,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            var dataSource = $.parseJSON(val);
            var columns = $('[name="EditColumns"]');
            for (i = 0; i < columns.length; i++) {
                if (columns[i].type == "select-one") {

                    //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                    //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                    var ID = columns[i].id;
                    var ElementDetails = ID.split("|");
                    var elementId = ElementDetails[1];
                    editDisplayDropDownList(i, elementId);

                    for (j = 0; j < dataSource.length; j++) {
                        dataSource[j][ElementDetails[0]] = DropDownTableValues[dataSource[j][ElementDetails[0]]];
                    }
                }
            }
            var t = $('#grid').DataTable({
                destroy: true,
                "scrollY": true,
                "scrollX": true,
                "data": dataSource,
                "Info": false,
                "pagingType": "simple_numbers",
                "LengthChange": true,
                "aLengthMenu": [[20, 40, 60, -1], [20, 40, 60, "All"]],
                "iDisplayLength": 20,
                "order": [[0, 'desc']],
                "columns": [
                    {
                        "data": "ID",
                        "title": "ID"
                    },
                    {
                        "data": "Subject",
                        "title": "Subject"
                    },
                    {
                        "data": "Type",
                        "title": "Type"
                    },
                    {
                        "data": "Body",
                        "title": "Body"
                    },
                    {
                        "data": "Active",
                        "title": "Active"
                    },

                    {
                        "title": "Actions",
                        "defaultContent": "<button class='btn btn-warning btn-flat'  onclick='editFunction(this)'><i class='fa fa-edit' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Edit</span></button><button class='deleteBtnClass btn btn-flat'  onclick='deleteFunction(this)'><i class='fa fa-trash' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Delete</span></button>"
                    },

                ], "oLanguage": { "sSearch": "" },
                "columnDefs": [{
                    "targets": '_all',
                    "defaultContent": "",
                },
                {
                    "aTargets": [0],
                    "sClass": "hidden"
                },
                {
                    "targets": [0],
                    "orderable": true
                },],

            });
            t.page('first').draw('page');
            $("#grid_wrapper .dataTables_length label").css("display", "block");
            $(".dataTables_filter").addClass("pull-left");
            $(".dataTables_length").addClass("pull-right");
            $($('#grid_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
            $("#loader").hide();

        },
        //        for (i = 0; i < dataSource.length; i++) {
        //            if (dataSource[i].ID != null) {

        //                dataSource[i].ID = dataSource[i].ID.toString()
        //                dataSource[i].id1 = dataSource[i].ID.toString()
        //            }
        //            if(dataSource[i].Subject!=null){

        //dataSource[i].Subject= dataSource[i].Subject.toString()

        //}

        //if(dataSource[i].Body!=null){

        //dataSource[i].Body= dataSource[i].Body.toString()

        //}

        //if(dataSource[i].Type!=null){

        //dataSource[i].Type= dataSource[i].Type.toString()

        //}

        //if(dataSource[i].Active!=null){

        //dataSource[i].Active= dataSource[i].Active.toString()

        //}



        //        }

        //        $("#grid").kendoGrid({
        //            dataSource: {
        //                data: dataSource,
        //                pageSize: 10,
        //                schema: {
        //                    model: {
        //                        id:"ID",

        //fields:{

        //ID:{type: "number", editable: false },
        //id1: { type: "text" },
        //Subject:{type: "text"},

        //Body:{type: "text"},

        //Type:{type: "text"},

        //Active:{type: "text"},

        //}

        //                        }
        //                },
        //                sort: { field: "ID", dir: "desc" },
        //            },
        //            sortable: true,

        //            //pageable: true,
        //            pageable: {
        //                messages: {
        //                    display: "{0} - {1} of {2} Items"
        //                }
        //            },

        //            columns:  [{field:"ID", title:" Id",width:"120px"},

        //{field:"Subject", title:" Subject",width:"120px"},

        //{field:"Body", title:" Body",width:"120px"},

        //{field:"Type", title:" Type",width:"120px"},

        //{field:"Active", title:" Active",width:"120px"},
        //{ field: "id1", title: " id1", hidden: true, width: "120px" },

        //            { command: [{ title: "Edit", template: "<button title='Edit' class='btn btn-flat' onclick='editFunction(this)'><span class='editIcon'></span> </button>" }, { title: "Delete", template: "<button title='Delete' class='deleteBtnClass btn btn-flat' onclick='deleteFunction(this)'><span class='deleteIcon'></span> </button>", }, ], title: "Action", width: "160px" }],

        ////edit: function (e) {
        ////    var updateBtn = e.container.find(".k-grid-update");
        ////    updateBtn.removeClass("k-grid-update");

        ////    //removing this class will prevent the grid from saving on click
        ////    updateBtn.click(function () {

        ////        updateFunction(); //call the function you wish
        ////        updateBtn.addClass("k-grid-update");
        ////    })



        //});


        //},
        error: function (e) {
            //alert("Something Wrong!!");
            gridLoad();
        }
    });
}
function addFunction() {
    $('#addModal').modal('show')
}
function editFunction(e) {
    

    $('#editModal').modal('show');

    var tr = $(e)[0].closest("tr");
    var td = tr.children;
    var columns = $('[name="EditColumns"]');
    //console.log(td);
    for (var i = 0; i < columns.length; i++) {
        if (i == 0) {
            $('[name="EditColumns"]')[i].innerHTML = $('[name="EditColumns"]')[i].value = td[i].innerHTML;
        }
        else if ($('[name="EditColumns"]')[i].localName == "select") {
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
                $('[name="EditColumns"]')[i].value = $("#" + td[i].innerHTML.replace(" ", "")).val().replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        else {
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
                $('[name="EditColumns"]')[i].value = td[i].innerHTML.replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        //console.log($('[name="EditColumns"]')[i].value);
        if ($(e).parent().closest('tr').find('td')[4].innerHTML == "true") {
            $(".check").prop("checked", true);
        }
        else {
            $(".check").prop("checked", false);
        }
    }
    typeEdit = document.getElementsByName("EditColumns")[2].value;
    if (addEdit == "EditColumns" && typeEdit != "") {
        getEmailParameter(typeEdit);
    }
}


function deleteFunction(e) {
    //$('#deleteModal').modal('show');
    swal("Are you sure you want to delete this record?", {
        buttons: ["Cancel", "Ok"],
        className: "deleterecord",
        closeOnClickOutside: false,
        closeOnEsc: false,

    });
    $(".deleterecord .swal-footer .swal-button-container .swal-button--confirm").on("click", function () {

        $('#deleteModal').modal('hide');

        var data = {

        }

        data[$("#deleteName")[0].innerHTML] = $("#deleteId")[0].innerHTML
        var IdData = JSON.stringify(data);
        //var IdJSON = JSON.stringify(Id);

        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/deleteTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML + '&ID=' + IdData,
            //data: Htmlname ,
            contentType: 'application/json; charset=utf-8',
            //dataType: 'json',

            success: function (val) {
                //alert(val);


                if (val == "Success") {

                    swal("", "Record Deleted Successfully", "success", { closeOnClickOutside: false }); 
                }
                // $("#feedbackMsg").html("Success");

                else {
                    //$("#feedbackMsg").html("Sorry cannot perform such operation");
                    swal("", "Email should not delete as it is assigned to  project/user", "error", { closeOnClickOutside: false }); 
                }
                gridLoad();
            },
            error: function (e) {
                $("#feedbackMsg").html("Something Wrong.");
            }
        });
    });

    $("#deleteId")[0].innerHTML = $($(e).closest("tr")[0]).children('td:first-child')[0].innerHTML
    $("#deleteName")[0].innerHTML = $($($("#grid").find('thead')[0]).find('tr')[0]).find('th')[0].innerText.trim().toUpperCase()


}

$(document).ready(function () {
    $("#SubmitForm").on("submit", function (e) {
        e.preventDefault();
        //if (validator.validate()) {
        var Values = {
        }
        var Columns = $('[name="Columns"]');
        var len = $('[name="Columns"]').length;
        for (var i = 1; i < len; i++) {
            var ID = $('[name="Columns"]')[i].id;
            if (ID != "var") {
                var columnName = ID.split("|");
            if (columnName[0].toUpperCase() == "CREATEBY") {
                Values[columnName[0]] = window.btoa(JSON.parse(localStorage.Result).User_ID);
            }
            else if (columnName[0].toUpperCase() == "CREATEDATETIME") {
                Values[columnName[0]] = window.btoa(GetNow());
            }
            else if (columnName[0].toUpperCase() == "STATUS")
                Values[columnName[0]] = window.btoa(true);
            else {
                if ($('[name="Columns"]')[i].type == "checkbox")
                    Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].checked);
                else
                    Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].value);
            }
        }
        }

        var values = JSON.stringify(Values);
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/createTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML,
            data: JSON.stringify(values),

            //data: Htmlname ,
            contentType: 'application/json; charset=utf-8',
            //dataType: 'json', 
            success: function (val) {
                if (val == "true") {

                    swal("", "Record Added Successfully", "success", { closeOnClickOutside: false }); 
                }
                else{
                    swal("", "Email name already exists", "error", { closeOnClickOutside: false }); 
                }
                //location.reload();
                $("#addModal .close").click();
                gridLoad();
            },
            error: function (e) {

            }
        });
        //}
    });
    $("#UpdateForm").on("submit", function (e) {
        e.preventDefault();
        //if (validator1.validate()) {
        var IDName = $('[name="EditColumns"]')[0].id.split("|");
        var ID = {};
        ID[IDName[0]] = $('[name="EditColumns"]')[0].value;
        columns = $('[name="EditColumns"]');
        var Values = {
            "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
            //  "UpdateDatetime": window.btoa(GetNow())
        }
        for (var i = 1; i < columns.length; i++) {
            var z = $('[name="EditColumns"]')[i].id.split("|");
            if (z != "var") {
            if ($('[name="EditColumns"]')[i].type == "checkbox") {
                Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].checked);
            }
            else {

                Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].value);
            }
        }
        }
        var values = JSON.stringify(Values);
        var Id = JSON.stringify(ID);
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/UpdateTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML + '&ID=' + Id,
            data: JSON.stringify(values),

            //data: Htmlname ,
            contentType: 'application/json; charset=utf-8',
            //dataType: 'json', 
            success: function (val) {

                //location.reload();
                $("#editModal .close").click();
                gridLoad();
                if (val == "true") {

                    swal("", "Updated Successfully", "success", { closeOnClickOutside: false }); 
                }
            },
            error: function (e) {

            }
        });
        //}
    });

});






