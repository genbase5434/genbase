
var TypeArr = [];


var DropDownDefaultValues = {};
var DropDownTableValues = {};
DropDownDefaultValues["selectID"] = ""
DropDownDefaultValues["selectName"] = ""
DropDownDefaultValues["selectDescription"] = ""
DropDownDefaultValues["selectValue"] = ""
DropDownDefaultValues["selectCreateBy"] = ""
DropDownDefaultValues["selectCreateDatetime"] = ""
DropDownDefaultValues["selectUpdateBy"] = ""
DropDownDefaultValues["selectUpdateDatetime"] = ""
DropDownDefaultValues["selectStatus"] = ""
DropDownDefaultValues["selectType"] = ""

function displayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            

            var select = $('[name="Columns"]')[i];
            $('[name="Columns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            console.log(key);
            select = $('[name="Columns"]')[i];
            var opt1 = '<option selected disabled hidden style=\"display: none\" value=\"1\"></option>';
            $('[name="Columns"]')[i].append(opt1);
            var key1 = Object.keys(DropDownDefaultValues);
            for (var j = 0; j < dropdownlist.length; j++) {
                
                //  var key = Object.keys(DropDownTableValues[0]);
                var opt = document.createElement('option');
                if (key.length == 2) {
                    opt.text = dropdownlist[j][key[1]];
                    opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                else {
                    opt.text = opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                select.add(opt, j);
            }
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}

function editDisplayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: GlobalURL + 'api/CrudService/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        async: false,
        success: function (val) {
            
            var value = $('[name="EditColumns"]')[i].value;
            var select = $('[name="EditColumns"]')[i];
            $('[name="EditColumns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            console.log(key);
            select = $('[name="EditColumns"]')[i];


            for (var j = 0; j < dropdownlist.length; j++) {
                var opt = document.createElement('option');
                if (key.length == 2) {
                    opt.text = dropdownlist[j][key[1]];
                    opt.id = dropdownlist[j][key[1]].replace(" ", "");
                    opt.value = dropdownlist[j][key[0]];
                    DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[1]];
                }
                else {
                    opt.text = opt.value = dropdownlist[j][key[0]];
                    opt.id = dropdownlist[j][key[0]].replace(" ", "");
                    DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[0]];
                }
                select.add(opt, j);
            }
            $('[name="EditColumns"]')[i].value = value;
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}

var x = localStorage.getItem("Result");
var UserDetails = JSON.parse(x);
$(document).ready(function () {
    
    
    $('#addModal').on('show.bs.modal', function () {

        columns = $('[name="Columns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                displayDropDownList(i, elementId)
            }
        }
    })
    $('#editModal').on('show.bs.modal', function () {

        columns = $('[name="EditColumns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                editDisplayDropDownList(i, elementId);
            }
        }
    });
});


gridLoad("general");
function gridLoad(tabvalue) {
    $("#" + tabvalue.toLowerCase()).click();    
}


function editFunction(e) {
    
    $('#editModal').modal('show');

    var tr = $(e)[0].closest("tr");
    var td = tr.children;
    var columns = $('[name="EditColumns"]');
    for (var i = 0; i < columns.length; i++) {
        if (i == 0) {
            $('[name="EditColumns"]')[i].innerHTML = $('[name="EditColumns"]')[i].value = td[i].innerHTML;
        }
        else if ($('[name="EditColumns"]')[i].localName == "select") {
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
                $('[name="EditColumns"]')[i].value = $("#" + td[i].innerHTML.replace(" ", "")).val().replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        else {
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
                $('[name="EditColumns"]')[i].value = td[i].innerHTML.replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
    }

}
function deleteFunction(e) {
    //$('#deleteModal').modal('show');
    swal("Are you sure you want to delete this record?", {
        buttons: ["Cancel", "Ok"],
        className: "deleterecord",
        closeOnClickOutside: false,
        closeOnEsc: false,

    });
    $(".deleterecord .swal-footer .swal-button-container .swal-button--confirm").on("click", function () {
        $('#deleteModal').modal('hide');
        var data = {
        }
        data[$("#deleteName")[0].innerHTML] = $("#deleteId")[0].innerHTML
        var IdData = JSON.stringify(data);
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/deleteTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML + '&ID=' + IdData,
            contentType: 'application/json; charset=utf-8',
            success: function (val) {
                // gridLoad();
                var tbValue = $($("li").find(".active")).attr("title");
                gridLoad(tbValue);
                if (val == "true") {
                    swal("", "Record Deleted Successfully", "success")
                }
                else {
                    $("#feedbackMsg").html("Sorry cannot perform such operation");
                }
            },
            error: function (e) {
                $("#feedbackMsg").html("Something Wrong.");
            }
        });
    });
    $("#deleteId")[0].innerHTML = $($(e).closest("tr")[0]).children('td:first-child')[0].innerHTML
    $("#deleteName")[0].innerHTML = $($($("#grid").find('thead')[0]).find('tr')[0]).find('th')[0].innerText.trim().toUpperCase()

}




$(document).ready(function () {

    $("#SubmitForm").on("submit", function (e) {
        e.preventDefault();

        var Values = {
        }
        var Columns = $('[name="Columns"]');
        var len = $('[name="Columns"]').length;
        for (var i = 1; i < len; i++) {
            var ID = $('[name="Columns"]')[i].id;
            var columnName = ID.split("|");
            if (columnName[0].toUpperCase() == "CREATEBY") {
                Values[columnName[0]] = window.btoa(JSON.parse(localStorage.Result).User_ID);
            }
            else if (columnName[0].toUpperCase() == "CREATEDATETIME") {
                Values[columnName[0]] = window.btoa(GetNow());
            }
            else if (columnName[0].toUpperCase() == "STATUS")
                Values[columnName[0]] = window.btoa(true);
            else {
                if ($('[name="Columns"]')[i].type == "checkbox")
                    Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].checked);
                else
                    Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].value);
            }
        }

        var values = JSON.stringify(Values);
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/createTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML,
            data: JSON.stringify(values),

            //data: Htmlname ,
            contentType: 'application/json; charset=utf-8',
            //dataType: 'json', 
            success: function (val) {
                if (val == "true") {

                }
                //location.reload();
                $("#addModal .close").click();
                //gridLoad();
                var tbValue = $($("li").find(".active")).attr("title");
                gridLoad(tbValue);
            },
            error: function (e) {

            }
        });

    });

    $("#UpdateForm").on("submit", function (e) {
        e.preventDefault();
        

        var IDName = $('[name="EditColumns"]')[0].id.split("|");
        var ID1 = {};
        ID1[IDName[0]] = $('[name="EditColumns"]')[0].value;
        columns = $('[name="EditColumns"]');
        var Values = {
            "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
            //  "UpdateDatetime": window.btoa(GetNow())
        }
        for (var i = 1; i < columns.length; i++) {
            var z = $('[name="EditColumns"]')[i].id.split("|");
            if ($('[name="EditColumns"]')[i].type == "checkbox") {
                Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].checked);
            }
            else {

                Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].value);
            }
        }
        var values = JSON.stringify(Values);
        var Id = "";
         Id = JSON.stringify(ID1);
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/UpdateTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML + '&ID=' + Id,
            data: JSON.stringify(values),

            //data: Htmlname ,
            contentType: 'application/json; charset=utf-8',
            //dataType: 'json', 
            success: function (val) {

                //location.reload();
                $("#editModal .close").click();
                //gridLoad();

                var tbValue = $($("li").find(".active")).attr("title");
                gridLoad(tbValue);

                //filterCharts = $(".config-icons li i.active")[0].title.toLowerCase();
                if (val == "true") {

                    swal("", "Updated Successfully", "success")
                }
            },
            error: function (e) {

            }
        });

    });
});

function filterGrid(e) {
    $(".config-icons li i").removeClass("active");
    $(e).find('i').addClass('active');

    //t.search(e.id).draw();
    if (e.id == "ldap") {
        var t = $('#grid').DataTable({
            "destroy": true,
            "scrollY": true,
            "scrollX": true,
            //"processing": true,
            "serverSide": true,
            "paging": true,
            "pageLength": 20,
            "lengthChange": false,
            "language": { search: '', searchPlaceholder: "Search..." },

            "ajax":
              {
                "url": GlobalURL + '/api/CrudService/getConfigurationGridData?tablename=' + document.getElementsByTagName('tablename')[0].innerHTML + '&Type=ldap',
                  "contentType": "application/json",
                  "type": "POST",
                  "dataType": "JSON",
                "data": function (d) {
                    return JSON.stringify(d);
                  },
                  "dataSrc": function (json) {

                      //json.draw = json.draw;
                      //json.recordsTotal = json.recordsTotal;
                      //json.recordsFiltered = json.recordsFiltered;
                      //json.data = json.data;

                      var return_data = json;
                      //return return_data.data;
                      var dttt = $.parseJSON(return_data.data);
                      return dttt;
                  }
              },
            "oLanguage": { "sSearch": "" },
            "columns": [
               {
                   "data": "ID",
                   "title": "ID"
               },
               {
                   "data": "Name",
                   "title": "Name"
               },
               {
                   "data": "Description",
                   "title": "Description"
               },
               {
                   "data": "Value",
                   "title": "Value"
               },
               {
                   "data": "Type",
                   "title": "Type"
               },
               {
                   "title": "Actions",
                   "defaultContent": "<button class='btn btn-warning btn-flat' title='Edit' onclick='editFunction(this)'><i class='fa fa-edit' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Edit</span></button>&nbsp;<button class='deleteBtnClass btn btn-flat' title='Delete' onclick='deleteFunction(this)'><i class='fa fa-trash' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Delete</span></button>"
               },

            ],

            "columnDefs": [{
                "targets": '_all',
                "defaultContent": "",
            },
           {
               "aTargets": [0],
               "sClass": "hidden"
           },
           {
               "aTargets": [4],
               "sClass": "hidden"
           },
           {
               "targets": [0],
               "orderable": true
           },
            ],
        });

        t.page('first').draw('page');
        $(".dataTables_filter").addClass("pull-left");
        
    }
    else if (e.id == "general") {

        //var t = $('#grid').DataTable({
        //    "destroy": true,
        //    "scrollY": true,
        //    "scrollX": true,
        //    "processing": true,
        //    "serverSide": true,
        //    "paging": true,
        //    "pageLength": 20,
        //    "lengthChange": false,
        //    "language": { search: '', searchPlaceholder: "Search..." },
        //    "ajax":
        //      {
        //        "url": GlobalURL + '/api/CrudService/getConfigurationGridData?tablename=' + document.getElementsByTagName('tablename')[0].innerHTML + '&Type=General',
        //          "contentType": "application/json",
        //          "type": "POST",
        //          "dataType": "JSON",
        //        "data": function (d) {
        //            return JSON.stringify(d);
        //          },
        //          "dataSrc": function (json) {

        //              //json.draw = json.draw;
        //              //json.recordsTotal = json.recordsTotal;
        //              //json.recordsFiltered = json.recordsFiltered;
        //              //json.data = json.data;

        //              var return_data = json;
        //              //return return_data.data;
        //              var dttt = $.parseJSON(return_data.data);
        //              return dttt;
        //          }
        //      },
        //    "oLanguage": { "sSearch": "" },
        //    "columns": [
        //         {
        //             "data": "ID",
        //             "title": "ID"
        //         },
        //         {
        //             "data": "Name",
        //             "title": "Name"
        //         },
        //         {
        //             "data": "Description",
        //             "title": "Description"
        //         },
        //         {
        //             "data": "Value",
        //             "title": "Value"
        //         },
        //         {
        //             "data": "Type",
        //             "title": "Type"
        //         },
        //         {
        //             "title": "Actions",
        //             "defaultContent": "<button class='btn btn-warning btn-flat' title='Edit' onclick='editFunction(this)'><i class='fa fa-edit' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Edit</span></button>&nbsp;<button class='deleteBtnClass btn btn-flat' title='Delete' onclick='deleteFunction(this)'><i class='fa fa-trash' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Delete</span></button>"
        //         },

        //    ],
           
        //    "columnDefs": [{
        //        "targets": '_all',
        //        "defaultContent": "",
        //    },
        //    {
        //        "aTargets": [0],
        //        "sClass": "hidden"
        //    },
        //    {
        //        "aTargets": [4],
        //        "sClass": "hidden"
        //    },
        //    {
        //        "targets": [0],
        //        "orderable": true
        //    },
        //    ],
        //});

        //t.page('first').draw('page');
        //$(".dataTables_filter").addClass("pull-left");      
        
        $.ajax({
            type: "GET",
            url: GlobalURL + '/api/CrudService/getTableData?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML + '&Type=General',
        //          "contentType": "application/json",
           // url: GlobalURL + '/api/CrudService/getConfigurationGridData?tablename=' + document.getElementsByTagName('tablename')[0].innerHTML + '&Type=General',
        //          "contentType": "application/json",
        //          "type": "POST",
            //data: Htmlname ,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false,
            success: function (val) {
                
                //var dataSource = $.parseJSON(val);
                var dataSource = val;
                var columns = $('[name="EditColumns"]');
                for (i = 0; i < columns.length; i++) {
                    if (columns[i].type == "select-one") {
                        //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                        //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                        var ID = columns[i].id;
                        var ElementDetails = ID.split("|");
                        var elementId = ElementDetails[1];
                        editDisplayDropDownList(i, elementId);
                        for (j = 0; j < dataSource.length; j++) {
                            dataSource[j][ElementDetails[0]] = DropDownTableValues[dataSource[j][ElementDetails[0]]];
                        }
                    }
                }
                var t = $('#grid').DataTable({
                    "destroy": true,
                    "scrollY": true,
                    "scrollX": true,
                    "data": dataSource,
                    //"serverSide": true,
                    "paging": true,
                    "pageLength": 20,
                    "lengthChange": true,
                    "language": { search: '', searchPlaceholder: "Search..." },



                    //destroy: true,
                    //"scrollY": true,
                    //"scrollX": true,
                    //"data": dataSource,
                    //"Info": false,
                    //"pagingType": "simple_numbers",
                    //"LengthChange": true,
                    //"aLengthMenu": [[20, 40, 60, -1], [20, 40, 60, "All"]],
                    //"iDisplayLength": 20,
                    //"order": [[0, 'desc']],
                    "columns": [
                 {
                     "data": "ID",
                     "title": "ID"
                 },
                 {
                     "data": "Name",
                     "title": "Name"
                 },
                 {
                     "data": "Description",
                     "title": "Description"
                 },
                 {
                     "data": "Value",
                     "title": "Value"
                 },
                 {
                     "data": "Type",
                     "title": "Type"
                 },
                 {
                     "title": "Actions",
                     "defaultContent": "<button class='btn btn-warning btn-flat' title='Edit' onclick='editFunction(this)'><i class='fa fa-edit' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Edit</span></button>&nbsp;<button class='deleteBtnClass btn btn-flat' title='Delete' onclick='deleteFunction(this)'><i class='fa fa-trash' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Delete</span></button>"
                 },

            ],

            "columnDefs": [{
                "targets": '_all',
                "defaultContent": "",
            },
            {
                "aTargets": [0],
                "sClass": "hidden"
            },
            {
                "aTargets": [4],
                "sClass": "hidden"
            },
            {
                "targets": [0],
                "orderable": true
            },
            ],
                });
                t.page('first').draw('page');
                $("#grid_wrapper .dataTables_length label").css("display", "block");
                $(".dataTables_filter").addClass("pull-left");
                $(".dataTables_length").addClass("pull-right");
                // $($('#grid_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');

                $(".dataTables_filter").addClass("pull-left");
            },
            error: function (e) {
                //alert("Something Wrong!!");
                gridLoad();
            }

        });

    }
    else if (e.id == "email") {
        var t = $('#grid').DataTable({
            "destroy": true,
            "scrollY": true,
            "scrollX": true,
           // "processing": true,
            "serverSide": true,
            "paging": true,
            "pageLength": 20,
            "lengthChange": false,
            "language": { search: '', searchPlaceholder: "Search..." },
            "ajax":
              {
                "url": GlobalURL + "/api/CrudService/getConfigurationGridData?tablename=" + document.getElementsByTagName('tablename')[0].innerHTML + '&Type=email',
                  "contentType": "application/json",
                  "type": "POST",
                  "dataType": "JSON",
                  "data": function (d) {
                    return JSON.stringify(d);
                  },
                  "dataSrc": function (json) {

                      //json.draw = json.draw;
                      //json.recordsTotal = json.recordsTotal;
                      //json.recordsFiltered = json.recordsFiltered;
                      //json.data = json.data;

                      var return_data = json;
                      //return return_data.data;
                      var dttt = $.parseJSON(return_data.data);
                      return dttt;
                  }
              },
            "oLanguage": { "sSearch": "" },
            "columns": [
               {
                   "data": "ID",
                   "title": "ID"
               },
               {
                   "data": "Name",
                   "title": "Name"
               },
               {
                   "data": "Description",
                   "title": "Description"
               },
               {
                   "data": "Value",
                   "title": "Value"
               },
               {
                   "data": "Type",
                   "title": "Type"
               },
               {
                   "title": "Actions",
                   "defaultContent": "<button class='btn btn-warning btn-flat' title='Edit' onclick='editFunction(this)'><i class='fa fa-edit' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Edit</span></button>&nbsp;<button class='deleteBtnClass btn btn-flat' title='Delete' onclick='deleteFunction(this)'><i class='fa fa-trash' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Delete</span></button>"
               },

            ],
           
            "columnDefs": [{
                "targets": '_all',
                "defaultContent": "",
            },
            {
                "aTargets": [0],
                "sClass": "hidden"
            },
            {
                "aTargets": [4],
                "sClass": "hidden"
            },
            {
                "targets": [0],
                "orderable": true
            },
            ],
        });

        t.page('first').draw('page');
        $(".dataTables_filter").addClass("pull-left");
                
    }
}







