

var DropDownDefaultValues = {};
var DropDownTableValues = {};
DropDownDefaultValues["selectID"]=""
DropDownDefaultValues["selectAPIName"]=""
DropDownDefaultValues["selectProductName"]=""
DropDownDefaultValues["selectAPIType"]=""
DropDownDefaultValues["selectURL"]=""
DropDownDefaultValues["selectDescription"]=""
DropDownDefaultValues["selectAccessType"]=""
DropDownDefaultValues["selectResponse"]=""
DropDownDefaultValues["selectCreateBy"]=""
DropDownDefaultValues["selectCreateDatetime"]=""
DropDownDefaultValues["selectUdpateBy"]=""
DropDownDefaultValues["selectUpdateDatetime"]=""
DropDownDefaultValues["selectStatus"]=""

function displayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            

            var select = $('[name="Columns"]')[i];
            $('[name="Columns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            //console.log(key);
            select = $('[name="Columns"]')[i];
            var opt1 = '<option selected disabled hidden style=\"display: none\" value=\"1\"></option>';
            $('[name="Columns"]')[i].append(opt1);
            var key1 = Object.keys(DropDownDefaultValues);
            for (var j = 0; j < dropdownlist.length; j++) {
                
                //  var key = Object.keys(DropDownTableValues[0]);
                var opt = document.createElement('option');
                if (key.length == 2) {
                    opt.text = dropdownlist[j][key[1]];
                    opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                else {
                    opt.text = opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                select.add(opt, j);
            }
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}

function editDisplayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        async: false,
        success: function (val) {
            
            var value = $('[name="EditColumns"]')[i].value;
            var select = $('[name="EditColumns"]')[i];
            $('[name="EditColumns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            //console.log(key);
            select = $('[name="EditColumns"]')[i];


            for (var j = 0; j < dropdownlist.length; j++) {


                if (key.length == 2) {
                    if (dropdownlist[j][key[1]] != null && dropdownlist[j][key[0]] != null) {
                        var opt = document.createElement('option');
                        opt.text = dropdownlist[j][key[1]];
                        opt.id = elementId + "_" + dropdownlist[j][key[1]].replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                            
                            var res = "ASCII" + ($0).charCodeAt();
                            return res
                        });
                        opt.value = dropdownlist[j][key[0]];
                        DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[1]];
                    }
                }
                else {
                    if (dropdownlist[j][key[0]] != null) {
                        var opt = document.createElement('option');
                        opt.text = opt.value = dropdownlist[j][key[0]];
                        opt.id = elementId + "_" + dropdownlist[j][key[0]].replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                            
                            var res = "ASCII" + ($0).charCodeAt();
                            return res
                        });
                        DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[0]];
                    }
                }
                select.add(opt, j);
            }
            $('[name="EditColumns"]')[i].value = value;
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}

$(document).ready(function () {
    
    var x = localStorage.getItem("Result");
    var UserDetails = JSON.parse(x);
    $('#addModal').on('show.bs.modal', function () {

        columns = $('[name="Columns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                displayDropDownList(i, elementId)
            }
        }
    })
    $('#editModal').on('show.bs.modal', function () {

        columns = $('[name="EditColumns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                editDisplayDropDownList(i, elementId);
            }
        }
    });

});
gridLoad();
function gridLoad() {
    
    var t = $('#grid').DataTable({
        "destroy": true,
        "processing": true,
        "serverSide": true,
        "paging": true,
        "pageLength": 20,
        "lengthChange": false,
        "language": { search: '', searchPlaceholder: "Search..." },

        "ajax":
          {
              "url": '/services/CrudService.svc/getGridData?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML,
              "contentType": "application/json",
              "type": "GET",
              "dataType": "JSON",
              "data": function (d) {
                  return d;
              },
              "dataSrc": function (json) {
                  json.draw = json.draw;
                  json.recordsTotal = json.recordsTotal;
                  json.recordsFiltered = json.recordsFiltered;
                  json.data = json.data;

                  var return_data = json;
                  //return return_data.data;
                  var dttt = $.parseJSON(return_data.data);
                  return dttt;
              }
          },

        "columns": [

{"data":"ID",

"title":"ID"},

 
        "columnDefs": [{
            "targets": '_all',
            "defaultContent": "",
        },
                {
                    "targets": [0],
                    "orderable": false
                },
                {"aTargets": [0,],

"sClass": "hidden" }

],
    });

    t.page('first').draw('page');
    $(".dataTables_filter").addClass("pull-left");

}

function addFunction(e) {
    $('#addModal').modal('show')
}
function editFunction(e) {

    
    $('#editModal').modal('show');

    var tr = $(e)[0].closest("tr");
    var td = tr.children;
    var columns = $('[name="EditColumns"]');
    //console.log(td);
    for (var i = 0; i < columns.length; i++) {
        if (i == 0) {
            $('[name="EditColumns"]')[i].innerHTML = $('[name="EditColumns"]')[i].value = td[i].innerHTML;
        }
        else if ($('[name="EditColumns"]')[i].localName == "select") {
            //$('[name="EditColumns"]')[i].value = $("#" + td[i].innerHTML.replace(" ","")).val();
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
                $('[name="EditColumns"]')[i].value = $("#" + $('[name="EditColumns"]')[i].id.split("|")[1] + "_" + td[i].innerHTML.replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                    
                    var res = "ASCII" + ($0).charCodeAt();
                    return res
                })).val().replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        else {
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
                $('[name="EditColumns"]')[i].value = td[i].innerHTML.replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        //console.log($('[name="EditColumns"]')[i].value);
    }

}

function deleteFunction(e) {
    
    swal("Are you sure you want to delete this record?", {
        buttons: ["Cancel", "Ok"],
        className: "deleterecord",
        closeOnClickOutside: false,
        closeOnEsc: false,
    });
    $(".deleterecord .swal-footer .swal-button-container .swal-button--confirm").on("click", function () {
        $('#deleteModal').modal('hide');
        var data = {
        }
        data[$("#deleteName")[0].innerHTML] = $("#deleteId")[0].innerHTML
        var IdData = JSON.stringify(data);
        $.ajax({
            type: "POST",
            url: '/services/CrudService.svc/DeleteTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML + '&ID=' + IdData,
            contentType: 'application/json; charset=utf-8',
            success: function (val) {
                if (val == "Success") {
                    swal("", "Record Deleted Successfully", "success")
                }
                else {
                    $("#feedbackMsg").html("Sorry cannot perform such operation");
                }
                gridLoad();
            },
            error: function (e) {
                $("#feedbackMsg").html("Something Wrong.");
            }
        });
    });
    $("#deleteId")[0].innerHTML = $($(e).closest("tr")[0]).children('td:first-child')[0].innerHTML
    $("#deleteName")[0].innerHTML = $($($($("#grid").find('thead')[0]).find('tr')[0]).find('th')[0]).text().trim()
}
$(document).ready(function () {

    $("#SubmitForm").on("submit", function (e) {
        e.preventDefault();

        var Values = {
        }
        var Columns = $('[name="Columns"]');
        var len = $('[name="Columns"]').length;
        for (var i = 1; i < len; i++) {
            var ID = $('[name="Columns"]')[i].id;
            var columnName = ID.split("|");
            if (columnName[0].toUpperCase() == "CREATEBY") {
                Values[columnName[0]] = window.btoa(JSON.parse(localStorage.Result).User_ID);
            }
            else if (columnName[0].toUpperCase() == "CREATEDATETIME") {
                Values[columnName[0]] = window.btoa(GetNow());
            }
            else if (columnName[0].toUpperCase() == "STATUS")
                Values[columnName[0]] = window.btoa(true);
            else {
                if ($('[name="Columns"]')[i].type == "checkbox") {
                    Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].checked);

                }
                else {
                    Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].value);

                }

            }
        }

        var values = JSON.stringify(Values);
        $.ajax({
            type: "POST",
            url: '/services/CrudService.svc/createTableRow',
            data: JSON.stringify({
                "tablename": document.getElementsByTagName("tablename")[0].innerHTML,
                "Values": values
            }),

            //data: Htmlname ,
            contentType: 'application/json; charset=utf-8',
            //dataType: 'json', 
            success: function (val) {
                var Columns = $('[name="Columns"]');
                for (var i = 1; i < len; i++) {
                    if ($('[name="Columns"]')[i].type == "checkbox") {
                        $('[name="Columns"]')[i].checked = false;

                    }
                    else {
                        $('[name="Columns"]')[i].value = "";

                    }
                }
                if (val.createTableRowResult == "true") {
                    swal("", "Record Added Successfully", "success")
                }
                //location.reload();
                $("#addModal").modal('hide');
                gridLoad();
            },
            error: function (e) {

            }
        });

    });
    $("#UpdateForm").on("submit", function (e) {
        e.preventDefault();

        var IDName = $('[name="EditColumns"]')[0].id.split("|");
        var ID = {};
        ID[IDName[0]] = $('[name="EditColumns"]')[0].value;
        columns = $('[name="EditColumns"]');
        var Values = {}
        for (var i = 1; i < columns.length; i++) {
            var z = $('[name="EditColumns"]')[i].id.split("|");
            if ($('[name="EditColumns"]')[i].type == "checkbox") {
                Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].checked);
            }
            else {

                Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].value);
            }
        }
        var values = JSON.stringify(Values);
        var Id = JSON.stringify(ID);
        $.ajax({
            type: "POST",
            url: '/services/CrudService.svc/updateTableRow',
            data: JSON.stringify({
                'tablename': document.getElementsByTagName("tablename")[0].innerHTML,
                'Values': values,
                'ID': Id
            }),

            //data: Htmlname ,
            contentType: 'application/json; charset=utf-8',
            //dataType: 'json', 
            success: function (val) {

                columns = $('[name="EditColumns"]');
                for (var i = 1; i < columns.length; i++) {
                    if ($('[name="EditColumns"]')[i].type == "checkbox") {
                        $('[name="EditColumns"]')[i].checked = false;
                    }
                    else {
                        $('[name="EditColumns"]')[i].value = "";
                    }
                }
                //location.reload();
                $("#editModal").modal('hide');
                gridLoad();
                if (val.updateTableRowResult == "true") {
                    swal("", " Updated Successfully", "success")
                }
            },
            error: function (e) {

            }
        });


    });
});
function d3Chart(dataSource) {
    
    if (dataSource["data1"] != null && dataSource["data1"] != '' && dataSource["data1"] != "null") {
        var d3data = [{

            values: JSON.parse(dataSource["data"]),
            color: '#2D74B1',
            key: "Current",
            area: true
        }, {
            values: JSON.parse(dataSource["data1"]),
            color: '#2D74B1',
            key: "Previous",
            area: true
        }
        ]
    } else {
        var d3data = [{

            values: JSON.parse(dataSource["data"]),
            color: '#2D74B1',
            key: "Current",

            area: true
        }]
    }
    dataSource["d3data"] = d3data;

    if (dataSource["Chart_Type"].toLowerCase() == "bar")
        bar_chart(dataSource);
    else if (dataSource["Chart_Type"].toLowerCase() == "line" || dataSource["Chart_Type"].toLowerCase() == "area")
        line_chart(dataSource);
    else if (dataSource["Chart_Type"].toLowerCase() == "pie")
        pie_chart(dataSource);
    else if (dataSource["Chart_Type"].toLowerCase() == "donut")
        donut_chart(dataSource);
    else if (dataSource["Chart_Type"].toLowerCase() == "column")
        column_chart(dataSource)

}
function column_chart(dataSource) {
    
    nv.addGraph(function () {
        var columnchart = nv.models.discreteBarChart()
            .x(function (d) { return d[dataSource["Column_Number"]] })
            .y(function (d) { return d[dataSource["Row_Number"]] })
            .staggerLabels(true)
            .tooltips(false)
            .showValues(true);
        columnchart.discretebar.dispatch.on("elementClick", function (e) {

            //alert("You've clicked " + e.data[dataSource["Column_Number"]] + " " + e.data[dataSource["Row_Number"]]);

        });
        d3.select('#' + dataSource["ID"])
            .datum(dataSource["d3data"])
            .call(columnchart)

        nv.utils.windowResize(function () {
            d3.select('#' + dataSource["ID"]).call(columnchart)
        });

        return columnchart;
    });
}
function pie_chart(dataSource) {
    
    nv.addGraph(function () {
        
        var piechart = nv.models.pieChart()
            .x(function (d) { return d[dataSource["Column_Number"]] })
            .y(function (d) { return d[dataSource["Row_Number"]] })

            .showLabels(true)
             .margin({ top: 10, right: 10, bottom: 10, left: 10 })
        .tooltips(false);
        d3.select('#' + dataSource["ID"])
            .datum(JSON.parse(dataSource["data"]))
            .transition().duration(1200)
            .call(piechart);
        piechart.pie.dispatch.on("elementClick", function (e) {
            //alert("You've clicked " + e.data[dataSource["Column_Number"]]);
            if (dataSource["Link"] != null && dataSource["Link"] != "" && dataSource["Link"] != "null")
                layoutRefresh(window.location.origin + dataSource["Link"], 0);

        });
        return piechart;
    });
}
function donut_chart(dataSource) {
    
    nv.addGraph(function () {
        
        var donutchart = nv.models.pieChart()
            .x(function (d) { return d[dataSource["Column_Number"]] })
            .y(function (d) { return d[dataSource["Row_Number"]] })
            .showLabels(true)
            .labelThreshold(.05)
            .labelType("percent")
            .donut(true)
            .donutRatio(0.35)
         .tooltips(false);

        d3.select('#' + dataSource["ID"])
            .datum(JSON.parse(dataSource["data"]))
            .transition().duration(1200)
            .call(donutchart);
        donutchart.pie.dispatch.on("elementClick", function (e) {
            if (dataSource["Link"] != null && dataSource["Link"] != "" && dataSource["Link"] != "null")
                layoutRefresh(window.location.origin + dataSource["Link"], 0);

        });
        return donutchart;
    });
}
var dindex = -1;
var ddict = {};
function line_chart(dataSource) {
    
    var linechart;
    if (dataSource["Chart_Type"].toLowerCase() == "line")
        dataSource["d3data"][0]["area"] = false
    nv.addGraph(function () {
        
        linechart = nv.models.lineChart()

             .x(function (d) {

                 if (dataSource["d3data"][0]["values"].length - 1 > dindex) {
                     dindex++;
                     ddict[d[dataSource["Column_Number"]]] = dindex;

                     return dindex
                 }
                 else {
                     dindex = 0;
                     return ddict[d[dataSource["Column_Number"]]];
                 }
                 // to show Date propery on X axis
             })
             .y(function (d) {
                 return d[dataSource["Row_Number"]]
             })
         .tooltips(false);




        linechart.xAxis
            .showMaxMin(false)

             .tickFormat(function (d) {

                 return dataSource["d3data"][0]["values"][d][dataSource["Column_Number"]]
             });
        ;
        linechart.yAxis;

        linechart.lines.dispatch.on("elementClick", function (e) {

            //alert("You've clicked " + e.data[dataSource["Column_Number"]] + " " + e.data[dataSource["Row_Number"]]);
            if (dataSource["Link"] != null && dataSource["Link"] != "" && dataSource["Link"] != "null")
                layoutRefresh(window.location.origin + dataSource["Link"], 0);

        });

        d3.select('#' + dataSource["ID"]).datum(dataSource["d3data"]).call(linechart);
        nv.utils.windowResize(function () {
            d3.select('#' + dataSource["ID"]).call(linechart)
        });
        return linechart;
    });

}
function bar_chart(dataSource) {
    
    var barchart;
    nv.addGraph(function () {
        
        var barchart = nv.models.multiBarChart()
               .x(function (d) {
                   return d[dataSource["Column_Number"]] // to show Date propery on X axis
               })
               .y(function (d) {
                   return d[dataSource["Row_Number"]]
               })
              .duration(300)
              .margin({ bottom: 100, left: 70 })
              .rotateLabels(0)
             .tooltips(false)
              .groupSpacing(0.1);

        barchart.reduceXTicks(false).staggerLabels(true);
        barchart.xAxis;
        //barchart.tooltip.hidden(true)
        barchart.yAxis;

        barchart.multibar.dispatch.on("elementClick", function (e) {
            
            //alert("You've clicked " + e.data[dataSource["Column_Number"]] + " " + e.data[dataSource["Row_Number"]]);
            barchart.tooltip(false);
            if (dataSource["Link"] != null && dataSource["Link"] != "" && dataSource["Link"] != "null")
                layoutRefresh(window.location.origin + dataSource["Link"], 0);

        });

        d3.select('#' + dataSource["ID"]).datum(dataSource["d3data"]).call(barchart);
        d3.select(".nv-controlsWrap").style("visibility", "hidden");
        nv.utils.windowResize(function () {
            d3.select('#' + dataSource["ID"]).call(barchart)
        });
        return barchart;
    });
}
function loadD3charts() {
    var x = localStorage.getItem("Result");
    var role = JSON.parse(x);
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/getCharts?roleid=' + role.Role_ID + '&monitor=' + $("#PageTitle")[0].innerHTML.trim(),
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            
            //console.log(result);
            $("#loader").hide();
            GlobalDataSource = $.parseJSON(result);
            //console.log(GlobalDataSource);
            var temp = [];
            //console.log(x);
            var j;
            $("#chart_view").html("");
            for (j = 0; j < GlobalDataSource.length; j++) {
                if (GlobalDataSource[j]["data"] != "") {
                    //$("#Chart_Type|15")[0].disabled = true;
                    if (j % 2 == 0 && j != 0) GlobalDataSource[j].pr = 12;
                    else GlobalDataSource[j].pr = 6;


                    var chartTemp = "<div class= '" + GlobalDataSource[j]["ID"] + "_user-card' style='padding-right:5px; display:inline-table;margin-bottom:1% !important; float:left; padding-left:5px;height:" + GlobalDataSource[j]["Height"] + "px; width:" + GlobalDataSource[j]["Width"] + "px'><div class='box box-primary'><div class='box-header with-border'><h3 class='box-title'>" + GlobalDataSource[j]["Title"] + "</h3><div id='dropdown' class='box-tools pull-right'><button type='button' id='bar' class='UserChartEnable fa fa-bar-chart' style='background-color: transparent !important' onclick='changeChartType(this)'></button>"
                    chartTemp += "<button type='button' id='line' class='UserChartEnable fa fa-line-chart' style='background-color: transparent !important' onclick='changeChartType(this)'></button><button type='button' id='area' class='UserChartEnable fa fa-area-chart' style='background-color: transparent !important' onclick='changeChartType(this)'></button><button type='button' id='pie' class='UserChartEnable fa fa-pie-chart' style='background-color: transparent !important' onclick='changeChartType(this)'></button><button type='button' id='donut' class='UserChartEnable fa fa-dot-circle-o' style='background-color: transparent !important' onclick='changeChartType(this)'></button></div></div><div class='box-body' style='height:90%'><center><svg id=" + GlobalDataSource[j]["ID"] + " style='width:970px !important;height:" + GlobalDataSource[j]["Height"] + "px !important'></svg></center><center><div id='wrongmsg'> "

                    chartTemp += "<h3 id='WrongMsg" + GlobalDataSource[j]["ID"] + "' style='color:red;display:none'></h3></div></center></div></div> </div>"

                    $("#chart_view").append(chartTemp);
                    var x = localStorage.getItem("Result");
                    var UserDetails1 = JSON.parse(x);
                    if (UserDetails1.Role_ID != "1") {
                        $(".ChartEditButton").hide();
                    }
                    else if (UserDetails1.Role_ID == "1") {
                        $(".UserChartEnable").hide();
                    }

                    if (GlobalDataSource[j]["data"] != "false" && GlobalDataSource[j]["data"] != "Connection Failure") {
                        d3Chart(GlobalDataSource[j]);



                    }
                    else {
                        $("#WrongMsg" + GlobalDataSource[j]["ID"]).css("display", "block");
                        $("#WrongMsg" + GlobalDataSource[j]["ID"])[0].innerHTML = "Sorry! Something went wrong. Please check the KPI details and try again";
                    }

                }
            }
        },


        error: function (e) {
            //  alert("Something Wrong!!");
        }
    });
}
var row = 0;
var h = 0;
function loadNKPI() {
    var x = localStorage.getItem("Result");
    var role = JSON.parse(x);

    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/getKPI?roleid=' + role.Role_ID + '&monitor=' + $("#PageTitle")[0].innerHTML.trim(),
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            
            //console.log("KPI-----" + result);
            var colorCSS = ["amber_grad", "red_grad", "green_grad", "cyan_grad"];
            var dataSource = $.parseJSON(result);

            //console.log(dataSource);

            var temp = [];
            //console.log(x);
            var j;
            $("#chart_view").html("");
            $("#loader").hide();

            for (j = 0; j < dataSource.length; j++) {
                
                row++;

                var templateContent = "<h3 class='box-title'>" + dataSource[j].Title + "</h3>"

                var data = JSON.parse(dataSource[j]["data"]);

                for (k = 0; k < data.length; k++) {

                    data[k]["colorClass"] = colorCSS[h];
                    if (h < colorCSS.length - 1) {
                        h++;
                    }
                    else {
                        h = 0;
                    }
                    var kpiDiv = "<div class= 'col-lg-3 '><div class='small-cardbox " + data[k].colorClass + "'><span class='no col-sm-6'>" + data[k].value + "</span><h4 class='some-text col-sm-6'>" + data[k].category + "</h4></div></div>"
                    var kpitemplate = kpiDiv;

                    chartTemplate += kpiDiv;
                }


                $("#KPI").append(chartTemplate + "</div>");

            }
        },


        error: function (e) {
            // alert("Something Wrong!!");
        }
    });
}

function Insightclick() {
    if ($(".btn-insight")[0].attributes.insights.nodeValue == "NKPI") {
        $("#KPIRow").toggle(1500);
        loadNKPI();
    }
    else if ($(".btn-insight")[0].attributes.insights.nodeValue == "KPI") {

        $("#chart_view").toggle(1500);
        loadD3charts();

    }
}


