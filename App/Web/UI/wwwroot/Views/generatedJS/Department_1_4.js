
var DropDownDefaultValues={};
var DropDownTableValues = {};
DropDownDefaultValues["selectID"]=""
DropDownDefaultValues["selectName"]=""
DropDownDefaultValues["selectDescription"]=""
DropDownDefaultValues["selectCreateBy"]=""
DropDownDefaultValues["selectCreateDatetime"]=""
DropDownDefaultValues["selectUpdateBy"]=""
DropDownDefaultValues["selectUpdateDatetime"]=""
DropDownDefaultValues["selectStatus"]=""

function displayDropDownList(i,elementId)
{
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            
           
            var select = $('[name="Columns"]')[i];
            $('[name="Columns"]')[i].length=0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            console.log(key);
            select = $('[name="Columns"]')[i];
            var opt1 = '<option selected disabled hidden style=\"display: none\" value=\"1\"></option>';
            $('[name="Columns"]')[i].append(opt1);
            var key1 = Object.keys(DropDownDefaultValues);
            for (var j = 0; j < dropdownlist.length; j++) {
                
              //  var key = Object.keys(DropDownTableValues[0]);
                var opt = document.createElement('option');
                if(key.length==2){
                    opt.text = dropdownlist[j][key[1]];
                    opt.value = dropdownlist[j][key[0]];
                    for (var k= 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                else{
                    opt.text = opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                select.add(opt, j);
            }
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}

function editDisplayDropDownList(i,elementId)
{
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        async:false,
        success: function (val) {
            
            var value = $('[name="EditColumns"]')[i].value;
            var select = $('[name="EditColumns"]')[i];
            $('[name="EditColumns"]')[i].length=0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            console.log(key);
            select = $('[name="EditColumns"]')[i];
           
            
            for (var j = 0; j < dropdownlist.length; j++) {

               
                if(key.length==2){
                    if (dropdownlist[j][key[1]] != null && dropdownlist[j][key[0]] != null) {
                        var opt = document.createElement('option');
                        opt.text = dropdownlist[j][key[1]];
                        opt.id = elementId + "_" + dropdownlist[j][key[1]].replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                            
                            var res = "ASCII"+($0).charCodeAt();
                            return res
                        });
                        opt.value = dropdownlist[j][key[0]];
                        DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[1]];
                    }
                }
                else{
                    if (dropdownlist[j][key[0]] != null) {
                        var opt = document.createElement('option');
                        opt.text = opt.value = dropdownlist[j][key[0]];
                        opt.id = elementId + "_" + dropdownlist[j][key[0]].replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                            
                            var res = "ASCII"+($0).charCodeAt();
                            return res
                        });
                        DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[0]];
                    }
                }
                select.add(opt, j);
            }
            $('[name="EditColumns"]')[i].value = value;
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}
//function onSearch()
//{
    
//     var Filter = $("#Filter").val();

//    var grid = $("#grid").data("kendoGrid");
//    grid.dataSource.query({
//        page:1,
//        pageSize:10,
//        filter:{
//            logic:"or",
//            filters:[
//              { field: "ID", operator: "eq", value: parseInt(Filter) },
//              { field: "id1", operator: "contains", value: Filter },
//{field: "Name", operator:"contains",value:Filter} ,
//{field: "Description", operator:"contains",value:Filter} ,


//            ]

//        },
//        sort: { field: "ID", dir: "desc" },
//    });
//}
//document.addEventListener('keypress', function (e) {
//    
//    var key = e.which || e.keyCode;
//    if (key === 13) { // 13 is enter
//        $('#btnFilter').click();
//    }
//});
//function clearFilters() {
//    var grid = $("#grid").data("kendoGrid");
//    grid.dataSource.query({
//        page:1,
//        pageSize: 10,
//    });
//}
$(document).ready(function () {
    
    var x = localStorage.getItem("Result");
    var UserDetails = JSON.parse(x);
    $('#addModal').on('show.bs.modal', function () {
      
        columns = $('[name="Columns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                displayDropDownList(i, elementId)
            }
        }
    })
    $('#editModal').on('show.bs.modal', function () {
      
        columns = $('[name="EditColumns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                editDisplayDropDownList(i,elementId);
            }
        }
    });

    
    if($(".btn-insight")[0].attributes.insights.nodeValue=="NKPI")
    {
        $("#loader").show();
        loadNKPI();
    }
    else if($(".btn-insight")[0].attributes.insights.nodeValue=="KPI")
    {
        $("#loader").show();
        loadCharts();
    }
    
    //$("#btnFilter").kendoButton({
    //    click: onSearch
    //})
    //$("#btnClear").kendoButton ({
    //    click: clearFilters
    //})


});
gridLoad();
function gridLoad()
{
   
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/getTableData?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML,

        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
    //dataType: 'json',

    success: function (val) {
        var dataSource = $.parseJSON(val);
        var columns = $('[name="EditColumns"]');
        for (i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                editDisplayDropDownList(i, elementId);
                
                for (j = 0; j < dataSource.length ; j++) {
                    dataSource[j][ElementDetails[0]] = DropDownTableValues[dataSource[j][ElementDetails[0]]];
                }
            }
        }
        var t = $('#grid').DataTable({
            destroy: true,
            "scrollY": true,
            "scrollX": true,
            "data": dataSource,
            "Info": false,
            "pagingType": "simple_numbers",
            "LengthChange": true,
            "aLengthMenu": [[20, 40, 60, -1], [20, 40, 60, "All"]],
            "iDisplayLength": 20,
            "order": [[0, 'desc']],
            "columns": [
                {
                    "data": "ID",
                    "title": "Id"
                },
                {
                    "data": "Name",
                    "title": "Name"
                },
                {
                    "data": "Description",
                    "title": "Description"
                },
                {
                    "title": "Actions",
                    "defaultContent": "<button class='btn btn-warning btn-flat' title='Edit' onclick='editFunction(this)'><i class='fa fa-edit' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Edit</span></button>&nbsp;<button class='deleteBtnClass btn btn-flat' title='Delete' onclick='deleteFunction(this)'><i class='fa fa-trash' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Delete</span></button>"
                },

            ],"oLanguage": { "sSearch": "" },
            "columnDefs": [{
                "targets": '_all',
                "defaultContent": "",
            },
            {
                "aTargets": [0],
                "sClass": "hidden"
            },
             {
                 "targets": [0],
                 "orderable": true
             }, ],

        });
        t.page('first').draw('page');
        $("#grid_wrapper .dataTables_length label").css("display", "block");
        $(".dataTables_filter").addClass("pull-left");
        $(".dataTables_length").addClass("pull-right");
        $($('#grid_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
    },
//        for (i = 0; i < dataSource.length; i++) {
//            if(dataSource[i].ID!=null){

//dataSource[i].ID= dataSource[i].ID.toString()
//dataSource[i].id1 = dataSource[i].ID.toString()
//}

//if(dataSource[i].Name!=null){

//dataSource[i].Name= dataSource[i].Name.toString()

//}

//if(dataSource[i].Description!=null){

//dataSource[i].Description= dataSource[i].Description.toString()

//}


            
//        }

//        $("#grid").kendoGrid({
//            dataSource: {
//                data: dataSource,
//                pageSize: 10,
//                schema: {
//                    model: {
//                        id:"ID",

//fields:{

//ID:{type: "number", editable: false },
//id1: { type: "text" },
//Name:{type: "text"},

//Description:{type: "text"},

//}

//                        }
//                },
//                sort: { field: "ID", dir: "desc" },
//            },
//            sortable: true,
            
//            //pageable: true,
//            pageable: {
//                messages: {
//                    display: "{0} - {1} of {2} Items"
//                }
//            },
              
//            columns:  [{field:"ID", title:" Id",width:"120px"},
                
//{field:"Name", title:" Name",width:"120px"},

//{field:"Description", title:" Description",width:"120px"},
//{ field: "id1", title: " id1", hidden: true, width: "120px" },
     
//            { command: [{ title: "Edit", template: "<button title='Edit' class='btn btn-warning btn-flat' onclick='editFunction(this)'><span class='editIcon'></span> </button>" }, { title: "Delete", template: "<button title='Delete' class='deleteBtnClass btn btn-flat' onclick='deleteFunction(this)'><span class='deleteIcon'></span>  </button>", }], title: "Action", width: "160px" }],
      
////edit: function (e) {
////    var updateBtn = e.container.find(".k-grid-update");
////    updateBtn.removeClass("k-grid-update");
                
////    //removing this class will prevent the grid from saving on click
////    updateBtn.click(function () {

////        updateFunction(); //call the function you wish
////        updateBtn.addClass("k-grid-update");
////    })
                   

     
//});


//},
error: function (e) {
    //alert("Something Wrong!!");
    gridLoad();
}
});
}

function editFunction(e) {
    
    
    $('#editModal').modal('show');
   
    var tr = $(e)[0].closest("tr");
    var td = tr.children;
    var columns = $('[name="EditColumns"]');
    //console.log(td);
    for(var i = 0; i <columns.length; i++) {
        if(i==0){
            $('[name="EditColumns"]')[i].innerHTML =$('[name="EditColumns"]')[i].value= td[i].innerHTML;
        }
        else if ($('[name="EditColumns"]')[i].localName == "select")
        {
            //$('[name="EditColumns"]')[i].value = $("#" + td[i].innerHTML.replace(" ","")).val();
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
            $('[name="EditColumns"]')[i].value = $("#" + $('[name="EditColumns"]')[i].id.split("|")[1] + "_" + td[i].innerHTML.replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                
                var res = "ASCII" + ($0).charCodeAt();
                return res
            })).val().replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        else {
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
            $('[name="EditColumns"]')[i].value = td[i].innerHTML.replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        //console.log($('[name="EditColumns"]')[i].value);
    }
    
}
function deleteFunction(e) {
    
    swal("Are you sure you want to delete this record?", {
        buttons: ["Cancel", "Ok"],
        className: "deleterecord",
        closeOnClickOutside: false,
        closeOnEsc: false,
    });
    $(".deleterecord .swal-footer .swal-button-container .swal-button--confirm").on("click", function () {
        $('#deleteModal').modal('hide');
        var data = {
        }
        data[$("#deleteName")[0].innerHTML] = $("#deleteId")[0].innerHTML
        var IdData = JSON.stringify(data);
        $.ajax({
            type: "POST",
            url: '/services/CrudService.svc/DeleteTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML + '&ID=' + IdData,
            contentType: 'application/json; charset=utf-8',
            success: function (val) {
                if (val == "Success") {
                    swal("", "Record Deleted Successfully", "success")
                }
                else {
                    $("#feedbackMsg").html("Sorry cannot perform such operation");
                }
                gridLoad();
            },
            error: function (e) {
                $("#feedbackMsg").html("Something Wrong.");
            }
        });
    });
    $("#deleteId")[0].innerHTML = $($(e).closest("tr")[0]).children('td:first-child')[0].innerHTML
    $("#deleteName")[0].innerHTML = $($($("#grid").find('thead')[0]).find('tr')[0]).find('th')[0].innerText.trim().toUpperCase()
}
//function deleteFunction(e) {
//   // e.preventDefault();
//   // var dataItem = $(this).closest('tr').children('td:first').innerHTML;
//    var r=confirm("Are you sure you want to delete this record");
//    if(r){
//        var tr = $(e)[0].closest("tr");
//        var uid = $(tr).children('td:first-child')[0].innerHTML;
    
//        var x = $('th')[0];
//        var columnName=$(x).attr('data-field');
//        var data={    
//        }
//        data[$(x).attr('data-field')]=uid;
   
//        console.log(data);
//        var IdData = JSON.stringify(data);
//        $.ajax({
//            type: "POST",
//            url: '/services/CrudService.svc/DeleteTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML+'&ID=' + IdData,
//            //data: Htmlname ,
//            contentType: 'application/json; charset=utf-8',
//            //dataType: 'json',

//            success: function (val) {
//                //alert(val);

                
//                gridLoad();
//                if(val == "true"){

//                }
//                    // $("#feedbackMsg").html("Success");
                
//                else {
//                    $("#feedbackMsg").html("Sorry cannot perform such operation");
//                }
//            },
//            error: function (e) {
//                $("#feedbackMsg").html("Something Wrong.");
//            }
//        });
//    }
//}


$(document).ready(function () {
    //var errorTemplate = '<div class="k-widget k-tooltip k-tooltip-validation"' +
    //         'style="margin:0.5em"><span class="k-icon k-warning"> </span>' +
    //         '#=message#<div class="k-callout k-callout-n"></div></div>'

    //var validator = $("#SubmitForm").kendoValidator({
    //    errorTemplate: errorTemplate
    //}).data("kendoValidator");

    //var tooltip = $("#SubmitForm").kendoTooltip({
    //    filter: ".k-invalid",
    //    content: function (e) {
    //        var id = e.target.attr("id").split("|")[0] || e.target.closest(".k-widget").find(".k-invalid:input").attr("id");
    //        //    var errorMessage = $("#SubmitForm").find("[data-for=" + id + "]");

    //        return '<span class="k-icon k-warning"> </span>' + id + " is required  ";
    //    },
    //    show: function () {
    //        this.refresh();
    //    }
    //});
    //var validator1 = $("#UpdateForm").kendoValidator({
    //    errorTemplate: errorTemplate
    //}).data("kendoValidator");

    //var tooltip = $("#UpdateForm").kendoTooltip({
    //    filter: ".k-invalid",
    //    content: function (e) {
    //        
    //        var id = e.target.attr("id").split("|")[0] || e.target.closest(".k-widget").find(".k-invalid:input").attr("id");
    //        //    var errorMessage = $("#SubmitForm").find("[data-for=" + id + "]");

    //        return '<span class="k-icon k-warning"> </span>' + id + " is required  ";
    //    },
    //    show: function () {
    //        this.refresh();
    //    }
    //});
    $("#SubmitForm").on("submit", function(e) { 
        e.preventDefault();
        //if (validator.validate()) {
            var Values = {
            }
            var Columns=$('[name="Columns"]');
            var len=$('[name="Columns"]').length;
            for(var i=1;i<len;i++)
            {
                var ID = $('[name="Columns"]')[i].id;
                var columnName = ID.split("|");
                if(columnName[0].toUpperCase()=="CREATEBY")
                {
                    Values[columnName[0]] = window.btoa(JSON.parse(localStorage.Result).User_ID);
                }
                else if (columnName[0].toUpperCase() == "CREATEDATETIME") {
                    Values[columnName[0]] = window.btoa(GetNow());
                }
                else if(columnName[0].toUpperCase()=="STATUS")
                    Values[columnName[0]] = window.btoa(true);
                else
                {
                    if($('[name="Columns"]')[i].type=="checkbox")
                        Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].checked);
                    else
                        Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].value);
                }
            }

            var values = JSON.stringify(Values);
            $.ajax({
                type: "POST",
                url: '/services/CrudService.svc/createTableRow',
                data: JSON.stringify({
                    "tablename": document.getElementsByTagName("tablename")[0].innerHTML,
                    "Values": values
                }),

                //data: Htmlname ,
                contentType: 'application/json; charset=utf-8',
                //dataType: 'json', 
                success: function (val) {
                    if (val.createTableRowResult == "true") {

                    }
                    //location.reload();
                    $("#addModal .close").click();
                    gridLoad();
                },
                error: function (e) {

                }
            });
        //}
    });
    $("#UpdateForm").on("submit", function (e) {
        
        e.preventDefault();
        //if (validator1.validate()) {
            var IDName=$('[name="EditColumns"]')[0].id.split("|");
            var ID = {};
            ID[IDName[0]] = $('[name="EditColumns"]')[0].value;
            columns = $('[name="EditColumns"]');
            var Values = {
                "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
                //  "UpdateDatetime": window.btoa(GetNow())
            }
            for (var i = 1; i < columns.length; i++) {
                var z = $('[name="EditColumns"]')[i].id.split("|");
                if($('[name="EditColumns"]')[i].type=="checkbox"){
                    Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].checked);
                }
                else
                {
           
                    Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].value);
                }
            }
            var values = JSON.stringify(Values);
            var Id = JSON.stringify(ID);
            $.ajax({
                type: "POST",
                url: '/services/CrudService.svc/updateTableRow',
                data: JSON.stringify({
                    'tablename': document.getElementsByTagName("tablename")[0].innerHTML,
                    'Values': values,
                    'ID': Id
                }),

                //data: Htmlname ,
                contentType: 'application/json; charset=utf-8',
                //dataType: 'json', 
                success: function (val) {
                
                    //location.reload();
                    $("#editModal .close").click();
                    gridLoad();
                    if (val.updateTableRowResult == "true") {
                        swal("", "Updated Successfully", "success");
                    }
                },
                error: function (e) {

                }
            });
        //}
    });
});
function d3Chart(dataSource) {
    
    if (dataSource["data1"] != null && dataSource["data1"] != '' && dataSource["data1"] != "null") {
        var d3data = [{

            values: JSON.parse(dataSource["data"]),
            color: '#2D74B1',
            key: "Current",
            area: true
        }, {
            values: JSON.parse(dataSource["data1"]),
            color: '#2D74B1',
            key: "Previous",
            area: true
        }
        ]
    } else {
        var d3data = [{

            values: JSON.parse(dataSource["data"]),
            color: '#2D74B1',
            key: "Current",

            area: true
        }]
    }
    dataSource["d3data"] = d3data;

    if (dataSource["Chart_Type"].toLowerCase() == "bar")
        bar_chart(dataSource);
    else if (dataSource["Chart_Type"].toLowerCase() == "line" || dataSource["Chart_Type"].toLowerCase() == "area")
        line_chart(dataSource);
    else if (dataSource["Chart_Type"].toLowerCase() == "pie")
        pie_chart(dataSource);
    else if (dataSource["Chart_Type"].toLowerCase() == "donut")
        donut_chart(dataSource);
    else if (dataSource["Chart_Type"].toLowerCase() == "column")
        column_chart(dataSource)


}

function column_chart(dataSource) {
    
    nv.addGraph(function () {
        var columnchart = nv.models.discreteBarChart()
            .x(function (d) { return d[dataSource["Column_Number"]] })
            .y(function (d) { return d[dataSource["Row_Number"]] })
            .staggerLabels(true)
            .tooltips(false)
            .showValues(true);
        columnchart.discretebar.dispatch.on("elementClick", function (e) {

            //alert("You've clicked " + e.data[dataSource["Column_Number"]] + " " + e.data[dataSource["Row_Number"]]);

        });
        d3.select('#' + dataSource["ID"])
            .datum(dataSource["d3data"])
            .call(columnchart)

        nv.utils.windowResize(function () {
            d3.select('#' + dataSource["ID"]).call(columnchart)
        });

        return columnchart;
    });
}
function pie_chart(dataSource) {
    
    nv.addGraph(function () {
        
        var piechart = nv.models.pieChart()
            .x(function (d) { return d[dataSource["Column_Number"]] })
            .y(function (d) { return d[dataSource["Row_Number"]] })

            .showLabels(true)
             .margin({ top: 10, right: 10, bottom: 10, left: 10 })
        .tooltips(false);
        d3.select('#' + dataSource["ID"])
            .datum(JSON.parse(dataSource["data"]))
            .transition().duration(1200)
            .call(piechart);
        piechart.pie.dispatch.on("elementClick", function (e) {
            //alert("You've clicked " + e.data[dataSource["Column_Number"]]);
            layoutRefresh(window.location.origin + dataSource["Link"], 0);

        });
        return piechart;
    });
}
function donut_chart(dataSource) {
    
    nv.addGraph(function () {
        
        var donutchart = nv.models.pieChart()
            .x(function (d) { return d[dataSource["Column_Number"]] })
            .y(function (d) { return d[dataSource["Row_Number"]] })
            .showLabels(true)
            .labelThreshold(.05)
            .labelType("percent")
            .donut(true)
            .donutRatio(0.35)
         .tooltips(false);

        d3.select('#' + dataSource["ID"])
            .datum(JSON.parse(dataSource["data"]))
            .transition().duration(1200)
            .call(donutchart);
        donutchart.pie.dispatch.on("elementClick", function (e) {

            layoutRefresh(window.location.origin + dataSource["Link"], 0);

        });
        return donutchart;
    });
}

var dindex = -1;
var ddict = {};
function line_chart(dataSource) {
    
    var linechart;
    if (dataSource["Chart_Type"].toLowerCase() == "line")
        dataSource["d3data"][0]["area"] = false
    nv.addGraph(function () {
        
        linechart = nv.models.lineChart()

             .x(function (d) {

                 if (dataSource["d3data"][0]["values"].length - 1 > dindex) {
                     dindex++;
                     ddict[d[dataSource["Column_Number"]]] = dindex;

                     return dindex
                 }
                 else {
                     dindex = 0;
                     return ddict[d[dataSource["Column_Number"]]];
                 }
                 // to show Date propery on X axis
             })
             .y(function (d) {
                 return d[dataSource["Row_Number"]]
             })
         .tooltips(false);




        linechart.xAxis
            .showMaxMin(false)

             .tickFormat(function (d) {

                 return dataSource["d3data"][0]["values"][d][dataSource["Column_Number"]]
             });
        ;
        linechart.yAxis;

        linechart.lines.dispatch.on("elementClick", function (e) {

            //alert("You've clicked " + e.data[dataSource["Column_Number"]] + " " + e.data[dataSource["Row_Number"]]);
            layoutRefresh(window.location.origin + dataSource["Link"], 0);

        });

        d3.select('#' + dataSource["ID"]).datum(dataSource["d3data"]).call(linechart);
        nv.utils.windowResize(function () {
            d3.select('#' + dataSource["ID"]).call(linechart)
        });
        return linechart;
    });

}
function bar_chart(dataSource) {
    
    var barchart;
    nv.addGraph(function () {
        
        var barchart = nv.models.multiBarChart()
               .x(function (d) {
                   return d[dataSource["Column_Number"]] // to show Date propery on X axis
               })
               .y(function (d) {
                   return d[dataSource["Row_Number"]]
               })
              .duration(300)
              .margin({ bottom: 100, left: 70 })
              .rotateLabels(0)
             .tooltips(false)
              .groupSpacing(0.1);

        barchart.reduceXTicks(false).staggerLabels(true);
        barchart.xAxis;
        //barchart.tooltip.hidden(true)
        barchart.yAxis;

        barchart.multibar.dispatch.on("elementClick", function (e) {
            
            //alert("You've clicked " + e.data[dataSource["Column_Number"]] + " " + e.data[dataSource["Row_Number"]]);
            barchart.tooltip(false)
            layoutRefresh(window.location.origin + dataSource["Link"], 0);

        });

        d3.select('#' + dataSource["ID"]).datum(dataSource["d3data"]).call(barchart);
        d3.select(".nv-controlsWrap").style("visibility", "hidden");
        nv.utils.windowResize(function () {
            d3.select('#' + dataSource["ID"]).call(barchart)
        });
        return barchart;
    });
}
function loadD3charts() {
    
    var x = localStorage.getItem("Result");
    var role = JSON.parse(x);
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/getCharts?roleid=' + role.Role_ID + '&monitor=' + $("#PageTitle")[0].innerHTML.trim(),
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            
            console.log(result);
            $("#loader").hide();
            GlobalDataSource = $.parseJSON(result);
            console.log(GlobalDataSource);
            var temp = [];
            var j;
            $("#chart_view").html("");
            for (j = 0; j < GlobalDataSource.length; j++) {
                if (GlobalDataSource[j]["data"] != "") {
                    //$("#Chart_Type|15")[0].disabled = true;
                    if (j % 2 == 0 && j != 0) GlobalDataSource[j].pr = 12;
                    else GlobalDataSource[j].pr = 6;
                    var chartTemp = "<div class= '" + GlobalDataSource[j]["ID"] + "_user-card' style='padding-right:5px; display:inline-table;margin-bottom:1% !important; float:left; padding-left:5px;height:" + GlobalDataSource[j]["Height"] + "px; width:" + GlobalDataSource[j]["Width"] + "%'><div class='box box-primary'><div class='box-header with-border'><h3 class='box-title'>" + GlobalDataSource[j]["Title"] + "</h3><div id='dropdown' class='box-tools pull-right'><button type='button' id=" + GlobalDataSource[j]["KPI_ID"] + " class='ChartEditButton fas fa-wrench' onclick='editsettingsFunction(this)' style='background-color: transparent !important'></button><button type='button' id='bar' class='UserChartEnable fa fa-bar-chart' style='background-color: transparent !important' onclick='changeChartType(this)'></button><button type='button' id='line' class='UserChartEnable fa fa-line-chart' style='background-color: transparent !important' onclick='changeChartType(this)'></button><button type='button' id='area' class='UserChartEnable fa fa-area-chart' style='background-color: transparent !important' onclick='changeChartType(this)'></button><button type='button' id='pie' class='UserChartEnable fa fa-pie-chart' style='background-color: transparent !important' onclick='changeChartType(this)'></button><button type='button' id='donut' class='UserChartEnable fa fa-dot-circle-o' style='background-color: transparent !important' onclick='changeChartType(this)'></button></div></div><div class='box-body' style='height:90%'><center><svg id=" + GlobalDataSource[j]["ID"] + " style='width:97% !important;height:" + GlobalDataSource[j]["Height"] + "px !important'></svg></center><center><div id='wrongmsg'> <h3 id='WrongMsg" + GlobalDataSource[j]["ID"] + "' style='color:#B71C1C;display:none'></h3></div></center></div></div> </div>"
                    $("#chart_view").append(chartTemp);
                    var x = localStorage.getItem("Result");
                    var UserDetails1 = JSON.parse(x);
                    if (UserDetails1.Role_ID != "1") {
                        $(".ChartEditButton").hide();
                    }
                    else if (UserDetails1.Role_ID == "1") {
                        $(".UserChartEnable").hide();
                    }

                    if (GlobalDataSource[j]["data"] != "false" && GlobalDataSource[j]["data"] != "Connection Failure") {
                        d3Chart(GlobalDataSource[j]);



                    }
                    else {
                        $("#WrongMsg" + GlobalDataSource[j]["ID"]).css("display", "block");
                        $("#WrongMsg" + GlobalDataSource[j]["ID"])[0].innerHTML = "Sorry! Something went wrong. Please check the KPI details and try again";
                    }

                }
            }
        },


        error: function (e) {
            //  alert("Something Wrong!!");
        }
    });
}
var row = 0;
var h = 0;
function loadNKPI() {
    var x = localStorage.getItem("Result");
    var role = JSON.parse(x);

    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/getKPI?roleid=' + role.Role_ID + '&monitor=' + $("#PageTitle")[0].innerHTML.trim(),
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            
            var colorCSS = ["amber_grad", "red_grad", "green_grad", "cyan_grad"];
            var dataSource = $.parseJSON(result);
            var temp = [];
            var j;
            $("#KPI").html("");
            $("#loader").hide();

            for (j = 0; j < dataSource.length; j++) {

                row++;

                var templateContent = "";

                var data = JSON.parse(dataSource[j]["data"]);

                for (k = 0; k < data.length; k++) {

                    data[k]["colorClass"] = colorCSS[h];
                    if (h < colorCSS.length - 1) {
                        h++;
                    }
                    else {
                        h = 0;
                    }
                    var kpiDiv = "<div class='col-lg-3'><div class='small-cardbox " + data[k].colorClass + "'><span class='no col-sm-6'>" + data[k].value + "</span><h4 class='some-text col-sm-6'>" + data[k].category + "</h4><button class='details btn-link show' onclick='" + dataSource[j].Group + "'><i class='fa fa-arrow-right'></i> View Details</button>";

                    templateContent += kpiDiv;
                }
                templateContent += "</div></div>";

                $("#KPI").append(templateContent);

            }
        },


        error: function (e) {
            // alert("Something Wrong!!");
        }
    });
}
function Insightclick(){
    if ($(".btn-insight")[0].attributes.insights.nodeValue=="NKPI")
    {
        $("#KPIRow").toggle(1500);
    }
    else if($(".btn-insight")[0].attributes.insights.nodeValue=="KPI")
    {
       
        $("#chart_view").toggle(1500);
        
    }  
}


