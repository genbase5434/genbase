
var DropDownDefaultValues = {};
var DropDownTableValues = {};
DropDownDefaultValues["selectID"] = ""
DropDownDefaultValues["selectModule"] = ""
DropDownDefaultValues["selectCreate"] = ""
DropDownDefaultValues["selectView"] = ""
DropDownDefaultValues["selectUpdate"] = ""
DropDownDefaultValues["selectDelete"] = ""
DropDownDefaultValues["selectRole_ID"] = ""
DropDownDefaultValues["selectCreateBy"] = ""
DropDownDefaultValues["selectCreateDatetime"] = ""
DropDownDefaultValues["selectUpdateBy"] = ""
DropDownDefaultValues["selectUpdateDatetime"] = ""
DropDownDefaultValues["selectStatus"] = ""

function displayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/genericScreenDropDown?elementId=' + elementId,
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            var select = $('[name="Columns"]')[i];
            $('[name="Columns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            select = $('[name="Columns"]')[i];
            var opt1 = '<option selected disabled hidden style=\"display: none\" value=\"1\"></option>';
            $('[name="Columns"]')[i].append(opt1);
            var key1 = Object.keys(DropDownDefaultValues);
            for (var j = 0; j < dropdownlist.length; j++) {
                var opt = document.createElement('option');
                if (key.length == 2) {
                    opt.text = dropdownlist[j][key[1]];
                    opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                else {
                    opt.text = opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                select.add(opt, j);
            }
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}
function editDisplayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/genericScreenDropDown?elementId=' + elementId,
        contentType: 'application/json; charset=utf-8',
        async: false,
        success: function (val) {
            var value = $('[name="EditColumns"]')[i].value;
            var select = $('[name="EditColumns"]')[i];
            $('[name="EditColumns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            select = $('[name="EditColumns"]')[i];
            for (var j = 0; j < dropdownlist.length; j++) {
                if (key.length == 2) {
                    if (dropdownlist[j][key[1]] != null && dropdownlist[j][key[0]] != null) {
                        var opt = document.createElement('option');
                        opt.text = dropdownlist[j][key[1]];
                        opt.id = elementId + "_" + dropdownlist[j][key[1]].replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                            var res = "ASCII" + ($0).charCodeAt();
                            return res
                        });
                        opt.value = dropdownlist[j][key[0]];
                        DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[1]];
                    }
                }
                else {
                    if (dropdownlist[j][key[0]] != null) {
                        var opt = document.createElement('option');
                        opt.text = opt.value = dropdownlist[j][key[0]];
                        opt.id = elementId + "_" + dropdownlist[j][key[0]].replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                            var res = "ASCII" + ($0).charCodeAt();
                            return res
                        });
                        DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[0]];
                    }
                }
                select.add(opt, j);
            }
            $('[name="EditColumns"]')[i].value = value;
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}
var RoleGridData;
$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableData?tablename=ROLE',
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            RoleGridData = JSON.parse(result);
            for (var i in RoleGridData) {
                $("#role").append('<option value="' + RoleGridData[i]["ID"] + '">' + RoleGridData[i]["RoleName"] + '</option>')
            }
        },
        error: function (e) {
        }
    });
    var x = localStorage.getItem("Result");
    var UserDetails = JSON.parse(x);
    $('#addModal').on('show.bs.modal', function () {

        columns = $('[name="Columns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                displayDropDownList(i, elementId)
            }
        }
    })
    $('#editModal').on('show.bs.modal', function () {

        columns = $('[name="EditColumns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                editDisplayDropDownList(i, elementId);
            }
        }
    });
    gridLoad();
    $("#role").on("change", function () {
        RoleDatasource = [];
        for (var i in dataSource) {
            if ($("#role").val() == dataSource[i]["Role_ID"].toString()) {
                RoleDatasource.push(dataSource[i]);
            }
        }
        var filterCharts = $("#role").val();
        if (filterCharts) {
            var t = $('#grid').DataTable({
                destroy: true,
                "order": [[0, 'desc']],
                "scrollY": true,
                "scrollX": true,
                "data": RoleDatasource,
                "Info": false,
                "pagingType": "simple_numbers",
                "LengthChange": true,
                "aLengthMenu": [[20, 40, 60, -1], [20, 40, 60, "All"]],
                "iDisplayLength": 20,
                "columns": [
                    {
                        "data": "ID",
                        "title": "ID"
                    },
                    {
                        "data": "Module",
                        "title": "Module"
                    },
                    {
                        "data": "Create",
                        "title": "Create"
                    },
                    {
                        "data": "View",
                        "title": "View"
                    },
                    {
                        "data": "Update",
                        "title": "Update"
                    },
                     {
                         "data": "Delete",
                         "title": "Delete"
                     },
                      {
                          "data": "Role_ID",
                          "title": "Role"
                      },
                    {
                        "title": "Action",
                        "defaultContent": "<button class='btn btn-warning btn-flat' title='Edit' onclick='editFunction(this)'><i class='fa fa-edit' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Edit</span></button>"
                    },

                ], "oLanguage": { "sSearch": "" },
                "columnDefs": [{
                    "targets": '_all',
                    "defaultContent": "",
                },
                {
                    "aTargets": [0],
                    "sClass": "hidden"
                },
                {
                    "targets": [0],
                    "orderable": true
                },
                {
                    "aTargets": [6],
                    "sClass": "hidden"
                }
                ],

            });
            t.page('first').draw('page');
            $("#grid_wrapper .dataTables_length label").css("display", "block");
            $(".dataTables_filter").addClass("pull-left");
            $(".dataTables_length").addClass("pull-right");
            $($('#grid_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
        }
    });
});
var dataSource;
var AdminDatasource = []; var AnalystDatasource = []; var UserDatasource = [];
var RoleDatasource = [];
function gridLoad() {
    $("#loader").show();
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableData?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML,
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            dataSource = $.parseJSON(val);
            var columns = $('[name="EditColumns"]');
            for (i = 0; i < columns.length; i++) {
                if (columns[i].type == "select-one") {
                    var ID = columns[i].id;
                    var ElementDetails = ID.split("|");
                    var elementId = ElementDetails[1];
                    editDisplayDropDownList(i, elementId);
                    for (j = 0; j < dataSource.length ; j++) {
                        dataSource[j][ElementDetails[0]] = DropDownTableValues[dataSource[j][ElementDetails[0]]];
                    }
                }
            }
            for (i = 0; i < dataSource.length; i++) {
                if (dataSource[i]["Create"] == true) {
                    dataSource[i]["Create"] = "Yes"
                }
                else {
                    dataSource[i]["Create"] = "No"
                }
                if (dataSource[i]["View"] == true) {
                    dataSource[i]["View"] = "Yes"
                }
                else {
                    dataSource[i]["View"] = "No"
                }
                if (dataSource[i]["Update"] == true) {
                    dataSource[i]["Update"] = "Yes"
                }
                else {
                    dataSource[i]["Update"] = "No"
                }
                if (dataSource[i]["Delete"] == true) {
                    dataSource[i]["Delete"] = "Yes"
                }
                else {
                    dataSource[i]["Delete"] = "No"
                }
            }
            for (var i in dataSource) {
                if ($("#role").val() == dataSource[i]["Role_ID"].toString()) {
                    RoleDatasource.push(dataSource[i]);
                }
            }
            var t = $('#grid').DataTable({
                destroy: true,
                "order": [[0, 'desc']],
                "scrollY": true,
                "scrollX": true,
                "data": RoleDatasource,
                "Info": false,
                "pagingType": "simple_numbers",
                "LengthChange": true,
                "aLengthMenu": [[20, 40, 60, -1], [20, 40, 60, "All"]],
                "iDisplayLength": 20,
                "columns": [
                    {
                        "data": "ID",
                        "title": "ID"
                    },
                    {
                        "data": "Module",
                        "title": "Module"
                    },
                    {
                        "data": "Create",
                        "title": "Create"
                    },
                    {
                        "data": "View",
                        "title": "View"
                    },
                    {
                        "data": "Update",
                        "title": "Update"
                    },
                     {
                         "data": "Delete",
                         "title": "Delete"
                     },
                      {
                          "data": "Role_ID",
                          "title": "Role"
                      },
                    {
                        "title": "Actions",
                        "defaultContent": "<button class='btn btn-warning btn-flat' title='Edit' onclick='editFunction(this)'><i class='fa fa-edit' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Edit</span></button>"
                    },

                ], "oLanguage": { "sSearch": "" },
                "columnDefs": [{
                    "targets": '_all',
                    "defaultContent": "",
                },
                {
                    "aTargets": [0],
                    "sClass": "hidden"
                },
                {
                    "targets": [0],
                    "orderable": true
                },
                {
                    "aTargets": [6],
                    "sClass": "hidden"
                }
                ],

            });
            t.page('first').draw('page');
            $("#grid_wrapper .dataTables_length label").css("display", "block");
            $(".dataTables_filter").addClass("pull-left");
            $(".dataTables_length").addClass("pull-right");
            $($('#grid_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
			$("#loader").hide();
		},
        error: function (e) {
        }
    });
}

function editFunction(e) {
    $('#editModal').modal('show');
    var tr = $(e)[0].closest("tr");
    var td = tr.children;
    var columns = $('[name="EditColumns"]');
    for (var i = 0; i < columns.length; i++) {
        if (i == 0) {
            $('[name="EditColumns"]')[i].innerHTML = $('[name="EditColumns"]')[i].value = td[i].innerHTML;
        }
        if ($('[name="EditColumns"]')[i].type == "checkbox") {
            if (td[i].innerHTML == "Yes" || td[i].innerHTML == "true") {


                $('[name="EditColumns"]')[i].checked = true;
            }
            else {
                $('[name="EditColumns"]')[i].checked = false;
            }

        }
        else if ($('[name="EditColumns"]')[i].localName == "select") {
            $('[name="EditColumns"]')[i].value = $("#" + $('[name="EditColumns"]')[i].id.split("|")[1] + "_" + td[i].innerHTML.replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                var res = "ASCII" + ($0).charCodeAt();
                return res
            })).val();
        }
        else {
            $('[name="EditColumns"]')[i].value = td[i].innerHTML;
        }
    }

}
function deleteFunction(e) {
    $('#deleteModal').modal('show');
    $("#deleteId")[0].innerHTML = $($(e).closest("tr")[0]).children('td:first-child')[0].innerHTML
    $("#deleteName")[0].innerHTML = $($($("#grid").find('thead')[0]).find('tr')[0]).find('th')[0].innerText.trim().toUpperCase()
}
function delconfirmFunction(e) {
    $('#deleteModal').modal('hide');
    var data = {}
    data[$("#deleteName")[0].innerHTML] = $("#deleteId")[0].innerHTML
    var IdData = JSON.stringify(data);
    $.ajax({
        type: "POST",
        url: GlobalURL + '/api/CrudService/deleteTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML + '&ID=' + IdData,
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            gridLoad();
            if (val == "true") {
                swal("", "Record Deleted Successfully", "success")
            }
            else {
                $("#feedbackMsg").html("Sorry cannot perform such operation");
            }
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}
$(document).ready(function () {
    $("#SubmitForm").on("submit", function (e) {
        e.preventDefault();
            var Values = {
            }
            var Columns = $('[name="Columns"]');
            var len = $('[name="Columns"]').length;
            for (var i = 1; i < len; i++) {
                var ID = $('[name="Columns"]')[i].id;
                var columnName = ID.split("|");
                if (columnName[0].toUpperCase() == "CREATEBY") {
                    Values[columnName[0]] = window.btoa(JSON.parse(localStorage.Result).User_ID);
                }
                else if (columnName[0].toUpperCase() == "CREATEDATETIME") {
                    Values[columnName[0]] = window.btoa(GetNow());
                }
                else if (columnName[0].toUpperCase() == "STATUS")
                    Values[columnName[0]] = window.btoa(true);
                else {
                    if ($('[name="Columns"]')[i].type == "checkbox")
                        Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].checked);
                    else
                        Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].value);
                }
            }
            var values = JSON.stringify(Values);
            $.ajax({
                type: "POST",
                url: GlobalURL + '/api/CrudService/createTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML,
                data: JSON.stringify(values),
                contentType: 'application/json; charset=utf-8',
                success: function (val) {
                    if (val.createTableRowResult == "true") {
                        swal("", "Record Added Successfully", "success")
                    }
                    $("#addModal .close").click();
                    gridLoad();
                },
                error: function (e) {
                }
        });
        
    });
    $("#UpdateForm").on("submit", function (e) {
        e.preventDefault();
            var IDName = $('[name="EditColumns"]')[0].id.split("|");
            var ID = {};
            ID[IDName[0]] = $('[name="EditColumns"]')[0].value;
            columns = $('[name="EditColumns"]');
            var Values = {
                "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
                //  "UpdateDatetime": window.btoa(GetNow())
            }
            for (var i = 1; i < columns.length; i++) {
                var z = $('[name="EditColumns"]')[i].id.split("|");
                if ($('[name="EditColumns"]')[i].type == "checkbox") {
                    Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].checked);
                }
                else {

                    Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].value);
                }
            }
            var values = JSON.stringify(Values);
            var Id = JSON.stringify(ID);
            $.ajax({
                type: "POST",
                url: GlobalURL + '/api/CrudService/UpdateTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML + '&ID=' + Id,
                data: JSON.stringify(values),
                contentType: 'application/json; charset=utf-8',
                success: function (val) {
                    $("#editModal .close").click();
                    gridLoad();
                    if (val.updateTableRowResult == "true") {
                        swal("", "Updated Successfully", "success")
                    }
                },
                error: function (e) {

                }
            });
    });
});