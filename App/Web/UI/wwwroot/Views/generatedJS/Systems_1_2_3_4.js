var DropDownDefaultValues = {};
var DropDownTableValues = {};
DropDownDefaultValues["selectID"] = ""
DropDownDefaultValues["selectName"] = ""
DropDownDefaultValues["selectIP_Address"] = ""
DropDownDefaultValues["selectMAC_Address"] = ""
DropDownDefaultValues["selectBinding_Time"] = ""
DropDownDefaultValues["selectUsername"] = ""
DropDownDefaultValues["selectPassword"] = ""
DropDownDefaultValues["selectSystem_Status"] = ""
DropDownDefaultValues["selectCreateBy"] = ""
DropDownDefaultValues["selectCreateDatetime"] = ""
DropDownDefaultValues["selectUpdateBy"] = ""
DropDownDefaultValues["selectUpdateDatetime"] = ""
DropDownDefaultValues["selectStatus"] = ""
DropDownDefaultValues["selectsystemtype"] = ""

function displayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            var select = $('[name="Columns"]')[i];
            $('[name="Columns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            console.log(key);
            select = $('[name="Columns"]')[i];
            var opt1 = '<option selected disabled hidden style=\"display: none\" value=\"1\"></option>';
            $('[name="Columns"]')[i].append(opt1);
            var key1 = Object.keys(DropDownDefaultValues);
            for (var j = 0; j < dropdownlist.length; j++) {
                //  var key = Object.keys(DropDownTableValues[0]);
                var opt = document.createElement('option');
                if (key.length == 2) {
                    opt.text = dropdownlist[j][key[1]];
                    opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                else {
                    opt.text = opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                select.add(opt, j);
            }
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}

function editDisplayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        async: false,
        success: function (val) {
            var value = $('[name="EditColumns"]')[i].value;
            var select = $('[name="EditColumns"]')[i];
            $('[name="EditColumns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            console.log(key);
            select = $('[name="EditColumns"]')[i];

            for (var j = 0; j < dropdownlist.length; j++) {

                if (key.length == 2) {
                    if (dropdownlist[j][key[1]] != null && dropdownlist[j][key[0]] != null) {
                        var opt = document.createElement('option');
                        opt.text = dropdownlist[j][key[1]];
                        opt.id = elementId + "_" + dropdownlist[j][key[1]].replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                            var res = "ASCII" + ($0).charCodeAt();
                            return res
                        });
                        opt.value = dropdownlist[j][key[0]];
                        DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[1]];
                    }
                }
                else {
                    if (dropdownlist[j][key[0]] != null) {
                        var opt = document.createElement('option');
                        opt.text = opt.value = dropdownlist[j][key[0]];
                        opt.id = elementId + "_" + dropdownlist[j][key[0]].replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                            var res = "ASCII" + ($0).charCodeAt();
                            return res
                        });
                        DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[0]];
                    }
                }
                select.add(opt, j);
            }
            $('[name="EditColumns"]')[i].value = value;
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}
//function onSearch()
//{

//     var Filter = $("#Filter").val();

//    var grid = $("#grid").data("kendoGrid");
//    grid.dataSource.query({
//        page:1,
//        pageSize:10,
//        filter:{
//            logic:"or",
//            filters:[
//              { field: "ID", operator: "eq", value: parseInt(Filter) },
//              { field: "id1", operator: "contains", value: Filter },
//{field: "Name", operator:"contains",value:Filter} ,
//{field: "IP_Address", operator:"contains",value:Filter} ,
//{field: "MAC_Address", operator:"contains",value:Filter} ,
//{ field: "System_Status", operator: "eq", value: Filter },
//            ]
//        },
//        sort: { field: "ID", dir: "desc" },
//    });
//}
//function clearFilters() {
//    var grid = $("#grid").data("kendoGrid");
//    grid.dataSource.query({
//        page:1,
//        pageSize: 10,
//    });
//}

$(document).ready(function () {
    var x = localStorage.getItem("Result");
    var UserDetails = JSON.parse(x);
    $('#addModal').on('show.bs.modal', function () {

        columns = $('[name="Columns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                displayDropDownList(i, elementId)
            }
        }
    })
    $('#editModal').on('show.bs.modal', function () {

        columns = $('[name="EditColumns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                editDisplayDropDownList(i, elementId);
            }
        }
    });

    if ($(".btn-insight")[0].attributes.insights.nodeValue == "NKPI") {
        $("#loader").show();
        loadNKPI();
    }
    else if ($(".btn-insight")[0].attributes.insights.nodeValue == "KPI") {
        $("#loader").show();
        loadCharts();
    }

    //$("#btnFilter").kendoButton({
    //    click: onSearch
    //})
    //$("#btnClear").kendoButton ({
    //    click: clearFilters
    //})
});

gridLoad();
function gridLoad() {
    $("#loader").show();
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableData?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML,
        async:false,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            var dataSource = $.parseJSON(val);
            var columns = $('[name="EditColumns"]');
            for (i = 0; i < columns.length; i++) {
                if (columns[i].type == "select-one") {
                    //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                    //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                    var ID = columns[i].id;
                    var ElementDetails = ID.split("|");
                    var elementId = ElementDetails[1];
                    editDisplayDropDownList(i, elementId);
                    for (j = 0; j < dataSource.length; j++) {
                        dataSource[j][ElementDetails[0]] = DropDownTableValues[dataSource[j][ElementDetails[0]]];
                    }
                }
            }
            var t = $('#grid').DataTable({
                destroy: true,
                "scrollY": true,
                "scrollX": true,
                "Info": false,
                "pagingType": "simple_numbers",
                "LengthChange": true,
                "aLengthMenu": [[20, 40, 60, -1], [20, 40, 60, "All"]],
                "iDisplayLength": 20,
                "order": [[0, 'desc']],
                "data": dataSource,

                "columns": [
                    {
                        "data": "ID",
                        "title": "Id"
                    },
                    {
                        "data": "Name",
                        "title": "Name"
                    },
                    {
                        "data": "IP_Address",
                        "title": "IP Address"
                    },
                    {
                        "data": "MAC_Address",
                        "title": "MAC Address"
                    },
                    {
                        "data": "System_Status",
                        "title": "System Status"
                    },
                    //{
                    //    "title": "Actions",
                    //    "defaultContent":"<button class='btn btn-warning btn-flat' title='Edit' onclick='editFunction(this)'><i class='fa fa-edit' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Edit</span></button>&nbsp;<button class='deleteBtnClass btn btn-flat' title='Delete' onclick='deleteFunction(this)'><i class='fa fa-trash' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Delete</span></button>"
                    //},

                ], "oLanguage": { "sSearch": "" },
                "columnDefs": [{
                    "targets": '_all',
                    "defaultContent": "",
                },
                {
                    "aTargets": [0],
                    "sClass": "hidden"
                },
                {
                    "targets": [0],
                    "orderable": true
                },],

            });
            t.page('first').draw('page');
            $("#grid_wrapper .dataTables_length label").css("display", "block");
            $(".dataTables_filter").addClass("pull-left");
            $(".dataTables_length").addClass("pull-right");
            $($('#grid_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');

            if (filterCharts != "") {
                //var filter = $($("#grid_filter").find('input')[0]).val(filterCharts);
                $("div.dataTables_filter input").val(filterCharts);
                t.search(filterCharts).draw();
                filterCharts = "";
            }
            $("#loader").hide();
        },
        //        for (i = 0; i < dataSource.length; i++) {

        //if (dataSource[i].ID != null) {

        //dataSource[i].ID = dataSource[i].ID.toString()
        //dataSource[i].id1 = dataSource[i].ID.toString()
        //}

        //if(dataSource[i].Name!=null){

        //dataSource[i].Name= dataSource[i].Name.toString()

        //}

        //if(dataSource[i].IP_Address!=null){

        //dataSource[i].IP_Address= dataSource[i].IP_Address.toString()

        //}

        //if(dataSource[i].MAC_Address!=null){

        //dataSource[i].MAC_Address= dataSource[i].MAC_Address.toString()

        //}
        //if (dataSource[i].System_Status != null) {

        //    dataSource[i].System_Status = dataSource[i].System_Status.toString()

        //}

        //        }

        //        $("#grid").kendoGrid({
        //            dataSource: {
        //                data: dataSource,
        //                pageSize: 10,
        //                schema: {
        //                    model: {
        //                        id:"ID",

        //fields:{

        //Name:{type: "text"},
        //id1: { type: "text" },
        //IP_Address:{type: "text"},

        //MAC_Address:{type: "text"},

        //System_Status:{type: "text"},

        //}
        //                        }
        //                },
        //                sort: { field: "ID", dir: "desc" },
        //            },
        //            sortable: true,

        //            //pageable: true,
        //            pageable: {
        //                messages: {
        //                    display: "{0} - {1} of {2} Items"
        //                }
        //            },

        //            columns: [{ field: "ID", title: " Id", width: "120px" },

        //{ field: "Name", title: " Name", width: "120px" },

        //{field:"IP_Address", title:" IP Address",width:"120px"},

        //{field:"MAC_Address", title:" MAC Address",width:"120px"},

        //{field:"System_Status", title:" System Status",width:"120px"},

        //      { field: "id1", title: " id1", hidden: true, width: "120px" }]

        ////edit: function (e) {
        ////    var updateBtn = e.container.find(".k-grid-update");
        ////    updateBtn.removeClass("k-grid-update");

        ////    //removing this class will prevent the grid from saving on click
        ////    updateBtn.click(function () {

        ////        updateFunction(); //call the function you wish
        ////        updateBtn.addClass("k-grid-update");
        ////    })
        //});
        //        if (filterCharts != "") {
        //            $("#Filter").val(filterCharts);
        //            $("#btnFilter").click();
        //            filterCharts = "";
        //        }
        //},
        error: function (e) {
            //alert("Something Wrong!!");
            gridLoad();
        }
    });
}

function editFunction(e) {
    $('#editModal').modal('show');

    var tr = $(e)[0].closest("tr");
    var td = tr.children;
    var columns = $('[name="EditColumns"]');
    //console.log(td);
    for (var i = 0; i < columns.length; i++) {
        if (i == 0) {
            $('[name="EditColumns"]')[i].innerHTML = $('[name="EditColumns"]')[i].value = td[i].innerHTML;
        }
        else if ($('[name="EditColumns"]')[i].localName == "select") {
            //$('[name="EditColumns"]')[i].value = $("#" + td[i].innerHTML.replace(" ","")).val();
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
                $('[name="EditColumns"]')[i].value = $("#" + $('[name="EditColumns"]')[i].id.split("|")[1] + "_" + td[i].innerHTML.replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                    var res = "ASCII" + ($0).charCodeAt();
                    return res
                })).val().replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        else {
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
                $('[name="EditColumns"]')[i].value = td[i].innerHTML.replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        //console.log($('[name="EditColumns"]')[i].value);
    }

}
function deleteFunction(e) {
    $('#deleteModal').modal('show');
    $("#deleteId")[0].innerHTML = $($(e).closest("tr")[0]).children('td:first-child')[0].innerHTML
    $("#deleteName")[0].innerHTML = $($($("#grid").find('thead')[0]).find('tr')[0]).find('th')[0].innerText.trim().toUpperCase()

}

function delconfirmFunction(e) {
    $('#deleteModal').modal('hide');

    var data = {

    }
    data[$("#deleteName")[0].innerHTML] = $("#deleteId")[0].innerHTML
    var IdData = JSON.stringify(data);
    //var IdJSON = JSON.stringify(Id);

    $.ajax({
        type: "POST",
        url: GlobalURL + '/api/CrudService/deleteTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML + '&ID=' + IdData,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            //alert(val);

            gridLoad();
            if (val == "true") {

            }
            // $("#feedbackMsg").html("Success");

            else {
                $("#feedbackMsg").html("Sorry cannot perform such operation");
            }
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}

//function deleteFunction(e) {
//   // e.preventDefault();
//   // var dataItem = $(this).closest('tr').children('td:first').innerHTML;
//    var r=confirm("Are you sure you want to delete this record");
//    if(r){
//        var tr = $(e)[0].closest("tr");
//        var uid = $(tr).children('td:first-child')[0].innerHTML;

//        var x = $('th')[0];
//        var columnName=$(x).attr('data-field');
//        var data={    
//        }
//        data[$(x).attr('data-field')]=uid;

//        console.log(data);
//        var IdData = JSON.stringify(data);
//        $.ajax({
//            type: "POST",
//            url: '/services/CrudService.svc/DeleteTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML+'&ID=' + IdData,
//            //data: Htmlname ,
//            contentType: 'application/json; charset=utf-8',
//            //dataType: 'json',

//            success: function (val) {
//                //alert(val);


//                gridLoad();
//                if(val == "true"){

//                }
//                    // $("#feedbackMsg").html("Success");

//                else {
//                    $("#feedbackMsg").html("Sorry cannot perform such operation");
//                }
//            },
//            error: function (e) {
//                $("#feedbackMsg").html("Something Wrong.");
//            }
//        });
//    }
//}

$(document).ready(function () {
    $("#SubmitForm").on("submit", function (e) {
        e.preventDefault();
        if (validator.validate()) {
            var Values = {
            }
            var Columns = $('[name="Columns"]');
            var len = $('[name="Columns"]').length;
            for (var i = 1; i < len; i++) {
                var ID = $('[name="Columns"]')[i].id;
                var columnName = ID.split("|");
                if (columnName[0].toUpperCase == "CREATEBY") {
                    Values[columnName[0]] = window.btoa(JSON.parse(localStorage.Result).User_ID);
                }
                else if (columnName[0].toUpperCase() == "CREATEDATETIME") {
                    Values[columnName[0]] = window.btoa(GetNow());
                }
                else if (columnName[0].toUpperCase() == "STATUS")
                    Values[columnName[0]] = window.btoa(true);
                else {
                    if ($('[name="Columns"]')[i].type == "checkbox")
                        Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].checked);
                    else
                        Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].value);
                }
            }

            var values = JSON.stringify(Values);
            $.ajax({
                type: "POST",
                url: GlobalURL + '/api/CrudService/createTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML,
                data: JSON.stringify(values),

                //data: Htmlname ,
                contentType: 'application/json; charset=utf-8',
                //dataType: 'json', 
                success: function (val) {
                    if (val == "true") {

                    }
                    //location.reload();
                    $("#addModal .close").click();
                    gridLoad();
                },
                error: function (e) {

                }
            });
        }
    });

    $("#UpdateForm").on("submit", function (e) {
        e.preventDefault();
        if (validator1.validate()) {
            var IDName = $('[name="EditColumns"]')[0].id.split("|");
            var ID = {};
            ID[IDName[0]] = $('[name="EditColumns"]')[0].value;
            columns = $('[name="EditColumns"]');
            var Values = {
                "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
                //  "UpdateDatetime": window.btoa(GetNow())
            }
            for (var i = 1; i < columns.length; i++) {
                var z = $('[name="EditColumns"]')[i].id.split("|");
                if ($('[name="EditColumns"]')[i].type == "checkbox") {
                    Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].checked);
                }
                else {

                    Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].value);
                }
            }
            var values = JSON.stringify(Values);
            var Id = JSON.stringify(ID);
            $.ajax({
                type: "POST",
                url: GlobalURL + '/api/CrudService/UpdateTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML + '&ID=' + Id,
                data: JSON.stringify(values),

                //data: Htmlname ,
                contentType: 'application/json; charset=utf-8',
                //dataType: 'json', 
                success: function (val) {

                    //location.reload();
                    $("#editModal .close").click();
                    gridLoad();
                    if (val == "true") {

                    }
                },
                error: function (e) {

                }
            });

        }
    });
});

function cChart(dataSource) {
    var label1 = "";
    if (dataSource["Chart_Type"].toLowerCase() == "pie" || dataSource["Chart_Type"].toLowerCase() == "donut") {
        label1 = "#= category # - #= value#";
    }
    var width1, height1;
    var windowwidth = $(window).width();
    var windowheight = $(window).height();
    if (windowwidth > 1500 && windowheight > 500) {
        width1 = (dataSource["Width"] * 15).toString();
        height1 = dataSource["Height"];
    }
    else {
        width1 = (dataSource["Width"] * 8).toString();
        height1 = dataSource["Height"];
    }
    $('#' + dataSource["ID"]).kendoChart({
        dataSource: {
            data: JSON.parse(dataSource["data"]),
        },
        seriesColors: ['#3398db ', '#19bc9c ', '#fad231 ', '#9b59b6 ', '#e74c3c '],
        chartArea:
        {
            width: width1,
            height: height1
        },
        legend: {
            position: "top"
        },
        seriesDefaults: {
            type: dataSource["Chart_Type"].toLowerCase(),
            labels: {
                template: label1,
                position: "outsideEnd",
                visible: true,
                background: "transparent"
            }

        },

        series: [{
            categoryField: dataSource["Column_Number"],
            field: dataSource["Row_Number"],
            gap: 8,
            overlay: {
                gradient: "none"
            },
            border: {
                width: 0
            },

        }],
        valueAxis: {
            field: dataSource["Row_Number"],
            labels: {
                format: "{0}"
            },
            line: {
                visible: false
            },
            axisCrossingValue: 0
        },
        categoryAxis: {

            field: dataSource["Column_Number"],
            labels: {
                rotation: -30,
            },
            line: {
                visible: true
            },
            majorGridLines: {
                visible: false
            }

        },
        tooltip: {
            visible: true,

            template: "${category} - ${value}"
        }

    })
}

function loadCharts() {
    var x = localStorage.getItem("Result");
    var role = JSON.parse(x);
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/Dashboard/getCharts?roleid=' + role.Role_ID + '&monitor=' + $("#PageTitle")[0].innerHTML.trim(),
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            console.log(result);

            var dataSource = $.parseJSON(result);

            //var y = dataSource[0].PortalName;
            // console.log(y);
            //$('.chart').innerHTML = dataSource[0].PortalName;

            console.log(dataSource);

            var temp = [];
            console.log(x);
            var j;
            $("#chart_view").html("");
            $("#loader").hide();
            for (j = 0; j < dataSource.length; j++) {
                var templateContent = $("#chartTemplate").html();
                var template = kendo.template(templateContent);

                //Create some dummy data

                if (j % 2 == 0 && j != 0) dataSource[j].pr = 12;
                else dataSource[j].pr = 6;
                //render the template
                var chartTemplate = template(dataSource[j]).replace("Chart_ID", dataSource[j].ID.split("#")[1]);
                // chartTemplate.replace("Chart_ID", dataSource[j].ID);

                $("#chart_view").append(chartTemplate);
                if (dataSource[j]["data"] != "false" && dataSource[j]["data"] != "Connection Failure") {
                    cChart(dataSource[j]);
                    //append the result to the page
                }
                else {
                    $("#WrongMsg" + dataSource[j]["ID"])[0].innerHTML = "Sorry! Something went wrong. Please check the KPI details and try again";
                    $("#WrongMsg" + dataSource[j]["ID"]).css("height", "300px");
                }
                //append the result to the page
            }
        },

        error: function (e) {
            //  alert("Something Wrong!!");
        }
    });
}

var row = 0;
var h = 0;
function loadNKPI() {
    var x = localStorage.getItem("Result");
    var role = JSON.parse(x);

    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/Dashboard/getKPI?roleid=' + role.Role_ID + '&monitor=' + $("#PageTitle")[0].innerHTML.trim(),
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            console.log("KPI-----" + result);
            var colorCSS = ["amber_grad", "red_grad", "green_grad", "cyan_grad"];
            var dataSource = $.parseJSON(result);

            //var y = dataSource[0].PortalName;
            // console.log(y);
            //$('.chart').innerHTML = dataSource[0].PortalName;

            console.log(dataSource);

            var temp = [];
            console.log(x);
            var j;
            $("#chart_view").html("");
            $("#loader").hide();

            for (j = 0; j < dataSource.length; j++) {
                row++;
                var templateContent = $("#KPIRowTemplate").html();
                var template = kendo.template(templateContent);
                var chartTemplate = template(dataSource[j])
                var data = JSON.parse(dataSource[j]["data"]);

                for (k = 0; k < data.length; k++) {
                    // var randomColor = '#' + ('000000' + Math.floor(Math.random() * 16777215).toString(16)).slice(-6);
                    data[k]["colorClass"] = colorCSS[h];
                    if (h < colorCSS.length - 1) {
                        h++;
                    }
                    else {
                        h = 0;
                    }
                    var kpiDiv = $("#KPITemplate").html();
                    var kpitemplate = kendo.template(kpiDiv);
                    //  data[k]["randomColor"] = randomColor;
                    //data[k]["colorId"] = "KPI_" + row + "-" + k + " KPIClass";
                    chartTemplate += kpitemplate(data[k]);
                }

                $("#KPI").append(chartTemplate + "</div>");
                // cChart(dataSource[j]);
                //append the result to the page
            }
        },

        error: function (e) {
            // alert("Something Wrong!!");
        }
    });
}

function Insightclick() {
    if ($(".btn-insight")[0].attributes.insights.nodeValue == "NKPI") {
        $("#KPIRow").toggle(1500);
    }
    else if ($(".btn-insight")[0].attributes.insights.nodeValue == "KPI") {

        $("#chart_view").toggle(1500);
    }
}

//$.ajax({
//    type: "GET",
//    url: '/services/CrudService.svc/getBreadcrumb?Screenid=111',
//    async: true,
//    contentType: 'application/json; charset=utf-8',
//    success: function (result) {
//        //for(var i=0;i<=length;i--)
//        //{
//        //    sb.Replace("{BreadCrumb}","<li><a href = \'javascript:;\' onclick = \'layoutRefresh(window.location.origin + \"/Views/Layout.html?/generatedHTML/KPI_1\", 0)\'><i class=\"fa fa-user\"></i> <b>Administrator</b></a></li>66Bot Monitor2/Views/Layout.html?Monitor2Monitor0#");
//        //}
//    },
//    error: function (e) {
//        // alert("Something Wrong!!");
//    }
//});