

var DropDownDefaultValues = {};
var DropDownTableValues = {};
DropDownDefaultValues["selectID"] = ""
DropDownDefaultValues["selectRobotId"] = "179"
DropDownDefaultValues["selectDescription"] = ""
DropDownDefaultValues["selectStepId"] = "19"
DropDownDefaultValues["selectMsgStatus"] = ""
DropDownDefaultValues["selectAction"] = ""
DropDownDefaultValues["selectCreateBy"] = ""
DropDownDefaultValues["selectCreateDatetime"] = ""
DropDownDefaultValues["selectUpdateBy"] = ""
DropDownDefaultValues["selectUpdateDatetime"] = ""
DropDownDefaultValues["selectStatus"] = ""

function displayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            

            var select = $('[name="Columns"]')[i];
            $('[name="Columns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            console.log(key);
            select = $('[name="Columns"]')[i];
            var opt1 = '<option selected disabled hidden style=\"display: none\" value=\"1\"></option>';
            $('[name="Columns"]')[i].append(opt1);
            var key1 = Object.keys(DropDownDefaultValues);
            for (var j = 0; j < dropdownlist.length; j++) {
                
                //  var key = Object.keys(DropDownTableValues[0]);
                var opt = document.createElement('option');
                if (key.length == 2) {
                    opt.text = dropdownlist[j][key[1]];
                    opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                else {
                    opt.text = opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                select.add(opt, j);
            }
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}
var BotId = {};
var CurrentOutput = "";
var ActionType = "";
function InboxAcceptClick(e) {
    $("#AcceptSubmitForm").removeClass("col-sm-6");
    $("#AcceptSubmitForm").addClass("col-sm-12");
    $("embed").remove();
    $("#AcceptModal .modal-dialog").width("600px")
    $("#AcceptModal").modal('show');
    //$("#TitleLabel")[0].innerHTML = "";
    $("#DisplayNameLabel")[0].innerHTML = "";
    $("#RobotLabel")[0].innerHTML = $(e).parent().closest('tr').find('td')[1].innerHTML;
    $("#IDLabel")[0].innerHTML = $(e).parent().closest('tr').find('td')[0].innerHTML;
    $("#StepLabel")[0].innerHTML = $(e).parent().closest('tr').find('td')[2].innerHTML;
    stepID = $(e).parent().closest('tr').find('td')[3].innerHTML;
    BotId["ID"] = $(e).parent().closest('tr').find('td')[0].innerHTML;
    CurrentOutput = $(e).parent().closest('tr').find('td')[6].innerHTML;
    ActionType = $(e).parent().closest('tr').find('td')[7].innerHTML;
    InboxResult = $.parseJSON(CurrentOutput);

    StepsdataSource = InboxResult["runtimerequest"]["step"]["StepProperties"];
    for (var i in StepsdataSource) {
       
        if (StepsdataSource[i]["StepProperty"] == "Title") {
            $(".modal-title")[0].innerHTML = StepsdataSource[i]["StepPropertyValue"];
        }
        else if (StepsdataSource[i]["StepProperty"] == "DisplayMessage") {
            $("#DisplayNameLabel")[0].innerHTML = StepsdataSource[i]["StepPropertyValue"];
        }
        if (StepsdataSource[i]["StepProperty"] == "WidgetType") {
            if (InboxResult["runtimerequest"]["step"]["Name"] == "Input") {
                $("#io_Action").removeClass('display-none');
                $('#o_Action').addClass('display-none');
                if (StepsdataSource[i]["StepPropertyValue"].trim().toLowerCase() == "textbox")
                    $("#io_Action").html("<div class='col-sm-6'><input type=text id=InputVariableValue class=form-control /></div>");
                else if ((StepsdataSource[i]["StepPropertyValue"].trim().toLowerCase() == "textarea"))
                    $("#io_Action").html("<div class='col-sm-6'><textarea id=InputVariableValue class=form-control ></textarea></div>");
                else if ((StepsdataSource[i]["StepPropertyValue"].trim().toLowerCase() == "checkbox"))
                    $("#io_Action").html("<div class='col-sm-6'><input  type=checkbox id=InputVariableValue /></div>");
                else if ((StepsdataSource[i]["StepPropertyValue"].trim().toLowerCase() == "image + textbox")) {
                    var DVariables;
                    var sourcepathImage;
                    var imageSource;
                    var img_extension;
                    try {
                        DVariables = InboxResult["runtimerequest"]["dvariables"];
                        ImageV = StepsdataSource.find(s => s.StepProperty == "ImageVariable");
                        sourcepathImage = DVariables.find(d => d.vlname == ImageV.StepPropertyValue);
                        if (typeof (sourcepathImage.vlvalue[0]["val"]) != typeof (undefined)) {

                            imageSource = JSON.parse(sourcepathImage.vlvalue[0]["val"])[0]["Files"].split("Uploads\\")[1];
                        }
                        else {
                            if (typeof (JSON.parse(sourcepathImage.vlvalue)[0]["Files"]) != typeof (undefined)) {
                                imageSource = JSON.parse(sourcepathImage.vlvalue)[0]["Files"].split("Uploads\\")[1];
                            }
                            else {
                                imageSource = JSON.parse(sourcepathImage.vlvalue)[0][ImageV.StepPropertyValue].split("Uploads\\")[1];
                            }
                        }

                        img_extension = imageSource.split(".")[1];
                        if (img_extension.toLowerCase() == "png" || img_extension.toLowerCase() == "jpg" || img_extension.toLowerCase() == "jpeg") {
                            $("#TotalAcceptModal").prepend("<img class='col-sm-8' src='/Uploads/" + imageSource + "'/>");
                            $("#io_Action").html("<div class='col-sm-12'><input class='col-sm-6 form-control' type=text id=InputVariableValue /></div>");
                            $("#AcceptSubmitForm").removeClass("col-sm-12");
                            $("#AcceptSubmitForm").addClass("col-sm-4");
                            $("#AcceptModal .modal-dialog").width("1200px");

                        }
                        else if (img_extension.toLowerCase() == "pdf") {
                            $("#TotalAcceptModal").prepend("<embed class='col-sm-6' height=\"500\" src='/Uploads/" + imageSource + "'/>")
                            $("#io_Action").html("<div class='col-sm-12'><input class='col-sm-6 form-control' type=text id=InputVariableValue /></div>");
                            $("#AcceptSubmitForm").removeClass("col-sm-12");
                            $("#AcceptSubmitForm").addClass("col-sm-6");
                            $("#AcceptModal .modal-dialog").width("1000px");
                        }
                        else
                            $("#io_Action").html("<div class='col-sm-6'><a target='_blank' href='/Uploads/" + imageSource + "'><i class='fa fa-download'></i></a><input type=text id=InputVariableValue class=form-control /></div>");
                    }
                    catch (err) {
                        $("#io_Action").html("<div class='col-sm-12'><img class='col-sm-6' alt='No Image Available' src='/Uploads/" + imageSource + "'/><input class='col-sm-6 form-control' type=text id=InputVariableValue /></div>");
                    }
                }
                else if ((StepsdataSource[i]["StepPropertyValue"].trim().toLowerCase() == "image + textbox + checkbox")) {
                    var DVariables;
                    var sourcepathImage;
                    var imageSource;
                    var img_extension;
                    try {
                        DVariables = InboxResult["runtimerequest"]["dvariables"];
                        ImageV = StepsdataSource.find(s => s.StepProperty == "ImageVariable");
                        sourcepathImage = DVariables.find(d => d.vlname == ImageV.StepPropertyValue);
                        if (typeof (sourcepathImage.vlvalue[0]["val"]) != typeof (undefined)) {

                            imageSource = JSON.parse(sourcepathImage.vlvalue[0]["val"])[0]["Files"].split("Uploads\\")[1];
                        }
                        else {
                            if (typeof (JSON.parse(sourcepathImage.vlvalue)[0]["Files"]) != typeof (undefined)) {
                                imageSource = JSON.parse(sourcepathImage.vlvalue)[0]["Files"].split("Uploads\\")[1];
                            }
                            else {
                                imageSource = JSON.parse(sourcepathImage.vlvalue)[0][ImageV.StepPropertyValue].split("Uploads\\")[1];
                            }
                        }

                        img_extension = imageSource.split(".")[1];
                        if (img_extension.toLowerCase() == "png" || img_extension.toLowerCase() == "jpg" || img_extension.toLowerCase() == "jpeg") {
                            $("#TotalAcceptModal").prepend("<img class='col-sm-8' src='/Uploads/" + imageSource + "'/>");
                            $("#io_Action").html("<div class='col-sm-12'><input class='col-sm-6 form-control' type=text id=InputVariableValue /><input type=checkbox id='InputVariableCheckbox'/></div>");
                            $("#AcceptSubmitForm").removeClass("col-sm-12");
                            $("#AcceptSubmitForm").addClass("col-sm-4");
                            $("#AcceptModal .modal-dialog").width("1200px");

                        }
                        else if (img_extension.toLowerCase() == "pdf") {
                            $("#TotalAcceptModal").prepend("<embed class='col-sm-6' height=\"500\" src='/Uploads/" + imageSource + "'/>")
                            $("#io_Action").html("<div class='col-sm-12'><input class='col-sm-6 form-control' type=text id=InputVariableValue /><input type=checkbox id='InputVariableCheckbox'/></div>");
                            $("#AcceptSubmitForm").removeClass("col-sm-12");
                            $("#AcceptSubmitForm").addClass("col-sm-6");
                            $("#AcceptModal .modal-dialog").width("1000px");
                        }
                        else
                            $("#io_Action").html("<div class='col-sm-6'><a target='_blank' href='/Uploads/" + imageSource + "'><i class='fa fa-download'></i></a><input type=text id=InputVariableValue class=form-control /></div>");
                    }
                    catch (err) {
                        $("#io_Action").html("<div class='col-sm-12'><img class='col-sm-6' alt='No Image Available' src='/Uploads/" + imageSource + "'/><input class='col-sm-6 form-control' type=text id=InputVariableValue /></div>");
                    }
                }
                else if ((StepsdataSource[i]["StepPropertyValue"].trim().toLowerCase() == "image + textarea")) {
                    var DVariables;
                    var sourcepathImage;
                    var imageSource;
                    var img_extension;
                    try {
                        DVariables = InboxResult["runtimerequest"]["dvariables"];
                        ImageV = StepsdataSource.find(s => s.StepProperty == "ImageVariable");
                        sourcepathImage = DVariables.find(d => d.vlname == ImageV.StepPropertyValue);
                        if (typeof (sourcepathImage.vlvalue[0]["val"]) != typeof (undefined)) {

                            imageSource = JSON.parse(sourcepathImage.vlvalue[0]["val"])[0]["Files"].split("Uploads\\")[1];
                        }
                        else {
                            if (typeof (JSON.parse(sourcepathImage.vlvalue)[0]["Files"]) != typeof (undefined)) {
                                imageSource = JSON.parse(sourcepathImage.vlvalue)[0]["Files"].split("Uploads\\")[1];
                            }
                            else {
                                imageSource = JSON.parse(sourcepathImage.vlvalue)[0][ImageV.StepPropertyValue].split("Uploads\\")[1];
                            }
                        }
                        img_extension = imageSource.split(".")[1];
                        if (img_extension.toLowerCase() == "png" || img_extension.toLowerCase() == "jpg" || img_extension.toLowerCase() == "jpeg") {

                            $("#io_Action").html("<div class='col-sm-12'><img class='col-sm-12' src='/Uploads/" + imageSource + "'/><textarea class='col-sm-6 form-control' id=InputVariableValue ></textarea></div>");
                            $("#AcceptModal .modal-dialog").width("800px");
                        }
                        else if (img_extension.toLowerCase() == "pdf") {
                            $("#TotalAcceptModal").prepend("<embed class='col-sm-6' height=\"500\" src='/Uploads/" + imageSource + "'/>")
                            $("#io_Action").html("<div class='col-sm-12'><textarea class='col-sm-6 form-control' id=InputVariableValue ></textarea></div>");
                            $("#AcceptSubmitForm").removeClass("col-sm-12");
                            $("#AcceptSubmitForm").addClass("col-sm-6");
                            $("#AcceptModal .modal-dialog").width("1000px");
                        }
                        else
                            $("#io_Action").html("<div class='col-sm-6'><a target='_blank' href='/Uploads/" + imageSource + "'><i class='fa fa-download'></i></a><textarea class='col-sm-6 form-control' id=InputVariableValue ></textarea></div>");
                    }
                    catch (err) {
                        $("#io_Action").html("<div class='col-sm-12'><img class='col-sm-6' alt='No Image Available' src='/Uploads/" + imageSource + "'/><textarea class='col-sm-6 form-control' id=InputVariableValue ></textarea></div>");
                    }
                }
            }
        }
        else if (StepsdataSource[i]["StepProperty"] == "Value") {
            if (InboxResult["runtimerequest"]["step"]["Name"] == "Output") {
                $("#io_Action").addClass('display-none');
                $('#o_Action').removeClass('display-none');
                var DynamicV = InboxResult["runtimerequest"]["dvariables"];
                var outputVariableIndex = DynamicV.find(V => V.vlname == StepsdataSource[i]["StepPropertyValue"]);
                OutputResult = JSON.parse(outputVariableIndex["vlvalue"]);
                if (OutputResult != "") {
                    var columns = [];
                    for (var key in Object.keys(OutputResult[0])) {
                        var key_column = {};
                        key_column["title"] = Object.keys(OutputResult[0])[key];
                        key_column["data"] = Object.keys(OutputResult[0])[key];
                        columns.push(key_column);
                    }
                    $("#OutputActionResult").DataTable({
                        destroy: true,
                        "scrollY": true,
                        "scrollX": true,
                        "data": OutputResult,
                        "iDisplayLength": 10,
                        "order": [[0, 'desc']],
                        "columns": columns,
                        "columnDefs": [{
                            "targets": '_all',
                            "defaultContent": "",
                        }],
                    });
                }
                else {
                    $("#OutputActionResult").innerHTML = "No Output";
                }
            }
        }
    }

}


function editDisplayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        async: false,
        success: function (val) {
            
            var value = $('[name="EditColumns"]')[i].value;
            var select = $('[name="EditColumns"]')[i];
            $('[name="EditColumns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            console.log(key);
            select = $('[name="EditColumns"]')[i];


            for (var j = 0; j < dropdownlist.length; j++) {


                if (key.length == 2) {
                    if (dropdownlist[j][key[1]] != null && dropdownlist[j][key[0]] != null) {
                        var opt = document.createElement('option');
                        opt.text = dropdownlist[j][key[1]];
                        opt.id = elementId + "_" + dropdownlist[j][key[1]].replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                            
                            var res = "ASCII" + ($0).charCodeAt();
                            return res
                        });
                        opt.value = dropdownlist[j][key[0]];
                        DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[1]];
                    }
                }
                else {
                    if (dropdownlist[j][key[0]] != null) {
                        var opt = document.createElement('option');
                        opt.text = opt.value = dropdownlist[j][key[0]];
                        opt.id = elementId + "_" + dropdownlist[j][key[0]].replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                            
                            var res = "ASCII" + ($0).charCodeAt();
                            return res
                        });
                        DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[0]];
                    }
                }
                select.add(opt, j);
            }
            $('[name="EditColumns"]')[i].value = value;
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}
//function onSearch() {

//    var Filter = $("#Filter").val();

//    var grid = $("#grid").data("kendoGrid");
//    grid.dataSource.query({
//        page: 1,
//        pageSize: 10,
//        filter: {
//            logic: "or",
//            filters: [
//              { field: "ID", operator: "eq", value: parseInt(Filter) },
//              { field: "id1", operator: "contains", value: Filter },
//{ field: "RobotId", operator: "contains", value: Filter },
//{ field: "Description", operator: "contains", value: Filter },
//{ field: "StepId", operator: "contains", value: Filter },
//{ field: "MsgStatus", operator: "contains", value: Filter },
//{ field: "Action", operator: "contains", value: Filter },


//            ]

//        },
//        sort: { field: "ID", dir: "desc" },
//    });
//}
//document.addEventListener('keypress', function (e) {
//    
//    var key = e.which || e.keyCode;
//    if (key === 13) { // 13 is enter
//        $('#btnFilter').click();
//    }
//});
//function clearFilters() {
//    var grid = $("#grid").data("kendoGrid");
//    grid.dataSource.query({
//        page: 1,
//        pageSize: 10,
//    });
//}
$(document).ready(function () {
    
    var x = localStorage.getItem("Result");
    var UserDetails = JSON.parse(x);
    $('#addModal').on('show.bs.modal', function () {

        columns = $('[name="Columns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                displayDropDownList(i, elementId)
            }
        }
    })
    $('#editModal').on('show.bs.modal', function () {

        columns = $('[name="EditColumns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                editDisplayDropDownList(i, elementId);
            }
        }
    });


    if ($(".btn-insight")[0].attributes.insights.nodeValue == "NKPI") {
        $("#loader").show();
        loadNKPI();
    }
    else if ($(".btn-insight")[0].attributes.insights.nodeValue == "KPI") {
        $("#loader").show();
        loadCharts();
    }

    //$("#btnFilter").kendoButton({
    //    click: onSearch
    //})
    //$("#btnClear").kendoButton({
    //    click: clearFilters
    //})


});
filterCharts = "New";
LoadInbox();
var conditionCheck;
var dataSource;
function gridLoad() {
    $("#loader").show();
    var t = $('#grid').DataTable({
        "destroy": true,
        "scrollY": true,
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "paging": true,
        "pagingType": "input",
        "pageLength": 7,
        "word-break": "break-all",
        "lengthChange": false,
        "language": { search: '', searchPlaceholder: "Search..." },

        "ajax":
        {
            "url": GlobalURL + "/api/CrudService/getGridDataByUser?tablename=CommandCenter_Inbox&CreateBy=" + UserDetails.User_ID,
            "contentType": "application/json",
            "type": "POST",
            "dataType": "JSON",
            "async": false,
            "data": function (d) {

                return d;
            },
            "dataSrc": function (json) {


                json.draw = json.draw;
                json.recordsTotal = json.recordsTotal;
                json.recordsFiltered = json.recordsFiltered;
                json.data = json.data;

                var return_data = json;
                //return return_data.data;
                var dttt = $.parseJSON(return_data.data);
                return dttt;
            }
        },
        "columns": [
            {
                "data": "ID",
                "title": "Id"
            },
            {
                "data": "Name",
                "title": "Robot"
            },
            {
                "data": "StepName",
                "title": "Step"
            },
            {
                "data": "StepId",
                "title": "StepId"
            },

            {
                "data": "MsgStatus",
                "title": "Status"
            },

            {
                "data": "JobId",
                "title": "JobId"
            },
            {
                "data": "CurrentOutput",
                "title": "CurrentOutput"
            },
            {
                "data": "ActionType",
                "title": "ActionType"
            },
            {
                "data": "CreateDatetime",
                "title": "Create Datetime"
            },
            {
                "data": "Actions",
                "defaultContent": "<button class='bton success' title='Accept'><i class='fas fa-check' onclick='InboxAcceptClick(this)'></i></button><button class='bton danger' title='Reject' onclick='InboxRejectClick(this)'> <i class='fas fa-times'></i></button>"
            },


        ],
        "columnDefs": [{
            "targets": '_all',
            "defaultContent": "",
        },
        {
            "aTargets": [0, 3, 4, 5, 6, 7],
            "sClass": "hidden"
        },
        {
            "targets": [0],
            "orderable": true
        },],
        
    });
    $("#loader").hide();
    t.page('first').draw('page');
    $(".dataTables_filter").addClass("pull-left");
}
function gridLoad1() {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableDataByUserID?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML + '&CreateBy=' + JSON.parse(localStorage.Result).ID,

        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            
            dataSource = $.parseJSON(val);
            var columns = $('[name="EditColumns"]');
            for (var i in dataSource) {
                conditionCheck = dataSource[i].MsgStatus;
            }
            for (i = 0; i < columns.length; i++) {
                if (columns[i].type == "select-one") {
                    
                    //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                    //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                    var ID = columns[i].id;
                    var ElementDetails = ID.split("|");
                    var elementId = ElementDetails[1];
                    editDisplayDropDownList(i, elementId);
                    
                    for (j = 0; j < dataSource.length ; j++) {
                        dataSource[j][ElementDetails[0] + "_1"] = dataSource[j][ElementDetails[0]];
                        dataSource[j][ElementDetails[0]] = DropDownTableValues[dataSource[j][ElementDetails[0]]];
                    }
                }
            }
            var t = $('#grid').DataTable({
                "destroy": true,
                "scrollY": true,
                "scrollX": true,
                "data": dataSource,
                "Info": false,
                "pagingType": "simple_numbers",
                "LengthChange": true,
                "order": [[0, 'desc']],
                "columns": [
                    {
                        "data": "ID",
                        "title": "Id"
                    },
                    {
                        "data": "RobotId",
                        "title": "Robot"
                    },
                    {
                        "data": "Description",
                        "title": "Description"
                    },
                    {
                        "data": "StepId",
                        "title": "Step"
                    },
                    {
                        "data": "MsgStatus",
                        "title": "Status"
                    },
                    {
                        "title": "Actions",
                        "defaultContent": "<button title='Accept' id='#=MsgStatus#' class='btn btn-success btn-flat' onclick='AcceptClick(this)'><span class='fa fa-check '></span></button><button title='Reject' id='#=MsgStatus#' class='btn btn-danger btn-flat' onclick='RejectClick(this)'><span class='fas fa-times'></span></button>"
                    },

                ], "oLanguage": { "sSearch": "" },
                "columnDefs": [{
                    "targets": '_all',
                    "defaultContent": "",
                },
                 {
                     "aTargets": [0],
                     "sClass": "hidden"
                 },
                {
                    "targets": [0],
                    "orderable": true
                },
                ],

            });
            t.page('first').draw('page');
            $("#grid_wrapper .dataTables_length label").css("display", "block");
            $(".dataTables_filter").addClass("pull-left");
            $(".dataTables_length").addClass("pull-right");
            $($('#grid_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');

            if (filterCharts != "") {
                
                //var filter = $($("#grid_filter").find('input')[0]).val(filterCharts);
                //$("div.dataTables_filter input").val(filterCharts);
                //t.search(filterCharts).draw();
                filterCharts = "";
            }
        },

        error: function (e) {
            //alert("Something Wrong!!");
            gridLoad();
        }
    });
}

function editFunction(e) {

    
    $('#editModal').modal('show');

    var tr = $(e)[0].closest("tr");
    var td = tr.children;
    var columns = $('[name="EditColumns"]');
    //console.log(td);
    for (var i = 0; i < columns.length; i++) {
        if (i == 0) {
            $('[name="EditColumns"]')[i].innerHTML = $('[name="EditColumns"]')[i].value = td[i].innerHTML;
        }
        else if ($('[name="EditColumns"]')[i].localName == "select") {
            //$('[name="EditColumns"]')[i].value = $("#" + td[i].innerHTML.replace(" ","")).val();
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
                $('[name="EditColumns"]')[i].value = $("#" + $('[name="EditColumns"]')[i].id.split("|")[1] + "_" + td[i].innerHTML.replace(/[\!\#\@\$\%\^\*\(\)\_\ \-\+\=\[\]\{\}\:\;\"\'\|\<\>\,\.\?\/\`\~\\]/g, function ($0) {
                    
                    var res = "ASCII" + ($0).charCodeAt();
                    return res
                })).val().replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        else {
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
                $('[name="EditColumns"]')[i].value = td[i].innerHTML.replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        //console.log($('[name="EditColumns"]')[i].value);
    }

}
function deleteFunction(e) {
    //$('#deleteModal').modal('show');

    swal("Are you sure you want to delete this record?", {
        buttons: ["Cancel", "Ok"],
        className: "deleterecord",
        closeOnClickOutside: false,
        closeOnEsc: false,

    });

    //function delconfirmFunction(e) 
    $(".deleterecord .swal-footer .swal-button-container .swal-button--confirm").on("click", function () {
        
        $('#deleteModal').modal('hide');

        var data = {}

        data[$("#deleteName")[0].innerHTML] = $("#deleteId")[0].innerHTML
        var IdData = JSON.stringify(data);
        //var IdJSON = JSON.stringify(Id);

        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/DeleteTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML + '&ID=' + IdData,
            //data: Htmlname ,
            contentType: 'application/json; charset=utf-8',
            //dataType: 'json',

            success: function (val) {
                //alert(val);

                gridLoad();
                if (val == "Success") {

                    swal("", "Record deleted Successfully", "success", { closeOnClickOutside: false });   
                }
                    // $("#feedbackMsg").html("Success");

                else {
                    $("#feedbackMsg").html("Sorry cannot perform such operation");
                }
            },
            error: function (e) {
                $("#feedbackMsg").html("Something Wrong.");
            }
        });
    });
    $("#deleteId")[0].innerHTML = $($(e).closest("tr")[0]).children('td:first-child')[0].innerHTML
    $("#deleteName")[0].innerHTML = $($($("#grid").find('thead')[0]).find('tr')[0]).find('th')[0].innerText.trim().toUpperCase()

}


$('[data-toggle="toggleButton"]').bootstrapToggle({

    on: "New",
    onstyle: "success",
    offstyle: "success",
    off: "All",
    style: "slow",
    width: "100"
    // additional settings if necessary
});
document.addEventListener("click", function (e) {
    
    if ($(e.srcElement).hasClass("toggle-off") || $(e.srcElement).hasClass("toggle-on")) {
        if ($(e.srcElement).hasClass("toggle-on")) {
            clearFilters();
        }
        else if ($(e.srcElement).hasClass("toggle-off")) {
            filterCharts = "New";

            $("#Filter").val(filterCharts);
            $("#btnFilter").click();
            //filterCharts = "";

        }
    }

});



$(document).ready(function () {
    $("#SubmitForm").on("submit", function (e) {
        e.preventDefault();
        if (validator.validate()) {
            var Values = {
            }
            var Columns = $('[name="Columns"]');
            var len = $('[name="Columns"]').length;
            for (var i = 1; i < len; i++) {
                var ID = $('[name="Columns"]')[i].id;
                var columnName = ID.split("|");
                if (columnName[0].toUpperCase == "CREATEBY") {
                    Values[columnName[0]] = window.btoa(JSON.parse(localStorage.Result).User_ID);
                }
                else if (columnName[0].toUpperCase() == "CREATEDATETIME") {
                    Values[columnName[0]] = window.btoa(GetNow());
                }
                else if (columnName[0].toUpperCase() == "STATUS")
                    Values[columnName[0]] = window.btoa(true);
                else {
                    if ($('[name="Columns"]')[i].type == "checkbox")
                        Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].checked);
                    else
                        Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].value);
                }
            }

            var values = JSON.stringify(Values);
            $.ajax({
                type: "POST",
                url: GlobalURL + '/api/CrudService/createTableRow',
                data: JSON.stringify({
                    "tablename": document.getElementsByTagName("tablename")[0].innerHTML,
                    "Values": values
                }),

                //data: Htmlname ,
                contentType: 'application/json; charset=utf-8',
                //dataType: 'json', 
                success: function (val) {
                    if (val.createTableRowResult == "true") {

                    }
                    //location.reload();
                    $("#addModal .close").click();
                    gridLoad();
                },
                error: function (e) {

                }
            });
        }
    });
    $("#UpdateForm").on("submit", function (e) {
        e.preventDefault();
        if (validator1.validate()) {
            var IDName = $('[name="EditColumns"]')[0].id.split("|");
            var ID = {};
            ID[IDName[0]] = $('[name="EditColumns"]')[0].value;
            columns = $('[name="EditColumns"]');
            var Values = {
                "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
                //  "UpdateDatetime": window.btoa(GetNow())
            }
            for (var i = 1; i < columns.length; i++) {
                var z = $('[name="EditColumns"]')[i].id.split("|");
                if ($('[name="EditColumns"]')[i].type == "checkbox") {
                    Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].checked);
                }
                else {

                    Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].value);
                }
            }
            var values = JSON.stringify(Values);
            var Id = JSON.stringify(ID);
            $.ajax({
                type: "POST",
                url: GlobalURL + '/api/CrudService/updateTableRow',
                data: JSON.stringify({
                    'tablename': document.getElementsByTagName("tablename")[0].innerHTML,
                    'Values': values,
                    'ID': Id
                }),

                //data: Htmlname ,
                contentType: 'application/json; charset=utf-8',
                //dataType: 'json', 
                success: function (val) {

                    //location.reload();
                    $("#editModal .close").click();
                    gridLoad();
                    if (val.updateTableRowResult == "true") {

                    }
                },
                error: function (e) {

                }
            });

        }
    });
});
function cChart(dataSource) {
    var label1 = "";
    if (dataSource["Chart_Type"].toLowerCase() == "pie" || dataSource["Chart_Type"].toLowerCase() == "donut") {
        label1 = "#= category # - #= value#";
    }
    var width1, height1;
    var windowwidth = $(window).width();
    var windowheight = $(window).height();
    if (windowwidth > 1500 && windowheight > 500) {
        width1 = (dataSource["Width"] * 15).toString();
        height1 = dataSource["Height"];
    }
    else {
        width1 = (dataSource["Width"] * 8).toString();
        height1 = dataSource["Height"];
    }
    $('#' + dataSource["ID"]).kendoChart({
        dataSource: {
            data: JSON.parse(dataSource["data"]),
        },
        seriesColors: ['#3398db ', '#19bc9c ', '#fad231 ', '#9b59b6 ', '#e74c3c '],
        chartArea:
        {
            width: width1,
            height: height1
        },
        legend: {
            position: "top"
        },
        seriesDefaults: {
            type: dataSource["Chart_Type"].toLowerCase(),
            labels: {
                template: label1,
                position: "outsideEnd",
                visible: true,
                background: "transparent"
            }

        },

        series: [{
            categoryField: dataSource["Column_Number"],
            field: dataSource["Row_Number"],
            gap: 8,
            overlay: {
                gradient: "none"
            },
            border: {
                width: 0
            },

        }],
        valueAxis: {
            field: dataSource["Row_Number"],
            labels: {
                format: "{0}"
            },
            line: {
                visible: false
            },
            axisCrossingValue: 0
        },
        categoryAxis: {

            field: dataSource["Column_Number"],
            labels: {
                rotation: -30,
            },
            line: {
                visible: true
            },
            majorGridLines: {
                visible: false
            }

        },
        tooltip: {
            visible: true,

            template: "${category} - ${value}"
        }

    })
}
function loadCharts() {
    var x = localStorage.getItem("Result");
    var role = JSON.parse(x);
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/Dashboard/getCharts?roleid=' + role.Role_ID + '&monitor=' + $("#PageTitle")[0].innerHTML.trim(),
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            
            console.log(result);

            var dataSource = $.parseJSON(result);

            //var y = dataSource[0].PortalName;
            // console.log(y);
            //$('.chart').innerHTML = dataSource[0].PortalName;

            console.log(dataSource);

            var temp = [];
            console.log(x);
            var j;
            $("#chart_view").html("");
            $("#loader").hide();
            for (j = 0; j < dataSource.length; j++) {
                
                var templateContent = $("#chartTemplate").html();
                var template = kendo.template(templateContent);

                //Create some dummy data

                if (j % 2 == 0 && j != 0) dataSource[j].pr = 12;
                else dataSource[j].pr = 6;
                //render the template
                var chartTemplate = template(dataSource[j]).replace("Chart_ID", dataSource[j].ID.split("#")[1]);
                // chartTemplate.replace("Chart_ID", dataSource[j].ID);

                $("#chart_view").append(chartTemplate);
                if (dataSource[j]["data"] != "false" && dataSource[j]["data"] != "Connection Failure") {
                    cChart(dataSource[j]);
                    //append the result to the page
                }
                else {
                    $("#WrongMsg" + dataSource[j]["ID"])[0].innerHTML = "Sorry! Something went wrong. Please check the KPI details and try again";
                    $("#WrongMsg" + dataSource[j]["ID"]).css("height", "300px");
                }
                //append the result to the page
            }
        },


        error: function (e) {
            //  alert("Something Wrong!!");
        }
    });
}
var row = 0;
var h = 0;
function loadNKPI() {
    
    var x = localStorage.getItem("Result");
    var role = JSON.parse(x);

    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/Dashboard/getKPI?roleid=' + role.Role_ID + '&monitor=' + $("#PageTitle")[0].innerHTML.trim(),
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            
            console.log("KPI-----" + result);
            var colorCSS = ["amber_grad", "red_grad", "green_grad", "cyan_grad"];
            var dataSource = $.parseJSON(result);

            //var y = dataSource[0].PortalName;
            // console.log(y);
            //$('.chart').innerHTML = dataSource[0].PortalName;

            console.log(dataSource);

            var temp = [];
            console.log(x);
            var j;
            $("#KPI").html("");
            $("#loader").hide();

            for (j = 0; j < dataSource.length; j++) {
                
                row++;
                var templateContent = $("#KPIRowTemplate").html();
                var template = kendo.template(templateContent);
                var chartTemplate = template(dataSource[j])
                var data = JSON.parse(dataSource[j]["data"]);

                for (k = 0; k < data.length; k++) {
                    // var randomColor = '#' + ('000000' + Math.floor(Math.random() * 16777215).toString(16)).slice(-6);
                    data[k]["colorClass"] = colorCSS[h];
                    if (h < colorCSS.length - 1) {
                        h++;
                    }
                    else {
                        h = 0;
                    }
                    var kpiDiv = $("#KPITemplate").html();
                    var kpitemplate = kendo.template(kpiDiv);
                    //  data[k]["randomColor"] = randomColor;
                    //data[k]["colorId"] = "KPI_" + row + "-" + k + " KPIClass";
                    chartTemplate += kpitemplate(data[k]);
                }


                $("#KPI").append(chartTemplate + "</div>");
                // cChart(dataSource[j]);
                //append the result to the page
            }
        },


        error: function (e) {
            // alert("Something Wrong!!");
        }
    });
}

function Insightclick() {
    if ($(".btn-insight")[0].attributes.insights.nodeValue == "NKPI") {
        $("#KPIRow").toggle(1500);
    }
    else if ($(".btn-insight")[0].attributes.insights.nodeValue == "KPI") {

        $("#chart_view").toggle(1500);

    }
}

var stepID;
var StepsdataSource;
var BotID1 = {};

function AcceptClick(e) {
    
    $("#grid").hide();
    $("#InsightDiv").hide();
    $("#BackButton").show();
    $("#StepPropertiesDetails").show();
    $("#DetailsDiv").show();
    $("#buttonsDiv").show();
    $("#RobotLabel")[0].innerHTML = $(e).parent().closest('tr').find('td')[1].innerHTML;
    $("#IDLabel")[0].innerHTML = $(e).parent().closest('tr').find('td')[0].innerHTML;
    $("#StepLabel")[0].innerHTML = $(e).parent().closest('tr').find('td')[3].innerHTML;
    stepID = $(e).parent().closest('tr').find('td')[4].innerHTML;
    BotID1["ID"] = $(e).parent().closest('tr').find('td')[0].innerHTML;

    $.ajax({
        type: "POST",
        url: GlobalURL + '/api/CrudService/getStepProperties?stepId=' + stepID,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            StepsdataSource = $.parseJSON(val);
            for (var i in StepsdataSource) {
                if (StepsdataSource[i]["StepProperty"] == "Title") {
                    $("#TitleLabel")[0].innerHTML = StepsdataSource[i]["StepPropertyValue"];
                }
                else if (StepsdataSource[i]["StepProperty"] == "DisplayMessage") {
                    $("#DisplayNameLabel")[0].innerHTML = StepsdataSource[i]["StepPropertyValue"];
                }

            }
        },
        error: function (e) {
            //$("#feedbackMsg").html("Something Wrong.");
        }
    });
}
function RejectClick(e) {
    
    RobotID1 = {};
    RobotID1["ID"] = $(e).parent().closest('tr').find('td')[0].innerHTML;
    var RobotID = JSON.stringify(RobotID1);
    Values = {
        "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
        //  "UpdateDatetime": window.btoa(GetNow())
    };
    Values["MsgStatus"] = window.btoa("Rejected");
    var values = JSON.stringify(Values);

    $.ajax({
        type: "POST",
        url: GlobalURL + '/api/CrudService/updateTableRow?',
        data: JSON.stringify({
            'tablename': 'Inbox',
            'Values': values,
            'ID': RobotID,
        }),
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            if (val.updateTableRowResult == "true") {
                layoutRefresh(window.location.origin + "/Views/Layout.html?generatedHTML/Inbox_1", 0);
            }
        },
        error: function (e) {
            //$("#feedbackMsg").html("Something Wrong.");
        }
    });
}
$("#detailsForm").on("submit", function (e) {
    e.preventDefault();
    //function SubmitClick() {
    
    //if ($("#InputValue1").val() != "") {
    Values = {
        "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
        //  "UpdateDatetime": window.btoa(GetNow())
    };
    Values["MsgStatus"] = window.btoa("Accepted");
    var values = JSON.stringify(Values);
    var BotID = JSON.stringify(BotID1);
    $.ajax({
        type: "POST",
        url: GlobalURL + '/api/CrudService/updateTableRow?',
        data: JSON.stringify({
            'tablename': 'Inbox',
            'Values': values,
            'ID': BotID,
        }),
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            if (val.updateTableRowResult == "true") {
                layoutRefresh(window.location.origin + "/Views/Layout.html?generatedHTML/Inbox_1", 0);
            }
        },
        error: function (e) {
            //$("#feedbackMsg").html("Something Wrong.");
        }
    });
    //}

});
function BackClick() {
    $("#BackButton").hide();
    $("#StepPropertiesDetails").hide();
    $("#DetailsDiv").hide();
    $("#buttonsDiv").hide();
    $("#grid").show();
    $("#InsightDiv").show();
}
