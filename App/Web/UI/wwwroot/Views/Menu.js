﻿
function getMenuItems() {
    var x = localStorage.getItem("Result");
    var result = JSON.parse(x);
    $("#MenuUl li").remove();
    var data1 = localStorage.getItem("MenuItems");
    var dataSource = JSON.parse(data1);
    screenReload(dataSource);
    getMenuItemsForScreen();
   
}
function screenReload(dataSource) {
    for (var i = 0; i < dataSource.length; i++) {
        var line = "";
        if (dataSource[i].parentId == null) {
            if (typeof ($("#PageTitle")[0]) != typeof (undefined)) {
                if ($("#PageTitle")[0].innerText != "" && $("#PageTitle")[0].innerText.toUpperCase() == dataSource[i].Name.toUpperCase()) {

                    line += "<li {mouseEvents} name='Menus' id='" + dataSource[i].id + "'  class='{TreeClasses} active {menuopen}'>";
                    menuID = dataSource[i].id;

                }
                else {
                    line += "<li {mouseEvents} name='Menus' id='" + dataSource[i].id + "' class='{TreeClasses}{menuopen}'>";
                }
            }
            else {
                line += "<li {mouseEvents} name='Menus' id='" + dataSource[i].id + "'  class='{TreeClasses}{menuopen}'>";
            }
        }
        if (dataSource[i].parentId == null) {
            var imageClass = dataSource[i].Class.split("|")
            line += "<a title='" + dataSource[i].Name + "' href='" + dataSource[i].Path + "'><div class='" + imageClass[0] + "'><i class='" + imageClass[2] + "'></i></div> <span class='paren'>" + dataSource[i].Name.trim() + "</span><i class='tooltiptext'>" + dataSource[i].Name + "</i>[iconTemplate]</a>";
        }
        line += "<ul id='ul" + dataSource[i].id + "' class='treeview-menu'>";
        for (j = 0; j < dataSource.length; j++) {

            if (dataSource[j].parentId == dataSource[i].id) {
                line = line.replace("{mouseEvents}", "");

                line = line.replace("{TreeClasses}", "treeview");
                line = line.replace("[iconTemplate]", "<span  class='pull-right-container'><i id='i" + dataSource[i].id + "'class='fa fa-angle-left pull-right'></i></span>");
                if (typeof ($("#PageTitle")[0]) != typeof (undefined)) {
                    if ($("#PageTitle")[0].innerText != "" && $("#PageTitle")[0].innerText.toUpperCase() == dataSource[j].Name.toUpperCase()) {

                        line = line.replace("{menuopen}", " active menu-open")
                        line += "<li name='Menus' id='" + dataSource[j].id + "' class='active {SecondTreeClasses} {Secondmenuopen}'>";

                    }
                    else {
                        line += "<li name='Menus' id='" + dataSource[j].id + "' class='{SecondTreeClasses}{Secondmenuopen}'>";
                    }
                }
                else {
                    line += "<li name='Menus' id='" + dataSource[j].id + "' class='{SecondTreeClasses}{Secondmenuopen}'>";
                }
                line += "<a title='" + dataSource[j].Name + "' href='" + dataSource[j].Path + "'><i class='" + dataSource[j].Class + "'></i> <span>" + dataSource[j].Name.trim() + "</span><i class='tooltiptext'>" + dataSource[j].Name + "</i>[SecondIconTemplate]</a>"

                line += "<ul id='ul" + dataSource[i].id + "' class='treeview-menu {activeColor}'>";

                for (k = 0; k < dataSource.length; k++) {

                    if (dataSource[k].parentId == dataSource[j].id) {

                        line = line.replace("{SecondTreeClasses}", "treeview");

                        line = line.replace("[SecondIconTemplate]", "<span  class='pull-right-container'><i id='i" + dataSource[j].id + "'class='fa fa-angle-left pull-right'></i></span>");
                        if (typeof ($("#PageTitle")[0]) != typeof (undefined)) {
                            if ($("#PageTitle")[0].innerText.toUpperCase() == dataSource[k].Name.toUpperCase()) {
                                line = line.replace("{Secondmenuopen}", " active menu-open");
                                line = line.replace("{activeColor}", "activeColor");
                                line = line.replace("{menuopen}", " active menu-open")
                                line += "<li name='Menus' id='" + dataSource[k].id + "' class='active'>";
                            }
                            else {
                                line += "<li name='Menus' id='" + dataSource[k].id + "'>";
                            }
                        }
                        else {
                            line += "<li name='Menus' id='" + dataSource[k].id + "'>";
                        }
                        line += "<a title='" + dataSource[k].Name + "' href='" + dataSource[k].Path + "'><i class='" + dataSource[k].Class + "'></i> <span>" + dataSource[k].Name.trim() + "</span><i class='tooltiptext'>" + dataSource[k].Name + "</i></a></li>"
                    }
                }
                line += "</ul></li>"
                line = line.replace("{activeColor}", "");
                line = line.replace("{Secondmenuopen}", "");
                line = line.replace("{SecondTreeClasses}", "");
                line = line.replace("[SecondIconTemplate]", "");
            }
        }
        line += "</ul></li>";
        line = line.replace("{menuopen}", "");
        line = line.replace("{mouseEvents}", "");
        line = line.replace("{TreeClasses}", "");
        line = line.replace("[iconTemplate]", "");

        $("#MenuUl").append(line);
    }
}
function getMenuItemsForScreen() {
    var x = localStorage.getItem("Result");
    var result = JSON.parse(x);
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getMenuRoleId?RoleId=' + result.Role_ID + '&LOB= ' + result.LOB,
        contentType: 'application/json; charset=utf-8',
        async: false,
        success: function (val) {

            var dataSource = $.parseJSON(val);
            $("#MenuUl li").remove();
            screenReload(dataSource);
        },
        error: function (e) {
            getMenuItemsForScreen();
        }
    });
}
function treefunction(e) {
    var x = $("#" + e).parent().find('#ul' + e).find(".active");
    if (x[0] == null) {


        $("#" + e).parent().find('#ul' + e).show(500);

        $("#i" + e).toggleClass("rotateMenuItems")
    }
}
function treeHidefunction(e) {
    var x = $("#" + e).parent().find('#ul' + e).find(".active");
    if (x[0] == null) {

        $("#" + e).parent().find('#ul' + e).hide(500);
        $("#i" + e).toggleClass("rotateMenuItems");
    }
}

$(document).on('click', "[name='Menus'] a", function (e) {
    e.preventDefault();
    if (e.currentTarget.innerText.trim() == "Command Center") {
        window.open(e.currentTarget.href, "_blank");
    }
    else if (e.currentTarget.attributes[1].nodeValue != '#') {
        menuID = $(e.currentTarget).parent()[0].id
        $("#loader").hide();
        if (menuID == "4" || menuID == "5") {
            var pageCurrentLocation = window.location.href;
            pageCurrentLocation = pageCurrentLocation.split("?")[1];
            var pageNavigateLocation = e.currentTarget.href;
            pageNavigateLocation = pageNavigateLocation.split("?")[1];
            if (pageCurrentLocation != pageNavigateLocation) {
                layoutRefresh(e.currentTarget.href, 0);
                $("body").removeClass("sidebar-collapse");
                $("[name='Menus']").removeClass('active');
                $(".treeview").addClass('menu-open');
                $(e.currentTarget).parent().addClass('active');
               // $(".treeview-menu").show();
            }
            else {
                $("body").removeClass("sidebar-collapse");
                $("[name='Menus']").removeClass('active');
                $(e.currentTarget).parent().addClass('active');
            }
        }
        else {
            var pageCurrentLocation = window.location.href;
            pageCurrentLocation = pageCurrentLocation.split("?")[1];
            var pageNavigateLocation = e.currentTarget.href;
            pageNavigateLocation = pageNavigateLocation.split("?")[1];
            if (pageCurrentLocation != pageNavigateLocation) {
                layoutRefresh(e.currentTarget.href, 0);
            }
            $("[name='Menus']").removeClass('active');
            $(e.currentTarget).parent().addClass('active');
        }
    }
});

window.onpopstate = function (event) {
    layoutRefresh(document.location.href, 1);
};

var finalURL;
var RequestURL;
var RequestTempURL;
function layoutRefresh(dest, popstate) {
    $("#loader").show();
    $(".content-wrapper").css("margin-left", "0%");

    $("#PageTitle").css("display", "none");
    finalURL = dest;
    RequestTempURL = dest.split("?");
    RequestURL;
    var url = dest.split("/");
    RequestURL = url[0] + "//" + url[2] + "/" + url[3] + "/" + RequestTempURL[1] + ".html";
    $.get(RequestURL, function (data) {
        $(".content-wrapper").css("margin-left", "");
        $("footer").css("margin-left", "");

        switch (RequestTempURL[1].split("/").length) {
            case 1: if (RequestTempURL[1].split("/")[0] == "Dashboard") {
              $("body").removeClass("sidebar-collapse");
            // $("body").addClass("sidebar-collapse");
                $('.logo_text .logo_sc').attr('src', 'Images/Genbase_header_logoc.svg');
                $("#loader").show();

                $(".content-wrapper").css("margin-left", "0% !important");
                $(".main-sidebar").addClass("dash_sider");
                $(".main-sidebar").show();
             $(".sidebar-toggle").hide();
            //  $(".sidebar-toggle").show();
                $("footer").css("margin-left", "0%");
                $(".d-lay").css("display", "none");
                $("#myInput").css("display", "block");
                $("#myInput").css("width", "100%");
                $("#loader").hide();
            }
            else {
                if ($("#MenuUl")[0].innerHTML.trim() == "") {
                    if (localStorage.getItem('layoutRefresh1') == 'true') {
                        localStorage.setItem('layoutRefresh1', 'false');
                        getMenuItemsForScreen();
                    }
                    else {
                        getMenuItems();
                    }
                }
                $('.logo_text .logo_sc').attr('src', 'Images/Genbase_header_logo.svg');
                $("#loader").show();
          //  $("body").removeClass("sidebar-collapse");
              $("body").addClass("sidebar-collapse");
                $(".sidebar-toggle").css("display", "block");
                $(".main-sidebar").show();
                $(".main-sidebar").css("background", "#ffffff");
                $(".main-sidebar").css("border", "1px solid #ffffff");
                $(".main-sidebar").css("box-shadow", "#ccc 0px 4px 4px 0px");
                $(".content-header").show();
                $(".content-header").css("display", "block");
                $(".content-header").css("background", "#ffffff");
                $(".content-header").css("box-shadow", "0px 2px 2px 0px rgba(116, 44, 69, 0.21)");
                $(".d-lay").css("display", "none");
                $("#myInput").css("display", "block");
                $("#myInput").css("width", "100%");
                $('.main-header').show();
                $(".treeview-menu").hide();
                // $(".treeview-menu").show();
                $(".treeview").removeClass('menu-open');
                $("#loader").hide();
            }
                break;
            case 2: if (RequestTempURL[1].split("/")[1] == "Dashboard") {
                $("body").removeClass("sidebar-collapse");
                $('.logo_text .logo_sc').attr('src', 'Images/Genbase_header_logoc.svg');
                $("#loader").show();
                $(".main-sidebar").addClass("dash_sider");
                $(".main-sidebar").hide();
                $(".sidebar-toggle").hide();
                $(".content-wrapper").css("margin-left", "0% !important");
                $("footer").css("margin-left", "0%");
                $(".d-lay").css("display", "inline-block");
                $("#myInput").css("display", "block");
                $('.main-header').hide();
                $("#loader").hide();
            }
            else {
                if ($("#MenuUl")[0].innerHTML.trim() == "") {
                    getMenuItems();
                }
                $("#loader").show();
             // $("body").removeClass("sidebar-collapse");
               $("body").addClass("sidebar-collapse");
                $('.logo_text .logo_sc').attr('src', 'Images/Genbase_header_logo.svg');

                $(".sidebar-toggle").css("display", "block");
                $(".main-sidebar").show();
                $(".main-sidebar").css("background", "#ffffff");
                $(".main-sidebar").css("border", "1px solid #ffffff");
                $(".main-sidebar").css("box-shadow", "#ccc 0px 4px 4px 0px");
                $(".content-header").show();
                $(".content-header").css("display", "block");
                $(".content-header").css("background", "#ffffff");
                $(".content-header").css("box-shadow", "0px 2px 2px 0px rgba(116, 44, 69, 0.21)");
                $(".d-lay").css("display", "none");
                $("#myInput").css("display", "block");
                $("#myInput").css("width", "100%");
                $('.main-header').show();
                $("#loader").hide();
            }
                break;
        }
        if (!popstate)
            history.pushState({ state: 1 }, '', finalURL);
        $('.content-wrapper').html(data);
        $("[name='Menus']").removeClass('active');
        $(".treeview-menu").hide();
       // $(".treeview-menu").show();
        $(".treeview").removeClass('menu-open');       
        for (i in $("[name='Menus']")) {
            if (typeof ($("#PageTitle")[0]) != typeof (undefined)) {
                if ((typeof ($("[name='Menus'] a")[i].innerText) != typeof (undefined)) && $("[name='Menus'] a")[i].innerText.trim().toLowerCase() == $("#PageTitle")[0].innerHTML.trim().toLowerCase()) {
                    $($("[name='Menus']")[i]).addClass('active');
                    $($("[name='Menus']")[i]).parents('ul.treeview-menu').css("display", "block");
                    $($("[name='Menus']")[i]).parents('li').addClass('menu-open');
                    $($("[name='Menus']")[i]).parents('li').addClass('active');
                    break;
                }
            }
        }
        if (RequestTempURL[1].split("/")[0] == "Screen_Generation" || RequestTempURL[1].split("/")[0] == "generatedHTML") {
            $("body").removeClass("sidebar-collapse");
            $("[name='Menus']").removeClass('active');
            //$(e.currentTarget).parent().addClass('active');
            //$(".treeview-menu").show();
            //$(".treeview").removeClass('menu-open');
           // $(".treeview-menu").show();
            //$(e.currentTarget).parent().addClass('active');
        }
        $("#loader").hide();

    }).fail(function () {
        $("#confirmationModal").modal('show');
    });
}
var elasticSearch = [
    {
        "name": "User",
        "code": "user",
        "url": "/Views/Layout.html?generatedHTML/User_1"
    },
    {
        "name": "Admin",
        "code": "user",
        "url": "/Views/Layout.html?generatedHTML/User_1"
    },
    {
        "name": "Monitor",
        "code": "monitor",
        "url": "/Views/Layout.html?Monitor"
    },
    {
        "name": "Genbase Studio",
        "code": "robot",
        "url": "/Views/Layout.html?Viewsstudio"
    },
    {
        "name": "Screen generation",
        "code": "control",
        "url": "/Views/Layout.html?Screen_Generation"
    }
];
var options = {
    data: elasticSearch,
    getValue: "name",
    list: {
        onHideListEvent: function (e) {
            if (typeof ($("#myInput").parent().find('li.selected span')) != 'undefined' && $("#myInput").parent().find('li.selected span')[0].attributes["url"].nodeValue != "undefined") {
                window.location.href = $("#myInput").parent().find('li.selected span')[0].attributes["url"].nodeValue;
            }
        },
        match: {
            enabled: true
        },
        maxNumberOfElements: 8
    },
    template: {
        type: "custom",
        method: function (value, item) {
            return "<span class='flag flag-" + (item.code).toLowerCase() + "' url='" + item.url + "' ></span>" + value;
        }
    },
    theme: "round"
};
function regconfirmFunction() {
    $("#confirmationModal").modal('hide');
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableDataByUserID?tablename=GetScreenIDForRegeneration&CreateBy=' + menuID,
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            var dataSource = $.parseJSON(val);
            ScreenIDForRegenration = dataSource[0].ScreenId;
            layoutRefresh(window.location.origin + "/Views/Layout.html?Add_newgeneration?screen_id=" + ScreenIDForRegenration, 0);
        },
        error: function (e) {

        }
    });
    $("#loader").hide(); // or whatever
}
function cancelFunction() {
    $("#confirmationModal").modal('hide');
    layoutRefresh(window.location.origin + "/Views/Layout.html?Screen_Generation", 0);
}
$(document).ready(function () {
    $("#myInput").easyAutocomplete(options);
    $(".eac-item").click(function (e) {
        window.location.href = $(e.currentTarget).find('span')[0].attributes["url"].nodeValue;
    })
});
function loadProjects() {
    var x = localStorage.getItem("Result");
    var UserDetails = JSON.parse(x);
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getStudio?roleId=' + UserDetails.Role_ID + '&userId=' + UserDetails.UserName,
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            var dataSource = $.parseJSON(val);
            for (i in dataSource) {
                var newData = { name: dataSource[i].NAME, code: "Project", url: "/Views/planogram.html?" + dataSource[i].ID } // a new movie object
                elasticSearch.push(newData);
            }
        },
        error: function (e) {
            loadProjects();
        }
    });
}