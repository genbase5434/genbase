﻿

$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/getServices',
        contentType: 'application/json; charset=utf-8',

        success: function (result) {
            //console.log(val);
            
            
            var methods = JSON.parse(result);

            $('.cab').append("<div class='col-md-12 caby-header'>  <b class='col-md-1'> Method </b> <b class='col-md-5'> Path </b>  <b class='col-md-2'> Function  </b> <b class='col-md-1'> Input </b><b class='col-md-1'>  Output </b><b class='col-md-2'> </b></div> <hr class='col-md-12 hr-break'>");

            for (var m = 0; m < methods.length; m++)
            {
                var flaggo;
                if (methods[m].Flag == null || methods[m].Flag.trim() == "")
                {
                    flaggo = "Desktop";
                }
                else
                {
                    flaggo = methods[m].Flag;
                    var jsontyper = "[\n\t{\n"
                    var paramser = methods[m].Parameters;
                    var typer = paramser.split(" , ");
                    var router = methods[m].Route;

                    var router1 = router.split("?");

                    if (typeof (router1[1]) != typeof (undefined))
                    {
                        var tyker = router1[1].split("&");
                        var typer = paramser.split(" , ");
                        var tyketyper = "";


                        for (var t = 0; t < tyker.length; t++)
                        {
                            tyketyper = tyketyper + typer[t].split(" ")[0] + "  " + tyker[t] + "\n";

                            var typees = typer[t].split(" ")[0];
                            var tx = "";

                            if (typees.toLowerCase() == "system.string")
                            {
                                tx = "string";
                                if (t == tyker.length - 1) {
                                    jsontyper = jsontyper + "\t\t\"" + tyker[t].split('=')[0] + "\" : \"" + tx + "\"\n";
                                }
                                else {
                                    jsontyper = jsontyper + "\t\t\"" + tyker[t].split('=')[0] + "\" : \"" + tx + "\",\n";
                                }
                            }
                            else if (typees.toLowerCase() == "int32")
                            {
                                tx = "int";
                                if (t == tyker.length - 1) {
                                    jsontyper = jsontyper + "\t\t\"" + tyker[t].split('=')[0] + "\" : " + 0 + "\n";
                                }
                                else {
                                    jsontyper = jsontyper + "\t\t\"" + tyker[t].split('=')[0] + "\" : " + 0 + ",\n";
                                }
                            }
                            else
                            {
                                tx = typees.toLowerCase();
                            }



                        }

                        //jsontyper = jsontyper.slice(0, -1);
                    }
                    else
                    {
                        tyketyper = "No Request Parameters Exist"
                    }
                    jsontyper = jsontyper + "\t}\n]";

                    $('.cab').append("<div class='col-md-12 caby'> " +
                                     "<b class='col-md-1'> <i class='btn btn-info http-typer-" + flaggo.toLowerCase() + "'> " + flaggo + " </i> </b>" +
                                     "<span class='col-md-5'><code class='color-red'> /" + methods[m].Route + " </code> </span>" +
                                     "<span class='col-md-2 color-steelblue'> " + methods[m].Name + "  </span>" +
                                     "<span class='col-md-1 color-steelblue'> Json </span>" +
                                     "<span class='col-md-1 color-steelblue'> Json </span>" +
                                     "<div class='col-md-2 btn-set btn-group'>"+
                                     //"<button onclick='slideNextDiv(this)' class='col-md-4 btn btn-success'> sync </button> " +
                                     "<button onclick='slideNextDiv(this)' class='col-md-6 btn btn-info'> edit </button>" +
                                     "<button class='col-md-6 btn btn-danger'> delete </button> </div> </div>" +
                                     "<div class='col-md-12 caby-desc' id='tab-content' style='display:none;'> " +
                                        "<div class='col-md-12'><label for='descerer' class='col-md-2'> Description </label>" +
                                        "<p id='descerer' class='col-md-10'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum </p></div>" +
                                        "<div class='reqheader col-md-12'><label class='col-md-2' style='padding-top:2%;' for='requer'>Headers</label><textarea style='background:#ddd; border-radius:1%; margin-bottom: 15px;margin-top: 2%;' class='col-md-10' cols='5' disabled readonly id='reser'>  </textarea> </div>" +

                                        "<div class='reqres col-md-12'>" +
                                        "<label class='col-md-2' style='padding-top:5%;' for='requer'>Request</label>"+
                                        "<textarea data-class='" + methods[m].ClassName + "' data-desc='" + methods[m].Description + "' data-route='" + methods[m].Route + "' data-name='" + methods[m].Name + "' data-flag='" + methods[m].Flag + "' data-returns='" + methods[m].ReturnType + "' data-params='" + methods[m].Parameters + "' style='background:#ddd; height:200px;border-radius:1%; margin-bottom: 15px;' class='col-md-10' data-types='" + jsontyper + "' cols='5' id='requer'>" + jsontyper + "</textarea>" +
                                        "<div class='col-md-12'><button type='button' class='btn btn-info pull-right chucker' onclick='onChecker(this)'>Check</button></div>" +
                                     "</div></div> <hr class='col-md-12 hr-break'>");
                    //$('.cab').append("<div class='col-md-12 caby'><button onclick='openAPI(this)' class='tablinks col-md-12 service-click' data-class='" + methods[m].ClassName + "' data-desc='" + methods[m].Description + "' data-route='" + methods[m].Route + "' data-name='" + methods[m].Name + "' data-flag='" + methods[m].Flag + "' data-returns='" + methods[m].ReturnType + "' data-params='" + methods[m].Parameters + "'> <i  class='btn btn-info col-md-3 http-typer-" + flaggo.toLowerCase() + "'> " + flaggo + " </i><b class='col-md-9'> /" + methods[m].Name + "</b> </button></div>")
                }
            }
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
});


var slideNextDiv = function (lem)
{
    
    if ($(lem).parent().parent().next().css('display').trim() == 'block')
    {
        $('.caby-active').removeClass('caby-active');
        $('.caby-desc').hide(500);
        //$('.caby-desc').css('display', 'none');
    }
    else
    {
        $('.caby-desc').css('display', 'none');
        $('.caby-active').removeClass('caby-active');
        $(lem).parent().parent().addClass('caby-active');
        
        //$(lem).parent().parent().next().css('display', 'block');
        $(lem).parent().parent().next().show(500);
    }
}

var openAPI = function (lem)
{

    var flager = lem.getAttribute("data-flag");
    var namer = lem.getAttribute("data-name");
    var returner = lem.getAttribute("data-returns");
    var paramser = lem.getAttribute("data-params");
    var router = lem.getAttribute("data-route");
    var descer = lem.getAttribute("data-desc");
    var classer = lem.getAttribute("data-class");

    var router1 = router.split("?");

    $('.http-type-gray').removeClass('http-type-gray');

    lem.classList.add("http-type-gray");
    $('#tab-content').html('');

    var topdef = "<h3 id='method-name' data-class='' class='box-title box-header with-border'>" + namer + "</h3>";
    $('#tab-content').append(topdef);

    
    var jsontyper = "[\n\t{\n"
    if (typeof (router1[1]) != typeof (undefined))
    {
        var tyker = router1[1].split("&");
        var typer = paramser.split(" , ");
        var tyketyper = "";


        for (var t = 0; t < tyker.length; t++)
        {
            tyketyper = tyketyper + typer[t].split(" ")[0] + "  " + tyker[t] + "\n";

            var typees = typer[t].split(" ")[0];
            var tx = "";

            if (typees.toLowerCase() == "system.string")
            {
                tx = "string";
                if (t == tyker.length - 1)
                {
                    jsontyper = jsontyper + "\t\t\"" + tyker[t].split('=')[0] + "\" : \"" + tx + "\"\n";
                }
                else
                {
                    jsontyper = jsontyper + "\t\t\"" + tyker[t].split('=')[0] + "\" : \"" + tx + "\",\n";
                }
            }
            else if (typees.toLowerCase() == "int32")
            {
                tx = "int";
                if (t == tyker.length - 1)
                {
                    jsontyper = jsontyper + "\t\t\"" + tyker[t].split('=')[0] + "\" : " + 0 + "\n";
                }
                else
                {
                    jsontyper = jsontyper + "\t\t\"" + tyker[t].split('=')[0] + "\" : " + 0 + ",\n";
                }
            }
            else
            {
                tx = typees.toLowerCase();
            }

            
            
        }

        //jsontyper = jsontyper.slice(0, -1);
    }
    else
    {
        tyketyper = "No Request Parameters Exist"
    }
    jsontyper = jsontyper + "\t}\n]";

    var middledef = "<label for='headerer' class='col-md-2'> API URL </label>" +
                    "<h4 id='headerer' class='col-md-10' style='color:Teal;font-weight:600'> " + window.location.origin + "/services/" + classer + ".svc/" + router1[0] + "</h4>" +
                    "<br> <br>" +
                    "<label for='descerer' class='col-md-2'> Description </label>" +
                    //"<p> " + descer + "</p>" + 
                    "<p id='descerer' class='col-md-10'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum </p>" +

                    "<div class='reqres'>" +
                    "<label class='col-md-2' style='padding-top:5%;' for='requer'>Request</label><textarea style='background:#ddd; height:200px;border-radius:1%; margin-bottom: 15px;' class='col-md-10' data-types='" + jsontyper + "' cols='5' id='requer'>" + jsontyper + "</textarea>" +
                    "<div class='col-md-12'><button type='button' class='btn btn-info pull-right' onclick='onChecker()'>Check</button></div>" +
                    "</div>" 

    $('#tab-content').append(middledef);

    //var rtrCheck = router.replace(/\{.*\}/g, 'XXXTEXTBOXXXX');
    var rtrCheck = window.location.origin + "/services/" + classer + ".svc/"+ router1[0] + "?";
    var rtrCheck1 = "";


    var tyketyperarr = tyketyper.split("\n");
    tyketyperarr = tyketyperarr.filter(value => Object.keys(value).length !== 0);

    for (var t = 0; t < tyketyperarr.length; t++)
    {
        var gener = tyketyper.split("\n")[t].split(" ");
        gener = gener.filter(value => Object.keys(value).length !== 0);
        rtrCheck1 = rtrCheck1 + gener[1].trim().replace(/\{.*\}/g, "<input class='validify inner-text' type='text' />") + "&";
    }



    var checkdef = "<div class='checkreqres form-group col-md-12'>" +
                    "<label for='checkerp' class='col-md-2'> Edit URL </label>" +
                    "<p id='FerguService' style='color:#842b3b;line-height: 3' class='col-md-10'>" + rtrCheck + rtrCheck1.slice(0, -1) + "</p>" +
                    "<button type='button' class='btn btn-info pull-right' onclick='onChecker()'>Check</button>"+
                   "</div>"

    //$('#tab-content').append(checkdef);
}

function onChecker(lem) {
    var textarear = $(lem).parent().prev();
    var tabcontenter = $(lem).parent().parent().parent();
    var fergu = "";
    fergu = $(textarear).val();
    fergu = fergu.replace('[', '').replace(']', '');

    fergu = fergu.replace('{', '').replace('}', '');


    fergu = fergu.replace(/['"]+/g, '');

    
    //var tergu = $('#headerer').text() + "?" + tergu;

    var httptype = $(textarear).attr('data-flag')
    var datatype = $(textarear).attr('data-params');
    var classtype = $(textarear).attr('data-class');
    var methodname = $(textarear).attr('data-name');

    $.ajax({
        type: "POST",
        url: '/services/CrudService.svc/postServices?obj=' + fergu.trim() + '&service=' + classtype.trim() + '&name=' + methodname,
        contentType: 'application/json; charset=utf-8',

        success: function (result) {
            console.log(result);
            $('.resdiv').remove();
            var resultDef = "<div class='col-md-12 resdiv'><label class='col-md-2' style='padding-top:2%; for='reser'>Response</label><textarea style='background:#ddd; border-radius:1%; margin-bottom: 15px;margin-top: 2%;' class='col-md-10' cols='5' disabled readonly id='reser'> " + result + " </textarea></div>";
            $(tabcontenter).append(resultDef);
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}