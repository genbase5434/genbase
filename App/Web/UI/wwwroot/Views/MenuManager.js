﻿(function ($) {
    $.fn.filetree = function (method) {
        var settings = { // settings to expose
            animationSpeed: 'fast',
            collapsed: true,
            console: false
        }
        var methods = {
            init: function (options) {
                // Get standard settings and merge with passed in values
                var options = $.extend(settings, options);
                // Do this for every file tree found in the documentColumns
                return this.each(function () {

                    var $fileList = $(this);

                    $fileList
                        .addClass('file-list')
                        .find('li')
                        .has('ul') // Any li that has a list inside is a folder root
                        .addClass('folder-root closed')
                        .on('click', 'a[href="#"]', function (e) { // Add a click override for the folder root links
                            e.preventDefault();
                            $(this).parent().toggleClass('closed').toggleClass('open');
                            return false;
                        });

                    //alert(options.animationSpeed); Are the settings coming in

                });
            }
        }
        if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.on("error", function () {
                //console.log(method + " does not exist in the file exploerer plugin");
            });
        }
    }

}(jQuery));
var Path;
var Type = "";
var Status = false;

$.ajax({
    type: "GET",
    url: GlobalURL + '/api/CrudService/getTableData?tablename=LOB',
    //data: Htmlname ,
    contentType: 'application/json; charset=utf-8',
    //dataType: 'json',
    success: function (val) {
        var dataSource = JSON.parse(val);
        //$.each(dataSource, function (i, item) {
        //    $('#AddMenuLOB').append($('<option>', {
        //        value: item.Name,
        //        text: item.Name,
        //        id:item.ID
        //    }));
        //});
        for (var i in dataSource) {
            $("#AddMenuLOB").append('<option value="' + dataSource[i]["ID"] + '">' + dataSource[i]["Name"] + '</option>')
        }
    },
    error: function (e) {
    }
});

var menutype;
var dragID;
var dropID;
var source = {};
var LOBValue;
$(document).on('dnd_start.vakata', function (event, data) {
    menutype = data.element.attributes[5].nodeValue;
    if (menutype == "1") {

        swal("", "You can move only child elements  ", "error")
    }

});
$(document).on('dnd_stop.vakata', function (event, data) {
    //console.log(data);
    source = {};
    //$(data.element.children[1].children[1]).hide();
    dropId = data.event.target.id.split('_')[1];
    source["id"] = data.element.id.split('_')[1];
    source["Name"] = data.element.text;
    source["Class"] = $(data.element).attr('custom');
    source["Path"] = $(data.element).attr('path');
    menutype = data.element.attributes[5].nodeValue;

    if (menutype != "1") {

        updateMenu(source, dropId);
    }
    else {

        data.helper.find('.jstree-icon').removeClass('jstree-ok').addClass('jstree-er');
        return false;
    }
    //updateMenu(source, dropId);
    getMenu();
})
$(document).ready(function () {

    var AddLOBValue;
    $('#AddMenuLOB').on('change', function () {
        LOBValue = this.value;
    });

    $("#SubmitForm").on("submit", function (e) {
        
        e.preventDefault();
        if ($("#RegisterBtn").text() == "Add") {
            if ($("#AddMenuPath")[0].disabled == false) {
                Path = "/Views/Layout.html?" + $("#AddMenuPath").val();
            } else {
                Path = "/Views/Layout.html?generatedHTML/" + $('#AddMenuName').val();
            }
            if ($("#AddMenuTypeMenu").prop("checked")) {
                Type = "1";
                AddLOBValue = "0";
            }
            else if ($("#AddMenuTypeItem").prop("checked")) {
                Type = "2";
                AddLOBValue = LOBValue;
            }
            if ($("#AddMenuActive").prop("checked")) {
                Status = true;
            }
            else {
                Status = false;
            }
            var x = localStorage.getItem("Result");
            var UserDetails = JSON.parse(x);
            var values = {};
            values["Name"] = $('#AddMenuName').val();
            values["Class"] = $('#AddMenuClass').val();
            values["Parent_ID"] = parentID;
            values["Path"] = Path;
            values["Type"] = Type;
            //values["order"] = $('#AddMenuOrder').val();
            values["Role_ID"] = UserDetails.Role_ID
            values["LOB"] = AddLOBValue;
            values["Status"] = Status;
            var Values = JSON.stringify(values);
            $.ajax({
                type: "POST",
                url: GlobalURL + '/api/CrudService/createTableRow?tablename=MENU',
                data: JSON.stringify(Values),
                contentType: 'application/json; charset=utf-8',
                success: function (val) {
                    if (val == "true") {
                        $.ajax({
                            type: "POST",
                            url: GlobalURL + '/api/CrudService/createTableRow?tablename=Role_Menu_Relation',
                            data: JSON.stringify(Values),
                            contentType: 'application/json; charset=utf-8',
                            success: function (val) {
                                if (val == "true") {
                                    getMenu();
                                    $("#AddMenuDiv").hide();
                                    $("#AddMenuName").val(" ");
                                    $("#AddMenuClass").val(" ");
                                    // $("#AddMenuOrder").val(" ");
                                    $("#AddMenuPath").val(" ");
                                    $("#AddMenuLOB").val(" ");
                                    if ($("#AddMenuPathCustom").prop("checked")) {
                                        $("#AddMenuPathCustom").attr('checked', false)
                                    }
                                    if ($("#AddMenuPathGenerated").prop("checked")) {
                                        $("#AddMenuPathGenerated").attr('checked', false)
                                    }
                                    if ($("#AddMenuTypeMenu").prop("checked")) {
                                        $("#AddMenuTypeMenu").attr('checked', false)
                                    }
                                    if ($("#AddMenuTypeItem").prop("checked")) {
                                        $("#AddMenuTypeItem").attr('checked', false)
                                    }
                                    if ($("#AddMenuActive").prop("checked")) {
                                        $("#AddMenuActive").attr('checked', false)
                                    }
                                    swal("", "Menu added successfully", "success", { closeOnClickOutside: false }); 
                                    $(".swal-button").on('click', function (event) {
                                        if ($(".swal-text")[0].innerHTML == "Menu Added Successfully") { location.reload(); }
                                    });
                                }
                                else
                                    swal("", "Menu not added successfully", "error")
                            },
                            error: function (e) {
                            }
                        });
                    }
                    else {
                        swal("", "Menu Name already exist", "error")
                    }
                },
                error: function (e) {
                }
            });
        }
        if ($("#RegisterBtn").text() == "Update") {
           
            var UpdateLOBValue;
            Path = $("#AddMenuPath").val();
            if ($("#AddMenuTypeMenu").prop("checked")) {
                Type = "1";
                UpdateLOBValue = "0";
            }
            else if ($("#AddMenuTypeItem").prop("checked")) {
                Type = "2";
                if (LOBValue == undefined)
                    LOBValue = $("#AddMenuLOB").val();
                UpdateLOBValue = LOBValue;
            }
            if ($("#AddMenuActive").prop("checked")) {
                Status = true;
            }
            else {
                Status = false;
            }
            var x = localStorage.getItem("Result");
            var UserDetails = JSON.parse(x);
            var values = {};
            values["Name"] = $('#AddMenuName').val();
            values["Class"] = $('#AddMenuClass').val();
            values["Parent_ID"] = parentID;
            values["Path"] = Path;
            values["Type"] = Type;
            //values["order"] = $('#AddMenuOrder').val();
            values["Role_ID"] = UserDetails.Role_ID;
            values["LOB"] = UpdateLOBValue;
            values["Status"] = Status;
            var Values = JSON.stringify(values);
            $.ajax({
                type: "POST",
                url: GlobalURL + '/api/CrudService/updateTableRow?tablename=MENU' + '&ID=' + EditID,
                data: JSON.stringify(Values),
                contentType: 'application/json; charset=utf-8',
                success: function (val) {
                    if (val == "true") {
                        getMenuItemsForScreen();
                        //getMenuItems();
                        getMenu();
                        $("#addmenudiv").hide();
                        $("#addmenuname").val(" ");
                        $("#addmenuclass").val(" ");
                        // $("#addmenuorder").val(" ");
                        $("#addmenupath").val(" ");
                        $("#addmenulob").val(" ");
                        if ($("#addmenupathcustom").prop("checked")) {
                            $("#addmenupathcustom").attr('checked', false)
                        }
                        if ($("#addmenupathgenerated").prop("checked")) {
                            $("#addmenupathgenerated").attr('checked', false)
                        }
                        if ($("#addmenutypemenu").prop("checked")) {
                            $("#addmenutypemenu").attr('checked', false)
                        }
                        if ($("#addmenutypeitem").prop("checked")) {
                            $("#addmenutypeitem").attr('checked', false)
                        }
                        if ($("#addmenuactive").prop("checked")) {
                            $("#addmenuactive").attr('checked', false)
                        }
                        swal("", "Menu updated successfully", "success", { closeOnClickOutside: false }); 
                        $(".swal-button").on('click', function (event) {
                            if ($(".swal-text")[0].innerHTML == "Menu updated successfully") { location.reload(); }
                        });

                    }
                    else {
                        swal("", "Menu not updated successfully", "error")
                    }
                },
                error: function (e) {
                }
            });
        }
    });
   
    $("#CancelBtn").click(function (e) {
        $("#SubmitForm").hide();

        e.preventDefault();
    });
    //    $("#CancelBtn").click(function () {
    //        $("#Submitform").fadeOut();
    //    });
   
    if (sessionStorage.getItem("iSessionState") == 'N') {
        //window.open('index.html');
        location.href = "../index.html";
    }
    else {

    }

    //$(".menu-content").scroll(function () {
    //    $("#contextMenu").css({ "top": event.pageY - x + "px" }).hide();
    //});
});
$('input:radio[name="Columns"]').change(
    function () {
        if ($("#AddMenuPathCustom").prop("checked")) {
            $("#AddMenuPath")[0].disabled = false;
        } else {
            $("#AddMenuPath")[0].disabled = true;
        }
    });
$('input:radio[name="rows"]').change(
    function () {
        if ($("#AddMenuTypeItem").prop("checked")) {
            $("#AddMenuLOB")[0].disabled = false;
        } else {
            $("#AddMenuLOB")[0].disabled = true;
        }
    });
$('input:radio[name="rows"]').change(
    function () {
        if ($("#AddMenuTypeMenu").prop("checked")) {
            Type = "Menu";
        } else {
            Type = "Item";
        }
    });
getMenu();
function getMenu() {
    
    $.ajax({
		type: "GET",
		url: GlobalURL + '/api/ScreenGeneration/getMenu?tablename=MENU',
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            var dataSource = JSON.parse(val);
            $("#TreeList").html("");
            $("#TreeList").hide();
            var i, j, k;
            for (i in dataSource) {
                if (dataSource[i].parentId == null) {
                    $("#TreeList")[0].innerHTML += "<li class='MenuManager_custom' for='parentNode'><a class='' id='Menu_" + dataSource[i].id + "' href='#'  Custom='" + dataSource[i].Class + "' customType = '" + dataSource[i].Type + "' Path='" + dataSource[i].Path + "' >" + dataSource[i].Name + " </a><ul id='subMenu_" + dataSource[i].id + "'></ul><button class='AddIcon' onclick='AddMenu(this)' style='background: transparent;'></button><button class='EditIcon' style='margin-left:1% !important;background: transparent;' onclick='editMenu(this)'></button></li>"
                }
            }
            for (j in dataSource) {
                if (dataSource[j].parentId != null && typeof ($("#subMenu_" + dataSource[j].parentId)[0]) != typeof (undefined)) {
                    if (dataSource[j].Type == "1") {
                        $("#subMenu_" + dataSource[j].parentId)[0].innerHTML += "<li class='MenuManager_custom'>><a class='' id='submenu_" + dataSource[j].id + "' href='#' Custom='" + dataSource[j].Class + "' customType = '" + dataSource[j].Type + "' Path='" + dataSource[j].Path + "' >" + dataSource[j].Name + "</a><ul id='sub_" + dataSource[j].id + "'></ul><button class='AddIcon' onclick='AddMenu(this)' style='background: transparent;'></button><button class='EditIcon' style='margin-left:1% !important;background: transparent;' onclick='editMenu(this)'></button></li>"
                    } else if (dataSource[j].Type == "2") {
                        $("#subMenu_" + dataSource[j].parentId)[0].innerHTML += "<li class='MenuManager_custom'><a class='' id='submenu_" + dataSource[j].id + "' href='#' Custom='" + dataSource[j].Class + "' customType = '" + dataSource[j].Type + "' Path='" + dataSource[j].Path + "'>" + dataSource[j].Name + "</a><ul id='sub_" + dataSource[j].id + "'></ul><button class='EditIcon' style='margin-left:8% !important;background: transparent;' onclick='editMenu(this)'></button></li>"
                    }
                }
            }

            for (k in dataSource) {
                if (dataSource[k].parentId != null && typeof ($("#sub_" + dataSource[k].parentId)[0]) != typeof (undefined)) {
                    if (dataSource[k].Type == "1") {
                        $("#sub_" + dataSource[k].parentId)[0].innerHTML += "<li class='MenuManager_custom'><a class='' id='submen_" + dataSource[k].id + "' href='#' Custom='" + dataSource[k].Class + "' customType = '" + dataSource[k].Type + "' Path='" + dataSource[k].Path + "'>" + dataSource[k].Name + " </a><button class='AddIcon' onclick='AddMenu(this)' style='background: transparent;'></button><button class='EditIcon' style='margin-left:1% !important;background: transparent;' onclick='editMenu(this)'></button><ul id='subOf_" + dataSource[k].id + "'></ul></li>"
                    }
                    else if (dataSource[k].Type == "2") {
                        $("#sub_" + dataSource[k].parentId)[0].innerHTML += "<li class='MenuManager_custom'><a class='' id='submen_" + dataSource[k].id + "' href='#' Custom='" + dataSource[k].Class + "' customType = '" + dataSource[k].Type + "' Path='" + dataSource[k].Path + "'>" + dataSource[k].Name + " </a><button class='EditIcon' style='margin-left:8% !important;background: transparent;' onclick='editMenu(this)'></button><ul id='subOf_" + dataSource[k].id + "'></ul></li>"

                    }
                }
            }
            //$(".file-tree").filetree();
            $('#treelist').jstree({
                "core": {
                    "check_callback": true,
                    "themes": {
                        "variant": "large",
                        "stripes": true,
                        "icons": false,
                    }
                },
                "plugins": ["wholerow", "dnd"]
            });
        }
    });
}
var ContextID;
var x = 268; // This is fixed position to match the text postion by varying the y position from "event.PageY".
var y = 95; // This is fixed left alignment position
$(document).bind("contextmenu", function (event) {
    //var winWidth = $(window).width();
    //var winHeight = $(window).height();
    ContextID = event.toElement.id.split('_')[1];
    $('.jstree-clicked').removeClass('jstree-clicked')
    $('.jstree-wholerow-clicked').removeClass('jstree-wholerow-clicked')
    if ($(event.toElement).hasClass('jstree-anchor')) {
        event.preventDefault();
        var ivalue = $(event.toElement)[0].attributes[5].nodeValue;
        if ($(event.toElement)[0].attributes[5].nodeValue == "2") {
            $("#contextMenu").html("<li onclick='editMenu(this)'><i class=\"fas fa-pencil-alt edit-pencil-alt\" ></i> Edit</li><li onclick='deleteMenu(this)'><i class=\"fa fa-trash add-plus\"></i> Delete</li>")
            $("#contextMenu").css({ "top": event.pageY - y + "px", "left": event.pageX - x + "px" }).show();
            event.preventDefault();
            $(this).css('background-color', '#333');
        } else {
            $("#contextMenu").html("<li onclick='AddMenu(this)'><i class=\"fas fa-plus add-plus\"></i> Add</li><li onclick='editMenu(this)'><i class=\"fas fa-pencil-alt edit-pencil-alt\"></i>Edit</li>")
            $("#contextMenu").css({ "top": event.pageY - y + "px", "left": event.pageX - x + "px" }).show();
            event.preventDefault();
        }
    }
});
$(document).bind('click', function () {
    $('#contextMenu').hide();
    $('.jstree-clicked').removeClass('jstree-clicked')
    $('.jstree-wholerow-clicked').removeClass('jstree-wholerow-clicked')

});
function updateMenu(source, destID) {
    var s = JSON.stringify(source);
    $.ajax({
        type: "POST",
        url: GlobalURL + '/api/ScreenGeneration/updateMenuDetails?Source=' + s + '&destID=' + destID,
        contentType: 'application/json; charset=utf-8',

        success: function (val) {
        },
        error: function (e) {
            //console.log('Menu Not updated');
        }
    });
}
var parentID;
function AddMenu(e) {
   
    $("#SubmitForm").show();
    $('#AddMenuDiv').show();
    parentID = ContextID;
    $("#AddMenuName").val(" ");
    $("#AddMenuClass").val(" ");
    // $("#AddMenuOrder").val(" ");
    $("#AddMenuPath").val(" ");
    $("#AddMenuLOB").val(" ");
    $("#AddMenuPathCustom")[0].disabled = false;
    $("#AddMenuPathGenerated")[0].disabled = false;
    $("#AddMenuPathCustom")[0].required = true;
    $("#AddMenuPathGenerated")[0].required = true;
    $('#AddMenuActive').prop('checked', false);
    $("#AddMenuPathCustom").prop("checked", false);
    $("#AddMenuPathGenerated").prop("checked", false);
    $("#AddMenuTypeMenu").prop("checked", false);
    $("#AddMenuTypeItem").prop("checked", false);

    $("#RegisterBtn").text('Add');
    $("#RegisterBtn").append('<span class="fa fa-plus pull-left pldd"></span>');
    //if ($("#AddMenuPathCustom").prop("checked")) {
    //    $("#AddMenuPathCustom").attr('checked', false)
    //}
    //if ($("#AddMenuPathGenerated").prop("checked")) {
    //    $("#AddMenuPathGenerated").attr('checked', false)
    //}
    //if ($("#AddMenuTypeMenu").prop("checked")) {
    //    $("#AddMenuTypeMenu").attr('checked', false)
    //} AddMenuTypeMenu
    //if ($("#AddMenuTypeItem").prop("checked")) {
    //    $("#AddMenuTypeItem").attr('checked', false)
    //}
}
var EditID;
function editMenu(e) {

    EditID = ContextID;
    $("#SubmitForm").show();
    $("#RegisterBtn").text('Update');
    $("#RegisterBtn").append('<span class="fas fa-check pull-left upldd"></span>');
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableDataByUserID?tablename=MENU&CreateBy=' + EditID,
        contentType: 'application/json; charset=utf-8',

        success: function (val) {
            var dataSource = $.parseJSON(val);
            $("#AddMenuName").val(dataSource[0]["Name"]);
            $("#AddMenuClass").val(dataSource[0]["Class"]);
            $("#AddMenuLOB").val(dataSource[0]["LOB"]);
            if ($("#AddMenuPathGenerated")[0].disabled = true)
                $('#AddMenuPathGenerated').prop('checked', true);
            else {
                $('#AddMenuPathCustom').prop('checked', true);
            }
            $("#AddMenuPath").val(dataSource[0]["Path"]);
            $("#AddMenuPathCustom")[0].disabled = false;
            $("#AddMenuPathGenerated")[0].disabled = false;
            if (dataSource[0]["Type"] == "1") {
                $('#AddMenuTypeMenu').prop('checked', true);
                $("#AddMenuLOB")[0].disabled = true;
            }
            else {
                $('#AddMenuTypeItem').prop('checked', true);
                $("#AddMenuLOB")[0].disabled = false;
            }

            if (dataSource[0]["Status"] == true) {
                $('#AddMenuActive').prop('checked', true);
            }
            else
                $('#AddMenuActive').prop('checked', false);

            $('#AddMenuDiv').show();
        },
        error: function (e) {

        }
    });
}
var deleteID;
function deleteMenu(e) {
    deleteID = ContextID;
    swal("Are you sure you want to delete this menu / submenu?", {
        buttons: ["Cancel", "Ok"],
        className: "deleterecord",
        closeOnClickOutside: false,
        closeOnEsc: false,
    });

    $(".deleterecord .swal-footer .swal-button-container .swal-button--confirm").on("click", function ()
    {
        $('#deleteModal').modal('hide');
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/ScreenGeneration/deleteScreen?tablename=MENU' + '&deleteid=' + deleteID,
            contentType: 'application/json; charset=utf-8',

            success: function (val) {
            
                if (val == "Success") {
                    location.reload(true);
                    swal("", "Menu / Submenu Deleted Successfully", "success", { closeOnClickOutside: false }); 
                }
                else {
                    $("#feedbackMsg").html("Sorry cannot perform such operation");
                }
            },
            error: function (e) {
                $("#feedbackMsg").html("Something Wrong.");
            }
        });
    });
 }
function contextMenu() {
    $("#contextMenu").css({ "top": event.pageY - y + "px", "left": event.pageX - x + "px" }).show();
}


