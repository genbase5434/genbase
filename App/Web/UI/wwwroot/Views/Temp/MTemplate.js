﻿var IDArr = [];

var PortalNameArr = [];

var TitleArr = [];

var DbConnection_IdArr = [];

var Chart_TypeArr = [];

var Role_IdArr = [];

var PreferencesArr = [];
var getChartType = "";

var DropDownDefaultValues = {};
var DropDownTableValues = {};
DropDownDefaultValues["selectID"] = ""
DropDownDefaultValues["selectPortalName"] = "65"
DropDownDefaultValues["selectTitle"] = "ASSETS"
DropDownDefaultValues["selectDbConnection_Id"] = "2"
DropDownDefaultValues["selectChart_Type"] = "Donut"
DropDownDefaultValues["selectSQL"] = ""
DropDownDefaultValues["selectRow_Number"] = ""
DropDownDefaultValues["selectColumn_Number"] = ""
DropDownDefaultValues["selectLink"] = ""
DropDownDefaultValues["selectHeight"] = ""
DropDownDefaultValues["selectWidth"] = ""
DropDownDefaultValues["selectRole_Id"] = "1"
DropDownDefaultValues["selectPreferences"] = "New"
DropDownDefaultValues["selectCreateBy"] = ""
DropDownDefaultValues["selectCreateDateTime"] = ""
DropDownDefaultValues["selectUpdateBy"] = ""
DropDownDefaultValues["selectUpdateDateTime"] = ""
DropDownDefaultValues["selectStatus"] = ""

function editFunction(e) {

    

    $('#editModal').modal('show');

    var tr = $(e)[0].closest("tr");
    var td = tr.children;
    var columns = $('[name="EditColumns"]');
    //console.log(td);
    for (var i = 0; i < columns.length; i++) {
        if (i == 0) {
            $('[name="EditColumns"]')[i].innerHTML = $('[name="EditColumns"]')[i].value = td[i].innerHTML;
        }
        else if ($('[name="EditColumns"]')[i].localName == "select") {
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
                $('[name="EditColumns"]')[i].value = $("#" + td[i].innerHTML.replace(" ", "")).val().replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        else {
            if (td[i].innerHTML != "" && typeof (td[i].innerHTML) != typeof (undefined))
                $('[name="EditColumns"]')[i].value = td[i].innerHTML.replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        //console.log($('[name="EditColumns"]')[i].value);
    }

}

var x = localStorage.getItem("Result");
var role = JSON.parse(x);
function cChart(dataSource) {
    var label1 = "";
    if (dataSource["Chart_Type"].toLowerCase() == "pie" || dataSource["Chart_Type"].toLowerCase() == "donut") {
        label1 = "#= category # - #= value#";
    }
    var width1, height1;
    var windowwidth = $(window).width();
    var windowheight = $(window).height();
    if (windowwidth > 1500 && windowheight > 500) {
        width1 = (dataSource["Width"] * 15).toString();
        height1 = dataSource["Height"];
    }
    else {
        width1 = (dataSource["Width"] * 8).toString();
        height1 = dataSource["Height"];
    }
    $('#' + dataSource["ID"]).kendoChart({
        dataSource: {
            data: JSON.parse(dataSource["data"]),
        },
        seriesColors: ['#3398db ', '#19bc9c ', '#fad231 ', '#9b59b6 ', '#e74c3c '],
        chartArea:
        {
            width: width1,
            height: height1
        },
        legend: {
            position: "top"
        },
        seriesDefaults: {
            type: dataSource["Chart_Type"].toLowerCase(),
            labels: {
                template: label1,
                position: "outsideEnd",
                visible: true,
                background: "transparent"
            }

        },

        series: [{
            categoryField: dataSource["Column_Number"],
            field: dataSource["Row_Number"],
            gap: 8,
            overlay: {
                gradient: "none"
            },
            border: {
                width: 0
            },

        }],
        valueAxis: {
            field: dataSource["Row_Number"],
            labels: {
                format: "{0}"
            },
            line: {
                visible: false
            },
            axisCrossingValue: 0
        },
        categoryAxis: {

            field: dataSource["Column_Number"],
            labels: {
                rotation: -30,
            },
            line: {
                visible: true
            },
            majorGridLines: {
                visible: false
            }

        },
        tooltip: {
            visible: true,

            template: "${category} - ${value}"
        }

    })
}
loadCharts();
function loadCharts() {
    var x = localStorage.getItem("Result");
    var role = JSON.parse(x);
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getCharts?roleid=' + role.Role_ID + '&monitor=' + $("#PageTitle")[0].innerHTML.trim(),
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            
            //console.log(result);

            var dataSource = $.parseJSON(result);

            //var y = dataSource[0].PortalName;
            // console.log(y);
            //$('.chart').innerHTML = dataSource[0].PortalName;

            //console.log(dataSource);

            var temp = [];
            //console.log(x);
            var j;
            $("#chart_view").html("");
            $("#loader").hide();
            for (j = 0; j < dataSource.length; j++) {
                
                var templateContent = $("#chartTemplate").html();
                var template = kendo.template(templateContent);

                //Create some dummy data

                if (j % 2 == 0 && j != 0) dataSource[j].pr = 12;
                else dataSource[j].pr = 6;
                //render the template
                var chartTemplate = template(dataSource[j]).replace("Chart_ID", dataSource[j].ID.split("#")[1]);
                // chartTemplate.replace("Chart_ID", dataSource[j].ID);

                $("#chart_view").append(chartTemplate);
                if (dataSource[j]["data"] != "false" && dataSource[j]["data"] != "Connection Failure") {
                    cChart(dataSource[j]);
                    //append the result to the page
                }
                else {
                    $("#WrongMsg" + dataSource[j]["ID"])[0].innerHTML = "Sorry! Something went wrong. Please check the KPI details and try again";
                    $("#WrongMsg" + dataSource[j]["ID"]).css("height", "300px");
                }
                //append the result to the page
            }
        },


        error: function (e) {
            //  alert("Something Wrong!!");
        }
    });
}
var row = 0;
var h = 0;
loadNKPI();
function loadNKPI() {
    var x = localStorage.getItem("Result");
    var role = JSON.parse(x);

    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getKPI?roleid=' + role.Role_ID + '&monitor=' + $("#PageTitle")[0].innerHTML.trim(),
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            
            //console.log("KPI-----" + result);
            var colorCSS = ["amber_grad", "red_grad", "green_grad", "cyan_grad", "sunset_grad", "vista_blue_grad", "bluePink_grad", "peach_orange_grad", "emerald_grad", "cornflower_blue_grad", "tickle_pink_grad", "turquoise_grad", "studio_grad", "gray_grad", "seablue_grad", "violetPink_grad", "dandelion_grad"];
            var dataSource = $.parseJSON(result);

            //var y = dataSource[0].PortalName;
            // console.log(y);
            //$('.chart').innerHTML = dataSource[0].PortalName;

            //console.log(dataSource);

            var temp = [];
            //console.log(x);
            var j;
            
            $("#loader").hide();

            for (j = 0; j < dataSource.length; j++) {
                
                row++;
                var templateContent = $("#KPIRowTemplate").html();
                var template = kendo.template(templateContent);
                var chartTemplate = template(dataSource[j])
                var data = JSON.parse(dataSource[j]["data"]);

                for (k = 0; k < data.length; k++) {
                    // var randomColor = '#' + ('000000' + Math.floor(Math.random() * 16777215).toString(16)).slice(-6);
                    data[k]["colorClass"] = colorCSS[h];
                    if (h < colorCSS.length - 1) {
                        h++;
                    }
                    else {
                        h = 0;
                    }
                    var kpiDiv = $("#KPITemplate").html();
                    var kpitemplate = kendo.template(kpiDiv);
                    //  data[k]["randomColor"] = randomColor;
                    //data[k]["colorId"] = "KPI_" + row + "-" + k + " KPIClass";
                    if (data[k]["value"].toString().length == 7) {
                        data[k]["font"] = "14px";
                    }
                    if (data[k]["value"].toString().length == 6) {
                        data[k]["font"] = "18px";
                    }
                    else if (data[k]["value"].toString().length == 5) {
                        data[k]["font"] = "24px";
                    }
                    else if (data[k]["value"].toString().length == 4) {
                        data[k]["font"] = "28px";
                    }
                    else {
                        data[k]["font"] = "36px";
                    }
                    chartTemplate += kpitemplate(data[k]);
                }


                $("#KPI").append(chartTemplate + "</div>");
                // cChart(dataSource[j]);
                //append the result to the page
            }
        },


        error: function (e) {
            // alert("Something Wrong!!");
        }
    });
}

function editDisplayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        async: false,
        success: function (val) {

            var value = $('[name="EditColumns"]')[i].value;
            var select = $('[name="EditColumns"]')[i];
            $('[name="EditColumns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            //console.log(key);
            select = $('[name="EditColumns"]')[i];


            for (var j = 0; j < dropdownlist.length; j++) {
                var opt = document.createElement('option');
                if (key.length == 2) {
                    opt.text = dropdownlist[j][key[1]];
                    opt.id = dropdownlist[j][key[1]].replace(" ", "");
                    opt.value = dropdownlist[j][key[0]];
                    DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[1]];
                }
                else {
                    opt.text = opt.value = dropdownlist[j][key[0]];
                    opt.id = dropdownlist[j][key[0]].replace(" ", "");
                    DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[0]];
                }
                select.add(opt, j);
            }
            $('[name="EditColumns"]')[i].value = value;
        },
        error: function (e) {
            //console.log("Something Wrong.");
        }
    });
}


/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */

function editsettingsFunction(e) {
    
    //$("#chart_view")[0].innerHTML += '<div class="col-sm-12"><div class="col-sm-5"></div><div class="col-sm-1"><img alt="" src="dist/img/ring_loader_monitor.gif" class="" style="position:fixed;text-align:center;z-index:1000" /></div></div>';
    for (i = 0; i < $(".MonitorIds").length; i++)
        if ($(".MonitorIds")[i].innerHTML == e.id)
            $($(".MonitorIds")[i]).parent().parent().find('.btn-warning').click()


}
function displayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {


            var select = $('[name="Columns"]')[i];
            $('[name="Columns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            //console.log(key);
            select = $('[name="Columns"]')[i];

            var opt1 = '<option selected disabled hidden style=\"display: none\" value=\"1\"></option>';
            $('[name="Columns"]')[i].append(opt1);
            var key1 = Object.keys(DropDownDefaultValues);
            for (var j = 0; j < dropdownlist.length; j++) {

                //  var key = Object.keys(DropDownTableValues[0]);
                var opt = document.createElement('option');
                if (key.length == 2) {
                    opt.text = dropdownlist[j][key[1]];
                    opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                else {
                    opt.text = opt.value = dropdownlist[j][key[0]];
                    for (var k = 0; k < key1.length; k++) {
                        if ($('[name="Columns"]')[i].className.split(" ")[2] == key1[k]) {
                            if (opt.value == DropDownDefaultValues[key1[k]]) {
                                opt.selected = true;
                            }
                        }
                    }
                }
                select.add(opt, j);
            }
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}
$(document).ready(function () {
    
    var x = localStorage.getItem("Result");
    var UserDetails = JSON.parse(x);
    $('#addModal1').on('show.bs.modal', function () {

        columns = $('[name="Columns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                displayDropDownList(i, elementId)
            }
        }
    })
    $('#addModal2').on('show.bs.modal', function () {

        columns = $('[name="Columns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                displayDropDownList(i, elementId)
            }
        }
    })
    $('#editModal').on('show.bs.modal', function () {

        columns = $('[name="EditColumns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                editDisplayDropDownList(i, elementId);
            }
        }
    })

});

gridLoad();
function gridLoad() {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableData?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML,

        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        async: false,
        success: function (val) {
            
            var dataSource = $.parseJSON(val);
            var columns = $('[name="EditColumns"]');
            for (i = 0; i < columns.length; i++) {
                if (columns[i].type == "select-one") {

                    //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                    //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                    var ID = columns[i].id;
                    var ElementDetails = ID.split("|");
                    var elementId = ElementDetails[1];
                    editDisplayDropDownList(i, elementId);

                    for (j = 0; j < dataSource.length ; j++) {
                        dataSource[j][ElementDetails[0]] = DropDownTableValues[dataSource[j][ElementDetails[0]]];
                    }
                }
            }

            $("#grid").kendoGrid({
                dataSource: {
                    data: dataSource,

                    schema: {
                        model: {
                            id: "ID",

                            fields: {

                                ID: { type: "number", editable: false },

                                PortalName: { type: "text" },

                                Title: { type: "text" },

                                DbConnection_Id: { type: "text" },

                                Chart_Type: { type: "text" },

                                SQL: { type: "text" },

                                Row_Number: { type: "text" },

                                Column_Number: { type: "text" },

                                Link: { type: "text" },

                                Height: { type: "text" },

                                Width: { type: "text" },

                                Role_Id: { type: "text" },

                                Preferences: { type: "text" },

                            }

                        }
                    },
                    sort: { field: "ID", dir: "asc" },
                },
                sortable: true,

                columns: [{ field: "ID", title: "ID", template: "<span class='col-md-12 MonitorIds' style='padding:0%;width:100%;cursor:pointer;'>#=ID#</span>", width: "120px" },

    { field: "PortalName", title: "Monitor Screen", width: "120px" },

    { field: "Title", title: " Title", width: "120px" },

    { field: "DbConnection_Id", title: "Db Instance", width: "120px" },

    { field: "Chart_Type", title: " Chart Type", width: "120px" },

    { field: "SQL", title: " SQL", hidden: true, width: "120px" },

    { field: "Row_Number", title: " Row", hidden: true, width: "120px" },

    { field: "Column_Number", title: " Column", hidden: true, width: "120px" },

    { field: "Link", title: " Link", hidden: true, width: "120px" },

    { field: "Height", title: " Height", hidden: true, width: "120px" },

    { field: "Width", title: " Width", hidden: true, width: "120px" },

    { field: "Role_Id", title: " Role ID", width: "120px" },

    { field: "Preferences", title: " Preferences", width: "120px" },


   {
       command: [
           { title: "Edit", template: "<button class='btn btn-warning btn-flat' onclick='editFunction(this)'><span class='fa fa-editcil-square-o'></span> Edit </button>" }, { title: "Delete", template: "<button class='deleteBtnClass btn btn-flat' onclick='deleteFunction(this)'><span class='fa fa-trash-o'></span> Delete </button>" }, ], title: "&nbsp;", width: "160px"
   }],
            });
        },
        error: function (e) {
            //console.log("Grid not loaded!!");
            gridLoad();
        }
    });
}
$(document).ready(function () {
    var errorTemplate = '<div class="k-widget k-tooltip k-tooltip-validation"' +
             'style="margin:0.5em"><span class="k-icon k-warning"> </span>' +
             '#=message#<div class="k-callout k-callout-n"></div></div>'

    var validator = $("#SubmitForm1").kendoValidator({
        errorTemplate: errorTemplate
    }).data("kendoValidator");

    var tooltip = $("#SubmitForm1").kendoTooltip({
        filter: ".k-invalid",
        content: function (e) {
            var id = e.target.attr("id").split("|")[0] || e.target.closest(".k-widget").find(".k-invalid:input").attr("id");
            //    var errorMessage = $("#SubmitForm").find("[data-for=" + id + "]");

            return '<span class="k-icon k-warning"> </span>' + id + " is required  ";
        },
        show: function () {
            this.refresh();
        }
    });
    var validator1 = $("#SubmitForm2").kendoValidator({
        errorTemplate: errorTemplate
    }).data("kendoValidator");

    var tooltip = $("#SubmitForm2").kendoTooltip({
        filter: ".k-invalid",
        content: function (e) {

            var id = e.target.attr("id").split("|")[0] || e.target.closest(".k-widget").find(".k-invalid:input").attr("id");
            //    var errorMessage = $("#SubmitForm").find("[data-for=" + id + "]");

            return '<span class="k-icon k-warning"> </span>' + id + " is required  ";
        },
        show: function () {
            this.refresh();
        }
    });
    var validator2 = $("#UpdateForm").kendoValidator({
        errorTemplate: errorTemplate
    }).data("kendoValidator");

    var tooltip = $("#UpdateForm").kendoTooltip({
        filter: ".k-invalid",
        content: function (e) {
            
            var id = e.target.attr("id").split("|")[0] || e.target.closest(".k-widget").find(".k-invalid:input").attr("id");
            //    var errorMessage = $("#SubmitForm").find("[data-for=" + id + "]");

            return '<span class="k-icon k-warning"> </span>' + id + " is required  ";
        },
        show: function () {
            this.refresh();
        }
    });
    $("#SubmitForm1").on("submit", function (e) {
        
        e.preventDefault();
        if (validator.validate()) {
            var Values = {
            }
            
            var Columns = $('#addModal1 [name="Columns"]');
            var len = $('#addModal1 [name="Columns"]').length;
            for (var i = 1; i < len; i++) {
                var ID = $('#addModal1 [name="Columns"]')[i].id;
                var columnName = ID.split("|");
                if (columnName[0].toUpperCase() == "CREATEBY") {
                    Values[columnName[0]] = window.btoa(JSON.parse(localStorage.Result)[0]["ID"]);
                }
                else if (columnName[0].toUpperCase() == "CREATEDATETIME") {
                    Values[columnName[0]] = window.btoa(GetNow());
                }
                else if (columnName[0].toUpperCase() == "PORTALNAME") {
                    Values[columnName[0]] = window.btoa($("#PageTitle")[0].innerHTML.trim());
                }
                else if (columnName[0].toUpperCase() == "STATUS")
                    Values[columnName[0]] = window.btoa(true);
                else {
                    if ($('#addModal1 [name="Columns"]')[i].type == "checkbox")
                        Values[columnName[0]] = window.btoa($('#addModal1 [name="Columns"]')[i].checked);
                    else
                        Values[columnName[0]] = window.btoa($('#addModal1 [name="Columns"]')[i].value);
                }
            }

            var values = JSON.stringify(Values);
            $.ajax({
                type: "POST",
                url: GlobalURL + '/api/CrudService/createTableRow',
                data: JSON.stringify({
                    "tablename": 'KPI',
                    "Values": values
                }),

                //data: Htmlname ,
                contentType: 'application/json; charset=utf-8',
                //dataType: 'json', 
                success: function (val) {
                    
                    if (val.createTableRowResult == "true") {
                    }
                    //location.reload();
                    $("#addModal1 .close").click();
                    gridLoad();
                    loadCharts();
                },
                error: function (e) {

                }
            });
        }
    });
    $("#SubmitForm2").on("submit", function (e) {
        
        e.preventDefault();
        if (validator1.validate()) {
            var Values = {
            }
            var Columns = $('#addModal2 [name="Columns"]');
            var len = $('#addModal2 [name="Columns"]').length;
            for (var i = 1; i < len; i++) {
                var ID = $('#addModal2 [name="Columns"]')[i].id;
                var columnName = ID.split("|");
                if (columnName[0].toUpperCase() == "CREATEBY") {
                    Values[columnName[0]] = window.btoa(JSON.parse(localStorage.Result)[0]["ID"]);
                }
                else if (columnName[0].toUpperCase() == "CREATEDATETIME") {
                    Values[columnName[0]] = window.btoa(GetNow());
                }
                else if (columnName[0].toUpperCase() == "PORTALNAME") {
                    Values[columnName[0]] = window.btoa($("#PageTitle")[0].innerHTML.trim());
                }
                else if (columnName[0].toUpperCase() == "STATUS")
                    Values[columnName[0]] = window.btoa(true);
                else {
                    if ($('#addModal2 [name="Columns"]')[i].type == "checkbox")
                        Values[columnName[0]] = window.btoa($('#addModal2 [name="Columns"]')[i].checked);
                    else
                        Values[columnName[0]] = window.btoa($('#addModal2 [name="Columns"]')[i].value);
                }
            }

            var values = JSON.stringify(Values);
            $.ajax({
                type: "POST",
                url: GlobalURL + '/api/CrudService/createTableRow',
                data: JSON.stringify({
                    "tablename": 'NKPI',
                    "Values": values
                }),

                //data: Htmlname ,
                contentType: 'application/json; charset=utf-8',
                //dataType: 'json', 
                success: function (val) {
                    
                    if (val.createTableRowResult == "true") {
                    }
                    //location.reload();
                    $("#addModal2 .close").click();
                    gridLoad();
                    loadNKPI();
                },
                error: function (e) {

                }
            });
        }
    });
    $("#UpdateForm").on("submit", function (e) {
        
        e.preventDefault();
        if (validator2.validate()) {
            var IDName = $('[name="EditColumns"]')[0].id.split("|");
            var ID = {};
            ID[IDName[0]] = $($('[name="EditColumns"]')[0].value)[0].innerHTML;
            columns = $('[name="EditColumns"]');
            var Values = {
                "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
                //  "UpdateDatetime": window.btoa(GetNow())
            }
            for (var i = 1; i < columns.length; i++) {
                var z = $('[name="EditColumns"]')[i].id.split("|");
                if ($('[name="EditColumns"]')[i].type == "checkbox") {
                    Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].checked);
                }
                else {

                    Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].value);
                }
            }
            var values = JSON.stringify(Values);
            var Id = JSON.stringify(ID);
            $.ajax({
                type: "POST",
                url: GlobalURL + '/api/CrudService/updateTableRow',
                data: JSON.stringify({
                    'tablename': document.getElementsByTagName("tablename")[0].innerHTML,
                    'Values': values,
                    'ID': Id
                }),
                async: false,
                //data: Htmlname ,
                contentType: 'application/json; charset=utf-8',
                //dataType: 'json',
                success: function (val) {
                    //location.reload();
                    $("#editModal").modal('hide');
                    gridLoad();
                    $("#loader").show();
                    loadCharts();
                },
                error: function (e) {
                    gridLoad();
                    $("#loader").show();
                    loadCharts();
                }
            });
        }
    });
});


function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}


