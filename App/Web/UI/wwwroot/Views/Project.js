
function displayDropDownList(i,elementId)
{
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            
           
            var select = $('[name="Columns"]')[i];
            $('[name="Columns"]')[i].length=0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            //console.log(key);
            select = $('[name="Columns"]')[i];
            var opt1 = '<option selected disabled hidden style=\"display: none\" value=\"\"></option>';
            $('[name="Columns"]')[i].append(opt1);
            for (var j = 0; j < dropdownlist.length; j++) {
                var opt = document.createElement('option');
                if(key.length==2){
                    opt.text = dropdownlist[j][key[1]].trim();
                    opt.value = dropdownlist[j][key[0]];
                }
                else{
                    opt.text = opt.value = dropdownlist[j][key[0]].trim();
                }
                select.add(opt, 0);
            }
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}
function editDisplayDropDownList(i,elementId)
{
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/genericScreenDropDown?elementId=' + elementId,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            
           
            var select = $('[name="EditColumns"]')[i];
            $('[name="EditColumns"]')[i].length=0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            //console.log(key);
            select = $('[name="EditColumns"]')[i];
            //var opt1 = '<option selected disabled hidden style=\"display: none\" value=\"0\"></option>';
            //$('[name="EditColumns"]')[i].append(opt1);
            var opt = document.createElement('option');
            opt.text = "";
            opt.value=0;
            select.add(opt,0);
            for (var j = 0; j < dropdownlist.length; j++) {
                var opt = document.createElement('option');
                if(key.length==2){
                    opt.text = dropdownlist[j][key[1]].trim();
                    opt.value = dropdownlist[j][key[0]];
                }
                else{
                    opt.text = opt.value = dropdownlist[j][key[0]].trim();
                }
                select.add(opt, 0);
            }
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}


$(document).ready(function () {
  
    $('#addModal').on('show.bs.modal', function () {
      
        columns = $('[name="Columns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                displayDropDownList(i, elementId)
            }
        }
    })
    $('#editModal').on('show.bs.modal', function () {
      
        columns = $('[name="EditColumns"]');
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].type == "select-one") {
                //$('[name="Columns"]')[i].addEventListener('click', onClickHandler);
                //$('[name="Columns"]')[i].addEventListener('mousedown', onMouseDownHandler);
                var ID = columns[i].id;
                var ElementDetails = ID.split("|");
                var elementId = ElementDetails[1];
                editDisplayDropDownList(i,elementId);
            }
        }
    })
    $.ajax({
        type: "GET",
        url: '/services/CrudService.svc/getTableData?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML,

        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            var dataSource = $.parseJSON(val);

            $("#grid").kendoGrid({
                dataSource: {
                    data: dataSource,
                    pageSize: 10,
                    sort: { field: "ID", dir: "asc" },
                },
                sortable: true,
            

                pageable: true,
            
              
                columns:  [{field:"ID", title:" ID",width:"120px"},

{field:"NAME", title:" NAME",width:"120px"},

{field:"DESCRIPTION", title:" DESCRIPTION",width:"120px"},

{field:"PATH", title:" PATH",width:"120px"},

     
                { command:[{text:"Edit",click:editFunction},{text:"Delete",click:deleteFunction}], title: "&nbsp;", width: "250px" }],
      
            //edit: function (e) {
            //    var updateBtn = e.container.find(".k-grid-update");
            //    updateBtn.removeClass("k-grid-update");
                
            //    //removing this class will prevent the grid from saving on click
            //    updateBtn.click(function () {

            //        updateFunction(); //call the function you wish
            //        updateBtn.addClass("k-grid-update");
            //    })
                   

     
    });


},
error: function (e) {
    //alert("Something Wrong!!");
}
});



});
function editFunction(e) {
    e.preventDefault();
    
    var dataItem = $(this).closest('tr').children('td');
    //var r = confirm("Are  you want to edit this reco rd");
    var tr = $(e.target).closest("tr");
    var td = tr.find("td");
    var columns = $('[name="EditColumns"]');
    //console.log(td);
    for(var i = 0; i <columns.length; i++) {
        if(i==0)
            $('[name="EditColumns"]')[i].innerHTML =$('[name="EditColumns"]')[i].value= $(td)[i].innerHTML;
        else
            $('[name="EditColumns"]')[i].value = $(td)[i].innerHTML;
        //console.log($('[name="EditColumns"]')[i].value);
    }
    $('#editModal').modal('show');
}
function deleteFunction(e) {
    e.preventDefault();
    var dataItem = $(this).closest('tr').children('td:first').innerHTML;
    var r=confirm("Are you sure you want to delete this record");
    if(r){
        var tr = $(e.target).closest("tr");
        var uid = $(tr).children('td:first-child')[0].innerHTML;
    
        var x = $('th')[0];
        var columnName=$(x).attr('data-field');
        var data={    
        }
        data[$(x).attr('data-field')]=uid;
   
        //console.log(data);
        var IdData = JSON.stringify(data);
        $.ajax({
            type: "POST",
            url: '/services/CrudService.svc/DeleteTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML+'&ID=' + IdData,
            //data: Htmlname ,
            contentType: 'application/json; charset=utf-8',
            //dataType: 'json',

            success: function (val) {
                //alert(val);

                if (val == "true") {
                    location.reload();
                    // $("#feedbackMsg").html("Success");
                }
                else {
                    $("#feedbackMsg").html("Sorry cannot perform such operation");
                }
            },
            error: function (e) {
                $("#feedbackMsg").html("Something Wrong.");
            }
        });
    }
}

function updateFunction() {

    var IDName=$('[name="EditColumns"]')[0].id.split("|");
    var ID = {};
    ID[IDName[0]] = $('[name="EditColumns"]')[0].value;
    columns = $('[name="EditColumns"]');
    var Values = {}
    for (var i = 1; i < columns.length; i++) {
        var z = $('[name="EditColumns"]')[i].id.split("|");
        if($('[name="EditColumns"]')[i].type=="checkbox"){
            Values[z[0]]=$('[name="EditColumns"]')[i].checked;
        }
        else
        {
           
            Values[z[0]]=$('[name="EditColumns"]')[i].value;
        }
    }
    var values = JSON.stringify(Values);
    var Id = JSON.stringify(ID);
    $.ajax({
        type: "POST",
        url: '/services/CrudService.svc/updateTableRow?tablename=' + document.getElementsByTagName("tablename")[0].innerHTML + '&Values=' + values + '&ID=' + Id,

        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json', 
        success: function (val) {
            location.reload();
        },
        error: function (e) {

        }
    });
}
function addFunction() {
    var Values = {
    }
    var Columns=$('[name="Columns"]');
    var len=$('[name="Columns"]').length;
    for(var i=1;i<len;i++)
    {
        var ID = $('[name="Columns"]')[i].id;
        var columnName = ID.split("|");
        if(i==len-1)
            Values[columnName[0]]=true;
        else
        {
            if($('[name="Columns"]')[i].type=="checkbox")
                Values[columnName[0]] = $('[name="Columns"]')[i].checked;
            else
                Values[columnName[0]] = $('[name="Columns"]')[i].value;
        }
    }
   

    
    
    var values = JSON.stringify(Values);
    $.ajax({
        type: "POST",
        url: '/services/CrudService.svc/createTableRow?tablename='+document.getElementsByTagName("tablename")[0].innerHTML+'&Values='+values,

        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json', 
        success: function (val) {
            if(val=="true")
                location.reload();
        },
        error: function (e) {

        }
    });
}






