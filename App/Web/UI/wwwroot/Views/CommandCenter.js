﻿

(function ($) {
    $.fn.filetree = function (method) {

        var settings = { // settings to expose
            animationSpeed: 'fast',
            collapsed: true,
            console: false
        }
        var methods = {
            init: function (options) {
                // Get standard settings and merge with passed in values
                var options = $.extend(settings, options);
                // Do this for every file tree found in the document
                return this.each(function () {

                    var $fileList = $(this);

                    $fileList
                        .addClass('file-list')
                        .find('li')
                        .has('ul') // Any li that has a list inside is a folder root
                            .addClass('folder-root closed')
                            .on('click', 'a[href="#"]', function (e) { // Add a click override for the folder root links
                                e.preventDefault();
                                $(this).parent().toggleClass('closed').toggleClass('open');


                                return false;
                            });

                    //alert(options.animationSpeed); Are the settings coming in

                });


            }
        }
        if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.on("error", function () {
                //console.log(method + " does not exist in the file exploerer plugin");
            });
        }
    }

}(jQuery));




var Cdata = [];
$(document).bind("contextmenu", function (event) {
    $("#contextMenu").css("display", "none");
    if ($(event.toElement).hasClass('small-cardbox') || $($(event.toElement).parent()).hasClass('small-cardbox') || $($(event.toElement).parent().parent()).hasClass('small-cardbox')) {
        event.preventDefault();
        $("#contextMenu").html("<li>Start</li><li>Stop</li><li>Pause</li><li>Resume</li>")
        $("#contextMenu").css({ "top": event.pageY + "px", "left": event.pageX + "px" }).show();

    }
    if ($(event.toElement).hasClass('small-cardbox Systems') || $($(event.toElement).parent()).hasClass('small-cardbox Systems') || $($(event.toElement).parent().parent()).hasClass('small-cardbox Systems')) {
        event.preventDefault();
        $("#contextMenu").html("<li>Add</li><li>Active</li><li>Inactive</li>")
        $("#contextMenu").css({ "top": event.pageY + "px", "left": event.pageX + "px" }).show();

    }
    if (event.toElement.id == "inboxItems" || $(event.toElement).parent()[0].id == "inboxItems" || $(event.toElement).parent().parent()[0].id == "inboxItems") {
        event.preventDefault();
        $("#contextMenu").html("<li>View</li><li>Accept</li><li>Reject</li><li>Assign</li><li>Comment</li><li>Reply</li>")
        $("#contextMenu").css({ "top": event.pageY + "px", "left": event.pageX + "px" }).show();

    }
    if ($(event.toElement).parents('#RobotsTimeline').length || event.toElement.id == "RobotsTimeline") {
        event.preventDefault();
        $("#contextMenu").html("<li>Change Chart</li><li>Change System</li><li>Change Date</li>")
        $("#contextMenu").css({ "top": event.pageY + "px", "left": event.pageX + "px" }).show();
        $("#contextMenu li").css({ "width": "150px" }, "important");
    }
    if ($(event.toElement).parents('#SystemsTimeline').length || event.toElement.id == "SystemsTimeline") {
        event.preventDefault();
        $("#contextMenu").html("<li>Change Chart</li><li>Change Robot</li><li>Change Date</li>")
        $("#contextMenu").css({ "top": event.pageY + "px", "left": event.pageX + "px" }).show();
        $("#contextMenu li").css({ "width": "150px" }, "important");
    }
    if ($(event.toElement).parents('#chart').length || event.toElement.id == "chart") {
        event.preventDefault();
        $("#contextMenu").html("<li>Connect</li><li>Disconnect</li><li>Add Connection</li><li>Test Connection</li><li>Remove Connection</li><li>Edit Connection</li>")
        $("#contextMenu").css({ "top": event.pageY + "px", "left": event.pageX + "px" }).show();
        $("#contextMenu li").css({ "width": "150px" }, "important");
    }
    if ($(event.toElement).parents('#RobotList').length || event.toElement.id == "RobotList") {
        event.preventDefault();
        $("#contextMenu").html("<li>View</li><li>Start</li><li>Stop</li><li>Scheduler</li>")
        $("#contextMenu").css({ "top": event.pageY + "px", "left": event.pageX + "px" }).show();

    }
});
$(document).bind('click', function () {
    $('#contextMenu').hide();
});


var x = localStorage.getItem("Result");
var UserDetails = JSON.parse(x);
var row = 0;
function NumberAnimation() {
    $('.count').each(function () {
    
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 1000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
}
function loadNKPI() {
    $.ajax({
        type: "GET",
		url: GlobalURL + '/api/Dashboard/getKPI?roleid=' + UserDetails.Role_ID + '&monitor=Controller',
		async: true,
        contentType: 'application/json; charset=utf-8',
        header: {
            "Access-Control-Allow-Headers": "*",
            "Access-Control-Allow-Origin": "*",
        },
        
        //async: true,
        success: function (result) {
            //console.log("KPI-----" + result);
            var dataSource = $.parseJSON(result);
            h = 0;

            //console.log(dataSource);

            var temp = [];
            //console.log(x);
            var j;
            $("#KPI").html("");
            for (j = 0; j < dataSource.length; j++) {

                row++;
                var data = JSON.parse(dataSource[j]["data"]);
                if (dataSource[j]["Title"] == "Systems") {
                    for (k = 0; k < data.length; k++) {
                        if (data[k]["category"] == "Not Active") {
                            $("#InactiveKPISys").html(data[k]["value"]);
                        }
                        else if (data[k]["category"] == "Active") {
                            $("#ActiveKPISys").html(data[k]["value"]);
                        }
                        else if (data[k]["category"] == "Warning") {
                            $("#ErrorsKPISys").html(data[k]["value"]);
                        }
                        else {
                            $("#AvailableKPISys").html(data[k]["value"]);
                        }
                    }
                }
                else if (dataSource[j]["Title"] == "Robots") {
                    for (k = 0; k < data.length; k++) {
                        if ((data[k]["category"]).toLowerCase() == "failure") {
                            $("#ErrorsKPI").html(data[k]["value"]);
                        } else if (data[k]["category"] == "Success") {
                            $("#ActiveKPI").html(data[k]["value"]);
                        }
                        else if (data[k]["category"] == "New") {
                            $("#NewKPI").html(data[k]["value"]);
                        }
                        else if (data[k]["category"] == "In Progress") {
                            $("#RunningKPI").html(data[k]["value"]);
                        }
                    }
                }
            }
            NumberAnimation();
        },
        error: function (e) {
            // alert("Something Wrong!!");
            //console.log("NKPI not loaded");
            $("#loader").show();
            loadNKPI();
        }
    });
}

function LoadInbox() {
    

    $("#dvLoadingInbox").hide();

    var t = $('#InboxTable').DataTable({
        "destroy": true,
        "scrollY": true,
        "scrollX": true,
        "processing": true,
        "serverSide": false,
        "paging": true,
        "pagingType": "input",
        "pageLength": 6,
        "word-break": "break-all",
        "lengthChange": false,
        "language": { search: '', searchPlaceholder: "Search..." },
        
        "ajax":
        {
            "url": GlobalURL + "/api/CrudService/GetDataByUser?tablename=CommandCenter_Inbox&CreateBy=" + UserDetails.User_ID,
            //"url": "/services/CrudService.svc/getGridDataByUser?tablename=Inbox&CreateBy=" + UserDetails.User_ID,
            "contentType": "application/json",
            "type": "Get",
            "dataType": "JSON",
            "data": function (d) {
               
                //console.log(d);
                return d;
            },
            "dataSrc": function (json) {
                
                //console.log(json);
                json.draw = json.draw;
                json.recordsTotal = json.recordsTotal;
                json.recordsFiltered = json.recordsFiltered;
                json.data = json.data;

                var return_data = json;
                //return return_data.data;
                var dttt = $.parseJSON(return_data.data);
                //dttt = $.parseJSON(dttt);
                //console.log(dttt);                
                for (var i = 0; i < dttt.length; i++) {
                    dttt[i].CreateDatetime = new Date(dttt[i].CreateDatetime);
                }
                return dttt;
            }
        },
        "columns": [
            {
                "data": "ID",
                "title": "Id"
            },
            {
                "data": "Name",
                "title": "Robot"
            },
            {
                "data": "StepName",
                "title": "Step"
            },
            {
                "data": "StepId",
                "title": "StepId"
            },

            {
                "data": "MsgStatus",
                "title": "Status"
            },

            {
                "data": "JobId",
                "title": "JobId"
            },
            {
                "data": "CurrentOutput",
                "title": "CurrentOutput"
            },
            {
                "data": "ActionType",
                "title": "ActionType"
            },
            {
                "data": "CreateDatetime",
                "title": "CreateDateTime"
            },
            {
                "title": "Actions",
                "defaultContent": "<button class=\"btn btn-warning btn-flat\" onclick=\"InboxAcceptClick(this)\"><i class=\"fas fa-check\" aria-hidden=\"true\" style=\"font-size:16px\"></i><span class='tooltiptext deleteedit'>Accept</span></button><button class=\"deleteBtnClass btn btn-flat\" onclick=\"InboxRejectClick(this)\"><i class=\"fas fa-times\" aria-hidden=\"true\" style=\"font-size:16px\"></i><span class='tooltiptext deleteedit'>Reject</span></button>"
                //"defaultContent": "<button class='bton success' onclick='InboxAcceptClick(this)'><i class='fas fa-check rb-act' style='font-size:12px' ><span class='tooltiptext'>Accept</span></i></button><button class='bton danger' onclick='InboxRejectClick(this)'><i class='fas fa-times rb-act' style='font-size:12px' ><span class='tooltiptext'>Reject</span></i></button>"
            },

        ],
        "columnDefs": [{
            "targets": '_all',
            "defaultContent": "",
        },
        {
            "aTargets": [0, 3, 4, 5, 6, 7],
            "sClass": "hidden"
        },
        {
            "targets": [0],
            "orderable": true
        },],

    });
    t.page('first').draw('page');
    $(".dataTables_filter").addClass("pull-left");



}
//var inboxrefreshId = setInterval("LoadInbox();", 1 / 2 * 60000);
var inboxrefreshId = setInterval("LoadInbox();", 180000);
function LoadNotification() {

    $("#dvLoadingNotification").hide();

    var t = $('#NotificationsTable').DataTable({
        "destroy": true,
        "scrollY": 160,
        "processing": true,
        "serverSide": true,
        "paging": true,
        "pagingType": "input",
        "pageLength": 8,
        "lengthChange": false,
        "word-break": "break-all",
        "language": { search: '', searchPlaceholder: "Search..." },


        "ajax":
          {
            "url": GlobalURL +"/api/CrudService/getGridData?tablename=Notification",
              "contentType": "application/json",
              "type": "POST",
              "dataType": "JSON",
            "data": function (d) {
                
                return JSON.stringify(d);
              },
              "dataSrc": function (json) {
                  

                  //json.draw = json.draw;
                  //json.recordsTotal = json.recordsTotal;
                  //json.recordsFiltered = json.recordsFiltered;
                  //json.data = json.data;

                  var return_data = json;
                  //return return_data.data;
                  var dttt = $.parseJSON(return_data.data);
                  //dttt = $.parseJSON(dttt);
                  return dttt;
              }
          },

        "columns": [
                {
                    "data": "ID",
                    "title": "Id"
                },
                    {
                        "data": "Message",
                        "title": "Message",
                         "width":"55%"
                    },
                    {
                        "data": "CreateBy",
                        "title": "User"
                         
                    },
                    {
                        "data": "CreateDatetime",
                        "title": "Time"
                    },


        ],

        "columnDefs": [{
            "targets": '_all',
            "defaultContent": "",
        },
                 {
                     "aTargets": [0],
                     "sClass": "hidden"
                 },
                 {
                     "targets": [0],
                     "orderable": true
                 }, ],

    });
    t.page('first').draw('page');
    $(".dataTables_filter").addClass("pull-left");

    //$("#NotificationsTable_first").css("display", "none");
    //$("#NotificationsTable_last").css("display", "none");
    //$(".paginate_input").css("width", "40px");

    //$.ajax({
    //    type: "GET",
    //    url: '/services/CrudService.svc/getTableData?tablename=Notification',
    //    async: true,
    //    contentType: 'application/json; charset=utf-8',
    //    success: function (result) {
    //        $("#dvLoadingNotification").hide();
    //        var dataSource = $.parseJSON(result);
    //        //$("#NotificationBody")[0].innerHTML += "<tr><td>" + dataSource[i]["ID"] + "</td><td>" + dataSource[i]["Message"] + "</td><td>" + dataSource[i]["UserName"] + "</td></tr>"

    //        var t = $('#NotificationsTable').DataTable({
    //            destroy: true,
    //            "scrollX": false,
    //            "scrollY": 174,
    //            "oLanguage": { "sSearch": "" },
    //            "columnDefs": [{
    //                "targets": '_all',
    //                "defaultContent": "",
    //            },
    //             {
    //                 "aTargets": [0],
    //                 "sClass": "hidden"
    //             }, ]
    //        });
    //        t.clear();
    //        for (var i in dataSource) {
    //            t.row.add([
    //         dataSource[i]["ID"],
    //         dataSource[i]["Message"],
    //         dataSource[i]["Type"],
    //         new Date(dataSource[i]["CreateDateTime"]).toLocaleString('en-US', {
    //             year: "numeric", month: "numeric",
    //             day: "numeric", hour: "2-digit", minute: "2-digit", hour12: true
    //         })
    //            ]);
    //            t.draw(false);
    //        }
    //        t.page('first').draw('page');

    //        $($('#NotificationsTable_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
    //    },
    //    error: function (e) {
    //        LoadNotification();
    //    }
    //});
}
function footertoggleclick() {
    if ($("#logfooter").height() == 250 || $("#logfooter").height() == 250.00001525878906) {
        $('#footertoggle .tooltiptext').html('');
        $('#footertoggle .tooltiptext').html('Expand');
        $("#logfooter").animate(
            { height: "30px" });
    }
    else if ($("#logfooter").height() == 30 || $("#logfooter").height() == 30.000003814697266) {
        $('#footertoggle .tooltiptext').html('');
        $('#footertoggle .tooltiptext').html('Collapse');
        $("#logfooter").animate({ height: "250px" });
    }

    $("#span4").toggleClass("fa fa-window-maximize");
    $("#span4").toggleClass("fa fa-window-minimize");
}
function openS(event, type) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent2");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks2");
    for (i = 0; i < tablinks.length; i++) {
        $(tablinks[i]).removeClass("active")
    }
    document.getElementById(type).style.display = "block";
    $(event.target).addClass("active");

}
function openSystemsrRobots(event, Type) {
    
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        $(tablinks[i]).removeClass("active")
    }
    document.getElementById(Type).style.display = "block";
    $(event.currentTarget).addClass("active");
    $("#TimeDropdown").val("Live");
    if (Type == "RobotsTimeline") {
        RobotsTimeLine();
    }
    else if (Type == "SystemsTimeline") {
        SystemsTimeLine();
    }

}
function openSystemsrRobot(event, Type) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent5");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks5");
    for (i = 0; i < tablinks.length; i++) {
        $(tablinks[i]).removeClass("active")
    }
    document.getElementById(Type).style.display = "block";
    $(event.currentTarget).addClass("active");
}
function drawDensityGraph() {
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'x');
    data.addColumn('number', 'Systems');
    data.addColumn('number', 'Robots');

    data.addRows([
      [0, 0, 0], [1, 10, 5], [2, 23, 15], [3, 17, 9], [4, 18, 10], [5, 9, 5],
      [6, 11, 3], [7, 27, 19], [8, 33, 25], [9, 40, 32], [10, 32, 24], [11, 35, 27],
      [12, 30, 22], [13, 40, 32], [14, 42, 34], [15, 47, 39], [16, 44, 36], [17, 48, 40],
      [18, 52, 44], [19, 54, 46], [20, 42, 34], [21, 55, 47], [22, 56, 48], [23, 57, 49],
      [24, 60, 52], [25, 50, 42], [26, 52, 44], [27, 51, 43], [28, 49, 41], [29, 53, 45],
      [30, 55, 47], [31, 60, 52], [32, 61, 53], [33, 59, 51], [34, 62, 54], [35, 65, 57],
      [36, 62, 54], [37, 58, 50], [38, 55, 47], [39, 61, 53], [40, 64, 56], [41, 65, 57],
      [42, 63, 55], [43, 66, 58], [44, 67, 59], [45, 69, 61], [46, 69, 61], [47, 70, 62],
      [48, 72, 64], [49, 68, 60], [50, 66, 58], [51, 65, 57], [52, 67, 59], [53, 70, 62],
      [54, 71, 63], [55, 72, 64], [56, 73, 65], [57, 75, 67], [58, 70, 62], [59, 68, 60],
      [60, 64, 56], [61, 60, 52], [62, 65, 57], [63, 67, 59], [64, 68, 60], [65, 69, 61],
      [66, 70, 62], [67, 72, 64], [68, 75, 67], [69, 80, 72]
    ]);

    var options = {
        hAxis: {
            title: 'Systems'
        },
        vAxis: {
            title: 'Robots'
        },
        colors: ['#AB0D06', '#007329'],
        width: 700, height: 200,
    };

    var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}
function NkpiClick(e) {
  
    $(e.id).animate({
        left: '250px',
        opacity: '0.5',
        transition: 'all 1s',
    });
    $("#Robotvalues").html("("+e.id+")");
    $("#dvLoadingRobotDetails").show();
    var clickedTabID;
    if (e.id == "Active") {
        clickedTabID = "Success";
    }
    else if (e.id == "Running") {
        clickedTabID = "In Progress";
    }
    else if (e.id == "Errors") {
        clickedTabID = "failure";
    }
    else if (e.id == "New") {
        clickedTabID = "New";
    }
    if (e.children[1].children[1].innerText.trim() != "0" || (e.children[1].children[1].innerText.trim() != "" || typeof (e.children[1].children[1].innerText.trim()) != typeof (undefined))) {

        $("#SysDetails").hide();
        $("#RobotDetails").show();
        $("#dvLoadingRobotDetails").hide();
        var t = $('#RobotDetailsTable').DataTable({
            "destroy": true,
            "scrollY": true,
            "scrollX": true,
            "processing": true,
            "serverSide": true,
            "paging": true,
            "pagingType": "input",
            "pageLength": 6,
            "word-break": "break-all",
            "lengthChange": false,
            "language": { search: '', searchPlaceholder: "Search..." },

            "ajax":
              {
                "url": GlobalURL +"/api/CrudService/getConfigurationGridData?tablename=CommandCenterNKPI&type=" + clickedTabID,
                  "contentType": "application/json",
                  "type": "POST",
                "dataType": "JSON",
                async: false,
                "data": function (d) {
                    return JSON.stringify(d);
                  },
                  "dataSrc": function (json) {
                      ////
                      
                      //json.draw = json.draw;
                      //json.recordsTotal = json.recordsTotal;
                      //json.recordsFiltered = json.recordsFiltered;
                      //json.data = json.data;

                      var return_data = json;
                      //return return_data.data;
                      var dttt = $.parseJSON(return_data.data);
                      //dttt = $.parseJSON(dttt);
                      return dttt;
                  }
              },
            "columns": [
                    {
                        "data": "Id",
                        "title": "Id"
                    },
                        {
                            "data": "Name",
                            "title": "Robot"
                        },
                         {
                             "data": "SystemName",
                             "title": "System"
                         },
                          {
                              "data": "CreatedBy",
                              "title": "User"
                          },
                        {
                            "title": "Actions",
                            //"defaultContent": "<button class='bton color1' title='Start' onclick='executeRobot1(this)'><i class='fas fa-play-circle' style='font-size:16px'></i><span class='tooltiptext'>Start</span></button>&nbsp;<button class='bton color2' title='Stop' onclick='StopSchedule(this)'><i class='fas fa-stop-circle' style='font-size:16px'></i><span class='tooltiptext'>Stop</span></button>&nbsp;<button class='bton color3' title='Scheduler' onclick=\"rsbotid=$(this).parent().closest('tr').find('td')[0].innerHTML;scheduleModal()\"><i class='fas fa-calendar-alt' style='font-size:16px'></i><span class='tooltiptext'></span></button>&nbsp;<button class='bton color4' title='History' onclick='historyModalOpen(this)'><i class='fas fa-history' style='font-size:16px'></i></button>&nbsp;<button class='bton color5' title='Log' style='display:none' onclick='OpenLog(this)'><i class='fa fa-tasks' aria-hidden='true' style='font-size:16px'></i></button>"
                            "defaultContent": "<button class='bton color1' onclick='executeRobot1(this)'><i class='fas fa-play-circle rb-act' style='font-size:16px'><span class='tooltiptext'>Start</span></i></button>&nbsp;<button class='bton color2' onclick='StopSchedule(this)'><i class='fas fa-stop-circle rb-act' style='font-size:16px'><span class='tooltiptext'>Stop</span></i></button>&nbsp;<button class='bton color3' onclick=\"rsbotid=$(this).parent().closest('tr').find('td')[0].innerHTML;scheduleModal()\"><i class='fas fa-calendar-alt rb-act' style='font-size:16px'><span class='tooltiptext'>Scheduler</span></i></button>&nbsp;<button class='bton color4' onclick='historyModalOpen(this)'><i class='fas fa-history rb-act' style='font-size:16px'><span class='tooltiptext'>History</span></i></button>&nbsp;<button class='bton color5' style='display:none' onclick='OpenLog(this)'><i class='fa fa-tasks rb-act' aria-hidden='true' style='font-size:16px'><span class='tooltiptext'>Log</span></i></button>"
                },

            ],
            "columnDefs": [{
                "targets": '_all',
                "defaultContent": "",
            },
                     {
                         "aTargets": [0],
                         "sClass": "hidden"
                     },
                     {
                         "targets": [0],
                         "orderable": true
                     },
                     {
                         "targets": [1],
                         render: function (data, type, row) {
                             ////
                             return "<div onclick='popupOpen(this)' style='cursor:pointer'> " + row.Name + "</div>"
                         }
                     },
                     {
                         "targets": [2],
                         render: function (data, type, row) {
                             ////
                             if (data == 'admin' || data == '' || data == null) {
                                 return 'Adhoc'
                             }
                         }
                     },
            ],

        });

        $("#divLoader").hide();
        t.page('first').draw('page');
        $(".dataTables_filter").addClass("pull-left");



        //$.ajax({
        //    type: "GET",
        //    url: '/services/CrudService.svc/CommandCenterNKPI?clickedNKPI=' + clickedTabID,
        //    async: true,
        //    contentType: 'application/json; charset=utf-8',
        //    success: function (result) {
        //        $("#dvLoadingRobotDetails").hide();
        //        var dataSource = $.parseJSON(result);
        //        var t = $('#RobotDetailsTable').DataTable({
        //            "oLanguage": { "sSearch": "" },
        //            "scrollX": false,
        //            "scrollY": 170,
        //            destroy: true,
        //            "columnDefs": [{
        //                "targets": '_all',
        //                "defaultContent": "",
        //            },
        //             {
        //                 "aTargets": [0],
        //                 "sClass": "hidden"
        //             }, ]
        //        });
        //        t.clear();
        //        for (var i in dataSource) {
        //            if (dataSource[i]["SystemName"] == 'admin' || dataSource[i]["SystemName"] == '' || dataSource[i]["SystemName"] == null)
        //                dataSource[i]["SystemName"] = 'Adhoc';
        //            t.row.add([
        //       dataSource[i]["Id"],
        //      "<div onclick='popupOpen(this)' style='cursor:pointer'> " + dataSource[i]["Name"] + "</div>",
        //    dataSource[i]["SystemName"],
        //       dataSource[i]["CreatedBy"],

        //       "<button class='bton color1' title='Start' onclick='executeRobot1(this)'><i class='fas fa-play-circle' style='font-size:16px'></i></button>&nbsp;<button class='bton color2' title='Stop' onclick='StopSchedule(this)'><i class='fas fa-stop-circle' style='font-size:16px'></i></button>&nbsp;<button class='bton color3' title='Schedule' onclick=\"rsbotid=$(this).parent().closest('tr').find('td')[0].innerHTML;scheduleModal()\"><i class='fas fa-calendar-alt' style='font-size:16px'></i></button>&nbsp;<button class='bton color4' title='History' onclick='historyModalOpen(this)'><i class='fas fa-history' style='font-size:16px'></i></button>&nbsp;<button class='bton color5' title='Log' onclick='OpenLog(this)'><i class='fa fa-tasks' aria-hidden='true' style='font-size:16px'></i></button>",
        //            ]).node().id = 'RobotList';
        //            t.draw(false);
        //        }
        //        t.page('first').draw('page');
        //        $($('#RobotDetailsTable_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
        //    },
        //    error: function (e) {
        //        //  alert("Something Wrong!!");
        //    }
        //});


    }
}
function NkpiSystemClick(e) {
  
    $(e.id).animate({
        left: '250px',
        opacity: '0.5',
        transition: 'all 1s',
    });
    
    $("#dvLoadingSystemDetails").show();
   
    var clickedTabID;
    if (e.id == "SystemActive") {
        clickedTabID = "Active";
    }
    else if (e.id == "SystemInactive") {
        clickedTabID = "Not Active";
    }
    else if (e.id == "SystemErrors") {
        clickedTabID = "Warning";
    }
    else if (e.id == "SystemAvailable") {
        clickedTabID = "Available";
    }
    $("#Systemvalues").html("(" + e.id.split("m")[1] + ")");
    if (e.children[1].children[1].innerText.trim() != "0" || (e.children[1].children[1].innerText.trim() != "" || typeof (e.children[1].children[1].innerText.trim()) != typeof (undefined))) {
        
        $("#RobotDetails").hide();
        $("#SysDetails").show();
        $("#dvLoadingSystemDetails").hide();
        var t = $('#SystemDetailsTable').DataTable({
            "destroy": true,
            "scrollY": true,
            "scrollX": true,
            "processing": true,
            "serverSide": true,
            "paging": true,
            "pagingType": "input",
            "word-break": "break-all",
            "pageLength": 6,
            "lengthChange": false,
            "language": { search: '', searchPlaceholder: "Search..." },

            "ajax":
              {
                "url": GlobalURL +"/api/CrudService/getConfigurationGridData?tablename=CommandCenterSystemNKPI&type=" + clickedTabID,
                  "contentType": "application/json",
                  "type": "POST",
                "dataType": "JSON",
                async: false,
                "data": function (d) {
                   
                    
                    return JSON.stringify(d);
                  },
                "dataSrc": function (json) {
                   async:false

                      //json.draw = json.draw;
                      //json.recordsTotal = json.recordsTotal;
                      //json.recordsFiltered = json.recordsFiltered;
                      //json.data = json.data;

                      var return_data = json;
                      //return return_data.data;
                      var dttt = $.parseJSON(return_data.data);
                      //dttt = $.parseJSON(dttt);
                      return dttt;
                  }
              },
            "columns": [
                    {
                        "data": "ID",
                        "title": "Id"
                    },
                        {
                            "data": "Name",
                            "title": "System"
                        },
                         {
                             "data": "IP_Address",
                             "title": "IP"
                         },
                          {
                              "data": "CreateBy",
                              "title": "User"
                          },
                        {
                            "title": "Actions",
                            //"defaultContent": "<button class='bton color1' title='Start' onclick='executeRobot1(this)'><i class='fas fa-play-circle' style='font-size:16px'></i></button>&nbsp;<button class='bton color2' title='Stop' onclick='StopSchedule(this)'><i class='fas fa-stop-circle' style='font-size:16px'></i></button>&nbsp;<button class='bton color3' title='Schedule' onclick=\"rsbotid=$(this).parent().closest('tr').find('td')[0].innerHTML;scheduleModal()\"><i class='fas fa-calendar-alt' style='font-size:16px'></i></button>&nbsp;<button class='bton color4' title='History' onclick='historyModalOpen(this)'><i class='fas fa-history' style='font-size:16px'></i></button>&nbsp;<button class='bton color5' title='Log' onclick='OpenLog(this)'><i class='fa fa-tasks' aria-hidden='true' style='font-size:16px'></i></button>"
                            "defaultContent":"<div class='btn-group' role='group'><button class='btn btn-default' data-toggle='modal' onclick='RobotsModalOpen()'><i class='fas fa-plus'></i></button></div>"
                        },

            ],
            "columnDefs": [{
                "targets": '_all',
                "defaultContent": "",
            },
                     {
                         "aTargets": [0],
                         "sClass": "hidden"
                     },
                     {
                         "targets": [0],
                         "orderable": true
                     },
                     //{
                         //"targets": [1],
                         //render: function (data, type, row) {
                         //    ////
                         //    return "<div onclick='popupOpen(this)' style='cursor:pointer'> " + row.Name + "</div>"
                         //}
                     //},
                     //{
                     //    "targets": [2],
                     //    render: function (data, type, row) {
                     //        ////
                     //        if (data == 'admin' || data == '' || data == null) {
                     //            return 'Adhoc'
                     //        }
                     //    }
                     //},
            ],

        });

        $("#divLoader").hide();
        t.page('first').draw('page');
        $(".dataTables_filter").addClass("pull-left");



        //$.ajax({
        //    type: "GET",
        //    url: '/services/CrudService.svc/CommandCenterNKPI?clickedNKPI=' + clickedTabID,
        //    async: true,
        //    contentType: 'application/json; charset=utf-8',
        //    success: function (result) {
        //        $("#dvLoadingRobotDetails").hide();
        //        var dataSource = $.parseJSON(result);
        //        var t = $('#RobotDetailsTable').DataTable({
        //            "oLanguage": { "sSearch": "" },
        //            "scrollX": false,
        //            "scrollY": 170,
        //            destroy: true,
        //            "columnDefs": [{
        //                "targets": '_all',
        //                "defaultContent": "",
        //            },
        //             {
        //                 "aTargets": [0],
        //                 "sClass": "hidden"
        //             }, ]
        //        });
        //        t.clear();
        //        for (var i in dataSource) {
        //            if (dataSource[i]["SystemName"] == 'admin' || dataSource[i]["SystemName"] == '' || dataSource[i]["SystemName"] == null)
        //                dataSource[i]["SystemName"] = 'Adhoc';
        //            t.row.add([
        //       dataSource[i]["Id"],
        //      "<div onclick='popupOpen(this)' style='cursor:pointer'> " + dataSource[i]["Name"] + "</div>",
        //    dataSource[i]["SystemName"],
        //       dataSource[i]["CreatedBy"],

        //       "<button class='bton color1' title='Start' onclick='executeRobot1(this)'><i class='fas fa-play-circle' style='font-size:16px'></i></button>&nbsp;<button class='bton color2' title='Stop' onclick='StopSchedule(this)'><i class='fas fa-stop-circle' style='font-size:16px'></i></button>&nbsp;<button class='bton color3' title='Schedule' onclick=\"rsbotid=$(this).parent().closest('tr').find('td')[0].innerHTML;scheduleModal()\"><i class='fas fa-calendar-alt' style='font-size:16px'></i></button>&nbsp;<button class='bton color4' title='History' onclick='historyModalOpen(this)'><i class='fas fa-history' style='font-size:16px'></i></button>&nbsp;<button class='bton color5' title='Log' onclick='OpenLog(this)'><i class='fa fa-tasks' aria-hidden='true' style='font-size:16px'></i></button>",
        //            ]).node().id = 'RobotList';
        //            t.draw(false);
        //        }
        //        t.page('first').draw('page');
        //        $($('#RobotDetailsTable_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
        //    },
        //    error: function (e) {
        //        //  alert("Something Wrong!!");
        //    }
        //});


    }
}
function LOBLoad() {
    $.ajax({
        type: "GET",
        url: GlobalURL +'/api/CrudService/getTableData?tablename=LOB',
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            $("#LOB_RobotsTree").html("");
            var LOB = $.parseJSON(result);
            //LOB = $.parseJSON(LOB);

            for (i in LOB) {
                $("#LOB_RobotsTree")[0].innerHTML += "<li class='' ><a id='LOB_" + LOB[i].ID + "' href='#'>" + LOB[i].Name + "</a><ul id='ProjectList_" + LOB[i].ID + "'></ul></li>"
            }
            $.ajax({
                type: "GET",
                url: GlobalURL +'/api/CrudService/getTableData?tablename=PROJECT',
                async: true,
                contentType: 'application/json; charset=utf-8',
                success: function (result) {

                    var Project = $.parseJSON(result);
                    //Project = $.parseJSON(Project);

                    for (i in Project) {
                        if (typeof ($("#ProjectList_" + Project[i].LOBId)[0]) != typeof (undefined))
                            $("#ProjectList_" + Project[i].LOBId)[0].innerHTML += "<li class=''><a id='Project_" + Project[i].ID + "' href='#'>" + Project[i].NAME + "</a><ul id='RobotsList_" + Project[i].ID + "'></ul></li>"
                    }
                    $.ajax({
                        type: "GET",
                        url: GlobalURL +'/api/CrudService/getTableData?tablename=ROBOT',
                        async: true,
                        contentType: 'application/json; charset=utf-8',
                        success: function (result) {

                            var Robots = $.parseJSON(result);
                            //Robots = $.parseJSON(Robots);

                            for (i in Robots) {
                                if (typeof ($("#RobotsList_" + Robots[i].Project_Id)[0]) != typeof (undefined))
                                    $("#RobotsList_" + Robots[i].Project_Id)[0].innerHTML += "<li class=''><a id='ROBOT_" + Robots[i].Id + "' href='#' draggable='true' ondragstart='drag(event)' style='overflow:auto'>" + Robots[i].Name + "</a></li>"

                            }
                            $('#tree-view').jstree({
                                "core": {
                                    "themes": {
                                        "variant": "large",
                                    }
                                },
                                "plugins": ["wholerow"]
                            });
                        },
                        error: function (e) {
                            //  alert("Something Wrong!!");
                        }
                    });
                },
                error: function (e) {
                    //  alert("Something Wrong!!");
                }
            });
        },
        error: function (e) {
            //  alert("Something Wrong!!");
        }
    });
}
var SystemsHeight = $("#SubmitForm").css("height");
var InboxTimeInterval = "";
var NotificationTimeInterval = "";
var NKPITimeInterval = "";
$(document).ready(function () {
    $("#Sys").addClass("active")
    $("#footerSchedule").addClass("active")
    s["id"] = "Systems";


    loadNKPI();
    LoadNotification();
    LoadInbox();
    //ConnectivityDB();
    var conn_default = document.getElementById("database");
    Connection_grid(conn_default);
    //LoadNotification();
    RobotsTimeLine();
    SystemsTimeLine();
    //ConnectivityDB();
});

var s = {};
function SystemsLoad() {
   
    var awr = 0;
    var atr = 0;
    var ir = 0;
    $.ajax({
        type: "GET",
        url: GlobalURL +'/api/CrudService/GetSystemsByRobotsCount',
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            awr = 0;
            atr = 0;
            ir = 0;
            var dataSource = $.parseJSON(val);
            //dataSource = $.parseJSON(dataSource);

            $("#activerobots")[0].innerHTML = "";
            $("#emptyrobots")[0].innerHTML = "";
            $("#inactiverobots")[0].innerHTML = "";
            for (var i in dataSource) {
                if (dataSource[i].System_Status.trim() == "Active") {
                    if (parseInt(dataSource[i].RobotsAssigned) > 0) {
                        $("#activerobots")[0].innerHTML += "<div class='col-sm-2 dropHere' style='text-align:center;padding-right:6px !important;padding-left:5px !important;' ><div class='SystemImage availableRobot dropSystem' ondrop='drop(event)' ondragover='allowDrop(event)' title='" + dataSource[i].Name + "' systemId='" + dataSource[i].ID + "' onclick='SystemClick(this)'><input type='checkbox' style='display:none'><img class='dropHere systemImages' src='dist/img/Controller/Icons-2/settings (1).png' style='height:35px;width:40px;'/><h6 class='h6Class' >" + dataSource[i].Name.replace(/\-/g, '&#8209;') + "</h6></div></div>";
                        awr++;
                    }
                    else {
                        $("#emptyrobots")[0].innerHTML += "<div class='col-sm-2 dropHere' style='text-align:center;padding-right:6px !important;padding-left:5px !important;' ><div class='SystemImage availableRobot dropSystem' ondrop='drop(event)' ondragover='allowDrop(event)' title='" + dataSource[i].Name + "' systemId='" + dataSource[i].ID + "' onclick='SystemClick(this)'><input type='checkbox' style='display:none'><img class='dropHere systemImages' src='dist/img/Controller/Icons-2/warnining (1).png' style='height:35px;width:40px;'/><h6 class='h6Class' >" + dataSource[i].Name.replace(/\-/g, '&#8209;') + "</h6></div></div>";
                        atr++;
                    }
                }
                else {
                    $("#inactiverobots")[0].innerHTML += "<div class='col-sm-2 dropHere' style='text-align:center;padding-right:6px !important;padding-left:5px !important;' ><div class='SystemImage' title='" + dataSource[i].Name + "' systemId='" + dataSource[i].ID + "' onclick='SystemClick(this)'><img class='dropHere unavailable systemImages' src='dist/img/Controller/Icons-2/system_grey.png' style='height:35px;width:40px;'/><h6 >" + dataSource[i].Name.replace(/\-/g, '&#8209;') + "</h6></div></div>";
                    ir++;
                }
            }
            $("#count1")[0].innerHTML = " (" + awr + ")";
            $("#count2")[0].innerHTML = " (" + atr + ")";
            $("#count3")[0].innerHTML = " (" + ir + ")";

        },

        error: function (e) {

        }
    });
}
function SystemClick(e) {
    if ($(e).hasClass('SystemImage availableRobot dropSystem')) {
        if ($(e).hasClass('activeSysRobos')) {
            $(e).removeClass('activeSysRobos')
            $(e).removeClass('activeRobos')
            $("#Schedule").css("color", "#1e57c1");
            $("#Run").css("color", "#1e57c1");
            $("#Remove").css("color", "#1e57c1");
            $("#Run").removeClass("clicked");
            $("#Schedule").removeClass("clicked");
            $("#Remove").removeClass("clicked")
            $("#Schedule")[0].disabled = true;
            $("#Run")[0].disabled = true;
            $("#Remove")[0].disabled = true;
        }
        else {
            $('.activeSysRobos').removeClass('activeSysRobos')
            $(e).addClass('activeSysRobos')
        }
        var tableDetails = {};
        tableDetails["tablename"] = "ROBOT";
        tableDetails["RTablename"] = "SystemsRobots";
        tableDetails["ConditionName"] = "Robot_Id";
        tableDetails["ConditionValue"] = "Id";
        tableDetails["MainConditionIdName"] = "System_Id";
        var i;
        tableDetails["MainConditionIdValue"] = "";
        var selectedItem = $(".activeSysRobos");

        if (selectedItem.length != 0) {


            for (i = 0; i < selectedItem.length; i++)
                tableDetails["MainConditionIdValue"] += selectedItem[i].attributes["systemid"].nodeValue + ",";
            tableDetails["MainConditionIdValue"] = tableDetails["MainConditionIdValue"].slice(0, -1);
        }
        else
            tableDetails["MainConditionIdValue"] = 0;
        if (selectedItem.length > 1)
            tableDetails["loop"] = true;
        else
            tableDetails["loop"] = false;
        ActiveSystemId = tableDetails["MainConditionIdValue"];
        var tableDetailsString = JSON.stringify(tableDetails);
        $.ajax({
            type: "GET",
            url: GlobalURL +'/api/CrudService/getDataByRTable?tableDetails=' + tableDetailsString,
            contentType: 'application/json; charset=utf-8',
            success: function (val) {
                var dataSource = {};
                if (val != false) {
                    dataSource = JSON.parse(val);
                }
                $("#RobotsRight-Panel")[0].innerHTML = "";
                for (var i in dataSource) {
                    $("#RobotsRight-Panel")[0].innerHTML += "<div class='col-sm-4'><div class='righPanelRobotIcons' Rs_id='" + dataSource[i].Id + "'  style='text-align:center' title='" + dataSource[i].Name + "' onclick=\"clickrobot(this)\"> <img src='dist/img/Controller/Icons-2/robot.png' style='height:32px;width:32px;'/><h6 >" + dataSource[i].Name.replace(/\-/g, '&#8209;') + "</h6></div></div>"

                }
            },
            error: function (e) {

            }
        });
    }

}
function activetitleclick() {
    $("#activerobots").toggle("2000");
    $("#span1").toggleClass("fa-angle-down");
    $("#span1").toggleClass("fa-angle-right");
}
function emptytitleclick() {
    $("#emptyrobots").toggle("2000");
    $("#span2").toggleClass("fa-angle-down");
    $("#span2").toggleClass("fa-angle-right");
}
function inactivetitleclick() {
    $("#inactiverobots").toggle("2000");
    $("#span3").toggleClass("fa-angle-down");
    $("#span3").toggleClass("fa-angle-right");
}

function removeClick() {
    
    var values = {};
    values["SystemName"] = $('.activeSysRobos')[0].attributes["systemid"].nodeValue;
    values["Robot_ID"] = $('.activeRobos')[0].attributes["rs_id"].nodeValue;
    values["Type"] = "SystemsRobots";
    $.ajax({
        type: "Post",
        url: GlobalURL +'/api/GenbaseStudio/Update?input=' + JSON.stringify(values),
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            $('.activeRobos').parent().remove();
            $("#Schedule").css("color", "#4084eb");
            $("#Run").css("color", "#4084eb");
            $("#Remove").css("color", "#4084eb");
            $("#Run").removeClass("clicked");
            $("#Schedule").removeClass("clicked");
            $("#Remove").removeClass("clicked");
            $("#Schedule")[0].disabled = true;
            $("#Run")[0].disabled = true;
            $("#Remove")[0].disabled = true;
            if ($("#RobotsRight-Panel")[0].innerHTML == "")
                SystemsLoad();

        },
        error: function (e) {

        }
    });
}

var rsbotid;
function clickrobot(e) {
   
    if ($(e).hasClass('activeRobos')) {
        $(e).removeClass('activeRobos')

        $("#Schedule").css("color", "#4084eb");
        $("#Run").css("color", "#4084eb");
        $("#Remove").css("color", "#4084eb");
        $("#Run").removeClass("clicked");
        $("#Schedule").removeClass("clicked");
        $("#Remove").removeClass("clicked");
        $("#Schedule")[0].disabled = true;
        $("#Run")[0].disabled = true;
        $("#Remove")[0].disabled = true;
    } else {
        $(".activeRobos").removeClass('activeRobos')
        $(e).addClass('activeRobos')
        $("#Schedule").css("color", "#1e57c1");
        $("#Run").css("color", "#1e57c1");

        $("#Remove").css("color", "#1e57c1");
        $("#Run").addClass("clicked");
        $("#Schedule").addClass("clicked");;
        $("#Remove").addClass("clicked");

        $("#Schedule")[0].disabled = false;
        $("#Remove")[0].disabled = false;
        $("#Run")[0].disabled = false;
    }

    rsbotid = $(e)[0].attributes[1].nodeValue;
    rsbotName = $(e)[0].attributes[3].nodeValue;
}
function executeDownload(item) {
    
    var dpath = item.FileName.split("\\");
    //window.open("/" + dpath[dpath.length - 2] + "/" + dpath[dpath.length - 1])
    window.open(GlobalURL + '/api/CrudService/Download?fileName=' + dpath[dpath.length - 1] + '&foldername=' + dpath[dpath.length - 2]);


}
var rsbotid1;
var rsbotName;
var job = {};
function executeRobot1(e) {
    rsbotid1 = $(e).parent().closest('tr').find('td')[0].innerHTML;
    rsbotName = $(e).parent().closest('tr').find('td')[1].innerText;
    var x = localStorage.getItem("Result");
    var UserDetails = JSON.parse(x);
    var ProcessManager_Input = {};
    ProcessManager_Input["Type"] = "ProcessManager";
    ProcessManager_Input["username"] = UserDetails.UserName;
    ProcessManager_Input["Robot_ID"] = rsbotid1;
    ProcessManager_Input["userid"] = UserDetails.User_ID;

    $.ajax({
        //headers: {
        //    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        //},
        type: 'POST',
        //url:datasource,
        url: GlobalURL +'/api/GenbaseStudio/Create?input=' + JSON.stringify(ProcessManager_Input),
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (jobjson) {
           
            $("#footerconsole")[0].disabled = false;
            $("#footerconsole").css("color", "black");
            $("#footerconsole").css("cursor", "pointer");
            if ($("#footertoggle").children().hasClass("fa-window-maximize")) {
                footertoggleclick();
            }
            if (jobjson != "false") {
                //job = JSON.parse(jobjson.CreateResult);
                job = JSON.parse(jobjson);
                $("#savedModel")[0].innerHTML = "\nJob is Created......"
            }
            if ($("#footertoggle").children().hasClass("fa-window-maximize"))
                footertoggleclick();

        },
        error: function (e) {
            
        }
    });

    uploadSeverFilename = [];
    uploadFiles_Service();
    var data = {};
    data["dynamicvariables"] = [];
    data["Type"] = "Execution";
    data["ID"] = rsbotid1;
    data["uploadFiles"] = uploadSeverFilename;
    data["systemId_pm"] = job[0]["max"];
    //Console.log(max)
    data["Project_Id"] = '';
    data["Name"] = rsbotName;
    data.Namer = UserDetails.UserName;
    var dataString = JSON.stringify(data);
    
    $.ajax({
     
        type: 'POST',
        url: GlobalURL + '/api/GenbaseStudio/set',
        data: dataString,
        contentType: 'application/json; charset=utf-8',
        //contentType: 'application/json; charset=utf-8',
        //dataType: "json",
        //data: JSON.stringify({ input: btoa(dataString) }),
        //data: data,
        //async: false,
        success: function (data2) {
            var JSONLog = JSON.parse(data2);
            var ResultStringS = JSON.parse(JSONLog.ResultString);
            if (ResultStringS.Downloads != null && ResultStringS.Downloads.length > 0) {
                ResultStringS.Downloads.forEach(executeDownload);
            }
            if (ResultStringS.runtimerequest != null && ResultStringS.runtimerequest != undefined)
                DVariables = ResultStringS.runtimerequest.dvariables;
            if (ResultStringS.SuccessState != "" && ResultStringS.SuccessState != null) {

                if (ResultStringS.SuccessState.toLowerCase() == "success") {
                    var error;
                    if ($(".Rbtbtn.active").parent().hasClass("ValueStreamMap") == true) {
                        swal("", "ValueStreamMap Executed Succesfully", "success", { closeOnClickOutside: false });  
                    }
                    else if ($(".Rbtbtn.active").parent().hasClass("Workflow_Robot") == true) {
                        swal("", "Workflow Executed Succesfully", "success", { closeOnClickOutside: false });  
                    }
                    else if ($(".Rbtbtn.active").parent().hasClass("FlowChart") == true) {
                        swal("", "Flow Chart Executed Succesfully", "success", { closeOnClickOutside: false });  
                    }
                    else
                        swal({
                            title: "",
                            text: "Robot Executed Succesfully",
                            icon: "success",
                            timer: 2000,
                            button: false,
                        });
                    $("#footerOutput").focus();
                    if (ResultStringS.elog != null && ResultStringS.elog.length > 0) {
                        elog(ResultStringS);
                    }
                    else if (ResultStringS.elog == null || ResultStringS.elog == [] || ResultStringS.elog == "") {
                        swal("", "Execution Stopped. Please check Steps and its properties and try again", "error", 10000);
                    }
                    //if ($(".Rbtbtn.active").parent().hasClass("ValueStreamMap") == true) {
                    //    $("#savedModel")[0].innerHTML += "\n>ValueStreamMap '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Executed Successfully";
                    //}
                    //else if ($(".Rbtbtn.active").parent().hasClass("Workflow_Robot") == true) {
                    //    $("#savedModel")[0].innerHTML += "\n>Workflow '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Executed Successfully";
                    //}
                    //else if ($(".Rbtbtn.active").parent().hasClass("FlowChart") == true) {
                    //    $("#savedModel")[0].innerHTML += "\n>Flow Chart '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Executed Successfully";
                    //}
                    //else
                    //    $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Executed Successfully";
                }
                else if (ResultStringS.SuccessState.toLowerCase() == "no steps") {
                    $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "'Cannot execute the robot without steps or without save. Please try again.";
                    swal("", "Cannot execute the robot without steps or without save. Please try again", "error");
                }
                else if (ResultStringS.SuccessState.toLowerCase() == "failure") {
                    $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Execution Stopped.Please check Steps and its properties and try again.";
                    swal("", "Execution Stopped. Please check Steps and its properties and try again", "error");
                }
                else if (ResultStringS.SuccessState.toLowerCase().trim() == "inprogress") {
                   
                    StepID = parseInt(ResultStringS["runtimerequest"]["nextStepID"]);
                    if (ResultStringS["runtimerequest"]["step"].Name == "InputBox") {
                        display_Input(ResultStringS);
                    }
                    else if (ResultStringS["runtimerequest"]["step"].Name == "OutputBox") {
                        display_output(ResultStringS);
                    }
                    else if ((ResultStringS["runtimerequest"]["step"].Name == "Input")) {
                        display_inboxInput(ResultStringS);
                    }
                }
            }
            else {
                $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Execution Stopped.Please check Steps and its properties and try again.";
                swal("", "Execution Stopped. Please check Steps and its properties and try again", "error");
            }
        },
        error: function (e) {
        }
    });

}
function display_inboxInput(ResultStringS) {
    
    elog(ResultStringS);
    $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Execution is initially paused and waiting for the Input";

}
function InputDone() {
    ////

    $("#dynamicModal").modal('hide');
    $(".modal-backdrop.fade.in").remove();
    if ($("#OutputValue_wrapper").length > 0)
        $('#OutputValue').DataTable().destroy();
    $("#OutputValue").html('');
    var data = {};
    for (i in DVariables) {
        if ($("#InputValue")[0].attributes.variable.nodeValue == DVariables[i].vlname) {
            if ($("#InputValue")[0].type == "checkbox") {
                DVariables[i].vlvalue = JSON.stringify(datat($("#InputValue").prop('checked'), DVariables[i].vlname));
            }
            else {
                DVariables[i].vlvalue = JSON.stringify(datat($("#InputValue").val(), DVariables[i].vlname));
            }
            DVariables[i].vlstatus = true;
        }
        if ($("#OutputDiv").css("display") == "block") {
            DVariables[i].vlstatus = false;
        }
    }
    if (DVariables == null)
        data["dynamicvariables"] = [];
    else
        data["dynamicvariables"] = DVariables;
    data["Type"] = "ExecutionInProgress";
    data["ID"] = rsbotid1;
    data["stepvalue"] = StepID;
    data["Project_Id"] = '';
    data["systemId"] = job[0]["max"];
    data["Namer"] = JSON.parse(localStorage.Result).UserName;
    data["Name"] = rsbotName;
    var dataString = JSON.stringify(data);
    $.ajax({
        type: 'POST',
        url: GlobalURL +'/api/GenbaseStudio/set',
        contentType: 'application/json; charset=utf-8',
        data: dataString,
        //data: JSON.stringify({ input: btoa(dataString) }),
        dataType: "json",
        success: function (data2) {
            ////
            var JSONLog = JSON.parse(data2)
            if (JSON.parse(JSONLog.ResultString).SuccessState != "" && JSON.parse(JSONLog.ResultString).SuccessState != null) {
                var ResultStringS = JSON.parse(JSONLog.ResultString);
                if (ResultStringS.runtimerequest != null && ResultStringS.runtimerequest != undefined)
                    DVariables = ResultStringS.runtimerequest.dvariables;
                if (ResultStringS.SuccessState.toLowerCase() == "success") {
                    var error;
                    $("#dynamicModal").modal('hide');
                    swal("", "Robot Executed Succesfully", "success", { closeOnClickOutside: false });  

                    elog(ResultStringS);
                    $("#savedModel")[0].innerHTML += "\n>Robot '" + rsbotName + "' Executed Successfully";
                }
                else if (ResultStringS.SuccessState.toLowerCase() == "failure") {
                    $("#savedModel")[0].innerHTML += "\n>Robot '" + rsbotName + "' Execution Stopped. Please check Steps and its properties and try again.";
                    swal("", "Execution Stopped. Please check Steps and its properties and try again", "error")
                    $("#dynamicModal").modal('hide');
                }
                else if (ResultStringS.SuccessState.toLowerCase().trim() == "inprogress") {

                    //StepID = parseInt(JSON.parse(JSONLog["ResultString"])["StepId"]);
                    StepID = ResultStringS["runtimerequest"]["nextStepID"];
                    if (ResultStringS["runtimerequest"]["step"].Name == "InputBox") {
                        display_Input(ResultStringS)
                    }
                    else if (ResultStringS["runtimerequest"]["step"].Name == "OutputBox") {
                        display_output(ResultStringS);
                    }
                    else if (ResultStringS["runtimerequest"]["step"].Name == "OutputBox") {
                        display_inboxInput(ResultStringS);
                    }

                }
            }
            else {
                $("#savedModel")[0].innerHTML += "\n>Robot '" + rsbotName + "' Execution Stopped.Please check Steps and its properties and try again.";
                swal("", "Execution Stopped. Please check Steps and its properties and try again", "error")
            }

        },
        error: function (e) {

        }
    });

}
function display_output(ResultStringS) {
    //
    for (i in ResultStringS["runtimerequest"]["step"]["StepProperties"]) {
        if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepProperty == "Title") {
            $("#dynamicModal .modal-title").html(ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepPropertyValue)
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepProperty.trim() == "MessageFormat") {
            $("#OutputLabel").html(ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepPropertyValue)
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepProperty.trim() == "Buttons") {
            if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepPropertyValue.trim() == "OK")
                $("#dynamicModal .modal-footer").html('<button type="button" class="btn btn-primary btn-flat"  onclick="InputDone()"><span class="fa fa-arrow-circle-right"></span> Ok</button>');
            else
                $("#dynamicModal .modal-footer").html('<button type="button" class="btn btn-primary btn-flat"  onclick="InputDone()"><span class="fa fa-arrow-circle-right"></span> Ok</button><button type="button" class="btn" data-dismiss="modal"><span class="fas fa-times"></span>Cancel</button>');
        }
    }
    var OutputValuResult;
    OutputValuResult = ResultStringS["runtimerequest"]["dvariables"].find(ind=>ind.vlname == ResultStringS["runtimerequest"]["thisVariableName"]);
    var columns = [];
    $("#OutputTable").show();
    if (OutputValuResult!=undefined && OutputValuResult.vlvalue != undefined && OutputValuResult.vlvalue != "" && OutputValuResult.vlvalue != "[]") {
        if (OutputValuResult.vltype != 'string') {
            $('#dynamicModal .modal-dialog').addClass('output-dialogue');
            $("#OutputTable").show();
            var otpResult = JSON.parse(OutputValuResult.vlvalue)
            for (var key in Object.keys(otpResult[0])) {
                var key_column = {};
                key_column["title"] = Object.keys(otpResult[0])[key];
                key_column["data"] = Object.keys(otpResult[0])[key];
                columns.push(key_column);
            }
            $("#OutputValue").DataTable({
                destroy: true,
                "scrollY": true,
                "scrollX": true,
                "data": otpResult,
                "iDisplayLength": 10,
                "order": [[0, 'desc']],
                "columns": columns,
                "columnDefs": [{
                    "targets": '_all',
                    "defaultContent": "",
                }],
            });
        }
        else {
            $("#OutputresultPre").html(OutputValuResult.vlvalue);

            $("#OutputTable").hide();
            $("#OutputValue").html('');
        }
    }
    else {
        $('#dynamicModal .modal-dialog').addClass('output-dialogue');
        $("#OutputresultPre").html("No Output!");
        $('#dynamicModal .modal-dialog').removeClass('output-dialogue');
    }
    elog(ResultStringS);
    $("#InputDiv").hide();
    $("#OutputDiv").show();
    $("#dynamicModal textarea").val("");

    $("#dynamicModal").modal('show');
}
function display_Input(ResultStringS) {
    $("#Input_WidgetType")[0].innerHTML = "";
    var wt = ResultStringS["runtimerequest"]["step"]["StepProperties"].findIndex(wtsp=>wtsp.StepProperty == "WidgetType")

    if (wt != -1) {
        if (ResultStringS["runtimerequest"]["step"]["StepProperties"][wt].StepPropertyValue.trim() == "Dropdown") {
            $("#Input_WidgetType")[0].innerHTML += "<select class='form-control' id='InputValue' variable=''></select>";
            var textSP = "", valueSP = "";
            var optionsTextResult = "";
            var optionsValueResult = "";
            var itemValueName = "", itemValueNumber = "", itemTextName = "", itemTextNumber = "";
            try {
                if ((textSP = ResultStringS["runtimerequest"]["step"]["StepProperties"].find(s=>s.StepProperty == "Text")) != undefined && textSP["StepPropertyValue"] != null) {
                    if (textSP["StepPropertyValue"].indexOf("[") > -1 && textSP["StepPropertyValue"].indexOf("]") > -1) {
                        itemTextName = textSP["StepPropertyValue"].split('[')[0].trim();
                        itemTextNumber = textSP["StepPropertyValue"].split('[')[1].split(']')[0].trim().replace(/[\"\']/g, "");
                        optionsTextResult = JSON.parse(ResultStringS.runtimerequest.dvariables.find(optionTextDV=>optionTextDV.vlname == itemTextName).vlvalue)
                    }
                    else {
                        optionsTextResult = JSON.parse(ResultStringS.runtimerequest.dvariables.find(optionTextDV=>optionTextDV.vlname == textSP["StepPropertyValue"]))
                    }
                }
                if ((valueSP = ResultStringS["runtimerequest"]["step"]["StepProperties"].find(s=>s.StepProperty == "Value")) != undefined && textSP["StepPropertyValue"] != null) {
                    if (valueSP["StepPropertyValue"].indexOf("[") > -1 && valueSP["StepPropertyValue"].indexOf("]") > -1) {
                        itemValueName = valueSP["StepPropertyValue"].split('[')[0].trim();
                        itemValueNumber = valueSP["StepPropertyValue"].split('[')[1].split(']')[0].trim().replace(/[\"\']/g, "");
                        optionsValueResult = JSON.parse(ResultStringS.runtimerequest.dvariables.find(optionTextDV=>optionTextDV.vlname == itemValueName).vlvalue)
                    }
                    else {
                        optionsValueResult = JSON.parse(ResultStringS.runtimerequest.dvariables.find(optionValueDV=>optionValueDV.vlname == valueSP["StepPropertyValue"]))
                    }
                }
                if (optionsTextResult != "" && optionsValueResult != "" && optionsTextResult.length == optionsValueResult.length) {
                    optionsTextResult.forEach(function iterOptText(TextResult, resultind) {
                        if (resultind != undefined) {
                            console.log(TextResult);
                            if (itemValueNumber != "")
                                $("#InputValue")[0].innerHTML += "<option>" + TextResult[itemTextNumber] + "</option>"
                            else
                                $("#InputValue")[0].innerHTML += "<option>" + TextResult[resultind] + "</option>"
                        }
                    });
                    optionsValueResult.forEach(function iterOptValue(ValueResult, resultind) {
                        if (resultind != undefined) {
                            console.log(ValueResult);
                            if (itemValueNumber != "")
                                $("#InputValue option")[resultind].val(ValueResult[itemValueNumber])
                            else
                                $("#InputValue option")[resultind].val(ValueResult[resultind])
                        }
                    });
                }

            }
            catch (ex) {

            }
            //  $("#InputValue")[0].
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][wt].StepPropertyValue.trim() == "Textbox") {
            $("#Input_WidgetType")[0].innerHTML += "<input type=text class='form-control' id='InputValue' variable=''/>";
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][wt].StepPropertyValue.trim() == "Checkbox") {
            $("#Input_WidgetType")[0].innerHTML += "<input type=checkbox class='form-control' id='InputValue' variable=''/>";
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][wt].StepPropertyValue.trim() == "Textarea") {
            $("#Input_WidgetType")[0].innerHTML += "<textarea class='form-control' id='InputValue' variable=''></textarea>";
        }


    }
    for (i in ResultStringS["runtimerequest"]["step"]["StepProperties"]) {
        if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepProperty == "Title") {
            $("#dynamicModal .modal-title").html(ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepPropertyValue)
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepProperty.trim() == "DisplayMessage") {
            $("#InputLabel").html(ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepPropertyValue)
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepProperty.trim() == "Variable") {
            $("#InputValue")[0].attributes.variable.nodeValue = ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepPropertyValue;
        }
        else if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepProperty.trim() == "Buttons") {
            if (ResultStringS["runtimerequest"]["step"]["StepProperties"][i].StepPropertyValue.trim() == "OK")
                $("#dynamicModal .modal-footer").html('<button type="button" class="btn btn-primary btn-flat" id="DynamicOk" onclick="InputDone()"><span class="fa fa-arrow-circle-right"></span> Ok</button>');
            else
                $("#dynamicModal .modal-footer").html('<button type="button" class="btn btn-primary btn-flat" id="DynamicOk" onclick="InputDone()"><span class="fa fa-arrow-circle-right"></span> Ok</button><button type="button" class="btn" data-dismiss="modal"><span class="fas fa-times"></span>Cancel</button>');
        }

    }
    $("#InputDiv").show();
    $("#OutputDiv").hide();
    $("#OutputTable").hide();
    elog(ResultStringS);
    $("#dynamicModal textarea").val("");
    $('#dynamicModal .modal-dialog').removeClass('output-dialogue');
    $("#dynamicModal").modal('show');

}
function elog(ResultStringS) {
    if (ResultStringS.elog != null) {
        for (i in ResultStringS.elog) {
            $("#savedModel")[0].innerHTML += "\n>'" + ResultStringS.elog[i]["StepName"] + "' " + ResultStringS.elog[i]["Description"];
        }
    }
}
function uploadFiles_Service() {
    for (u = 0; u < uploadList.length; u = u + 2) {
        var upload = {};
        upload["fPath"] = uploadList[u + 1];

        upload["data1"] = JSON.stringify(uploadList[u + 1] + "," + uploadList[u].result);
        var Uploads = JSON.stringify(upload);
        $.ajax({
            type: "POST",
            url: GlobalURL +'/api/GenbaseStudio/Upload',
            data: Uploads,
            async: false,
            contentType: 'application/json; charset=utf-8',
            //dataType: 'json',
            success: function (uploadresult) {


                //console.log(uploadresult);
                //while (uploadSeverFilename.length > 0) {
                //    uploadSeverFilename.pop();
                //}
                uploadSeverFilename.push(uploadresult.UploadResult)
            },
            error: function (e) {
                //  importRobot();
            }
        });

    }

}
var uploadList = [];
var changedURL = "";
var uploadfile = function (event) {
    uploadList = [];
    var input = event.target;
    changedURL = event.target.files[0].name
    var reader = new FileReader();
    reader.onload = function () {

        var dataURL = reader.result;
        var output = event.target;
        output.src = dataURL;
        //alert(reader.result);
    };
    for (var i = 0; i < input.files.length; i++) {
        var reader = new FileReader();
        input.files[i].name;
        reader.readAsDataURL(input.files[i]);

        uploadList.push(reader, input.files[i].name);
    }
};
function executeRobot() {
    $("#myModal1").modal('hide');
    var x = localStorage.getItem("Result");
    var UserDetails = JSON.parse(x);
    var ProcessManager_Input = {};
    ProcessManager_Input["Type"] = "ProcessManager";
    ProcessManager_Input["username"] = UserDetails.UserName;
    ProcessManager_Input["Robot_ID"] = rsbotid;
    ProcessManager_Input["userid"] = UserDetails.User_ID;
    var job = {};
    $.ajax({
        //headers: {
        //    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        //},
        type: 'POST',
        //url:datasource,
        url: GlobalURL +'/api/GenbaseStudio/Create?input=' + JSON.stringify(ProcessManager_Input),
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (jobjson) {
           
            $("#footerconsole")[0].disabled = false;
            $("#footerconsole").css("color", "black");
            $("#footerconsole").css("cursor", "pointer");
            if ($("#footertoggle").children().hasClass("fa-window-maximize")) {
                footertoggleclick();
            }
            if (jobjson != "false") {
                //job = JSON.parse(jobjson.CreateResult);
                job = JSON.parse(jobjson);
                //job = JSON.parse(job);
                $("#savedModel")[0].innerHTML = "\nJob is Created....."

            }
            if ($("#footertoggle").children().hasClass("fa-window-maximize"))
                footertoggleclick();

        },
        error: function (e) {
        }
    });
    
    uploadSeverFilename = [];
    uploadFiles_Service();
    var data = {};
    data["dynamicvariables"] = [];
    data["Type"] = "Execution";
    data["ID"] = rsbotid;
    data["uploadFiles"] = uploadSeverFilename;
    data["systemId_pm"] = job[0]["max"];
    data["Project_Id"] = '';
    //data["Namer"] = (JSON.parse(localStorage.Result).UserName);
    data.Namer = UserDetails.UserName;
    data["Name"] = rsbotName;
    var dataString = JSON.stringify(data);

    $.ajax({
        type: 'POST',
        url: GlobalURL + '/api/GenbaseStudio/set',
        contentType: 'application/json; charset=utf-8',
        data: dataString,
        dataType: "json",
        success: function (data2) {
           
            //var JSONLog = JSON.parse(data2);
            var ResultStringS = JSON.parse(data2.ResultString);
            if (ResultStringS.Downloads != null && ResultStringS.Downloads.length > 0) {
                ResultStringS.Downloads.forEach(executeDownload);
            }
            if (ResultStringS.runtimerequest != null && ResultStringS.runtimerequest != undefined)
                DVariables = ResultStringS.runtimerequest.dvariables;
            if (ResultStringS.SuccessState != "" && ResultStringS.SuccessState != null) {

                if (ResultStringS.SuccessState.toLowerCase() == "success") {
                    var error;
                    if ($(".Rbtbtn.active").parent().hasClass("ValueStreamMap") == true) {
                        swal("", "ValueStreamMap Executed Succesfully", "success", { closeOnClickOutside: false });  
                    }
                    else if ($(".Rbtbtn.active").parent().hasClass("Workflow_Robot") == true) {
                        swal("", "Workflow Executed Succesfully", "success", { closeOnClickOutside: false });  
                    }
                    else if ($(".Rbtbtn.active").parent().hasClass("FlowChart") == true) {
                        swal("", "Flow Chart Executed Succesfully", "success", { closeOnClickOutside: false });  
                    }
                    else
                        swal({
                            title: "",
                            text: "Robot Executed Succesfully",
                            icon: "success",
                            timer: 2000,
                            button: false,
                        });
                    $("#footerOutput").focus();
                    if (ResultStringS.elog != null && ResultStringS.elog.length > 0) {
                        elog(ResultStringS);
                    }
                    else if (ResultStringS.elog == null || ResultStringS.elog == [] || ResultStringS.elog == "") {
                        swal("", "Execution Stopped. Please check Steps and its properties and try again", "error", 10000);
                    }
                    if ($(".Rbtbtn.active").parent().hasClass("ValueStreamMap") == true) {
                        $("#savedModel")[0].innerHTML += "\n>ValueStreamMap '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Executed Successfully";
                    }
                    else if ($(".Rbtbtn.active").parent().hasClass("Workflow_Robot") == true) {
                        $("#savedModel")[0].innerHTML += "\n>Workflow '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Executed Successfully";
                    }
                    else if ($(".Rbtbtn.active").parent().hasClass("FlowChart") == true) {
                        $("#savedModel")[0].innerHTML += "\n>Flow Chart '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Executed Successfully";
                    }
                    else
                        $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Executed Successfully";
                }
                else if (ResultStringS.SuccessState.toLowerCase() == "no steps") {
                    $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "'Cannot execute the robot without steps or without save. Please try again.";
                    swal("", "Cannot execute the robot without steps or without save. Please try again", "error");
                }
                else if (ResultStringS.SuccessState.toLowerCase() == "failure") {
                    $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Execution Stopped.Please check Steps and its properties and try again.";
                    swal("", "Execution Stopped. Please check Steps and its properties and try again", "error");
                }
                else if (ResultStringS.SuccessState.toLowerCase().trim() == "inprogress") {
                    
                    StepID = parseInt(ResultStringS["runtimerequest"]["nextStepID"]);
                    if (ResultStringS["runtimerequest"]["step"].Name == "InputBox") {
                        display_Input(ResultStringS);
                    }
                    else if (ResultStringS["runtimerequest"]["step"].Name == "OutputBox") {
                        display_output(ResultStringS);
                    }
                    else if ((ResultStringS["runtimerequest"]["step"].Name == "Input")) {
                        display_inboxInput(ResultStringS);
                    }
                }
            }
            else {
                $("#savedModel")[0].innerHTML += "\n>Robot '" + $("#ProcessBreadCrumb")[0].innerHTML + "' Execution Stopped.Please check Steps and its properties and try again.";
                swal("", "Execution Stopped. Please check Steps and its properties and try again", "error");
            }
        },
        error: function (e) {
        }
    });
}

var interval;
//function ScheduleFunction() {
    
//    var interval = $("#Interval").val();
//    var Values = {};
//    Values["RobotId"] = rsbotid;
//    Values["SystemId"] = ActiveSystemId;
//    Values["interval"] = interval;
//    Values["StartTime"] = $("#appt-time").val();
//    Values["StartDate"] = $("#datetimepicker").val();
//    Values["Priority"] = $("#priority").val();
//    Values["TimePeriod"] = $("#TimePeriod").val();
//    Values["CreateBy"] = (JSON.parse(localStorage.Result).User_ID);
//    Values["UpdateBy"] = (JSON.parse(localStorage.Result).User_ID);
//    Values["Status"] = "true";
//    $.ajax({
//        type: "GET",
//        url: GlobalURL +'/api/CrudService/robotSchedule?ScheduleDetails=' + JSON.stringify(Values),
//        async: false,
//        //data: Htmlname ,
//        contentType: 'application/json; charset=utf-8',
//        //dataType: 'json',
//        success: function (val) {
//            ScheduleClose();
//            $('#myModal1').modal('hide');
//            if (val == "true" || val == "Success") {
//                swal("", "Scheduled Successfully", "success")
//            }
//            else {
//                swal("", "Please Schedule With All Details", "error")
//            }
//        },
//        error: function (e) {

//        }
//    });
//}
function ScheduleFunction1(e) {
    // ////
    var rsbotid = $(e).parent().closest('tr').find('td')[0].innerHTML;
    var interval = $("#Interval").val();

    var Values = {};
    Values["RobotId"] = rsbotid;
    Values["interval"] = interval;
    Values["StartTime"] = $("#appt-time").val();
    Values["StartDate"] = $("#datetimepicker").val();
    Values["Priority"] = $("#priority").val();
    Values["TimePeriod"] = $("#TimePeriod").val();
    Values["Status"] = "true";
    $.ajax({
        type: "GET",
        url: GlobalURL +'/api/CrudService/robotSchedule?ScheduleDetails=' + JSON.stringify(Values),
        async: false,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        success: function (val) {
            //location.reload();
            ScheduleClose();

            if (val == "true") {
                swal("", "Robot Scheduled Successfully", "success", { closeOnClickOutside: false });  
            }
        },
        error: function (e) {

        }
    });
}
function StopSchedule(e) {
    ////
    var stopScheduleBotId = $(e).parent().closest('tr').find('td')[0].innerHTML;
    var Values = {};
    Values["RobotId"] = stopScheduleBotId;
    Values["Status"] = "true";
    $.ajax({
        type: "GET",
        url: GlobalURL +'/api/CrudService/stopRobotSchedule?ScheduleDetails=' + JSON.stringify(Values),
        async: false,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        success: function (val) {
            //location.reload();
            $('#ScheduleModal').modal('hide');
            if (val == "true" || val == "Success") {
                swal("", "Schedule Stopped Successfully", "success", { closeOnClickOutside: false });  
            }
            else {
                swal("", "There is no schedule", "error")
            }
        },
        error: function (e) {

        }
    });
}

//function scheduleModal() {
//    rsbotid;
//    ActiveSystemId;
//    $.ajax({
//        type: "GET",
//        url: GlobalURL +'/api/CrudService/getScheduleDetails?RobotId=' + rsbotid + '&SystemId=' + ActiveSystemId,
//        async: false,
//        contentType: 'application/json; charset=utf-8',
//        success: function (val) {
//            var scheduleDetails = JSON.parse(val);
//            if (scheduleDetails.length > 0) {
//                $("#Interval").val(scheduleDetails[0]['interval']);
//                $("#datetimepicker").val(scheduleDetails[0]['Startdate']);
//                $("#appt-time").val(scheduleDetails[0]['Starttime']);
//                $("#priority").val(scheduleDetails[0]['Priority']);
//                $("#TimePeriod").val(scheduleDetails[0]['Timeperiod']);
//            }
//            else {
//                $("#datetimepicker").val('');
//                $("#appt-time").val('');
//                $("#priority").val('');
//                $("#Interval").val('');
//                $("#TimePeriod").val('');
//            }
//        },
//        error: function (e) {
//        }
//    });
//    $("#ScheduleModal").modal('show');

//     $("#Interval").val('')
//}
function openCity(event, type) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent1");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks1");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(type).style.display = "block";
    event.currentTarget.className += " active";
}
//function ScheduleClose() {
//    $("#datetimepicker").val('');
//    $("#appt-time").val('');
//    $("#priority").val('');
//    $("#Interval").val('');
//    $("#TimePeriod").val('');
//    $("#ScheduleModal").modal('hide');
//}

var Systems_List = [];
function loadSystems() {
    
    $.ajax({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        type: 'GET',
        url: GlobalURL + '/api/CrudService/getTableData?tablename=Systems',
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (SystemResult) {
            Systems_List = JSON.parse(SystemResult);


        },
        error: function (e) {

        }
    });
}
function scheduleModal() {

    $("#ScheduleModal").modal('show');
    // $("#Interval").val('')
}
function loadScheduleDetails() {
    
    if ($("#ScheduleSystems").val() != null) {
        RobotID = this.rsbotid;
        //RobotName = $(e).parent().closest('tr').find('td')[1].innerText;
        $.ajax({
            type: "GET",
            url: GlobalURL + '/api/CrudService/getScheduleDetails?RobotId=' + RobotID + '&SystemId=' + $("#ScheduleSystems").val(),
            async: false,
            contentType: 'application/json; charset=utf-8',
            success: function (val) {
                var scheduleDetails = JSON.parse(val);
                if (scheduleDetails.length > 0) {
                    $("#Interval").val(scheduleDetails[0]['interval']);
                    $("#startdatetimepicker").val(scheduleDetails[0]['Startdate']);
                    $("#appt-time").val(scheduleDetails[0]['Starttime']);
                    //$("#priority").val(scmheduleDetails[0]['Priority']);
                    $("#RepeatType").val(scheduleDetails[0]['Timeperiod']);
                    $("#description").val(scheduleDetails[0]['Tag']);
                    $("#Status_Scheduler")[0].checked = scheduleDetails[0]['ScheduleStatus']
                    $("#Server_Scheduler")[0].checked = (scheduleDetails[0]['Runatserver']);
                    if (scheduleDetails[0]['Timeperiod'] != "Never") {
                        document.getElementsByName("end_date")[0].style.display = "block"
                        var chk_list = document.getElementsByName("end_date")[0].getElementsByClassName("radio_main_cl")
                        for (j = 0; j < chk_list.length; j++) {
                            if (chk_list[j].getElementsByTagName("label")[0].textContent == scheduleDetails[0]["End"]) {
                                chk_list[j].getElementsByTagName("input")[0].checked = "true";
                            }
                        }
                    }
                }
                else {
                    $("#Interval").val('');
                    $("#startdatetimepicker").val('');
                    $("#appt-time").val('');
                    //$("#priority").val(scmheduleDetails[0]['Priority']);
                    $("#RepeatType").val('');
                    $("#description").val('');
                    $("#Status_Scheduler")[0].checked = false;
                    $("#Server_Scheduler")[0].checked = false;
                    document.getElementsByName("end_date")[0].style.display = "none"
                }
            },
            error: function (e) {
            }
        });
    }
}
function ScheduleClose() {
    $("#Interval").val("0");
    $("#startdatetimepicker").val('');
    $("#appt-time").val('');
    //$("#priority").val(scmheduleDetails[0]['Priority']);
    $("#RepeatType").val("Never");
    $("#description").val('');
    $("#Status_Scheduler")[0].checked = false;
    $("#Server_Scheduler")[0].checked = false;
    document.getElementsByName("end_date")[0].style.display = "none"
    $("#ScheduleModal").modal('hide');
    $("#ScheduleEventModal").modal('hide');
}
function ScheduleEventClose() {
    $("#Interval").val('');
    $("#startdatetimepicker").val('');
    $("#appt-time").val('');
    //$("#priority").val(scmheduleDetails[0]['Priority']);
    $("#RepeatType").val('');
    $("#description").val('');
    $("#Status_Scheduler")[0].checked = false;
    $("#Server_Scheduler")[0].checked = false;
    document.getElementsByName("end_date")[0].style.display = "none"

    $("#ScheduleModal").modal('hide');
    $("#ScheduleEventModal").modal('hide');
}
function ScheduleFunction() {
    var interval = $("#Interval").val();
    var Values = {};
    Values["RobotId"] = RobotID;
    Values["SystemId"] = $("#ScheduleSystems").val();
    Values["interval"] = interval;
    Values["StartTime"] = $("#appt-time").val();
    Values["StartDate"] = $("#startdatetimepicker").val();
    //Values["Priority"] = null;
    Values["TimePeriod"] = $('#RepeatType')[0].value;
    Values["CreateBy"] = (JSON.parse(localStorage.Result).User_ID);
    Values["UpdateBy"] = (JSON.parse(localStorage.Result).User_ID);
    Values["Status"] = $("#Status_Scheduler")[0].checked;
    Values["Runatserver"] = $("#Server_Scheduler")[0].checked;
    //Values["EndDate"] = $("#enddatetimepicker").val();
    Values["EndDate"] = "";
    Values["End"] = "";
    Values["RepeatOn"] = "";
    Values["RepeatEvery"] = "";
    Values["Description"] = $('#description')[0].value;
    if (Values["TimePeriod"] != "Never") {
        if (Values["TimePeriod"] == "Daily") {
            Values["RepeatEvery"] = $('#RepeatEvery')[0].value + 'd';
        }
        else if (Values["TimePeriod"] == "Weekly") {
            Values["RepeatOn"] = [];
            Values["RepeatEvery"] = $('#RepeatEvery')[0].value + 'w';
            for (i = 0; i < document.getElementsByClassName('weekly_checkbox')[0].getElementsByClassName('predefinedCheckbox').length; i++) {
                if (document.getElementsByClassName('weekly_checkbox')[0].getElementsByClassName('predefinedCheckbox')[i].checked == true) {
                    Values["RepeatOn"].push(document.getElementsByClassName('weekly_checkbox')[0].children[i].children[2].textContent);
                }
            }
            Values["RepeatOn"] = Values["RepeatOn"].join('_');
        }
        else if (Values["TimePeriod"] == "Monthly") {
            Values["RepeatEvery"] = $('#RepeatEvery')[0].value + 'm';
            if (document.getElementsByName("end_monthly")[0].checked == true) {
                Values["RepeatOn"] = document.getElementsByName("end_monthly")[0].parentElement.children[2].value; // Only Day
            }
            else if (document.getElementsByName("end_monthly")[1].checked == true) {
                Values["RepeatOn"] = document.getElementsByName("end_monthly")[1].parentElement.children[2].value + " " + document.getElementsByName("end_monthly")[1].parentElement.children[3].value
            }
        }
        else if (Values["TimePeriod"] == "Yearly") {
            Values["RepeatEvery"] = $('#RepeatEvery')[0].value + 'y';
            if (document.getElementsByName("end_yearly")[0].checked == true) {
                Values["RepeatOn"] = document.getElementsByName("end_yearly")[0].parentElement.children[2].value + "_" + document.getElementsByName("end_yearly")[0].parentElement.children[1].value;; //  Day
            }
            else if (document.getElementsByName("end_yearly")[1].checked == true) {
                Values["RepeatOn"] = document.getElementsByName("end_yearly")[1].parentElement.children[2].value + " " + document.getElementsByName("end_yearly")[1].parentElement.children[3].value + " " + document.getElementsByName("end_yearly")[1].parentElement.children[5].value;
            }
        }
        else { }
        for (j = 0; j < document.getElementsByName("end_repeat").length; j++) {
            if (document.getElementsByName("end_repeat")[j].checked == true) {
                Values["End"] = document.getElementsByName("end_repeat")[j].parentElement.children[1].textContent;
                var End_Type = document.getElementsByName("end_repeat")[j].parentElement.children[1].textContent;
                if (End_Type == "Never") {
                    Values["End"] = End_Type;
                }
                else if (End_Type == "After") {
                    Values["End"] = document.getElementsByName("end_repeat")[j].parentElement.children[2].value + "O"
                }
                else if (End_Type == "On") {
                    Values["End"] = document.getElementsByName("end_repeat")[j].parentElement.children[2].value
                }
                else { }
            }
        }
    }
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/robotSchedule?ScheduleDetails=' + JSON.stringify(Values),
        async: false,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        success: function (val) {
            ScheduleClose();
            $('#ScheduleModal').modal('hide');
            if (val == "true" || val == "Success") {
                swal("", "Scheduled Successfully", "success", { closeOnClickOutside: false });  
            }
            else {
                swal("", "Please Schedule With All Details", "error")
            }
        },
        error: function (e) {

        }
    });
}
function scheduleEvent() {
    $('#ScheduleModal').modal('hide');
    $("#ScheduleSystems")[0].innerHTML = ""
    var select = document.getElementById("ScheduleSystems")
    loadSystems();
    for (var system in Systems_List) {
        var opt = document.createElement('option');
        opt.value = Systems_List[system]["ID"];
        opt.text = Systems_List[system]["Name"];
        select.add(opt, system);
    }
    //var opt = document.createElement('option');
    //opt.value = 0;
    //opt.text = "Server";
    //select.add(opt, 0);
    var opt = document.createElement('option');
    opt.value = "";
    opt.text = "Select System";
    opt.selected = true;
    opt.disabled = true;
    opt.hidden = true;
    select.add(opt, 0);
    $("#ScheduleEventModal").modal('show');

    $('#RepeatType').on("change", function (e, data) {
        var list = document.getElementsByClassName("repeat_every")[0];
        document.getElementsByName("interval")[0].style.display = "none";
        document.getElementsByName("weekly")[0].style.display = "none";
        document.getElementsByName("daily")[0].style.display = "none";
        document.getElementsByName("end_date")[0].style.display = "none";
        document.getElementsByName("Monthly")[0].style.display = "none";
        document.getElementsByName("yearly")[0].style.display = "none";
        if (e.currentTarget.value == "Repeat By Interval") {
            //document.getElementsByName("interval")[0].style.display = "block";
            document.getElementsByName("end_date")[0].style.display = "block";
        }
        else if (e.currentTarget.value == "Daily") {
            list.getElementsByTagName("label")[0].innerHTML = "Day(s)";
            document.getElementsByName("daily")[0].style.display = "block";
            document.getElementsByName("end_date")[0].style.display = "block";
        }
        else if (e.currentTarget.value == "Weekly") {
            list.getElementsByTagName("label")[0].innerHTML = "Week(s)";
            document.getElementsByName("daily")[0].style.display = "block";
            document.getElementsByName("end_date")[0].style.display = "block";
            document.getElementsByName("weekly")[0].style.display = "block";
        }
        else if (e.currentTarget.value == "Monthly") {
            list.getElementsByTagName("label")[0].innerHTML = "Month(s)";
            document.getElementsByName("daily")[0].style.display = "block";
            document.getElementsByName("end_date")[0].style.display = "block";
            document.getElementsByName("Monthly")[0].style.display = "block";
        }
        else if (e.currentTarget.value == "Yearly") {
            list.getElementsByTagName("label")[0].innerHTML = "Year(s)";
            document.getElementsByName("daily")[0].style.display = "block";
            document.getElementsByName("end_date")[0].style.display = "block";
            document.getElementsByName("yearly")[0].style.display = "block";
            $("#week_selection1").html($("#week_selection").html());
            $("#weekday_selection1").html($("#weekday_selection").html());
            $("#Month_selection1").html($("#Month_selection").html());
        }
    });
}


function allowDrop(ev) {
    ev.preventDefault();
}
function drag(e) {
    e.dataTransfer.setData("text", e.target.id);
}
function drop(e) {
    
    e.preventDefault();
    var Robot_ID;
    var System_ID;
    if ($(e.target).hasClass("dropSystem") || $(e.target).hasClass("dropHere systemImages") || $(e.target).hasClass("SystemImage dropSystem") || $(e.target).hasClass("h6Class") || $(e.target).hasClass("SystemImage availableRobot dropSystem")) {
        Robot_ID = e.dataTransfer.getData("text").split("_")[1];
        CreateBy = (JSON.parse(localStorage.Result).User_ID);
        UpdateBy = (JSON.parse(localStorage.Result).User_ID);
       
        if ($(e.target).hasClass("dropHere")) {
            System_ID = $(e.currentTarget.attributes)[4].nodeValue;
        }
        else if ($(e.target).hasClass("SystemImage dropSystem")) {
            System_ID = $(e.currentTarget.attributes)[4].nodeValue;
        }
        else if ($(e.target).hasClass("SystemImage availableRobot dropSystem")) {
            System_ID = $(e.currentTarget.attributes)[4].nodeValue;
        }
        
        var Values = {};
        Values["Robot_Id"] = window.btoa(Robot_ID);
        Values["System_Id"] = window.btoa(System_ID);
        Values["CreateBy"] = window.btoa(CreateBy);
        Values["CreateDatetime"] = window.btoa(datetime);
        Values["UpdateBy"] = window.btoa(UpdateBy);
        Values["UpdateDatetime"] = window.btoa(datetime);
        Values["Status"] = window.btoa("true");
        var values = JSON.stringify(Values);
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/createTableRow?tablename=SystemsRobots',
            data:  JSON.stringify(values),
            async: false,
            //data: Htmlname ,
            contentType: 'application/json; charset=utf-8',
            //dataType: 'json',
            success: function (val) {
                
                $(e.target)[0].src = 'dist/img/Controller/Icons-2/settings (1).png';
                $("#RobotsRight-Panel")[0].innerHTML = "";
                SystemsLoad();
            },
            error: function (e) {
            }
        });
    }
}

var currentdate = new Date();
var datetime = (currentdate.getMonth() + 1) + "/"
            + currentdate.getDate()  + "/"
            + currentdate.getFullYear() + " "
            + currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();
$(document).ready(function () {
    //$($("#calendar")[0].children[1].children[0].children[0].children[1]).click(function () {

    //    scheduleEvent();
    //});

    $("#ScheduleForm").on("submit", function (e) {
        e.preventDefault();
        ScheduleFunction();
    })

    $("#SubmitForm").on("submit", function (e) {
       
        e.preventDefault();
        var Values = {
        }
        var Columns = $('[name="Columns"]');
        var len = $('[name="Columns"]').length;
        for (var i = 1; i < len; i++) {
            var ID = $('[name="Columns"]')[i].id;
            var columnName = ID.split("|");
            if (columnName[0].toUpperCase() == "CREATEBY") {
                Values[columnName[0]] = window.btoa(JSON.parse(localStorage.Result).UserName);
            }
            else if (columnName[0].toUpperCase() == "CREATEDATETIME" || columnName[0].toUpperCase() == "BINDING_TIME") {
                //Values[columnName[0]] = window.btoa(datetime);
                let current_datetime = new Date();
                let formatted_date = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes();
                Values[columnName[0]] = window.btoa(formatted_date);
                
            }
            else if (columnName[0].toUpperCase() == "SYSTEM_STATUS") {
                Values[columnName[0]] = window.btoa("Active");
            }
            else if (columnName[0].toUpperCase() == "STATUS")
                Values[columnName[0]] = window.btoa(true);
            else if (columnName[0].toUpperCase() == "SYSTEMTYPE") {
                if (JSON.parse((localStorage.Result)).Role_ID == "1")
                    Values[columnName[0]] = window.btoa("Admin");
                else if (JSON.parse((localStorage.Result)).Role_ID == "2")
                    Values[columnName[0]] = window.btoa("Analyst");
                else if (JSON.parse((localStorage.Result)).Role_ID == "3")
                    Values[columnName[0]] = window.btoa("User");
            }
            else {
                if ($('[name="Columns"]')[i].type == "checkbox")
                    Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].checked);
                else
                    Values[columnName[0]] = window.btoa($('[name="Columns"]')[i].value);
            }
        }
        var values = JSON.stringify(Values);
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/createTableRow?tablename=Systems',
            data: JSON.stringify(values),
            //url: GlobalURL +'/api/CrudService/createTableRow',
            //data: JSON.stringify({
            //    "tablename": 'Systems',
            //    "Values": values
            //}),
            //data: Htmlname ,
            contentType: 'application/json; charset=utf-8',
            //dataType: 'json', 
            success: function (val) {
                if (val == "true") {
                    swal("", "System Added Successfully", "success", { closeOnClickOutside: false });  
                }
                else {
                    swal("", "System name already exists", "error", { closeOnClickOutside: false });  
                }
                SystemsModalOpen();
                $("#addModal").modal('hide');
                loadNKPI();
            },
            error: function (e) {

            }
        });
    });
});
var rsbotid2;
function popupOpen(e) {
  
    rsbotid2 = $(e).parent().closest('tr').find('td')[0].innerHTML;
    var botname = $(e).parent().closest('tr').find('td')[1].innerText;
    $.ajax({
        type: "GET",
        url: GlobalURL +'/api/CrudService/SystemsByRobotCount?RobotId=' + rsbotid2,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        success: function (val) {
            //  ////            
            var dataSource = $.parseJSON(val);
            //dataSource = $.parseJSON(dataSource);

            $("#activerobots1")[0].innerHTML = "";
            $("#RobotDescription")[0].innerHTML = "";
            if (typeof (dataSource[0]) != typeof (undefined))
                $("#RobotDescription")[0].innerHTML += "<h3 class='col-sm-12' style='text-align:left'>" + botname + " </h3><h5 class='col-sm-12' style='text-align:left'>" + dataSource[0].Description + "</h5> "
            else
                $("#RobotDescription")[0].innerHTML += "<h3 class='col-sm-12' style='text-align:left'>" + botname + " </h3><h5 class='col-sm-12' style='text-align:left'>" + botname + "</h5> "
            for (var i in dataSource) {
                $("#activerobots1")[0].innerHTML += "<div class='col-sm-2 dropHere' style='text-align:center;padding-right:6px !important;padding-left:5px !important;' ><div class='SystemImage availableRobot dropSystem' title='" + dataSource[i].Name + "' systemId='" + dataSource[i].ID + "' onclick='SystemClick(this)'><input type='checkbox' style='display:none'><img class='dropHere systemImages' src='dist/img/Controller/Icons-2/settings (1).png'/><h6 class='h6Class' >" + dataSource[i].Name.replace(/\-/g, '&#8209;') + "</h6></div></div>";
            }
            if ($("#activerobots1")[0].innerHTML == "") {
                $("#activerobots1")[0].innerHTML += "<div class='col-sm-2' style='text-align:center;'>No Systems</div>"
            }
        },
        error: function (e) {
        }
    });
    $.ajax({
        type: "GET",
        url: GlobalURL +'/api/CrudService/getTableDataByUserID?tablename=ProcessMangerBotDetails&CreateBy=' + rsbotid2,
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            var dataSource = $.parseJSON(result);
            //dataSource = $.parseJSON(dataSource);

            var arrValues = [['Status', 'Value']];
            var iCnt = 0;
            var colors = [];
            $.each(dataSource, function () {
                arrValues.push([dataSource[iCnt].category, dataSource[iCnt].value]);
                if (dataSource[iCnt].category == "Success")
                    colors.push("limegreen");
                else if (dataSource[iCnt].category == "Failure")
                    colors.push("red");
                else
                    colors.push("blue");
                iCnt += 1;
            });
            var options = {
                title: 'Status of the Robot',
                colors: colors,
                areaOpacity: 0.8,
                height: 200,
                width: 300,
                is3D: true
            };
            var figures = google.visualization.arrayToDataTable(arrValues)
            var chart = new google.visualization.PieChart(document.getElementById('Status_Chart'));
            chart.draw(figures, options);
        },
        error: function (e) {
        }
    });


    $.ajax({
        type: "GET",
        url: GlobalURL +'/api/CrudService/getTableDataByUserID?tablename=Scheduler_CommandCenter&CreateBy=' + rsbotid2,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        success: function (val) {
            ////
            var dataSource = $.parseJSON(val);
            //dataSource = $.parseJSON(dataSource);
            var t = $('#BotSchedularDetails').DataTable({
                destroy: true,
                "scrollY": true,
                "scrollX": true,
                "order": [[0, "desc"]],
                "pagingType": "input",
                "oLanguage": { "sSearch": "" },
                "columnDefs": [{
                    "targets": '_all',
                    "defaultContent": "",
                },
                 {
                     "aTargets": [0],
                     "sClass": "hidden"
                 }, ]
            });
            t.clear();
            for (var i in dataSource) {
                t.row.add([
           dataSource[i]["RobotId"],
           dataSource[i]["interval"],
           dataSource[i]["LastExecutionTime"],
           dataSource[i]["createby"],
                ]);
                t.draw(false);
            }
            t.page('first').draw('page');
            $($('#BotSchedularDetails_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
        },
        error: function (e) {
        }
    });



    $.ajax({
        type: "GET",
        url: GlobalURL +'/api/CrudService/getTableDataByUserID?tablename=History_CommandCenter&CreateBy=' + rsbotid2,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        success: function (val) {
            
            var dataSource1 = $.parseJSON(val);
            //dataSource1 = $.parseJSON(dataSource1);
            var t = $('#BotHistoryDetails').DataTable({
                destroy: true,
                "scrollY": true,
                "scrollX": true,
                "order": [[0, "desc"]],
                "pagingType": "input",
                "oLanguage": { "sSearch": "" },
                "columnDefs": [{
                    "targets": '_all',
                    "defaultContent": "",
                },
                 {
                     "aTargets": [0],
                     "sClass": "hidden"
                 }, ]
            });
            t.clear();
            for (var i in dataSource1) {
                t.row.add([
           dataSource1[i]["JobName"],
           dataSource1[i]["SubmittedBy"],
           dataSource1[i]["JobStatus"],
           dataSource1[i]["StartingTime"],
           dataSource1[i]["EndingTime"],
                ]);
                t.draw(false);
            }
            t.page('first').draw('page');
            $($('#BotHistoryDetails_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
        },
        error: function (e) {
        }
    });


    $('#myModal').modal('show');
}
function OpenLog(e) {
   
    var Log = $(e).parent().closest('tr').find('td')[0].innerHTML;

    $("#footerLog")[0].disabled = false;
    $("#footerLog").css("color", "black");
    $("#footerLog").css("cursor", "pointer");
    $("#footerLog").click();
    var t = $('#RobotLogDetails').DataTable({
        destroy: true,
        "scrollY": true,
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "paging": true,
        "word-break": "break-all",
        "pageLength": 10,
        "pagingType": "input",
        "lengthChange": false,
        "language": { search: '', searchPlaceholder: "Search..." },

        "ajax":
          {
            "url": GlobalURL +"/api/CrudService/getTableDataByUserID?tablename=ProcessManager&roleid=" + Log,
              "contentType": "application/json",
              "type": "GET",
              "dataType": "JSON",
              "data": function (d) {
                  //
                  return d;
              },
              "dataSrc": function (json) {
                  //

                  json.draw = json.draw;
                  json.recordsTotal = json.recordsTotal;
                  json.recordsFiltered = json.recordsFiltered;
                  json.data = json.data;

                  var return_data = json;
                  //return return_data.data;
                  var dttt = $.parseJSON(return_data.data);
                  //dttt = $.parseJSON(dttt);
                  return dttt;
              }
          },

        "columns": [

                    {
                        "data": "Robot_ID",
                        "title": "Robot_ID"
                    },
                    {
                        "data": "StepName",
                        "title": "Step Name"
                    },
                    {
                        "data": "Description",
                        "title": "Description"
                    },
                    {
                        "data": "CreateBy",
                        "title": "Create by"
                    },




        ],
        "columnDefs": [{
            "targets": '_all',
            "defaultContent": "",
        },
         {
             "aTargets": [0, 1],
             "sClass": "hidden"
         },
        {
            "targets": [0],
            "orderable": true
        },
        ],
    });

    t.page('first').draw('page');
    $(".dataTables_filter").addClass("pull-left");
    if ($("#footertoggle").children().hasClass("fa-window-maximize"))
        footertoggleclick();


    //$.ajax({
    //    type: "GET",
    //    url: '/services/CrudService.svc/getRobotLog?tablename=ProcessManager&roleid=' + Log,
    //    async: false,
    //    contentType: 'application/json; charset=utf-8',
    //    success: function (val) {
    //        ////
    //        if (val == "") {
    //            swal("", "No log details on this robot", "error")
    //        }
    //        $("#footerLog")[0].disabled = false;
    //        $("#footerLog").css("color", "black");
    //        $("#footerLog").css("cursor", "pointer");
    //        $("#footerLog").click();
    //        var dataSource = $.parseJSON(val);
    //        var t = $('#RobotLogDetails').DataTable({
    //            destroy: true,
    //            "oLanguage": { "sSearch": "" },
    //            "order": [[0, "desc"]],
    //            "columnDefs": [{
    //                "targets": '_all',
    //                "defaultContent": "",
    //            },
    //             {
    //                 "aTargets": [0],
    //                 "sClass": "hidden"
    //             }, ]
    //        });
    //        t.clear();
    //        for (var i in dataSource) {
    //            t.row.add([
    //       dataSource[i]["Robot_ID"],
    //       dataSource[i]["StepName"],
    //       dataSource[i]["Description"],
    //       dataSource[i]["CreateBy"],
    //            ]);
    //            t.draw(false);
    //        }
    //        t.page('first').draw('page');
    //        $($('#RobotLogDetails_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');


    //if ($("#footertoggle").children().hasClass("fa-window-maximize"))
    //    footertoggleclick();
    //    },
    //    error: function (e) {
    //    }
    //});
}
function historyModalOpen(e) {
    var historybot = $(e).parent().closest('tr').find('td')[0].innerHTML;

    $("#footerHistory")[0].disabled = false;
    $("#footerHistory").css("color", "black");
    $("#footerHistory").css("cursor", "pointer");
    $("#footerHistory").click();
    var t = $('#RobotHistoryDetails').DataTable({
        destroy: true,
        "scrollY": true,
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "paging": true,
        "pageLength": 10,
        "pagingType": "input",
        "word-break": "break-all",
        "lengthChange": false,
        "language": { search: '', searchPlaceholder: "Search..." },

        "ajax":
          {
            "url": GlobalURL +"/api/CrudService/getGridDataByUser?tablename=History_CommandCenter&CreateBy=" + historybot,
              "contentType": "application/json",
              "type": "POST",
              "dataType": "JSON",
            "data": function (d) {
                
                return JSON.stringify(d);
              },
              "dataSrc": function (json) {
                 

                  //json.draw = json.draw;
                  //json.recordsTotal = json.recordsTotal;
                  //json.recordsFiltered = json.recordsFiltered;
                  //json.data = json.data;

                  var return_data = json;
                  //return return_data.data;
                  var dttt = $.parseJSON(return_data.data);
                  //dttt = $.parseJSON(dttt);
                  return dttt;
              }
          },

        "columns": [
                    {
                        "data": "ID",
                        "title": "Id"
                    },
                    {
                        "data": "JobName",
                        "title": "Job Name"
                    },
                    {
                        "data": "JobStatus",
                        "title": "Job Status"
                    },
                    {
                        "data": "SubmittedBy",
                        "title": "Submitted By"
                    },
                    {
                        "data": "StartingTime",
                        "title": "Starting Time"
                    },

                    {
                        "data": "EndingTime",
                        "title": "Ending Time"
                    },


        ],
        "columnDefs": [{
            "targets": '_all',
            "defaultContent": "",
        },
         {
             "aTargets": [0, 1],
             "sClass": "hidden"
         },
        {
            "targets": [0],
            "orderable": true
        },
        ],
    });

    t.page('first').draw('page');
    $(".dataTables_filter").addClass("pull-left");
    if ($("#footertoggle").children().hasClass("fa-window-maximize"))
        footertoggleclick();



    //$.ajax({
    //    type: "GET",
    //    url: '/services/CrudService.svc/getTableDataByUserID?tablename=History_CommandCenter&CreateBy=' + historybot,
    //    contentType: 'application/json; charset=utf-8',
    //    //dataType: 'json',
    //    success: function (val) {
    //        //   ////
    //        $("#footerHistory")[0].disabled = false;
    //        $("#footerHistory").css("color", "black");
    //        $("#footerHistory").css("cursor", "pointer");
    //        $("#footerHistory").click();
    //        var dataSource1 = $.parseJSON(val);
    //        var t1 = $('#RobotHistoryDetails').DataTable({
    //            destroy: true,
    //            "oLanguage": { "sSearch": "" },
    //            "order": [[0, "desc"]],
    //            "columnDefs": [{
    //                "targets": '_all',
    //                "defaultContent": "",
    //            },
    //             {
    //                 "aTargets": [0, 1],
    //                 "sClass": "hidden"
    //             },
    //            ]
    //        });
    //        t1.clear();
    //        for (var i in dataSource1) {
    //            t1.row.add([
    //       parseInt(dataSource1[i]["ID"]),
    //       dataSource1[i]["JobName"],
    //       dataSource1[i]["JobStatus"],
    //       dataSource1[i]["SubmittedBy"],

    //       dataSource1[i]["StartingTime"],
    //       dataSource1[i]["EndingTime"],
    //            ]);
    //            t1.draw(false);
    //        }
    //        t1.page('first').draw('page');
    //        $($('#RobotHistoryDetails_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
    //    },
    //    error: function (e) {
    //    }
    //});
    //if ($("#footertoggle").children().hasClass("fa-window-maximize"))
    //    footertoggleclick();


}


function RobotsModalOpen() {
    
    $('.nvtooltip').remove();
    SystemsLoad();
    LOBLoad();
    $("#RobotsRight-Panel")[0].innerHTML = "";

    $("#Run").removeClass("clicked");
    $("#Schedule").removeClass("clicked");
    $("#Remove").removeClass("clicked");

    $("#Schedule")[0].disabled = true;
    $("#Run")[0].disabled = true;
    $("#Remove")[0].disabled = true;
    $("#Schedule").css("color", "#4084eb");
    $("#Run").css("color", "#4084eb");
    $("#Remove").css("color", "#4084eb");

    $("#myModal1").modal('show');
}
function SystemsModalOpen() {
    //
    $("#RegisterBtn").disabled = true;
    $('.nvtooltip').remove();
    $('.form-control').val(' ');
    $("#addModal").modal('show');
    $('.systemsaddmodal').val('');
    addModalSystems();
}
function addModalSystems() {
    $.ajax({
        type: "GET",
        url: GlobalURL +'/api/CrudService/getSystemsByRobotsCount',
        async: true,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            ////
            awr = 0;
            atr = 0;
            ir = 0;
            var SystemSource = $.parseJSON(val);
            //SystemSource = $.parseJSON(SystemSource);

            $("#activerobots2")[0].innerHTML = "";
            $("#emptyrobots2")[0].innerHTML = "";
            $("#inactiverobots2")[0].innerHTML = "";
            for (var i in SystemSource) {
                if (SystemSource[i].System_Status.trim() == "Active") {
                    if (parseInt(SystemSource[i].RobotsAssigned) > 0) {
                        $("#activerobots2")[0].innerHTML += "<div class='col-sm-4 col-lg-3 dropHere' style='text-align:center;padding-right:6px !important;padding-left:5px !important;' ><div class='SystemImage availableRobot dropSystem' ondrop='drop(event)' ondragover='allowDrop(event)' title='" + SystemSource[i].Name + "' systemId='" + SystemSource[i].ID + "' onclick='SystemClick1(this)'><input type='checkbox' style='display:none'><img class='dropHere systemImages' src='dist/img/Controller/Icons-2/settings (1).png' style='height:35px;width:40px;'/><h6 class='h6Class' >" + SystemSource[i].Name.replace(/\-/g, '&#8209;') + "</h6></div></div>";
                        awr++;
                    }
                    else {
                        $("#emptyrobots2")[0].innerHTML += "<div class='col-sm-4 col-lg-3 dropHere' style='text-align:center;padding-right:6px !important;padding-left:5px !important;' ><div class='SystemImage availableRobot dropSystem' ondrop='drop(event)' ondragover='allowDrop(event)' title='" + SystemSource[i].Name + "' systemId='" + SystemSource[i].ID + "' onclick='SystemClick1(this)'><input type='checkbox' style='display:none'><img class='dropHere systemImages' src='dist/img/Controller/Icons-2/warnining (1).png' style='height:35px;width:40px;'/><h6 class='h6Class' >" + SystemSource[i].Name.replace(/\-/g, '&#8209;') + "</h6></div></div>";
                        atr++;
                    }
                }
                else {
                    $("#inactiverobots2")[0].innerHTML += "<div class='col-sm-4 col-lg-3 dropHere' style='text-align:center;padding-right:6px !important;padding-left:5px !important;' ><div class='SystemImage' title='" + SystemSource[i].Name + "' systemId='" + SystemSource[i].ID + "' onclick='SystemClick1(this)'><img class='dropHere unavailable systemImages' src='dist/img/Controller/Icons-2/system_grey.png' style='height:35px;width:40px;'/><h6 >" + SystemSource[i].Name.replace(/\-/g, '&#8209;') + "</h6></div></div>";
                    ir++;
                }
            }
            $("#countOne")[0].innerHTML = " (" + awr + ")";
            $("#countTwo")[0].innerHTML = " (" + atr + ")";
            $("#countThree")[0].innerHTML = " (" + ir + ")";
            $("#SystemsIcons1").css("height", SystemsHeight, "!important");
        },
        error: function (e) {
        }
    });
}
function activetitleclick2() {
    $(".availableRobot").removeClass('activeSysRobos');
    $("#activerobots2").toggle("2000");
    $("#spanOne").toggleClass("fa-angle-down");
    $("#spanOne").toggleClass("fa-angle-right");
    $("#RemoveSystem").hide();
}
function emptytitleclick2() {
    $("#emptyrobots2").toggle("2000");
    $("#spanTwo").toggleClass("fa-angle-down");
    $("#spanTwo").toggleClass("fa-angle-right");
}
function inactivetitleclick2() {
    $("#inactiverobots2").toggle("2000");
    $("#spanThree").toggleClass("fa-angle-down");
    $("#spanThree").toggleClass("fa-angle-right");
}
var removeSystemid;
var ActiveSystemId;
function SystemClick1(e) {
    ////
    removeSystemid = $(e)[0].attributes["systemid"].nodeValue;
    if ($(e).hasClass('activeSysRobos')) {
        $(e).removeClass('activeSysRobos');
        $("#RemoveSystem").hide();
        $("#RemoveSystem").css("color", "#4084eb");
        $("#RemoveSystem")[0].disabled = true;
    }
    else {
        $('.activeSysRobos').removeClass('activeSysRobos');
        $(e).addClass('activeSysRobos');
        $("#RemoveSystem").css("color", "#4084eb");
        $("#RemoveSystem").show();
        $("#RemoveSystem")[0].disabled = false;
    }
}
function removeSystemClick() {
    
    $.ajax({
        type: "GET",
        url: GlobalURL +'/api/CrudService/getTableDataByUserID?tablename=RemoveSystem_CommandCenter&CreateBy=' + removeSystemid,
        //data: Htmlname ,
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json', 
        success: function (val) {
            ////
            if (val == "true") {
                swal("", "System Removed Successfully", "Success", { closeOnClickOutside: false });  
            }
            SystemsModalOpen();
        },
        error: function (e) {
        }
    });
}
function InboxRejectClick(e) {
   
    RobotID1 = {};
    RobotID1["ID"] = $(e).parent().closest('tr').find('td')[0].innerHTML;
    var RobotID = JSON.stringify(RobotID1);
    Values = {
        "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
        //  "UpdateDatetime": window.btoa(GetNow())
    };
    Values["MsgStatus"] = window.btoa("Rejected");
    var values = JSON.stringify(Values);
    $.ajax({
        type: "POST",
        url: GlobalURL + '/api/CrudService/UpdateTableRow?tablename=Inbox&ID='+ RobotID,
        async: false,
        data: JSON.stringify(values),
       
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',
        success: function (val) {
            if (val.updateTableRowResult == "true") {
                swal("", "Rejected Successfully", "success", { closeOnClickOutside: false });  
            }
            LoadInbox();
        },
        error: function (e) {
        }
    });
}
var BotId = {};
var CurrentOutput = "";
var ActionType = "";
function InboxAcceptClick(e) {
   
    $("#AcceptSubmitForm").removeClass("col-sm-4");
    $("#AcceptSubmitForm").addClass("col-sm-12");
    $("#TotalAcceptModal div.col-sm-8").remove();
    $("#TotalAcceptModal img").remove();
    $("#TotalAcceptModal iframe").remove();
    $("#TotalAcceptModal embed").remove();
    $("#TotalAcceptModal .file_image").remove();
    $("#AcceptModal .modal-dialog").width("600px")
    $("#AcceptModal").modal('show');
    //$("#TitleLabel")[0].innerHTML = "";
    $("#DisplayNameLabel")[0].innerHTML = "";
    $("#RobotLabel")[0].innerHTML = $(e).parent().closest('tr').find('td')[1].innerHTML;
    $("#IDLabel")[0].innerHTML = $(e).parent().closest('tr').find('td')[0].innerHTML;
    $("#StepLabel")[0].innerHTML = $(e).parent().closest('tr').find('td')[2].innerHTML;
    stepID = $(e).parent().closest('tr').find('td')[3].innerHTML;
    BotId["ID"] = $(e).parent().closest('tr').find('td')[0].innerHTML;
    CurrentOutput = $(e).parent().closest('tr').find('td')[6].innerHTML;
    ActionType = $(e).parent().closest('tr').find('td')[7].innerHTML;
    InboxResult = $.parseJSON(CurrentOutput);

    StepsdataSource = InboxResult["runtimerequest"]["step"]["StepProperties"];
    //for (var i in StepsdataSource) {
    var titleStep = StepsdataSource.find(stepdataSource => stepdataSource.StepProperty.toLowerCase() == "title");
    $(".modal-title")[0].innerHTML = titleStep.StepPropertyValue;
    if (InboxResult["runtimerequest"]["step"]["Name"] == "Input") {
        $("#io_Action").removeClass('display-none');
        $('#o_Action').addClass('display-none');
        var widgettypeStep = StepsdataSource.find(stepdataSource => stepdataSource.StepProperty.toLowerCase() == "widgettype");
        var widgetTypes = widgettypeStep.StepPropertyValue.split(",");
        var displayMessageStep = StepsdataSource.find(stepdataSource => stepdataSource.StepProperty.toLowerCase() == "displaymessage");
        var displayMessages = displayMessageStep.StepPropertyValue.split(",");
        var variableStep = StepsdataSource.find(stepdataSource => stepdataSource.StepProperty.toLowerCase() == "variable");
        var variables = variableStep.StepPropertyValue.split(",");
        $("#io_Action").html("")
        for (var widgetIndex in widgetTypes) {
            if (widgetTypes[widgetIndex].trim().toLowerCase() == "textbox")
                $("#io_Action")[0].innerHTML += "<div class=\"col-sm-12\"><div class=\"col-sm-12\"><label class=\"control-label\" name=\"DisplayNameLabel\">" + displayMessages[widgetIndex] + "</label></div><div class='col-sm-12'><input type=text name=InputVariableValue class=form-control varaible='" + variables[widgetIndex] + "'/></div></div>";
            else if (widgetTypes[widgetIndex].trim().toLowerCase() == "textarea")
                $("#io_Action")[0].innerHTML += "<div class=\"col-sm-12\"><div class=\"col-sm-12\"><label class=\"control-label\" name=\"DisplayNameLabel\">" + displayMessages[widgetIndex] + "</label></div><div class='col-sm-12'><textarea rows=10 name=InputVariableValue class=form-control varaible='" + variables[widgetIndex] + "'></textarea></div></div>";
            else if (widgetTypes[widgetIndex].trim().toLowerCase() == "checkbox")
                $("#io_Action")[0].innerHTML += "<div class=\"col-sm-12\"><div class='col-sm-1'><input type=checkbox name=InputVariableValue varaible='" + variables[widgetIndex] + "'/></div><label class=\"control-label col-sm-10\" name=\"DisplayNameLabel\">" + displayMessages[widgetIndex] + "</label></div>";
        }
        var imagevariableStep = StepsdataSource.find(stepdataSource => stepdataSource.StepProperty.toLowerCase() == "imagevariable");
        if (imagevariableStep.StepPropertyValue.trim() != "" && imagevariableStep.StepPropertyValue != null) {
            var DVariables;
            var sourcepathImage;
            var imageSource;
            var img_extension;
            try {
                DVariables = InboxResult["runtimerequest"]["dvariables"];
                ImageV = StepsdataSource.find(s => s.StepProperty == "ImageVariable");
                sourcepathImage = DVariables.find(d => d.vlname == ImageV.StepPropertyValue);
                if (typeof (sourcepathImage.vlvalue[0]["val"]) != typeof (undefined)) {
                    imageSource = JSON.parse(sourcepathImage.vlvalue[0]["val"])[JSON.parse(sourcepathImage.vlvalue[0]["val"]).length - 1]["Files"].split("Uploads\\")[1];
                }
                else {
                    if (typeof (JSON.parse(sourcepathImage.vlvalue)[0]["Files"]) != typeof (undefined)) {
                        imageSource = JSON.parse(sourcepathImage.vlvalue)[JSON.parse(sourcepathImage.vlvalue).length - 1]["Files"].split("Uploads\\")[1];
                    }
                    else {
                        imageSource = JSON.parse(sourcepathImage.vlvalue)[JSON.parse(sourcepathImage.vlvalue).length - 1][ImageV.StepPropertyValue].split("Uploads\\")[1];
                    }
                }

                img_extension = imageSource.split(".")[1];
                if (img_extension.toLowerCase() == "png" || img_extension.toLowerCase() == "jpg" || img_extension.toLowerCase() == "jpeg") {
                    $("#TotalAcceptModal").prepend("<div class=\"col-sm-8 text-center\" style = \"height: 100%; width: 98%;\"><img style = \"height: inherit; width: inherit;\" src='" + GlobalURL + '/api/CrudService/Download?fileName=' + imageSource + "&foldername=Uploads'/></div>");
                    $("#AcceptSubmitForm").removeClass("col-sm-12");
                    $("#AcceptSubmitForm").addClass("col-sm-4");
                    $("#AcceptModal .modal-dialog").width("1200px");

                }
                else if (img_extension.toLowerCase() == "pdf") {
                    $("#TotalAcceptModal").prepend("<div class='col-sm-8 text-center'><a href='" + GlobalURL + '/api/CrudService/Download?fileName=' + imageSource + "&foldername=Uploads' download><img src=\"/Views/Images/pdf.png\" /><p style=\"color: #c74343;\">Download</p></a></div>");
                    // $("#io_Action").html("<div class='col-sm-12'><input class='col-sm-6 form-control' type=text id=InputVariableValue /></div>");
                    $("#AcceptSubmitForm").removeClass("col-sm-12");
                    $("#AcceptSubmitForm").addClass("col-sm-4");
                    $("#AcceptModal .modal-dialog").width("1200px");
                }
                else
                    $("#TotalAcceptModal").prepend("<a class='file_image col-sm-12' target='_blank' href='/Uploads/" + imageSource + "'><i class='fa fa-download'><label class=control-label>" + imageSource + "</label></i></a>");
                //$("#io_Action").html("<div class='col-sm-6'><a target='_blank' href='/Uploads/" + imageSource + "'><i class='fa fa-download'></i></a><input type=text id=InputVariableValue class=form-control /></div>");
            }
            catch (err) {
                // $("#io_Action").html("<div class='col-sm-12'><img class='col-sm-6' alt='No Image Available' src='/Uploads/" + imageSource + "'/><input class='col-sm-6 form-control' type=text id=InputVariableValue /></div>");
            }
        }
    }
    if (InboxResult["runtimerequest"]["step"]["Name"] == "Output") {
        for (var i in StepsdataSource) {
        if (StepsdataSource[i]["StepProperty"] == "Value") {
            if (InboxResult["runtimerequest"]["step"]["Name"] == "Output") {
                $("#io_Action").removeClass('display-none');
                $('#o_Action').addClass('display-none');
                $("#io_Action").html("")
                var imagevariableStep = StepsdataSource.find(stepdataSource => stepdataSource.StepProperty.toLowerCase() == "value");
                if (imagevariableStep.StepPropertyValue.trim() != "" && imagevariableStep.StepPropertyValue != null) {
                    var DVariables;
                    var sourcepathImage;
                    var imageSource;
                    var img_extension;
                    try {
                        DVariables = InboxResult["runtimerequest"]["dvariables"];
                        ImageV = StepsdataSource.find(s => s.StepProperty == "Value");
                        sourcepathImage = DVariables.find(d => d.vlname == ImageV.StepPropertyValue);
                        if (typeof (sourcepathImage.vlvalue[0]["val"]) != typeof (undefined)) {
                            imageSource = JSON.parse(sourcepathImage.vlvalue[0]["val"])[JSON.parse(sourcepathImage.vlvalue[0]["val"]).length - 1]["Files"].split("Uploads\\")[1];
                        }
                        else {
                            if (typeof (JSON.parse(sourcepathImage.vlvalue)[0]["Files"]) != typeof (undefined)) {
                                imageSource = JSON.parse(sourcepathImage.vlvalue)[JSON.parse(sourcepathImage.vlvalue).length - 1]["Files"].split("Uploads\\")[1];
                            }
                            else {
                                imageSource = JSON.parse(sourcepathImage.vlvalue)[JSON.parse(sourcepathImage.vlvalue).length - 1][ImageV.StepPropertyValue].split("Uploads\\")[1];
                            }
                        }

                        img_extension = imageSource.split(".")[1];
                        if (img_extension.toLowerCase() == "png" || img_extension.toLowerCase() == "jpg" || img_extension.toLowerCase() == "jpeg") {
                            $("#TotalAcceptModal").prepend("<div class=\"col-sm-8 text-center\" style = \"height: 100%; width: 98%;\"><img style = \"height: inherit; width: inherit;\" src='" + GlobalURL + '/api/CrudService/Download?fileName=' + imageSource + "&foldername=Uploads'/></div>");
                            $("#AcceptSubmitForm").removeClass("col-sm-12");
                            $("#AcceptSubmitForm").addClass("col-sm-4");
                            $("#AcceptModal .modal-dialog").width("1200px");

                        }
                        else if (img_extension.toLowerCase() == "pdf") {
                            $("#TotalAcceptModal").prepend("<div class='col-sm-8 text-center'><a href='" + GlobalURL + '/api/CrudService/Download?fileName=' + imageSource + "&foldername=Uploads' download><img src=\"/Views/Images/pdf.png\" /><p style=\"color: #c74343;\">Download</p></a></div>");
                            // $("#io_Action").html("<div class='col-sm-12'><input class='col-sm-6 form-control' type=text id=InputVariableValue /></div>");
                            $("#AcceptSubmitForm").removeClass("col-sm-12");
                            $("#AcceptSubmitForm").addClass("col-sm-4");
                            $("#AcceptModal .modal-dialog").width("1200px");
                        }
                        else
                            $("#TotalAcceptModal").prepend("<a class='file_image col-sm-12' target='_blank' href='/Uploads/" + imageSource + "'><i class='fa fa-download'><label class=control-label>" + imageSource + "</label></i></a>");
                        //$("#io_Action").html("<div class='col-sm-6'><a target='_blank' href='/Uploads/" + imageSource + "'><i class='fa fa-download'></i></a><input type=text id=InputVariableValue class=form-control /></div>");
                    }
                    catch (err) {
                        // $("#io_Action").html("<div class='col-sm-12'><img class='col-sm-6' alt='No Image Available' src='/Uploads/" + imageSource + "'/><input class='col-sm-6 form-control' type=text id=InputVariableValue /></div>");
                    }
                }
            }
        }
        }
        $("#StepPropertiesDetails").hide();
        $("#DetailsDiv").hide();
    }

}
function elog(ResultStringS) {
    if (ResultStringS.elog != null) {
        for (i in ResultStringS.elog) {
            $("#savedModel")[0].innerHTML += "\n>'" + ResultStringS.elog[i]["StepName"] + "' " + ResultStringS.elog[i]["Description"];
        }
    }
}
function datat(vlvalue, vlname) {
    vlname = "Input" + vlname;
    var vardict = {};
    vardict[vlname] = vlvalue
    var myData = [];
    myData.push(vardict);

    return myData;
}
$(document).ready(function () {   

    $("#AcceptSubmitForm").on("submit", function (e) {
        e.preventDefault();
        var data = {};
        
        var varIndex;
        var previousOutput = JSON.parse(CurrentOutput);
        variableIndex = previousOutput.runtimerequest.step.StepProperties.find(i => i.StepProperty == "Variable")
        var DVariables = previousOutput.runtimerequest.dvariables;
        if (previousOutput.runtimerequest.step.Name == "Input") {
            var InputWidgets = $("[name='InputVariableValue']");
            for (var iw = 0; iw < InputWidgets.length; iw++) {
                varIndex = DVariables.findIndex(dv => dv.vlname == InputWidgets[iw].attributes.varaible.nodeValue);
                if (varIndex >= 0) {
                    if ($("[name='InputVariableValue']")[iw].type == "checkbox")
                        DVariables[varIndex].vlvalue = JSON.stringify(datat(InputWidgets[iw].checked, DVariables[varIndex].vlname));
                    else
                        DVariables[varIndex].vlvalue = JSON.stringify(datat(InputWidgets[iw].value, DVariables[varIndex].vlname));
                    DVariables[varIndex].vlstatus = true;
                }
                else {
                    var dvb = {};
                    dvb["vlname"] = InputWidgets[iw].attributes.varaible.nodeValue;
                    if ($("[name='InputVariableValue']")[iw].type == "checkbox")
                        //dvb["vlvalue"] = JSON.stringify(datat($("[name='InputVariableValue']")[iw].checked, dvb["vlname"]));
                        dvb["vlvalue"] = $("[name='InputVariableValue']")[iw].checked;
                    else
                        //dvb["vlvalue"] = JSON.stringify(datat($("[name='InputVariableValue']")[iw].value, dvb["vlname"]));
                        dvb["vlvalue"] = $("[name='InputVariableValue']")[iw].value;
                    dvb["executionID"] = previousOutput.executionID;
                    dvb["vlstatus"] = true;
                    DVariables.push(dvb);
                    
                }
            }



        }
        if (DVariables == null)
            data["dynamicvariables"] = [];
        else
            data["dynamicvariables"] = DVariables;
        data["Type"] = "ExecutionInProgress";
        data["ID"] = previousOutput["RobotId"]
        data["stepvalue"] = previousOutput.runtimerequest.nextStepID;
        data["systemId_pm"] = previousOutput.SystemId;
        var dataString = JSON.stringify(data);
        


        Values = {
            "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
            //  "UpdateDatetime": window.btoa(GetNow())
        };
        Values["MsgStatus"] = window.btoa("Approved");
        previousOutput.runtimerequest.dvariables = DVariables;
        //Values["CurrentOutput"] = window.btoa(JSON.stringify(previousOutput));
        Values["CurrentOutput"] = window.btoa(escape(JSON.stringify(previousOutput)));
        var values = JSON.stringify(Values);
        var BotID = JSON.stringify(BotId);
        $('#AcceptModal').modal('hide');
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/UpdateTableRow?tablename=Inbox' + '&ID=' + BotID,
            async: false,
            data: JSON.stringify(values),
            contentType: 'application/json; charset=utf-8',
            success: function (val) {
                LoadInbox();
                if (val == "true") {
                    swal("", "Approved Successfully", "success", { closeOnClickOutside: false });  
                }
            },
            error: function (e) {
            }
        });

        if (previousOutput.requestFrom != "Client") {

            $.ajax({
                type: 'POST',
                url: GlobalURL + '/api/GenbaseStudio/Set',
                contentType: 'application/json; charset=utf-8',
                data: dataString,
                success: function (data2) {
                    JSONLog = JSON.parse(data2.setResult)
                    //JSONLog = JSON.parse(JSONLog)
                    if (JSON.parse(JSONLog.ResultString).SuccessState != "" && JSON.parse(JSONLog.ResultString).SuccessState != null) {
                        var ResultStringS = JSON.parse(JSONLog.ResultString);
                        if (ResultStringS.SuccessState.toLowerCase() == "success") {
                            var error;
                            swal("", "Robot Executed Succesfully", "success", { closeOnClickOutside: false });  
                            // elog(ResultStringS);
                            //  $("#savedModel")[0].innerHTML += "\n>Robot '" + rsbotName + "' Executed Successfully";
                        }
                        else if (ResultStringS.SuccessState.toLowerCase() == "failure") {
                            // $("#savedModel")[0].innerHTML += "\n>Robot '" + rsbotName + "' Execution Stopped. Please check Steps and its properties and try again.";
                            swal("", "Execution Stopped. Please check Steps and its properties and try again", "error");
                        }

                    }
                    else {
                        //  $("#savedModel")[0].innerHTML += "\n>Robot '" + rsbotName + "' Execution Stopped.Please check Steps and its properties and try again.";
                        swal("", "Execution Stopped. Please check Steps and its properties and try again", "error");
                    }
                },
                error: function (e) {

                }
            });
        }
    });
});
$(function () {
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();

    if (month < 10)
        month = '0' + month.toString();
    if (day < 10)
        day = '0' + day.toString();

    var minDate = year + '-' + month + '-' + day;
    $('#datetimepicker').attr('min', minDate);

});
$(document).ready(function () {
    setInterval(function () {
        if (localStorage.isloggedin == 'Y') {
            //location.href = "../index.html";
        }
    }, 1000 * 1);
    if (sessionStorage.getItem("isLoadingState") != 'Y' || sessionStorage.getItem("isLoadingState") == null) {
       // location.href = "../index.html";
      
    }
    else {
    }
});

//Generate some nice data.
//function ActiveServerDetails() {
//    
//    $("#ServerHistoryDetails1").hide();
//    $("#ServerDetailsPopuoRobot").show();
//    $("#ServerDetailsPopuoSystem").show();
//    $("#ServerDetailsModal").modal('show');
//    
//    $.ajax({
//        type: "POST",
//        url: GlobalURL +'/api/CrudService/getGridData?tablename=ConnectivityGraph_Systems',
//        async: true,
//        contentType: 'application/json; charset=utf-8',
//        success: function (result) {
//            
//            var dataSource1 = $.parseJSON(result);
//            //dataSource1 = $.parseJSON(dataSource1);
//            
//            var t1 = $('#ServerHistory').DataTable({
//                destroy: true,
//                "scrollY": true,
//                "scrollX": true,
//                "pagingType": "input",
//                "oLanguage": { "sSearch": "" },
//                "columnDefs": [{
//                    "targets": '_all',
//                    "defaultContent": "",
//                },
//                 {
//                     "aTargets": [0],
//                     "sClass": "hidden"
//                 }, ]
//            });
//            t1.clear();
//            
//            var obj = JSON.parse(dataSource1.data);
//            for (var i in obj) {
//                t1.row.add([
//                    obj[i]["ID"],
//                    obj[i]["Name"],
//                    obj[i]["IP_Address"],
//                    obj[i]["servername"],
//                ]);
//                t1.draw(false);
//            }
//            t1.page('first').draw('page');
//            $($('#ServerHistory_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
//        },
//        error: function (e) {
//        }
//    });
//    $.ajax({
//        type: "POST",
//        url: GlobalURL +'/api/CrudService/getGridData?tablename=ConnectivityGraph_Robots',
//        async: true,
//        contentType: 'application/json; charset=utf-8',
//        success: function (result) {
//            
//            var dataSource1 = $.parseJSON(result);
//            //dataSource1 = $.parseJSON(dataSource1);
            
//            var t1 = $('#DataBaseDetails').DataTable({
//                destroy: true,
//                "scrollY": true,
//                "scrollX": true,
//                "pagingType": "input",
//                "word-break": "break-all",
//                "oLanguage": { "sSearch": "" },
//                "columnDefs": [{
//                    "targets": '_all',
//                    "defaultContent": "",
//                }, ]
//            });
//            
//            t1.clear();
//            //for (var i in dataSource1) {
//            var obj = JSON.parse(dataSource1.data);
//            for (var i in obj) {
//                i = parseInt(i);
//                t1.row.add([
//                    obj[i]["Id"],
//                    obj[i]["Name"],
//                    obj[i]["systemname"],
//                    obj[i]["servername"],
//                ]);
//                t1.draw(false);
//            }
//            t1.page('first').draw('page');
//            $($('#DataBaseDetails_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
//        },
//        error: function (e) {
//        }
//    });
//}
function ActiveServerDetails(e) {
    $("#ServerHistoryDetails1").hide();
    $("#ServerDetailsPopuoRobot").show();
    $("#ServerDetailsPopuoSystem").hide();
    $("#ServerDetailsModal").modal('show');
    var conn_id = e.parentElement.parentElement.getElementsByClassName("hidden sorting_1")[0].textContent;
    var conn_name = e.parentElement.parentElement.getElementsByTagName("td")[1].textContent;
    var host_name = e.parentElement.parentElement.getElementsByTagName("td")[2].textContent;
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getConnectivity_Info?tablename=ConnectivityInfo&Name=' + conn_name + '&Server=' + host_name + '&Conn_ID=' + conn_id,
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            
            var dataSource1 = $.parseJSON(result);
            var t1 = $('#DataBaseDetails').DataTable({
                destroy: true,
                "scrollY": true,
                "scrollX": true,
                "pagingType": "input",
                "word-break": "break-all",
                "oLanguage": { "sSearch": "" },
                "columnDefs": [{
                    "targets": '_all',
                    "defaultContent": "",
                },]
            });
            t1.clear();
            for (var i in dataSource1) {
                t1.row.add([
                    //dataSource1[i]["ID"],
                    dataSource1[i]["Name"],
                    dataSource1[i]["Server"],
                    dataSource1[i]["Connection_Status"],
                    dataSource1[i]["CreateDatetime"],
                ]);
                t1.draw(false);
            }
            t1.page('first').draw('page');
            $($('#DataBaseDetails_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
        },
        error: function (e) {
        }
    });
    //$.ajax({
    //    type: "GET",
    //    url: GlobalURL + '/api/CrudService/gettablebyId?tablename=ConnectivityGraph_Systems' + '&id=' + sysId,
    //    async: true,
    //    contentType: 'application/json; charset=utf-8',
    //    success: function (result) {
            
    //        var dataSource1 = $.parseJSON(result);
    //        var t1 = $('#ServerHistory').DataTable({
    //            destroy: true,
    //            "scrollY": true,
    //            "scrollX": true,
    //            "pagingType": "input",
    //            "oLanguage": { "sSearch": "" },
    //            "columnDefs": [{
    //                "targets": '_all',
    //                "defaultContent": "",
    //            },
    //            {
    //                "aTargets": [0],
    //                "sClass": "hidden"
    //            },]
    //        });
    //        t1.clear();
    //        for (var i in dataSource1) {
    //            t1.row.add([
    //                dataSource1[i]["ID"],
    //                dataSource1[i]["Name"],
    //                dataSource1[i]["IP_Address"],
    //                dataSource1[i]["servername"],
    //            ]);
    //            t1.draw(false);
    //        }
    //        t1.page('first').draw('page');
    //        $($('#ServerHistory_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
    //    },
    //    error: function (e) {
    //    }
    //});
    //$.ajax({
    //    type: "GET",
    //    url: GlobalURL + '/api/CrudService/getTableData?tablename=ConnectivityGraph_Robots',
    //    async: true,
    //    contentType: 'application/json; charset=utf-8',
    //    success: function (result) {
            
    //        var dataSource1 = $.parseJSON(result);
    //        var t1 = $('#DataBaseDetails').DataTable({
    //            destroy: true,
    //            "scrollY": true,
    //            "scrollX": true,
    //            "pagingType": "input",
    //            "word-break": "break-all",
    //            "oLanguage": { "sSearch": "" },
    //            "columnDefs": [{
    //                "targets": '_all',
    //                "defaultContent": "",
    //            },]
    //        });
    //        t1.clear();
    //        for (var i in dataSource1) {
    //            t1.row.add([
    //                dataSource1[i]["Id"],
    //                dataSource1[i]["Name"],
    //                dataSource1[i]["systemname"],
    //                dataSource1[i]["servername"],
    //            ]);
    //            t1.draw(false);
    //        }
    //        t1.page('first').draw('page');
    //        $($('#DataBaseDetails_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
    //    },
    //    error: function (e) {
    //    }
    //});
}
function FailedServerDetails() {
    
    $("#ServerDetailsPopuoRobot").hide();
    $("#ServerDetailsPopuoSystem").hide();
    $("#ServerHistoryDetails1").show();
    $("#ServerDetailsModal").modal('show');
    $.ajax({
        type: "POST",
        url: GlobalURL +'/api/CrudService/getGridData?tablename=ConnectivityInfo',
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            var dataSource1 = $.parseJSON(result);
            //dataSource1 = $.parseJSON(dataSource1);

            var t1 = $('#ServerHistoryDetails').DataTable({
                destroy: true,
                "scrollY": true,
                "scrollX": true,
                "pagingType": "input",
                "oLanguage": { "sSearch": "" },
                "columnDefs": [{
                    "targets": '_all',
                    "defaultContent": "",
                },
                 {
                     "aTargets": [0],
                     "sClass": "hidden"
                 }, ]
            });
            t1.clear();
            for (var i in dataSource1) {
                t1.row.add([
           dataSource1[i]["Name"],
           dataSource1[i]["Server"],
           dataSource1[i]["port"],
           dataSource1[i]["createdatetime"],
                ]);
                t1.draw(false);
            }
            t1.page('first').draw('page');
            $($('#ServerHistoryDetails_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
        },
        error: function (e) {
        }
    });
}
var ConncetivityGraphColor;

function Connection_HealthCheck() {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/connectionHealthCheck?tablename=ConnectionConfig',
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {

            var dataSource1 = $.parseJSON(result);
            for (var i in dataSource1) {
                //if (result != "[]" || result != "undefined") {
                //    var return_result = JSON.parse(result);
                //    dttt[i].Status = return_result[0]._conn_status;
                //}
                //if (dataSource1[i].connectiontype == "Database") {
                //    if (dataSource1[i].authenticationtype == "postgres") { }
                //    else if (dataSource1[i].authenticationtype == "mysql") { }
                //}
                //else if (dataSource1[i].connectiontype == "Server") {
                //    if (dataSource1[i].authenticationtype == "linux") { }
                //    else if (dataSource1[i].authenticationtype == "windows") { }
                //}
                //else if (dataSource1[i].connectiontype == "API") { }
                //else { }
            }
            
        },
        error: function (e) {
        }
    });

}

function ConnectivityDB() {

    $("#dvLoadingConn").hide();
    var t = $('#ConnectivityTable').DataTable({
        "destroy": true,
        "scrollY": 174,
        "processing": true,
        "serverSide": true,
        "paging": true,
        "pagingType": "input",
        "pageLength": 8,
        "word-break": "break-all",
        "lengthChange": false,
        "language": { search: '', searchPlaceholder: "Search..." },

        "ajax":
          {
              //"url": "/services/CrudService.svc/getGridData?tablename=DBConnection_CommandCenter",
            "url": GlobalURL +"/api/CrudService/getGridData?tablename=ConnectivityInfo",
              "contentType": "application/json",
              "type": "POST",
              "dataType": "JSON",
            "data": function (d) {
                
                return JSON.stringify(d);
              },
              "dataSrc": function (json) {
                  //

                  //json.draw = json.draw;
                  //json.recordsTotal = json.recordsTotal;
                  //json.recordsFiltered = json.recordsFiltered;
                  //json.data = json.data;

                  var return_data = json;
                  //return return_data.data;
                  var dttt = $.parseJSON(return_data.data);
                  return dttt;
              }
          },
        "columns": [
                    {
                        "data": "ID",
                        "title": "Id"
                    },
                    {
                        "data": "Name",
                        "title": "Name"
                    },
                    {
                        "data": "Server",
                        "title": "Server"
                    },
                    {
                        "data": "Status",
                        "title": "Status",
                    },


        ],
        "columnDefs": [
        //    {
        //    "targets": '_all',
        //    "defaultContent": "",
        //},
                 {
                     "aTargets": [0],
                     "sClass": "hidden"
                 },
                 {
                     "targets": [0],
                     "orderable": true
                 },
                 {
                     "targets": [3],
                     render: function (data, type, row) {
                         //
                         return row.Status == '1' ? "<button class='bton success' title='Click to see the details' onclick='ActiveServerDetails(this)'><img src='dist/img/greenStatus.png' style='width:50%;height:50%;'/></button>" : "<button class='bton success' title='Click to see the details' onclick='ActiveServerDetails(this)'><img src='dist/img/redStatus.png' style='width:50%;height:50%;'/></button>"
                     }
                 },
        ],

    });

    t.page('first').draw('page');
    $(".dataTables_filter").addClass("pull-left");


    //
    //$.ajax({
    //    type: "GET",
    //    url: '/services/CrudService.svc/getTableData?tablename=DBConnection_CommandCenter',
    //    async: true,
    //    contentType: 'application/json; charset=utf-8',
    //    success: function (result) {
    //        $("#dvLoadingConn").hide();
    //        var dataSource = $.parseJSON(result);
    //        var t = $('#ConnectivityTable').DataTable({
    //            destroy: true,
    //            "scrollX": false,
    //            "scrollY": 150,
    //            "oLanguage": { "sSearch": "" },
    //            "order": [[0, 'desc']],
    //            "columnDefs": [{
    //                "targets": '_all',
    //                "defaultContent": "",
    //            },
    //             {
    //                 "aTargets": [0, 3, 4, 5, 6, 7],
    //                 "sClass": "hidden"
    //             },

    //            ]
    //        });
    //        t.clear();
    //        for (var i = 0; i < dataSource[0].length; i++) {
    //            var dataS = dataSource[0][i].split(",");
    //            var status = ""; var clickStatus = "";
    //            if (dataS[3] == "1") {
    //                status = "dist/img/greenStatus.png";
    //                clickStatus = "ActiveServerDetails()";
    //            }
    //            else {
    //                status = "dist/img/redStatus.png";
    //                clickStatus = "FailedServerDetails()";
    //            }
    //            t.row.add([
    //       dataS[0],
    //       dataS[2],
    //       dataS[1],
    //       dataS[0],
    //       dataS[0],
    //       dataS[0],
    //       dataS[0],
    //       dataS[0],
    //       "<button class='bton success' title='Click to see the details' onclick='" + clickStatus + "'><img src='" + status + "' style='width:70%;height:70%;'/></button>",
    //            ]);
    //            t.draw(false);

    //            t.page('first').draw('page');
    //            $($('#InboxTable_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
    //        }
    //    },
    //    error: function (e) {
    //        ConnectivityDB();
    //    }
    //});


}
function ConnectivityDB1() {
    
    nv.addGraph(function () {
        var chart = nv.models.multiBarHorizontalChart()
        .x(function (d) { return d.label })
            .y(function (d) { return d.value })
        .height(200)
        chart.stacked(true);
        d3.select('#DBchart svg')
            .datum(exampleData())
          .transition().duration(500).call(chart)

        d3.select(".nv-controlsWrap").style("visibility", "hidden")
        nv.utils.windowResize(chart.update);

        chart.multibar.dispatch.on('elementClick', function (e) {
            if (e.color == "rgb(0, 128, 0)") {
                ActiveServerDetails(e);
                $("#ServerHistoryDetails1").hide();
                $("#ServerDetailsPopuoRobot").show();
                $("#ServerDetailsPopuoSystem").show();
                $("#ServerDetailsModal").modal('show');
            }
            else if (e.color == "rgb(255, 0, 0)") {
                FailedServerDetails();
                $("#ServerDetailsPopuoRobot").hide();
                $("#ServerDetailsPopuoSystem").hide();
                $("#ServerHistoryDetails1").show();
                $("#ServerDetailsModal").modal('show');
            }
        });

        return chart;
    });
    $.ajax({
        type: "POST",
        url: GlobalURL +'/api/CrudService/getGridData?tablename=DBConnection_CommandCenter',
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            //
            $("#dvLoading").hide();
            var increment = 0;
            if (result != "") {
                ConnectivityData = $.parseJSON(result);

                $.each(ConnectivityData, function (key, val) {
                    var keys = [];
                    var a = [];
                    a.push(key);
                    increment++;
                    keys.push('Connection');
                    for (var i in val) {
                        $.each(val[i], function (key1, val1) {
                            keys.push(key1);
                            a.push(parseInt(val1));
                        })
                    }
                    if (increment == 1)
                        Cdata.push(keys);

                    Cdata.push(a);

                })
            }
        },
        error: function (e) {
            //ConnectivityDB();
            LoadInbox();
        }
    });
}
function exampleData() {
    //
    return [
{
    "key": "Localhost",
    "color": "green",
    "values": [
      {
          "label": "Database",
          "value": 1.8746444827653
      },
      {
          "label": "Services",
          "value": 8.0961543492239
      },
      {
          "label": "Systems",
          "value": 0.57072943117674
      },
      {
          "label": "Robots",
          "value": 2.4174010336624
      },
    ]
},
{
    "key": "10.11.12.02",
    "color": "green",
    "values": [
      {
          "label": "Database",
          "value": 2.307646510375
      },
      {
          "label": "Services",
          "value": 16.756779544553
      },
      {
          "label": "Systems",
          "value": 18.451534877007
      },
      {
          "label": "Robots",
          "value": 8.6142352811805
      },
    ]
},
{
    "key": "203.129.196.90",
    "color": "#B71C1C",
    "values": [
      {
          "label": "Database",
          "value": 11.8746444827653
      },
      {
          "label": "Services",
          "value": 18.0961543492239
      },
      {
          "label": "Systems",
          "value": 10.57072943117674
      },
      {
          "label": "Robots",
          "value": 12.4174010336624
      },
    ]
},
{
    "key": "203.169.196",
    "color": "green",
    "values": [
      {
          "label": "Database",
          "value": 11.8746444827653
      },
      {
          "label": "Services",
          "value": 18.0961543492239
      },
      {
          "label": "Systems",
          "value": 10.57072943117674
      },
      {
          "label": "Robots",
          "value": 12.4174010336624
      },
    ]
},
{
    "key": "Oracle",
    "color": "#B71C1C",
    "values": [
      {
          "label": "Database",
          "value": 12.8746444827653
      },
      {
          "label": "Services",
          "value": 7.0961543492239
      },
      {
          "label": "Systems",
          "value": 6.57072943117674
      },
      {
          "label": "Robots",
          "value": 13.4174010336624
      },
    ]
},
{
    "key": "ap.Genbase",
    "color": "green",
    "values": [
      {
          "label": "Database",
          "value": 7.8746444827653
      },
      {
          "label": "Services",
          "value": 10.0961543492239
      },
      {
          "label": "Systems",
          "value": 12.57072943117674
      },
      {
          "label": "Robots",
          "value": 15.4174010336624
      },
    ]
},
    ]
}
var SystemValueList = [];
var SystemNotActive = [];
var SystemInprogress = [];
var SystemActive = [];
var SystemAvailable = [];

var ValueList = [];
var RobotNewValues = [];
var RobotInprogressvalues = [];
var RobotSuccessValues = [];
var RobotFailureValues = [];

function TimeSeries() {
    
    if ($("#RobotsTimeline").css("display") == "block") {
        RobotsTimeLine();
    }
    else {
        SystemsTimeLine();
    }
}
var LastValueOfArray;
function RobotsTimeLine() {

    $("#dvLoadingRobots").hide();

    nv.addGraph(function () {
        var chart = nv.models.stackedAreaChart()
                      .x(function (d) { return d[0] })
                      .y(function (d) { return d[1] })
                      .height(200)
                      .useInteractiveGuideline(true)
                      .showControls(true)
                      .clipEdge(true)
                  
        chart.xAxis.tickFormat(function ()
        {
            debugger;
            var tomorrow = new Date();
            return d3.time.format('%d/%m/%y')(tomorrow)
        });
        //chart.xAxis.tickFormat(d3.timeHour);
        chart.yAxis.tickFormat(d3.format(',.2f'));
        d3.select('#chartContainer svg').datum(exampleData2).call(chart);
          $('.nvtooltip').remove();
        nv.utils.windowResize(chart.update);

        return chart;
    });


    $.ajax({
        type: "GET",
        url: GlobalURL +'/api/CrudService/getTableDataByUserID?tablename=ProcessManager_CommandCenter&CreateBy=' + $("#TimeDropdown").val(),
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            $("#dvLoadingRobots").hide();
            result = result.replace("][", ",");
            ValueList = $.parseJSON(result);
            //ValueList = $.parseJSON(ValueList);
            //&& ValueList.length != 0
            if (ValueList != null && typeof (ValueList) != typeof (undefined) ) {
                //console.log(ValueList);
                //console.log(ValueList.length);
                //LastValueOfArray = parseInt(ValueList[ValueList.length - 1].CreateDatetime.replace(/([\/\(\)Date])/g, ""));
                for (var i = 0; i < ValueList.length; i++) {
                    var a = []
                    $.each(ValueList[i], function (key, val) {
                        if (key == "CreateDatetime") {
                            //  val = new Date(parseInt(val.replace(/([\/\(\)Date])/g, "")));//timestampToDate(val)
                            val = parseInt(val.replace(/([\/\(\)Date])/g, ""));
                            a.push(val)
                        }
                        else if (key == "New") {
                            val = parseInt(val)
                            a.push(val)
                        }


                    });
                    RobotNewValues.push(a);
                }
                for (var i = 0; i < ValueList.length; i++) {
                    var a = []
                    $.each(ValueList[i], function (key, val) {
                        if (key == "CreateDatetime") {
                            //val = new Date(parseInt(val.replace(/([\/\(\)Date])/g, "")));//timestampToDate(val)
                            val = parseInt(val.replace(/([\/\(\)Date])/g, ""));
                            a.push(val)
                        }
                        else if (key == "In Progress") {
                            val = parseInt(val)
                            a.push(val)
                        }


                    });
                    RobotInprogressvalues.push(a);
                }
                for (var i = 0; i < ValueList.length; i++) {
                    var a = []
                    $.each(ValueList[i], function (key, val) {
                        //////
                        if (key == "CreateDatetime") {
                            // val = new Date(parseInt(val.replace(/([\/\(\)Date])/g, "")));//timestampToDate(val)
                            val = parseInt(val.replace(/([\/\(\)Date])/g, ""));
                            a.push(val)
                        }
                        else if (key == "Success") {
                            val = parseInt(val)
                            a.push(val)
                        }


                    });
                    RobotSuccessValues.push(a);
                }
                for (var i = 0; i < ValueList.length; i++) {
                    var a = []
                    $.each(ValueList[i], function (key, val) {
                        if (key == "CreateDatetime") {
                            //val = new Date(parseInt(val.replace(/([\/\(\)Date])/g, "")));//timestampToDate(val)
                            val = parseInt(val.replace(/([\/\(\)Date])/g, ""));
                            a.push(val);
                        }
                        else if (key == "Failure") {
                            val = parseInt(val)
                            a.push(val);
                        }


                    });
                    RobotFailureValues.push(a);
                }
            }
        },
        error: function (e) {
        }
    });

}
function exampleData2() {
    debugger;

    return [
{
    "key": "New",
    "color": "#FFFF00",
    "values": RobotNewValues,
},

{
    "key": "In Progress",
    "color": "blue",
    "values": RobotInprogressvalues,
},

{
    "key": "Success",
    "color": "#008000",
    "values": RobotSuccessValues,
},

{
    "key": "Failure",
    "color": "red",
    "values": RobotFailureValues,
},
    ]
}
//function exampleData2() {
//    

//    return [
//        {
//            "key": "New",
//            "color": "#FFFF00",
//            "values": [[2033 - 12 - 02, 35], [2033 - 12 - 03, 12], [2033-12-04, 40]],
//        },

//        {
//            "key": "In Progress",
//            "color": "blue",
//            "values": [[2033 - 12 - 02, 48], [2033 - 12 - 03, 65], [2033 - 12 - 04, 12]],
//        },

//        {
//            "key": "Success",
//            "color": "#008000",
//            "values": [[2033 - 12 - 02, 22], [2033 - 12 - 03, 92], [2033 - 12 - 04, 63]],
//        },

//        {
//            "key": "Failure",
//            "color": "red",
//            "values": [[2033 - 12 - 02, 70], [2033 - 12 - 03, 45], [2033 - 12 - 04, 15]],
//        },
//    ]
//}
function intervalRobotTimeline() {
    
    var lastd = new Date(LastValueOfArray)

    $.ajax({
        type: "GET",
        url: GlobalURL +'/api/CrudService/getRobotTimelineLive?lasttimestamp=' + (lastd.getMonth() + 1) + '/' + lastd.getDate() + '/' + lastd.getFullYear() + ' ' + lastd.getHours() + ':' + lastd.getMinutes() + ':' + lastd.getSeconds(),
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            
            result = result.replace("][", ",");
            //ValueList = $.parseJSON(result);
            if (ValueList.length != 0) {
                LastValueOfArray = parseInt(ValueList[ValueList.length - 1].CreateDateTime.replace(/([\/\(\)Date])/g, ""));

                for (var i = 0; i < ValueList.length; i++) {
                    var a = [];
                    $.each(ValueList[i], function (key, val) {
                        
                        if (key == "CreateDateTime") {
                            //  val = new Date(parseInt(val.replace(/([\/\(\)Date])/g, "")));//timestampToDate(val)
                            val = parseInt(val.replace(/([\/\(\)Date])/g, ""));
                            a.push(val)
                        }
                        else if (key == "New") {
                            val = parseInt(val)
                            a.push(val)
                        }
                    });
                    RobotNewValues.shift();
                    RobotNewValues.push(a);
                }
                for (var i = 0; i < ValueList.length; i++) {
                    var a = []
                    $.each(ValueList[i], function (key, val) {
                        if (key == "CreateDateTime") {
                            //val = new Date(parseInt(val.replace(/([\/\(\)Date])/g, "")));//timestampToDate(val)
                            val = parseInt(val.replace(/([\/\(\)Date])/g, ""));
                            a.push(val)
                        }
                        else if (key == "In Progress") {
                            val = parseInt(val)
                            a.push(val)
                        }
                    });
                    RobotInprogressvalues.shift();
                    RobotInprogressvalues.push(a);
                }
                for (var i = 0; i < ValueList.length; i++) {
                    var a = []
                    $.each(ValueList[i], function (key, val) {
                        if (key == "CreateDateTime") {
                            // val = new Date(parseInt(val.replace(/([\/\(\)Date])/g, "")));//timestampToDate(val)
                            val = parseInt(val.replace(/([\/\(\)Date])/g, ""));
                            a.push(val)
                        }
                        else if (key == "Success") {
                            val = parseInt(val)
                            a.push(val)
                        }
                    });
                    RobotSuccessValues.shift();
                    RobotSuccessValues.push(a);
                }
                for (var i = 0; i < ValueList.length; i++) {
                    var a = []
                    $.each(ValueList[i], function (key, val) {
                        // //
                        if (key == "CreateDateTime") {
                            //val = new Date(parseInt(val.replace(/([\/\(\)Date])/g, "")));//timestampToDate(val)
                            val = parseInt(val.replace(/([\/\(\)Date])/g, ""));
                            a.push(val);
                        }
                        else if (key == "Failure") {
                            val = parseInt(val)
                            a.push(val);
                        }
                    });
                    RobotFailureValues.shift();
                    RobotFailureValues.push(a);
                }
                //RobotsTimeLine();
                roboTimeLine()
            }
        },
        error: function (e) {
        }
    });
}
function roboTimeLine() {
    
    nv.addGraph(function () {
        var chart = nv.models.stackedAreaChart()

                      .x(function (d) { return d[0] })
                      .y(function (d) { return d[1] })

                      //.margin({ right: 100 })
            .height(200)
                      .useInteractiveGuideline(true)
                      //.rightAlignYAxis(true)
                      .showControls(true)
                      .clipEdge(true);

        chart.xAxis.tickFormat(function (d) {
            //console.log(d);
            return d3.time.format('%c')(new Date(d))
        });

        chart.yAxis.tickFormat(d3.format(',.2f'));
        d3.select('#chartContainer svg')
          .datum(exampleData2)

          .call(chart);

        nv.utils.windowResize(chart.update);

        return chart;
    });
}
//var IntervalMinutes = 0.0;
//var refreshId = setInterval("intervalRobotTimeline();", IntervalMinutes * 1000);

function SystemsTimeLine() {
    $("#dvLoadingSystems").hide();

    nv.addGraph(function () {
        var chart = nv.models.stackedAreaChart()
                      .x(function (d) { return d[0] })
                      .y(function (d) { return d[1] })
                      .margin({ right: 100 })
                        .height(200)
                      .useInteractiveGuideline(true)
                      //.rightAlignYAxis(true)
                      .showControls(true)
                      .clipEdge(true);

    chart.xAxis.tickFormat(function (d)
    {
        //console.log(d);
        return d3.time.format('%x')(new Date(d))
    });
        chart.yAxis.tickFormat(d3.format(',.2f'));
        d3.select('#chartSystem svg')
          .datum(exampleData1)
          .call(chart);
        //chart.stacked.dispatch.on('areaClick', function (e) {
        //    //
        //    $('.nvtooltip').remove();
        //});
        $('.nvtooltip').remove();
        nv.utils.windowResize(chart.update);
        return chart;
    });
    $.ajax({
        type: "GET",
        url: GlobalURL +'/api/CrudService/getTableDataByUserID?tablename=Systems_CommandCenter&CreateBy=' + $("#TimeDropdown").val(),
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            
            $("#dvLoadingSystems").hide();
            
            SystemValueList = $.parseJSON(result);
            //SystemValueList = $.parseJSON(SystemValueList);
            //console.log(SystemValueList);

            for (var i = 0; i < SystemValueList.length; i++) {
                var a = []
                //console.log(SystemValueList[i]);
                $.each(SystemValueList[i], function (key, val) {
                    if (key == "CreateDatetime") {
                        val = new Date(parseInt(val.replace(/([\/\(\)Date])/g, "")));//timestampToDate(val)
                        a.push(val)
                    }
                    else if (key == "Not Active") {
                        val = parseInt(val) * 100
                        a.push(val)
                    }
                });
                SystemNotActive.push(a);
            }
            for (var i = 0; i < SystemValueList.length; i++) {
                var a = []
                $.each(SystemValueList[i], function (key, val) {
                    if (key == "CreateDatetime") {
                        val = new Date(parseInt(val.replace(/([\/\(\)Date])/g, "")));//timestampToDate(val)
                        a.push(val)
                    }
                    else if (key == "In Progress") {
                        val = parseInt(val) * 100
                        a.push(val)
                    }
                });
                SystemInprogress.push(a);
            }
            for (var i = 0; i < SystemValueList.length; i++) {
                var a = []
                $.each(SystemValueList[i], function (key, val) {
                    if (key == "CreateDatetime") {
                        val = new Date(parseInt(val.replace(/([\/\(\)Date])/g, "")));//timestampToDate(val)
                        a.push(val)
                    }
                    else if (key == "Active") {
                        val = parseInt(val) * 100
                        a.push(val)
                    }
                });
                SystemActive.push(a);
            }
            for (var i = 0; i < SystemValueList.length; i++) {
                var a = []
                $.each(SystemValueList[i], function (key, val) {
                    // ////
                    if (key == "CreateDatetime") {
                        val = new Date(parseInt(val.replace(/([\/\(\)Date])/g, "")));//timestampToDate(val)
                        a.push(val);
                    }
                    else if (key == "Warning") {
                        val = parseInt(val) * 100
                        a.push(val);
                    }
                });
                SystemAvailable.push(a);
            }
        },
        error: function (e) {
            //SystemTimeLIne();
        }
    });

}
function exampleData1() {
    return [
{
    "key": "Active",
    "color": "#008000",
    "values": SystemActive,
},

{
    "key": "InActive",
    "color": "#B71C1C",
    "values": SystemNotActive,
},

{
    "key": "Inprogress",
    "color": "blue",
    "values": SystemInprogress,
},

{
    "key": "Available",
    "color": "orange",
    "values": SystemAvailable,
},

    ]
}
//var HealthCheck_count = setInterval("Connection_HealthCheck();", 9000000);
function Connection_grid(e) {
    $(".tablinks_conn").removeClass("active");
    $(e).addClass('active');

    $("#dvLoadingConn").hide();
    if (e.id == "database") {
        var t = $('#ConnectivityTable').DataTable({
            "destroy": true,
            "scrollY": 174,
            "processing": true,
            "serverSide": true,
            "paging": true,
            "pagingType": "input",
            "pageLength": 8,
            "word-break": "break-all",
            "lengthChange": false,

            "ajax":
            {
                "url": GlobalURL + '/api/CrudService/getConnectionGridData?tablename=ConnectionConfig&Type=Database',
                "contentType": "application/json",
                "type": "POST",
                "dataType": "JSON",
                "data": function (d) {
                    return JSON.stringify(d);
                },
                "dataSrc": function (json) {
                    var return_data = json;
                    var dttt = $.parseJSON(return_data.data);
                    return dttt;
                }
            },
            "oLanguage": { "sSearch": "" },
            "columns": [
                {
                    "data": "ID",
                    "title": "ID"
                },
                {
                    "data": "connectionname",
                    "title": "Name"
                },
                {
                    "data": "hostname",
                    "title": "Host"
                },
                {
                    "data": "Connection_Status",
                    "title": "Status"
                },
            ], "columnDefs": [
                //    {
                //    "targets": '_all',
                //    "defaultContent": "",
                //},
                {
                    "aTargets": [0],
                    "sClass": "hidden"
                },
                {
                    "targets": [0],
                    "orderable": true
                },
                {
                    "targets": [3],
                    render: function (data, type, row) {
                        
                        return row.Connection_Status == '1' ? "<button class='bton success' title='Click to see the details' onclick='ActiveServerDetails(this)'><img src='dist/img/greenStatus.png' style='width:50%;height:50%;'/></button>" : "<button class='bton success' title='Click to see the details' onclick='ActiveServerDetails(this)'><img src='dist/img/redStatus.png' style='width:50%;height:50%;'/></button>"
                    }
                },
            ],
        });

        t.page('first').draw('page');
        $(".dataTables_filter").addClass("pull-left");
        $($('#ConnectivityTable_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');


    }
    else if (e.id == "server") {
        
        var t = $('#ConnectivityTable').DataTable({
            "destroy": true,
            "scrollY": 174,
            "processing": true,
            "serverSide": true,
            "paging": true,
            "pagingType": "input",
            "pageLength": 8,
            "word-break": "break-all",
            "lengthChange": false,

            "ajax":
            {
                "url": GlobalURL + '/api/CrudService/getConnectionGridData?tablename=ConnectionConfig&Type=Server',
                "contentType": "application/json",
                "type": "POST",
                "dataType": "JSON",
                "data": function (d) {
                    return JSON.stringify(d);
                },
                "dataSrc": function (json) {
                    var return_data = json;
                    var dttt = $.parseJSON(return_data.data);
                    return dttt;
                }
            },
            "oLanguage": { "sSearch": "" },
            "columns": [
                {
                    "data": "ID",
                    "title": "ID"
                },
                {
                    "data": "connectionname",
                    "title": "Name"
                },
                {
                    "data": "hostname",
                    "title": "Host"
                },
                {
                    "data": "Connection_Status",
                    "title": "Status"
                },
            ], "columnDefs": [
                //    {
                //    "targets": '_all',
                //    "defaultContent": "",
                //},
                {
                    "aTargets": [0],
                    "sClass": "hidden"
                },
                {
                    "targets": [0],
                    "orderable": true
                },
                {
                    "targets": [3],
                    render: function (data, type, row) {
                        
                        return row.Connection_Status == '1' ? "<button class='bton success' title='Click to see the details' onclick='ActiveServerDetails(this)'><img src='dist/img/greenStatus.png' style='width:50%;height:50%;'/></button>" : "<button class='bton success' title='Click to see the details' onclick='ActiveServerDetails(this)'><img src='dist/img/redStatus.png' style='width:50%;height:50%;'/></button>"
                    }
                },
            ],
        });

        t.page('first').draw('page');
        $(".dataTables_filter").addClass("pull-left");
        $($('#ConnectivityTable_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
    }
    else if (e.id == "api") {
        var t = $('#ConnectivityTable').DataTable({
            "destroy": true,
            "scrollY": 174,
            "processing": true,
            "serverSide": true,
            "paging": true,
            "pagingType": "input",
            "pageLength": 8,
            "word-break": "break-all",
            "lengthChange": false,

            "ajax":
            {
                "url": GlobalURL + '/api/CrudService/getConnectionGridData?tablename=ConnectionConfig&Type=API',
                "contentType": "application/json",
                "type": "POST",
                "dataType": "JSON",
                "data": function (d) {
                    return JSON.stringify(d);
                },
                "dataSrc": function (json) {
                    var return_data = json;
                    var dttt = $.parseJSON(return_data.data);
                    return dttt;
                }
            },
            "oLanguage": { "sSearch": "" },
            "columns": [
                {
                    "data": "ID",
                    "title": "ID"
                },
                {
                    "data": "connectionname",
                    "title": "Name"
                },
                {
                    "data": "hostname",
                    "title": "Host"
                },
                {
                    "data": "Connection_Status",
                    "title": "Status"
                },
            ], "columnDefs": [
                //    {
                //    "targets": '_all',
                //    "defaultContent": "",
                //},
                {
                    "aTargets": [0],
                    "sClass": "hidden"
                },
                {
                    "targets": [0],
                    "orderable": true
                },
                {
                    "targets": [3],
                    render: function (data, type, row) {
                        
                        return row.Connection_Status == '1' ? "<button class='bton success' title='Click to see the details' onclick='ActiveServerDetails(this)'><img src='dist/img/greenStatus.png' style='width:50%;height:50%;'/></button>" : "<button class='bton success' title='Click to see the details' onclick='ActiveServerDetails(this)'><img src='dist/img/redStatus.png' style='width:50%;height:50%;'/></button>"
                    }
                },
            ],
        });

        t.page('first').draw('page');
        $(".dataTables_filter").addClass("pull-left");
        $($('#ConnectivityTable_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
    }
}

function load_ConnectivityInfo() {
    $.ajax({
        type: "GET",
        url: GlobalURL +'/api/CrudService/Connective_info()',
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
        }
    });
}