﻿gridLoad();
function gridLoad() {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getapp_tablesData',
        contentType: 'application/json; charset=utf-8',
        async: false,

        success: function (val) {
            var dataSource = $.parseJSON(val);
            for (var i in dataSource)
            {
                if (dataSource[i]["category"] == "null")
                {
                    dataSource[i]["category"] = '';
                }
            }
            var columns = $('[name="EditColumns"]');
            var t = $('#grid').DataTable({
                destroy: true,
                "scrollY": true,
                "scrollX": true,
                "data": dataSource,
                "Info": false,
                "pagingType": "simple_numbers",
                "LengthChange": true,
                "aLengthMenu": [[20, 40, 60, -1], [20, 40, 60, "All"]],
                "iDisplayLength": 20,
                "order": [[0, 'desc']],
                "columns": [
                    {
                        "data": "table_id",
                        "title": "Table Id"
                    },

                    {
                        "data": "table_name",
                        "title": "Table Name"
                    },
                    {
                        "data": "category",
                        "title": "Category"
                    },
                   {
                       "data": "status",
                       "title": "Status"
                   },
                    {
                        "data": "createby",
                        "title": "Create By"
                    },
                    {
                        "data": "updateby",
                        "title": "Update By"
                    },
                    {
                        "data": "createdatetime",
                        "title": "Create DateTime"
                    },
                    {
                        "data": "updatedatetime",
                        "title": "Update Datetime"
                    },
                    {
                        "data": "comments",
                        "title": "Comments"
                    },
                    {
                        "title": "Actions",
                        "defaultContent": "<button class='btn btn-warning btn-flat' title='Edit' onclick='editTable(this)'><i class='fa fa-edit' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Edit</span></button><button class='btn btn-warning btn-flat' title='View' onclick='viewdata(this)'><i class='fa fa-eye' aria-hidden='true' style='font-size:16px;'></i></button><button class='deleteBtnClass btn btn-flat' title='Delete' onclick='deleteTables(this)'><i class='fa fa-trash' aria-hidden='true' style='font-size:16px'></i><span class='tooltiptext deleteedit'>Delete</span></button>"
                    },

                ],
                "oLanguage": { "sSearch": "" },
                "columnDefs": [{
                    "targets": '_all',
                    "defaultContent": "",
                },
                {
                    "aTargets": [0,4,5,6,7],
                    "sClass": "hidden"
                },
                ],

            });
            t.page('first').draw('page');
            

            $("#grid_wrapper .dataTables_length label").css("display", "block");
            $(".dataTables_filter").addClass("pull-left");
            $(".dataTables_length").addClass("pull-right");
            $($('#grid_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');

        },

        error: function (e) {
            gridLoad();
        }
    });
}

function deleteTables(e) {
    swal("Are you sure you want to delete this record?", {
        buttons: ["Cancel", "Ok"],
        className: "deleterecord",
        closeOnClickOutside: false,
        closeOnEsc: false,

    });

    $(".deleterecord .swal-footer .swal-button-container .swal-button--confirm").on("click", function () {
        $("#deleteId").val('');
        $("#deleteId").val($($($(e).closest("tr")[0]).children('td:first-child')[0]).text());

        var table_id = $("#deleteId").val();
        if (table_id.length > 0) {

            $('#deleteModal').modal('hide');
            $.ajax({
                type: "GET",
                url: GlobalURL + '/api/CrudService/deleteTables?table_id=' + table_id,
                contentType: 'application/json; charset=utf-8',
                success: function (val) {
                    gridLoad();
                    if (val == "1") {
                        swal("", "Table deleted Successfully", "success")
                    }

                    else {
                        swal("", "Try again later", "error")
                    }
                },
                error: function (e) {
                    swal("", "Something Wrong", "error")
                }
            });
        }

    });
}

function editTable(e) {
    tableId = $($(e).closest("tr")[0]).children('td:first-child')[0].innerHTML;
    tableMode = 'Edit';
    if (tableMode != '') {
        layoutRefresh(window.location.origin + "/Views/Layout.html?Create_Edit", 0);
    }
}

function viewdata(e) {
    var tblName = $(e).parent().parent()[0].cells[1].innerText;
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/viewtable?tablename=' + tblName,
        contentType: 'application/json; charset=utf-8',
        async: false,

        success: function (result) {
            if (result != null)
            {
                var Columns = [];
                var fedo = $.parseJSON(result[0]);
                if (fedo.length > 0)
                {
                    $.each($.parseJSON(result[0]), function (key, value) {
                        Columns.push({ "data": value.column_name, "title": value.column_name })
                    });
                    var dataSource1 = $.parseJSON(result[1]);
                    $("#griddata").empty();
                    $("#griddata").append("<table id='grid_ViewData'></table>");
                    var tv = $('#grid_ViewData').DataTable({
                        "scrollY": true,
                        "scrollX": true,
                        "oLanguage": { "sSearch": "" },
                        "data": dataSource1,
                        "columns": Columns,

                    });
                    $("#viewModal").modal('show');
                    $("#grid_wrapper .dataTables_length label").css("display", "block");
                    $(".dataTables_filter").addClass("pull-left");
                    $(".dataTables_length").addClass("pull-right");
                    $($('#grid_filter').find('[type="search"]')[0]).attr('placeholder', 'Search...');
                }
                else
                {
                    //alert("no table exists");
                    swal("", "No Table in the Current Connected DataSource Exists with the Name", "error");
                }
            }
            
        },
    });
}

$("#btn_CreateTable").click(function () {
    tableId = '';
    tableMode = 'Add';
    layoutRefresh(window.location.origin + "/Views/Layout.html?Create_Edit", 0);
})
