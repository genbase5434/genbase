CREATE OR REPLACE FUNCTION public.ImportRobot(Project_Id integer)
RETURNS integer as $id$
declare
Robot_Id integer;
Checker integer;
Step_Id_1 integer;
StepProp_Id_1 integer;
StepProp_Id_2 integer;
StepProp_Id_3 integer;
StepProp_Id_4 integer;
Step_Id_2 integer;
StepProp_Id_5 integer;
StepProp_Id_6 integer;
StepProp_Id_7 integer;
StepProp_Id_8 integer;
Step_Id_3 integer;
StepProp_Id_9 integer;
StepProp_Id_10 integer;
StepProp_Id_11 integer;
StepProp_Id_12 integer;
StepProp_Id_13 integer;
StepProp_Id_14 integer;
StepProp_Id_15 integer;
StepProp_Id_16 integer;
StepProp_Id_17 integer;
StepProp_Id_18 integer;
StepProp_Id_19 integer;
StepProp_Id_20 integer;
StepProp_Id_21 integer;
StepProp_Id_22 integer;
StepProp_Id_23 integer;
Step_Id_4 integer;
StepProp_Id_24 integer;
StepProp_Id_25 integer;
StepProp_Id_26 integer;
StepProp_Id_27 integer;
StepProp_Id_28 integer;
StepProp_Id_29 integer;
StepProp_Id_30 integer;
StepProp_Id_31 integer;
StepProp_Id_32 integer;
StepProp_Id_33 integer;
StepProp_Id_34 integer;
StepProp_Id_35 integer;
Links_1 integer;
Links_2 integer;
Links_3 integer;
Links_4 integer;
BEGIN
select count (*) from "ROBOT" where "Project_Id" = Project_Id and "Name" ='Delete' and "Status" = true into Checker;
if Checker<1 THEN
INSERT INTO public."ROBOT"("Name","Description","RobotType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status")VALUES('Delete',null,'Robot',Project_Id,'1',now(),'1','/Date(1559543880000)/','true')returning "Id" into Robot_Id;
INSERT INTO public."STEPS"("Name","Robot_Id","Element_Id","Action_Id","Workflow_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","RuntimeUserInput","Version","Project_Id")VALUES('Start',Robot_Id,'1','1','5','1',now(),'1','6/3/2019 12:11 PM','true','9','false','1',Project_Id)returning "Id" into Step_Id_1;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_1,'Name','Start','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','1',null)returning "Id" into StepProp_Id_1;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_1,'Type','Start','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','4',null)returning "Id" into StepProp_Id_2;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_1,'Left','165','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','6',null)returning "Id" into StepProp_Id_3;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_1,'Top','76','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','7',null)returning "Id" into StepProp_Id_4;
INSERT INTO public."STEPS"("Name","Robot_Id","Element_Id","Action_Id","Workflow_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","RuntimeUserInput","Version","Project_Id")VALUES('Stop',Robot_Id,'1','2','5','1',now(),'1','6/3/2019 12:11 PM','true','10','false','1',Project_Id)returning "Id" into Step_Id_2;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_2,'Name','Stop','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','1',null)returning "Id" into StepProp_Id_5;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_2,'Type','Stop','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','4',null)returning "Id" into StepProp_Id_6;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_2,'Left','1014','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','6',null)returning "Id" into StepProp_Id_7;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_2,'Top','458','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','7',null)returning "Id" into StepProp_Id_8;
INSERT INTO public."STEPS"("Name","Robot_Id","Element_Id","Action_Id","Workflow_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","RuntimeUserInput","Version","Project_Id")VALUES('Connection',Robot_Id,'10','80','5','1',now(),'1','6/3/2019 12:11 PM','true','11','false','1',Project_Id)returning "Id" into Step_Id_3;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'Name','Connection','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','1',null)returning "Id" into StepProp_Id_9;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'Type','Connection','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','4',null)returning "Id" into StepProp_Id_10;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'Left','359','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','6',null)returning "Id" into StepProp_Id_11;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'Top','213','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','7',null)returning "Id" into StepProp_Id_12;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'ConnectionType',null,'DropDownList',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','1',null)returning "Id" into StepProp_Id_13;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'Connections',null,'DropDownList',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','2',null)returning "Id" into StepProp_Id_14;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'DB URL','localhost','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','3',null)returning "Id" into StepProp_Id_15;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'DB Username','postgres','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','4',null)returning "Id" into StepProp_Id_16;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'DB Password','pallavi','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','5',null)returning "Id" into StepProp_Id_17;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'DB Port','5432','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','6',null)returning "Id" into StepProp_Id_18;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'DB Name','stable_local','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','7',null)returning "Id" into StepProp_Id_19;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'DB Type','postgreSQL','DropDownList',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','8',null)returning "Id" into StepProp_Id_20;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'Input',null,'Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','9',null)returning "Id" into StepProp_Id_21;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'InputType','Value','DropDownList',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','10',null)returning "Id" into StepProp_Id_22;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_3,'Output','{conn}','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','11',null)returning "Id" into StepProp_Id_23;
INSERT INTO public."STEPS"("Name","Robot_Id","Element_Id","Action_Id","Workflow_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","RuntimeUserInput","Version","Project_Id")VALUES('Delete',Robot_Id,'10','78','5','1',now(),'1','6/3/2019 12:11 PM','true','12','false','1',Project_Id)returning "Id" into Step_Id_4;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_4,'Name','Delete','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','1',null)returning "Id" into StepProp_Id_24;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_4,'Type','Delete','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','4',null)returning "Id" into StepProp_Id_25;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_4,'Left','684','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','6',null)returning "Id" into StepProp_Id_26;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_4,'Top','388','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','7',null)returning "Id" into StepProp_Id_27;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_4,'Connection','{conn}','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','1',null)returning "Id" into StepProp_Id_28;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_4,'TableName','PROJECT','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','2',null)returning "Id" into StepProp_Id_29;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_4,'Condition','ID:==:11;','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','3',null)returning "Id" into StepProp_Id_30;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_4,'DeletePart','Status:==:true;','Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','4',null)returning "Id" into StepProp_Id_31;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_4,'isFullDelete','false','Checkbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','5',null)returning "Id" into StepProp_Id_32;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_4,'InputType','Value','DropDownList',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','6',null)returning "Id" into StepProp_Id_33;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_4,'Input',null,'Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','7',null)returning "Id" into StepProp_Id_34;
INSERT INTO public."STEPPROPERTIES"("Steps_Id","StepProperty","StepPropertyValue","StepPropertyType","Project_Id","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status","Order","CustomValidation")VALUES(Step_Id_4,'Output',null,'Textbox',Project_Id,'1',now(),'1','6/3/2019 12:11 PM','true','8',null)returning "Id" into StepProp_Id_35;
INSERT INTO public."LinkNodes"("StepId","Location","ChildStepIds","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status")VALUES(Step_Id_1,'164.14424601627286 76',Step_Id_3||'`'||'','Admin',now(),'Admin','6/3/201912:11 PM','true')returning "Id" into Links_1;
INSERT INTO public."LinkNodes"("StepId","Location","ChildStepIds","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status")VALUES(Step_Id_2,'1013.1442460162727 458','','Admin',now(),'Admin','6/3/201912:11 PM','true')returning "Id" into Links_2;
INSERT INTO public."LinkNodes"("StepId","Location","ChildStepIds","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status")VALUES(Step_Id_3,'358.14424601627286 213',Step_Id_4||'`'||'','Admin',now(),'Admin','6/3/201912:11 PM','true')returning "Id" into Links_3;
INSERT INTO public."LinkNodes"("StepId","Location","ChildStepIds","CreateBy","CreateDatetime","UpdateBy","UpdateDatetime","Status")VALUES(Step_Id_4,'683.1442460162729 388',Step_Id_2||'`'||'','Admin',now(),'Admin','6/3/201912:11 PM','true')returning "Id" into Links_4;
INSERT INTO public."Versions"("ID","rid","versionid","rflag","CreateDatetime","Status")VALUES(DEFAULT,Robot_Id,'1','1',now(),'true');
END IF;
return 1;
END;
$id$ LANGUAGE plpgsql;

