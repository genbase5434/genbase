﻿var RoleValue = "";
var TypeValue = "";
var initValues = [];
var modValues = [];
var finalValues = [];
var FinalAddValues = [];
var FinalDelValues = [];
(function ($) {
    $.fn.filetree = function (method) {

        var settings = { // settings to expose
            animationSpeed: 'fast',
            collapsed: true,
            console: false
        }
        var methods = {
            init: function (options) {
                // Get standard settings and merge with passed in values
                var options = $.extend(settings, options);
                // Do this for every file tree found in the document
                return this.each(function () {

                    var $fileList = $(this);

                    $fileList
                        .addClass('file-list')
                        .find('li')
                        .has('ul') // Any li that has a list inside is a folder root
                        .addClass('folder-root closed')
                        .on('click', 'a[href="#"]', function (e) { // Add a click override for the folder root links
                            e.preventDefault();
                            $(this).parent().toggleClass('closed').toggleClass('open');
                            return false;
                        });

                    //alert(options.animationSpeed); Are the settings coming in

                });
            }
        }
        if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.on("error", function () {
                //console.log(method + " does not exist in the file exploerer plugin");
            });
        }
    }

}(jQuery));
var Path;
var Type = "";
$.ajax({
    type: "GET",
    url: GlobalURL + '/api/CrudService/getTableData?tablename=ROLE',
    async: true,
    contentType: 'application/json; charset=utf-8',
    success: function (val) {
        var dataSource = JSON.parse(val);
        for (var i in dataSource) {
            $("#SelectRole").append('<option value="' + dataSource[i]["ID"] + '">' + dataSource[i]["RoleName"] + '</option>')
        }
    },
    error: function (e) {
    }
});

var source = {};
function TypeChangeOperation(roleValue) {

    $.ajax({
		type: "GET",
		async: true,
        url: GlobalURL + '/api/Dashboard/dashboardIcons?roleid=' + roleValue,
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            var dataSource = $.parseJSON(val);
            for (var i in dataSource) {
                var flag = 0;
                var Nodes = $(".Dashboard_Custom");
                for (j in Nodes) {
                    if (j < Nodes.length) {
                        if (dataSource[i].ID == Nodes[j].children[2].id.split("_")[1]) {
                            $(Nodes[j].children[2].children[0]).click();
                            flag = 1;
                            initValues.push(dataSource[i].ID);
                            break;
                        }
                    }
                }
            }
        },

        error: function (e) {
        }
    });
}

var source = {};
function RoleChangeOperation(roleValue) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getRoleMenu?roleID=' + roleValue,
        contentType: 'application/json; charset=utf-8',
        //dataType : "json",
        success: function (val) {
            var dataSource = JSON.parse(val);
            //test for menucheck 
            for (var i in dataSource) {
                var flag = 0;
                var leafNode = $(".jstree-leaf");
                var openNode = $(".jstree-open");
                for (j in leafNode) {
                    if (j < leafNode.length) {
                        if (dataSource[i].Menu_ID == leafNode[j].children[2].id.split("_")[1]) {
                            $(leafNode[j].children[2].children[0]).click();
                            flag = 1;
                            break;
                        }
                    }
                }
                if (flag != 1) {
                    for (k in openNode) {
                        if (k < openNode.length) {
                            if (dataSource[i].Menu_ID == openNode[k].children[2].id.split("_")[1] && (openNode[k].children[2].classList.value != "jstree-anchor jstree-checked") && openNode[k].hasAttribute("aria-selected=\"false\"")) {
                                $(openNode[k].children[2].children[0]).click();
                                break;
                            }
                        }
                    }
                }
            }
        },
        error: function (e) {
        }
    });
}

$(document).ready(function () {
  
    $('#SelectRole').on('change', function () {
        $('#dtreelist').jstree("refresh");
        $('#treelist').jstree("refresh");
        $('#treelist').jstree('open_all');
        RoleValue = this.value;
        TypeValue = document.getElementById('SelectType').value;
        if (RoleValue != "" && TypeValue != "" && TypeValue != "default") {
            if (RoleValue == "default") {
                $('#dtreelist').css('display', 'none');
                $('#treelist').css('display', 'none');
                document.getElementById("RegisterBtn").disabled = true;
            }
            else {

                if (TypeValue == "DashboardDropdown") {
                    $('#dtreelist').jstree("refresh");
                    $('#dtreelist').jstree('open_all');
                    TypeChangeOperation(RoleValue);
                    $('#treelist').css('display', 'none');
                    $('#dtreelist').css('display', 'block');
                    document.getElementById("RegisterBtn").disabled = false;

                }
                else if (TypeValue == "MenuDropdown") {
                    
                    $('#treelist').jstree("refresh");
                    $('#treelist').jstree('open_all');
                    RoleValue = this.value;
                    RoleChangeOperation(RoleValue);
                    $('#dtreelist').css('display', 'none');
                    $('#treelist').css('display', 'block');
                    document.getElementById("RegisterBtn").disabled = false;

                }

            }
        }

    });

    $('#SelectType').on('change', function () {
        $('#dtreelist').jstree("refresh");
        $('#treelist').jstree("refresh");
        $('#treelist').jstree('open_all');
        TypeValue = this.value;
        RoleValue = document.getElementById('SelectRole').value;
        if (RoleValue != "" && TypeValue != "" && RoleValue != "default") {
            if (TypeValue == "default") {
                $('#dtreelist').css('display', 'none');
                $('#treelist').css('display', 'none');
                document.getElementById("RegisterBtn").disabled = true;
            }
            else if (TypeValue == "DashboardDropdown") {
                $('#dtreelist').jstree("refresh");
                $('#dtreelist').jstree('open_all');
                TypeChangeOperation(RoleValue);
                $('#treelist').css('display', 'none');
                $('#dtreelist').css('display', 'block');
                document.getElementById("RegisterBtn").disabled = false;
            }
            else if (TypeValue == "MenuDropdown") {
                $('#treelist').jstree("refresh");
                $('#treelist').jstree('open_all');
                RoleChangeOperation(RoleValue);
                $('#dtreelist').css('display', 'none');
                $('#treelist').css('display', 'block');
                document.getElementById("RegisterBtn").disabled = false;
            }
        }
    });

    $("#SubmitForm").on("submit", function (e) {
        e.preventDefault();

        if (TypeValue == "MenuDropdown") {
            var AddRoleValue;
            AddRoleValue = RoleValue;
            tablename = "Role_Menu_Relation";
            var Menus = new Array();
            var MenuList = $(".MenuManager_custom");
            var MenuOpenList = $(".jstree-open")
            for (j in MenuList) {
                if (j < MenuList.length) {
                    if (MenuList[j].children[2].classList.value == "jstree-anchor jstree-checked") {
                        Menus.push(MenuList[j].children[2].id.split("_")[1]);
                    }
                }
            }
            for (k in MenuOpenList) {
                if (k < MenuOpenList.length) {
                    if (MenuOpenList[k].children[2].children[0].classList.value == "jstree-icon jstree-checkbox jstree-undetermined") {
                        Menus.push(MenuOpenList[k].children[2].id.split("_")[1]);
                    }
                }
            }

            $.ajax({
                type: "POST",
                url: GlobalURL + '/api/CrudService/hardDeleteTableRow?tablename=' + tablename + '&roleid=' + AddRoleValue,
                contentType: 'application/json; charset=utf-8',
                success: function (val) {
                    var Values = {};
                    Values["Role_ID"] = AddRoleValue;
                    for (i in Menus) {
                        if (i < Menus.length) {
                            Values["Menu_ID"] = Menus[i];
                            var values = JSON.stringify(Values);
                            $.ajax({
                                type: "POST",
                                url: GlobalURL + '/api/CrudService/createMenuTableRow?tablename=Role_Menu_Relation',
                                data: JSON.stringify(values),
                                contentType: 'application/json; charset=utf-8',
                                async: false,
                                success: function (val) {
                                },
                                error: function (e) {
                                }
                            });
                        }
                    }
                    getMenu();
                    $("#AddMenuDiv").hide();
                    $("#SelectType").val(" ");
                    swal("", "Permissions Updated Successfully", "success", { closeOnClickOutside: false }); 
                    $(".swal-button").on('click', function (event) {
                        if ($(".swal-text")[0].innerHTML == "Permissions Updated Successfully") { location.reload(); }
                    });
                },
                error: function (e) {
                }
            });
        }
        else if (TypeValue == "DashboardDropdown") {


            var AddRoleValue;
            var Values = {};
            AddRoleValue = RoleValue;
            var modValues = new Array();
            var DashboardList = $(".Dashboard_Custom");
            for (j in DashboardList) {
                if (j < DashboardList.length) {
                    if (DashboardList[j].children[2].classList.value == "jstree-anchor jstree-checked") {
                        modValues.push(DashboardList[j].children[2].id.split("_")[1]);
                    }
                }
            }

            for (var j = 0; j < initValues.length; j++) {
                if (modValues.indexOf(JSON.stringify(initValues[j])) > -1) {

                }
                else {
                    finalValues.push(initValues[j]);
                }
            }
            for (var i = 0; i < modValues.length; i++) {
                if (initValues.indexOf(parseInt(modValues[i])) > -1) {

                }
                else {
                    finalValues.push(parseInt(modValues[i]));
                }
            }
            for (var k = 0; k < finalValues.length; k++) {
                if (modValues.indexOf(finalValues[k].toString()) > -1) {
                    FinalAddValues.push(finalValues[k].toString());
                }
                else {
                    FinalDelValues.push(finalValues[k].toString());
                }

            }
            var DelVelues = JSON.stringify(FinalDelValues);
            $.ajax({
                type: "POST",
                // url: GlobalURL + '/api/Dashboard/updateDashboard?roleid=' + AddRoleValue + '&dashboardId' + values,
                url: GlobalURL + '/api/Dashboard/updateDashboard?Type=Add&roleid=' + AddRoleValue + '&AddnewdashboardId=' + JSON.stringify(FinalAddValues),
				data: JSON.stringify(DelVelues),
				async: true,
                contentType: 'application/json; charset=utf-8',
                success: function (val) {
                    var dataSource = $.parseJSON(val);
                    getDashboard();
                    $("#AddTypeDiv").hide();
                    $("#SelectRole").val(" ");
                    swal("", "Permissions Updated Successfully", "success", { closeOnClickOutside: false }); 
                    $(".swal-button").on('click', function (event) {
                        if ($(".swal-text")[0].innerHTML == "Permissions Updated Successfully") { location.reload(); }
                    });
                },
                error: function (e) {
                }
            });
        }
        initValues = [];
        modValues = [];
        finalValues = [];
        FinalAddValues = [];
        FinalDelValues = [];
    });

    $("#CancelBtn").on("click", function () {
        location.reload();
    });

    $('#dtreelist').on('changed.jstree', function (e, data) {
        document.getElementById("RegisterBtn").disabled = false;
    })

    $('#treelist').on('changed.jstree', function (e, data) {
        document.getElementById("RegisterBtn").disabled = false;
    })

    if (sessionStorage.getItem("iSessionState") == 'N') {
        location.href = "index.html";
    }

});
getMenu();
function getMenu() {
    $.ajax({
		type: "GET",
        url: GlobalURL + '/api/ScreenGeneration/getMenu?tablename=MENU',
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            var dataSource = JSON.parse(val);
            $("#TreeList").html("");
            $("#TreeList").hide();
            var i, j, k;
            for (i in dataSource) {
                if (dataSource[i].parentId == null) {
                    $("#TreeList")[0].innerHTML += "<li class='MenuManager_custom' for='parentNode'><a class='' id='Menu_" + dataSource[i].id + "' href='#'  Custom='" + dataSource[i].Class + "' customType = '" + dataSource[i].Type + "' Path='" + dataSource[i].Path + "' >" + dataSource[i].Name + " </a><ul id='subMenu_" + dataSource[i].id + "'></ul></li>"
                }
            }
            for (j in dataSource) {
                if (dataSource[j].parentId != null && typeof ($("#subMenu_" + dataSource[j].parentId)[0]) != typeof (undefined)) {
                    $("#subMenu_" + dataSource[j].parentId)[0].innerHTML += "<li class='MenuManager_custom'><a class='' id='submenu_" + dataSource[j].id + "' href='#' Custom='" + dataSource[j].Class + "' customType = '" + dataSource[j].Type + "' Path='" + dataSource[j].Path + "'>" + dataSource[j].Name + "</a><ul id='sub_" + dataSource[j].id + "'></ul></li>"
                }
            }

            for (k in dataSource) {
                if (dataSource[k].parentId != null && typeof ($("#sub_" + dataSource[k].parentId)[0]) != typeof (undefined)) {
                    $("#sub_" + dataSource[k].parentId)[0].innerHTML += "<li class='MenuManager_custom'><a class='' id='submen_" + dataSource[k].id + "' href='#' Custom='" + dataSource[k].Class + "' customType = '" + dataSource[k].Type + "' Path='" + dataSource[k].Path + "'>" + dataSource[k].Name + " </a><ul id='subOf_" + dataSource[k].id + "'></ul></li>"
                }
            }
            $('#treelist').jstree({
                "core": {
                    "check_callback": true,
                    "themes": {
                        "variant": "large",
                        "stripes": true,
                        "icons": false,
                    }
                },
                "checkbox": {
                    "three_state": true,
                    "tie_selection": false,
                    "whole_node": false
                },
                "plugins": ["checkbox", "wholerow"]
            });
        }
    });
}
getDashboard();
function getDashboard() {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableData?tablename=Dashboard',
        async: true,
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            var dataSource = JSON.parse(val);
            $("#DTreeList").html("");
            $("#DTreeList").hide();

            for (var i in dataSource) {
                var flag = 0;
                $("#DTreeList")[0].innerHTML += "<li class='Dashboard_Custom' for='parentNode'><a class='' id='Dashboard_" + dataSource[i].ID + "' >" + dataSource[i].Name + " </a></li>"
                
            }
            $('#dtreelist').jstree({
                "core": {
                    "check_callback": true,
                    "themes": {
                        "variant": "large",
                        "stripes": true,
                        "icons": false,
                    }
                },
                "checkbox": {
                    "three_state": true,
                    "tie_selection": false,
                    "whole_node": false
                },
                "plugins": ["checkbox", "wholerow"]
            });
        },
        error: function (e) {
        }
    });
}

var ContextID;
var x = 128;
$(document).bind("contextmenu", function (event) {
    ContextID = event.toElement.id.split('_')[1];
    $('#dtreelist').jstree('open_all');
    $('#treelist').jstree('open_all');
    $('.jstree-checkbox-clicked').removeClass('jstree-checkbox-clicked')
    $('.jstree-wholerow-checkbox-clicked').removeClass('jstree-wholerow-checkbox-clicked')
});

$(document).bind('click', function () {
    $('#contextMenu').hide();
    $('.jstree-checkbox-clicked').removeClass('jstree-checkbox-clicked')
    $('.jstree-wholerow-checkbox-clicked').removeClass('jstree-wholerow-checkbox-clicked')
});

function contextDashboard() {
    $("#contextMenu").css({ "top": event.pageY - x + "px" }).hide();
}

function contextMenu() {
    $("#contextMenu").css({ "top": event.pageY - x + "px" }).hide();
}