﻿gridLoad();
function gridLoad() {
    var t = $('#grid').DataTable({
        "destroy": true,
        "scrollY": true,
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "paging": true,
        "pageLength": 20,
        "lengthChange": false,
        "language": { search: '', searchPlaceholder: "Search..." },

        "ajax":
          {
            "url": GlobalURL + '/api/CrudService/getGridData?tablename=PendingSubscriptionsapproval',
              "contentType": "application/json",
              "type": "POST",
              "dataType": "JSON",
              "data": function (d) {
                  return JSON.stringify(d);
              },
              "dataSrc": function (json) {

                  json.draw = json.draw;
                  json.recordsTotal = json.recordsTotal;
                  json.recordsFiltered = json.recordsFiltered;
                  json.data = json.data;

                  var return_data = json;
                  //return return_data.data;
                  var dttt = $.parseJSON(return_data.data);
                  return dttt;
              }
          },

        "columns": [

{
    "data": "ID",

    "title": "ID"
},

{
    "data": "ServiceName",

    "title": "Service Name"
},

{
    "data": "URL",

    "title": "URL"
},

{
    "data": "UserName",

    "title": "User Name"
},

{
    "data": "DateOfSubscription",

    "title": "Date Of Subscription"
},

{
    "data": "SubscriptionStatus",

    "title": "Status",

},

],
        "columnDefs": [{
            "targets": '_all',
            "defaultContent": "",
        },
                {
                    "targets": [0],
                    "orderable": false
                },
                {
                    "aTargets": [0, ],

                    "sClass": "hidden"
                }

        ],
    });

    t.page('first').draw('page');
    $(".dataTables_filter").addClass("pull-left");

}

