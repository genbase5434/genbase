﻿
gridLoad();
function gridLoad() {
    
    var t = $('#grid').DataTable({
        "destroy": true,
        "scrollY": true,
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "paging": true,
        "pageLength": 20,
        "lengthChange": false,
        "language": { search: '', searchPlaceholder: "Search..." },

        "ajax":
          {
            "url": GlobalURL + '/api/CrudService/getGridData?tablename=PendingSubscriptions',
              "contentType": "application/json",
              "type": "POST",
              "dataType": "JSON",
              "data": function (d) {
                  
                  return JSON.stringify(d);
              },
              "dataSrc": function (json) {
                  

                  json.draw = json.draw;
                  json.recordsTotal = json.recordsTotal;
                  json.recordsFiltered = json.recordsFiltered;
                  json.data = json.data;

                  var return_data = json;
                  //return return_data.data;
                  var dttt = $.parseJSON(return_data.data);
                  return dttt;
              }
          },

        "columns": [

{
    "data": "ID",

    "title": "ID"
},

{
    "data": "ServiceName",

    "title": "Service Name"
},

{
    "data": "URL",

    "title": "URL"
},

{
    "data": "UserName",

    "title": "User Name"
},

{
    "data": "DateOfSubscription",

    "title": "Date Of Subscription"
},

{
    "data": "SubscriptionStatus",

    "title": "Status",

},
{

    "title": "Actions", "defaultContent": "<button class='btn btn-warning btn-flat' title='Accept' onclick='AcceptStatus(this);'><i class='fa fa-check' aria-hidden='true' style='font-size:16px'></i></button>&nbsp;<button class='btn btn-warning btn-flat' title='Decline' onclick='DeclineStatus(this);'><i class='fa fa-times' aria-hidden='true' style='font-size:16px'></i></button>"
}],
        "columnDefs": [{
            "targets": '_all',
            "defaultContent": "",
        },
                {
                    "targets": [0],
                    "orderable": false
                },
                {
                    "aTargets": [0, ],

                    "sClass": "hidden"
                }

        ],
    });

    t.page('first').draw('page');
    $(".dataTables_filter").addClass("pull-left");

}

//function editStatus(e) {
//    
//    $('#editModal').modal('show');
//}

var statusId = {};
function AcceptStatus(e) {
    
    $("#AcceptModal").modal('show');
    $("#IDLabel")[0].innerHTML = $(e).parent().closest('tr').find('td')[0].innerHTML;
    $("#ServiceName")[0].innerHTML = $(e).parent().closest('tr').find('td')[1].innerHTML;
    $("#UserId")[0].innerHTML = $(e).parent().closest('tr').find('td')[3].innerHTML;
    $("#DOS")[0].innerHTML = $(e).parent().closest('tr').find('td')[4].innerHTML;
    statusId["ID"] = $(e).parent().closest('tr').find('td')[0].innerHTML;
}

$("#AcceptSubmitForm").on("submit", function (e) {
    e.preventDefault();
    
    Values = {
        "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
        //  "UpdateDatetime": window.btoa(GetNow())
    };
    Values["SubscriptionStatus"] = window.btoa("Approved");
    var values = JSON.stringify(Values);
    var StatusID = JSON.stringify(statusId);
    $.ajax({
        type: "POST",
        url: GlobalURL + '/api/CrudService/UpdateTableRow?tablename=PendingSubscriptions&ID=' + StatusID,
        data: JSON.stringify(values),
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            $('#AcceptModal').modal('hide');            
            if (val.updateTableRowResult == "true") {
                swal("", "Accepted Successfully", "success")
            }
            gridLoad();
        },
        error: function (e) {
        }
    });
});

function DeclineStatus(e) {
    
    statusId = {};
    statusId["ID"] = $(e).parent().closest('tr').find('td')[0].innerHTML;
    var StatusID = JSON.stringify(statusId);
    Values = {
        "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
        //  "UpdateDatetime": window.btoa(GetNow())
    };
    Values["SubscriptionStatus"] = window.btoa("Rejected");
    var values = JSON.stringify(Values);
    $.ajax({
        type: "POST",
        url: GlobalURL + '/api/CrudService/UpdateTableRow?tablename=PendingSubscriptions&ID=' + StatusID,
        data: JSON.stringify(values),
        contentType: 'application/json; charset=utf-8',
        //dataType: 'json',

        success: function (val) {
            if (val.updateTableRowResult == "true") {
                swal("", "Rejected Successfully", "success")
            }
            gridLoad();
        },
        error: function (e) {
        }
    });
}