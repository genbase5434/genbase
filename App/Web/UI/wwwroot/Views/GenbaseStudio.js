﻿var x = localStorage.getItem("Result");
var UserDetails = JSON.parse(x);
var r = "";
var DropDownDefaultValues = {};
var DropDownTableValues = {};
DropDownDefaultValues["selectID"] = ""
DropDownDefaultValues["selectNAME"] = ""
DropDownDefaultValues["selectDESCRIPTION"] = ""
DropDownDefaultValues["selectDesktopProjectType"] = ""
DropDownDefaultValues["selectWorkFlowProjectType"] = ""
DropDownDefaultValues["selectCreateBy"] = ""
DropDownDefaultValues["selectCreateDatetime"] = ""
DropDownDefaultValues["selectUpdateBy"] = ""
DropDownDefaultValues["selectUpdateDatetime"] = ""
DropDownDefaultValues["selectStatus"] = ""
function editDisplayDropDownList(i, elementId) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/genericScreenDropDown?elementId=' + elementId,
        contentType: 'application/json; charset=utf-8',
        async: false,
        success: function (val) {
            var value = $('[name="EditColumns"]')[i].value;
            var select = $('[name="EditColumns"]')[i];
            $('[name="EditColumns"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);
            select = $('[name="EditColumns"]')[i];

            for (var j = 0; j < dropdownlist.length; j++) {
                var opt = document.createElement('option');
                if (key.length == 2) {
                    opt.text = dropdownlist[j][key[1]];
                    opt.id = dropdownlist[j][key[1]].replace(" ", "");
                    opt.value = dropdownlist[j][key[0]];
                    DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[1]];
                }
                else {
                    opt.text = opt.value = dropdownlist[j][key[0]];
                    opt.id = dropdownlist[j][key[0]].replace(" ", "");
                    DropDownTableValues[dropdownlist[j][key[0]]] = dropdownlist[j][key[0]];
                }
                select.add(opt, j);
            }
            $('[name="EditColumns"]')[i].value = value;
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}
function EditBots(resData) {
    var url = "planogram.html?" + resData;
    window.location.href = url;
}
gridLoad();
function gridLoad() {
    var x = localStorage.getItem("Result");
    var UserDetails = JSON.parse(x);
    $("#loader").show();
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/GenbaseStudio/getStudio?roleId=' + UserDetails.Role_ID + '&userId=' + UserDetails.UserName,
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            var dataSource = $.parseJSON(val);
            var columns = $('[name="EditColumns"]');
            for (i = 0; i < columns.length; i++) {
                if (columns[i].type == "select-one") {
                    var ID = columns[i].id;
                    var ElementDetails = ID.split("|");
                    var elementId = ElementDetails[1];
                    loadLOB("LOBId", false)
                    for (j = 0; j < dataSource.length; j++) {
                        dataSource[j][ElementDetails[0]] = DropDownTableValues[dataSource[j][ElementDetails[0]]];
                    }
                }
            }
            var t = $('#grid').DataTable({
                destroy: true,
                "aLengthMenu": [[20, 40, 60, -1], [20, 40, 60, "All"]],
                "iDisplayLength": 20,
                "order": [[0, 'desc']],
                "Info": false,
                "scrollX": false,
                "scrollY": true,
                "pagingType": "simple_numbers",
                "oLanguage": { "sSearch": "" },
                "columnDefs": [
                    {
                        "aTargets": [0],
                        "sClass": "hidden"
                    },
                    {
                        "targets": '_all',
                        "defaultContent": "",
                    },
                    {
                        "targets": [0],
                        "orderable": true
                    },
                ]
            });
            t.clear();
            for (i = 0; i < dataSource.length; i++) {
                var d = new Date(dataSource[i]["CreateDatetime"]);

                t.row.add([
                    "<div onclick='datatableclick(this)' style='cursor:pointer'> " + dataSource[i]["ID"] + "</div>",
                    "<div onclick='datatableclick(this)' style='cursor:pointer'> " + dataSource[i]["NAME"] + "</div>",
                    "<div onclick='datatableclick(this)' style='cursor:pointer'> " + dataSource[i]["DESCRIPTION"] + "</div>",
                    "<div onclick='datatableclick(this)' style='cursor:pointer'> " + dataSource[i]["LOBId"] + "</div>",
                    //"<div onclick='datatableclick(this)' style='cursor:pointer'> " + dataSource[i]["DesktopProjectType"] + "</div>",
                    "<div onclick='datatableclick(this)' style='cursor:pointer'> " + dataSource[i]["WorkFlowProjectType"] + "</div>",
                    "<div onclick='datatableclick(this)' style='cursor:pointer'> " + d.toLocaleDateString() + " " + d.toLocaleTimeString() + "</div>",
                    "<button class=\"btn btn-warning btn-flat\" onclick=\"editFunction(this)\"><i class=\"fa fa-edit\" aria-hidden=\"true\" style=\"font-size:16px\"></i><span class='tooltiptext deleteedit'>Edit</span></button>&nbsp;<button class=\"deleteBtnClass btn btn-flat\" onclick=\"deleteFunction(this)\"><i class=\"fa fa-trash\" aria-hidden=\"true\" style=\"font-size:16px\"></i><span class='tooltiptext deleteedit'>Delete</span></button>"
                ]);

                t.draw(false);

                $("#grid_wrapper .dataTables_length label").css("display", "block");
                $(".dataTables_filter").addClass("pull-left");
                $(".dataTables_length").addClass("pull-right");
                $($('#grid_filter').find('[type="search"]')[0]).attr('placeholder', 'Search');
            }
            t.page('first').draw('page');
            $("#loader").hide();
        },

        error: function (e) {
            gridLoad();
        }
    });
}
function datatableclick(e) {
    EditBots($($(e).closest('tr')[0]).find('td')[0].innerText.trim());
}
function editFunction(e) {
    $('#editModal').modal('show');

    var tr = $(e)[0].closest("tr");
    var td = tr.children;
    var columns = $('[name="EditColumns"]');

    for (var i = 0; i < columns.length; i++) {
        if (i == 0) {
            $('[name="EditColumns"]')[i].innerHTML = $('[name="EditColumns"]')[i].value = td[i].innerText;
        }
        else if ($('[name="EditColumns"]')[i].localName == "select") {
            $("#LOBId").val($('#LOBId option').filter(function () { return $(this).html() == td[i].innerText; }).val());
            if (td[i].innerText == "Unassigned")
                $("#LOBDiv").hide();
            else
                $("#LOBDiv").show();
            //if (td[i].innerText != "" && typeof (td[i].innerText) != typeof (undefined))
            // $('[name="EditColumns"]')[i].value = $("#" + td[i].innerText.replace(" ", "")).val().replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        else {
            if (td[i].innerText != "" && typeof (td[i].innerText) != typeof (undefined))
                $('[name="EditColumns"]')[i].value = td[i].innerText.replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
    }
}
/*
function deleteFunction(e) {
    var r = confirm("Are you sure you want to delete this record");
    if (r) {
        var tr = $(e)[0].closest("tr");
        var uid = $(tr).children('td:first-child')[0].innerText.trim();

        var x = $('th')[0];
        var columnName = $(x).attr('data-field');
        var data = {
        }
        data["ID"] = uid;
        var IdData = JSON.stringify(data);
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/DeleteTableRow?tablename=PROJECT&ID=' + IdData,
            contentType: 'application/json; charset=utf-8',
            success: function (val) {
                if (val == "Success") {
                    gridLoad();
                    swal("", "Deleted Successfully", "success")
                }
                else {
                    $("#feedbackMsg").html("Sorry cannot perform such operation");
                }
            },
            error: function (e) {
                $("#feedbackMsg").html("Something Wrong.");
            }
        });
    }
}
*/
function deleteFunction(e) {
    var script = document.createElement('script');
    script.type = 'text/javascript';

    script.src = 'https://unpkg.com/sweetalert/dist/sweetalert.min.js';
    document.body.appendChild(script);
    $('#deleteModal').modal('show');
    var tr = $(e)[0].closest("tr");
        var uid = $(tr).children('td:first-child')[0].innerText.trim();

        var x = $('th')[0];
    var columnName = $(x).attr('data-field');
   
        var data = {
        }
        data["ID"] = uid;
        var IdData = JSON.stringify(data);
   // alert(JSON.stringify(data));
   
    $('#ok').on('click', function (e) {
        
            $.ajax({
                type: "POST",
                url: GlobalURL + '/api/CrudService/DeleteTableRow?tablename=PROJECT&ID=' + IdData,
                contentType: 'application/json; charset=utf-8',
                success: function (val) {
                    
                   
                    if (val == "Success") {
                        gridLoad();
                        swal({
                            title: "Deleted",
                            text: "Deleted Successfully",
                            icon: "success",
                            button: "Ok",
                        });
                        $('#deleteModal').modal('hide');
                    }
                    else {
                        //alert("Sorry cannot perform such operation");
                        $('#deleteModal').modal('hide');
                    }
                },
                error: function (e) {
                    //alert("Something Wrong.");
                }
            });
      
    });
    }
function loadLOB(id, flag) {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableData?tablename=LOB',
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            var result = JSON.parse(val);
            $("#" + id).empty();
            if (flag) {
                $("#" + id).append('<option>Select Option</option>');
            }
            for (i in result) {
                if (!flag) {
                    DropDownTableValues[result[i]["ID"]] = result[i]["Name"]
                }
                DropDownTableValues[null] = "Unassigned"
                $("#" + id).append('<option value="' + result[i]["ID"] + '">' + result[i]["Name"] + '</option>');
            }
        },
        error: function (e) {

        }
    });
}
$(document).ready(function () {
    var x = localStorage.getItem("Result");
    var UserDetails = JSON.parse(x);
    gridLoad();
    $('#addModal').on('show.bs.modal', function () {
        loadLOB("LOB", true)
    })
    $('#editModal').on('show.bs.modal', function () {
        loadLOB("LOBId", false)
    })
    $("#LOB").change(function () {
        if ($('#LOB option:selected').val() == "Select Option") {
            $('#spn_LOB_error').show();
        }
        else {
            $('#spn_LOB_error').hide();
        }
    });
    $("#UpdateForm").on("submit", function (e) {
        e.preventDefault();
        //if (validator1.validate()) {
        var IDName = $('[name="EditColumns"]')[0].id.split("|");
        var ID = {};
        ID[IDName[0]] = $('[name="EditColumns"]')[0].value;
        columns = $('[name="EditColumns"]');
        var Values = {
            "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
            //  "UpdateDatetime": window.btoa(GetNow())
        }
        for (var i = 1; i < columns.length; i++) {
            var z = $('[name="EditColumns"]')[i].id.split("|");
            if ($('[name="EditColumns"]')[i].type == "checkbox") {
                Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].checked);
            }
            else {

                Values[z[0]] = window.btoa($('[name="EditColumns"]')[i].value);
            }
        }
        if ($("#LOBId").val() == null)
            Values["WorkFlowProjectType"] = window.btoa("Workflow");
        else
            Values["WorkFlowProjectType"] = window.btoa("Project");
        var values = JSON.stringify(Values);
        var Id = JSON.stringify(ID);
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/CrudService/updateTableRowforLob?tablename=PROJECT&Values=' + values + '&ID=' + Id,
            //data: JSON.stringify({
            //    'tablename': 'PROJECT',
            //    'Values': values,
            //    'ID': Id
            //}),

            //data: Htmlname ,
            contentType: 'application/json; charset=utf-8',
            //dataType: 'json', 
            success: function (val) {
                $("#editModal .close").click();
                if (val == "true") {
                    gridLoad();
                    swal("", "Updated Successfully", "success")
                }
                else {
                    swal("", "Project already exists, please try again!", "error");
                }
            },
            error: function (e) {

            }
        });
        //}
    });
    $("#CreateForm").on("submit", function (e) {
        if ($("#LOB").is(":visible") == true) {
            if ($('#LOB option:selected').val() == "Select Option") {
                $('#spn_LOB_error').show();
                return false;
                e.preventDefault();
            }
            else {
                $('#spn_LOB_error').hide();
            }
        }
        e.preventDefault();
        var enter_condition = 1;
        for (i = 0; i < $(e.target).find("[type='submit']").length; i++) {
            if (!$($(e.target).find("[type='submit']")[i]).prop('disabled')) {
                switch ($(e.target).find("[type='submit']")[i].id) {
                    case "popup1":
                        $("#TypeSelection").hide();
                        $("#ProjectTypeSelection").hide();
                        $("#ProjectProcessDiv").hide();
                        $("#WorkflowNameDiv").hide();
                        $("#ProjectNameDiv").hide();
                        $("#addModal button")[2].innerHTML = '<span class="fa fa-arrow-right"></span> Create';
                        $("#addModal button")[2].id = "popup2";
                        if ($("input[name='Type']:checked").val() == "Project") {
                            $("#ProjectTypeSelection").show();
                            $($("#addModal button")[1]).show();
                            $("#RobotDiv").show();
                            $("#RobotNameDiv").show();
                            $("#addModal button")[2].disabled = false;
                        }
                        $($("#addModal button")[1]).show();
                        $("#addModal button")[1].disabled = false;
                        $("#RobotName").prop("required", false);
                        $("#addModal button")[1].id = "popup0";
                        break;
                    case "popup2":
                        AddProject();
                        if (r == true) {
                            $("#addModal button")[2].disabled = true;
                            var data = {}
                            data["ProjectName"] = $("#ProjectName").val();
                            data["ProjectID"] = ProjectID;
                            data["RobotName"] = $("#RobotName").val();
                            data["RobotID"] = RobotID;
                            data["Stat"] = 'Add';
                            data["CreateBy"] = JSON.parse(localStorage.Result).User_ID

                            if ($("input[name='projectType']:checked").val() == "Process") {
                                switch ($("input[name='projectProcessType']:checked").val()) {

                                    case "FlowChart":
                                        data["RobotType"] = 'FlowChart';
                                        break;

                                    case "ValueStreamMap":
                                        data["RobotType"] = 'ValueStreamMap';
                                        break;
                                }
                            }
                            else {
                                data["RobotType"] = 'Robot';
                            }
                            var url = "planogram.html?";
                            window.location.href = url + ProjectID;
                        }
                        break;
                    case "popup5":
                        AddProject();

                        if (r == false) {

                        }
                        else {
                            var url = "planogram.html?" + ProjectID;
                            window.location.href = url;
                        }
                        break;
                    case "popup6": $("#TypeSelection").hide();
                        $("#ProjectTypeSelection").hide();
                        $("#ProjectProcessDiv").hide();
                        $("#RobotDiv").hide();
                        $("#RobotNameDiv").show();
                        $("#ProjectNameDiv").hide();
                        $("#ProjectTypeSelection").hide();
                        $("#popup2").show();
                        $("#addModal button")[1].id = "popup7";
                        $("#grid1").show();
                        $("#grid2").hide();
                        gridLoad1();
                        //$("#addModal button")[2].disabled = true;
                        //$("#addModal button")[3].disabled = true;
                        break;
                    case "popup8": $("#TypeSelection").hide();
                        $("#ProjectTypeSelection").hide();
                        $("#ProjectProcessDiv").hide();
                        $("#RobotDiv").hide();
                        $("#RobotNameDiv").show();
                        $("#ProjectNameDiv").hide();
                        $("#ProjectTypeSelection").hide();
                        $("#popup2").show();
                        $("#addModal button")[1].id = "popup9";
                        $("#grid1").hide();
                        $("#grid2").show();

                        //$("#addModal button")[2].disabled = true;
                        //$("#addModal button")[3].disabled = true;
                        break;
                    default: enter_condition = 0;
                        break;
                }
            }
            else {
                enter_condition = 0;
            }
            if (enter_condition)
                break;
        }
    });
});
var RobotID;
var ProjectID;
function resetModal() {
    $($("#addModal button")[1]).hide();
    $("#TypeSelection").show();
    $("#addModal button")[1].id = "popup1";
    $("#addModal button")[2].id = "popup1";
    $("#popup2").show();
    $("#ProjectTypeSelection").hide();
    $("#WorkflowNameDiv").hide();
    $("#ProjectProcessDiv").hide();
    $("#RobotDiv").hide();
    $("#RobotNameDiv").hide();
    $("#ProjectNameDiv").show();
    $("#addModal button")[2].disabled = false;
    //$("#addModal button")[3].disabled = true;
    $("#grid1").hide();
    $("#grid2").hide();

}
function resetPopup() {
   // $("#Robot")[0].checked = true;
    $("#Robot")[0].checked = true;
    $("#ProcessDiv").children().prop("disabled", false)
    $("#ProcessDiv").children()[0].checked = true;
    $($("#addModal button")[1]).hide();
    $("#addModal button")[2].id = "popup1";
    $("#robotGrid")[0].checked = false;
    $("#TypeSelection").show();
    $("#ProjectNameDiv").show();
    $("#Project")[0].checked = true;
    $("#addModal button")[2].disabled = false;
    //$("#addModal button")[3].disabled = true;
    $("#grid1").hide();
    $("#grid2").hide();
    $("#popup2").show();
    $("#ProjectTypeSelection").hide();
    $("#WorkflowNameDiv").hide();
    $("#ProjectProcessDiv").hide();
    $("#RobotDiv").hide();
    $("#RobotNameDiv").hide();
    $("#ProjectName").val("");
    $("#ProjectDescription").val("");
    $("#RobotName").val("");
    $("#WorkflowName").val("");
    $("#LOB").val("");
    $("#addModal button")[1].id = "popup1";
    $("#addModal").modal('show');
    $("#WorkflowName").prop("required", false);
    $("#RobotName").prop("required", false);
}
function addNew() {

}
$("#robotGrid").change(function () {
    if ($("#robotGrid").prop('checked') == true) {
        // $("#addModal button")[2].disabled = false;
        //$("#addModal button")[3].disabled = true;
        $("#addModal button")[2].id = "popup6";
        $("#addModal button")[2].innerHTML = '<span class="fa fa-arrow-right"></span> Next';
    }
    else if ($("#robotGrid").prop('checked') == false) {
        //$("#addModal button")[2].disabled = true;
        // $("#addModal button")[3].disabled = false;
        $("#addModal button")[2].id = "popup2";
        $("#addModal button")[2].innerHTML = '<span class="fa fa-arrow-right"></span> Create';
    }
});
function TypeSelect(e) {
    if (e.value == "Project") {
        $("#WorkflowNameDiv").hide();
        $("#ProjectNameDiv").show();
        $("#ProjectName").prop("required", false);
        $("#WorkflowName").prop("required", false);
        //$("#addModal button")[2].disabled = false;
        //$("#addModal button")[2].id = "popup1";
        //$("#addModal button")[3].disabled = true;
        $("#addModal button")[2].innerHTML = '<span class="fa fa-arrow-right"></span> Next';
        $("#addModal button")[2].id = "popup1";
    }
    else {
        $("#ProjectNameDiv").hide();
        $("#WorkflowNameDiv").show();
        $("#WorkflowName").prop("required", false);
        $("#ProjectName").prop("required", false);
        $("#addModal button")[2].innerHTML = '<span class="fa fa-arrow-right"></span> Create';
        //$("#addModal button")[2].disabled = false;
        $("#addModal button")[2].id = "popup5";
    }
}
function ProjectTypeSelect(e) {
    var ele = $("#ProcessDiv").children();
    $("#addModal button")[2].innerHML = '<span class="fa fa-arrow-right"></span> Create';
    if ($("input[name='projectType']:checked").val() != "Process") {
        $(".predefinedCheckbox").prop('disabled', false);
        $("#" + $("input[name='projectProcessType']:checked")[0].id).prop("checked", false);
        for (var i in ele) {
            ele[i].disabled = true;
        }
    }
    else {
        $("#FlowChart").prop("checked", true);
        $(".predefinedCheckbox").prop("checked", false);
        $(".predefinedCheckbox").prop('disabled', true);
        //$("#addModal button")[2].disabled = true;
        //$("#addModal button")[3].disabled = false;
        for (var i in ele) {

            ele[i].disabled = false;
        }
    }
}
function nextPopup(e) {
    switch (e.id) {
        case "popup1":
            $("#TypeSelection").hide();
            $("#ProjectTypeSelection").hide();
            $("#ProjectProcessDiv").hide();
            $("#WorkflowNameDiv").hide();
            $("#ProjectNameDiv").hide();
            if ($('#robotGrid').prop('checked') == false) {
                $("#addModal button")[2].disabled = true;
                //$("#addModal button")[3].disabled = false;
            }
            if ($('#robotGrid').prop('checked') == true) {
                $("#addModal button")[2].disabled = false;
                //$("#addModal button")[3].disabled = true;
            }
            $("#addModal button")[2].id = "popup6"
            if ($("input[name='Type']:checked").val() == "Project") {
                $("#ProjectTypeSelection").show();
                $($("#addModal button")[1]).show();
                $("#RobotDiv").show();
                $("#RobotNameDiv").show();
                $("#addModal button")[2].disabled = false;
                //$("#addModal button")[3].disabled = true;

            }
            $($("#addModal button")[1]).show();
            if ($('#robotGrid').prop('checked') == false) {
                $("#addModal button")[2].disabled = true;
                //$("#addModal button")[3].disabled = false;
            }
            if ($('#robotGrid').prop('checked') == true) {
                $("#addModal button")[2].disabled = false;
                //$("#addModal button")[3].disabled = true;
            }
            $("#addModal button")[1].disabled = false;
            $("#addModal button")[1].id = "popup0";
            break;
        case "popup2":
            AddProject();
            if (r == true) {
                $("#addModal button")[2].disabled = true;
                //$("#addModal button")[3].disabled = false;
                var data = {}
                data["ProjectName"] = $("#ProjectName").val();
                data["ProjectID"] = ProjectID;
                data["RobotName"] = $("#RobotName").val();
                data["RobotID"] = RobotID;
                data["Stat"] = 'Add';

                if ($("input[name='projectType']:checked").val() == "Process") {
                    switch ($("input[name='projectProcessType']:checked").val()) {

                        case "FlowChart":
                            data["RobotType"] = 'FlowChart';
                            break;

                        case "ValueStreamMap":
                            data["RobotType"] = 'ValueStreamMap';
                            break;
                    }
                }
                else {
                    data["RobotType"] = 'Robot';

                }

                var url = "planogram.html?";
                window.location.href = url + ProjectID;
            }
            break;
        case "popup5":
            AddProject();

            if (r == false) {

            } else {
                var url = "planogram.html?" + ProjectID;
                window.location.href = url;
            }
            break;
        case "popup6": $("#TypeSelection").hide();
            $("#ProjectTypeSelection").hide();
            $("#ProjectProcessDiv").hide();
            $("#RobotDiv").hide();
            $("#RobotNameDiv").show();
            $("#ProjectNameDiv").hide();
            $("#ProjectTypeSelection").hide();
            $("#popup2").show();
            $("#addModal button")[1].id = "popup7";
            $("#grid1").show();
            $("#grid2").hide();
            gridLoad1();
            $("#addModal button")[2].disabled = true;
            //$("#addModal button")[3].disabled = false;
            break;
        case "popup8": $("#TypeSelection").hide();
            $("#ProjectTypeSelection").hide();
            $("#ProjectProcessDiv").hide();
            $("#RobotDiv").hide();
            $("#RobotNameDiv").show();
            $("#ProjectNameDiv").hide();
            $("#ProjectTypeSelection").hide();
            $("#popup2").show();
            $("#addModal button")[1].id = "popup9";
            $("#grid1").hide();
            $("#grid2").show();

            $("#addModal button")[2].disabled = true;
        //$("#addModal button")[3].disabled = false;
    }
}
function AddRobot(ProjectID) {
    var data = {};
    data["Type"] = "Robot";
    if ($("[name='Type']:checked")[0].value == "Project") {
        if ($("#RobotName").val() != "")
            data["Name"] = $("#RobotName").val();
        else {
            data["Name"] = $("#ProjectName").val() + "_Robot";
            $("#RobotName").val($("#ProjectName").val() + "_Robot");
        }
        data["projectType"] = $("#ProjectType").val();
        data["Parent"] = ProjectID;
        data["CreateBy"] = JSON.parse(localStorage.Result).User_ID;
        if ($("input[name='projectType']:checked").val() == "Process") {
            data["RobotType"] = $("input[name='projectProcessType']:checked").val();
        }
        else {
            data["RobotType"] = $("input[name='projectType']:checked").val();
        }
    }
    else {
        data["Name"] = $("#WorkflowName").val();
        data["projectType"] = "Workflow_Robot";
        data["Parent"] = ProjectID;
        data["RobotType"] = "Workflow_Robot";
        data["CreateBy"] = JSON.parse(localStorage.Result).User_ID;


    }
    var dataString = JSON.stringify(data);
    $.ajax({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        type: 'POST',
        url: GlobalURL + '/api/GenbaseStudio/Create?input=' + dataString,
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (data2) {
            if (data2 != "false") {
                RobotID = data2;
            }
        },
        error: function (e) {
        }
    });
}
function AddProject() {
    var data = {};
    data["Type"] = "Project";
    data["username"] = UserDetails.UserName;
    data["projectType"] = "API"
    if ($("[name='Type']:checked")[0].value == "Project") {
        data["Name"] = $("#ProjectName").val().trim();
        data["WorkFlowProjectType"] = "Project"
        data["Description"] = $("#ProjectDescription").val();
        if ($("#LOB").val() != "Select Option") {
            data["LOBId"] = $("#LOB").val();
        } else {
            data["LOBId"] = "1";
        }
        data["CreateBy"] = UserDetails.User_ID;
    }
    else {
        data["Name"] = $("#WorkflowName").val().trim();
        data["WorkFlowProjectType"] = "Workflow"
        data["Description"] = $("#WorkflowDescription").val();
        data["CreateBy"] = UserDetails.User_ID;
    }
    var dataString = JSON.stringify(data);
    $.ajax({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        type: 'POST',
        url: GlobalURL + '/api/GenbaseStudio/Create?input=' + dataString,
        ContentType: 'application/json; charset=utf-8',
        DataType: "json",
        async: false,
        success: function (data2) {
            if (data2 != "false") {
                ProjectID = data2
                if ($("#robotGrid")[0].checked == true) {
                    AddPredefinedRobot();
                    r = true;
                }
                else {
                    AddRobot(ProjectID);
                    $("#addModal").modal("hide");
                    r = true;
                }
            }
            else {
                $("#addModal").modal("hide");
                swal("", "Project already exists, please try again!", "error");
                r = false;
            }
        },
        error: function (e) {
        }
    });
}
function previousPopup(e) {
    if (e.id == "popup0") {
        $("#RobotName").prop("required", false);
        resetModal();
        $("#addModal button")[2].innerHTML = '<span class="fa fa-arrow-right"></span> Next';
    }
    else if (e.id == "popup7") {
        $("#grid1").hide();
        $("#TypeSelection").hide();
        $("#ProjectTypeSelection").show();
        $("#ProjectProcessDiv").hide();
        $("#WorkflowNameDiv").hide();
        $("#ProjectNameDiv").hide();
        $("#RobotDiv").show();
        $("#RobotNameDiv").show();
        $($("#addModal button")[1]).show();
        if ($('#robotGrid').prop('checked') == false) {
            $("#addModal button")[2].disabled = true;
            //$("#addModal button")[3].disabled = false;
        }
        if ($('#robotGrid').prop('checked') == true) {
            $("#addModal button")[2].disabled = false;
            //$("#addModal button")[3].disabled = true;
        }
        $("#addModal button")[1].id = "popup0";
        $("#addModal button")[2].id = "popup6";
        $("#grid2").hide();
    } else if (e.id == "popup9") {
        $("#TypeSelection").hide();
        $("#ProjectTypeSelection").hide();
        $("#ProjectProcessDiv").hide();
        $("#RobotDiv").hide();
        $("#RobotNameDiv").show();
        $("#ProjectNameDiv").hide();
        $("#ProjectTypeSelection").hide();
        $("#popup2").show();
        $("#addModal button")[1].id = "popup7";
        $("#grid1").show();
        $("#grid2").hide();
        $("#addModal button")[2].disabled = false;
        //$("#addModal button")[3].disabled = true;
        gridLoad1();
    }
    else
        nextPopup(e);

}
gridLoad1();
function gridLoad1() {
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/getTableData?tablename=Category',
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            var dataSource = {};
            if (val != false) {
                dataSource = JSON.parse(val);
            }
            $("#grid1")[0].innerHTML = "";
            for (var i in dataSource) {
                $("#grid1")[0].innerHTML += "<div class='col-sm-3 predefinedRobotIcons' CategoryId='" + dataSource[i].ID + "' title='" + dataSource[i].Name + "' onclick=\"predefineBot(this)\" style='cursor:pointer;text-align:center;'> <img src='" + dataSource[i].icon + "'/><h6 >" + dataSource[i].Name.replace(/\-/g, '&#8209;') + "</h6></div>";
            }
        },
        error: function (e) {

        }
    });
}
var LOBid;
var RobotdataSource = {};
$.ajax({
    type: "GET",
    url: GlobalURL + '/api/CrudService/getTableData?tablename=PreDefinedRobot',
    contentType: 'application/json; charset=utf-8',
    success: function (val) {
        if (val != false) {
            RobotdataSource = JSON.parse(val);
        }
    },
    error: function (e) {

    }
});
function predefineBot(e) {
    $('.predefinedRobotIcons').removeClass('activeCategory');
    $(e).addClass('activeCategory');
    $("#addModal button")[2].disabled = false;
    //$("#addModal button")[3].disabled = true;
    $("#addModal button")[2].id = "popup8";
    $("#addModal button")[1].id = "popup7";
    LOBid = e.attributes["CategoryId"].nodeValue;
    $("#grid2")[0].innerHTML = "";
    for (var i in RobotdataSource) {
        if (RobotdataSource[i].Category_Id == LOBid) {
            $("#grid2")[0].innerHTML += "<div class='col-sm-3 predefinedRobots' RobotId='" + RobotdataSource[i].ID + "' title='" + RobotdataSource[i].Name + "' onclick=\"SelectedBot(this)\" style='cursor:pointer;text-align:center;'> <img src='dist/img/Controller/Icons-2/robot.png' style='height:32px;width:32px;'/><h6 >" + RobotdataSource[i].Name.replace(/\-/g, '&#8209;') + "</h6></div>";
        }
    }
}
function SelectedBot(e) {
    $('.predefinedRobots').removeClass("activerobot");
    $(e).addClass("activerobot");
    //$("#addModal button")[2].disabled = true;
    //$("#addModal button")[3].disabled = false;
    $("#addModal button")[2].id = "popup5";
    $("#addModal button")[2].innerHTML = '<span class="fa fa-arrow-right"></span> Create';
}
function AddPredefinedRobot(projectid, predefinedbotid, robotname) {
    var botid = $(".activerobot")[0].attributes["robotid"].nodeValue;
    var robotname = $("#RobotName").val();
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/CrudService/PreDefinedRobot?projectid=' + ProjectID + '&predefinedbotid=' + botid + '&robotname=' + robotname,
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (val) {

        },
        error: function (e) {

        }
    });
}