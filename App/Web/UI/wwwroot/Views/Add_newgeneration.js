﻿
function AddNewScreen() {
  
    loadRoles();
    $.ajax({
        type: "GET",
        //url: '/services/CrudService.svc/DropDownTables',
        url: GlobalURL + '/api/ScreenGeneration/DropDownTables',
        contentType: 'application/json; charset=utf-8',
        async: false,
        success: function (val) {
            var objData = $.parseJSON(val);
            $("#t_name").empty();
            var select = document.getElementById("t_name");
            var opt1 = "<option selected disabled hidden style='display: none' value=''></option>";
            $('#t_name').append(opt1);
            for (var i = 0; i < objData.length; i++) {
                var opt = document.createElement('option');
                opt.text = opt.value = objData[i].table_name;
                select.add(opt, i + 1);
            }
            var opt = document.createElement('option');
            opt.text = "Select Option";
            opt.selected = true;
            opt.value = "";
            select.add(opt, 0);
        },
        error: function (e) {
            AddNewScreen();
        }
    });
}
function validateLength(e) {
    console.log(e.id);
    if (e.id == "s_name") {
        if ($("#" + e.id).val().length > 25) {
            $("#" + e.id).css("border-color", "#B71C1C");
            $("#Validate_" + e.id)[0].innerHTML = "You have exceeded the limit!!";
            $("#Validate_" + e.id).show();
        }
        else {
            $("#" + e.id).css("border-color", "");
            $("#Validate_" + e.id).hide();
        }
    }
}
function validateContent(e) {
    if (e.id == "Roles") {
        $("#Roles").css("border-color", "");
        $("#Validate_" + e.id).hide();
    }
    else {
        if (e.id == "s_name") {
            validateLength(e);
        }
        else {
            $(e).css("border-color", "");
            $("#Validate_" + e.id).hide();
        }
    }
    $("#Validate_Submit").hide();
}

var layoutRefresh1 = false;
function insertDetails() {
    
    t = 0
    for (var s in $("[name='DisplayName']:enabled")) {
        if ($("[name='DisplayName']:enabled")[s].value == "") {
            $($("[name='DisplayName']:enabled")[s]).css("border-color", "#B71C1C");
            t++;
        }
    }
    if (($("#s_name").val() == "") || ($("#t_name").val().length == 0) || ($("#Roles").val().length == 0) || t != 0) {
        if ($("#s_name").val() == "") {
            $("#Validate_s_name")[0].innerHTML = "Please fill out this field";
            $("#Validate_s_name").show();
            $("#s_name").css("border-color", "#B71C1C");
        }
        if ($("#t_name").val().length == 0) {
            $("#Validate_t_name")[0].innerHTML = "Please fill out this field";
            $("#Validate_t_name").show();
            $("#t_name")[0].style.setProperty('border-color', '#B71C1C', 'important');
            $("#t_name").attr("onfocus", "validateContent(this)");
        }
        if ($("#Roles").val().length == 0) {
            $("#Validate_Roles")[0].innerHTML = "Please fill out this field";
            $("#Validate_Roles").show();
            $("#Roles")[0].style.setProperty("border-color", "#B71C1C", "important");
            $("#Roles").attr("onfocus", "validateContent(this)");
        }
        if ($("#s_name").val().length > 25) {
            $("#Validate_s_name")[0].innerHTML = "You have exceeded the limit!"
            $("#Validate_s_name").show();
        }
    }
    else if ($("#s_name").val().length > 25) {
        $("#Validate_s_name")[0].innerHTML = "You have exceeded the limit!"
        $("#Validate_s_name").show();
    }
    else {
        $("#SaveChanges").prop("disabled", true);
        var SName = $("#s_name").val().trim();
        var TName = $("#t_name").val().trim();
        var AInsert = $("#a_insert").prop('checked');
        var AEdit = $("#a_edit").prop('checked');
        var ADelete = $("#a_delete").prop('checked');
        var SChild = $("#s_child").prop('checked');
        var DSort = $("#d_sort").val();
        var FilterClause = $("#d_filter").val();
        var Insights = $("#Insights").val();
        var formType = $("#FormType").val();
        var RCount = $("#r_count").val();
        //var Preferences = $("#Preferences").val();
        var Preferences = "All";
        var CreateBy = (JSON.parse(localStorage.Result).User_ID);
        var UpdateBy = (JSON.parse(localStorage.Result).User_ID);

        var Fields;
        if ($("#Preferences").val() == "User")
            Fields = $("#Fields").val();
        else
            Fields = "";
        var Roles = [];
        for (var z = 0; z < $("#Roles").val().length; z++) {
            Roles[z] = $("#Roles").val()[z];
        }
        var Elements = [];
        var active = $('[name="active"]');
        var ColumnName = $('[name="ColumnName"]');
        var DisplayName = $('[name="DisplayName"]');
        var ColumnOrder = $('[name="ColumnOrder"]');
        var ShowGrid = $('[name="ShowGrid"]');
        var ShowSearch = $('[name="ShowSearch"]');
        var ShowForm = $('[name="ShowForm"]');
        var DefaultValue = $('[name="DefaultValue"]');
        var CreateBy = (JSON.parse(localStorage.Result).User_ID);
        var UpdateBy = (JSON.parse(localStorage.Result).User_ID);
        for (var i = 0; i < active.length; i++) {
            Elements.push({
                "elementId": "",
                "active": active[i].checked,
                "columnName": ColumnName[i].innerText.trim(),
                "displayName": DisplayName[i].value,
                "columnOrder": ColumnOrder[i].value,
                "showGrid": ShowGrid[i].checked,
                "showSearch": ShowSearch[i].checked,
                "showForm": ShowForm[i].checked,
                "defaultValue": DefaultValue[i].value,
                "CreateBy": CreateBy,
                "UpdateBy": UpdateBy

            });
        }
        var ScreenDetails = {
            "SName": SName,
            "TName": TName,
            "AInsert": AInsert,
            "AEdit": AEdit,
            "ADelete": ADelete,
            "SChild": SChild,
            "DSort": DSort,
            "FilterClause": FilterClause,
            "RCount": RCount,
            "Preferences": Preferences,
            "Insights": Insights,
            "formType": formType,
            "Fields": Fields,
            "Roles": Roles,
            "CreateBy": CreateBy,
            "UpdateBy": UpdateBy

        }
      
        var data1 = JSON.stringify(ScreenDetails);
        var data2 = JSON.stringify(Elements);
        //var data3 = `${localStorage.getItem('webRoot')}${'/'}`;
        $.ajax({
            type: "POST",
            url: GlobalURL + '/api/ScreenGeneration/SetPage',
            contentType: 'application/json; charset=utf-8',
            async:false,
            data: JSON.stringify({
                "data1": data1,
                "data2": data2,
               // "data3": data3
            }),
            async: true,
            success: function (val) {
                localStorage.setItem('layoutRefresh1', 'true');
                if (val == "Success") {
                   //getMenuItems();
                    getMenuItemsForScreen();
                    swal("", "Screen Generated Successfully", "success", { closeOnClickOutside: false }), window.setTimeout(function () {
                        var url = "/Views/Layout.html?Screen_Generation";
                        layoutRefresh(window.location.origin + url, 0);
                    }, 2500);
                }
                else {
                    //swal("", "Cannot Perform Such Operations.", "error")
                    swal("", "Screen name already exists", "error", { closeOnClickOutside: false });
                    $("#SaveChanges").prop("disabled", false);
                }

            },
            error: function (e) {
                swal("", "Something is going wrong.Please Try Again", "error")
                $("#SaveChanges").prop("disabled", false);
            }
        });
    }
}
function load() {
   
    $("#tbodyId").empty();
    $("#s_name").val("");
    $("#t_name").val("");
    $("#t_name").prop("disabled", false);
    $("#t_name").css("background", 'white');
    $("#a_insert").prop("checked", false);
    $("#a_edit").prop("checked", false);
    $("#a_delete").prop("checked", false);
    $("#s_child").prop("checked", false);
    $("#d_sort").val("");
    $("#d_filter").val("");
    $("#Insights").val("");
    $("#r_count").val("");
    $("#FormType").val("");
    $("#tableSection").hide();
}
var row_id;

$("#Preferences").on("change", function (e) {
    if ($("#Preferences").val() == "User") {
        $("#fieldLabel").show();
        $("#Fields").show();
    }
    else {
        $("#fieldLabel").hide();
        $("#Fields").hide();
        $("#Fields").val("");
    }
})
$("#Insights").on("change", function (e) {
    if ($("#Insights").val() == "KPI") {
        $("#Insights").show();
    }
    if ($("#Insights").val() == "Chart") {
        $("#Insights").show();
    }
    if ($("#Insights").val() == "None") {
        $("#Insights").show();
    }
})


$("#widget_type").on("change", function (e) {
    if ($("#widget_type").val() == "DropDown")
        $("#widget").css("display", "block");
    else
        $("#widget").css("display", "none");
});

$("#t_name").on("change", function (e) {

    var tableName = document.getElementById("t_name").value;
    $("#tbodyId").empty();
    $.ajax({
        type: "GET",
        url: GlobalURL + '/api/ScreenGeneration/EditView?name=' + tableName,
        contentType: 'application/json; charset=utf-8',
        success: function (val) {
            showElementScreen();
            var objData1 = JSON.parse(val);
            $("#Fields").empty();
            var select = document.getElementById("Fields");
            for (var i = 0; i < objData1.length; i++) {
                if (objData1[i].column_name.toUpperCase() == "STATUS") {
                    var RowInsert = "<tr id=" + i + "><td style=\"width:2%\"><input name='active' type='checkbox' disabled/></td><td style='width:10%' name='ColumnName' class='column-table'>" + objData1[i].column_name + "</td><td style='width:10%'><input name='DisplayName' onfocus=\"validateContent(this)\" style='width:100%' class='form-control' type='text' value=' " + objData1[i].column_name + "' disabled/></td><td style='width:5%' ><select name='ColumnOrder' class='form-control' type='text' style=\"width:100%\" disabled></select></td><td style=\"width:2%\"><label class='robot_check'><input name='ShowGrid' type='checkbox' disabled/><span class='rb_checkmark'></span></label></td><td style=\"width:2%\"><label class='robot_check'><input name='ShowSearch' type='checkbox' disabled /><span class='rb_checkmark'></span></label></td><td style=\"width:2%\"><label class='robot_check'><input name='ShowForm'  type='checkbox' disabled/><span class='rb_checkmark'></span></label></td><td style=\"width:10%\"><input name='DefaultValue' class='form-control' type='text' style=\"width:100%\" disabled/></td>";
                }
                else if (i == 0)
                    var RowInsert = "<tr id=" + i + "><td style=\"width:2%\"><input name='active' type='checkbox' onChange=\"activeChange(this)\" checked disabled/></td><td style='width:10%' name='ColumnName' class='column-table'>" + objData1[i].column_name + "</td><td style='width:10%'><input name='DisplayName' onfocus=\"validateContent(this)\" style='width:100%' class='form-control' type='text' value=' " + objData1[i].column_name + "'disabled/></td><td style='width:5%' ><select name='ColumnOrder' class='form-control' type='text' style=\"width:100%\" value='1' disabled></select></td><td style=\"width:2%\"><label class='robot_check'><input name='ShowGrid' type='checkbox' checked disabled /><span class='rb_checkmark'></span></label></td><td style=\"width:2%\"><label class='robot_check'><input name='ShowSearch' type='checkbox' checked disabled /><span class='rb_checkmark'></span></label></td><td style=\"width:2%\"><label class='robot_check'><input name='ShowForm'  type='checkbox' disabled/><span class='rb_checkmark'></span></label></td><td style=\"width:10%\"><input name='DefaultValue' class='form-control' type='text' style=\"width:100%\" disabled/></td>";
                else {
                    var RowInsert = "<tr id=" + i + "><td style=\"width:2%\"><input name='active' type='checkbox' onChange=\"activeChange(this)\" /></td><td style='width:10%' name='ColumnName' class='column-table'> " + objData1[i].column_name + "</td><td style='width:10%'><input name='DisplayName'  onfocus=\"validateContent(this)\" style='width:100%' class='form-control' type='text' value=' " + objData1[i].column_name + "'/></td><td style='width:5%' ><select name='ColumnOrder' class='form-control' type='text' onchange='DisableDropdownValues(this)' style=\"width:100%\" ></select></td><td style=\"width:2%\"><label class='robot_check'><input name='ShowGrid' type='checkbox' disabled/><span class='rb_checkmark'></span></label></td><td style=\"width:2%\"><label class='robot_check'><input name='ShowSearch' type='checkbox' disabled/><span class='rb_checkmark'></span></label></td><td style=\"width:2%\"><label class='robot_check'><input name='ShowForm'  type='checkbox' disabled /><span class='rb_checkmark'></span></label></td><td style=\"width:10%\"><input name='DefaultValue' type='text' class='form-control' style=\"width:100%\"/></td>";
                }
                var opt = document.createElement('option');
                opt.text = opt.value = objData1[i].column_name;
                select.add(opt, i);
                $("#example2").append(RowInsert);
                ColumnOrderDropDown1();
            }
        },
        error: function (e) {
            $("#tableSection").hide();
        }
    });
});

function showElementScreen() {
  
    $("#tableSection").css('display', 'block');
}

function activeChange(e) {
    if (!e.checked) {
        var Row = e.closest("tr")
        for (var i = 0; i < Row.getElementsByClassName("robot_check").length; i++) {
            Row.getElementsByClassName("robot_check")[i].children[0].checked = false
        }
        $("#" + Row.id + " input, #" + Row.id + " button, #" + Row.id + " select").prop("disabled", true);
        $(e).prop("disabled", false);
    }
    else {
        var RowID = e.closest("tr").id
        $("#" + RowID + " input, #" + RowID + " button, #" + RowID + " select").prop("disabled", false);
        $(e).prop("disabled", false);
    }
}

var ElementDetails;
function loadScreensDetails(e) {
   
    AddNewScreen();
    load();
    showElementScreen();
    $.ajax({
        type: "GET",
        //url: '/services/CrudService.svc/getScreenDetails?id=' + e,
        url: GlobalURL + '/api/ScreenGeneration/getScreenDetails?id=' + e,
        contentType: 'application/json; charset=utf-8',
        async: false,
        success: function (val) {
            var obj = val.split("|");
            $("#EditTh").show();
            var ScreenDetails = $.parseJSON(obj[0]);
            $("#s_name").val(ScreenDetails[0].Screen_Name);
            $("#t_name").val(ScreenDetails[0].Table_Name.trim());
            $("#t_name").prop("disabled", true);
            $("#t_name").css("background", '#dddddd');
            if (ScreenDetails[0].Allow_Insert)
                $("#a_insert").prop("checked", true);
            else
                $("#a_insert").prop("checked", false);

            if (ScreenDetails[0].Allow_Edit)
                $("#a_edit").prop("checked", true);
            else
                $("#a_edit").prop("checked", false);
            if (ScreenDetails[0].Allow_Delete)
                $("#a_delete").prop("checked", true);
            else
                $("#a_delete").prop("checked", false);
            if (ScreenDetails[0].Show_As_Child)
                $("#s_child").prop("checked", true);
            else
                $("#s_child").prop("checked", false);
            $("#d_sort").val(ScreenDetails[0].Default_Sort.trim());
            $("#d_filter").val(ScreenDetails[0].Default_Filter_Clause);
            $("#r_count").val(ScreenDetails[0].Row_Count);
            $("#Preferences").val(ScreenDetails[0].Preferences.trim());
            $("#Insights").val(ScreenDetails[0].Insights.trim());
            $("#FormType").val(ScreenDetails[0].FormType.trim());
            var RoleIds = $.parseJSON(obj[1]);
            var RolesValues = [];
            for (var r = 0; r < RoleIds.length; r++) {
                RolesValues.push(RoleIds[r].Role_ID);
            }
            $('#Roles').val(RolesValues);
            $("#Roles").select2();
            ElementDetails = $.parseJSON(obj[2]);
            $("#Fields").empty();
            var select = document.getElementById("Fields");
            for (var i = 0; i < ElementDetails.length; i++) {

                var RowInsert = "<tr name='ElementRow' id=tr" + ElementDetails[i].ElementId + "><td style=\"width:2%\" ><input style='width:100%' name='active' type='checkbox' onChange='activeChange(this)'";
                if (ElementDetails[i].Active == true)
                    RowInsert += "checked"
                if (ElementDetails[i].ColumnName.toUpperCase() == "ID" || ElementDetails[i].ColumnName.toUpperCase() == "STATUS")
                    RowInsert += " disabled"
                RowInsert += "/></td><td style='width:20%' name='ColumnName' class='column-table' >" + ElementDetails[i].ColumnName + "</td><td style='width:20%'><input type='text' class='form-control' name='DisplayName' style='width:100%' value='" + ElementDetails[i].DisplayName + "' onfocus=\"validateContent(this)\"/></td><td style='width:10%'><select  class='form-control' type='text' style=\"width:100%\" name='ColumnOrder' value='" + ElementDetails[i].ColumnOrder + "'></select></td><td style=\"width:2%\"><label class='robot_check'><input  name='ShowGrid' type='checkbox' "
                if (ElementDetails[i].ShowGrid == true)
                    RowInsert += "checked"
                RowInsert += "/><span class='rb_checkmark'></span></label></td><td style=\"width:2%\"><label class='robot_check'><input name='ShowSearch' type='checkbox' "
                if (ElementDetails[i].ShowSearch == true)
                    RowInsert += "checked"
                RowInsert += " /><span class='rb_checkmark'></span></label></td><td style=\"width:2%\"><label class='robot_check'><input name='ShowForm' type='checkbox' "
                if (ElementDetails[i].ShowForm == true)
                    RowInsert += "checked"
                RowInsert += "/><span class='rb_checkmark'></span></label></td>"

                if (ElementDetails[i].DefaultValue == "null")
                    ElementDetails[i].DefaultValue = "";
                RowInsert += "<td style='width:20%'><input class='form-control' name='DefaultValue' disabled='disabled' style=\"width:100%\" type='text' value='" + ElementDetails[i].DefaultValue + "'/></td><td style='width:7%'><button name='edit' id='edit" + ElementDetails[i].ElementId + "' type='button' class='btn btn-default btn-primary' style='width:100%' onclick='editModal(" + ElementDetails[i].ElementId + ")' data-toggle='modal' data-target='#EditModal'><span class='k-icon k-i-pencil'></span> Edit</button></td></tr>";
                $("#example2").append(RowInsert);
                var opt = document.createElement('option');
                opt.text = opt.value = ElementDetails[i].ColumnName;
                select.add(opt, i);

            }
            for (var i = 0; i < ElementDetails.length; i++) {
                var ColName = $("[name='ElementRow']").find("[name='ColumnName']")[i].innerHTML.toUpperCase();
                if (ColName == "ID" || ColName == "STATUS") {
                    var RowID = $("[name='ElementRow']")[i].id;
                    $("#" + RowID + " input, #" + RowID + " button, #" + RowID + " select").prop("disabled", true);
                }
                else if (!ElementDetails[i].Active) {
                    var RowID = $("[name='ElementRow']")[i].id;
                    $("#" + RowID + " input, #" + RowID + " button, #" + RowID + " select").prop("disabled", true);
                    $("#" + RowID + " [name='active']").prop("disabled", false);
                }
                if (ColName == "ID") {
                    var RowID = $("[name='ElementRow']")[i].id;
                    $("#" + RowID + " [name='active']").prop("disabled", true);
                }


            }

            if (ScreenDetails[0].Preferences == "User") {
                $("#Fields").show();
                $("#fieldLabel").show();
                $("#Fields").val(ScreenDetails[0].Fields);
            }
            //$("#SaveChanges").attr('class','fas fa-pen-alt');
            //$("#SaveChanges").attr('class',"fas fa-check").text('Update');
            $("#SaveChanges").addClass("fas fa-check").text(" Update ");
            $("#SaveChanges").attr('onclick', 'updateChanges(' + e + ')');

            ColumnOrderDropDown(ElementDetails);
            return false;
        },
        error: function (e) {

        }
    });
}

function DisableDropdownValues(e) {
    var listchosen = [];
    var num = $(e).val();
    if (num == "") { }
    else {

        for (i = 0; i < (document.getElementsByName("ColumnOrder").length) - 1; i++) {
            if (document.getElementsByName("ColumnOrder")[i + 1].value != "") {
                listchosen.push(document.getElementsByName("ColumnOrder")[i + 1].value);
            }
        }
        var columnslength = document.getElementsByName("ColumnOrder").length;
        for (i = 0; i < columnslength - 1; i++) {
                document.getElementsByName("ColumnOrder")[i + 1].options[num - 2].disabled = true;
        }
        //for (i = 0; i < (document.getElementsByName("ColumnOrder").length) - 1; i++) {
        //    for (j = 0; j < document.getElementsByName("ColumnOrder")[0].options.length; j++) {
        //        var tmp = document.getElementsByName("ColumnOrder")[0].options[j].value;
        //        if (listchosen.indexOf(tmp.toString()) == -1) {
        //        document.getElementsByName("ColumnOrder")[i + 1].options[j].disabled = false;
        //        }
        //    }
        //}
    }
}

function ColumnOrderDropDown1() {
    $("[name='ColumnOrder'] option").remove();
    for (var i = 0; i < $("[name='ColumnOrder']").length; i++) {
        var select = $("[name='ColumnOrder']")[i];
        var opt1 = document.createElement('option');
        opt1.text = opt1.value = "";
        opt1.selected = true;
        opt1.hidden = true;
        opt1.style.display = "none";
        select.add(opt1, 0);
        if (select.parentElement.parentElement.firstElementChild.firstChild.disabled == false || i == 0) {
            for (var j = 0; j < $("[name='ColumnOrder']:enabled").length; j++) {
                var opt = document.createElement('option');

                if ($($("[name='ColumnOrder']")[i]).parent().parent().find('[name="ColumnName"]')[0].innerHTML.toUpperCase() == "ID") {
                    if ($($("[name='ColumnOrder']")[j]).parent().parent().find('[name="ColumnName"]')[0].innerHTML.toUpperCase() == "ID") {
                        opt.selected = true;
                    }
                    opt.text = opt.value = j + 1;
                }
                else {
                    opt.text = opt.value = j + 2;
                }
                select.add(opt, j);
            }
        }
    }
}

function ColumnOrderDropDown(ElementDetails) {
    
    for (var i = 0; i < $("[name='ColumnOrder']").length; i++) {
        var select = $("[name='ColumnOrder']")[i];
        var opt1 = document.createElement('option');
        opt1.text = opt1.value = "";
        opt1.selected = true;
        opt1.hidden = true;
        opt1.style.display = "none";
        select.add(opt1, 0);
        if (select.parentElement.parentElement.firstElementChild.firstChild.disabled == false || i == 0) {
            for (var j = 0; j < $("[name='active']:enabled").length; j++) {

                var opt = document.createElement('option');
                if ($($("[name='ColumnOrder']")[i]).parent().parent().find('[name="ColumnName"]')[0].innerHTML.toUpperCase() == "ID") {
                    opt.text = opt.value = j + 1;
                }

                else {
                    opt.text = opt.value = j + 2;
                }
                if (ElementDetails[i].ColumnOrder == opt.value)
                    opt.selected = true;
                select.add(opt, j);
            }
        }
    }
}

function updateChanges(e) {
 
    t = 0
    for (var s in $("[name='DisplayName']:enabled")) {
        if ($("[name='DisplayName']:enabled")[s].value == "") {
            $($("[name='DisplayName']:enabled")[s]).css("border-color", "#B71C1C");
            t++;
        }

    }

    if (($("#s_name").val() == "") || ($("#t_name").val() == null) || ($("#Roles").val() == null) || t != 0) {
        if ($("#s_name").val() == "") {
            $("#Validate_s_name")[0].innerHTML = "Please fill out this field";
            $("#Validate_s_name").show();
            $("#s_name").css("border-color", "#B71C1C")
        }
        if ($("#t_name").val() == null) {
            $("#Validate_t_name")[0].innerHTML = "Please fill out this field";
            $("#Validate_t_name").show();
            $("#t_name").css("border-color", "#B71C1C");
        }
        if ($("#Roles").val() == null) {
            $("#Validate_Roles_input")[0].innerHTML = "Please fill out this field";
            $("#Validate_Roles_input").show();
            $(".k-header")[0].style.setProperty("border-color", "#B71C1C", "important");
        }

        if ($("#s_name").val().length > 25) {
            $("#Validate_s_name")[0].innerHTML = "You have exceeded the limit!"

            $("#Validate_s_name").show();

        }


    }
    else if ($("#s_name").val().length > 25) {
        $("#Validate_s_name")[0].innerHTML = "You have exceeded the limit!"

        $("#Validate_s_name").show();

    }
    else {

        if (ScreenIDForRegenration != "" || ScreenIDForRegenration != null) {
            ScreenIDForRegenration = "";
            x = true;
        }
        else {
            var x = confirm("This may lead to loss of previous data!!! Do u really want to do so!!!");
        }


        if (x) {

            var SName = $("#s_name").val().trim();
            var TName = $("#t_name").val().trim();
            var AInsert = $("#a_insert").prop('checked');
            var AEdit = $("#a_edit").prop('checked');
            var ADelete = $("#a_delete").prop('checked');
            var SChild = $("#s_child").prop('checked');
            var DSort = $("#d_sort").val();
            var FilterClause = $("#d_filter").val();
            var RCount = $("#r_count").val().trim();
            //var Preferences = $("#Preferences").val();
            var Preferences = "All";
            var Insights = $("#Insights").val();
            var formType = $("#FormType").val();

            var Fields;
            if ($("#Preferences").val() == "User")
                Fields = $("#Fields").val();
            else
                Fields = "null";
            var Roles = [];
            for (var z = 0; z < $("#Roles").val().length; z++) {
                Roles[z] = $("#Roles").val()[z];
            }

            var Elements = [];

            var ElementRow = $('[name="ElementRow"]');
            var active = $('[name="active"]');
            var ColumnName = $('[name="ColumnName"]');
            var DisplayName = $('[name="DisplayName"]');

            var ColumnOrder = $('[name="ColumnOrder"]');
            var ShowGrid = $('[name="ShowGrid"]');
            var ShowSearch = $('[name="ShowSearch"]');

            var ShowForm = $('[name="ShowForm"]');

            var DefaultValue = $('[name="DefaultValue"]');

            for (var i = 0; i < active.length; i++) {
                Elements.push({
                    "elementId": ElementRow[i].id.split("r")[1],
                    "active": active[i].checked,
                    "columnName": ColumnName[i].innerText.trim(),
                    "displayName": DisplayName[i].value,

                    "columnOrder": ColumnOrder[i].value,
                    "showGrid": ShowGrid[i].checked,
                    "showSearch": ShowSearch[i].checked,

                    "showForm": ShowForm[i].checked,

                    "defaultValue": DefaultValue[i].value,


                });
            }
            var ScreenDetails = {
                "SId": e,
                "SName": SName,
                "TName": TName,
                "AInsert": AInsert,
                "AEdit": AEdit,
                "ADelete": ADelete,
                "SChild": SChild,
                "DSort": DSort,
                "FilterClause": FilterClause,
                "RCount": RCount,
                "Preferences": Preferences,
                "Insights": Insights,
                "formType": formType,
                "Fields": Fields,
                "Roles": Roles,

            }
            var data1 = JSON.stringify(ScreenDetails);
            var data2 = JSON.stringify(Elements);
            var data3 = `${localStorage.getItem('webRoot')}${'/'}`;

            $.ajax({
                type: "POST",
                url: GlobalURL + '/api/ScreenGeneration/UpdatePage',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    "data1": data1,
                    "data2": data2,
                    "data3": data3
                }),
                async: true,
                success: function (val) {
               
                    if (val == "true") {
                        //getMenuItems();
                        getMenuItemsForScreen();
                        swal("", "Screen Updated Successfully", "success"), window.setTimeout(function () {
                            var url = "/Views/Layout.html?Screen_Generation";
                            layoutRefresh(window.location.origin + url, 0);

                        }, 2500);

                    }
                    else {

                        swal("", "Cannot Perform Such Operations.", "error")
                        $("#SaveChanges").prop("disabled", false);
                    }
                },
                error: function (e) {

                    swal("", "Something is going wrong.Please Try Again.", "error", 2500)
                    $("#SaveChanges").prop("disabled", false);
                }
            });
        }
    }

}

function editModal(id) {

    var WidgetType = "";
    var query = "";
    row_id = id;

    $.ajax({
        type: "GET",
        //url: '/services/CrudService.svc/getWidgetRow?elementId=' + id,
        url: GlobalURL + '/api/ScreenGeneration/getWidgetRow?elementId=' + id,


        contentType: 'application/json; charset=utf-8',

        async: false,
        success: function (val) {

            var Length;

            var Mandatory;
            if (val == "Not Exist") {
                WidgetType = "TextBox";
                query = "";
                Mandatory = false;

            }
            else {
                var result = $.parseJSON(val);
                WidgetType = result[0].WidgetType;
                query = result[0].query;
                Mandatory = result[0].Mandatory;
                Length = result[0].Length;
            }
            if (WidgetType == "DropDown") {
                
                $("#widget").css("display", "block");
                $("#widget_type").val(WidgetType);
                $("#query").val(query);
            }
            else {
                $("#widget").css("display", "none");
                $("#widget_type").val(WidgetType);
                $("#query").val("");
            }
            $("#mandatory")[0].checked = Mandatory;
            $("#l_number").val(Length);
            $("#column_name").val($("#tr" + id + " [name='ColumnName']")[0].innerHTML);
            $("#column_label_name").val($("#tr" + id + " [name='DisplayName']")[0].value);
            if ($("#tr" + id + " [name='ShowGrid']")[0].checked)
                $("#s_grid").prop("checked", true);
            else
                $("#s_grid").prop("checked", false);

            if ($("#tr" + id + " [name='ShowSearch']")[0].checked)
                $("#s_search").prop("checked", true);
            else
                $("#s_search").prop("checked", false);
            if ($("#tr" + id + " [name='ShowForm']")[0].checked)
                $("#s_form").prop("checked", true);
            else
                $("#s_form").prop("checked", false);

        },
        error: function (e) {

        }
    });



}


function displayDropDownList(i, elementId) {

    $.ajax({
        type: "GET",
        //url: '/services/CrudService.svc/genericScreenDropDown?elementId=' + elementId,
        url: GlobalURL + '/api/ScreenGeneration/genericScreenDropDown?elementId=' + elementId,

        contentType: 'application/json; charset=utf-8',

        async: false,
        success: function (val) {
            var select = $('[name="DefaultValue"]')[i];
            $('[name="DefaultValue"]')[i].length = 0;
            var dropdownlist = $.parseJSON(val);
            var key = Object.keys(dropdownlist[0]);

            select = $('[name="DefaultValue"]')[i];

            for (var j = 0; j < dropdownlist.length; j++) {
                var opt = document.createElement('option');
                if (key.length == 2) {
                    opt.text = dropdownlist[j][key[1]];
                    opt.value = dropdownlist[j][key[0]];
                }
                else {
                    opt.text = opt.value = dropdownlist[j][key[0]];
                }
                select.add(opt, j);
            }
        },
        error: function (e) {
            $("#feedbackMsg").html("Something Wrong.");
        }
    });
}

$("#SaveColumn").on("click", function (e) {
    var query = "null";
    if ($("#widget_type").val() == "DropDown") {
        query = $("#query").val();
    }
    ScreenColumnValues = {
        "ElementId": row_id,
        "WidgetType": $("#widget_type").val(),
        "Mandatory": $("#mandatory")[0].checked,
        "query": query,
        "Length": $("#l_number").val(),
        "CreateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
        "CreateDatetime": window.btoa(GetNow()),
        "UpdateBy": window.btoa(JSON.parse(localStorage.Result).User_ID),
        //  "UpdateDatetime": window.btoa(GetNow()),
        "Status": window.btoa(true),
    };
    ColumnValues = JSON.stringify(ScreenColumnValues);

    $('#EditModal').modal('toggle');
    $.ajax({
        type: "POST",
        //url: '/services/CrudService.svc/widget?columnDetails=' + ColumnValues,
        url: GlobalURL + '/api/ScreenGeneration/widget?columnDetails=' + ColumnValues,
        contentType: 'application/json; charset=utf-8',
        async: false,
        success: function (val) {
            
            if ($("#widget_type").val() == "DropDown") {
                
                $("#tr" + row_id + " [name='DefaultValue']")[0].outerHTML = "<select class=\"form-control\" name=\"DefaultValue\" style=\"width:100%\"></select>"
                x = $("#tr" + row_id + " [name='DefaultValue']")[0];
                for (i = 0; i < $("[name='DefaultValue']").length; i++) {
                    y = $("[name='DefaultValue']")[i]

                    if (y == x) {
                        displayDropDownList(i, row_id);
                    }
                }
            }
            else
                $("#tr" + row_id + " [name='DefaultValue']")[0].outerHTML = "<input class=\"form-control\" name=\"DefaultValue\" style=\"width:100%\"/>"


        },
        error: function (e) {

        }
    });

})


function loadRoles() {
    $.ajax({
        type: "GET",
        //url: '/services/CrudService.svc/LoadRoles',
        url: GlobalURL + '/api/ScreenGeneration/LoadRoles',
        contentType: 'application/json; charset=utf-8',

        async: false,
        success: function (val) {

            var objData = $.parseJSON(val);
            $("#Roles").empty();
            var select = document.getElementById("Roles");
            
            var i = 0;
            var opt = document.createElement('option');
            opt.text = "Select Option";
            opt.value = "Null";
            select.add(opt, i);

            var j = 0;

            do {
                var opt = document.createElement('option');
                opt.text = objData[j].RoleName;
                opt.value = objData[j].ID;

                select.add(opt, j+1);
                j++;
            }
            while (j < objData.length);

            if (window.location.search == "?Add_newgeneration") {

                $('#Roles').select2()
            }


        },
        error: function (e) {

            loadRoles();
        }
    });


}

$(document).ready(function () {

    const originURL = location.origin;
    $.getJSON(`${originURL}/webroot/root.json`).then(
        (data) => {
            console.log(data);
            localStorage.setItem('webRoot', data['path']);
        }
    );
    setInterval(function () {
        if (localStorage.isloggedin == 'N') {
            location.href = "../index.html";
        }
    }, 1000 * 1);

    if (sessionStorage.getItem("iSessionState") == 'N') {

        location.href = "../index.html";
    }
    else {

    }
});

