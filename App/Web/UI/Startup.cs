﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace UI
{
    public class Startup
    {

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //string webRoot = env.WebRootPath;
            //string fakeWebRoot = webRoot.Replace("wwwroot", "");
            //File.Delete(fakeWebRoot + "\\webroot\\root.json");



            //JObject webRootString = new JObject(
            //    new JProperty("path", webRoot)
            //    );
            //File.WriteAllText(fakeWebRoot + "\\webroot\\root.json", webRootString.ToString());



            //using (StreamWriter sw = File.CreateText(fakeWebRoot + "\\webroot\\root.json"))
            //using (JsonTextWriter writer = new JsonTextWriter(sw))
            //{
            //    webRootString.WriteTo(writer);
            //}

            //string webRoot = env.WebRootPath;

            //JObject webRootString = new JObject(
            //    new JProperty("path", webRoot)
            //    );
            //File.WriteAllText(@"wwwroot/webroot/root.json", webRootString.ToString());

            //using (StreamWriter sw = File.CreateText(@"wwwroot/webroot/root.json"))
            //using (JsonTextWriter writer = new JsonTextWriter(sw))
            //{
            //    webRootString.WriteTo(writer);
            //}

            //Raja, Bhargav,Shiva
            //if (env.IsDevelopment())
            //{
            //    string webRoot = env.WebRootPath;

            //    JObject webRootString = new JObject(
            //        new JProperty("path", webRoot)
            //        );
            //    File.WriteAllText(@"wwwroot/webroot/root.json", webRootString.ToString());

            //    using (StreamWriter sw = File.CreateText(@"wwwroot/webroot/root.json"))
            //    using (JsonTextWriter writer = new JsonTextWriter(sw))
            //    {
            //        webRootString.WriteTo(writer);
            //    }

            //    app.UseDeveloperExceptionPage();
            //}

            if (env.IsDevelopment())
			{
				//manikanta

				//string webRoot = Directory.GetCurrentDirectory().ToString();
				//string path = webRoot + "\\wwwroot" + "\\webroot";
				//string mf = "root" + DateTime.UtcNow.ToString().Replace("_", string.Empty).Replace(" ", string.Empty).Replace(":", string.Empty) + ".json";
				//string mainpaty = Path.Combine(path, mf);
				//if (!Directory.Exists(path))
				//{
				//	Directory.CreateDirectory(path);
				//}
				//if (!System.IO.File.Exists(mainpaty))
				//{
				//	System.IO.File.Create(mainpaty).Close();
				//}

				//JObject webRootString = new JObject(new JProperty("path", path));
				//System.IO.File.WriteAllText(mainpaty, webRootString.ToString());

				//using (StreamWriter sw = System.IO.File.CreateText(mainpaty))
				//using (JsonTextWriter writer = new JsonTextWriter(sw))
				//{
				//	webRootString.WriteTo(writer);
				//}

				app.UseDeveloperExceptionPage();
			}

			if (env.IsProduction())
			{
			//	manikanta
				//string webRoot = Directory.GetCurrentDirectory().ToString();
				//string path = webRoot + "\\wwwroot" + "\\webroot";
				//string mf = "root" + DateTime.UtcNow.ToString().Replace("_", string.Empty).Replace(" ", string.Empty).Replace(":", string.Empty) + ".json";
				//string mainpaty = Path.Combine(path, mf);
				//if (!Directory.Exists(path))
				//{
				//	Directory.CreateDirectory(path);
				//}
				//if (!System.IO.File.Exists(mainpaty))
				//{
				//	System.IO.File.Create(mainpaty).Close();
				//}
				//JObject webRootString = new JObject(new JProperty("path", path));
				//System.IO.File.WriteAllText(mainpaty, webRootString.ToString());
				//using (StreamWriter sw = System.IO.File.CreateText(mainpaty))
				//using (JsonTextWriter writer = new JsonTextWriter(sw))
				//{
				//	webRootString.WriteTo(writer);
				//}

				app.UseDeveloperExceptionPage();

			}






			app.UseDefaultFiles();
            /*app.UseDefaultFiles(new DefaultFilesOptions
            {
                DefaultFileNames = new List<string> { "wwwroot/Views/index.html" }
            });*/
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot/Views")),
                RequestPath = new PathString("")
            });
            app.UseDirectoryBrowser(new DirectoryBrowserOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot/Views")),
                RequestPath = new PathString("")
            });
            app.UseFileServer(enableDirectoryBrowsing: true);

        }

    }
}
